<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Stacionary;

/**
 * StacionarySearch represents the model behind the search form about `backend\models\Stacionary`.
 */
class StacionarySearch extends Stacionary
{
    public $status;
    public $fullName;
    public $room;
    public $place;
    public $doctor;
    public $user;
    public $updatedUser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place_id', 'doctor_id', 'id', 'pacient_id', 'department_id', 'type', 'user_id', 'updated_user_id', 'parent', 'diagnosis_id'], 'integer'],
            [['date_stacionary_out', 'date_stacionary','updatedUser', 'user', 'status', 'place', 'room','room_id','doctor','fullName', 'description', 'created_at', 'updated_at'], 'safe'],
            [['date_stacionary', 'created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stacionary::find();
        $query->leftJoin('{{%handbook_stacionary}} AS stac', 'place_id=stac.id');
        //$query->leftJoin('{{%handbook_room}} AS room', 'stac.room_id=room.id');
        //$query->leftJoin('{{%pacient}} AS pacient', 'pacient_id=pacient.id');
        //$query->leftJoin('{{%profile}} AS profile', 'doctor_id=profile.user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                '{{%stacionary}}.pacient_id',
                'fullName' => [
                   'asc' => ['{{%stacionary}}.pacient_id' => SORT_ASC],
                   'desc' => ['{{%stacionary}}.pacient_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'doctor_id',
                'doctor' => [
                   'asc' => ['doctor_id' => SORT_ASC],
                   'desc' => ['doctor_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'department_id',
                'room_id',
                'place_id',
                'type',
                'description',
                'created_at',
                'updated_at',
                'user_id',
                'user' => [
                   'asc' => ['user_id' => SORT_ASC],
                   'desc' => ['user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['updated_user_id' => SORT_ASC],
                   'desc' => ['updated_user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updated_user_id',
                'date_stacionary',
                'parent',
                'date_stacionary_out',
                'diagnosis_id',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%stacionary}}.id' => $this->id,
            '{{%stacionary}}.pacient_id' => $this->pacient_id,
            //'pacient_id' => $this->fullName,
            '{{%stacionary}}.department_id' => $this->department_id,
            '{{%stacionary}}.room_id' => $this->room_id,
            'stac.id' => $this->place_id,
            'stac.status' => $this->status,
            '{{%stacionary}}.doctor_id' => $this->doctor_id,
            //'doctor_id' => $this->doctor,
            '{{%stacionary}}.type' => $this->type,
            '{{%stacionary}}.user_id' => $this->user_id,
            '{{%stacionary}}.updated_user_id' => $this->updated_user_id,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
            '{{%stacionary}}.parent' => $this->parent,

        ]);

        $query->andFilterWhere(['like', '{{%stacionary}}.description', $this->description])
                ->andFilterWhere(['like', '{{%stacionary}}.created_at', $this->created_at])
                ->andFilterWhere(['like', '{{%stacionary}}.updated_at', $this->updated_at])
                ->andFilterWhere(['like', '{{%stacionary}}.date_stacionary', $this->date_stacionary])
                ->andFilterWhere(['like', '{{%stacionary}}.date_stacionary_out', $this->date_stacionary_out]);

        return $dataProvider;
    }
}
