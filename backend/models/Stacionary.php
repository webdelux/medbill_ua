<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbook\models\Department;
use backend\modules\handbook\models\HandbookStacionary;
use backend\models\Pacient;
use backend\modules\handbook\models\HandbookRoom;

/**
 * This is the model class for table "{{%stacionary}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $department_id
 * @property integer $room_id
 * @property integer $place_id
 * @property integer $doctor_id
 * @property integer $type
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $date_stacionary
 * @property integer $parent
 * @property string $date_stacionary_out
 * @property integer $diagnosis_id
 */

class Stacionary extends \yii\db\ActiveRecord
{
    public $pacientFullName;
    public $doctorFullName;
    public $hasParent;
    public $modelAlert;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stacionary}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'pacient_id', 'department_id', 'place_id', 'doctor_id', 'type', 'user_id', 'updated_user_id', 'parent', 'diagnosis_id' ], 'integer'],
            [['doctorFullName', 'pacientFullName', 'type', 'pacient_id', 'department_id', 'room_id', 'place_id', 'doctor_id', 'date_stacionary', 'diagnosis_id' ], 'required'],
            [['hasParent', 'date_stacionary_out', 'modelAlert'], 'safe'],
            ['doctorFullName', function ($attribute, $params) {
                if (!$this->doctor_id) {
                    $this->addError($attribute, 'The Doctors name is incorrect!');
                }
            }],
            ['pacientFullName', function ($attribute, $params) {
                if (!$this->pacient_id) {
                    $this->addError($attribute, 'The Pacients name is incorrect!');
                }
            }],
           // [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'department_id' => Yii::t('app', 'Departments'),
            'place_id' => Yii::t('app', 'Place'),
            'doctor_id' => Yii::t('app', 'Doctor'),
            'type' => Yii::t('app', 'Type'),            // Тип операції: 1 - Поступив; 2 - Вибув;
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'pacientFullName' => Yii::t('app', 'Pacient'),
            'doctorFullName' => Yii::t('app', 'Doctor'),
            'room_id' => Yii::t('app', 'Room'),
            'date_stacionary' => Yii::t('app', 'Entered') . ' ' . Yii::t('app', 'Date'),
            'parent' => Yii::t('app', "The mother's stay near the of a sick child"),
            'hasParent' => Yii::t('app', "The mother's stay near the of a sick child"),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'date_stacionary_out' => Yii::t('app', 'Released') . ' ' . Yii::t('app', 'Date'),

        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->type == 1)                                           // Якщо розміщуємо пацієнта,
                if ($this->isTherePacient($this->pacient_id, $this->id)) {    // але його вже розміщено, то
                    return false;                                             //  попередження, - не зберігаємо, виходимо.
                }

            if ($insert)
            {
                if (!$this->user_id){
                    $this->user_id = Yii::$app->user->id;
                }

                $this->updated_at = 0;

                return $this->setStatus($this->place_id, $this->type);  // Якщо вільне ліжко, змінюємо статус та зберігаємо.

            } else {
                $this->updated_user_id = Yii::$app->user->id;


                if ($this->place_id <> $this->oldAttributes['place_id']){               //Якщо змінили ліжко,

                    if ($this->setStatus($this->place_id, $this->type)){                // і нове ліжко успішно зайняте,
                        $this->setStatus($this->oldAttributes['place_id'], 2);          // то очищаємо статус для "старого" ліжка.

                        return true;

                    } else {

                        return false;                                           // Нове ліжко зайнете. Прериваємо зберігання моделі.
                    }
                } else {

                    return $this->setStatus($this->place_id, $this->type);  // Якщо вільне ліжко, змінюємо статус та зберігаємо.
                }
            }

        } else {

            return false;
        }
    }

    public function setStatus($place_id, $type)
    {
        $status = HandbookStacionary::find()
                ->andWhere(['id' => $place_id])
                ->one();

        if (($status->pacient_id == 0) && ($type == 1))
        {  //  Якщо вільно та тип = Прибув
            $status->pacient_id = $this->pacint_id;
            $status->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'This place is occupied!'));
            $this->modelAlert = Yii::t('app', 'This place is occupied!');

            //return false;
        }
        elseif ($type == 2)
        {  //  Якщо тип = Вибув
            $status->pacient_id = 0;
            $status->save();

            //Yii::$app->session->setFlash('success', Yii::t('app', 'This place is exempted!'));
        }
        elseif (($status->pacient_id == $this->pacient_id) && ($type == 1))
        {  //  Якщо він же зайняв та тип = Прибув
            //Yii::$app->session->setFlash('success', Yii::t('app', 'This place is exempted! 3'));
        }
        else
        {  //  Якщо зайнято
            Yii::$app->session->setFlash('danger', Yii::t('app', 'This place is not empty!'));
            $this->modelAlert = Yii::t('app', 'This place is not empty!');

            return false;
        }

        return true;
    }

    public function isTherePacient($pacient, $id)            //Перевіряємо, чи немає дубля пацієнта
    {
        $query = Stacionary::find();

        if (isset($id)){
            $query->andWhere(['!=','id', $id]);            //крім вказаного (поточного) запису
        }

        $double = $query
                ->andWhere(['pacient_id' => $pacient])
                ->andWhere(['date_staconary_out' => 0])
                ->one();

        if (isset($double)){
            Yii::$app->session->setFlash('danger', Yii::t('app', 'This pacient is placed already!'));
            $this->modelAlert = Yii::t('app', 'This pacient is placed already!');

            return true;
        }

        return false;                                  // немає дубля.

    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(\backend\modules\user\models\Profle::className(), ['user_id' => 'doctor_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Deprtment::className(), ['id' => 'department_id']);
    }

    public function getPlace()
    {
        return $this->hasOne(HandbookStacionary::className(), ['id' => 'place_id']);
    }

    public function getRoom()
    {
        return $this->hasOne(HandbookRoom::className(), ['id' => 'room_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnsis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'type' => [
                '1' => Yii::t('app', 'Entered'),
                '2' => Yii::t('app', 'Released')
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * @inheritdoc
     * @return StacionaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StacionaryQuery(get_called_class());
    }
}
