<?php

namespace backend\models;

use Yii;
use paulzi\adjacencylist\AdjacencyListBehavior;
use backend\models\User;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $name
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'user_id', 'updated_user_id'], 'integer'],
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent category'),
            'sort' => Yii::t('app', 'Sort'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => AdjacencyListBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    // ІНША РЕАЛІЗАЦІЯ МЕТОДУ! повертає назву відділення з його батьківськими відділеннями. Можна задати глибину виводу.
    public static function parentsName($id, $depth = null)
    {
        $model = (new Category)->treeListParent((new Category)->treeData($id));

        if ($depth)
        if ($model)
        {
            if (sizeOf($model) > 0)
            {
                $out = '';

                foreach ($model as $parent)
                {
                    if (strlen($out) > 1)
                        $out = $out . '-' . $parent['name'];
                    else
                        $out = $parent['name'];
                }

                return $out;
            } else
                return null;
        }
        return null;
    }

    public static function childList($parentId = 1)
    {
        $model = Category::findOne(['id' => $parentId]);

        return ArrayHelper::map($model->getChildren()->all(), 'id', 'name');
    }

    private $out = [];

    public function treeList($data, $level = 0)
    {
        foreach ($data as $item)
        {
            $this->out[$item['id']] = str_repeat(" - ", $level). $item['text'];

            if (isset($item['nodes']))
            {
                $this->treeList($item['nodes'], $level + 1);
            }
        }

        return $this->out;
    }

    public function treeChild($id)
    {
        $model = Category::findOne(['id' => $id]);
        if ($model)
        {
            $childrens = $model->getChildren()->all();
            if (sizeOf($childrens) > 0)
            {
                $out = [];
                foreach ($childrens as $children)
                {
                    $out[] = [
                        'id' => $children->id,
                        'parent_id' => $children->parent_id,
                        'sort' => $children->sort,
                        'description' => $children->description,
                        'user_id' => $children->user_id,
                        'updated_user_id' => $children->updated_user_id,
                        'created_at' => $children->created_at,
                        'updated_at' => $children->updated_at,
                        'text' => $children->name,
                        'icon' => 'fa fa-folder-o',
                        'href' => Url::to(['view', 'id' => $children->id]),
                        'nodes' => $this->treeChild($children->id),
                    ];
                }
                return $out;
            }
            else
                return null;
        }
        return null;
    }

    public function treeData($id)
    {
        $root = Category::findOne(['id' => $id]);

        $out[] = [
            'id' => $root->id,
            'text' => $root->name,
            'parent_id' => $root->parent_id,
            'sort' => $root->sort,
            'description' => $root->description,
            'user_id' => $root->user_id,
            'updated_user_id' => $root->updated_user_id,
            'created_at' => $root->created_at,
            'updated_at' => $root->updated_at,
            'icon' => 'fa fa-folder-o',
            'href' => Url::to(['view', 'id' => $root->id]),
            'nodes' => $this->treeChild($root->id),
        ];

        return $out;
    }

    public function treeListParent($data)
    {
        foreach ($data as $item)
        {
            $this->out[$item['id']] = [
                'id' => $item['id'],
                'parent_id' => $item['parent_id'],
                'name' => $item['text'],
                'sort' => $item['sort'],
                'description' => $item['description'],
                'user_id' => $item['user_id'],
                'updated_user_id' => $item['updated_user_id'],
                'created_at' => $item['created_at'],
                'updated_at' => $item['updated_at'],
           ];

            if (isset($item['nodes']))
            {
                $this->treeListParent($item['nodes']);
            }
        }

        return $this->out;
    }
}
