<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PacientService;

/**
 * PacientServiceSearch represents the model behind the search form about `backend\models\PacientService`.
 */
class PacientServiceSearch extends PacientService
{
    public $fullName;
    public $user;
    public $updatedUser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'user_id', 'updated_user_id', 'diagnosis_id'], 'integer'],
            [['total', 'discount'], 'number'],
            [['updatedUser', 'user', 'service_id', 'fullName','docum', 'description', 'created_at', 'updated_at'], 'safe'],
            [['date_service', 'created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PacientService::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
           'attributes' => [
                'id',
                'fullName' => [
                   'asc' => ['pacient_id' => SORT_ASC],
                   'desc' => ['pacient_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'total',
                'discount',
                'docum',
                'description',
                'user' => [
                    'asc' => ['user_id' => SORT_ASC],
                    'desc' => ['user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'updatedUser' => [
                    'asc' => ['updated_user_id' => SORT_ASC],
                    'desc' => ['updated_user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'created_at',
                'updated_at',
                'service_id',
                'date_service',
                'diagnosis_id'
           ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%pacient_service}}.id' => $this->id,
            '{{%pacient_service}}.pacient_id' => $this->pacient_id,
            '{{%pacient_service}}.service_id' => $this->service_id,
            '{{%pacient_service}}.total' => $this->total,
            '{{%pacient_service}}.discount' => $this->discount,
            '{{%pacient_service}}.user_id' => $this->user_id,
            '{{%pacient_service}}.updated_user_id' => $this->updated_user_id,
            //'{{%pacient_service}}.created_at' => $this->created_at,
            //'{{%pacient_service}}.updated_at' => $this->updated_at,
            'diagnosis_id' => $this->diagnosis_id,
        ]);

        $query->andFilterWhere(['like', 'docum', $this->docum])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'date_service', $this->date_service]);

        return $dataProvider;
    }
}
