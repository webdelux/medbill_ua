<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property string $total
 * @property string $date_payment
 * @property string $discount
 *
 * @property string $type  - // Тип операції: 1 - Внесено кошти; 2 - Повернуто кошти;
 *
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $payment_method_id
 */
class Payment extends \yii\db\ActiveRecord
{
     public $pacientFullName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'user_id', 'updated_user_id', 'payment_method_id', 'type'], 'integer'],
            [['total', 'discount'], 'number'],
            [['total', 'pacient_id'], 'required'],
            [['created_at', 'updated_at', 'date_payment'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'pacientFullName' => Yii::t('app', 'Pacient'),
            'total' => Yii::t('app', 'Total'),
            'type' => Yii::t('app', 'Type'),        // Тип операції: 1 - Нарахування; 2 - Оплата;
            'discount' => Yii::t('app', 'Discount'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'payment_method_id' => Yii::t('app', 'Payment method'),
            'date_payment' => Yii::t('app', 'Date'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getPaymentMethod()
    {
        return $this->hasOne(\backend\modules\handbook\models\PaymentMethods::className(), ['id' => 'payment_method_id']);
    }

    /**
     * @inheritdoc
     * @return PaymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentQuery(get_called_class());
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'type' => [
                '1' => Yii::t('app', 'Got payment'),
                '2' => Yii::t('app', 'Refund a payment'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
}
