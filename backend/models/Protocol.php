<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbook\models\Department;
use yii\helpers\ArrayHelper;
use backend\models\Pacient;
use backend\modules\pattern\models\ProtocolPattern;

/**
 * This is the model class for table "{{%protocol}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $department_id
 * @property integer $doctor_id
 * @property integer $protocol_id
 * @property integer $refferal_id
 * @property integer $category_id
 * @property string $protocol_date
 * @property string $total
 * @property string $discount
 * @property integer $status
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $type
 * @property string $place
 * @property integer $diagnosis_id
 */
class Protocol extends \yii\db\ActiveRecord
{

    public $pacientFullName;
    public $doctorFullName;

    public $protocol_date_start;
    public $protocol_date_end;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%protocol}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'department_id', 'doctor_id', 'protocol_id', 'refferal_id', 'category_id', 'status', 'user_id', 'updated_user_id', 'type', 'place', 'diagnosis_id'], 'integer'],
            [['protocol_date', 'created_at', 'updated_at'], 'safe'],
            [['total', 'discount'], 'number'],
            [['protocol_date', 'pacient_id', 'department_id', 'doctor_id', 'protocol_id', 'status'], 'required'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'pacientFullName' => Yii::t('app', 'Pacient'),
            'department_id' => Yii::t('app', 'Departments'),
            'doctor_id' => Yii::t('app', 'Doctor'),
            'doctorFullName' => Yii::t('app', 'Doctor'),
            'protocol_id' => Yii::t('app', 'Protocol'),
            'protocol' => Yii::t('app', 'Protocol'),
            'refferal_id' => Yii::t('app', 'Refferal'),
            'refferal' => Yii::t('app', 'Refferal'),
            'category_id' => Yii::t('app', 'Category'),
            'protocol_date' => Yii::t('app', 'Protocol date'),
            'total' => Yii::t('app', 'Total'),
            'discount' => Yii::t('app', 'Discount'),
            'status' => Yii::t('app', 'Status'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'type' => Yii::t('app', 'Type'),
            'place' => Yii::t('app', 'Spot treatment'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'protocol_date_start' => Yii::t('app', 'Protocol date') .' '. Yii::t('app', 'From time'),
            'protocol_date_end' => Yii::t('app', 'Protocol date') .' '. Yii::t('app', 'To time'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;

                // Очистка старых данных в шаблонах протоколів
                if ($this->protocol_id != $this->oldAttributes['protocol_id'])
                {
                    $protocol = \backend\modules\handbooemc\models\Protocol::findOne($this->oldAttributes['protocol_id']);
                    if (isset($protocol->template->id) && $protocol->template->id)
                    {
                        $protocolId = ProtocolPattern::choicTemplateId($protocol->template->id, $protocol->template->handbook_emc_category_id);

                        $pattern = '\backend\modules\pattern\models\ProtocolPattern_' . $protocolId;
                        $pattern = $pattern::find()->where(['protocol_id' => $this->id])->one();

                        if ($pattern !== null)
                            $pattern->delete();
                    }
                }
            }

            if ($this->protocol_id)
            {
                $protocol = \backend\modules\handbokemc\models\Protocol::findOne($this->protocol_id);
                $this->category_id = $protocol->handbook_emc_category_id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

         if ($this->protocol_id == 103){
             $this->protocol103();      //Стандарт лабораторного обстеження хворих №1 (перед ургентною операцією)
         }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'doctor_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\modls\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    public function getProtocol()
    {
        return $this->hasOne(\backend\modules\handbookemc\models\Protocol::className(), ['id' => 'protocol_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(\backend\modules\handbokemc\models\Category::className(), ['id' => 'category_id']);
    }

    public function getRefferal()
    {
        return $this->hasOne(\backend\modules\handbook\models\Refferal::className(), ['id' => 'refferal_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnois\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public function getCountDay($status = null, $tests = null, $search = false)
    {
        $dateFrom = date('Y-m-d 00:00:00', strtotime($this->protocol_date));
        $dateTo = date('Y-m-d 23:59:59', strtotime($this->protocol_date));

        $protocol = Protocol::find()
                    ->where(['status' => $status])
                    ->andWhere(['>=', 'protocol_date', $dateFrom])
                    ->andWhere(['<=', 'protocol_date', $dateTo])
                    ->count();

        return $protocol;
    }

    public function getSumDay($status = null, $tests = null, $search = false)
    {
        $dateFrom = date('Y-m-d 00:00:00', strtotime($this->protocol_date));
        $dateTo = date('Y-m-d 23:59:59', strtotime($this->protocol_date));

        $protocol = Protocol::find()
            ->where(['status' => $status])
            ->andWhere(['>=', 'protocol_date', $dateFrom])
            ->andWhere(['<=', 'protocol_date', $dateTo])
            ->sum('total');

        if ($protocol > 0)
            return sprintf("%01.2f", $protocol);
        return sprintf("%01.2f", 0);
    }


    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'status' => [
                '1' => Yii::t('app', 'Plans'),
                '2' => Yii::t('app', 'Compleated'),
                '3' => Yii::t('app', 'Not defined'),
                '4' => Yii::t('app', 'Canceled'),
            ],
            'type' => [
                '1' => Yii::t('app', 'Preventive inspection'),
                '2' => Yii::t('app', 'Other'),
            ],
            'place' => [                    //Поліклініка, вдома, денний стаціонар, стаціонар вдома, стаціонар
                '1' => Yii::t('app', 'Polyclinic'),
                '2' => Yii::t('app', 'At home'),
                '3' => Yii::t('app', 'Day hospital'),
                '4' => Yii::t('app', 'At home as hospital'),
                '5' => Yii::t('app', 'Hospital'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    protected function protcol103()    // Стандарт лабораторного обстеження хворих №1 (перед ургентною операцією)
    {

        $this->protocol_id = 2530;  // визначення крові по АВО
        $this->save();

        $protocol = new Protocol();
        $protocol->attribtes = $this->attributes;
        $protocol->protocol_id = 2531;  // визначення резус-фактору
        $protocol->save();

        $protocol = new Protocol();
        $protocol->attributes = $this->attributes;
        $protocol->protocol_id = 2806; //клінічний аналіз крові №2
        $protocol->save();

        $protocol = new Protocol();
        $protocol->attributes = $this->attributes;
        $protocol->protocol_id = 2830;  //клінічний аналіз сечі №2
        $protocol->save();

    }
}
