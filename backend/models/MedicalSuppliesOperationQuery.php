<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[MedicalSuppliesOperation]].
 *
 * @see MedicalSuppliesOperation
 */
class MedicalSuppliesOperationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MedicalSuppliesOperation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MedicalSuppliesOperation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}