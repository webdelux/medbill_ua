<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbookemc\models\Icd;
use backend\modules\diagnosis\models\Diagnosis;
use backend\models\User;
use backend\modules\handbook\models\Department;
use backend\modules\handbookemc\models\Complication;
use backend\modules\handbookemc\models\Surgery;

/**
 * This is the model class for table "{{%pacient_operation}}".
 *
 * @property integer $id
 * @property integer $diagnosis_id
 * @property integer $handbook_emc_surgery_id
 * @property string $handbook_emc_surgery_note
 * @property integer $handbook_emc_icd_id
 * @property string $handbook_emc_icd_note
 * @property string $start_operation_date
 * @property string $start_operation_time
 * @property integer $duration_operation_hour
 * @property integer $duration_operation_minute
 * @property integer $surgeon_user_id
 * @property integer $department_id
 * @property integer $result_id
 * @property integer $anesthetist_user_id
 * @property integer $anesthesia_id
 * @property integer $handbook_emc_complication_id
 * @property string $handbook_emc_complication_note
 * @property integer $type_id
 * @property string $date_at
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Diagnosis $diagnosis
 * @property HandbookEmcSurgery $handbookEmcSurgery
 * @property HandbookEmcIcd $handbookEmcIcd
 * @property User $surgeonUser
 * @property Department $department
 * @property User $anesthetistUser
 * @property HandbookEmcComplication $handbookEmcComplication
 * @property User $user
 * @property User $updatedUser
 */
class PacientOperation extends ActiveRecord
{

    const SCENARIO_HISTORY = 'history';
    const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient_operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['diagnosis_id'], 'required'],
            [['diagnosis_id', 'handbook_emc_surgery_id', 'handbook_emc_icd_id', 'duration_operation_hour', 'duration_operation_minute', 'surgeon_user_id', 'department_id', 'result_id', 'anesthetist_user_id', 'anesthesia_id', 'handbook_emc_complication_id', 'type_id', 'user_id', 'updated_user_id'], 'integer'],
            [['start_operation_date', 'start_operation_time', 'date_at', 'created_at', 'updated_at'], 'safe'],
            [['handbook_emc_surgery_note', 'handbook_emc_icd_note', 'handbook_emc_complication_note'], 'string', 'max' => 255],

            [['diagnosis_id', 'handbook_emc_icd_id', 'handbook_emc_surgery_id', 'date_at', 'surgeon_user_id', 'anesthetist_user_id', 'type_id', 'department_id', 'anesthesia_id', 'result_id'], 'required', 'on' => self::SCENARIO_HISTORY],
            [['diagnosis_id', 'handbook_emc_icd_id', 'handbook_emc_surgery_id', 'date_at', 'surgeon_user_id', 'anesthetist_user_id', 'type_id', 'department_id', 'anesthesia_id', 'result_id'], 'required', 'on' => self::SCENARIO_UPDATE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'handbook_emc_surgery_id' => Yii::t('app', 'Operation'),
            'handbook_emc_surgery_note' => Yii::t('app', 'Additionally'),
            'handbook_emc_icd_id' => Yii::t('app', 'Diagnosis on operation'),
            'handbook_emc_icd_note' => Yii::t('app', 'Additional diagnosis'),
            'start_operation_date' => Yii::t('app', 'Beginning operation'),
            'start_operation_time' => Yii::t('app', 'Start time operation'),
            'duration_operation_hour' => Yii::t('app', 'Duration') . ', ' . Yii::t('app', 'hours.'),
            'duration_operation_minute' => Yii::t('app', 'Duration') . ', ' . Yii::t('app', 'minutes.'),
            'surgeon_user_id' => Yii::t('app', 'Surgeon'),
            'department_id' => Yii::t('app', 'Departments'),
            'result_id' => Yii::t('app', 'Result'),
            'anesthetist_user_id' => Yii::t('app', 'Anesthetist'),
            'anesthesia_id' => Yii::t('app', 'Anesthesia'),
            'handbook_emc_complication_id' => Yii::t('app', 'Complications'),
            'handbook_emc_complication_note' => Yii::t('app', 'Additional complications'),
            'type_id' => Yii::t('app', 'The type of operation'),
            'date_at' => Yii::t('app', 'Date At'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiagnosis()
    {
        return $this->hasOne(Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurgery()
    {
        return $this->hasOne(Surgery::className(), ['id' => 'handbook_emc_surgery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIcd()
    {
        return $this->hasOne(Icd::className(), ['id' => 'handbook_emc_icd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurgeonUser()
    {
        return $this->hasOne(User::className(), ['id' => 'surgeon_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnesthetistUser()
    {
        return $this->hasOne(User::className(), ['id' => 'anesthetist_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplication()
    {
        return $this->hasOne(Complication::className(), ['id' => 'handbook_emc_complication_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    /**
     * @inheritdoc
     * @return PacientOperationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientOperationQuery(get_called_class());
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'type_id' => [
                1 => Yii::t('app', 'Urgent'),
                2 => Yii::t('app', 'Planned'),
            ],
            'result_id' => [
                1 => Yii::t('app', 'Written(a) recovery'),
                2 => Yii::t('app', 'Written(a) improving'),
                3 => Yii::t('app', 'Written(a) deterioration'),
                4 => Yii::t('app', 'Written(a) unchanged'),
                5 => Yii::t('app', 'Died'),
            ],
            'anesthesia_id' => [
                1 => Yii::t('app', 'Endotracheal'),
                2 => Yii::t('app', 'Intravenous'),
                3 => Yii::t('app', 'Intramuscularly'),
                4 => Yii::t('app', 'Peridural'),
                5 => Yii::t('app', 'Laryngeal mask'),
                6 => Yii::t('app', 'Cerebrospinal'),
                7 => Yii::t('app', 'Inhalation'),
                8 => Yii::t('app', 'Nazoedotrahealna'),
                9 => Yii::t('app', 'The local'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}