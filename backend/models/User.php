<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

use dektrium\user\helpers\Password;

use backend\models\AuthAssignment;
use backend\modules\user\models\Users;
use backend\modules\user\models\Profile;
use backend\modules\user\models\ProfileSchedule;
use backend\modules\user\models\ProfileSpeciality;
use backend\modules\user\models\ProfileDepartment;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $phone
 * @property string $password_hash
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 *
 * @property Pacient[] $pacients
 * @property Profile $profile
 * @property ProfileDepartment[] $profileDepartments
 * @property ProfileSchedule[] $profileSchedules
 * @property ProfileSpeciality[] $profileSpecialities
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
class User extends \yii\db\ActiveRecord
{
    const ROLE_ADMIN = 'administrator';
    const ROLE_DOCTOR = 'doctor';
    const ROLE_MANAGER = 'manager';

    //Перелік ролей адміністративного (керівного) складу
    public static $managers = [self::ROLE_ADMIN, self::ROLE_MANAGER];

    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /** @inheritdoc */
    public function scenarios()
    {
        return ArrayHelper::merge((new Users)->scenarios(), [
            'default' => ['username', 'email', 'phone', 'password'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge((new Users)->rules(), [
            [['username', 'email', 'phone'], 'required'],
            [['password'], 'required', 'on' => ['create']],
            [['password'], 'string', 'min' => 6],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge((new Users)->attributeLabels(), [
            'id' => Yii::t('app', 'ID'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignment()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacients()
    {
        return $this->hasMany(Pacient::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileDepartments()
    {
        return $this->hasMany(ProfileDepartment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileSchedules()
    {
        return $this->hasMany(ProfileSchedule::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileSpecialities()
    {
        return $this->hasMany(ProfileSpeciality::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                // ...
            }

            if (!empty($this->password))
            {
                $this->setAttribute('password_hash', Password::hash($this->password));
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            (new Users())->relationsDeleteAll($this->id);

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Повертає масив користувачів з вказаних відділень
     */
    public static function getListByDepartments($ids = [])
    {
        $user = Profile::find()
            ->innerJoin('{{%profile_department}} AS profile_department', '{{%profile}}.user_id = profile_department.user_id')
            ->andWhere(['IN', 'profile_department.department_id', $ids])
            ->asArray()
            ->all();

        return ArrayHelper::map($user, 'user_id', 'name');
    }

}