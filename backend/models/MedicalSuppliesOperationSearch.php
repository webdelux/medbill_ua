<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MedicalSuppliesOperation;

/**
 * MedicalSuppliesOperationSearch represents the model behind the search form about `backend\models\MedicalSuppliesOperation`.
 */
class MedicalSuppliesOperationSearch extends MedicalSuppliesOperation
{
    public $fullName;
    public $user;
    public $updatedUser;
    public $supplies;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'department_id', 'medical_supplies_id', 'quantity', 'user_id', 'updated_user_id', 'diagnosis_id'], 'integer'],
            [['supplies', 'updatedUser', 'user', 'fullName', 'docum_date', 'docum_num', 'docum', 'description', 'created_at', 'updated_at'], 'safe'],
            [['docum_date', 'created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
            [['total', 'discount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MedicalSuppliesOperation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'pacient_id',
                'fullName' => [
                   'asc' => ['pacient_id' => SORT_ASC],
                   'desc' => ['pacient_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'supplies' => [
                   'asc' => ['medical_supplies_id' => SORT_ASC],
                   'desc' => ['medical_supplies_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'department_id',
                'medical_supplies_id',
                'quantity',
                'docum_date',
                'docum_num',
                'docum',
                'user_id',
                'updated_user_id',
                'user' => [
                    'asc' => ['user_id' => SORT_ASC],
                    'desc' => ['user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'updatedUser' => [
                    'asc' => ['updated_user_id' => SORT_ASC],
                    'desc' => ['updated_user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'description',
                'created_at',
                'updated_at',
                'diagnosis_id',
                'total',
                'discount',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'department_id' => $this->department_id,
            'medical_supplies_id' => $this->medical_supplies_id,
            'quantity' => $this->quantity,
            //'docum_date' => $this->docum_date,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
            'diagnosis_id' => $this->diagnosis_id,
            'total' => $this->total,
            'discount' => $this->discount,
        ]);

        $query->andFilterWhere(['like', 'docum_num', $this->docum_num])
            ->andFilterWhere(['like', 'docum', $this->docum])
            ->andFilterWhere(['like', 'docum_date', $this->docum_date])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'description', $this->description]);


        return $dataProvider;
    }
}
