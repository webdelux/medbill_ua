<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Pacient]].
 *
 * @see Pacient
 */
class PacientQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Pacient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Pacient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}