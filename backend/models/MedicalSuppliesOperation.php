<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Pacient;
use backend\modules\handbook\models\MedicalSupplies;
use backend\models\Category;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%medical_supplies_operation}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $department_id
 * @property integer $medical_supplies_id
 * @property integer $quantity   // кількість (+ надходження,  - витрата)
 * @property string $docum_date
 * @property string $docum_num
 * @property string $docum
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $diagnosis_id
 * @property string $total
 * @property string $discount
 */
class MedicalSuppliesOperation extends \yii\db\ActiveRecord
{
    public $fullName;
    public $msupplies;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%medical_supplies_operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'department_id', 'medical_supplies_id', 'quantity', 'user_id', 'updated_user_id', 'diagnosis_id'], 'integer'],
            [['docum_date', 'created_at', 'updated_at'], 'safe'],
            [['msupplies', 'department_id', 'medical_supplies_id', 'quantity'], 'required'],
            [['docum_num', 'docum', 'description'], 'string', 'max' => 255],
            ['docum_date', 'date', 'format' => 'yyyy-M-d'],
            [['total', 'discount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'fullName' => Yii::t('app', 'Pacient'),
            'department_id' => Yii::t('app', 'Departments'),
            'medical_supplies_id' => Yii::t('app', 'Medical Supplies'),
            'msupplies' => Yii::t('app', 'Medical Supplies'),
            'quantity' => Yii::t('app', 'Quantity'),
            'docum_date' => Yii::t('app', 'Docum date'),
            'docum_num' => Yii::t('app', 'Docum Number'),
            'docum' => Yii::t('app', 'Docum Type'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'total' => Yii::t('app', 'Total'),
            'discount' => Yii::t('app', 'Discount'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            $this->total = (abs($this->quantity) * $this->medicalSupplies->price) - $this->discount;    //обчислюємо суму - знижка

            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getMedicalSupplies()
    {
        return $this->hasOne(MedicalSupplies::className(), ['id' => 'medical_supplies_id']);
    }

    public function getMedicalSuppliesMeasurement()
    {
         return (isset($this->medicalSupplies->measurement->name)) ? ($this->medicalSupplies->name . ' ('
                 . $this->medicalSupplies->measurement->name . ')') : $this->medicalSupplies->name;
    }

    public function getDepartment()
    {
        return $this->hasOne(\backend\modules\handbook\models\Department::className(), ['id' => 'department_id']);
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }


    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    public function getType()
    {
        return ($this->quantity > 0) ? 1 : 0;  //Якщо кількість більше 0, то надходження, інакше - витрата.
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    /**
     * @inheritdoc
     * @return MedicalSuppliesOperationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MedicalSuppliesOperationQuery(get_called_class());
    }
}
