<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbook\models\City;

/**
 * This is the model class for table "{{%pacient}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $lastname
 * @property string $birthday
 * @property string $ipn
 * @property string $phone
 * @property string $phone1
 * @property integer $city_id
 * @property string $address
 * @property integer $gender
 * @property string $work
 * @property string $seat
 * @property integer $dispensary_group
 * @property string $contingent
 * @property string $docum_type
 * @property string $docum_number
 * @property string $memo
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status_id
 * @property string $card_amb
 * @property string $card_stac
 * @property integer $city_type
 * @property integer $citizenship_id
 *

 * @property User $user
 */
class Pacient extends \yii\db\ActiveRecord
{

    /**
     * Регулярний вираз для Прізвища Ім'я та По-батькові
     */
    public static $pibRegexp = "/^[-а-яА-ЯіІїЇєЄ'’\s]+$/ui";

    /**
     * Регулярний вираз для телефонного номеру +99 (999) 999-99-99
     */
    //public static $phoneRegex = '/^(\+\s*[0-9][0-9]*\s*(\([0-9]*\)\s*-*|-[0-9]*))?[0]?[1-9][0-9\- ]*$/';

    public $cityName;
    public $citizenship;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'fieldsRequired' => [['name', 'surname', 'lastname', 'phone', 'city_id', 'address', 'gender', 'dispensary_group', 'status_id'], 'required'],
            'fieldsSafe' => [['birthday', 'user_id', 'updated_user_id', 'created_at', 'updated_at', 'contingent', 'cityName'], 'safe'],
            'pibMatch' => [['name', 'surname', 'lastname'], 'match', 'pattern' => static::$pibRegexp],
            'phoneMatch' => [['phone', 'phone1'], 'match', 'pattern' => \backend\modules\user\models\Users::$phoneRegexp],
            'fieldaInteger' => [['city_type', 'city_id', 'gender', 'dispensary_group', 'user_id', 'status_id', 'citizenship_id'], 'integer'],
            'fieldsString' => [['card_stac', 'card_amb', 'phone', 'phone1', 'memo'], 'string'],
            //'fieldsDate' => [['birthday'], 'date', 'format' => 'yyyy-M-d'],
            'city' => [['cityName'], 'required', 'on' => ['create', 'update']],
            'pibLength' => [['name', 'surname', 'lastname'], 'string', 'min' => 2, 'max' => 30],
            'phoneLength' => [['phone', 'phone1'], 'string', 'max' => 20],
            'docLength' => [['ipn', 'docum_number'], 'string', 'max' => 20],
            'docTypeLength' => [['docum_type'], 'string', 'max' => 100],
            'addressLength' => [['address', 'work', 'seat'], 'string', 'min' => 4, 'max' => 150],
            'ipnUnique' => [['ipn'], 'unique', 'message' => Yii::t('app', 'This ipn has already been taken')],
            'phoneUnique' => [['phone'], 'unique', 'message' => Yii::t('user', 'This phone has already been taken')],
            'fieldsTrim' => [['name', 'surname', 'lastname', 'phone', 'phone1', 'city_id', 'address', 'gender', 'dispensary_group'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'First name'),
            'surname' => Yii::t('app', 'Surname'),
            'lastname' => Yii::t('app', 'Last name'),
            'birthday' => Yii::t('app', 'Birthday'),
            'ipn' => Yii::t('app', 'IPN'),
            'phone' => Yii::t('app', 'Phone number'),
            'phone1' => Yii::t('app', 'Additional phone number'),
            'cityName' => Yii::t('app', 'City'),
            'city_id' => Yii::t('app', 'City'),
            'address' => Yii::t('app', 'Address'),
            'gender' => Yii::t('app', 'Gender'),
            'work' => Yii::t('app', 'Work place'),
            'seat' => Yii::t('app', 'Work Seat'),
            'dispensary_group' => Yii::t('app', 'Dispensary group'),
            'contingent' => Yii::t('app', 'Contingent'),
            'docum_type' => Yii::t('app', 'Docum Type'),
            'docum_number' => Yii::t('app', 'Docum Number'),
            'memo' => Yii::t('app', 'Memo'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status_id' => Yii::t('app', 'Status'),
            'fullName' => Yii::t('app', 'Full Name'),
            'username' => Yii::t('app', 'Username'),
            'city_type' => Yii::t('app', 'Type'),
            'card_stac' => Yii::t('app', 'Stationary card #'),
            'card_amb' => Yii::t('app', 'Ambulatory card #'),
            'citizenship_id' => Yii::t('app', 'Citizenship'),
            'citizenship' => Yii::t('app', 'Citizenship'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            'normalDate' => [
                'class' => \backend\behaviors\NormalDateBehavior::className(),
                'date_in' => ['birthday'],
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profle::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /**
     * Гетер для Користувача
     */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getStatus()
    {
        return $this->hasOne(\backend\modules\handbok\models\Status::className(), ['id' => 'status_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getCountry()
    {
        return $this->hasOne(\backend\modules\handbok\models\Country::className(), ['id' => 'citizenship_id']);
    }

    /**
     * @inheritdoc
     * @return PacientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientQuery(get_called_class());
    }

    /**
     * @inheritdoc
     * @return Вік пацієнта на поточний момент.
     */
    public function getAge()
    {
        return intval((strtotime(date('d-m-Y')) - strtotime($this->birhday))/(60*60*24*365)); //Кількість років
    }

    /**
     * Геттер для полного имени человека
     */
    public function getFulName()
    {
        return $this->lastname . ' ' . $this->name . ' ' . $this->surname;
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'gender' => [
                '1' => Yii::t('app', 'Man'),
                '2' => Yii::t('app', 'Woman'),
            ],
            'docum' => [
                '1' => Yii::t('app', 'Passport'),
                '2' => Yii::t('app', 'Drive license'),
                '3' => Yii::t('app', 'Pension certificate'),
                '4' => Yii::t('app', 'Other'),
            ],
            'contingent' => [
                '1' => Yii::t('contingent', 'Contingent1'),
                '2' => Yii::t('contingent', 'Contingent2'),
                '3' => Yii::t('contingent', 'Contingent3'),
                '4' => Yii::t('contingent', 'Contingent4'),
                '5' => Yii::t('contingent', 'Contingent5'),
                '6' => Yii::t('contingent', 'Contingent6'),
                '7' => Yii::t('contingent', 'Contingent7'),
                '8' => Yii::t('contingent', 'Contingent8'),
                '9' => Yii::t('contingent', 'Contingent9'),
            ],
            'yn' => [
                '1' => Yii::t('app', 'No'),
                '2' => Yii::t('app', 'Yes'),
            ],
            'city_type' => [
                '1' => Yii::t('app', 'City'),
                '2' => Yii::t('app', 'Village'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}