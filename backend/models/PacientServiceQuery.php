<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[PacientService]].
 *
 * @see PacientService
 */
class PacientServiceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PacientService[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientService|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}