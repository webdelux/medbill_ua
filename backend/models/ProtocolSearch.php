<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * ProtocolSearch represents the model behind the search form about `backend\models\Protocol`.
 */
class ProtocolSearch extends Protocol
{
    public $fullName;
    public $doctor;
    public $user;
    public $updatedUser;
    public $protocol;
    public $refferal;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'diagnosis_id', 'pacient_id', 'department_id', 'doctor_id', 'protocol_id', 'refferal_id', 'category_id', 'status', 'user_id', 'updated_user_id', 'type', 'place'], 'integer'],
            [['protocol_date_end', 'protocol_date_start','user', 'refferal', 'doctor', 'fullName', 'protocol', 'protocol_date', 'description', 'created_at', 'updated_at'], 'safe'],
            [['total', 'discount'], 'number'],
            [['protocol_date', 'created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Protocol::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
        * Настройка параметров сортировки
        * Важно: должна быть выполнена раньше $this->load($params)
        */
        $dataProvider->setSort([
            'defaultOrder' => ['protocol_date' => 'DESC'],
            'attributes' => [
                'id',
                'diagnosis_id',
                'fullName' => [
                    'asc' => ['pacient_id' => SORT_ASC],
                    'desc' => ['pacient_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'pacient_id',
                'department_id',
                'category_id',
                'doctor' => [
                    'asc' => ['doctor_id' => SORT_ASC],
                    'desc' => ['doctor_id' => SORT_DESC],
                    //'default' => SORT_ASC
                ],
                'protocol' => [
                    'asc' => ['protocol_id' => SORT_ASC],
                    'desc' => ['protocol_id' => SORT_DESC],
                //    'default' => SORT_ASC
                ],
                'refferal' => [
                    'asc' => ['refferal_id' => SORT_ASC],
                    'desc' => ['refferal_id' => SORT_DESC],
                  //  'default' => SORT_ASC
                ],
                'protocol_date' => [
                    'asc' => ['protocol_date' => SORT_ASC],
                    'desc' => ['protocol_date' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'refferal_id',
                'total',
                'discount',
                'status',
                'user' => [
                    'asc' => ['user_id' => SORT_ASC],
                    'desc' => ['user_id' => SORT_DESC],
                    //'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['updated_user_id' => SORT_ASC],
                   'desc' => ['updated_user_id' => SORT_DESC],
                  // 'default' => SORT_ASC
                ],
                'created_at',
                'updated_at',
                'type',
                'place',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'diagnosis_id' => $this->diagnosis_id,
            'pacient_id' => $this->pacient_id,
            'department_id' => $this->department_id,
            'doctor_id' => $this->doctor_id,
            'protocol_id' => $this->protocol_id,
            'refferal_id' => $this->refferal_id,
            'category_id' => $this->category_id,
            'total' => $this->total,
            'discount' => $this->discount,
            'status' => $this->status,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'type' => $this->type,
            'place' => $this->place,
        ]);

        if (strlen($this->protocol_date)>0)
        {
            $dateFrom = date('Y-m-d 00:00:00', strtotime($this->protocol_date));
            $dateTo = date('Y-m-d 23:59:59', strtotime($this->protocol_date));
        }
        elseif (strlen($this->protocol_date_start) > 0 & strlen($this->protocol_date_end) > 0)
        {
            $dateFrom = date('Y-m-d 00:00:00', strtotime($this->protocol_date_start));
            $dateTo = date('Y-m-d 23:59:59', strtotime($this->protocol_date_end));
        }
        else
        {
            $dateFrom = $this->protocol_date;
            $dateTo = $this->protocol_date;
        }

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['>=', 'protocol_date', $dateFrom])
            ->andFilterWhere(['<=', 'protocol_date', $dateTo])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
