<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbook\models\Service;
use yii\helpers\ArrayHelper;
use backend\models\Category;

/**
 * This is the model class for table "{{%pacient_service}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $service_id
 * @property string $total
 * @property string $discount
 * @property string $date_service
 * @property string $docum
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $diagnosis_id
 *
 */
class PacientService extends \yii\db\ActiveRecord
{
    public $pacientFullName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'service_id', 'user_id', 'updated_user_id', 'diagnosis_id'], 'integer'],
            [['total', 'discount'], 'number'],
            [['pacient_id', 'service_id', 'pacientFullName'], 'required'],
            [['date_service', 'created_at', 'updated_at'], 'safe'],
            [['docum', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'pacientFullName' => Yii::t('app', 'Pacient'),
            'service_id' => Yii::t('app', 'Service'),
            'total' => Yii::t('app', 'Total'),
            'discount' => Yii::t('app', 'Discount'),
            'docum' => Yii::t('app', 'Docum Type'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'date_service' => Yii::t('app', 'Date'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }


    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }


    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    /**
     * @inheritdoc
     * @return PacientServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientServiceQuery(get_called_class());
    }

    //Формує список всіх послуг по категоріях (категорія[послуги])
    public function serviceList($options = null)
    {
        $list = Service::find()->all();
        $out = [];
        foreach ($list as $value)
        {
            $out[] = [
                'id' => $value->id,
                'category' => Category::findOne(['id' => $value->category_id])->name,
                'name' => $value->name,
                'price' => $value->price,
            ];
        }
        if ($options == 1)
        {
            return ArrayHelper::map($out, 'id', 'price');
        } else {
            return ArrayHelper::map($out, 'id', 'name',  'category');
        }

    }

}
