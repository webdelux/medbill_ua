<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PacientOperation;

/**
 * PacientOperationSearch represents the model behind the search form about `backend\models\PacientOperation`.
 */
class PacientOperationSearch extends PacientOperation
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'diagnosis_id', 'handbook_emc_surgery_id', 'handbook_emc_icd_id', 'duration_operation_hour', 'duration_operation_minute', 'surgeon_user_id', 'department_id', 'result_id', 'anesthetist_user_id', 'anesthesia_id', 'handbook_emc_complication_id', 'type_id', 'user_id', 'updated_user_id'], 'integer'],
            [['handbook_emc_surgery_note', 'handbook_emc_icd_note', 'start_operation_date', 'start_operation_time', 'handbook_emc_complication_note', 'date_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PacientOperation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'diagnosis_id' => $this->diagnosis_id,
            'handbook_emc_surgery_id' => $this->handbook_emc_surgery_id,
            'handbook_emc_icd_id' => $this->handbook_emc_icd_id,
            'start_operation_date' => $this->start_operation_date,
            'start_operation_time' => $this->start_operation_time,
            'duration_operation_hour' => $this->duration_operation_hour,
            'duration_operation_minute' => $this->duration_operation_minute,
            'surgeon_user_id' => $this->surgeon_user_id,
            'department_id' => $this->department_id,
            'result_id' => $this->result_id,
            'anesthetist_user_id' => $this->anesthetist_user_id,
            'anesthesia_id' => $this->anesthesia_id,
            'handbook_emc_complication_id' => $this->handbook_emc_complication_id,
            'type_id' => $this->type_id,
            'date_at' => $this->date_at,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'handbook_emc_surgery_note', $this->handbook_emc_surgery_note])
                ->andFilterWhere(['like', 'handbook_emc_icd_note', $this->handbook_emc_icd_note])
                ->andFilterWhere(['like', 'handbook_emc_complication_note', $this->handbook_emc_complication_note]);

        return $dataProvider;
    }

}