<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Payment;

/**
 * PaymentSearch represents the model behind the search form about `backend\models\Payment`.
 */
class PaymentSearch extends Payment
{
    public $fullName;
    public $user;
    public $updatedUser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'user_id', 'updated_user_id', 'payment_method_id', 'type'], 'integer'],
            [['total', 'discount'], 'number'],
            [['updatedUser','user', 'fullName','description', 'created_at', 'updated_at'], 'safe'],
            [['date_payment', 'created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
           'attributes' => [
               'id',
                'fullName' => [
                   'asc' => ['pacient_id' => SORT_ASC],
                   'desc' => ['pacient_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
               'total',
               'discount',
               'type',
               'description',
               'user' => [
                   'asc' => ['user_id' => SORT_ASC],
                   'desc' => ['user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
               'updatedUser' => [
                   'asc' => ['updated_user_id' => SORT_ASC],
                   'desc' => ['updated_user_id' => SORT_DESC],
                   'default' => SORT_ASC
               ],
               'created_at',
               'updated_at',
               'payment_method_id',
               'date_payment',
           ]
       ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%payment}}.id' => $this->id,
            '{{%payment}}.pacient_id' => $this->pacient_id,
            '{{%payment}}.total' => $this->total,
            '{{%payment}}.discount' => $this->discount,
            '{{%payment}}.type' => $this->type,
            '{{%payment}}.user_id' => $this->user_id,
            '{{%payment}}.updated_user_id' => $this->updated_user_id,
            //'{{%payment}}.created_at' => $this->created_at,
            //'{{%payment}}.updated_at' => $this->updated_at,
            '{{%payment}}.payment_method_id' => $this->payment_method_id,
        ]);

        $query->andFilterWhere(['like', '{{%payment}}.description', $this->description])
                ->andFilterWhere(['like', '{{%payment}}.created_at', $this->created_at])
                ->andFilterWhere(['like', '{{%payment}}.updated_at', $this->updated_at])
                ->andFilterWhere(['like', '{{%payment}}.date_payment', $this->date_payment]);

        return $dataProvider;
    }
}
