<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Stacionary]].
 *
 * @see Stacionary
 */
class StacionaryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Stacionary[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Stacionary|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}