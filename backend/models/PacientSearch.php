<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Pacient;

/**
 * PacientSearch represents the model behind the search form about `backend\models\Pacient`.
 */
class PacientSearch extends Pacient
{
    public $fullName;
    public $user;
    public $updatedUser;
    public $cityName;
    public $citizenship;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'dispensary_group', 'status_id', 'user_id', 'updated_user_id', 'city_type', 'citizenship_id'], 'integer'],
            [[
                'phone', 'phone1', 'cityName', 'updatedUser', 'user', 'fullName', 'name', 'surname', 'lastname',
                'gender' ,'ipn', 'address', 'work', 'seat', 'docum_type', 'docum_number', 'contingent',
                'memo', 'created_at', 'updated_at', 'card_amb', 'card_stac', 'citizenship', 'birthday',
            ], 'safe'],
            [['created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pacient::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
        * Настройка параметров сортировки
        * Важно: должна быть выполнена раньше $this->load($params)
        */
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'fullName' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'contingent',
                'user' => [
                    'asc' => ['user_id' => SORT_ASC],
                    'desc' => ['user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['updated_user_id' => SORT_ASC],
                   'desc' => ['updated_user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'birthday',
                'phone',
                'cityName' => [
                    'asc' => ['city_id' => SORT_ASC],
                    'desc' => ['city_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'address',
                'gender',
                'citizenship' => [
                    'asc' => ['citizenship_id' => SORT_ASC],
                    'desc' => ['citizenship_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'citizenship_id',
                'city_type',
                'card_amb',
                'card_stac',
                'dispensary_group',
                'dispensaryGroup' => [
                    'asc' => ['dispensary_group' => SORT_ASC],
                    'desc' => ['dispensary_group' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'created_at',
                'updated_at',
                'status_id',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%pacient}}.id' => $this->id,
            //'{{%pacient}}.birthday' => $this->birthday,
            //'{{%pacient}}.phone' => $this->phone,
            //'{{%pacient}}.phone1' => $this->phone1,
            'city_id' => $this->city_id,
            'citizenship_id' => $this->citizenship_id,
            'city_type' => $this->city_type,
            '{{%pacient}}.gender' => $this->gender,
            //'dispensary_group' => $this->dispensary_group,
            //'contingent' => $this->contingent,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            '{{%pacient}}.status_id' => $this->status_id,
        ]);

        $query->andFilterWhere(['like', '{{%pacient}}.ipn', $this->ipn])
            ->andFilterWhere(['like', '{{%pacient}}.name', $this->name])
                ->andFilterWhere(['like', '{{%pacient}}.surname', $this->surname])
                ->andFilterWhere(['like', '{{%pacient}}.lastname', $this->lastname])
            ->andFilterWhere(['like', '{{%pacient}}.birthday', $this->birthday])
            ->andFilterWhere(['like', '{{%pacient}}.address', $this->address])
            ->andFilterWhere(['like', '{{%pacient}}.phone', $this->phone])
            ->andFilterWhere(['like', '{{%pacient}}.phone1', $this->phone1])
            ->andFilterWhere(['like', '{{%pacient}}.work', $this->work])
            ->andFilterWhere(['like', '{{%pacient}}.seat', $this->seat])
            ->andFilterWhere(['like', '{{%pacient}}.card_amb', $this->card_amb])
            ->andFilterWhere(['like', '{{%pacient}}.card_stac', $this->card_stac])
            //->andFilterWhere(['like', '{{%pacient}}.docum_type', $this->docum_type])
            ->andFilterWhere(['like', '{{%pacient}}.docum_number', $this->docum_number])
            ->andFilterWhere(['like', '{{%pacient}}.contingent', $this->contingent])
            ->andFilterWhere(['like', '{{%pacient}}.created_at', $this->created_at])
            ->andFilterWhere(['like', '{{%pacient}}.updated_at', $this->updated_at])
            ->andFilterWhere(['like', '{{%pacient}}.memo', $this->memo]);


        return $dataProvider;
    }
}
