<?php

namespace backend\modules\handbook\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class DefaultController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}