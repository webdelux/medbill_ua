<?php

namespace backend\modules\handbook\controllers;

use Yii;
use backend\modules\handbook\models\HandbookStacionary;
use backend\modules\handbook\models\HandbookStacionarySearch;
use \backend\modules\handbook\models\HandbookRoom;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * HandbookStacionaryController implements the CRUD actions for HandbookStacionary model.
 */
class HandbookStacionaryController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionRooms($department = null, $room = null,  $pacient = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;


        if (isset($department))             // вибираємо всі палати для зазначеного відділення
        {
            $query = HandbookStacionary::find()->joinWith(['room']);

            if ($pacient)
                $query->orFilterWhere(['=', '{{%handbook_stacionary}}.pacient_id', $pacient]);

            $places = $query
                    ->where(['{{%handbook_room}}.department_id' => $department])
                    ->andWhere(['=', '{{%handbook_stacionary}}.pacient_id', 0])
                    ->orderBy('{{%handbook_room}}.room ASC')
                    ->all();

            $out = [];
            foreach ($places as $value)
            {
                $out[$value->room_id] = $value->room->room;
            }

            $res = [];          //конвертуємо масив для відображення
            foreach ($out as $key => $value)
            {
                $res[] = [
                    'id' => $key,
                    'value' => $value
                ];
            }

            return $res;
        }

        if (isset($room))               //вибираємо всі ліжка для зазначеної палати
        {
            $query = HandbookStacionary::find();

            if ($pacient)
                $query->orFilterWhere(['=', 'pacient_id', $pacient]);

            $places = $query
                    ->where(['room_id' => $room])
                    ->andWhere(['pacient_id' => 0])
                    ->orderBy('place ASC')
                    ->all();

            $out = [];
            foreach ($places as $value)
            {
                $out[] = [
                    'id' => $value->id,
                    'value' => $value->place
                ];
            }

            return $out;
        }
    }

    /**
     * Lists all HandbookStacionary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HandbookStacionarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HandbookStacionary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HandbookStacionary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HandbookStacionary();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing HandbookStacionary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing HandbookStacionary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HandbookStacionary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HandbookStacionary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HandbookStacionary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
