<?php

namespace backend\modules\handbook\controllers;

use Yii;
use backend\modules\handbook\models\Service;
use backend\modules\handbook\models\ServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use backend\models\Category;
use yii\web\Response;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends Controller
{
    private $categoryId = 2; // ІД батьківської категорії  "Послуги".

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $category = Yii::$app->request->get('category');

        if ($category)
        {
            $searchModel = new \backend\models\CategorySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->categoryId);

            return $this->render('//category/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'categoryId' => $this->categoryId
            ]);
        }
        else
        {
            if (!Yii::$app->request->isAjax)
            {
                $searchModel = new ServiceSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'categoryId' => $this->categoryId
                ]);
            }
            else
            {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $term = Yii::$app->request->get('term', null);

                $out = [];

                if (!is_null($term))
                {
                    $data = Service::find()->where(['LIKE', 'name', trim($term)])->limit(10)->all();

                    $out = [
                        'results' => []
                    ];

                    foreach ($data as $value)
                    {
                        $out['results'][] = [
                            'id' => $value->id,
                            'text' => $value->name
                        ];
                    }
                }

                return $out;
            }
        }
    }

    /**
     * Displays a single Service model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->get('category'))
        {
            return $this->render('//category/view', [
            'model' => $this->findModel($id),
            ]);
        }
        else
        {
            return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->request->get('category'))
        {
            $model = new Category();

            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                if (!$model->parent_id)
                {
                    // Якщо створення кореневий вузол, то додаємо до корня 1 ("Медикаменти")
                    $root = Category::findOne(['id' => $this->categoryId]);
                    if ($root)
                    {
                        // Inserting new node
                        if ($model->appendTo($root)->save())
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                    }

                }
                else
                {
                    // Перевіряємо чи є кореневий вузол
                    $root = Category::findOne(['id' => $model->parent_id]);
                    if ($root)
                    {
                        // Inserting new node
                        if ($model->appendTo($root)->save())
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                    }
                }

                return $this->refresh();
            }
            else
            {
                return $this->render('//category/create', [
                    'model' => $model,
                    'categoryId' => $this->categoryId
                ]);
            }
        }
        else
        {
            $model = new Service();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'categoryId' => $this->categoryId
                ]);
            }
        }
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->get('category'))
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                // Перевіряємо щоб ієрархії небула "сам на себе"
                if ($model->id != $model->parent_id)
                {
                    // Перевіряємо чи є кореневий вузол
                    $root = Category::findOne(['id' => $model->parent_id]);
                    if ($root)
                    {
                        // Move existing node
                        if ($model->appendTo($root)->save())
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                        else
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Failed to save entered data!'));
                    }
                    else
                    {
                        Yii::$app->session->setFlash('success', Yii::t('app', "You can't change parent category!"));
                    }
                }
                return $this->refresh();
            }
            else
            {
                return $this->render('//category/update', [
                    'model' => $model,
                    'categoryId' => $this->categoryId
                ]);
            }
        }
        else
        {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
            {
                return $this->render('update', [
                    'model' => $model,
                    'categoryId' => $this->categoryId
                ]);
            }
        }
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->get('category'))
        {
            if ($model->parent_id) // Перевіряємо щоб не була корнева категорія
            {
                $model->deleteWithChildren(); // Delete node and all descendants
            }
            else
            {
                Yii::$app->session->setFlash('success', Yii::t('app', "You can't delete parent category!"));
            }
            return $this->redirect(['index', 'category'=>TRUE]);
        }
        else
        {
            $this->findModel($id)->delete();
            return $this->redirect(['index', 'category'=>FALSE]);
        }
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->request->get('category'))
        {
            if (($model = \backend\models\Category::findOne($id)) !== null) {
                return $model;
            }
            else
            {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        else
        {
            if (($model = Service::findOne($id)) !== null) {
                return $model;
            }
            else
            {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
}
