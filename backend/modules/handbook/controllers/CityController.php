<?php

namespace backend\modules\handbook\controllers;

use Yii;
use backend\modules\handbook\models\City;
use backend\modules\handbook\models\CitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isAjax)
        {
            $searchModel = new CitySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else
        {
            $term = Yii::$app->request->get('term');
            if ($term)
            {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $city = City::find()
                        ->where(['LIKE', 'title', trim($term)])
                        ->limit(10)
                        ->all();

                $out = [];
                foreach ($city as $value)
                {
                    $out[] = [
                        'id' => $value->id,
                        'label' => $value->address,
                        'value' => $value->title
                    ];
                }

                return $out;
            }
            else
                throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single City model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new City model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new City();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing City model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}