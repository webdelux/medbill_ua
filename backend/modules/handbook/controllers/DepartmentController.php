<?php

namespace backend\modules\handbook\controllers;

use Yii;
use backend\modules\handbook\models\Department;
use backend\modules\handbook\models\DepartmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DepartmentController implements the CRUD actions for Department model.
 */
class DepartmentController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Department models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isAjax)
        {
            $searchModel = new DepartmentSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $department = new Department();
            $tree = $department->treeData();

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'tree' => $tree
            ]);
        }
        else
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $term = Yii::$app->request->get('term', null);

            $out = [];

            if (!is_null($term))
            {
                $data = Department::find()->where(['LIKE', 'name', trim($term)])->limit(10)->all();

                $out = [
                    'results' => []
                ];

                foreach ($data as $value)
                {
                    $out['results'][] = [
                        'id' => $value->id,
                        'text' => $value->name
                    ];
                }
            }

            return $out;
        }
    }

    /**
     * Displays a single Department model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Department model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Department();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if (is_array($model->working_days))
                $model->working_days = implode(',', $model->working_days);

            if (!$model->parent_id)
            {
                // Створення кореневий вузол
                $department = new Department($model->attributes);
                if ($department->makeRoot())
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
            }
            else
            {
                // Перевіряємо чи є кореневий вузол
                $root = Department::findOne(['id' => $model->parent_id]);
                if ($root)
                {
                    $department = new Department($model->attributes);
                    if ($department->appendTo($root))
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                }
            }

            return $this->refresh();
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Department model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $department = Department::findOne(['id' => $model->id]);

        $model->parent_id = $department->parents(1)->one();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if (is_array($model->working_days))
                $model->working_days = implode(',', $model->working_days);

            if ($model->id != $model->parent_id)
                $department->appendTo(Department::findOne(['id' => $model->parent_id]));

            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            $model->working_days = explode(',', $model->working_days);

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Department model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        $model = Department::findOne(['id' => $id]);
        $model->deleteWithChildren();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Department model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Department the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Department::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}