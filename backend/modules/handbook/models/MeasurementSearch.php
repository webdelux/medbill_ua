<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\Measurement;

/**
 * MeasurementSearch represents the model behind the search form about `backend\modules\handbook\models\Measurement`.
 */
class MeasurementSearch extends Measurement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'sort', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['name','abbreviation', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $model = new Measurement();
        $treelist = $model->treeListParent($model->treeData());

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $treelist,
            'pagination' => false,
        ]);

        //$query = Measurement::find();

        //$dataProvider = new ActiveDataProvider([
        //    'query' => $query,
        //    'pagination' => false,
        //]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'id' => $this->id,
//            'parent_id' => $this->parent_id,
//            'sort' => $this->sort,
//            'user_id' => $this->user_id,
//            'updated_user_id' => $this->updated_user_id,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//            'status_id' => $this->status_id,
//        ]);
//
//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'abbreviation', $this->abbreviation])
//            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
