<?php

namespace backend\modules\handbook\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;

/**
 * This is the ActiveQuery class for [[DepartmentCategory]].
 *
 * @see DepartmentCategory
 */
class DepartmentCategoryQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;
    
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DepartmentCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DepartmentCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
