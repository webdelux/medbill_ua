<?php

namespace backend\modules\handbook\models;

/**
 * This is the ActiveQuery class for [[Refferal]].
 *
 * @see Refferal
 */
class RefferalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Refferal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Refferal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}