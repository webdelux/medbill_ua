<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Category;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%medical_supplies}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property integer $measurement_id
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status_id
 * @property string $price
 */
class MedicalSupplies extends \yii\db\ActiveRecord
{
    public $measurementName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%medical_supplies}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'measurement_id', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['name', 'measurement_id'], 'required'],
            [['created_at', 'updated_at', 'user_id', 'measurement_id', 'measurementName'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category'),
            'name' => Yii::t('app', 'Name'),
            'measurement_id' => Yii::t('app', 'Measurement'),
            'measurementName' => Yii::t('app', 'Measurement'),
            'measurement' => Yii::t('app', 'Measurement'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status_id' => Yii::t('app', 'Status'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;
                $this->updated_at = 0;
            }
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }


    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Оновлено Користувачем */
    public function getUpdatedUserName()
    {
        return (isset($this->updatedUser->username) ? $this->updatedUser->username : NULL );
    }

    public function getStatus()
    {
        return $this->hasOne(\backend\modules\handbook\models\Status::className(), ['id' => 'status_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(\backend\models\Category::className(), ['id' => 'category_id']);
    }

    public function getMeasurement()
    {
        return $this->hasOne(\backend\modules\handbook\models\Measurement::className(), ['id' => 'measurement_id']);
    }

    //Формує список всіх медикаментів по категоріях (категорія[медикамент])
    public function suppliesList()
    {
        $list = MedicalSupplies::find()->all();
        $out = [];
        foreach ($list as $value)
        {
            $out[] = [
                'id' => $value->id,
                'category' => Category::findOne(['id' => $value->category_id])->name,
                'name' => $value->name . '  ('. $value->measurement->name .')',
            ];
        }

        return ArrayHelper::map($out, 'id', 'name',  'category');
    }

    /**
     * @inheritdoc
     * @return MedicalSuppliesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MedicalSuppliesQuery(get_called_class());
    }
}
