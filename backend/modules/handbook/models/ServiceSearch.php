<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\Service;

/**
 * ServiceSearch represents the model behind the search form about `backend\modules\handbook\models\Service`.
 */
class ServiceSearch extends Service
{
    public $userName;
    public $updatedUserName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'price', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['name', 'description', 'created_at', 'updated_at', 'userName', 'updatedUserName'], 'safe'],
            [['created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Service::find();
        $query->leftJoin('{{%user}}', 'user_id={{%user}}.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
            'status_id' => $this->status_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', '{{%user}}.username', $this->userName])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', '{{%service}}.created_at', $this->created_at])
            ->andFilterWhere(['like', '{{%service}}.updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
