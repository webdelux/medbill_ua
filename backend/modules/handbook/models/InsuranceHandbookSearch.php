<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\InsuranceHandbook;

/**
 * InsuranceHandbookSearch represents the model behind the search form about `backend\modules\handbook\models\InsuranceHandbook`.
 */
class InsuranceHandbookSearch extends InsuranceHandbook
{
    public $user;
    public $updatedUser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'updated_user_id'], 'integer'],
            [['updatedUser','user','name', 'address', 'contacts', 'www', 'edrpou', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InsuranceHandbook::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['id' => 'DESC'],
            'attributes' => [
                'id',
                'name',
                'address',
                'contacts',
                'www',
                'edrpou',
                'user' => [
                    'asc' => ['user_id' => SORT_ASC],
                    'desc' => ['user_id' => SORT_DESC],
                    //'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['updated_user_id' => SORT_ASC],
                   'desc' => ['updated_user_id' => SORT_DESC],
                  // 'default' => SORT_ASC
                ],
                'created_at',
                'updated_at',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'contacts', $this->contacts])
            ->andFilterWhere(['like', 'www', $this->www])
            ->andFilterWhere(['like', 'edrpou', $this->edrpou])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
