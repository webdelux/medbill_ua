<?php

namespace backend\modules\handbook\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;
/**
 * This is the ActiveQuery class for [[Measurement]].
 *
 * @see Measurement
 */
class MeasurementQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    public function abbreviation()
    {
        $this->andWhere('[[abbreviation]] IS NOT NULL');
        return $this;
    }

    /**
     * @inheritdoc
     * @return Measurement[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Measurement|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}