<?php

namespace backend\modules\handbook\models;

/**
 * This is the ActiveQuery class for [[MedicalSupplies]].
 *
 * @see MedicalSupplies
 */
class MedicalSuppliesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MedicalSupplies[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MedicalSupplies|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}