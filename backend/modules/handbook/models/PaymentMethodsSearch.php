<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\PaymentMethods;

/**
 * PaymentMethodsSearch represents the model behind the search form about `backend\modules\handbook\models\PaymentMethods`.
 */
class PaymentMethodsSearch extends PaymentMethods
{
    public $userName;
    public $updatedUser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['updatedUser', 'userName', 'name', 'description', 'created_at', 'updated_at', 'userName', 'updatedUser'], 'safe'],
            [['created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentMethods::find();
        $query->leftJoin('{{%user}} AS user1', 'user_id=user1.id');
        $query->leftJoin('{{%user}} AS user2', 'updated_user_id=user2.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'description',
                'userName' => [
                   'asc' => ['user1.username' => SORT_ASC],
                   'desc' => ['user1.username' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['user2.username' => SORT_ASC],
                   'desc' => ['user2.username' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'created_at',
                'updated_at',
                'status_id',
            ]
       ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
            'status_id' => $this->status_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'user1.username', $this->userName])
            ->andFilterWhere(['like', 'user2.username', $this->updatedUser])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
