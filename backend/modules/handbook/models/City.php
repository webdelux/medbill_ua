<?php

namespace backend\modules\handbook\models;

use Yii;

/**
 * This is the model class for table "{{%geo_city}}".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $important
 * @property integer $region_id
 * @property string $title
 * @property string $area
 * @property string $region
 * @property string $zip
 *
 * @property Region $regionLink
 * @property Country $country
 */
class City extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'important'], 'required'],
            [['country_id', 'important', 'region_id'], 'integer'],
            [['title', 'area', 'region', 'zip'], 'string', 'max' => 160]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Country'),
            'important' => Yii::t('app', 'Great town'),
            'region_id' => Yii::t('app', 'Region'),
            'title' => Yii::t('app', 'Name'),
            'area' => Yii::t('app', 'Area'),
            'region' => Yii::t('app', 'Region'),
            'zip' => Yii::t('app', 'Post index'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegionLink()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @inheritdoc
     * @return CityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityQuery(get_called_class());
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'important' => [
                '1' => Yii::t('app', 'Yes'),
                '0' => Yii::t('app', 'No')
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getAddress()
    {
        $out = $this->title;

        if ($this->area)
            $out .= ', ' . $this->area;

        if ($this->region)
            $out .= ', ' . $this->region;

        return $out;
    }

}