<?php

namespace backend\modules\handbook\models;

/**
 * This is the ActiveQuery class for [[InsuranceHandbook]].
 *
 * @see InsuranceHandbook
 */
class InsuranceHandbookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return InsuranceHandbook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return InsuranceHandbook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}