<?php

namespace backend\modules\handbook\models;

/**
 * This is the ActiveQuery class for [[HandbookRoom]].
 *
 * @see HandbookRoom
 */
class HandbookRoomQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return HandbookRoom[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HandbookRoom|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}