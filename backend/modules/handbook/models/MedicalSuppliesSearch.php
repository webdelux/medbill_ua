<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\MedicalSupplies;

/**
 * MedicalSuppliesSearch represents the model behind the search form about `backend\modules\handbook\models\MedicalSupplies`.
 */
class MedicalSuppliesSearch extends MedicalSupplies
{
     public $user;
     public $updatedUser;
     public $measurement;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['measurement_id', 'measurement', 'name', 'description', 'created_at', 'updated_at', 'user', 'updatedUser'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MedicalSupplies::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
           'attributes' => [
                'id',
                'category_id',
                'name',
                'measurement' => [
                    'asc' => ['measurement_id' => SORT_ASC],
                    'desc' => ['measurement_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'description',
                'user' => [
                    'asc' => ['user_id' => SORT_ASC],
                    'desc' => ['user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'updatedUser' => [
                    'asc' => ['updated_user_id' => SORT_ASC],
                    'desc' => ['updated_user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'created_at',
                'updated_at',
                'status_id',
               'price'
           ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'measurement_id' => $this->measurement_id,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status_id' => $this->status_id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
