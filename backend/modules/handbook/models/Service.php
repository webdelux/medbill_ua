<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property integer $price
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status_id
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['name'], 'required'],
            [['price'], 'number'],
            [['created_at', 'updated_at', 'user_id'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status_id' => Yii::t('app', 'Status'),
        ];
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    /* Гетер для Оновлено Користувачем */
    public function getUpdatedUserName()
    {
        return (isset($this->updatedUser->username) ? $this->updatedUser->username : NULL );
    }

    public function getStatus()
    {
        return $this->hasOne(\backend\modules\handbook\models\Status::className(), ['id' => 'status_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(\backend\models\Category::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return ServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServiceQuery(get_called_class());
    }
}
