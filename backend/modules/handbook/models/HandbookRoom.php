<?php

namespace backend\modules\handbook\models;

use Yii;
use dektrium\user\models\User;
use backend\modules\handbook\models\Department;

/**
 * This is the model class for table "{{%handbook_room}}".
 *
 * @property integer $id
 * @property integer $department_id
 * @property string $room
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class HandbookRoom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_room}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_id', 'user_id', 'updated_user_id'], 'integer'],
            [['department_id', 'room'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['description', 'room'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department_id' => Yii::t('app', 'Departments'),
            'room' => Yii::t('app', 'Room'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;
                
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            $room = HandbookRoom::find() // Шукаємо в цьому відділенні однакові палати (дублікати).
                    ->andWhere(['department_id' =>  $this->department_id])
                    ->andWhere(['room' =>  $this->room])
                    ->one();

            if (isset($room))  // Знайшли вже таку палату
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'You have added this room already!'));
                return false;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    /* Гетер для Оновлено Користувачем */
    public function getUpdatedUserName()
    {
        return (isset($this->updatedUser->username) ? $this->updatedUser->username : NULL );
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @inheritdoc
     * @return HandbookRoomQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HandbookRoomQuery(get_called_class());
    }
}
