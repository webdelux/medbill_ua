<?php

namespace backend\modules\handbook\models;

use Yii;
use dektrium\user\models\User;
use backend\modules\handbook\models\City;
/**
 * This is the model class for table "{{%refferal}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $speciality_id
 * @property string $pib
 * @property integer $gender
 * @property string $birthday
 * @property string $phone
 * @property string $phone1
 * @property string $email
 * @property string $requisites
 * @property integer $work_id
 * @property integer $city_id
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status_id
 */
class Refferal extends \yii\db\ActiveRecord
{
    public $cityName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%refferal}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'pib'], 'required'],
            [['category_id', 'speciality_id', 'gender', 'work_id', 'city_id', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['birthday', 'created_at', 'updated_at'], 'safe'],
            [['pib'], 'match', 'pattern' => \backend\models\Pacient::$pibRegexp],
            [['phone','phone1'], 'match', 'pattern' => \backend\modules\user\models\Users::$phoneRegexp],
            [['requisites', 'description'], 'string', 'max' => 255],
            [['phone', 'phone1'], 'string', 'max' => 20],
            [['email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category'),
            'speciality_id' => Yii::t('speciality', 'Speciality'),
            'pib' => Yii::t('app', 'Full Name'),
            'gender' => Yii::t('app', 'Gender'),
            'birthday' => Yii::t('app', 'Birthday'),
            'phone' => Yii::t('app', 'Phone number'),
            'phone1' => Yii::t('app', 'Additional phone number'),
            'email' => Yii::t('app', 'Email'),
            'requisites' => Yii::t('app', 'Requisites'),
            'work_id' => Yii::t('app', 'Work place'),
            'city_id' => Yii::t('app', 'City'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status_id' => Yii::t('app', 'Status'),
            'cityName' => Yii::t('app', 'City'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    /* Гетер для Оновлено Користувачем */
    public function getUpdatedUserName()
    {
        return (isset($this->updatedUser->username) ? $this->updatedUser->username : NULL );
    }

    public function getStatus()
    {
        return $this->hasOne(\backend\modules\handbook\models\Status::className(), ['id' => 'status_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(\backend\models\Category::className(), ['id' => 'category_id']);
    }

    public function getSpeciality()
    {
        return $this->hasOne(\backend\modules\handbook\models\Speciality::className(), ['id' => 'speciality_id']);
    }

    public function getWorkplace()
    {
        return $this->hasOne(\backend\modules\handbook\models\Workplace::className(), ['id' => 'work_id']);
    }

    /**
     * @inheritdoc
     * @return RefferalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefferalQuery(get_called_class());
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'gender' => [
                '1' => Yii::t('app', 'Man'),
                '2' => Yii::t('app', 'Woman'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
}
