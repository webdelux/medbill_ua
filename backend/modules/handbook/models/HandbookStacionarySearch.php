<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\HandbookStacionary;

/**
 * HandbookStacionarySearch represents the model behind the search form about `backend\modules\handbook\models\HandbookStacionary`.
 */
class HandbookStacionarySearch extends HandbookStacionary
{
    public $userName;
    public $updatedUser;
    public $department;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'updated_user_id', 'pacient_id'], 'integer'],
            [['type', 'bed_type', 'room_id', 'department', 'updatedUser', 'userName', 'place', 'description', 'created_at', 'updated_at', 'status'], 'safe'],
            [['created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HandbookStacionary::find();
        $query->leftJoin('{{%user}} AS user1', 'user_id=user1.id');
        //$query->leftJoin('{{%user}} AS user2', 'updated_user_id=user2.id');
        $query->leftJoin('{{%handbook_room}} AS room', 'room_id=room.id');
        $query->orderBy('department_id, room_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                '{{%handbook_stacionary}}.id',
                '{{%handbook_stacionary}}.room_id',
                '{{%handbook_stacionary}}.place',
                '{{%handbook_stacionary}}.description',
                'userName' => [
                   'asc' => ['user1.username' => SORT_ASC],
                   'desc' => ['user1.username' => SORT_DESC],
                   'default' => SORT_ASC
                ],
//                'updatedUser' => [
//                   'asc' => ['user2.username' => SORT_ASC],
//                   'desc' => ['user2.username' => SORT_DESC],
//                   'default' => SORT_ASC
//                ],
                'created_at',
                'updated_at',
//                'department' => [
//                   'asc' => ['room.department_id' => SORT_ASC],
//                   'desc' => ['room.department_id' => SORT_DESC],
//                ],
                'place',
                'description',
                'bed_type',
                'type',
                'pacient_id',
                'room_id',
                'status',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%handbook_stacionary}}.id' => $this->id,
            //'{{%handbook_stacionary}}.room_id' => $this->room_id,
            //'user_id' => $this->user_id,
            //'updated_user_id' => $this->updated_user_id,
            //'{{%handbook_stacionary}}.created_at' => $this->created_at,
            //'{{%handbook_stacionary}}.updated_at' => $this->updated_at,
            'room.department_id' => $this->department,
            'bed_type' => $this->bed_type,
            'type' => $this->type,
             '{{%handbook_stacionary}}.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', '{{%handbook_stacionary}}.place', $this->place])
            ->andFilterWhere(['like', '{{%handbook_stacionary}}.description', $this->description])
            ->andFilterWhere(['like', 'room.room', $this->room_id])
            ->andFilterWhere(['like', 'user1.username', $this->userName])
            //->andFilterWhere(['like', 'user2.username', $this->updatedUser])
            ->andFilterWhere(['like', '{{%handbook_stacionary}}.created_at', $this->created_at])
            ->andFilterWhere(['like', '{{%handbook_stacionary}}.updated_at', $this->updated_at]);

        if ($this->pacient_id == 0){
            $query->andFilterWhere([
                'pacient_id' => $this->pacient_id,
            ]);

        } else {
            $query->andFilterWhere(['>', '{{%handbook_stacionary}}.pacient_id', 0]);
        }

        return $dataProvider;
    }
}
