<?php

namespace backend\modules\handbook\models;

use Yii;
use paulzi\adjacencylist\AdjacencyListBehavior;
use dektrium\user\models\User;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%status}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $name
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Status extends \yii\db\ActiveRecord
{

    const _COMMON = 1;
    const _INACTIVE = 2;
    const _ACTIVE = 3;
    const _BLOCKED = 4;
    const _ARCHIVE = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent status'),
            'sort' => Yii::t('app', 'Sort'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return StatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatusQuery(get_called_class());
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => AdjacencyListBehavior::className(),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function childList($parentId = 1)
    {
        $model = Status::findOne(['id' => $parentId]);

        return ArrayHelper::map($model->getChildren()->all(), 'id', 'name');
    }

    private $out = [];

    public function treeList($data, $level = 0)
    {
        foreach ($data as $item)
        {
            $this->out[$item['id']] = str_repeat(" - ", $level). $item['text'];

            if (isset($item['nodes']))
            {
                $this->treeList($item['nodes'], $level + 1);
            }
        }

        return $this->out;
    }

    public function treeChild($id)
    {
        $model = Status::findOne(['id' => $id]);
        if ($model)
        {
            $childrens = $model->getChildren()->all();
            if (sizeOf($childrens) > 0)
            {
                $out = [];
                foreach ($childrens as $children)
                {
                    $out[] = [
                        'id' => $children->id,
                        'text' => $children->name,
                        'icon' => 'fa fa-folder-o',
                        'href' => Url::to(['view', 'id' => $children->id]),
                        'nodes' => $this->treeChild($children->id),
                    ];
                }
                return $out;
            }
            else
                return null;
        }
        return null;
    }

    public function treeData()
    {
        $roots = Status::find()->roots()->all();

        $out = [];
        foreach ($roots as $root)
        {
            $out[] = [
                'id' => $root->id,
                'text' => $root->name,
                'icon' => 'fa fa-folder-o',
                'href' => Url::to(['view', 'id' => $root->id]),
                'nodes' => $this->treeChild($root->id),
            ];
        }

        return $out;
    }

}