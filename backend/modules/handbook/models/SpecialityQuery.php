<?php

namespace backend\modules\handbook\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;

/**
 * This is the ActiveQuery class for [[Speciality]].
 *
 * @see Speciality
 */
class SpecialityQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;
    
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Speciality[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Speciality|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}