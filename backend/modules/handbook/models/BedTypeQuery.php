<?php

namespace backend\modules\handbook\models;

/**
 * This is the ActiveQuery class for [[BedType]].
 *
 * @see BedType
 */
class BedTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return BedType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BedType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}