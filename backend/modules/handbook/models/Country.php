<?php

namespace backend\modules\handbook\models;

use Yii;

/**
 * This is the model class for table "{{%geo_country}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $code1
 * @property string $code2
 * @property string $code3
 * @property string $note
 *
 * @property City[] $cities
 * @property Region[] $regions
 */
class Country extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code1', 'code2', 'code3'], 'string', 'max' => 64],
            [['note'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Name'),
            'code1' => Yii::t('app', 'Code1'),
            'code2' => Yii::t('app', 'Code2'),
            'code3' => Yii::t('app', 'Code3'),
            'note' => Yii::t('app', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::className(), ['country_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return CountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CountryQuery(get_called_class());
    }

}