<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\Refferal;

/**
 * RefferalSearch represents the model behind the search form about `backend\modules\handbook\models\Refferal`.
 */
class RefferalSearch extends Refferal
{
    public $userName;
    public $updatedUser;
    public $cityName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'gender', 'work_id', 'city_id', 'user_id', 'updated_user_id', 'status_id', 'speciality_id'], 'integer'],
            [['cityName', 'updatedUser', 'userName', 'pib', 'birthday', 'phone', 'phone1', 'email', 'requisites', 'description', 'created_at', 'updated_at'], 'safe'],
            [['created_at', 'updated_at'], 'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Refferal::find();
        $query->leftJoin('{{%geo_city}}', 'city_id={{%geo_city}}.id');
        $query->leftJoin('{{%user}} AS user1', 'user_id=user1.id');
        $query->leftJoin('{{%user}} AS user2', 'updated_user_id=user2.id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'pib',
                'category_id',
                'speciality_id',
                'birthday',
                'phone',
                'phone1',
                'email',
                'requisites',
                'gender',
                'work_id',
                'city_id',
                'cityName' => [
                   'asc' => ['{{%geo_city}}.title' => SORT_ASC],
                   'desc' => ['{{%geo_city}}.title' => SORT_DESC],
                   'default' => SORT_ASC
               ],
                'description',
                'userName' => [
                   'asc' => ['user1.username' => SORT_ASC],
                   'desc' => ['user1.username' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['user2.username' => SORT_ASC],
                   'desc' => ['user2.username' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'created_at',
                'updated_at',
                'status_id',
            ]
       ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%refferal}}.id' => $this->id,
            '{{%refferal}}.category_id' => $this->category_id,
            '{{%refferal}}.speciality_id' => $this->speciality_id,
            '{{%refferal}}.gender' => $this->gender,
            '{{%refferal}}.birthday' => $this->birthday,
            '{{%refferal}}.work_id' => $this->work_id,
            //'city_id' => $this->city_id,
            //'user_id' => $this->user_id,
            //'updated_user_id' => $this->updated_user_id,
            //'{{%refferal}}.created_at' => $this->created_at,
            //'{{%refferal}}.updated_at' => $this->updated_at,
            '{{%refferal}}.status_id' => $this->status_id,
        ]);

        $query->andFilterWhere(['like', '{{%refferal}}.pib', $this->pib])
            ->andFilterWhere(['like', '{{%refferal}}.phone', $this->phone])
            ->andFilterWhere(['like', '{{%refferal}}.phone1', $this->phone1])
            ->andFilterWhere(['like', '{{%refferal}}.email', $this->email])
            ->andFilterWhere(['like', '{{%refferal}}.requisites', $this->requisites])
            ->andFilterWhere(['like', '{{%refferal}}.description', $this->description])
            ->andFilterWhere(['like', '{{%geo_city}}.title', $this->cityName])
            ->andFilterWhere(['like', 'user1.username', $this->userName])
            ->andFilterWhere(['like', 'user2.username', $this->updatedUser])
            ->andFilterWhere(['like', '{{%refferal}}.created_at', $this->created_at])
            ->andFilterWhere(['like', '{{%refferal}}.updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
