<?php
namespace backend\modules\handbook\models;

use Yii;
use dektrium\user\models\User;
use backend\modules\handbook\models\HandbookRoom;
use backend\modules\handbook\models\BedType;

/**
 * This is the model class for table "{{%handbook_stacionary}}".
 *
 * @property integer $id
 * @property integer $room_id
 * @property string $place
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $pacient_id
 * @property integer $bed_type
 * @property integer $type
 * @property integer $status
 */
class HandbookStacionary extends \yii\db\ActiveRecord
{
    public $department;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_stacionary}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'user_id', 'updated_user_id', 'pacient_id'], 'integer'],
            [['room_id', 'department'], 'required', 'on' => 'create'],
            [['created_at', 'updated_at', 'bed_type', 'type', 'status'], 'safe'],
            [['place', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department' => Yii::t('app', 'Departments'),
            'room_id' => Yii::t('app', 'Room'),
            'place' => Yii::t('app', 'Place'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'pacient_id' => Yii::t('app', 'Empty'), //0 - is empty place;
            'bed_type' => Yii::t('app', 'Bed type'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

//            if (!$this->pacient_id >= 0)  // встановлюємо "вільно" для нових ліжок
//            {
//                $this->pacient_id = 0;
//            }

                $place = HandbookStacionary::find() // Шукаємо в цьому відділенні в цій палаті однакові місця (дублікати).
                    ->andWhere(['room_id' =>  $this->room_id])   //Ід палати визначає ід відділення.
                    ->andWhere(['place' =>  $this->place])
                    ->one();

                if (isset($place))  // Якщо знайшли вже таке місце
                {
                    if ($insert)    // Якщо вставили новий запис
                    {
                        Yii::$app->session->setFlash('success', Yii::t('app', 'You have added this place already!'));
                        return false;
                    }
                    else            // Інакше оновлюємо вже існуючий запис
                    {
                        if (!($place->id == $this->id)) //знайшли іншимй запис з такими даними!
                        {
                            Yii::$app->session->setFlash('success', Yii::t('app', 'You have added this place already!'));
                        return false;
                        }

                    }
                }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    /* Гетер для Оновлено Користувачем */
    public function getUpdatedUserName()
    {
        return (isset($this->updatedUser->username) ? $this->updatedUser->username : NULL );
    }

    public function getRoom()
    {
        return $this->hasOne(HandbookRoom::className(), ['id' => 'room_id']);
    }

    public function getBedType()
    {
        return $this->hasOne(BedType::className(), ['id' => 'bed_type']);
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'status' => [
                '1' => Yii::t('app', 'Active'),
                '2' => Yii::t('app', 'Repairing'),
                '3' => Yii::t('app', 'Not active'),
            ],
            'gender_type' => [
                '1' => Yii::t('app', 'Common'),
                '2' => Yii::t('app', 'Male'),
                '3' => Yii::t('app', 'Female'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function roomList($department = null, $room = null, $pacient = null)
    {
        if (isset($department))             // вибираємо всі палати для зазначеного відділення
        {
            $query = HandbookStacionary::find()->joinWith(['room']);

            $places = $query
                ->where(['{{%handbook_room}}.department_id' => $department])
                ->andWhere(['=', '{{%handbook_stacionary}}.pacient_id', 0])
                ->orWhere(['=', '{{%handbook_stacionary}}.pacient_id', $pacient])
                ->orderBy('{{%handbook_room}}.room ASC')
                ->all();

            $out = [];
            foreach ($places as $value)
            {
                $out[$value->room_id] = $value->room->room;

            }

            return $out;
        }

        if (isset($room))               //вибираємо всі ліжка для зазначеної палати
        {
            $places = HandbookStacionary::find()
                //->leftJoin('{{%handbook_room}}', 'room_id={{%handbook_room}}.id')
                ->where(['room_id' => $room])
                ->andWhere(['pacient_id' => 0])
                ->orWhere(['=', 'pacient_id', $pacient])
                ->orderBy('place ASC')
                ->all();

            $out = [];
            foreach ($places as $value)
            {
                $out[$value->id] = $value->place;
            }

            return $out;
        }
    }

    public static function freePlaces()
    {
        $places = HandbookStacionary::find()
                ->where(['pacient_id' => 0])
                ->count();

        return $places;
    }

    public static function totalPlaces()
    {
        $places = HandbookStacionary::find()
                ->count();

        return $places;
    }

    /**
     * @inheritdoc
     * @return HandbookStacionaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HandbookStacionaryQuery(get_called_class());
    }
}
