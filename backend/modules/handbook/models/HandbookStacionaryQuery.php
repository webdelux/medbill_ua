<?php

namespace backend\modules\handbook\models;

/**
 * This is the ActiveQuery class for [[HandbookStacionary]].
 *
 * @see HandbookStacionary
 */
class HandbookStacionaryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return HandbookStacionary[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HandbookStacionary|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}