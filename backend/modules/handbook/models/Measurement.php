<?php

namespace backend\modules\handbook\models;

use Yii;
use dektrium\user\models\User;
use paulzi\adjacencylist\AdjacencyListBehavior;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%measurement}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $name
 * @property string $abbreviation
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status_id
 */
class Measurement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%measurement}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'user_id', 'updated_user_id', 'status_id'], 'integer'],
            [['name'], 'required'],
            [['sort', 'user_id', 'created_at', 'updated_at', 'abbreviation'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent category'),
            'sort' => Yii::t('app', 'Sort'),
            'name' => Yii::t('app', 'Name'),
            'abbreviation' => Yii::t('app', 'Abbreviation'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status_id' => Yii::t('app', 'Status'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => AdjacencyListBehavior::className(),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @inheritdoc
     * @return MeasurementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MeasurementQuery(get_called_class());
    }

    private $out = [];

    public function treeList($data, $level = 0)
    {
        foreach ($data as $item)
        {
            $this->out[$item['id']] = str_repeat(" - ", $level). $item['text'];

            if (isset($item['nodes']))
            {
                $this->treeList($item['nodes'], $level + 1);
            }
        }

        return $this->out;
    }

    public function treeListParent($data)
    {
        foreach ($data as $item)
        {
            $this->out[$item['id']] = [
                'id' => $item['id'],
                'parent_id' => $item['parent_id'],
                'name' => $item['text'],
                'abbreviation' => $item['abbreviation'],
                'description' => $item['description'],
                'user_id' => $item['user_id'],
                'updated_user_id' => $item['updated_user_id'],
                'created_at' => $item['created_at'],
                'updated_at' => $item['updated_at'],
                'status_id' => $item['status_id'],
           ];

            if (isset($item['nodes']))
            {
                $this->treeListParent($item['nodes']);
            }
        }

        return $this->out;
    }

    public function treeChild($id)
    {
        $model = Measurement::findOne(['id' => $id]);
        if ($model)
        {
            $childrens = $model->getChildren()->all();
            if (sizeOf($childrens) > 0)
            {
                $out = [];
                foreach ($childrens as $children)
                {
                    $out[] = [
                        'id' => $children->id,
                        'parent_id' => $children->parent_id,
                        'text' => $children->name,
                        'abbreviation' => $children->abbreviation,
                        'description' => $children->description,
                        'user_id' => $children->user_id,
                        'updated_user_id' => $children->updated_user_id,
                        'created_at' => $children->created_at,
                        'updated_at' => $children->updated_at,
                        'status_id' => $children->status_id,
                        'icon' => 'fa fa-folder-o',
                        'href' => Url::to(['view', 'id' => $children->id]),
                        'nodes' => $this->treeChild($children->id),
                    ];
                }
                return $out;
            }
            else
                return null;
        }
        return null;
    }

    public function treeData()
    {
        $roots = Measurement::find()->roots()->all();

        $out = [];
        foreach ($roots as $root)
        {
            $out[] = [
                'id' => $root->id,
                'parent_id' => $root->parent_id,
                'text' => $root->name,
                'abbreviation' => $root->abbreviation,
                'description' => $root->description,
                'user_id' => $root->user_id,
                'updated_user_id' => $root->updated_user_id,
                'created_at' => $root->created_at,
                'updated_at' => $root->updated_at,
                'status_id' => $root->status_id,
                'icon' => 'fa fa-folder-o',
                'href' => Url::to(['view', 'id' => $root->id]),
                'nodes' => $this->treeChild($root->id),
            ];
        }

        return $out;
    }

}
