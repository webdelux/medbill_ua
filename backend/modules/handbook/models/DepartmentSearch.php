<?php

namespace backend\modules\handbook\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbook\models\Department;

/**
 * DepartmentSearch represents the model behind the search form about `backend\models\Department`.
 */
class DepartmentSearch extends Department
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lft', 'rgt', 'depth', 'status_id', 'type_id', 'user_id', 'updated_user_id'], 'integer'],
            [['name', 'address', 'phone', 'email', 'edrpou', 'requisites', 'working_days', 'working_time_mon_from', 'working_time_mon_to', 'working_time_tue_from', 'working_time_tue_to', 'working_time_wed_from', 'working_time_wed_to', 'working_time_thu_from', 'working_time_thu_to', 'working_time_fri_from', 'working_time_fri_to', 'working_time_sat_from', 'working_time_sat_to', 'working_time_sun_from', 'working_time_sun_to', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Department::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lft' => $this->lft,
            'rgt' => $this->rgt,
            'depth' => $this->depth,
            'working_time_mon_from' => $this->working_time_mon_from,
            'working_time_mon_to' => $this->working_time_mon_to,
            'working_time_tue_from' => $this->working_time_tue_from,
            'working_time_tue_to' => $this->working_time_tue_to,
            'working_time_wed_from' => $this->working_time_wed_from,
            'working_time_wed_to' => $this->working_time_wed_to,
            'working_time_thu_from' => $this->working_time_thu_from,
            'working_time_thu_to' => $this->working_time_thu_to,
            'working_time_fri_from' => $this->working_time_fri_from,
            'working_time_fri_to' => $this->working_time_fri_to,
            'working_time_sat_from' => $this->working_time_sat_from,
            'working_time_sat_to' => $this->working_time_sat_to,
            'working_time_sun_from' => $this->working_time_sun_from,
            'working_time_sun_to' => $this->working_time_sun_to,
            'status_id' => $this->status_id,
            'type_id' => $this->type_id,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'edrpou', $this->edrpou])
            ->andFilterWhere(['like', 'requisites', $this->requisites])
            ->andFilterWhere(['like', 'working_days', $this->working_days]);

        return $dataProvider;
    }

}