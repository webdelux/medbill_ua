<?php

namespace backend\modules\handbook;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\handbook\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}