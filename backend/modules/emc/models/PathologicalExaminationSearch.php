<?php

namespace backend\modules\emc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\emc\models\PathologicalExamination;

/**
 * PathologicalExaminationSearch represents the model behind the search form about `backend\modules\emc\models\PathologicalExamination`.
 */
class PathologicalExaminationSearch extends PathologicalExamination
{
    public $pacientFullName;
    public $doctorFullName;
    public $icdName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'department_id', 'doctor_id', 'diagnosis_id', 'pathological_diagnosis_id', 'accuracy', 'divergence', 'user_id', 'updated_user_id'], 'integer'],
            [['pathological_number', 'pathological_date', 'ill_time', 'cause', 'ills', 'other_reasons', 'pregnancy_interval', 'description', 'created_at', 'updated_at', 'pacientFullName', 'doctorFullName', 'icdName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PathologicalExamination::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'pacient_id',
                'pacientFullName' => [
                   'asc' => ['pacient_id' => SORT_ASC],
                   'desc' => ['pacient_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'doctor_id',
                'doctorFullName' => [
                   'asc' => ['doctor_id' => SORT_ASC],
                   'desc' => ['doctor_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'department_id',
                'description',
                'created_at',
                'updated_at',
                'user_id',
                'user' => [
                   'asc' => ['user_id' => SORT_ASC],
                   'desc' => ['user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['updated_user_id' => SORT_ASC],
                   'desc' => ['updated_user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updated_user_id',
                'diagnosis_id',
                'pathological_diagnosis_id',
                'pathological_number',
                'pathological_date',
                'ill_time',
                'cause',
                'ills',
                'other_reasons',
                'pregnancy_interval',
                'accuracy',
                'divergence',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'department_id' => $this->department_id,
            'doctor_id' => $this->doctor_id,
            'diagnosis_id' => $this->diagnosis_id,
            'pathological_diagnosis_id' => $this->pathological_diagnosis_id,
            'pathological_date' => $this->pathological_date,
            'accuracy' => $this->accuracy,
            'divergence' => $this->divergence,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'pathological_number', $this->pathological_number])
            ->andFilterWhere(['like', 'ill_time', $this->ill_time])
            ->andFilterWhere(['like', 'cause', $this->cause])
            ->andFilterWhere(['like', 'ills', $this->ills])
            ->andFilterWhere(['like', 'other_reasons', $this->other_reasons])
            ->andFilterWhere(['like', 'pregnancy_interval', $this->pregnancy_interval])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
