<?php

namespace backend\modules\emc\models;

use Yii;

/**
 * This is the model class for table "{{%diary}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $diagnosis_id
 * @property integer $diary_number
 * @property string $diary_date
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Diary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%diary}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'diagnosis_id'], 'required'],
            [['pacient_id', 'diagnosis_id', 'diary_number', 'user_id', 'updated_user_id'], 'integer'],
            [['diary_date', 'created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'diary_number' => Yii::t('app', 'Diary') . ' №',
            'diary_date' => Yii::t('app', 'Diary') . ' ' . Yii::t('app', 'Date'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(\backend\models\Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    /**
     * @inheritdoc
     * @return DiaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiaryQuery(get_called_class());
    }
}
