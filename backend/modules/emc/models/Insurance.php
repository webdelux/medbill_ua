<?php

namespace backend\modules\emc\models;

use Yii;

/**
 * This is the model class for table "{{%insurance}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $insurance_id
 * @property string $insurance_number
 * @property string $insurance_date
 * @property string $insurance_date_end
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Insurance extends \yii\db\ActiveRecord
{
     public $fullName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%insurance}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insurance_id', 'pacient_id', 'user_id', 'updated_user_id'], 'integer'],
            [['insurance_date', 'insurance_date_end', 'created_at', 'updated_at'], 'safe'],
            [['pacient_id', 'insurance_id'], 'required'],
            [['insurance_number', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'fullName' => Yii::t('app', 'Pacient'),
            'insurance_id' => Yii::t('app', 'Insurance company'),
            'insurance_number' => Yii::t('app', 'Insurance number'),
            'insurance_date' => Yii::t('app', 'Date start'),
            'insurance_date_end' => Yii::t('app', 'Date end'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(\backend\models\Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getInsurance()
    {
        return $this->hasOne(\backend\modules\handbook\models\InsuranceHandbook::className(), ['id' => 'insurance_id']);
    }

    /**
     * @inheritdoc
     * @return InsuranceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InsuranceQuery(get_called_class());
    }
}
