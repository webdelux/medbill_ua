<?php

namespace backend\modules\emc\models;

use Yii;

/**
 * This is the model class for table "{{%pathological_examination}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $department_id
 * @property integer $doctor_id
 * @property integer $diagnosis_id
 * @property integer $pathological_diagnosis_id
 * @property string $pathological_number
 * @property string $pathological_date
 * @property integer $accuracy
 * @property integer $divergence
 * @property string $ill_time
 * @property string $cause
 * @property string $ills
 * @property string $other_reasons
 * @property string $pregnancy_interval
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class PathologicalExamination extends \yii\db\ActiveRecord
{
    public $pacientFullName;
    public $doctorFullName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pathological_examination}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'doctor_id', 'diagnosis_id', 'pathological_diagnosis_id', 'pathological_number', 'pathological_date', 'accuracy', 'divergence', 'ill_time', 'cause'], 'required'],
            [['pacient_id', 'department_id', 'doctor_id', 'diagnosis_id', 'pathological_diagnosis_id', 'accuracy', 'divergence', 'user_id', 'updated_user_id'], 'integer'],
            [['pathological_date', 'created_at', 'updated_at'], 'safe'],
            [['pathological_number', 'ill_time', 'cause', 'ills', 'other_reasons', 'pregnancy_interval', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'pacientFullName' => Yii::t('app', 'Pacient'),
            'department_id' => Yii::t('app', 'Departments'),
            'doctor_id' => Yii::t('app', 'Doctor'),
            'doctorFullName' => Yii::t('app', 'Doctor'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'pathological_diagnosis_id' => Yii::t('app', 'Pathological') . ' ' . Yii::t('app', 'Diagnosis'),
            'pathological_number' => Yii::t('app', 'Pathological number'),
            'pathological_date' => Yii::t('app', 'Pathological Date'),
            'accuracy' => Yii::t('app', 'Accuracy'),
            'divergence' => Yii::t('app', 'Divergence'),
            'ill_time' => Yii::t('app', 'Ill time'),
            'cause' => Yii::t('app', 'Cause'),
            'ills' => Yii::t('app', 'Ills'),
            'other_reasons' => Yii::t('app', 'Other reasons'),
            'pregnancy_interval' => Yii::t('app', 'Pregnancy interval'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(\backend\models\Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(\backend\modules\handbook\models\Department::className(), ['id' => 'department_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'doctor_id']);
    }

    public function getDiagnosisPathological()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'pathological_diagnosis_id']);
    }

    public function getAdditional()
    {
        return $this->hasMany(\backend\modules\diagnosis\models\DiagnosisAdditional::className(), ['diagnosis_id' => 'pathological_diagnosis_id']);
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'accuracy' => [
                '1' => Yii::t('app', 'Full coincidence'),
                '2' => Yii::t('app', 'Divergences primary diagnosis'),
                '3' => Yii::t('app', 'Divergences concomitant diagnosis'),
                '4' => Yii::t('app', 'Divergences сomplication diagnosis'),
                '5' => Yii::t('app', 'Full divergence'),
            ],
            'divergence' => [
                '1' => Yii::t('app', 'None'),
                '2' => Yii::t('app', 'The objective difficulties of diagnosis'),
                '3' => Yii::t('app', 'Short hospital stay'),
                '4' => Yii::t('app', 'Less examination of the patient'),
                '5' => Yii::t('app', 'Revaluation survey data'),
                '6' => Yii::t('app', 'Rare diseases'),
                '7' => Yii::t('app', 'Wrong disign of diagnosis'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * @inheritdoc
     * @return PathologicalExaminationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PathologicalExaminationQuery(get_called_class());
    }
}
