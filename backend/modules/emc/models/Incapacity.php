<?php

namespace backend\modules\emc\models;

use Yii;

/**
 * This is the model class for table "{{%incapacity}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $diagnosis_id
 * @property integer $icd
 * @property integer $doctor_id
 * @property integer $department_id
 * @property integer $incapacity_type
 * @property string $incapacity_number
 * @property string $incapacity_date
 * @property string $incapacity_date_start
 * @property string $incapacity_date_end
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Incapacity extends \yii\db\ActiveRecord
{
    public $pacientFullName;
    public $doctorFullName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%incapacity}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'diagnosis_id', 'doctor_id', 'department_id', 'incapacity_date', 'incapacity_number', 'incapacity_type',  'incapacity_date_start'], 'required'],
            [['icd', 'pacient_id', 'diagnosis_id', 'doctor_id', 'department_id', 'incapacity_type', 'user_id', 'updated_user_id'], 'integer'],
            [['incapacity_date', 'incapacity_date_start', 'incapacity_date_end', 'created_at', 'updated_at', 'pacientFullName', 'doctorFullName'], 'safe'],
            [['incapacity_number', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'icd' => Yii::t('app', 'ICD'),
            'doctor_id' => Yii::t('app', 'Doctor'),
            'department_id' => Yii::t('app', 'Departments'),
            'incapacity_type' => Yii::t('app', 'Incapacity Type'),
            'incapacity_number' => Yii::t('app', '№'),
            'incapacity_date' => Yii::t('app', 'Date'),
            'incapacity_date_start' => Yii::t('app', 'Date start'),
            'incapacity_date_end' => Yii::t('app', 'Date end'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(\backend\models\Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'doctor_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(\backend\modules\handbook\models\Department::className(), ['id' => 'department_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'incapacity_type' => [
                '1' => Yii::t('app', 'Sick'),
                '2' => Yii::t('app', 'Still sick'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * @inheritdoc
     * @return IncapacityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IncapacityQuery(get_called_class());
    }
}
