<?php

namespace backend\modules\emc\models;

/**
 * This is the ActiveQuery class for [[Diary]].
 *
 * @see Diary
 */
class DiaryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Diary[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Diary|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
