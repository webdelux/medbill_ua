<?php

namespace backend\modules\emc\models;

/**
 * This is the ActiveQuery class for [[Incapacity]].
 *
 * @see Incapacity
 */
class IncapacityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Incapacity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Incapacity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}