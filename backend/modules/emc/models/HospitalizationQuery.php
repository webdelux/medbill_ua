<?php

namespace backend\modules\emc\models;

/**
 * This is the ActiveQuery class for [[Hospitalization]].
 *
 * @see Hospitalization
 */
class HospitalizationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Hospitalization[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Hospitalization|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}