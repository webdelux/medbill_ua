<?php

namespace backend\modules\emc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\emc\models\Insurance;

/**
 * InsuranceSearch represents the model behind the search form about `backend\modules\emc\models\Insurance`.
 */
class InsuranceSearch extends Insurance
{
    public $fullName;
    public $user;
    public $updatedUser;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'user_id', 'updated_user_id'], 'integer'],
            [['insurance_id', 'user', 'updatedUser', 'fullName','insurance_number', 'insurance_date',
                'insurance_date_end', 'description', 'created_at', 'updated_at'], 'safe'],
            [['insurance_date_end', 'insurance_date', 'created_at', 'updated_at'],
                'date', 'format' => 'yyyy-M-d', 'message' => Yii::t('app', 'Wrong date format. Must be: year-month-date')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Insurance::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'pacient_id',
                'insurance_id',
                'fullName' => [
                   'asc' => ['pacient_id' => SORT_ASC],
                   'desc' => ['pacient_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'insurance_number',
                'insurance_date',
                'insurance_date_end',
                'user_id',
                'updated_user_id',
                'user' => [
                    'asc' => ['user_id' => SORT_ASC],
                    'desc' => ['user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'updatedUser' => [
                    'asc' => ['updated_user_id' => SORT_ASC],
                    'desc' => ['updated_user_id' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'description',
                'created_at',
                'updated_at',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'insurance_id' => $this->insurance_id,
            //'insurance_date' => $this->insurance_date,
            //'insurance_date_end' => $this->insurance_date_end,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'insurance_number', $this->insurance_number])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'insurance_date', $this->insurance_date])
            ->andFilterWhere(['like', 'insurance_date_end', $this->insurance_date_end]);

        return $dataProvider;
    }
}
