<?php

namespace backend\modules\emc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\emc\models\EpitomeEpicrisis;

/**
 * EpitomeEpicrisisSearch represents the model behind the search form about `backend\modules\emc\models\EpitomeEpicrisis`.
 */
class EpitomeEpicrisisSearch extends EpitomeEpicrisis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'diagnosis_id', 'doctor_id', 'user_id', 'updated_user_id'], 'integer'],
            [['epicrisis_date', 'epicrisis_text', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EpitomeEpicrisis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'diagnosis_id' => $this->diagnosis_id,
            'epicrisis_date' => $this->epicrisis_date,
            'doctor_id' => $this->doctor_id,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'epicrisis_text', $this->epicrisis_text]);

        return $dataProvider;
    }
}
