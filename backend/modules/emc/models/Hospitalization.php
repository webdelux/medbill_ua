<?php

namespace backend\modules\emc\models;

use Yii;

/**
 * This is the model class for table "{{%hospitalization}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $diagnosis_id
 * @property integer $department_id
 * @property integer $refferal_id
 * @property integer $hospitalization_type
 * @property string $hospitalization_date
 * @property string $diagnosis_in
 * @property integer $hospitalization_interval
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $moved_to
 * @property integer $cure_type
 * @property integer $cure_working
 * @property integer $cure_result
 * @property string $ill_days
 * @property integer $diagnosis_end_id
 * @property string $resume
 * @property string $hospitalization_date_out
 * @property integer $first_hospitalization
 *
 */
class Hospitalization extends \yii\db\ActiveRecord
{
    public $modelAlert;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%hospitalization}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'diagnosis_id', 'hospitalization_type', 'hospitalization_date', 'department_id'], 'required'],
            [['pacient_id', 'diagnosis_id', 'department_id', 'refferal_id', 'hospitalization_type', 'hospitalization_interval', 'user_id', 'updated_user_id', 'diagnosis_end_id', 'first_hospitalization'], 'integer'],
            [['modelAlert', 'hospitalization_date_out', 'hospitalization_date', 'created_at', 'updated_at', 'moved_to', 'cure_type', 'cure_working', 'cure_result', 'ill_days', 'resume'], 'safe'],
            [['diagnosis_in', 'description'], 'string', 'max' => 255],
            [['cure_type', 'cure_working', 'cure_result', 'ill_days', 'diagnosis_end_id'], 'required', 'on' => 'release' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'department_id' => Yii::t('app', 'Departments'),
            'refferal_id' => Yii::t('app', 'Refferal'),
            'hospitalization_type' => Yii::t('app', 'Type') . ' ' . Yii::t('app', 'hospitalization'),
            'hospitalization_date' => Yii::t('app', 'Date') . ' ' . Yii::t('app', 'hospitalization'),
            'hospitalization_date_out' => Yii::t('app', 'Date') . ' ' . Yii::t('app', 'discharge'),
            'diagnosis_in' => Yii::t('app', 'Diagnosis In'),
            'hospitalization_interval' => Yii::t('app', 'Hospitalization Interval'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'moved_to' => Yii::t('app', 'Moved') . ' ' . Yii::t('app', 'to'),
            'cure_type' => Yii::t('app', 'Cure type'),
            'cure_working' => Yii::t('app', 'Recovery'),
            'cure_result' => Yii::t('app', 'Cure result'),
            'ill_days' => Yii::t('app', 'Ill days'),
            'diagnosis_end_id' => Yii::t('app', 'Diagnosis out'),
            'resume' => Yii::t('app', 'Resume examination'),
            'first_hospitalization' => Yii::t('app', 'First time hospitalization'),
        ];
    }

    public function beforeValidate() {
        if(parent::beforeValidate()) {
            if ($this->hospitalization_date_out == 0)                          // Якщо госпіталізуємо нового пацієнта,
                if ($this->isTherePacient($this->pacient_id, $this->id))      // але його вже госпіталізовано, то
                    return false;                                             //  не проходимо валідацію

        }
        return TRUE;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function isTherePacient($pacient, $id)            //Перевіряємо, чи немає дубля пацієнта
    {
        $query = Hospitalization::find();

        if (isset($id)){
            $query->andWhere(['!=','id', $id]);            //крім вказаного (поточного) запису
        }

        $double = $query
                ->andWhere(['pacient_id' => $pacient])
                ->andWhere(['=','hospitalization_date_out', 0])
                ->one();

        if (isset($double)){
            Yii::$app->session->setFlash('danger', Yii::t('app', 'This pacient is placed already!'));
            $this->modelAlert = Yii::t('app', 'This pacient is placed already!');

            return true;
        }

        return false;                                  // немає дубля.

    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */
    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(\backend\models\Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(\backend\modules\handbook\models\Department::className(), ['id' => 'department_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public function getDiagnosisEnd()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_end_id']);
    }

    public function getRefferal()
    {
        return $this->hasOne(\backend\modules\handbook\models\Refferal::className(), ['id' => 'refferal_id']);
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'hospitalization_type' => [
                '1' => Yii::t('app', 'Immediately'),
                '2' => Yii::t('app', 'Illness'),
            ],
            'cure_type' => [                // проведене лікування - Спеціальне, паліативне, симптомічне
                '1' => Yii::t('app', 'Special'),
                '2' => Yii::t('app', 'Palliative'),
                '3' => Yii::t('app', 'Symptomic'),
            ],
            'cure_working' => [         // відновлення працездатності (відновлена повністю, знижена, тимчасово втрачена, стійко втрачена у зв’язку із захворюванням, стійко втрачена з інших причин);
                '1' => Yii::t('app', 'Fully restored'),
                '2' => Yii::t('app', 'Reduced'),
                '3' => Yii::t('app', 'Temporarily lost'),
                '4' => Yii::t('app', 'Steadily lost due to illness'),
                '5' => Yii::t('app', 'Steadily lost for other reasons'),
            ],
            'cure_result' => [          // результат лікування (виписаний(а) з одужанням – 1, поліпшенням – 2, погіршенням – 3, без змін – 4; помер (ла) – 5; переведений(а) в інший лікувальний заклад – 6; переведений(а) в інше відділення – 7; здоровий(а) – 8);
                '1' => Yii::t('app', 'Written(a) recovery'),
                '2' => Yii::t('app', 'Written(a) improving'),
                '3' => Yii::t('app', 'Written(a) deterioration'),
                '4' => Yii::t('app', 'Written(a) unchanged'),
                '5' => Yii::t('app', 'Died'),
                '6' => Yii::t('app', 'Transferred to another medical facility'),
                '7' => Yii::t('app', 'Transferred to another department'),
                '8' => Yii::t('app', 'A healthy'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * @inheritdoc
     * @return HospitalizationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HospitalizationQuery(get_called_class());
    }
}
