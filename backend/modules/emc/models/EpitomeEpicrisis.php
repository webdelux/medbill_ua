<?php

namespace backend\modules\emc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\User;

/**
 * This is the model class for table "{{%epitome_epicrisis}}".
 *
 * @property integer $id
 * @property integer $diagnosis_id
 * @property string $epicrisis_date
 * @property string $epicrisis_text
 * @property integer $doctor_id
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class EpitomeEpicrisis extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%epitome_epicrisis}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['diagnosis_id', 'doctor_id', 'user_id', 'updated_user_id'], 'integer'],
            [['epicrisis_date', 'created_at', 'updated_at'], 'safe'],
            [['diagnosis_id'], 'required'],
            [['epicrisis_text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'epicrisis_date' => Yii::t('app', 'Date'),
            'epicrisis_text' => Yii::t('app', 'Epitome epicrisis'),
            'doctor_id' => Yii::t('app', 'Doctor'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function()
        {
            return date('Y-m-d H:i:s');
        }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(User::className(), ['id' => 'doctor_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    /**
     * @inheritdoc
     * @return EpitomeEpicrisisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EpitomeEpicrisisQuery(get_called_class());
    }

}
