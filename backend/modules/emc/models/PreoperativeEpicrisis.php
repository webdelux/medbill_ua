<?php

namespace backend\modules\emc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\helpers\StringHelper;
use backend\models\User;

/**
 * This is the model class for table "{{%preoperative_epicrisis}}".
 *
 * @property integer $id
 * @property integer $diagnosis_id
 * @property string $concomitant_diseases
 * @property integer $surgery_id
 * @property string $contraindication
 * @property string $anesthesia
 * @property string $preoperative_preparation_time
 * @property string $preoperative_preparation
 * @property integer $doctor_id
 * @property string $start_operation_date
 * @property integer $surgeon_id
 * @property integer $anesthetist_id
 * @property string $operation_early_period
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class PreoperativeEpicrisis extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%preoperative_epicrisis}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['diagnosis_id', 'surgery_id', 'doctor_id', 'surgeon_id', 'anesthetist_id', 'user_id', 'updated_user_id'], 'integer'],
            [['preoperative_preparation_time', 'start_operation_date', 'created_at', 'updated_at'], 'safe'],
            [['doctor_id', 'surgeon_id', 'anesthetist_id', 'preoperative_preparation_time', 'start_operation_date', 'diagnosis_id', 'surgery_id'], 'required'],
            [['concomitant_diseases', 'contraindication', 'anesthesia', 'preoperative_preparation', 'operation_early_period'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'concomitant_diseases' => Yii::t('app', 'Concomitant') . ' ' . StringHelper::strToLower(Yii::t('app', 'Disease')),
            'surgery_id' => Yii::t('app', 'Operation'),
            'contraindication' => Yii::t('app', 'Contraindication'),
            'anesthesia' => Yii::t('app', 'Anesthesia'),
            'preoperative_preparation_time' => Yii::t('app', 'Preoperative preparation time'),
            'preoperative_preparation' => Yii::t('app', 'Preoperative preparation'),
            'doctor_id' => Yii::t('app', 'Doctor'),
            'start_operation_date' => Yii::t('app', 'Date') . ' ' . StringHelper::strToLower(Yii::t('app', 'Surgeries')),
            'surgeon_id' => Yii::t('app', 'Surgeon'),
            'anesthetist_id' => Yii::t('app', 'Anesthetist'),
            'operation_early_period' => Yii::t('app', 'Operation early period'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(User::className(), ['id' => 'doctor_id']);
    }

    public function getSurgeon()
    {
        return $this->hasOne(User::className(), ['id' => 'surgeon_id']);
    }

    public function getAnesthetist()
    {
        return $this->hasOne(User::className(), ['id' => 'anesthetist_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public function getSurgery()
    {
        return $this->hasOne(\backend\modules\handbookemc\models\Surgery::className(), ['id' => 'surgery_id']);
    }

    /**
     * @inheritdoc
     * @return PreoperativeEpicrisisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PreoperativeEpicrisisQuery(get_called_class());
    }

}