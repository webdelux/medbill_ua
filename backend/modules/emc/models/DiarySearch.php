<?php

namespace backend\modules\emc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\emc\models\Diary;

/**
 * DiarySearch represents the model behind the search form about `backend\modules\emc\models\Diary`.
 */
class DiarySearch extends Diary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'diagnosis_id', 'diary_number', 'user_id', 'updated_user_id'], 'integer'],
            [['diary_date', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'diagnosis_id' => $this->diagnosis_id,
            'diary_number' => $this->diary_number,
            'diary_date' => $this->diary_date,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
