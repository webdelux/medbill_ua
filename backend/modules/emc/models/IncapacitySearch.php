<?php

namespace backend\modules\emc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\emc\models\Incapacity;

/**
 * IncapacitySearch represents the model behind the search form about `backend\modules\emc\models\Incapacity`.
 */
class IncapacitySearch extends Incapacity
{
    public $fullName;
    public $doctor;
    public $diagnosis;
    public $user;
    public $updatedUser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'diagnosis_id', 'doctor_id', 'department_id', 'incapacity_type', 'user_id', 'updated_user_id'], 'integer'],
            [['icd', 'diagnosis', 'updatedUser', 'user', 'doctor', 'fullName', 'incapacity_number', 'incapacity_date', 'incapacity_date_start', 'incapacity_date_end', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Incapacity::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                '{{%incapacity}}.pacient_id',
                'fullName' => [
                   'asc' => ['{{%incapacity}}.pacient_id' => SORT_ASC],
                   'desc' => ['{{%incapacity}}.pacient_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'doctor_id',
                'doctor' => [
                   'asc' => ['doctor_id' => SORT_ASC],
                   'desc' => ['doctor_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'department_id',
                'incapacity_number',
                'incapacity_date',
                'incapacity_date_start',
                'incapacity_date_end',
                'diagnosis_id',
                'diagnosis' => [
                   'asc' => ['diagnosis_id' => SORT_ASC],
                   'desc' => ['diagnosis_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'incapacity_type',
                'description',
                'created_at',
                'updated_at',
                'user_id',
                'user' => [
                   'asc' => ['user_id' => SORT_ASC],
                   'desc' => ['user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updatedUser' => [
                   'asc' => ['updated_user_id' => SORT_ASC],
                   'desc' => ['updated_user_id' => SORT_DESC],
                   'default' => SORT_ASC
                ],
                'updated_user_id',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'diagnosis_id' => $this->diagnosis_id,
            'doctor_id' => $this->doctor_id,
            'department_id' => $this->department_id,
            'incapacity_type' => $this->incapacity_type,
            'incapacity_date' => $this->incapacity_date,
            'incapacity_date_start' => $this->incapacity_date_start,
            'incapacity_date_end' => $this->incapacity_date_end,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'icd' => $this->icd,
        ]);

        $query->andFilterWhere(['like', 'incapacity_number', $this->incapacity_number])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
