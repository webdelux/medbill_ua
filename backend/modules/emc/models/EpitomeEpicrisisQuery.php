<?php

namespace backend\modules\emc\models;

/**
 * This is the ActiveQuery class for [[EpitomeEpicrisis]].
 *
 * @see EpitomeEpicrisis
 */
class EpitomeEpicrisisQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return EpitomeEpicrisis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EpitomeEpicrisis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
