<?php

namespace backend\modules\emc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\emc\models\PreoperativeEpicrisis;

/**
 * PreoperativeEpicrisisSearch represents the model behind the search form about `backend\modules\emc\models\PreoperativeEpicrisis`.
 */
class PreoperativeEpicrisisSearch extends PreoperativeEpicrisis
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'diagnosis_id', 'surgery_id', 'doctor_id', 'surgeon_id', 'anesthetist_id', 'user_id', 'updated_user_id'], 'integer'],
            [['concomitant_diseases', 'contraindication', 'anesthesia', 'preoperative_preparation_time', 'preoperative_preparation', 'start_operation_date', 'operation_early_period', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PreoperativeEpicrisis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'diagnosis_id' => $this->diagnosis_id,
            'surgery_id' => $this->surgery_id,
            'preoperative_preparation_time' => $this->preoperative_preparation_time,
            'doctor_id' => $this->doctor_id,
            'start_operation_date' => $this->start_operation_date,
            'surgeon_id' => $this->surgeon_id,
            'anesthetist_id' => $this->anesthetist_id,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'concomitant_diseases', $this->concomitant_diseases])
                ->andFilterWhere(['like', 'contraindication', $this->contraindication])
                ->andFilterWhere(['like', 'anesthesia', $this->anesthesia])
                ->andFilterWhere(['like', 'preoperative_preparation', $this->preoperative_preparation])
                ->andFilterWhere(['like', 'operation_early_period', $this->operation_early_period]);

        return $dataProvider;
    }

}