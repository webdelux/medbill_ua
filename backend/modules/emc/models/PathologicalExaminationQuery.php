<?php

namespace backend\modules\emc\models;

/**
 * This is the ActiveQuery class for [[PathologicalExamination]].
 *
 * @see PathologicalExamination
 */
class PathologicalExaminationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PathologicalExamination[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PathologicalExamination|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
