<?php

namespace backend\modules\emc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\emc\models\Hospitalization;

/**
 * HospitalizationSearch represents the model behind the search form about `backend\modules\emc\models\Hospitalization`.
 */
class HospitalizationSearch extends Hospitalization
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'diagnosis_id', 'department_id', 'refferal_id', 'hospitalization_type', 'hospitalization_interval', 'user_id', 'updated_user_id'], 'integer'],
            [['hospitalization_date', 'diagnosis_in', 'description', 'created_at', 'updated_at', 'first_hospitalization'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hospitalization::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'diagnosis_id' => $this->diagnosis_id,
            'department_id' => $this->department_id,
            'refferal_id' => $this->refferal_id,
            'hospitalization_type' => $this->hospitalization_type,
            'hospitalization_date' => $this->hospitalization_date,
            'hospitalization_interval' => $this->hospitalization_interval,
            'first_hospitalization' =>$this->first_hospitalization,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'diagnosis_in', $this->diagnosis_in])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
