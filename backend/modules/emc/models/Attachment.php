<?php

namespace backend\modules\emc\models;

use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%attachment}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $diagnosis_id
 * @property integer $protocol_id
 * @property string $filename
 * @property string $path
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Attachment extends \yii\db\ActiveRecord
{

    public $pacientFullName;
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attachment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id'], 'required'],
            [['pacient_id', 'diagnosis_id', 'protocol_id', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['path', 'description', 'filename'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'protocol_id' => Yii::t('app', 'Protocol'),
            'filename' => Yii::t('app', 'Filename'),
            'imageFile' => Yii::t('app', 'Filename'),
            'path' => Yii::t('app', 'Path'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if (!$this->checkPath())        //Якщо не можемо створити директорію,
                return false;               // то прериваємо зберігання моделі

            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->created_at = date('Y-m-d H:i:s');
                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            $file = Yii::getAlias(Yii::$app->params['UPLOAD_DIR'] . $this->path .$this->filename);

            if (is_file($file))
                unlink($file);

            return true;
        } else {
            return false;
        }
    }

    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'updated_user_id']);
    }

    public function getUpdatedUserProfile()
    {
        return $this->hasOne(\backend\modules\user\models\Profile::className(), ['user_id' => 'updated_user_id']);
    }

    /* Гетер для Користувача */

    public function getUserName()
    {
        return (isset($this->user->username) ? $this->user->username : NULL );
    }

    public function getPacient()
    {
        return $this->hasOne(\backend\models\Pacient::className(), ['id' => 'pacient_id']);
    }

    public function getProtocol()
    {
        return $this->hasOne(\backend\models\Protocol::className(), ['id' => 'protocol_id']);
    }

    public function getDiagnosis()
    {
        return $this->hasOne(\backend\modules\diagnosis\models\Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    public function checkPath()
    {
        $this->path = 'attachment/' . date("Y") . '/' . date("m") . '/';

        return FileHelper::createDirectory(Yii::getAlias(Yii::$app->params['UPLOAD_DIR'].$this->path));

    }

    /**
     * @inheritdoc
     * @return AttachmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AttachmentQuery(get_called_class());
    }
}
