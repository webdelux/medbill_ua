<?php

namespace backend\modules\emc\models;

/**
 * This is the ActiveQuery class for [[PreoperativeEpicrisis]].
 *
 * @see PreoperativeEpicrisis
 */
class PreoperativeEpicrisisQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }
    */

    /**
     * @inheritdoc
     * @return PreoperativeEpicrisis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PreoperativeEpicrisis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}