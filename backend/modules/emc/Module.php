<?php

namespace backend\modules\emc;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\emc\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
