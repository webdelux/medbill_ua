<?php

namespace backend\modules\emc\controllers;

use Yii;
use backend\modules\emc\models\PathologicalExamination;
use backend\modules\emc\models\PathologicalExaminationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * PathologicalExaminationController implements the CRUD actions for PathologicalExamination model.
 */
class PathologicalExaminationController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PathologicalExamination models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PathologicalExaminationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PathologicalExamination model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PathologicalExamination model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PathologicalExamination();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PathologicalExamination model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $tab = NULL)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($tab)         // повернути в місце виклику
                    return $this->redirect(['/pacient/view', 'id' => $model->pacient_id, 'tab' => $tab]);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            if (Yii::$app->request->isAjax)
                return true;

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PathologicalExamination model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax)
            return true;

        return $this->redirect(['index']);
    }

    public function actionReport($id)
    {
        $model = PathologicalExamination::find()->joinWith(['additional'])->where(['pacient_id' => $id])->one();

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_report1', [
                'model' => $model,
            ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            //'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            //'cssInline' => 'table, th, td {border: 1px solid black; border-collapse: collapse; text-align: left;} ',
             // set mPDF properties on the fly
            //'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();

    }

    /**
     * Finds the PathologicalExamination model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PathologicalExamination the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PathologicalExamination::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
