<?php

namespace backend\modules\emc\controllers;

use Yii;
use backend\modules\emc\models\Hospitalization;
use backend\modules\emc\models\HospitalizationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use backend\modules\diagnosis\models\Diagnosis;

/**
 * HospitalizationController implements the CRUD actions for Hospitalization model.
 */
class HospitalizationController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hospitalization models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HospitalizationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hospitalization model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hospitalization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Hospitalization();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Hospitalization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($type = NULL, $id, $tab = NULL)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($tab)         // повернути в місце виклику
                    return $this->redirect(['/pacient/view', 'id' => $model->pacient_id, 'tab' => $tab]);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if ($type == 2){    // type = 2 виписати пацієнта
                $model->scenario = 'release';
                $model->hospitalization_date_out = date("Y-m-d H:i");

                return $this->render('update_out', [
                    'model' => $model,
                    'tab' => $tab,
                ]);
            }
            return $this->render('update', [
                'model' => $model,
                'tab' => $tab,
            ]);
        }
    }

    /**
     * Deletes an existing Hospitalization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $diagnosis = NULL)
    {
        if ($diagnosis){
            $this->loadModelDiagnosis($diagnosis)->delete();

        }

        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax)
            return true;

        return $this->redirect(['index']);
    }

    /**
     * Finds the Hospitalization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hospitalization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hospitalization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function loadModelDiagnosis($id)
    {
        if (($model = Diagnosis::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
