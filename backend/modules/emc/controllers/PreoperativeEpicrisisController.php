<?php

namespace backend\modules\emc\controllers;

use Yii;
use backend\modules\emc\models\PreoperativeEpicrisis;
use backend\modules\emc\models\PreoperativeEpicrisisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use backend\models\Pacient;
use kartik\mpdf\Pdf;

/**
 * PreoperativeEpicrisisController implements the CRUD actions for PreoperativeEpicrisis model.
 */
class PreoperativeEpicrisisController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PreoperativeEpicrisis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PreoperativeEpicrisisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PreoperativeEpicrisis model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $pacient = $this->findModelPacient($model->diagnosis->pacient_id);

        return $this->render('view', [
            'model' => $model,
            'pacient' => $pacient
        ]);
    }

    /**
     * Creates a new PreoperativeEpicrisis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PreoperativeEpicrisis();

        $pacient = $this->findModelPacient(Yii::$app->request->get('pacient_id'));

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
                'pacient' => $pacient
            ]);
        }
    }

    /**
     * Updates an existing PreoperativeEpicrisis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $pacient = $this->findModelPacient($model->diagnosis->pacient_id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
                'pacient' => $pacient
            ]);
        }
    }

    public function actionPrint($id)
    {
        $this->layout = 'layout';

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_print', [
            'model' => $this->findModel($id),
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            //'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            'marginTop' => 5,
            'marginBottom' => 5,
            'marginLeft' => 5,
            'marginRight' => 5,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => Yii::t('app', 'Preoperative epicrisis')],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader' => [Yii::t('app', 'Preoperative epicrisis')],
                //'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Deletes an existing PreoperativeEpicrisis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PreoperativeEpicrisis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PreoperativeEpicrisis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PreoperativeEpicrisis::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPacient($id)
    {
        if (($model = Pacient::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}