<?php

namespace backend\modules\emc\controllers;

use Yii;
use backend\modules\emc\models\EpitomeEpicrisis;
use backend\modules\emc\models\EpitomeEpicrisisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use backend\models\Pacient;
use kartik\mpdf\Pdf;

/**
 * EpitomeEpicrisisController implements the CRUD actions for EpitomeEpicrisis model.
 */
class EpitomeEpicrisisController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EpitomeEpicrisis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EpitomeEpicrisisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EpitomeEpicrisis model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $pacient = $this->findModelPacient($model->diagnosis->pacient_id);

        return $this->render('view', [
                    'model' => $model,
                    'pacient' => $pacient
        ]);
    }

    /**
     * Creates a new EpitomeEpicrisis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EpitomeEpicrisis();

        $pacient = $this->findModelPacient(Yii::$app->request->get('pacient_id'));

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['update', 'id' => $model->id, 'pacient_id' => $pacient->id]);
        }
        else
        {
            return $this->render('create', [
                        'model' => $model,
                        'pacient' => $pacient,
            ]);
        }
    }

    /**
     * Updates an existing EpitomeEpicrisis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // Вибираємо данні з ЕМК пацієнта
        $models = [
            'Pacient' => $this->findModelPacient(Yii::$app->request->get('pacient_id')),
//                    'Hospitalization' => \backend\modules\emc\models\Hospitalization::find()->where(['diagnosis_id' => $model->diagnosis_id])->orderBy('hospitalization_date DESC')->one(),
//                    'Protocol' => \backend\models\Protocol::find()->where(['diagnosis_id' => $model->diagnosis_id, 'status' => 2])->all(),
        ];

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
            return $this->render('update', [
                        'model' => $model,
                        'models' => $models,
            ]);
        }
    }

    /**
     * Deletes an existing EpitomeEpicrisis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EpitomeEpicrisis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EpitomeEpicrisis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EpitomeEpicrisis::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrint($id)
    {
        $this->layout = 'layout';

        $model = $this->findModel($id);

        $models = [
            'Pacient' => $this->findModelPacient($model->diagnosis->pacient_id),
            'Hospitalization' => \backend\modules\emc\models\Hospitalization::find()->where(['diagnosis_id' => $model->diagnosis_id])->orderBy('hospitalization_date DESC')->one(),
            'Diagnosis' => \backend\modules\diagnosis\models\Diagnosis::find()->where(['id' => $model->diagnosis_id])->one(),
            'Surgery' => \backend\models\PacientOperation::find()->where(['diagnosis_id' => $model->diagnosis_id])->all(),
            'Protocol' => \backend\models\Protocol::find()->where(['diagnosis_id' => $model->diagnosis_id, 'status' => 2])->all(),
        ];

        $signalmarks = [
            'PacientSignalmark' => \backend\modules\signalmark\models\PacientSignalmark::find()->where(['pacient_id' => $model->diagnosis->pacient_id])->one(),
            'PacientTransfusion' => \backend\modules\signalmark\models\PacientTransfusion::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientDiabetes' => \backend\modules\signalmark\models\PacientDiabetes::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientInfectious' => \backend\modules\signalmark\models\PacientInfectious::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientSurgery' => \backend\modules\signalmark\models\PacientSurgery::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientAllergic' => \backend\modules\signalmark\models\PacientAllergic::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientIntolerance' => \backend\modules\signalmark\models\PacientIntolerance::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientComplaints' => \backend\modules\signalmark\models\PacientComplaints::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientAnamnesis' => \backend\modules\signalmark\models\PacientAnamnesis::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientHypertension' => \backend\modules\signalmark\models\PacientHypertension::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
            'PacientOverall' => \backend\modules\signalmark\models\PacientOverall::findAll(['pacient_id' => $model->diagnosis->pacient_id]),
        ];

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_print', [
            'model' => $model,
            'models' => $models,
            'signalmarks' => $signalmarks,
        ]);
        $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; }';

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            //'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            'marginTop' => 5,
            'marginBottom' => 5,
            'marginLeft' => 5,
            'marginRight' => 5,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => $cssInline,
            // set mPDF properties on the fly
            'options' => ['title' => Yii::t('app', 'Epitome epicrisis')],
            // call mPDF methods on the fly
            'methods' => [
            //'SetHeader' => [Yii::t('app', 'Preoperative epicrisis')],
            //'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    protected function findModelPacient($id)
    {
        if (($model = Pacient::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
