<?php

namespace backend\modules\user\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use backend\modules\handbook\models\Speciality;
use backend\models\User;

/**
 * This is the model class for table "{{%profile_speciality}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $speciality_id
 * @property integer $created_user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Speciality $speciality
 * @property User $user
 */
class ProfileSpeciality extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_speciality}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'speciality_id'], 'required'],
            [['user_id', 'speciality_id', 'created_user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'speciality_id'], 'unique', 'targetAttribute' => ['user_id', 'speciality_id'],
                'message' => 'The combination of User ID and Speciality ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'speciality_id' => Yii::t('speciality', 'Speciality'),
            'created_user_id' => Yii::t('app', 'Created User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeciality()
    {
        return $this->hasOne(Speciality::className(), ['id' => 'speciality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ProfileSpecialityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProfileSpecialityQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
                $this->created_user_id = Yii::$app->user->id;
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

}