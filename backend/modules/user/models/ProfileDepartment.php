<?php

namespace backend\modules\user\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use backend\models\User;
use backend\modules\handbook\models\Department;
use backend\modules\user\models\ProfileSchedule;

/**
 * This is the model class for table "{{%profile_department}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $department_id
 * @property string $working_days
 * @property string $working_time_mon_from
 * @property string $working_time_mon_to
 * @property string $working_time_tue_from
 * @property string $working_time_tue_to
 * @property string $working_time_wed_from
 * @property string $working_time_wed_to
 * @property string $working_time_thu_from
 * @property string $working_time_thu_to
 * @property string $working_time_fri_from
 * @property string $working_time_fri_to
 * @property string $working_time_sat_from
 * @property string $working_time_sat_to
 * @property string $working_time_sun_from
 * @property string $working_time_sun_to
 * @property integer $created_user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Department $department
 * @property User $user
 * @property ProfileSchedule[] $profileSchedules
 */
class ProfileDepartment extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'department_id'], 'required'],
            [['user_id', 'department_id', 'created_user_id', 'updated_user_id'], 'integer'],
            [['working_time_mon_from', 'working_time_mon_to', 'working_time_tue_from', 'working_time_tue_to', 'working_time_wed_from', 'working_time_wed_to', 'working_time_thu_from', 'working_time_thu_to', 'working_time_fri_from', 'working_time_fri_to', 'working_time_sat_from', 'working_time_sat_to', 'working_time_sun_from', 'working_time_sun_to', 'created_at', 'updated_at'], 'safe'],
            [['working_days'], 'string', 'max' => 255],
            [['user_id', 'department_id'], 'unique',
                'targetAttribute' => ['user_id', 'department_id'],
                'message' => 'The combination of User ID and Department ID has already been taken.'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(),
                'targetAttribute' => ['department_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'department_id' => Yii::t('app', 'Departments'),
            'working_days' => Yii::t('app', 'Working Days'),
            'working_time_mon_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'mon.'),
            'working_time_mon_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'mon.'),
            'working_time_tue_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'tue.'),
            'working_time_tue_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'tue.'),
            'working_time_wed_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'wed.'),
            'working_time_wed_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'wed.'),
            'working_time_thu_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'thu.'),
            'working_time_thu_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'thu.'),
            'working_time_fri_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'fri.'),
            'working_time_fri_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'fri.'),
            'working_time_sat_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'sat.'),
            'working_time_sat_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'sat.'),
            'working_time_sun_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'sun.'),
            'working_time_sun_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'sun.'),
            'created_user_id' => Yii::t('app', 'Created User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileSchedules()
    {
        return $this->hasMany(ProfileSchedule::className(), ['profile_department_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ProfileDepartmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProfileDepartmentQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
                $this->created_user_id = Yii::$app->user->id;
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'working_days' => [
                'mon' => Yii::t('app', 'Monday'),
                'tue' => Yii::t('app', 'Tuesday'),
                'wed' => Yii::t('app', 'Wednesday'),
                'thu' => Yii::t('app', 'Thursday'),
                'fri' => Yii::t('app', 'Friday'),
                'sat' => Yii::t('app', 'Saturday'),
                'sun' => Yii::t('app', 'Sunday')
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            ProfileSchedule::deleteAll(['profile_department_id' => $this->id, 'user_id' => $this->user_id]);

            return true;
        }
        else
        {
            return false;
        }
    }

}