<?php

namespace backend\modules\user\models;

/**
 * This is the ActiveQuery class for [[ProfileSchedule]].
 *
 * @see ProfileSchedule
 */
class ProfileScheduleQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }
    */

    /**
     * @inheritdoc
     * @return ProfileSchedule[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProfileSchedule|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}