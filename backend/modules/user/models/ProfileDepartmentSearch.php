<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\ProfileDepartment;

/**
 * ProfileDepartmentSearch represents the model behind the search form about `backend\modules\user\models\ProfileDepartment`.
 */
class ProfileDepartmentSearch extends ProfileDepartment
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'department_id', 'created_user_id', 'updated_user_id'], 'integer'],
            [['working_days', 'working_time_mon_from', 'working_time_mon_to', 'working_time_tue_from', 'working_time_tue_to', 'working_time_wed_from', 'working_time_wed_to', 'working_time_thu_from', 'working_time_thu_to', 'working_time_fri_from', 'working_time_fri_to', 'working_time_sat_from', 'working_time_sat_to', 'working_time_sun_from', 'working_time_sun_to', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProfileDepartment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'department_id' => $this->department_id,
            'working_time_mon_from' => $this->working_time_mon_from,
            'working_time_mon_to' => $this->working_time_mon_to,
            'working_time_tue_from' => $this->working_time_tue_from,
            'working_time_tue_to' => $this->working_time_tue_to,
            'working_time_wed_from' => $this->working_time_wed_from,
            'working_time_wed_to' => $this->working_time_wed_to,
            'working_time_thu_from' => $this->working_time_thu_from,
            'working_time_thu_to' => $this->working_time_thu_to,
            'working_time_fri_from' => $this->working_time_fri_from,
            'working_time_fri_to' => $this->working_time_fri_to,
            'working_time_sat_from' => $this->working_time_sat_from,
            'working_time_sat_to' => $this->working_time_sat_to,
            'working_time_sun_from' => $this->working_time_sun_from,
            'working_time_sun_to' => $this->working_time_sun_to,
            'created_user_id' => $this->created_user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'working_days', $this->working_days]);

        return $dataProvider;
    }

}