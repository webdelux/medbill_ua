<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace backend\modules\user\models;

use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;


use dektrium\user\models\LoginForm as BaseLoginForm;

class LoginForm extends BaseLoginForm {
/**
 * LoginForm get user's login and password, validates them and logs the user in. If user has been blocked, it adds
 * an error to login form.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */

    /** @var string User's email or username */
    public $login;

    /** @var string User's plain password */
    public $password;

    /** @var string Whether to remember the user */
    public $rememberMe = false;

    /** @var \dektrium\user\models\User */
    protected $user;

    /** @var \dektrium\user\Module */
    //protected $module;

    public $captcha;

      /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'login'      => Yii::t('user', 'Email or Phone number'),
            'password'   => Yii::t('user', 'Password'),
            'rememberMe' => Yii::t('user', 'Remember me next time'),
        ];
    }

    /** @inheritdoc */
    public function rules()
    {
        $rules= [
                    'requiredFields' => [['login', 'password'], 'required'],
                    'loginTrim' => ['login', 'trim'],
                    'passwordValidate' => [
                        'password',
                        function ($attribute) {
                            if ($this->user === null || !Password::validate($this->password, $this->user->password_hash)) {
                                $this->addError($attribute, Yii::t('user', 'Invalid phone or password'));
                            }
                        }
                    ],
                    'confirmationValidate' => [
                        'login',
                        function ($attribute) {
                            if ($this->user !== null) {
                                $confirmationRequired = $this->module->enableConfirmation && !$this->module->enableUnconfirmedLogin;
                                if ($confirmationRequired && !$this->user->getIsConfirmed()) {
                                    $this->addError($attribute, Yii::t('user', 'You need to confirm your email'));
                                }
                                if ($this->user->getIsBlocked()) {
                                    $this->addError($attribute, Yii::t('user', 'Your account has been blocked'));
                                }
                            }
                        }
                    ],
                    'rememberMe' => ['rememberMe', 'boolean'],
                ];

        // Add captcha if more than n times wrong input.
        if (Yii::$app->session->get('mycount') > Yii::$app->session->get('mycount_try'))
            {
                $rules['captchaRequired'] =  ['captcha', 'required'];
                $rules['captchaPattern'] =  ['captcha', 'captcha'];
            }
        return $rules;
    }

    /** @inheritdoc */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->user = $this->findUserByUserPhoneOrEmail($this->login);

            return true;
        } else {
            return false;
        }
    }

     /**
     * Finds a user by the given username or email.
     *
     * @param string $usernameOrEmail Username or email to be used on search.
     *
     * @return models\User
     */
    public function findUserByUserPhoneOrEmail($userPhoneOrEmail)
    {
        if (filter_var($userPhoneOrEmail, FILTER_VALIDATE_EMAIL)) {

             return $this->findUser(['email' => $userPhoneOrEmail])->one();
        }

        return $this->findUser(['phone' => $userPhoneOrEmail])->one();
    }

     public function findUser($condition)
    {
        return Users::find()->where($condition);
    }

}
