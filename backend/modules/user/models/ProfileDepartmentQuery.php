<?php

namespace backend\modules\user\models;

/**
 * This is the ActiveQuery class for [[ProfileDepartment]].
 *
 * @see ProfileDepartment
 */
class ProfileDepartmentQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }
    */

    /**
     * @inheritdoc
     * @return ProfileDepartment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProfileDepartment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}