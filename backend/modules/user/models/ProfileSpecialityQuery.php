<?php

namespace backend\modules\user\models;

/**
 * This is the ActiveQuery class for [[ProfileSpeciality]].
 *
 * @see ProfileSpeciality
 */
class ProfileSpecialityQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return ProfileSpeciality[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProfileSpeciality|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}