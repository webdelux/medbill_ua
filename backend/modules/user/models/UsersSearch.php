<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\user\models;

use Yii;
use dektrium\user\Finder;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use dektrium\user\models\UserSearch as BaseUserSearch;

/**
 * UserSearch represents the model behind the search form about User.
 */
class UsersSearch extends BaseUserSearch
{

    /** @var int */
    public $id;

    /** @var string */
    public $username;

    /** @var string */
    public $email;

    /** @var string */
    public $phone;

    /** @var int */
    public $created_at;

    /** @var string */
    public $registration_ip;

    public $profileName;

    /** @var Finder */
    protected $finder;

    /**
     * @param Finder $finder
     * @param array  $config
     */

    /** @inheritdoc */
    public function rules()
    {
        return [
            'fieldsSafe' => [['id', 'username', 'phone', 'email', 'registration_ip', 'created_at'], 'safe'],
            'createdDefault' => ['created_at', 'default', 'value' => null],
            [['profileName'], 'safe'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Username'),
            'email' => Yii::t('user', 'Email'),
            'phone' => Yii::t('user', 'Phone'),
            'created_at' => Yii::t('user', 'Registration time'),
            'registration_ip' => Yii::t('user', 'Registration ip'),
        ];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->finder->getUserQuery();
        $query->joinWith('profile');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['profileName'] = [
            'asc' => ['{{%profile}}.name' => SORT_ASC],
            'desc' => ['{{%profile}}.name' => SORT_DESC]
        ];

        if (!($this->load($params) && $this->validate()))
        {
            return $dataProvider;
        }

        if ($this->created_at !== null)
        {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['id' => $this->id])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'phone', $this->phone])
                ->andFilterWhere(['registration_ip' => $this->registration_ip])
                ->andFilterWhere(['like', '{{%profile}}.name', $this->profileName]);

        return $dataProvider;
    }

}