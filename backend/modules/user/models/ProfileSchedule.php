<?php

namespace backend\modules\user\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use backend\models\User;
use backend\modules\user\models\ProfileDepartment;

/**
 * This is the model class for table "{{%profile_schedule}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $profile_department_id
 * @property integer $type_id
 * @property string $date_from
 * @property string $date_to
 * @property integer $created_user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProfileDepartment $profileDepartment
 * @property User $user
 */
class ProfileSchedule extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_schedule}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type_id', 'date_from', 'date_to'], 'required'],
            [['user_id', 'profile_department_id', 'type_id', 'created_user_id', 'updated_user_id'], 'integer'],
            [['date_from', 'date_to', 'created_at', 'updated_at'], 'safe'],
            [['profile_department_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProfileDepartment::className(), 'targetAttribute' => ['profile_department_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'profile_department_id' => Yii::t('app', 'Departments'),
            'type_id' => Yii::t('app', 'Type'),
            'date_from' => Yii::t('app', 'From time'),
            'date_to' => Yii::t('app', 'To time'),
            'created_user_id' => Yii::t('app', 'Created User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileDepartment()
    {
        return $this->hasOne(ProfileDepartment::className(), ['id' => 'profile_department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ProfileScheduleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProfileScheduleQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->created_user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'type_id' => [
                '1' => Yii::t('user', 'Vacation'),
                '2' => Yii::t('user', 'Sick leave'),
                '3' => Yii::t('user', 'Business trip')
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}