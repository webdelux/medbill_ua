<?php

namespace backend\modules\user\models;

use Yii;

use backend\modules\user\models\Profile;
use backend\modules\user\models\ProfileSchedule;
use backend\modules\user\models\ProfileSpeciality;
use backend\modules\user\models\ProfileDepartment;
use backend\models\Pacient;

class Users extends \dektrium\user\models\User
{

    public static $phoneRegexp = '/^\+[0-9]{12}+$/';

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('app', 'ID'),
            'username'          => Yii::t('user', 'Username'),
            'email'             => Yii::t('user', 'Email'),
            'phone'             => Yii::t('user', 'Phone'),
            'registration_ip'   => Yii::t('user', 'Registration ip'),
            'unconfirmed_email' => Yii::t('user', 'New email'),
            'password'          => Yii::t('user', 'Password'),
            'created_at'        => Yii::t('user', 'Registration time'),
            'confirmed_at'      => Yii::t('user', 'Confirmation time'),
        ];
    }

    /** @inheritdoc */
    public function scenarios()
    {
        return [
            'register' => ['username', 'phone', 'email', 'password'],
            'connect'  => ['phone', 'email'],
            'create'   => ['username', 'phone', 'email', 'password'],
            'update'   => ['username', 'phone', 'email', 'password'],
            'settings' => ['username', 'phone', 'email', 'password'],
        ];
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
             // username rules
            'usernameRequired' => ['username', 'required', 'on' => ['register', 'create', 'connect', 'update']],
            'usernameMatch'    => ['username', 'match', 'pattern' => static::$usernameRegexp],
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            'usernameUnique'   => ['username', 'unique', 'message' => Yii::t('user', 'This username has already been taken')],
            'usernameTrim'     => ['username', 'trim'],

            // phone rules
            'phoneRequired' => ['phone', 'required', 'on' => ['register', 'create', 'connect', 'update']],
            'phoneMatch'    => ['phone', 'match', 'pattern' => static::$phoneRegexp],
            'phoneLength'   => ['phone', 'string', 'length' => 13],
            'phoneUnique'   => ['phone', 'unique', 'message' => Yii::t('user', 'This phone has already been taken')],
            'phoneTrim'     => ['phone', 'trim'],

            // email rules
            'emailRequired' => ['email', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            'emailPattern'  => ['email', 'email'],
            'emailLength'   => ['email', 'string', 'max' => 255],
            'emailUnique'   => ['email', 'unique', 'message' => Yii::t('user', 'This email address has already been taken')],
            'emailTrim'     => ['email', 'trim'],

            // password rules
            'passwordRequired' => ['password', 'required', 'on' => ['register']],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'on' => ['register', 'create']],
        ];
    }

    public function getIsAdmin()
    {
        if (in_array($this->username, $this->module->admins))
            return true;

        $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
        foreach ($roles as $value)
            if ($value->name == \Yii::$app->params['roleAdmin'])
                return true;

        return false;
    }

    public function relationsDeleteAll($id)
    {
        Profile::deleteAll(['user_id' => $id]);
        ProfileDepartment::deleteAll(['user_id' => $id]);
        ProfileSchedule::deleteAll(['user_id' => $id]);
        ProfileSpeciality::deleteAll(['user_id' => $id]);
        ProfileSpeciality::deleteAll(['user_id' => $id]);
        Pacient::deleteAll(['user_id' => $id]);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            $this->relationsDeleteAll($this->id);

            return true;
        }
        else
        {
            return false;
        }
    }

}