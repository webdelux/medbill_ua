<?php

namespace backend\modules\user\models;

use Yii;
use dektrium\user\models\Profile as BaseProfile;
use backend\models\User;
use backend\modules\handbook\models\City;
use backend\modules\handbook\models\Status;
use backend\modules\user\models\ProfileDepartment;

use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\helpers\Json;
use Imagine\Image\Box;
use Imagine\Image\Point;

class Profile extends BaseProfile
{

    public $cityAddress;
    public $image;
    public $crop_info;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'bioString' => ['bio', 'string'],
            'publicEmailPattern' => ['public_email', 'email'],
            'gravatarEmailPattern' => ['gravatar_email', 'email'],
            'websiteUrl' => ['website', 'url'],
            'nameLength' => ['name', 'string', 'max' => 255],
            'publicEmailLength' => ['public_email', 'string', 'max' => 255],
            'gravatarEmailLength' => ['gravatar_email', 'string', 'max' => 255],
            'locationLength' => ['location', 'string', 'max' => 255],
            'websiteLength' => ['website', 'string', 'max' => 255],
            [['payment', 'note', 'avatar_url'], 'string', 'max' => 255],
            [['city_id', 'status_id', 'mark_id'], 'integer'],
            [['working_days'], 'safe'],
            [[
                'working_time_mon_from',
                'working_time_mon_to',
                'working_time_tue_from',
                'working_time_tue_to',
                'working_time_wed_from',
                'working_time_wed_to',
                'working_time_thu_from',
                'working_time_thu_to',
                'working_time_fri_from',
                'working_time_fri_to',
                'working_time_sat_from',
                'working_time_sat_to',
                'working_time_sun_from',
                'working_time_sun_to'
            ], 'date', 'format' => 'HH:mm:ss'],

            //[['imageFile'], 'file', 'extensions' => 'png, jpg', 'maxSize' => 512000],
            [
                'image',
                'image',
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'mimeTypes' => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'],
                'maxSize' => 512000,
            ],
            ['crop_info', 'safe'],

        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('user', 'Full name'),
            'public_email' => Yii::t('user', 'Email (public)'),
            'gravatar_email' => Yii::t('user', 'Gravatar email'),
            'location' => Yii::t('user', 'Address'),
            'website' => Yii::t('user', 'Website'),
            'bio' => Yii::t('user', 'Bio'),
            'payment' => Yii::t('user', 'Payment data'),
            'note' => Yii::t('user', 'Note'),
            'city_id' => Yii::t('user', 'City'),
            'status_id' => Yii::t('user', 'Status'),
            'mark_id' => Yii::t('user', 'Mark'),
            'working_days' => Yii::t('app', 'Working Days'),
            'working_time_mon_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'mon.'),
            'working_time_mon_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'mon.'),
            'working_time_tue_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'tue.'),
            'working_time_tue_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'tue.'),
            'working_time_wed_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'wed.'),
            'working_time_wed_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'wed.'),
            'working_time_thu_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'thu.'),
            'working_time_thu_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'thu.'),
            'working_time_fri_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'fri.'),
            'working_time_fri_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'fri.'),
            'working_time_sat_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'sat.'),
            'working_time_sat_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'sat.'),
            'working_time_sun_from' => Yii::t('app', 'From time') . ' ' . Yii::t('app', 'sun.'),
            'working_time_sun_to' => Yii::t('app', 'To time') . ' ' . Yii::t('app', 'sun.'),

            'cityAddress' => Yii::t('user', 'City'),
            'avatar_url' => Yii::t('app', 'Avatar'),
        ];
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if (isset($this->working_days) && is_array($this->working_days))
                $this->working_days = implode(',', $this->working_days);

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getProfileDepartments()
    {
        return $this->hasMany(ProfileDepartment::className(), ['user_id' => 'user_id']);
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'mark_id' => [
                '1' => Yii::t('user', 'New'),
                '2' => Yii::t('user', 'Approved')
            ],
            'working_days' => [
                'mon' => Yii::t('app', 'Monday'),
                'tue' => Yii::t('app', 'Tuesday'),
                'wed' => Yii::t('app', 'Wednesday'),
                'thu' => Yii::t('app', 'Thursday'),
                'fri' => Yii::t('app', 'Friday'),
                'sat' => Yii::t('app', 'Saturday'),
                'sun' => Yii::t('app', 'Sunday')
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

//    public function upload()
//    {
//        if ($this->validate()) {
//
//            $this->avatar_url = 'user/' . $this->user_id . '/';
//
//            if (FileHelper::createDirectory(Yii::getAlias(Yii::$app->params['UPLOAD_DIR'] . $this->avatar_url)))
//                $this->imageFile->saveAs(Yii::getAlias(Yii::$app->params['UPLOAD_DIR'] . $this->avatar_url . $this->imageFile->baseName . '.' . $this->imageFile->extension));
//            return true;
//
//        } else {
//            return false;
//        }
//    }

    public function upload()
    {
        $this->avatar_url = 'user/';

        $image_path = '@webroot/images/user/';
        // open image
        $image = Image::getImagine()->open($this->image->tempName);

        if ($this->crop_info == '[]')
            return FALSE;

        // rendering information about crop of ONE option
        $cropInfo = Json::decode($this->crop_info)[0];
        $cropInfo['dWidth'] = (int)$cropInfo['dWidth']; //new width image
        $cropInfo['dHeight'] = (int)$cropInfo['dHeight']; //new height image
        $cropInfo['x'] = $cropInfo['x']; //begin position of frame crop by X
        $cropInfo['y'] = $cropInfo['y']; //begin position of frame crop by Y
        // Properties bolow we don't use in this example
        //$cropInfo['ratio'] = $cropInfo['ratio'] == 0 ? 1.0 : (float)$cropInfo['ratio']; //ratio image.
        //$cropInfo['width'] = (int)$cropInfo['width']; //width of cropped image
        //$cropInfo['height'] = (int)$cropInfo['height']; //height of cropped image
        //$cropInfo['sWidth'] = (int)$cropInfo['sWidth']; //width of source image
        //$cropInfo['sHeight'] = (int)$cropInfo['sHeight']; //height of source image

        //delete old images
        $oldImages = FileHelper::findFiles(Yii::getAlias($image_path), [
            'only' => [
                $this->user_id . '.*',
                'thumb_' . $this->user_id . '.*',
            ],
        ]);
        for ($i = 0; $i != count($oldImages); $i++) {
            @unlink($oldImages[$i]);
        }

        //saving thumbnail
        $newSizeThumb = new Box($cropInfo['dWidth'], $cropInfo['dHeight']);
        $cropSizeThumb = new Box(200, 200); //frame size of crop
        $cropPointThumb = new Point($cropInfo['x'], $cropInfo['y']);
        $pathThumbImage = Yii::getAlias($image_path)
            . '/thumb_'
            . $this->user_id
            . '.'
            . $this->image->getExtension();

        if ($image->resize($newSizeThumb)
            ->crop($cropPointThumb, $cropSizeThumb)
            ->save($pathThumbImage, ['quality' => 100]))
        {
            return TRUE;
        }

//        //saving original
//        if ($this->image->saveAs(
//                Yii::getAlias($image_path)
//                . '/'
//                . $this->user_id
//                . '.'
//                . $this->image->getExtension()
//            ))
//        {
//            return TRUE;
//        }
    }

}