<?php

namespace backend\modules\user\controllers;

use Yii;
use dektrium\user\models\LoginForm;

class SecurityController extends \dektrium\user\controllers\SecurityController
{

    public function actionLogin()
    {
        $this->layout = "/main";

        if (!Yii::$app->user->isGuest)
        {
            $this->goHome();
        }

        $model = Yii::createObject(LoginForm::className());

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login())
        {
            return $this->goBack();
        }

        Yii::$app->session->set('mycount_try', 5);

        if (!(Yii::$app->session->get('mycount')))
            Yii::$app->session->set('mycount', 1);

        if ($model->load(Yii::$app->request->post()) && !$model->validate())
        {
            Yii::$app->session->set('mycount', Yii::$app->session->get('mycount') + 1);
        }

        return $this->render('login', [
            'model' => $model,
            'module' => $this->module,
        ]);
    }

}