<?php

namespace backend\modules\handbookemc;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\handbookemc\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}