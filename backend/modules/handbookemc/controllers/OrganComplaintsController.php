<?php

namespace backend\modules\handbookemc\controllers;

use Yii;
use backend\modules\handbookemc\models\Organ;
use backend\modules\handbookemc\models\OrganComplaints;
use backend\modules\handbookemc\models\OrganComplaintsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * OrganComplaintsController implements the CRUD actions for OrganComplaints model.
 */
class OrganComplaintsController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrganComplaints models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isAjax)
        {
            $searchModel = new OrganComplaintsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents)
            {
                $out = [];

                $organ = Organ::findOne($parents[0]);

                $data = OrganComplaints::find()->where(['handbook_emc_organ_id' => $organ->root->id])->all();
                foreach ($data as $value)
                    $out[] = ['id' => $value->id, 'name' => $value->name];
            }
            else
                $out = '';

            return ['output' => $out, 'selected' => ''];
        }
    }

    /**
     * Displays a single OrganComplaints model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrganComplaints model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrganComplaints();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OrganComplaints model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OrganComplaints model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax)
            return true;

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrganComplaints model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrganComplaints the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrganComplaints::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}