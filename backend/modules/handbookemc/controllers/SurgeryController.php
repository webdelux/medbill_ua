<?php

namespace backend\modules\handbookemc\controllers;

use Yii;
use backend\modules\handbookemc\models\Surgery;
use backend\modules\handbookemc\models\SurgerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SurgeryController implements the CRUD actions for Surgery model.
 */
class SurgeryController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Surgery models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isAjax)
        {
            $searchModel = new SurgerySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $term = Yii::$app->request->get('term', null);

            $out = [];

            if (!is_null($term))
            {
                $data = Surgery::find()->where(['LIKE', 'name', trim($term)])->limit(10)->all();

                $out = [
                    'results' => []
                ];

                foreach ($data as $value)
                {
                    $out['results'][] = [
                        'id' => $value->id,
                        'text' => $value->name
                    ];
                }
            }

            return $out;
        }
    }

    /**
     * Displays a single Surgery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Surgery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Surgery();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if (!$model->parent_id)
            {
                // Створення кореневий вузол
                if ($model->makeRoot()->save())
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
            }
            else
            {
                // Перевіряємо чи є кореневий вузол
                $root = Surgery::findOne(['id' => $model->parent_id]);
                if ($root)
                {
                    // Inserting new node
                    if ($model->appendTo($root)->save())
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                }
            }

            return $this->refresh();
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Surgery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            // Перевіряємо щоб ієрархії небула "сам на себе"
            if ($model->id != $model->parent_id)
            {
                // Перевіряємо чи є кореневий вузол
                $root = Surgery::findOne(['id' => $model->parent_id]);
                if ($root)
                {
                    // Move existing node
                    if ($model->appendTo($root)->save())
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                }
                else
                {
                    if ($model->save())
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                }
            }

            return $this->refresh();
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Surgery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        // Delete node, children come up to the parent
        //$model->delete();

        // Delete node and all descendants
        $model->deleteWithChildren();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Surgery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Surgery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Surgery::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}