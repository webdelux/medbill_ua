<?php

namespace backend\modules\handbookemc\controllers;

use Yii;
use backend\modules\handbookemc\models\Protocol;
use backend\modules\handbookemc\models\ProtocolSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProtocolController implements the CRUD actions for Protocol model.
 */
class ProtocolController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Protocol models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $term = Yii::$app->request->get('term');
            $results = Yii::$app->request->get('results', null);
            $category = Yii::$app->request->get('category', null);

            $out = [];

            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents)
            {
                $data = Protocol::find()->where(['handbook_emc_category_id' => $parents[0]])->active()->all();
                foreach ($data as $value)
                    $out[] = ['id' => $value->id, 'name' => $value->name];

                return ['output' => $out, 'selected' => ''];
            }

            $query = Protocol::find()->with('category')->where(['LIKE', 'name', trim($term)]);

            if ($category)
                $query->andFilterWhere(['=', 'handbook_emc_category_id', $category]);

            $protocol = $query->limit(10)->active()->asArray()->all();

            foreach ($protocol as $value)
            {
                $out[] = [
                    'id' => $value['id'],
                    'label' => $value['name'],
                    'text' => $value['name'] . ' - (' . $value['category']['name'] . ')',
                    'value' => $value['name'],
                    'amount' => $value['amount'],
                ];
            }

            if (!is_null($results))
                $out = ['results' => $out];

            return $out;
        }
        else
        {
            $searchModel = new ProtocolSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Protocol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Protocol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Protocol();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Protocol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Protocol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Protocol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Protocol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Protocol::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}