<?php

namespace backend\modules\handbookemc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use dektrium\user\models\User;
use backend\modules\handbook\models\Status;
use backend\modules\handbookemc\models\Category;
use backend\modules\handbookemc\models\Template;

/**
 * This is the model class for table "{{%handbook_emc_protocol}}".
 *
 * @property integer $id
 * @property integer $handbook_emc_category_id
 * @property integer $handbook_emc_template_id
 * @property string $name
 * @property string $abbreviation
 * @property string $description
 * @property string $amount
 * @property integer $execution
 * @property integer $required
 * @property integer $status_id
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $code
 *
 * @property HandbookEmcTemplate $handbookEmcTemplate
 * @property HandbookEmcCategory $handbookEmcCategory
 */
class Protocol extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_emc_protocol}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['handbook_emc_category_id', 'handbook_emc_template_id', 'name', 'code'], 'required'],
            [['handbook_emc_category_id', 'handbook_emc_template_id', 'execution', 'required', 'status_id', 'user_id', 'updated_user_id'], 'integer'],
            [['description'], 'string'],
            [['amount'], 'number'],
            [['created_at', 'updated_at', 'code'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['abbreviation'], 'string', 'max' => 128],
            [['amount', 'execution', 'required'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'handbook_emc_category_id' => Yii::t('app', 'Category'),
            'handbook_emc_template_id' => Yii::t('app', 'Template'),
            'name' => Yii::t('app', 'Name'),
            'abbreviation' => Yii::t('app', 'Abbreviation'),
            'description' => Yii::t('app', 'Description'),
            'amount' => Yii::t('app', 'Price'),
            'execution' => Yii::t('app', 'Execution') . ', ' . Yii::t('app', 'min.'),
            'required' => Yii::t('app', 'Required'),
            'status_id' => Yii::t('app', 'Status'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'code' => Yii::t('app', 'Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'handbook_emc_template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'handbook_emc_category_id']);
    }

    /**
     * @inheritdoc
     * @return ProtocolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProtocolQuery(get_called_class());
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'required' => [
                '1' => Yii::t('app', 'Yes'),
                '0' => Yii::t('app', 'No')
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}