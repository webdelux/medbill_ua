<?php

namespace backend\modules\handbookemc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbookemc\models\Category;
use backend\modules\handbook\models\Department;
use backend\models\User;

/**
 * This is the model class for table "{{%handbook_emc_category_department}}".
 *
 * @property integer $id
 * @property integer $handbook_emc_category_id
 * @property integer $department_id
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Department $department
 * @property HandbookEmcCategory $handbookEmcCategory
 */
class CategoryDepartment extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_emc_category_department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['handbook_emc_category_id', 'department_id'], 'required'],
            [['handbook_emc_category_id', 'department_id', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['handbook_emc_category_id', 'department_id'], 'unique', 'targetAttribute' => ['handbook_emc_category_id', 'department_id'], 'message' => 'The combination of Handbook Emc Category ID and Department ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'handbook_emc_category_id' => Yii::t('app', 'Category'),
            'department_id' => Yii::t('app', 'Department'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'handbook_emc_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    /**
     * @inheritdoc
     * @return CategoryDepartmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryDepartmentQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

}