<?php

namespace backend\modules\handbookemc\models;

use Yii;
use paulzi\adjacencylist\AdjacencyListBehavior;
use dektrium\user\models\User;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\handbookemc\models\CategoryDepartment;

/**
 * This is the model class for table "{{%handbook_emc_category}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $name
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $code
 * @property integer $status
 */
class Category extends \yii\db\ActiveRecord
{

    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;

    public $department = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_emc_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['parent_id', 'sort', 'user_id', 'updated_user_id', 'status'], 'integer'],
            [['department', 'created_at', 'updated_at', 'code'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent category'),
            'sort' => Yii::t('app', 'Sort'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'department' => Yii::t('app', 'Departments'),
            'code' => Yii::t('app', 'Code'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function getDepartments()
    {
        return $this->hasMany(CategoryDepartment::className(), ['handbook_emc_category_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => AdjacencyListBehavior::className(),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        CategoryDepartment::deleteAll(['handbook_emc_category_id' => $this->id]);
        if (is_array($this->department))
        {
            foreach ($this->department as $value)
            {
                $department = new CategoryDepartment();
                $department->handbook_emc_category_id = $this->id;
                $department->department_id = $value;
                $department->save();
            }
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            CategoryDepartment::deleteAll(['handbook_emc_category_id' => $this->id]);

            return true;
        }
        else
        {
            return false;
        }
    }

    // повертає назву відділення з його батьківськими відділеннями. Можна задати глибину виводу.
    public static function parentsName($id, $depth = null)
    {
        $model = Category::findOne(['id' => $id]);

        if ($depth)
        if ($model)
        {
            $parents = $model->getParents($depth)->all();
            if (sizeOf($parents) > 0)
            {
                $out = '';

                foreach ($parents as $parent)
                {
                    if (strlen($out) > 1)
                        $out = $out . '-' . $parent->name;
                    else
                        $out = $parent->name;
                }

                return $out;
            } else
                return null;
        }
        return null;
    }

    public static function childList($parentId = 1)
    {
        $model = Category::findOne(['id' => $parentId]);

        return ArrayHelper::map($model->getChildren()->active()->all(), 'id', 'name');
    }

    private $out = [];

    public function treeList($data, $level = 0)
    {
        foreach ($data as $item)
        {
            $this->out[$item['id']] = str_repeat(" - ", $level) . $item['text'];

            if (isset($item['nodes']))
            {
                $this->treeList($item['nodes'], $level + 1);
            }
        }

        return $this->out;
    }

    public function treeChild($id, $active = true)
    {
        $model = Category::findOne(['id' => $id]);
        if ($model)
        {
            $childrens = $model->getChildren();

            if ($active)
                $childrens = $childrens->active();

            $childrens = $childrens->all();

            if (sizeOf($childrens) > 0)
            {
                $out = [];
                foreach ($childrens as $children)
                {
                    $out[] = [
                        'id' => $children->id,
                        'text' => $children->name . " [{$children->code}]",
                        'icon' => 'fa fa-folder-o',
                        'href' => Url::to(['view', 'id' => $children->id]),
                        'nodes' => $this->treeChild($children->id, $active),
                    ];
                }
                return $out;
            }
            else
                return null;
        }
        return null;
    }

    public function treeData($active = true)
    {
        $roots = Category::find()->roots()->orderBy(['code' => SORT_ASC]);

        if ($active)
            $roots = $roots->active();

        $roots = $roots->all();

        $out = [];
        foreach ($roots as $root)
        {
            $out[] = [
                'id' => $root->id,
                'text' => $root->name . " [{$root->code}]",
                'icon' => 'fa fa-folder-o',
                'href' => Url::to(['view', 'id' => $root->id]),
                'nodes' => $this->treeChild($root->id, $active),
            ];
        }

        return $out;
    }

    public function listData($active = true)
    {
        return $this->treeList($this->treeData($active));
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'status' => [
                self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
                self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}