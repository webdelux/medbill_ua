<?php

namespace backend\modules\handbookemc\models;

use backend\modules\handbook\models\Status;

/**
 * This is the ActiveQuery class for [[Protocol]].
 *
 * @see Protocol
 */
class ProtocolQuery extends \yii\db\ActiveQuery
{

    public function active()
    {
        $this->andWhere('[[status_id]] = ' . Status::_ACTIVE);
        return $this;
    }

    /**
     * @inheritdoc
     * @return Protocol[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Protocol|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}