<?php

namespace backend\modules\handbookemc\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;

/**
 * This is the ActiveQuery class for [[Complication]].
 *
 * @see Complication
 */
class ComplicationQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return Complication[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Complication|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}