<?php

namespace backend\modules\handbookemc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbookemc\models\OrganComplaints;

/**
 * OrganComplaintsSearch represents the model behind the search form about `backend\modules\handbookemc\models\OrganComplaints`.
 */
class OrganComplaintsSearch extends OrganComplaints
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'handbook_emc_organ_id', 'user_id', 'updated_user_id'], 'integer'],
            [['name', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrganComplaints::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'handbook_emc_organ_id' => $this->handbook_emc_organ_id,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}