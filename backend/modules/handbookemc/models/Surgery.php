<?php

namespace backend\modules\handbookemc\models;

use Yii;
use paulzi\adjacencylist\AdjacencyListBehavior;
use dektrium\user\models\User;
use backend\modules\handbookemc\models\SurgeryCategory;

/**
 * This is the model class for table "{{%handbook_emc_surgery}}".
 *
 * @property integer $id
 * @property integer $handbook_emc_surgery_category_id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $name
 * @property string $code
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Surgery extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_emc_surgery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['handbook_emc_surgery_category_id', 'parent_id', 'sort', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'handbook_emc_surgery_category_id' => Yii::t('app', 'Category'),
            'parent_id' => Yii::t('app', 'Category'),
            'sort' => Yii::t('app', 'Sort'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return SurgeryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SurgeryQuery(get_called_class());
    }

    public function getFullName()
    {
        return $this->code . ', ' . $this->name;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(SurgeryCategory::className(), ['id' => 'handbook_emc_surgery_category_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => AdjacencyListBehavior::className(),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

}