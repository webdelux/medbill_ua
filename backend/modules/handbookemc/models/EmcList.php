<?php

namespace backend\modules\handbookemc\models;

use Yii;
use yii\helpers\ArrayHelper;
use paulzi\adjacencylist\AdjacencyListBehavior;
use backend\models\User;

/**
 * This is the model class for table "{{%handbook_emc_list}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $name
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class EmcList extends \yii\db\ActiveRecord
{

    // Цукровий діабет
    const TREATMENT = 1;
    const DEGREE = 2;
    const STAGE = 3;

    // Гіпертонічна хвороба
    const HYPERTENSION_STAGE = 12;
    const HYPERTENSION_DEGREE = 13;
    const HYPERTENSION_RISK = 14;
    const HYPERTENSION_CATEGORY = 15;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_emc_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name'], 'required'],
            [['parent_id', 'sort', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['sort'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent'),
            'sort' => Yii::t('app', 'Sort'),
            'name' => Yii::t('app', 'Name'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return EmcListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmcListQuery(get_called_class());
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => AdjacencyListBehavior::className(),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function childList($parentId)
    {
        $model = EmcList::findOne(['id' => $parentId]);

        return ArrayHelper::map($model->getChildren()->all(), 'id', 'name');
    }

}