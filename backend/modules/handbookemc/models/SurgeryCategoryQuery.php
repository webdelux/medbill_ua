<?php

namespace backend\modules\handbookemc\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;

/**
 * This is the ActiveQuery class for [[SurgeryCategory]].
 *
 * @see SurgeryCategory
 */
class SurgeryCategoryQuery extends \yii\db\ActiveQuery
{

    use AdjacencyListQueryTrait;

    /*
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }
    */

    /**
     * @inheritdoc
     * @return SurgeryCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SurgeryCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}