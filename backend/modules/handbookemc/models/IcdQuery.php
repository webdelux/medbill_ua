<?php

namespace backend\modules\handbookemc\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;

/**
 * This is the ActiveQuery class for [[Icd]].
 *
 * @see Icd
 */
class IcdQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return Icd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Icd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}