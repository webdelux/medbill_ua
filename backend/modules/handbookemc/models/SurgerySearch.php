<?php

namespace backend\modules\handbookemc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\handbookemc\models\Surgery;

/**
 * SurgerySearch represents the model behind the search form about `backend\modules\handbookemc\models\Surgery`.
 */
class SurgerySearch extends Surgery
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'handbook_emc_surgery_category_id', 'parent_id', 'sort', 'user_id', 'updated_user_id'], 'integer'],
            [['name', 'code', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Surgery::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'handbook_emc_surgery_category_id' => $this->handbook_emc_surgery_category_id,
            'parent_id' => $this->parent_id,
            'sort' => $this->sort,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }

}