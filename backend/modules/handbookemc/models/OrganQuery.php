<?php

namespace backend\modules\handbookemc\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;

/**
 * This is the ActiveQuery class for [[Organ]].
 *
 * @see Organ
 */
class OrganQuery extends \yii\db\ActiveQuery
{

    use AdjacencyListQueryTrait;

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return Organ[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Organ|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}