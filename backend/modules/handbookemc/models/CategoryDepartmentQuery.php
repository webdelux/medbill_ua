<?php

namespace backend\modules\handbookemc\models;

/**
 * This is the ActiveQuery class for [[CategoryDepartment]].
 *
 * @see CategoryDepartment
 */
class CategoryDepartmentQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return CategoryDepartment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CategoryDepartment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}