<?php

namespace backend\modules\handbookemc\models;

use paulzi\adjacencylist\AdjacencyListQueryTrait;

/**
 * This is the ActiveQuery class for [[Surgery]].
 *
 * @see Surgery
 */
class SurgeryQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return Surgery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Surgery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}