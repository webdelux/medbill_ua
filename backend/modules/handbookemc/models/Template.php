<?php

namespace backend\modules\handbookemc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use dektrium\user\models\User;
use backend\modules\handbook\models\Status;
use backend\modules\handbookemc\models\Category;

/**
 * This is the model class for table "{{%handbook_emc_template}}".
 *
 * @property integer $id
 * @property integer $handbook_emc_category_id
 * @property string $name
 * @property string $description
 * @property string $substitutional
 * @property string $pattern
 * @property integer $status_id
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Template extends ActiveRecord
{

    /*
    // Клінічний аналіз крові N 1-4 з АА4-01 по АА4-04.
    const PATTERN_1 = 1;
    const PATTERN_2 = 2;
    const PATTERN_3 = 3;
    const PATTERN_4 = 4;

    // Клінічний аналіз сечі N 1 (повний). BА4-01.
    const PATTERN_5 = 5;
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%handbook_emc_template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['handbook_emc_category_id', 'name'], 'required'],
            [['handbook_emc_category_id', 'status_id', 'user_id', 'updated_user_id'], 'integer'],
            [['description', 'substitutional', 'pattern'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'handbook_emc_category_id' => Yii::t('app', 'Category'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'substitutional' => Yii::t('app', 'Substitutional'),
            'pattern' => Yii::t('app', 'Form pattern'),
            'status_id' => Yii::t('app', 'Status'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return TemplateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TemplateQuery(get_called_class());
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'handbook_emc_category_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            /*
            'pattern' => [
                self::PATTERN_1 => Yii::t('app', 'protocol_pattern_1'),
                self::PATTERN_2 => Yii::t('app', 'protocol_pattern_2'),
                self::PATTERN_3 => Yii::t('app', 'protocol_pattern_3'),
                self::PATTERN_4 => Yii::t('app', 'protocol_pattern_4'),
                self::PATTERN_5 => Yii::t('app', 'protocol_pattern_5'),
            ],
            */
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}