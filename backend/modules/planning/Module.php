<?php

namespace backend\modules\planning;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\planning\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}