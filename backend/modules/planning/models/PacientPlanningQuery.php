<?php

namespace backend\modules\planning\models;

/**
 * This is the ActiveQuery class for [[PacientPlanning]].
 *
 * @see PacientPlanning
 */
class PacientPlanningQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientPlanning[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientPlanning|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}