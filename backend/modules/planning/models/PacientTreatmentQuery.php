<?php

namespace backend\modules\planning\models;

/**
 * This is the ActiveQuery class for [[PacientTreatment]].
 *
 * @see PacientTreatment
 */
class PacientTreatmentQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientTreatment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientTreatment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}