<?php

namespace backend\modules\planning\models;

/**
 * This is the ActiveQuery class for [[PacientDiagnosis]].
 *
 * @see PacientDiagnosis
 */
class PacientDiagnosisQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientDiagnosis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientDiagnosis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}