<?php

namespace backend\modules\planning\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Pacient;
use backend\modules\handbookemc\models\Protocol;
use backend\modules\diagnosis\models\Diagnosis;

/**
 * This is the model class for table "{{%pacient_planning_survey}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $handbook_emc_protocol_id
 * @property string $description
 * @property string $date_at
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $diagnosis_id
 *
 * @property Pacient $pacient
 * @property HandbookEmcProtocol $handbookEmcProtocol
 */
class PacientSurvey extends ActiveRecord
{

    public $handbook_emc_category_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient_planning_survey}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'handbook_emc_protocol_id', 'handbook_emc_category_id', 'diagnosis_id'], 'required'],
            [['pacient_id', 'handbook_emc_protocol_id', 'user_id', 'updated_user_id', 'diagnosis_id'], 'integer'],
            [['date_at', 'created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'handbook_emc_protocol_id' => Yii::t('app', 'Protocol'),
            'description' => Yii::t('app', 'Description'),
            'date_at' => Yii::t('app', 'Date At'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'handbook_emc_category_id' => Yii::t('app', 'Category'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocol()
    {
        return $this->hasOne(Protocol::className(), ['id' => 'handbook_emc_protocol_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiagnosis()
    {
        return $this->hasOne(Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    /**
     * @inheritdoc
     * @return PacientSurveyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientSurveyQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

}