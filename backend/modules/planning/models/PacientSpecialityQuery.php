<?php

namespace backend\modules\planning\models;

/**
 * This is the ActiveQuery class for [[PacientSpeciality]].
 *
 * @see PacientSpeciality
 */
class PacientSpecialityQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientSpeciality[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientSpeciality|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}