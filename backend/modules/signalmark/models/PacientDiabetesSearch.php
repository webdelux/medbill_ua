<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\signalmark\models\PacientDiabetes;

/**
 * PacientDiabetesSearch represents the model behind the search form about `backend\modules\signalmark\models\PacientDiabetes`.
 */
class PacientDiabetesSearch extends PacientDiabetes
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'handbook_emc_list_degree_id', 'handbook_emc_list_stage_id', 'handbook_emc_list_treatment_id', 'user_id', 'updated_user_id'], 'integer'],
            [['term', 'description', 'date_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PacientDiabetes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'handbook_emc_list_degree_id' => $this->handbook_emc_list_degree_id,
            'handbook_emc_list_stage_id' => $this->handbook_emc_list_stage_id,
            'handbook_emc_list_treatment_id' => $this->handbook_emc_list_treatment_id,
            'date_at' => $this->date_at,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'term', $this->term])
                ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

}