<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\signalmark\models\PacientSignalmark;

/**
 * PacientSignalmarkSearch represents the model behind the search form about `backend\modules\signalmark\models\PacientSignalmark`.
 */
class PacientSignalmarkSearch extends PacientSignalmark
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'blood_group', 'blood_rhesus', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at', 'complaints_note', 'anamnesis_note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PacientSignalmark::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'blood_group' => $this->blood_group,
            'blood_rhesus' => $this->blood_rhesus,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'complaints_note', $this->complaints_note])
                ->andFilterWhere(['like', 'anamnesis_note', $this->anamnesis_note]);

        return $dataProvider;
    }

}