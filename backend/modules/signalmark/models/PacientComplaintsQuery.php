<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientComplaints]].
 *
 * @see PacientComplaints
 */
class PacientComplaintsQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientComplaints[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientComplaints|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}