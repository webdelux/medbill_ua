<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Pacient;
use backend\models\User;
use backend\modules\diagnosis\models\Diagnosis;

/**
 * This is the model class for table "{{%pacient_signalmark_experts}}".
 *
 * @property integer $id
 * @property integer $diagnosis_id
 * @property integer $pacient_id
 * @property integer $user_id
 * @property string $description
 * @property string $date_at
 * @property integer $created_user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Pacient $pacient
 * @property User $user
 */
class PacientExperts extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient_signalmark_experts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['diagnosis_id', 'pacient_id', 'user_id', 'date_at', 'description'], 'required'],
            [['diagnosis_id', 'pacient_id', 'user_id', 'created_user_id', 'updated_user_id'], 'integer'],
            [['date_at', 'created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'user_id' => Yii::t('app', 'Consultant'),
            'description' => Yii::t('app', 'Description'),
            'date_at' => Yii::t('app', 'Date At'),
            'created_user_id' => Yii::t('app', 'Created User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getDiagnosis()
    {
        return $this->hasOne(Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return PacientExpertsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientExpertsQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->created_user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

}