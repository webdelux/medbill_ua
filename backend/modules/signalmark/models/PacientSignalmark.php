<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Pacient;

/**
 * This is the model class for table "{{%pacient_signalmark}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $blood_group
 * @property integer $blood_rhesus
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $complaints_note
 * @property string $anamnesis_note
 *
 * @property Pacient $pacient
 */
class PacientSignalmark extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient_signalmark}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id'], 'required'],
            [['pacient_id', 'blood_group', 'blood_rhesus', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at', 'complaints_note', 'anamnesis_note'], 'safe'],
            //[['complaints_note'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'blood_group' => Yii::t('app', 'Blood group'),
            'blood_rhesus' => Yii::t('app', 'Rhesus factor'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'complaints_note' => Yii::t('user', 'Note'),
            'anamnesis_note' => Yii::t('user', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    /**
     * @inheritdoc
     * @return PacientSignalmarkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientSignalmarkQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'blood_group' => [
                '1' => Yii::t('app', '1 (0)'),
                '2' => Yii::t('app', '2 (A)'),
                '3' => Yii::t('app', '3 (B)'),
                '4' => Yii::t('app', '4 (AB)')
            ],
            'blood_rhesus' => [
                '1' => Yii::t('app', 'Rh+'),
                '2' => Yii::t('app', 'Rh-')
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}