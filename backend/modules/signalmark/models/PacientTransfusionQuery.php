<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientTransfusion]].
 *
 * @see PacientTransfusion
 */
class PacientTransfusionQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientTransfusion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientTransfusion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}