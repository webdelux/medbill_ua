<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientAllergic]].
 *
 * @see PacientAllergic
 */
class DispensaryQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientAllergic[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientAllergic|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}