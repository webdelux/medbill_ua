<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\signalmark\models\Dispensary;

/**
 * DispensarySearch represents the model behind the search form about `backend\modules\signalmark\models\Dispensary`.
 */
class DispensarySearch extends Dispensary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'user_id', 'updated_user_id'], 'integer'],
            [['group', 'date_in', 'date_out', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dispensary::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'date_in' => $this->date_in,
            'date_out' => $this->date_out,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'group', $this->group])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
