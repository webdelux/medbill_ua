<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbookemc\models\EmcList;
use backend\models\Pacient;

/**
 * This is the model class for table "{{%pacient_signalmark_hypertension}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $handbook_emc_list_degree_id
 * @property integer $handbook_emc_list_stage_id
 * @property integer $handbook_emc_list_risk_id
 * @property integer $handbook_emc_list_category_id
 * @property string $description
 * @property string $date_at
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Pacient $pacient
 * @property HandbookEmcList $handbookEmcListDegree
 * @property HandbookEmcList $handbookEmcListStage
 * @property HandbookEmcList $handbookEmcListRisk
 * @property HandbookEmcList $handbookEmcListCategory
 */
class PacientHypertension extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient_signalmark_hypertension}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'date_at', 'description'], 'required'],
            [['pacient_id', 'handbook_emc_list_degree_id', 'handbook_emc_list_stage_id', 'handbook_emc_list_risk_id', 'handbook_emc_list_category_id', 'user_id', 'updated_user_id'], 'integer'],
            [['date_at', 'created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'handbook_emc_list_degree_id' => Yii::t('app', 'Degree'),
            'handbook_emc_list_stage_id' => Yii::t('app', 'Stage'),
            'handbook_emc_list_risk_id' => Yii::t('app', 'Risk'),
            'handbook_emc_list_category_id' => Yii::t('app', 'Category'),
            'description' => Yii::t('app', 'Medical products'),
            'date_at' => Yii::t('app', 'Date'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegree()
    {
        return $this->hasOne(EmcList::className(), ['id' => 'handbook_emc_list_degree_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStage()
    {
        return $this->hasOne(EmcList::className(), ['id' => 'handbook_emc_list_stage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRisk()
    {
        return $this->hasOne(EmcList::className(), ['id' => 'handbook_emc_list_risk_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(EmcList::className(), ['id' => 'handbook_emc_list_category_id']);
    }

    /**
     * @inheritdoc
     * @return PacientHypertensionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientHypertensionQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

}