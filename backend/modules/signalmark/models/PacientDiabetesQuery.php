<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientDiabetes]].
 *
 * @see PacientDiabetes
 */
class PacientDiabetesQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientDiabetes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientDiabetes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}