<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientExperts]].
 *
 * @see PacientExperts
 */
class PacientExpertsQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientExperts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientExperts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}