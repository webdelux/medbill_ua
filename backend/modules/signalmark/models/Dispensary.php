<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use backend\models\Pacient;
use backend\models\User;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%dispensary}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property string $group
 * @property string $date_in
 * @property string $date_out
 * @property string $description
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Dispensary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dispensary}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'user_id', 'updated_user_id'], 'integer'],
            [['date_in', 'date_out', 'created_at', 'updated_at'], 'safe'],
            [['date_in', 'date_out' ], 'date','format' => 'yyyy-mm-dd'],
            [['pacient_id', 'date_in', 'group'], 'required'],
            [['group', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'group' => Yii::t('app', 'Dispensary group'),
            'date_in' => Yii::t('app', 'Date in'),
            'date_out' => Yii::t('app', 'Date out'),
            'description' => Yii::t('app', 'Dispensary note'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
                $this->updated_user_id = Yii::$app->user->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'group' => [
                '1' => 'I',
                '2' => 'II',
                '3' => 'III',
                '4' => 'IV',
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * @inheritdoc
     * @return DispensaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DispensaryQuery(get_called_class());
    }
}
