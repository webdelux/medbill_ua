<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientOverall]].
 *
 * @see PacientOverall
 */
class PacientOverallQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientOverall[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientOverall|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}