<?php

namespace backend\modules\signalmark\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\modules\handbookemc\models\Icd;
use backend\models\Pacient;

/**
 * This is the model class for table "{{%pacient_signalmark_anamnesis}}".
 *
 * @property integer $id
 * @property integer $pacient_id
 * @property integer $handbook_emc_icd_id
 * @property string $description
 * @property integer $type
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Pacient $pacient
 * @property HandbookEmcIcd $handbookEmcIcd
 */
class PacientAnamnesis extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pacient_signalmark_anamnesis}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id', 'handbook_emc_icd_id', 'type'], 'required'],
            [['pacient_id', 'handbook_emc_icd_id', 'type', 'user_id', 'updated_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'handbook_emc_icd_id' => Yii::t('app', 'Disease'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIcd()
    {
        return $this->hasOne(Icd::className(), ['id' => 'handbook_emc_icd_id']);
    }

    /**
     * @inheritdoc
     * @return PacientAnamnesisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientAnamnesisQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'type' => [
                '1' => Yii::t('app', 'Diseases childhood'),
                '2' => Yii::t('app', 'TVS, venous diseases, hereditary diseases'),
                '3' => Yii::t('app', 'Other'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}