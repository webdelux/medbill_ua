<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientSignalmark]].
 *
 * @see PacientSignalmark
 */
class PacientSignalmarkQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientSignalmark[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientSignalmark|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}