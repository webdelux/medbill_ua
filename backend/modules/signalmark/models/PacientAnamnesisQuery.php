<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientAnamnesis]].
 *
 * @see PacientAnamnesis
 */
class PacientAnamnesisQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientAnamnesis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientAnamnesis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}