<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientInfectious]].
 *
 * @see PacientInfectious
 */
class PacientInfectiousQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientInfectious[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientInfectious|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}