<?php

namespace backend\modules\signalmark\models;

/**
 * This is the ActiveQuery class for [[PacientIntolerance]].
 *
 * @see PacientIntolerance
 */
class PacientIntoleranceQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return PacientIntolerance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PacientIntolerance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}