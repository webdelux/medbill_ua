<?php

namespace backend\modules\signalmark;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\signalmark\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}