<?php

namespace backend\modules\diagnosis\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\diagnosis\models\Diagnosis;

/**
 * DiagnosisSearch represents the model behind the search form about `backend\modules\diagnosis\models\Diagnosis`.
 */
class DiagnosisSearch extends Diagnosis
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pacient_id', 'handbook_emc_icd_id', 'alternative_print', 'user_id', 'updated_user_id', 'trauma'], 'integer'],
            [['type', 'first_set', 'prophylactic_set', 'department_id'], 'integer'],
            [['parent_id', 'description', 'date_at', 'alternative', 'created_at', 'updated_at', 'date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diagnosis::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $departments = (new \backend\modules\handbook\models\Department)->childs(intval($this->department_id));

        $query->andFilterWhere([
            'id' => $this->id,
            'pacient_id' => $this->pacient_id,
            'handbook_emc_icd_id' => $this->handbook_emc_icd_id,
            //'date_at' => $this->date_at,
            'alternative_print' => $this->alternative_print,
            'type' => $this->type,
            'parent_id' => $this->parent_id,
            'first_set' => $this->first_set,
            'prophylactic_set' => $this->prophylactic_set,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'trauma' => $this->trauma,
//            'department_id' => $this->department_id,
            'department_id' => $departments,
        ]);


        if (strlen($this->date_at) > 0)
        {
            $dateFrom = date('Y-m-d 00:00:00', strtotime($this->date_at));
        }
        else
        {
            $dateFrom = date('Y-0-0 00:00:00');  // з початку поточного року
        }

        if (strlen($this->date_to) > 0)
        {
            $dateTo = date('Y-m-d 23:59:59', strtotime($this->date_to));
        }
        else
        {
            $dateTo = date('Y-m-d 23:59:59');
        }
        $query->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['>=', 'date_at', $dateFrom])
                ->andFilterWhere(['<=', 'date_at', $dateTo])
                ->andFilterWhere(['like', 'alternative', $this->alternative]);

        //\yii\helpers\VarDumper::dump($query->createCommand()->getRawSql());

        return $dataProvider;
    }

}