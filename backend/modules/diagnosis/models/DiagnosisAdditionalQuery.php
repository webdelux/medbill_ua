<?php

namespace backend\modules\diagnosis\models;

/**
 * This is the ActiveQuery class for [[DiagnosisAdditional]].
 *
 * @see DiagnosisAdditional
 */
class DiagnosisAdditionalQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    /**
     * @inheritdoc
     * @return DiagnosisAdditional[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DiagnosisAdditional|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}