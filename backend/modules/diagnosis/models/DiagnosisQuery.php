<?php

namespace backend\modules\diagnosis\models;

/**
 * This is the ActiveQuery class for [[Diagnosis]].
 *
 * @see Diagnosis
 */
class DiagnosisQuery extends \yii\db\ActiveQuery
{

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */

    public function main()
    {
        $this->andWhere('parent_id IS NULL');
        return $this;
    }

    /**
     * @inheritdoc
     * @return Diagnosis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Diagnosis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}