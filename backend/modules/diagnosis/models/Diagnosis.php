<?php

namespace backend\modules\diagnosis\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Pacient;
use backend\modules\handbookemc\models\Icd;

/**
 * This is the model class for table "{{%diagnosis}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $pacient_id
 * @property integer $handbook_emc_icd_id
 * @property string $description
 * @property string $date_at
 * @property string $alternative
 * @property integer $alternative_print
 * @property integer $type
 * @property integer $first_set
 * @property integer $prophylactic_set
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $trauma
 *
 * @property Pacient $pacient
 * @property HandbookEmcIcd $handbookEmcIcd
 * @property DiagnosisAdditional[] $diagnosisAdditionals
 *
 * @property integer $department_id
 */

class Diagnosis extends ActiveRecord
{

    const SCENARIO_HISTORY = 'history';
    const SCENARIO_PLANNING = 'planning';

    const TYPE_PREVIOUS = 1;
    const TYPE_CLINICAL = 2;
    const TYPE_CORRECTED = 3;
    const TYPE_ENDING = 4;
    const TYPE_PATHOLOGICAL = 5;

    public $date_to;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%diagnosis}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pacient_id'], 'required'],
            [['pacient_id', 'handbook_emc_icd_id', 'alternative_print', 'user_id', 'updated_user_id', 'department_id', 'trauma'], 'integer'],
            [['type', 'first_set', 'prophylactic_set', 'parent_id'], 'integer'],
            [['date_at', 'created_at', 'updated_at'], 'safe'],
            [['alternative'], 'string'],
            [['description'], 'string', 'max' => 255],

            [['handbook_emc_icd_id', 'type', 'first_set', 'prophylactic_set', 'date_at'], 'required', 'on' => self::SCENARIO_HISTORY],
            [['handbook_emc_icd_id', 'first_set', 'prophylactic_set', 'date_at'], 'required', 'on' => self::SCENARIO_PLANNING],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pacient_id' => Yii::t('app', 'Pacient'),
            'handbook_emc_icd_id' => Yii::t('app', 'Disease'),
            'description' => Yii::t('app', 'Note'),
            'date_at' => Yii::t('app', 'Date At'),
            'alternative' => Yii::t('app', 'An alternative diagnosis'),
            'alternative_print' => Yii::t('app', 'Print instead of the main alternative diagnosis'),
            'type' => Yii::t('app', 'Diagnosis'),
            'first_set' => Yii::t('app', 'Newly diagnosed'),
            'prophylactic_set' => Yii::t('app', 'Established in routine inspection'),
            'parent_id' => Yii::t('app', 'Parent'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'department_id' => Yii::t('app', 'Departments'),
            'trauma' => Yii::t('app', 'Trauma'),
            'date_to' => Yii::t('app', 'Date') . ' ('. Yii::t('app', 'till').')',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                if (!$this->user_id)
                    $this->user_id = Yii::$app->user->id;

                $this->updated_at = 0;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;

                // При зміні основного діагнозу, «старий» діагноз зберігається в архів.
                $oldAttributes = $this->getOldAttributes();
                if ($oldAttributes['handbook_emc_icd_id'] && $oldAttributes['handbook_emc_icd_id'] != $this->handbook_emc_icd_id)
                {
                    $oldAttributes['parent_id'] = $oldAttributes['id'];
                    unset($oldAttributes['id']);

                    $model = new Diagnosis;
                    $model->attributes = $oldAttributes;
                    $model->save();
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacient()
    {
        return $this->hasOne(Pacient::className(), ['id' => 'pacient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIcd()
    {
        return $this->hasOne(Icd::className(), ['id' => 'handbook_emc_icd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiagnosisAdditionals()
    {
        return $this->hasMany(DiagnosisAdditional::className(), ['diagnosis_id' => 'id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(\backend\modules\handbook\models\Department::className(), ['id' => 'department_id']);
    }

    /**
     * @inheritdoc
     * @return DiagnosisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiagnosisQuery(get_called_class());
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'alternative_print' => [
                '1' => Yii::t('app', 'Yes'),
                '2' => Yii::t('app', 'No'),
            ],
            'type' => [
                self::TYPE_PREVIOUS => Yii::t('app', 'Previous'),
                self::TYPE_CLINICAL => Yii::t('app', 'Clinical'),
                self::TYPE_CORRECTED => Yii::t('app', 'Corrected'),
                self::TYPE_ENDING => Yii::t('app', 'Ending'),
                self::TYPE_PATHOLOGICAL => Yii::t('app', 'Pathological'),
            ],
            'first_set' => [
                '1' => Yii::t('app', 'Yes'),
                '2' => Yii::t('app', 'No'),
            ],
            'prophylactic_set' => [
                '1' => Yii::t('app', 'Yes'),
                '2' => Yii::t('app', 'No'),
            ],
            'trauma' => [
                '1' => Yii::t('app', 'Work injury'),
                '2' => Yii::t('app', 'Non-productive injury'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }
}