<?php

namespace backend\modules\diagnosis\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\diagnosis\models\DiagnosisAdditional;

/**
 * DiagnosisAdditionalSearch represents the model behind the search form about `backend\modules\diagnosis\models\DiagnosisAdditional`.
 */
class DiagnosisAdditionalSearch extends DiagnosisAdditional
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'diagnosis_id', 'handbook_emc_icd_id', 'type', 'user_id', 'updated_user_id'], 'integer'],
            [['description', 'date_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DiagnosisAdditional::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'diagnosis_id' => $this->diagnosis_id,
            'handbook_emc_icd_id' => $this->handbook_emc_icd_id,
            'type' => $this->type,
            'date_at' => $this->date_at,
            'user_id' => $this->user_id,
            'updated_user_id' => $this->updated_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

}