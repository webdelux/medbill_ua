<?php

namespace backend\modules\diagnosis\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Pacient;
use backend\modules\handbookemc\models\Icd;

/**
 * This is the model class for table "{{%diagnosis_additional}}".
 *
 * @property integer $id
 * @property integer $diagnosis_id
 * @property integer $handbook_emc_icd_id
 * @property integer $type
 * @property string $description
 * @property string $date_at
 * @property integer $user_id
 * @property integer $updated_user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Diagnosis $diagnosis
 * @property HandbookEmcIcd $handbookEmcIcd
 */
class DiagnosisAdditional extends ActiveRecord
{

    const TYPE_COMPLICATION = 1;
    const TYPE_CONCOMITANT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%diagnosis_additional}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['diagnosis_id', 'handbook_emc_icd_id', 'type'], 'required'],
            [['diagnosis_id', 'handbook_emc_icd_id', 'type', 'user_id', 'updated_user_id'], 'integer'],
            [['date_at', 'created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'diagnosis_id' => Yii::t('app', 'Diagnosis'),
            'handbook_emc_icd_id' => Yii::t('app', 'Disease'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'date_at' => Yii::t('app', 'Date At'),
            'user_id' => Yii::t('app', 'User'),
            'updated_user_id' => Yii::t('app', 'Updated User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiagnosis()
    {
        return $this->hasOne(Diagnosis::className(), ['id' => 'diagnosis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIcd()
    {
        return $this->hasOne(Icd::className(), ['id' => 'handbook_emc_icd_id']);
    }

    /**
     * @inheritdoc
     * @return DiagnosisAdditionalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiagnosisAdditionalQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($insert)
            {
                $this->user_id = Yii::$app->user->id;
            }
            else
            {
                $this->updated_user_id = Yii::$app->user->id;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public static function itemAlias($type, $code = null)
    {
        $_items = [
            'type' => [
                self::TYPE_COMPLICATION => Yii::t('app', 'Complications'),
                self::TYPE_CONCOMITANT => Yii::t('app', 'Concomitant'),
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}