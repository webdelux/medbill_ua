<?php

namespace backend\modules\diagnosis;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\diagnosis\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}