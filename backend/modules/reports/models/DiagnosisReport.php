<?php
namespace backend\modules\reports\models;

use Yii;
use yii\base\Model;

class DiagnosisReport extends Model
{
    public $from;
    public $to;
    public $department;

    public function rules()
    {
        return [
            [['from', 'to', 'department'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'from' => Yii::t('app', 'Date At'),
            'department' => Yii::t('app', 'Departments'),
            'to' => Yii::t('app', 'Date') . ' ('. Yii::t('app', 'till').')',
        ];
    }
}

