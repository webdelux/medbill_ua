<?php

namespace backend\modules\reports\controllers;

use Yii;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\diagnosis\models\DiagnosisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use kartik\mpdf\Pdf;
use DateTime;

class ReportController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->module->id . '_' . $action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $time_start = microtime(true);

        $report = Yii::$app->request->get('report');
        $report_data = Yii::$app->request->get('report_data');
        $done = Yii::$app->request->get('done');

        if ($report == 12){

            $name = '_search12';

            //перевіряємо форму через модель на введення всіх даних
            $modelForm = new \backend\modules\reports\models\DiagnosisReport();
            if (!($modelForm->load(Yii::$app->request->queryParams) && $modelForm->validate())) {

                // либо страница отображается первый раз, либо есть ошибка в данных
                return $this->render($name, ['modelForm' => $modelForm]);
            }
        }

        if ( (($report_data == 121000) || ($report_data == 121001)) && $done)
        {
            $name_print = 'print/form' . $report_data;

            if ($report_data == 121000)
                $data_name = $this->mkhCnt($report_data, $modelForm->from, $modelForm->to,
                    $modelForm->department, 14,
                    [
                        ['>=', '{{%handbook_emc_icd}}.code', 'A00'],
                        ['<', '{{%handbook_emc_icd}}.code', 'Z99']
                    ]
            );

            if ($report_data == 121001)
                $data_name = $this->mkhCnt($report_data, $modelForm->from, $modelForm->to,
                    $modelForm->department, 14,
                    [
                        ['>=', '{{%handbook_emc_icd}}.code', 'A15'],
                        ['<', '{{%handbook_emc_icd}}.code', 'C99'],
                        ['=', '{{%pacient}}.dispensary_group', 2]
                    ]
            );

            $time_end = microtime(true);
            $time = $time_end - $time_start;

            $cssInline = 'table.t1000, table.t1000 th, table.t1000 td, table.t1000 tr {border: 1px solid black; border-collapse: collapse; text-align: left; line-height: 15px;}'
                    . 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 15px;}';

            // get your HTML raw content without any layouts or scripts
            $content = $this->renderPartial($name_print, [
                'name' => $data_name,
                'time' =>$time,
            ]);

            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                //'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'marginLeft' => 5,
                'marginRight' => 5,
                'marginTop' => 10,
                'marginBottom' => 5,
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => $cssInline,
                // set mPDF properties on the fly
                //'options' => ['title' => 'Krajee Report Title'],
                // call mPDF methods on the fly
                'methods' => [
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
                ]
            ]);

            // return the pdf output as per the destination setting
            return $pdf->render();

        }

// для тесту виводимо діагнози у GridView - потрібно забрати гет параметр "done"
        if ($modelForm->validate())
        {
            $name = 'form12';

            $searchModel = new DiagnosisSearch();
            $searchModel->date_at = date('Y-m-0 00:00:00', strtotime($modelForm->from));
            $searchModel->date_to = date('Y-m-t 23:59:59', strtotime($modelForm->to));
            $searchModel->department_id = $modelForm->department;

            $dataProvider = $searchModel->search('');
            //$model = $dataProvider->getModels();
            $params = [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelForm' => $modelForm,
                ];

            return $this->render($name, $params);
        }

        throw new NotFoundHttpException(Yii::t('app', 'There is no data for view!'));
    }

    public function actionReport($id)
    {
        $this->layout = 'layout';

        $tab = Yii::$app->request->get('tab', 1);
        $diagnosis = Yii::$app->request->get('diagnosis');
        $surgery = Yii::$app->request->get('surgery');
        $report = Yii::$app->request->get('report');

        $model = $this->findModel($id);

        if ($report)
        {
            $name = '';             //назва звіту та шлях до нього

            if ($report == 1 && $diagnosis)       // рахунок
            {
                $name = 'reports/_report1';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 15px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnosis\models\Diagnosis::find()->where(['id' => $diagnosis])->one(),
                    'Protocol' => \backend\models\Protocol::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis, 'status' => 2])->all(),
                    'Supplies' => \backend\models\MedicalSuppliesOperation::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->all(),
                    'Services' => \backend\models\PacientService::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->all(),
                ];
            }
            elseif ($report == 2)       // Виписка з протоколу(карти) патолого-анатомічного обстеження.
            {
                $name = '/modules/emc/views/pathological-examination/_report1';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 15px;}';

                $model = \backend\modules\emc\models\PathologicalExamination::find()->joinWith(['additional'])->where(['pacient_id' => $id])->one();
                $models = [];

                if (!$model)
                {
                    throw new NotFoundHttpException(Yii::t('app', 'There is no data for view!'));
                }
            }
            elseif ($report == 3 && $diagnosis)       // form066o - Статистична карта хворого, який вибув із стаціонару
            {
                $name = 'reports/_form066o';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 17px; padding: 0 5px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnosis\models\Diagnosis::find()->joinWith(['diagnosisAdditionals'])->where(['pacient_id' => $id, '{{%diagnosis}}.id' => $diagnosis])->one(),
                    'Hospitalization' => \backend\modules\emc\models\Hospitalization::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Stacionary' => \backend\models\Stacionary::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Pathological' => \backend\modules\emc\models\PathologicalExamination::find()->joinWith(['additional'])->where(['pacient_id' => $id, '{{%pathological_examination}}.diagnosis_id' => $diagnosis])->one(),
                    'Surgery' => \backend\models\PacientOperation::find()->where(['diagnosis_id' => $diagnosis])->all(),
                    'Protocols' => \backend\models\Protocol::find()->where(['pacient_id' => $id])->orderBy('protocol_date DESC')->all(),
                ];
            }
            elseif ($report == 4 && $diagnosis)       // form204-1/0 - Направлення на мікробіологічне дослідження
            {
                $name = 'reports/_form204-1-0';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 17px; padding: 0 5px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnosis\models\Diagnosis::find()->joinWith(['diagnosisAdditionals'])->where(['pacient_id' => $id, '{{%diagnosis}}.id' => $diagnosis])->one(),
                ];
            }

            elseif ($report == 5 && $diagnosis)       // form204-1/0 - Направлення на мікробіологічне дослідження
            {
                $name = 'reports/_form_epidemiological_map';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 17px; padding: 0 5px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnosis\models\Diagnosis::find()->joinWith(['diagnosisAdditionals'])->where(['pacient_id' => $id, '{{%diagnosis}}.id' => $diagnosis])->one(),
                    'Hospitalization' => \backend\modules\emc\models\Hospitalization::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Stacionary' => \backend\models\Stacionary::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Surgery' => \backend\models\PacientOperation::find()->where(['id' => $surgery])->one(),
                ];
            }

            // get your HTML raw content without any layouts or scripts
            $content = $this->renderPartial($name, [
                //return $this->render('_report1', [
                'model' => $model,
                'models' => $models,
            ]);

            if (!isset($cssInline)){
                $cssInline = 'table, th, td, tr {border: 1px solid black; border-collapse: collapse; text-align: left; }';
            }

            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                //'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'marginLeft' => 5,
                'marginRight' => 5,
                'marginTop' => 10,
                'marginBottom' => 5,
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => $cssInline,
                // set mPDF properties on the fly
                //'options' => ['title' => 'Krajee Report Title'],
                // call mPDF methods on the fly
                'methods' => [
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
                ]
            ]);

            // return the pdf output as per the destination setting
            return $pdf->render();
        }
        else
        {
            $models = [];
        }

        return $this->render('view', [
                    'model' => $model,
                    'models' => $models,
                    'tab' => $tab
        ]);
    }

    /**
     * Select data from {{%diagnosis}} & {{%pacient}} with passed parameters
     * and if file does not exist save it to file in json
     * @return filename
     */
    private function mkhCnt($report_data, $date_from,  $date_to,  $department, $max_age = 14, $condition)
    {
        if (strlen($date_from) > 0)
        {
            $date_from = date('Y-m-d 00:00:00', strtotime($date_from));
        }
        else
        {
            $date_from = date('Y-0-0 00:00:00');  // з початку поточного року
        }

        if (strlen($date_to) > 0)
        {
            $date_to = date('Y-m-t 23:59:59', strtotime($date_to));
        }
        else
        {
            $date_to = date('Y-m-t 23:59:59');
        }

        $departments = (new \backend\modules\handbook\models\Department)->childs(intval($department));

        $max_date = strtotime('-'.$max_age.'year');
        $max_date =  date('Y-m-d', $max_date);

        $cnt = [];

//        $model = Diagnosis::find()->select(['first_set', 'handbook_emc_icd_id', 'pacient_id', '{{%diagnosis}}.id AS diagn_id' ])
//                ->joinWith([
//                'icd' => function ($query) {
//                    $query->select(['{{%handbook_emc_icd}}.id', 'code']);
//                },
//                'pacient' => function ($query) {
//                    $query->select(['{{%pacient}}.id', 'gender', 'birthday', 'dispensary_group']);
//                }
//                ])
//                ->andWhere(['>=', 'date_at', $dateFrom])
//                ->andWhere(['<=', 'date_at', $dateTo])
//                ->andWhere(['department_id' => $departments])
//                ->andWhere(['>=','{{%pacient}}.birthday', $maxDate]);
//
//        $model->andWhere([$mkh1[0], '{{%handbook_emc_icd}}.code', $mkh1[1]])
//                ->andWhere([$mkh2[0], '{{%handbook_emc_icd}}.code', $mkh2[1]]);
//
//        $model = $model->asArray()->all();



        $fname = $report_data . '_' .
                date('mY',  strtotime($date_from)) .
                date('mY',  strtotime($date_to)) .
                $department .
                $max_age;


        $cache = Yii::$app->cache;
        $data = $cache->get($fname);

        if ($data === false)
        {
            $model = (new \yii\db\Query())
            ->select(['first_set',  'code', 'gender', 'birthday', 'dispensary_group'])
            ->from('{{%diagnosis}}')
            ->leftJoin('{{%handbook_emc_icd}}', '{{%handbook_emc_icd}}.id = {{%diagnosis}}.handbook_emc_icd_id')
            ->leftJoin('{{%pacient}}', '{{%pacient}}.id = {{%diagnosis}}.pacient_id')
            ->andWhere(['{{%diagnosis}}.parent_id' => null])
            ->andWhere(['>=', '{{%diagnosis}}.date_at', $date_from])
            ->andWhere(['<=', '{{%diagnosis}}.date_at', $date_to])
            ->andWhere(['{{%diagnosis}}.department_id' => $departments])
            ->andWhere(['>=','{{%pacient}}.birthday', $max_date]);

            foreach ($condition as $value)
            {
                $model->andWhere($value);

            }

            $model = $model->all();

            $common = (new \yii\db\Query())
            ->select(['name',  'address', 'phone', 'email', 'edrpou', 'requisites'])
            ->from('{{%department}}')
            ->andWhere(['id' => $department])
            ->one();

            if ($report_data == 121000)
            $result = $this->form12_1000($model, $common);

            if ($report_data == 121001)
            $result = $this->form12_1001($model, $common);

            $result['from'] = date('m/Y',  strtotime($date_from));
            $result['to'] = date('m/Y',  strtotime($date_to));

            $cache->set($fname,
                [
                    $result
                ],
                31536000,
                new \yii\caching\ChainedDependency(
                    [
                        'dependencies' =>
                        [
                            new \yii\caching\DbDependency([
                                'sql' => 'SELECT MAX(updated_at), MAX(created_at) FROM' . Diagnosis::tableName(),
                            ]),
                            new \yii\caching\DbDependency([
                                'sql' => 'SELECT MAX(updated_at), MAX(created_at) FROM' . \backend\models\Pacient::tableName(),
                            ]),
                        ]
                    ]
                )
            );
            //}


        }

        return $fname;
    }

    private function form12_1000($model, $common)
    {
        $result = [
            // загальні данні
            'common' => $common,

            // рядок 1,0
            'val10' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 2,0
            'val20' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 2,1
            'val21' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 2,2
            'val22' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 3,0
            'val30' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 3,2
            'val32' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 3,4
            'val34' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,0
            'val40' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,1
            'val41' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,2
            'val42' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,3
            'val43' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,4
            'val44' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,6
            'val46' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,7
            'val47' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,8
            'val48' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,0
            'val50' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,1
            'val51' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,2
            'val52' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,3
            'val53' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,4
            'val54' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,5
            'val55' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,6
            'val56' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,8
            'val58' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,17
            'val517' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,18
            'val518' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 5,19
            'val519' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 6,0
            'val60' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 7,0
            'val70' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 7,1
            'val71' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 7,5
            'val75' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 7,8
            'val78' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 7,11
            'val711' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 8,0
            'val80' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 8,2
            'val82' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 8,4
            'val84' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 8,8
            'val88' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 8,11
            'val811' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 9,0
            'val90' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 9,1
            'val91' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 9,2
            'val92' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 9,3
            'val93' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 10,0
            'val100' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 10,1
            'val101' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 10,2
            'val102' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 10,3
            'val103' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 10,4
            'val104' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 10,6
            'val106' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 10,15
            'val1015' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,0
            'val110' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,1
            'val111' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,2
            'val112' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,5
            'val115' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,8
            'val118' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,8
            'val119' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,10
            'val1110' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,12
            'val1112' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,14
            'val1114' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 11,16
            'val1116' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,0
            'val120' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,1
            'val121' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,3
            'val123' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,4
            'val124' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,5
            'val125' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,6
            'val126' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,7
            'val127' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,8
            'val128' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,10
            'val1210' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,12
            'val1212' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,13
            'val1213' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,14
            'val1214' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,15
            'val1215' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 12,16
            'val1216' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 13,0
            'val130' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 13,1
            'val131' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 13,2
            'val132' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 13,3
            'val133' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 14,0
            'val140' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 14,2
            'val142' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 14,9
            'val149' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,0
            'val150' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,2
            'val152' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,4
            'val154' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,6
            'val156' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,7
            'val157' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,10
            'val1510' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,11
            'val1511' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,12
            'val1512' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,15
            'val1515' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 15,28
            'val1528' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 16,0
            'val160' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 17,0
            'val170' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 17,1
            'val171' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 17,2
            'val172' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 17,3
            'val173' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 18,0
            'val180' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 18,1
            'val181' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 19,0
            'val190' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 20,0
            'val200' => [0, 0, 0, 0, 0, 0, 0, 0, 0],
        ];

        foreach ($model as $value)
        {
            $result['val10'] = $this->mkhCheck($result['val10'], $value, (strcasecmp($value['code'], 'A00') >= 0 && strcasecmp($value['code'], 'T99') < 0));
            $result['val20'] = $this->mkhCheck($result['val20'], $value, (strcasecmp($value['code'], 'A00') >= 0 && strcasecmp($value['code'], 'C00') < 0));
            $result['val21'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'B18.0') >= 0 && strcasecmp($value['code'], 'B18.1') <= 0));
            $result['val22'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'B18.2') == 0));
            $result['val30'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'C00') >= 0 && strcasecmp($value['code'], 'D49') < 0));
            $result['val32'] = $this->mkhCheck($result['val21'], $value, ((strcasecmp($value['code'], 'D22') >= 0 && strcasecmp($value['code'], 'D23') <= 0) ||
                    (strcasecmp($value['code'], 'D28') >= 0 && strcasecmp($value['code'], 'D30') < 0)));
            $result['val34'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D27') >= 0 && strcasecmp($value['code'], 'D28') < 0));
            $result['val40'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D50') >= 0 && strcasecmp($value['code'], 'D90') < 0));
            $result['val41'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D50') >= 0 && strcasecmp($value['code'], 'D65') < 0));
            $result['val42'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D50') >= 0 && strcasecmp($value['code'], 'D51') < 0));
            $result['val43'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D60') >= 0 && strcasecmp($value['code'], 'D62') < 0));
            $result['val44'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D66') >= 0 && strcasecmp($value['code'], 'D69') < 0));
            $result['val46'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D80') >= 0 && strcasecmp($value['code'], 'D85') < 0));
            $result['val47'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D86') >= 0 && strcasecmp($value['code'], 'D87') < 0));
            $result['val48'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'D89') >= 0 && strcasecmp($value['code'], 'D90') < 0));
            $result['val50'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'E00') >= 0 && strcasecmp($value['code'], 'E91') < 0));
            $result['val51'] = $this->mkhCheck($result['val21'], $value, (strcasecmp($value['code'], 'E01.0') == 0 || strcasecmp($value['code'], 'E04.0') == 0));
            $result['val52'] = $this->mkhCheck($result['val52'], $value, (strcasecmp($value['code'], 'E01.0') == 0 || strcasecmp($value['code'], 'E04.0') == 0)); //?! таке саме, як 51
            $result['val53'] = $this->mkhCheck($result['val53'], $value, (strcasecmp($value['code'], 'E01.8') == 0 ||
                    (strcasecmp($value['code'], 'E03') >= 0) && strcasecmp($value['code'], 'E04') < 0));
            $result['val54'] = $this->mkhCheck($result['val54'], $value, (strcasecmp($value['code'], 'E01.1') == 0 || strcasecmp($value['code'], 'E04.2') == 0));
            $result['val55'] = $this->mkhCheck($result['val55'], $value, (strcasecmp($value['code'], 'E05') >= 0 && strcasecmp($value['code'], 'E06') < 0));
            $result['val56'] = $this->mkhCheck($result['val56'], $value, (strcasecmp($value['code'], 'E06') >= 0 && strcasecmp($value['code'], 'E07') < 0));
            $result['val58'] = $this->mkhCheck($result['val58'], $value, (strcasecmp($value['code'], 'E10') >= 0 && strcasecmp($value['code'], 'E15') < 0));
            $result['val517'] = $this->mkhCheck($result['val517'], $value, (strcasecmp($value['code'], 'E23.2') == 0));
            $result['val518'] = $this->mkhCheck($result['val518'], $value, (strcasecmp($value['code'], 'E66') >= 0 && strcasecmp($value['code'], 'E67') < 0));
            $result['val519'] = $this->mkhCheck($result['val519'], $value, (strcasecmp($value['code'], 'E89.0') == 0));
            $result['val60'] = $this->mkhCheck($result['val60'], $value, (strcasecmp($value['code'], 'F00') >= 0 && strcasecmp($value['code'], 'G00') < 0));
            $result['val70'] = $this->mkhCheck($result['val70'], $value, (strcasecmp($value['code'], 'G00') >= 0 && strcasecmp($value['code'], 'H00') < 0));
            $result['val71'] = $this->mkhCheck($result['val71'], $value, (strcasecmp($value['code'], 'G00') == 0 || strcasecmp($value['code'], 'G03') == 0 ||
                    strcasecmp($value['code'], 'G04') == 0 || strcasecmp($value['code'], 'G06') == 0 ||
                    strcasecmp($value['code'], 'G08') == 0 || strcasecmp($value['code'], 'G09') == 0));
            $result['val75'] = $this->mkhCheck($result['val75'], $value, (strcasecmp($value['code'], 'G40') >= 0 && strcasecmp($value['code'], 'G42') < 0));
            $result['val78'] = $this->mkhCheck($result['val78'], $value, (strcasecmp($value['code'], 'G50') == 0 || strcasecmp($value['code'], 'G51') == 0 ||
                    strcasecmp($value['code'], 'G52') == 0 || strcasecmp($value['code'], 'G54') == 0 ||
                    strcasecmp($value['code'], 'G56') == 0 || strcasecmp($value['code'], 'G57') == 0 ||
                    strcasecmp($value['code'], 'G58') == 0 || strcasecmp($value['code'], 'G60') == 0 ||
                    strcasecmp($value['code'], 'G61') == 0 || strcasecmp($value['code'], 'G62') == 0 ||
                    strcasecmp($value['code'], 'G64') == 0 || strcasecmp($value['code'], 'G70') == 0 ||
                    strcasecmp($value['code'], 'G71') == 0 || strcasecmp($value['code'], 'G72') == 0));
            $result['val711'] = $this->mkhCheck($result['val711'], $value, (strcasecmp($value['code'], 'G80') == 0));
            $result['val80'] = $this->mkhCheck($result['val80'], $value, (strcasecmp($value['code'], 'H00') >= 0 && strcasecmp($value['code'], 'H60') < 0));
            $result['val82'] = $this->mkhCheck($result['val82'], $value, (strcasecmp($value['code'], 'H10') >= 0 && strcasecmp($value['code'], 'H12') < 0));
            $result['val84'] = $this->mkhCheck($result['val84'], $value, (strcasecmp($value['code'], 'H25') >= 0 && strcasecmp($value['code'], 'H27') < 0));
            $result['val88'] = $this->mkhCheck($result['val88'], $value, (strcasecmp($value['code'], 'H49') >= 0 && strcasecmp($value['code'], 'H51') < 0));
            $result['val811'] = $this->mkhCheck($result['val811'], $value, (strcasecmp($value['code'], 'Н52.1') == 0));
            $result['val90'] = $this->mkhCheck($result['val90'], $value, (strcasecmp($value['code'], 'H60') >= 0 && strcasecmp($value['code'], 'H96') < 0));
            $result['val91'] = $this->mkhCheck($result['val91'], $value, ((strcasecmp($value['code'], 'H65') >= 0 && strcasecmp($value['code'], 'H67') < 0) ||
                    (strcasecmp($value['code'], 'H68') >= 0 && strcasecmp($value['code'], 'H75') < 0) ));
            $result['val92'] = $this->mkhCheck($result['val92'], $value, ((strcasecmp($value['code'], 'H65.0') >= 0 && strcasecmp($value['code'], 'H65.1') <= 0) ||
                    strcasecmp($value['code'], 'H66.0') == 0 ));
            $result['val93'] = $this->mkhCheck($result['val93'], $value, ((strcasecmp($value['code'], 'H65.2') >= 0 && strcasecmp($value['code'], 'H65.4') <= 0) ||
                   strcasecmp($value['code'], 'H66.1') >= 0 && strcasecmp($value['code'], 'H66.3') <= 0));
            $result['val100'] = $this->mkhCheck($result['val100'], $value, (strcasecmp($value['code'], 'I00') >= 0 && strcasecmp($value['code'], 'J00') < 0));
            $result['val101'] = $this->mkhCheck($result['val101'], $value, (strcasecmp($value['code'], 'I00') >= 0 && strcasecmp($value['code'], 'I03') < 0));
            $result['val102'] = $this->mkhCheck($result['val102'], $value, (strcasecmp($value['code'], 'I00') >= 0 && strcasecmp($value['code'], 'I01') < 0));
            $result['val103'] = $this->mkhCheck($result['val103'], $value, (strcasecmp($value['code'], 'I05') >= 0 && strcasecmp($value['code'], 'I10') < 0));
            $result['val104'] = $this->mkhCheck($result['val104'], $value, (strcasecmp($value['code'], 'I05') >= 0 && strcasecmp($value['code'], 'I09') < 0));
            $result['val106'] = $this->mkhCheck($result['val106'], $value, (strcasecmp($value['code'], 'I10') >= 0 && strcasecmp($value['code'], 'I11') < 0));
            $result['val1015'] = $this->mkhCheck($result['val1015'], $value, (strcasecmp($value['code'], 'I34') >= 0 && strcasecmp($value['code'], 'I39') < 0));
            $result['val110'] = $this->mkhCheck($result['val110'], $value, (strcasecmp($value['code'], 'J00') >= 0 && strcasecmp($value['code'], 'K00') < 0));
            $result['val111'] = $this->mkhCheck($result['val111'], $value, (strcasecmp($value['code'], 'J02') >= 0 && strcasecmp($value['code'], 'J04') < 0));
            $result['val112'] = $this->mkhCheck($result['val112'], $value, (strcasecmp($value['code'], 'J04') >= 0 && strcasecmp($value['code'], 'J05') < 0));
            $result['val115'] = $this->mkhCheck($result['val115'], $value, (strcasecmp($value['code'], 'J12') >= 0 && strcasecmp($value['code'], 'J17') < 0) ||
                    (strcasecmp($value['code'], 'J18') >= 0 && strcasecmp($value['code'], 'J19') < 0));
            $result['val118'] = $this->mkhCheck($result['val118'], $value, (strcasecmp($value['code'], 'J30.1') >= 0 && strcasecmp($value['code'], 'J30.4') <= 0));
            $result['val119'] = $this->mkhCheck($result['val119'], $value, (strcasecmp($value['code'], 'J31') >= 0 && strcasecmp($value['code'], 'J32') < 0));
            $result['val1110'] = $this->mkhCheck($result['val1110'], $value, (strcasecmp($value['code'], 'J35') >= 0 && strcasecmp($value['code'], 'J36') < 0));
            $result['val1112'] = $this->mkhCheck($result['val1112'], $value, (strcasecmp($value['code'], 'J37') >= 0 && strcasecmp($value['code'], 'J38') < 0));
            $result['val1114'] = $this->mkhCheck($result['val1114'], $value, (strcasecmp($value['code'], 'J41') >= 0 && strcasecmp($value['code'], 'J43') < 0));
            $result['val1116'] = $this->mkhCheck($result['val1116'], $value, (strcasecmp($value['code'], 'J45') >= 0 && strcasecmp($value['code'], 'J47') < 0));
            $result['val120'] = $this->mkhCheck($result['val120'], $value, (strcasecmp($value['code'], 'K00') >= 0 && strcasecmp($value['code'], 'K94') < 0));
            $result['val121'] = $this->mkhCheck($result['val121'], $value, (strcasecmp($value['code'], 'K21') >= 0 && strcasecmp($value['code'], 'K22') < 0));
            $result['val123'] = $this->mkhCheck($result['val123'], $value, (strcasecmp($value['code'], 'K25') >= 0 && strcasecmp($value['code'], 'K28') < 0));
            $result['val124'] = $this->mkhCheck($result['val124'], $value, (strcasecmp($value['code'], 'K29') >= 0 && strcasecmp($value['code'], 'K30') < 0));
            $result['val125'] = $this->mkhCheck($result['val125'], $value, (strcasecmp($value['code'], 'K30') >= 0 && strcasecmp($value['code'], 'K31') < 0));
            $result['val126'] = $this->mkhCheck($result['val126'], $value, (strcasecmp($value['code'], 'K31') >= 0 && strcasecmp($value['code'], 'K32') < 0));
            $result['val127'] = $this->mkhCheck($result['val127'], $value, (strcasecmp($value['code'], 'K50') >= 0 && strcasecmp($value['code'], 'K51') < 0));
            $result['val128'] = $this->mkhCheck($result['val128'], $value, (strcasecmp($value['code'], 'K51') >= 0 && strcasecmp($value['code'], 'K52') < 0));
            $result['val1210'] = $this->mkhCheck($result['val1210'], $value, (strcasecmp($value['code'], 'K58') >= 0 && strcasecmp($value['code'], 'K59') < 0));
            $result['val1212'] = $this->mkhCheck($result['val1212'], $value, (strcasecmp($value['code'], 'K73') >= 0 && strcasecmp($value['code'], 'K74') < 0) ||
                    (strcasecmp($value['code'], 'K75.2') >= 0 && strcasecmp($value['code'], 'K75.4') < 0));
            $result['val1213'] = $this->mkhCheck($result['val1213'], $value, (strcasecmp($value['code'], 'K80') >= 0 && strcasecmp($value['code'], 'K81') < 0));
            $result['val1214'] = $this->mkhCheck($result['val1214'], $value, (strcasecmp($value['code'], 'K81') >= 0 && strcasecmp($value['code'], 'K82') < 0)||
                    (strcasecmp($value['code'], 'K83.0') >= 0 && strcasecmp($value['code'], 'K83.1') < 0));
            $result['val1215'] = $this->mkhCheck($result['val1215'], $value, (strcasecmp($value['code'], 'K85') >= 0 && strcasecmp($value['code'], 'K86') < 0));
            $result['val1216'] = $this->mkhCheck($result['val1216'], $value, (strcasecmp($value['code'], 'K90.0') >= 0 && strcasecmp($value['code'], 'K90.1') < 0));
            $result['val130'] = $this->mkhCheck($result['val130'], $value, (strcasecmp($value['code'], 'L00') >= 0 && strcasecmp($value['code'], 'M00') < 0));
            $result['val131'] = $this->mkhCheck($result['val131'], $value, (strcasecmp($value['code'], 'L00') >= 0 && strcasecmp($value['code'], 'L09') < 0));
            $result['val132'] = $this->mkhCheck($result['val132'], $value, (strcasecmp($value['code'], 'L20') >= 0 && strcasecmp($value['code'], 'L21') < 0));
            $result['val133'] = $this->mkhCheck($result['val133'], $value, (strcasecmp($value['code'], 'L23') >= 0 && strcasecmp($value['code'], 'L26') < 0));
            $result['val140'] = $this->mkhCheck($result['val140'], $value, (strcasecmp($value['code'], 'M00') >= 0 && strcasecmp($value['code'], 'N00') < 0));
            $result['val142'] = $this->mkhCheck($result['val142'], $value, (strcasecmp($value['code'], 'M05') >= 0 && strcasecmp($value['code'], 'M07') < 0)||
                    (strcasecmp($value['code'], 'M08.0') >= 0 && strcasecmp($value['code'], 'M08.1') < 0));
            $result['val149'] = $this->mkhCheck($result['val149'], $value, (strcasecmp($value['code'], 'M32') >= 0 && strcasecmp($value['code'], 'M33') < 0));
            $result['val150'] = $this->mkhCheck($result['val150'], $value, (strcasecmp($value['code'], 'N00') >= 0 && strcasecmp($value['code'], 'O00') < 0));
            $result['val152'] = $this->mkhCheck($result['val152'], $value, (strcasecmp($value['code'], 'N00') >= 0 && strcasecmp($value['code'], 'N01') < 0));
            $result['val154'] = $this->mkhCheck($result['val154'], $value, (strcasecmp($value['code'], 'N03') >= 0 && strcasecmp($value['code'], 'N04') < 0));
            $result['val156'] = $this->mkhCheck($result['val156'], $value, (strcasecmp($value['code'], 'N10') >= 0 && strcasecmp($value['code'], 'N13') < 0));
            $result['val157'] = $this->mkhCheck($result['val157'], $value, (strcasecmp($value['code'], 'N11') >= 0 && strcasecmp($value['code'], 'N12') < 0));
            $result['val1510'] = $this->mkhCheck($result['val1510'], $value, (strcasecmp($value['code'], 'N30') >= 0 && strcasecmp($value['code'], 'N31') < 0));
            $result['val1511'] = $this->mkhCheck($result['val1511'], $value, (strcasecmp($value['code'], 'N30.0') >= 0 && strcasecmp($value['code'], 'N30.1') < 0));
            $result['val1512'] = $this->mkhCheck($result['val1512'], $value, (strcasecmp($value['code'], 'N30.1') >= 0 && strcasecmp($value['code'], 'N30.3') < 0));
            $result['val1515'] = $this->mkhCheck($result['val1515'], $value, (strcasecmp($value['code'], 'N43') >= 0 && strcasecmp($value['code'], 'N44') < 0));
            $result['val1528'] = $this->mkhCheck($result['val1528'], $value, (strcasecmp($value['code'], 'N91') >= 0 && strcasecmp($value['code'], 'N93') < 0) ||
                    (strcasecmp($value['code'], 'N94') >= 0 && strcasecmp($value['code'], 'N95') < 0));
            $result['val160'] = $this->mkhCheck($result['val60'], $value, (strcasecmp($value['code'], 'O00') >= 0 && strcasecmp($value['code'], 'P00') < 0) &&
                    !(strcasecmp($value['code'], 'O80') >= 0 && strcasecmp($value['code'], 'O81') < 0));
            $result['val170'] = $this->mkhCheck($result['val170'], $value, (strcasecmp($value['code'], 'P05') >= 0 && strcasecmp($value['code'], 'P97') < 0));
            $result['val171'] = $this->mkhCheck($result['val171'], $value, (strcasecmp($value['code'], 'P10') >= 0 && strcasecmp($value['code'], 'P16') < 0));
            $result['val172'] = $this->mkhCheck($result['val172'], $value, (strcasecmp($value['code'], 'P20') >= 0 && strcasecmp($value['code'], 'P22') < 0));
            $result['val173'] = $this->mkhCheck($result['val173'], $value, (strcasecmp($value['code'], 'P55') >= 0 && strcasecmp($value['code'], 'P56') < 0) ||
                    (strcasecmp($value['code'], 'P57.0') >= 0 && strcasecmp($value['code'], 'P57.1') < 0));
            $result['val180'] = $this->mkhCheck($result['val180'], $value, (strcasecmp($value['code'], 'Q00') >= 0 && strcasecmp($value['code'], 'R00') < 0));
            $result['val181'] = $this->mkhCheck($result['val181'], $value, (strcasecmp($value['code'], 'Q20') >= 0 && strcasecmp($value['code'], 'Q27') < 0));
            $result['val190'] = $this->mkhCheck($result['val190'], $value, (strcasecmp($value['code'], 'R00') >= 0 && strcasecmp($value['code'], 'S00') < 0));
            $result['val200'] = $this->mkhCheck($result['val200'], $value, (strcasecmp($value['code'], 'S00') >= 0 && strcasecmp($value['code'], 'T99') < 0));
        }

        return $result;

    }

    private function form12_1001($model, $common)
    {
        $result = [
            // загальні данні
            'common' => $common,

            // рядок 1,0
            'val10' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 2,0
            'val20' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 3,0
            'val30' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

            // рядок 4,0
            'val40' => [0, 0, 0, 0, 0, 0, 0, 0, 0],

        ];

        foreach ($model as $value)
        {
            $result['val10'] = $this->mkhCheck($result['val10'], $value, (strcasecmp($value['code'], 'C00') >= 0 && strcasecmp($value['code'], 'C98') < 0));
            $result['val20'] = $this->mkhCheck($result['val20'], $value, (strcasecmp($value['code'], 'A15') >= 0 && strcasecmp($value['code'], 'A20') < 0));
            $result['val30'] = $this->mkhCheck($result['val30'], $value, (strcasecmp($value['code'], 'A15.0') >= 0 && strcasecmp($value['code'], 'A15.3') <= 0) ||
                (strcasecmp($value['code'], 'A16.0') >= 0 && strcasecmp($value['code'], 'A16.2') <= 0) ||
                (strcasecmp($value['code'], 'A19') >= 0 && strcasecmp($value['code'], 'A20') < 0));
            $result['val40'] = $this->mkhCheck($result['val40'], $value, (strcasecmp($value['code'], 'A50') >= 0 && strcasecmp($value['code'], 'A54') < 0));

        }

        return $result;

    }


// передаємо об'єкт для порівнянн та заповнення масиву значень
    private function mkhCheck($mkh, $value, $condition)
    {
        $maxDate6 = date('Y-m-d', strtotime('-6year'));

        if ($condition)
            {
                $mkh[0] += 1;

                if ($value['gender'] == 1)
                {
                    $mkh[1] += 1;
                }

                if ($value['birthday'] >= $maxDate6)
                {
                    $mkh[2] += 1;
                }

                if ($value['birthday'] < $maxDate6)
                {
                    $mkh[3] += 1;
                }

                if ($value['first_set'] == 1)
                {
                    $mkh[4] += 1;

                    if ($value['gender'] == 1)
                    {
                        $mkh[5] += 1;
                    }

                    if ($value['birthday'] >= $maxDate6)
                    {
                        $mkh[6] += 1;
                    }

                    if ($value['birthday'] < $maxDate6)
                    {
                        $mkh[7] += 1;
                    }
                }

                if ($value['dispensary_group'] == 2)
                {
                    $mkh[8] += 1;
                }
            }

        return $mkh;
    }

}
