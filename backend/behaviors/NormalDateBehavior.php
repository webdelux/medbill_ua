<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;

class NormalDateBehavior extends Behavior
{
    public $date_in = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'beforeSave',

        ];
    }

    public function afterFind($event)
    {
        if (!empty($this->owner->date_in))
        {
            foreach ($this->owner->date_in as $value)
            {
                 $this->owner->$value = date('d-m-Y', strtotime($this->owner->$value));
            }
        }
    }

    public function beforeSave($event) {
        if (!empty($this->owner->date_in))
        {
            foreach ($this->owner->date_in as $value)
            {
                if ($this->owner->$value)
                    $this->owner->$value = date('Y-m-d', strtotime($this->owner->$value));
            }
        }
    }

}
