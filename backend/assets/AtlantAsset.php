<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AtlantAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/atlant/';
    public $css = [
        //'css/jquery/jquery-ui.min.css',
        //'css/bootstrap/bootstrap.min.css',
        'css/fontawesome/font-awesome.min.css',
        //'css/summernote/summernote.css',
        //'css/codemirror/codemirror.css',
        //'css/nvd3/nv.d3.css',
        //'css/mcustomscrollbar/jquery.mCustomScrollbar.css',
        //'css/fullcalendar/fullcalendar.css',
        //'css/blueimp/blueimp-gallery.min.css',
        //'css/rickshaw/rickshaw.css',
        //'css/dropzone/dropzone.css',
        //'css/introjs/introjs.min.css',
        'css/animate/animate.min.css',
        'css/theme-default.css',
        'css/custom.css',
    ];
    public $js = [
        "js/plugins/maskedinput/jquery.maskedinput.min.js",
        //"js/plugins/bootstrap/bootstrap-datepicker.js",
        "js/plugins/bootstrap/bootstrap-timepicker.min.js",
        "js/plugins/bootstrap/bootstrap-select.js",
        "js/plugins.js",
        "js/actions.js",
        "js/urls.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}