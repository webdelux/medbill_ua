<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use common\widgets\Alert;
use backend\modules\handbook\models\Department;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\Protocol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="protocol-form">

    <?php $form = ActiveForm::begin([
        'id' => 'protocol-form',
        'action' => ($model->isNewRecord) ? Url::to(['/protocol/create']) : '',
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'offset' => 'col-sm-offset-4',
                'label' => 'col-sm-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => 'col-sm-4',
            ],
        ],
    ]); ?>

    <div id="protocol-form-content">

        <?php echo Alert::widget(); ?>

        <?= Html::activeHiddenInput($model, 'id') ?>

        <?php $token = $model->id . $model->diagnosis_id; ?>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'pacient_id')->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null,
                    'options' => [
                        'id' => ($model->isNewRecord) ? Html::getInputId($model, 'pacient_id') : Html::getInputId($model, 'pacient_id') . "-id{$token}",
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/pacient/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?php /* $form->field($model, 'department_id')->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($model->department) ? '' : $model->department->name,
                    'options' => [
                        'id' => ($model->isNewRecord) ? Html::getInputId($model, 'department_id') : Html::getInputId($model, 'department_id') . "-id{$token}",
                        'placeholder' => '',
                    ],
                    'data' => (new Department)->treeList((new Department)->treeData()),
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]); */ ?>

                <?= $form->field($model, 'department_id')->dropDownList((new Department)->getTreeListByUserId(), [
                    'prompt' => '',
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'department_id') : Html::getInputId($model, 'department_id') . "-id{$token}",
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">

                <?php $protocolFieldOptions = [
                    'template' => '{label}'
                    . '<div class="col-sm-8">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-pencil-square-o"></span>', [
                                    'id' => ($model->isNewRecord) ? "btn-protocol-pattern" : "btn-protocol-pattern-id{$token}",
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modal-protocol-pattern',
                                    'data-token' => $token,
                                    'data-protocol' => (!$model->isNewRecord) ? $model->protocol_id : '',
                                    'data-protocol-name' => (isset($model->protocol->name)) ? $model->protocol->name : '',
                                    'data-id' => (!$model->isNewRecord) ? $model->id : '',
                                ])
                            . '</span>'
                        . '</div>'
                        . '{error}'
                        . '{hint}'
                    . '</div>',
                ]; ?>

                <?= $form->field($model, 'protocol_id', (!$model->isNewRecord) ? $protocolFieldOptions : [])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => (isset($model->protocol->name)) ? $model->protocol->name : null,
                    'options' => [
                        'id' => ($model->isNewRecord) ? Html::getInputId($model, 'protocol_id') : Html::getInputId($model, 'protocol_id') . "-id{$token}",
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/protocol/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?php /* $form->field($model, 'doctor_id')->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => (isset($model->doctor->name)) ? $model->doctor->name : null,
                    'options' => [
                        'id' => ($model->isNewRecord) ? Html::getInputId($model, 'doctor_id') : Html::getInputId($model, 'doctor_id') . "-id{$token}",
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/user-viewer/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]); */ ?>

                <?php
                // Список всіх користувачів з відділень доступ до яких має поточний користувач
                $doctors = User::getListByDepartments(array_keys((new Department)->getListByUserId()));

                // Chained Selects Plugin
                $doctorsOptions = [];
                foreach ($doctors as $key => $value)
                    $doctorsOptions[$key] = ['class' => implode(' ', array_keys((new Department)->getListParentsByUserId($key)))];
                ?>

                <?= $form->field($model, 'doctor_id')->dropDownList($doctors, [
                    'prompt' => '',
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'doctor_id') : Html::getInputId($model, 'doctor_id') . "-id{$token}",
                    // Chained Selects Plugin
                    'options' => $doctorsOptions
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'total')->textInput([
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'total') : Html::getInputId($model, 'total') . "-id{$token}",
                    'maxlength' => true,
                    'style' => 'width: 100px;'
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'refferal_id')->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => (isset($model->refferal->pib)) ? $model->refferal->pib : null,
                    'options' => [
                        'id' => ($model->isNewRecord) ? Html::getInputId($model, 'refferal_id') : Html::getInputId($model, 'refferal_id') . "-id{$token}",
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbook/refferal/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'discount')->textInput([
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'discount') : Html::getInputId($model, 'discount') . "-id{$token}",
                    'maxlength' => true,
                    'style' => 'width: 100px;'
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'type')->dropDownList($model::itemAlias('type'), [
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'type') : Html::getInputId($model, 'type') . "-id{$token}",
                    'prompt' => ''
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'protocol_date')->widget(DateTimePicker::classname(), [
                    'options' => [
                        'id' => ($model->isNewRecord) ? Html::getInputId($model, 'protocol_date') : Html::getInputId($model, 'protocol_date') . "-id{$token}",
                        'value' => ($model->isNewRecord && isset($_GET['date'])) ? $_GET['date'] : $model->protocol_date,
                        'style' => 'width: 150px;',
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii'
                    ]
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'place')->dropDownList($model::itemAlias('place'), [
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'place') : Html::getInputId($model, 'place') . "-id{$token}",
                    'prompt' => ''
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea([
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'description') : Html::getInputId($model, 'description') . "-id{$token}",
                    'rows' => 2
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownList($model::itemAlias('status'), [
                    'id' => ($model->isNewRecord) ? Html::getInputId($model, 'status') : Html::getInputId($model, 'status') . "-id{$token}",
                    'prompt' => ''
                ]) ?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php echo $this->render('//modules/pattern/views/protocol/_modal'); ?>


<?php $this->registerCss("

    .form-horizontal .modal-row {
        margin-bottom: 15px;
    }

"); ?>

<?php $this->registerJsFile('@web/themes/atlant/js/plugins/chained/jquery.chained.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJs("

    if ($('#protocol-doctor_id-id{$token}').length)
    {
        // Chained Selects Plugin
        $('#protocol-doctor_id-id{$token}').chained('#protocol-department_id-id{$token}');
    }

    if ($('#protocol-doctor_id').length)
    {
        // Chained Selects Plugin
        $('#protocol-doctor_id').chained('#protocol-department_id');
    }

    $('#protocol-protocol_id').on('select2:select', function (e) {
        $('#protocol-total').val(e.params.data.amount);
    });

    $('#protocol-protocol_id-id{$token}').on('select2:select', function (e) {
        $('#btn-protocol-pattern-id{$token}').attr('data-protocol', e.params.data.id);
        $('#btn-protocol-pattern-id{$token}').attr('data-protocol-name', e.params.data.label);
    });

"); ?>