<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Protocol */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Protocols'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="protocol-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>