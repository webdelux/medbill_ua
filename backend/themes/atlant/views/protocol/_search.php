<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\ProtocolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="protocol-search">

     <?php $form = ActiveForm::begin([
        'id' => 'form-search',
        'layout' => 'horizontal',
        'action' => ['index'],
        'method' => 'get',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'protocol_date_start')
        ->widget(DatePicker::className(),
            [
            'language' => 'uk',
            'value' => date('Y-m-d'),
            'pluginOptions' =>
                [
                    'autoclose'=>true,
                    'format' => 'yyyy-m-dd',
                    'todayHighlight' => true
                ]
            ]
        )
    ?>
    <?= $form->field($model, 'protocol_date_end')
        ->widget(DatePicker::className(),
            [
            'language' => 'uk',
            'value' => date('Y-m-d'),
            'pluginOptions' =>
                [
                    'autoclose'=>true,
                    'format' => 'yyyy-m-dd',
                    'todayHighlight' => true
                ]
            ]
        )
    ?>


    <?php // echo $form->field($model, 'refferal_id') ?>

    <?php // echo $form->field($model, 'category_id') ?>

    <?php // echo $form->field($model, 'protocol_date') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'discount') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'updated_user_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

     <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Reset'), ['/protocol/index'],  ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
