<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Protocol */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'ID') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Protocols'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ID') . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="protocol-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>