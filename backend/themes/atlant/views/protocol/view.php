<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Protocol */

$this->title = Yii::t('app', 'Protocol') . ' №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Protocols'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="protocol-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'id' => 'protocol-view',
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'pacient_id',
                'value' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null
            ],
             [
                'attribute' => 'department_id',
                'value' => (isset($model->department->name)) ? $model->department->name : null
            ],
            [
                'attribute' => 'doctor_id',
                'value' => (isset($model->doctor->name)) ? $model->doctor->name : null
            ],
            [
                'attribute' => 'protocol_id',
                'value' => (isset($model->protocol->name)) ? $model->protocol->name : null
            ],
            [
                'attribute' => 'refferal_id',
                'value' => (isset($model->refferal->pib)) ? $model->refferal->pib : null
            ],
            [
                'attribute' => 'category_id',
                'value' => (isset($model->category->name)) ? $model->category->name : null
            ],
            'protocol_date',
             [
                'attribute' => 'type',
                'value' => $model::itemAlias('type', ($model->type) ? $model->type : '')
            ],
             [
                'attribute' => 'place',
                'value' => $model::itemAlias('place', ($model->place) ? $model->place : '')
            ],
            'total',
            'discount',
            [
                'attribute' => 'status',
                'value' => $model::itemAlias('status', ($model->status) ? $model->status : '')
            ],
            'description',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>