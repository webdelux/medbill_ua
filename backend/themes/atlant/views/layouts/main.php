<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\assets\AtlantAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
$bundle = AtlantAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <?= Html::csrfMetaTags() ?>

        <!-- Favicon -->
        <link rel="icon" href="<?php echo $bundle->baseUrl; ?>/favicon.ico" type="image/x-icon" />

        <title><?= Html::encode($this->title . ' | ' . Yii::$app->name) ?></title>

        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            <?= $content ?>
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title">
                        <span class="fa fa-sign-out"></span>
                        <?php echo Yii::t('app', 'Log Out'); ?>?
                    </div>
                    <div class="mb-content">
                        <p><?php echo Yii::t('app', 'Are you sure you want to log out?'); ?></p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a data-method="post" href="<?= Url::to(['/user/security/logout']) ?>" class="btn btn-success btn-lg">
                                <?php echo Yii::t('app', 'Yes'); ?>
                            </a>
                            <button class="btn btn-default btn-lg mb-control-close">
                                <?php echo Yii::t('app', 'No'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>