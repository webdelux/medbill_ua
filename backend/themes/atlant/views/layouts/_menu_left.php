<?php

use yii\widgets\Menu;
use yii\helpers\Html;
?>

<!-- START X-NAVIGATION -->
<?php
$avatar = '';

if (isset(Yii::$app->user->identity->profile->avatar_url))
    $avatar = Html::img('@web/images/' . Yii::$app->user->identity->profile->avatar_url);
else
    $avatar = Html::img('@web/images/default-user.png');

/**
 * Повертає true, якщо для поточного користувача показати розділи меню
 * @return boolean
 */
function isShow()
{
    // Перелік ролей, для яких відображаємо розділи меню
    $items = backend\models\User::$managers;

    $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

    foreach ($roles as $value)
    {
        if (in_array($value->name, $items))
            return true;
    }

    return false;
}

echo Menu::widget([
    'activeCssClass' => 'active',
    'activateParents' => true,
    'options' => [
        'class' => 'x-navigation',
    ],
    'items' => [
        [
            'label' => Yii::$app->name,
            'url' => ['/'],
            'options' => ['class' => 'xn-logo'],
            'template' => '<a href="{url}">{label}</a> <a href="#" class="x-navigation-control"></a>',
        ],
        [
            'label' => Yii::$app->user->identity->username,
            'url' => ['/user-viewer/update', 'id' => Yii::$app->user->id],
            'options' => ['class' => 'xn-profile'],
            'template' => '<div class="profile">'
                . '<div class="profile-image">' . $avatar . '</div>'
                . '<div class="profile-data">'
                    . '<div class="profile-data-name">{label}</div>'
                    . '<div class="profile-data-title">' . Yii::$app->user->identity->phone . '<br>' . Yii::$app->user->identity->email . '</div>'
                . '</div>'
                . '<div class="profile-controls">'
                    . '<a href="{url}" class="profile-control-left"><span class="fa fa-info"></span> </a>'
                . '</div>'
            . '</div>',
        ],
        [
            'label' => '',
            'options' => ['class' => 'xn-title']
        ],
        [
            'label' => Yii::t('app', 'Protocols'),
            'url' => ['/protocol/index'],
            'visible' => (Yii::$app->user->can('protocol_index')),
            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> <span class="xn-text">{label}</span></a>',
        ],
        [
            'label' => Yii::t('user', 'Users'),
            'url' => '#',
            'options' => ['class' => 'xn-openable'],
            'visible' => (Yii::$app->user->identity->isAdmin || Yii::$app->user->can('user-viewer_index')),
            'template' => '<a href="{url}"><span class="fa fa-user-md"></span> <span class="xn-text">{label}</span></a>',
            'items' => [
                [
                    'label' => Yii::t('user', 'Manage users'),
                    'url' => ['/user/admin/index'],
                    'visible' => Yii::$app->user->identity->isAdmin,
                    'template' => '<a href="{url}"><span class="fa fa-user-md"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('user', 'Manage users'),
                    'url' => ['/user-viewer/index'],
                    'visible' => (!Yii::$app->user->identity->isAdmin && Yii::$app->user->can('user-viewer_index')),
                    'template' => '<a href="{url}"><span class="fa fa-user-md"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'The hierarchy of permissions'),
                    'url' => ['/rbac-viewer/index'],
                    'visible' => Yii::$app->user->identity->isAdmin,
                    'template' => '<a href="{url}"><span class="fa fa-sitemap"></span> {label}</a>',
                ],
            ]
        ],
        [
            'label' => Yii::t('app', 'Pacients'),
            'url' => ['/pacient/index'],
            'visible' => Yii::$app->user->can('pacient_index'),
            'template' => '<a href="{url}"><span class="fa fa-users"></span> <span class="xn-text">{label}</span></a>',
        ],
        [
            'label' => Yii::t('app', 'Registry'),
            'url' => ['/registry/index'],
            'visible' => Yii::$app->user->can('registry_index'),
            'template' => '<a href="{url}"><span class="fa fa-calendar"></span> <span class="xn-text">{label}</span></a>',
        ],
        [
            'label' => Yii::t('app', 'Payments'),
            'url' => ['/payment/index'],
            'visible' => (Yii::$app->user->can('payment_index') && isShow()),
            'template' => '<a href="{url}"><span class="fa fa-usd"></span> <span class="xn-text">{label}</span></a>',
        ],
        [
            'label' => Yii::t('app', 'Services'),
            'url' => ['/pacient-service/index'],
            'visible' => (Yii::$app->user->can('pacient-service_index') && isShow()),
            'template' => '<a href="{url}"><span class="fa fa-ambulance"></span> <span class="xn-text">{label}</span></a>',
        ],
        [
            'label' => Yii::t('app', 'Hospital'),
            'url' => ['/stacionary/index'],
            'visible' => (Yii::$app->user->can('stacionary_index') && isShow()),
            'template' => '<a href="{url}"><span class="fa"><img style="padding-right:15px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAnUlEQVQ4T2NkoBAwUqifgXID/v//vwHoigJGRsYH5LiGEWjAf6BGRyB2AOJ8KBtkFj8QfwQafAGfwcgGNAAV2gNxIRAHQNnIekEGfYBimKEHkQ0AecEAaOMGoKMOYDEAq0OQDUgAqognNRzgBgBtBtlKMoAZQLJGmAaYAaCAwxvaWGwwAIr1k+0FYEA7AA3YT7kBZHseqpHyvECpCwDA+EY2TT9lXAAAAABJRU5ErkJggg=="/></span> <span class="xn-text">{label}</span></a>',
        ],
        [
            'label' => Yii::t('app', 'Medical Supplies'),
            'url' => ['/medical-supplies-operation/index'],
            'visible' => (Yii::$app->user->can('medical-supplies-operation_index') && isShow()),
            'template' => '<a href="{url}"><span class="fa fa-plus-square"></span> <span class="xn-text">{label}</span></a>',
        ],
        [
            'label' => Yii::t('app', 'E.M.C.'),
            'url' => '#',
            'options' => ['class' => 'xn-openable'],
            'visible' => isShow(),
            'template' => '<a href="{url}"><span class="fa fa-inbox"></span> <span class="xn-text">{label}</span></a>',
            'items' => [
                [
                    'label' => Yii::t('app', 'Insurance'),
                    'url' => ['/emc/insurance/index'],
                    'visible' => Yii::$app->user->can('emc_insurance_index'),
                    'template' => '<a href="{url}"><span class="fa fa-briefcase"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Incapacities'),
                    'url' => ['/emc/incapacity/index'],
                    'visible' => Yii::$app->user->can('emc_incapacity_index'),
                    'template' => '<a href="{url}"><span class="fa fa-file-text"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Pathological examination'),
                    'url' => ['/emc/pathological-examination/index'],
                    'visible' => Yii::$app->user->can('emc_pathological-examination_index'),
                    'template' => '<a href="{url}"><span class="fa fa-frown-o"></span> {label}</a>',
                ],
            ]
        ],
        [
            'label' => Yii::t('app', 'Reports'),
            'url' => '#',
            'options' => ['class' => 'xn-openable'],
            'visible' => Yii::$app->user->can('reports_report_index'),
            'template' => '<a href="{url}"><span class="fa fa-print"></span> <span class="xn-text">{label}</span></a>',
            'items' => [
                [
                    'label' => Yii::t('app', 'Form12'),
                    'url' => ['/reports/report/index?report=12'],
                    'visible' => Yii::$app->user->can('reports_report_index'),
                    'template' => '<a href="{url}"><span class="fa fa-print"></span> <span class="xn-text">{label}</span></a>'
                ],
            ],
        ],
        [
            'label' => Yii::t('app', 'Handbooks'),
            'url' => '#',
            'options' => ['class' => 'xn-openable'],
            'template' => '<a href="{url}"><span class="fa fa-book"></span> <span class="xn-text">{label}</span></a>',
            'items' => [
                [
                    'label' => Yii::t('app', 'Departments'),
                    'url' => ['/handbook/department/index'],
                    'visible' => (Yii::$app->user->can('handbook_department_index') && isShow()),
                    'template' => '<a href="{url}"><span class="fa fa-hospital-o"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Medical Supplies'),
                    'url' => ['/handbook/medical-supplies/index'],
                    'visible' => (Yii::$app->user->can('handbook_medical-supplies_index') && isShow()),
                    'template' => '<a href="{url}"><span class="fa fa-plus-square"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Services'),
                    'url' => ['/handbook/service/index'],
                    'visible' => (Yii::$app->user->can('handbook_service_index') && isShow()),
                    'template' => '<a href="{url}"><span class="fa fa-ambulance"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Refferal'),
                    'url' => ['/handbook/refferal/index'],
                    'visible' => Yii::$app->user->can('handbook_refferal_index'),
                    'template' => '<a href="{url}"><span class="fa fa-bullhorn "></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Hospital'),
                    'url' => ['/handbook/handbook-stacionary/index'],
                    'visible' => (Yii::$app->user->can('handbook_handbook-stacionary_index') && isShow()),
                    'template' => '<a href="{url}"><span><img style="padding-right:15px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAnUlEQVQ4T2NkoBAwUqifgXID/v//vwHoigJGRsYH5LiGEWjAf6BGRyB2AOJ8KBtkFj8QfwQafAGfwcgGNAAV2gNxIRAHQNnIekEGfYBimKEHkQ0AecEAaOMGoKMOYDEAq0OQDUgAqognNRzgBgBtBtlKMoAZQLJGmAaYAaCAwxvaWGwwAIr1k+0FYEA7AA3YT7kBZHseqpHyvECpCwDA+EY2TT9lXAAAAABJRU5ErkJggg=="/></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Insurance companies'),
                    'url' => ['/handbook/insurance-handbook/index'],
                    'visible' => Yii::$app->user->can('handbook_insurance-handbook_index'),
                    'template' => '<a href="{url}"><span class="fa fa-briefcase"></span> {label}</a>',
                ],
                [
                    'label' => Yii::t('app', 'Service handbooks'),
                    'url' => '#',
                    'options' => ['class' => 'xn-openable'],
                    'visible' => isShow(),
                    'template' => '<a href="{url}"><span class="fa fa-wrench"></span> {label}</a>',
                    'items' => [
                        [
                            'label' => Yii::t('app', 'Departments category'),
                            'url' => ['/handbook/department-category/index'],
                            'visible' => Yii::$app->user->can('handbook_department-category_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hospital-o"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Statuses'),
                            'url' => ['/handbook/status/index'],
                            'visible' => Yii::$app->user->can('handbook_status_index'),
                            'template' => '<a href="{url}"><span class="fa fa-thumb-tack"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('speciality', 'Specialities'),
                            'url' => ['/handbook/speciality/index'],
                            'visible' => Yii::$app->user->can('handbook_speciality_index'),
                            'template' => '<a href="{url}"><span class="fa fa-briefcase"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Bed type'),
                            'url' => ['/handbook/bed-type/index'],
                            'visible' => Yii::$app->user->can('handbook_bed-type_index'),
                            'template' => '<a href="{url}"><span><img style="padding-right:15px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAnUlEQVQ4T2NkoBAwUqifgXID/v//vwHoigJGRsYH5LiGEWjAf6BGRyB2AOJ8KBtkFj8QfwQafAGfwcgGNAAV2gNxIRAHQNnIekEGfYBimKEHkQ0AecEAaOMGoKMOYDEAq0OQDUgAqognNRzgBgBtBtlKMoAZQLJGmAaYAaCAwxvaWGwwAIr1k+0FYEA7AA3YT7kBZHseqpHyvECpCwDA+EY2TT9lXAAAAABJRU5ErkJggg=="/></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Measurements'),
                            'url' => ['/handbook/measurement/index'],
                            'visible' => Yii::$app->user->can('handbook_measurement_index'),
                            'template' => '<a href="{url}"><span class="fa fa-subscript"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'GEO'),
                            'url' => '#',
                            'options' => ['class' => 'xn-openable'],
                            'template' => '<a href="{url}"><span class="fa fa-globe"></span> {label}</a>',
                            'items' => [
                                [
                                    'label' => Yii::t('app', 'Cities'),
                                    'url' => ['/handbook/city/index'],
                                    'visible' => Yii::$app->user->can('handbook_city_index'),
                                    'template' => '<a href="{url}"><span class="fa fa-map-marker"></span> {label}</a>',
                                ],
                                [
                                    'label' => Yii::t('app', 'Regions'),
                                    'url' => ['/handbook/region/index'],
                                    'visible' => Yii::$app->user->can('handbook_region_index'),
                                    'template' => '<a href="{url}"><span class="fa fa-building-o"></span> {label}</a>',
                                ],
                                [
                                    'label' => Yii::t('app', 'Countries'),
                                    'url' => ['/handbook/country/index'],
                                    'visible' => Yii::$app->user->can('handbook_country_index'),
                                    'template' => '<a href="{url}"><span class="fa fa-flag-o"></span> {label}</a>',
                                ]
                            ]
                        ],
                        [
                            'label' => Yii::t('app', 'Payment methods'),
                            'url' => ['/handbook/payment-methods/index'],
                            'visible' => Yii::$app->user->can('handbook_payment-methods_index'),
                            'template' => '<a href="{url}"><span class="fa fa-money"></span> {label}</a>',
                        ],
                    ],
                ],
                [
                    'label' => Yii::t('app', 'E.M.C.'),
                    'url' => '#',
                    'options' => ['class' => 'xn-openable'],
                    'visible' => isShow(),
                    'template' => '<a href="{url}"><span class="fa fa-inbox"></span> {label}</a>',
                    'items' => [
                        [
                            'label' => Yii::t('app', 'Icds'),
                            'url' => ['/handbookemc/icd/index'],
                            'visible' => Yii::$app->user->can('handbookemc_icd_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Surgeries'),
                            'url' => ['/handbookemc/surgery/index'],
                            'visible' => Yii::$app->user->can('handbookemc_surgery_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Complications'),
                            'url' => ['/handbookemc/complication/index'],
                            'visible' => Yii::$app->user->can('handbookemc_complication_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Organs'),
                            'url' => ['/handbookemc/organ/index'],
                            'visible' => Yii::$app->user->can('handbookemc_organ_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Categories'),
                            'url' => ['/handbookemc/category/index'],
                            'visible' => Yii::$app->user->can('handbookemc_category_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Templates'),
                            'url' => ['/handbookemc/template/index'],
                            'visible' => Yii::$app->user->can('handbookemc_template_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> {label}</a>',
                        ],
                        [
                            'label' => Yii::t('app', 'Protocols'),
                            'url' => ['/handbookemc/protocol/index'],
                            'visible' => Yii::$app->user->can('handbookemc_protocol_index'),
                            'template' => '<a href="{url}"><span class="fa fa-hdd-o"></span> {label}</a>',
                        ],
                    ]
                ],
            ]
        ],
    ],
]);
?>
<!-- END X-NAVIGATION -->