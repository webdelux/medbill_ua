<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * Layout for authentificated users.
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
?>

<?php $this->beginContent('@backend/views/layouts/main.php'); ?>

<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">

    <?php echo $this->render('_menu_left'); ?>

</div>
<!-- END PAGE SIDEBAR -->

<!-- PAGE CONTENT -->
<div class="page-content">

    <?php echo $this->render('_menu_top'); ?>

    <!-- START BREADCRUMB -->
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <!-- END BREADCRUMB -->

    <div class="page-title">
        <h2>
            <span class="fa fa-arrow-circle-o-left"></span>&nbsp;
                <?php echo Html::encode($this->title) ?>
        </h2>
    </div>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">

                <?php if (isset($panel) && $panel == false): ?>

                    <?= $content ?>

                <?php else: ?>

                    <div class="panel panel-default">

                        <?php if (isset($this->params['layoutPanelTitle']) && $this->params['layoutPanelTitle']): ?>

                            <div class="panel-heading">
                                <h3 class="panel-title"><?= $this->params['layoutPanelTitle'] ?></h3>
                            </div>

                        <?php endif; ?>

                        <div class="panel-body">
                            <?= $content ?>
                        </div>
                    </div>

                <?php endif; ?>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    <footer class="footer">
        <div class="container">
            <p class="pull-left"><?= Yii::t('app', 'Copyright') ?> &copy; <?= date('Y') ?> <?= Yii::$app->name ?>. <?= Yii::t('app', 'All Rights Reserved') ?>.</p>
            <!--<p class="pull-right"><?= Yii::powered() ?></p>-->
        </div>
    </footer>

</div>
<!-- END PAGE CONTENT -->

<?php $this->endContent(); ?>