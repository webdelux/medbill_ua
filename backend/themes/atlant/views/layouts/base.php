<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\assets\AtlantAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
$bundle = AtlantAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <?= Html::csrfMetaTags() ?>

        <!-- Favicon -->
        <link rel="icon" href="<?php echo $bundle->baseUrl; ?>/favicon.ico" type="image/x-icon" />

        <title><?= Html::encode($this->title . ' | ' . Yii::$app->name) ?></title>

        <?php $this->head() ?>
    </head>
    <body>

        <?php $this->beginBody() ?>

            <?= $content ?>

        <?php $this->endBody() ?>

    </body>
</html>
<?php $this->endPage() ?>