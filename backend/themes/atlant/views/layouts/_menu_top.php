<?php

use yii\helpers\Url;
?>

<!-- START X-NAVIGATION VERTICAL -->
<ul class="x-navigation x-navigation-horizontal x-navigation-panel">

    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button">
        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
    </li>
    <!-- END TOGGLE NAVIGATION -->

    <!-- SEARCH -->
    <li class="xn-search">
        <form role="form">
            <input type="text" name="search" placeholder="<?php echo Yii::t('app', 'Search'); ?>"/>
        </form>
    </li>
    <!-- END SEARCH -->

    <!-- POWER OFF -->
    <li class="xn-icon-button pull-right last btn-toggle">
        <a href="#">
            <span class="fa fa-power-off"></span>
        </a>
        <ul class="xn-drop-left animated zoomIn">
            <li>
                <a href="#">
                    <span class="fa fa-lock"></span>
                    <?php echo Yii::t('app', 'Lock Screen'); ?>
                </a>
            </li>
            <li>
                <a href="#" class="mb-control" data-box="#mb-signout">
                    <span class="fa fa-sign-out"></span>
                    <?php echo Yii::t('app', 'Logout'); ?>
                </a>
            </li>
        </ul>
    </li>
    <!-- END POWER OFF -->

</ul>
<!-- END X-NAVIGATION VERTICAL -->