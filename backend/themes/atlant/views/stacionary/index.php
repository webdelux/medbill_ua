<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use backend\modules\handbook\models\Department;
use backend\modules\handbook\models\HandbookStacionary;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\StacionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Hospital');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stacionary-index">

<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a(Yii::t('app', 'Place a') . ' ' . Yii::t('app', 'pacient'), ['create', 'type' => 1], ['class' => 'btn btn-success']) ?>

        <span style="float: right; color: green">
            <?= Yii::t('app', 'Empty places') . ':  ' . HandbookStacionary::freePlaces() .
            ' ' . Yii::t('app', 'from') . ' ' . HandbookStacionary::totalPlaces() ?>
        </span>
    </p>

    <?php Pjax::begin(['id' => 'stacionary']); ?>

    <?=
    GridView::widget([
        'options' => [
            'id' => 'stacionary-grid'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'striped' => false,
        'export' => false,
        'pjax' => false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
            ],
            [
                'label' => Yii::t('app', 'Pacient'),
                'attribute' => 'fullName',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data)
                {
                    return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'fullName',
                    'options' => [
                        'class' => 'form-control  ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/pacient/index']),
                        'select' => new JsExpression("function(event, ui) {
                            var url = addUrlParameter(window.location.href, 'StacionarySearch[pacient_id]',  ui.item.id);
                            jQuery('#stacionary-grid').yiiGridView({
                                'filterUrl': addUrlParameter(url, 'StacionarySearch[fullName]',  ui.item.value)
                            });
                            jQuery('#stacionary-grid').yiiGridView('applyFilter');

                        }"),
                    ],
                ]),
            ],
            [
                'label' => Yii::t('app', 'Doctor'),
                'attribute' => 'doctor',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data)
                {
                    return (isset($data->doctor->name)) ? $data->doctor->name : '';
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'doctor',
                    'options' => [
                        'class' => 'form-control ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            var url = addUrlParameter(window.location.href, 'StacionarySearch[doctor_id]',  ui.item.id);
                            jQuery('#stacionary-grid').yiiGridView({
                                'filterUrl': addUrlParameter(url, 'StacionarySearch[doctor]',  ui.item.value)
                            });
                            jQuery('#stacionary-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            [
                'attribute' => 'date_stacionary',
                //'format' => ['date', 'dd MMM Y hh:mm'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                'value' => function ($data)
                {
                    return ($data->date_stacionary) ? $data->date_stacionary : NULL;
                }
            ],
            [
                'attribute' => 'date_stacionary_out',
                //'format' => ['date', 'dd MMM Y hh:mm'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                'value' => function ($data)
                {
                    return ($data->date_stacionary_out) ? $data->date_stacionary_out : NULL;
                }
            ],
            [
                'attribute' => 'diagnosis_id',
                'value' => function ($data)
                {
                    return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                },
            ],
            [
                'attribute' => 'department_id',
                'value' => function ($data)
                {
                    return (isset($data->department->name)) ? $data->department->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'department_id', (new Department)->treeList(), ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'label' => Yii::t('app', 'Room'),
                'attribute' => 'room_id',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data)
                {
                    return (isset($data->room->room)) ? $data->room->room : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'room_id', [], ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'label' => Yii::t('app', 'Place'),
                'attribute' => 'place_id',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    return (isset($data->place->place)) ? $data->place->place : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'place_id', [], ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'label' => Yii::t('app', 'Type'),
                'attribute' => 'type',
                'contentOptions' => function ($data)
                {
                    return ['class' => ($data->type == 1) ? 'success' : 'danger', 'width' => '200'];
                },
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'filter' => Html::activeDropDownList($searchModel, 'type', \backend\models\Stacionary::itemAlias('type'), ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($data)
                {
                    return $data::itemAlias('type', ($data->type) ? $data->type : '');
                }
            ],
            [
                'attribute' => 'parent',
                'contentOptions' => ['width' => '50'],
                'headerOptions' => ['style' => 'white-space: normal;', 'width' => '50'],
                'value' => function ($data)
                {
                    return ($data->parent ==0) ? Yii::t('app', 'No') : Yii::t('app', 'Yes');
                },
                'filter' => Html::activeDropDownList($searchModel, 'parent', ['0'=>Yii::t('app', 'No'),'1'=>Yii::t('app', 'Yes')], ['class' => 'form-control', 'prompt' => '']),
            ],
//            [
//                'label' => Yii::t('app', 'Status'),
//                'attribute' => 'status',
//                'contentOptions' => function ($data)
//                {
//                    return ['class' => ($data->place->status == 1) ? 'success' : 'danger', 'width' => '200'];
//                },
//                'filter' => HandbookStacionary::itemAlias('type'),
//                'value' => function ($data)
//                {
//                    return HandbookStacionary::itemAlias('type', ($data->place->status) ? $data->place->status : '');
//                }
//            ],

            // 'description',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'user',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    $res = (isset($data->userProfile->name)) ? $data->userProfile->name : '';
                    $res .= ' (';
                    $res .= (isset($data->user->username)) ? $data->user->username : '';
                    $res .= ')';

                    return $res;
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'user',
                    'options' => [
                        'class' => 'form-control ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            var url = addUrlParameter(window.location.href, 'StacionarySearch[user_id]',  ui.item.id);
                            jQuery('#stacionary-grid').yiiGridView({
                                'filterUrl': addUrlParameter(url, 'StacionarySearch[user]',  ui.item.value)
                            });
                            jQuery('#stacionary-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
//            [
//                'label' => Yii::t('app', 'Updated User'),
//                'attribute' => 'updatedUser',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
//                'value' => function ($data)
//                    {
//                        $res = (isset($data->updatedUserProfile->name)) ? $data->updatedUserProfile->name : '';
//                        $res .= ' (';
//                        $res .= (isset($data->updatedUser->username)) ? $data->updatedUser->username : '';
//                        $res .= ')';
//
//                        return $res;
//                    },
//                'filter' => AutoComplete::widget([
//                    'model' => $searchModel,
//                    'attribute' => 'updatedUser',
//                    'options' => [
//                        'class' => 'form-control ms-operation',
//                    ],
//                    'clientOptions' => [
//                        'source' => Url::to(['/user-viewer/index']),
//                        'select' => new JsExpression("function(event, ui) {
//                            jQuery('#stacionary-grid').yiiGridView({
//                                'filterUrl': '" . Url::to(['/stacionary/index']) . "?"
//                                                . Html::getInputName($searchModel, 'updated_user_id') . "=' + ui.item.id + '&"
//                                                . Html::getInputName($searchModel, 'updatedUser') . "=' + ui.item.value
//                            });
//                            jQuery('#stacionary-grid').yiiGridView('applyFilter');
//                        }"),
//                    ],
//                ]),
//            ],
            // 'updated_user_id',
            'created_at',
//            'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                //'class' => 'kartik\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '100'],
                'template' => '{release} {view} {update} {delete} {link}',
                'buttons' => [
                    'release' => function ($url, $model) {
                        if ($model->type == 1)
                            return Html::a('<span class="fa fa-share"></span>', ['update','type' => 2, 'id' => $model->id],
                            [
                                'title' => Yii::t('app', 'Release'),
                                //'class'=>'btn btn-primary btn-xs',
                            ]);
                        },
                ],
            ],
        ],
    ]);
?>

<?php Pjax::end(); ?>

</div>

<?php $this->registerJs("

// Блок вибору та завантаження палати та ліжка
function dep(){
    if (!$('select#stacionarysearch-department_id').val() > 0) {
        $('select#stacionarysearch-room_id').prop('disabled', true);
        $('select#stacionarysearch-place_id').prop('disabled', true);
    } else {
        $.get( '" . Url::toRoute('handbook/handbook-stacionary/rooms') . "', {
            department: $('select#stacionarysearch-department_id').val()
        })
        .done(function(data) {
            $('select#stacionarysearch-room_id').val('').empty();
            $('select#stacionarysearch-room_id').append($('<option></option>').attr('value', '').text(''));

            $.each(data, function(index, opt) {
                $('select#stacionarysearch-room_id').append($('<option></option>').attr('value', opt.id).text(opt.value));
            });

            var room = getUrlParameter('StacionarySearch[room_id]');
            if(!room > 0) {
                $('select#stacionarysearch-place_id').prop('disabled',true);
            } else {
                $('select#stacionarysearch-room_id').val(room);

                $.get( '" . Url::toRoute('handbook/handbook-stacionary/rooms') . "', { room: room } )
                .done(function( data ) {
                        $('select#stacionarysearch-place_id').val('').empty();
                        $('select#stacionarysearch-place_id').append($('<option></option>').attr('value', '').text(''));

                        $.each(data, function(index, opt) {
                            $('select#stacionarysearch-place_id').append($('<option></option>').attr('value', opt.id).text(opt.value));
                        });

                    var place = getUrlParameter('StacionarySearch[place_id]');

                    if(place > 0) {
                        $('select#stacionarysearch-place_id').val(place);
                    }
                });
            }
        });
    }

};

// очищуємо ід, якщо немає значення
function clear(){
    if (!getUrlParameter('StacionarySearch[fullName]') > 0 && getUrlParameter('StacionarySearch[pacient_id]') > 0){
            jQuery('#stacionary-grid').yiiGridView({'filterUrl': removeUrlParameter(window.location.href, 'StacionarySearch[pacient_id]')});
            jQuery('#stacionary-grid').yiiGridView('applyFilter');
    }

    if (!getUrlParameter('StacionarySearch[doctor]') > 0 && getUrlParameter('StacionarySearch[doctor_id]') > 0){
            jQuery('#stacionary-grid').yiiGridView({'filterUrl': removeUrlParameter(window.location.href, 'StacionarySearch[doctor_id]')});
            jQuery('#stacionary-grid').yiiGridView('applyFilter');
    }

    if (!getUrlParameter('StacionarySearch[user]') > 0 && getUrlParameter('StacionarySearch[user_id]') > 0){
            jQuery('#stacionary-grid').yiiGridView({'filterUrl': removeUrlParameter(window.location.href, 'StacionarySearch[user_id]')});
            jQuery('#stacionary-grid').yiiGridView('applyFilter');
    }

}

$(document).on('pjax:timeout', function(event) {
    // Prevent default timeout redirection behavior
    event.preventDefault();
})

$(document).ready(function() {
    clear();
    dep();

});

$(document).on('pjax:complete', function() {
    clear();
    dep();
});

"); ?>


<?php $this->registerCss("

select.form-control  {
    padding-left: 0px;
}
.ui-widget-content .ui-state-focus {
    border: none;
    border-bottom: 1px solid #d5d5d5;
}

"); ?>

