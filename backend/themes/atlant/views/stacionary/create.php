<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Stacionary */


$this->title = Yii::t('app', 'Place a') . ' ' . mb_strtolower(Yii::t('app', 'pacient'));
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hospital'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stacionary-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
