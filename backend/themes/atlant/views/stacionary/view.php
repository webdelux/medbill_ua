<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\widgets\Alert;
use backend\modules\handbook\models\HandbookStacionary;


/* @var $this yii\web\View */
/* @var $model backend\models\Stacionary */

$this->title = $model->pacient->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hospital'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo Alert::widget(); ?>

<div class="stacionary-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'pacient_id',
                'value' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null
            ],
            [
                'attribute' => 'department_id',
                'value' => (isset($model->department->name)) ? $model->department->name : null
            ],
            [
                'attribute' => 'diagnosis_id',
                'value' => (isset($model->diagnosis->icd->name)) ? $model->diagnosis->icd->name : null
            ],
            [
                'attribute' => 'room',
                'value' => (isset($model->place->room->room)) ? $model->place->room->room : null
            ],
            [
                'attribute' => 'place_id',
                'value' => (isset($model->place->place)) ? $model->place->place : null
            ],
            [
                'attribute' => 'doctor_id',
                'value' => (isset($model->doctor->name)) ? $model->doctor->name : null
            ],
            [
              'attribute' => 'type',
                'value' => $model::itemAlias('type', ($model->type) ? $model->type : '')
            ],
            [
              'attribute' => 'parent',
                'value' => ($model->parent == 0) ? Yii::t('app', 'No') : Yii::t('app', 'Yes')
            ],
            'description',
            'date_stacionary',
            'date_stacionary_out',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <?php
        echo Html::label( ($model->place->pacient_id == 0) ? Yii::t('app', 'This place is exempted!') : Yii::t('app', 'This place is occupied!'), ['#'], ['class' => ($model->place->pacient_id == 0) ? 'label label-success label-form' : 'label label-danger label-form'])
    ?>

</div>
