<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Stacionary */

$this->title = Yii::t('app', 'Update') . ' ' . $model->pacient->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hospital'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pacient->fullName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="stacionary-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
