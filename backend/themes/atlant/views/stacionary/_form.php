<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use backend\modules\handbook\models\Department;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\datetime\DateTimePicker;
use backend\modules\handbook\models\HandbookStacionary;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\Stacionary */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="stacionary-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?php if ($model->pacient_id){
        echo $form->field($model, 'diagnosis_id', ['labelOptions' => ['class' => 'control-label col-sm-2'],
                        ])->dropDownList(ArrayHelper::map(backend\modules\diagnosis\models\Diagnosis::find()
                                ->andWhere(['pacient_id' => $model->pacient_id])
                                ->andWhere(['parent_id' => NULL])
                                ->all(),
                                'id', 'icd.name'), ['prompt' => '']);
    } else {
        $model->diagnosis_id = 0;
    }
    ?>

    <?php echo
        $form->field($model, 'pacientFullName', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(\yii\jui\AutoComplete::classname(), [
            'options' => [
                'class' => 'form-control',
                'value' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null
            ],
            'clientOptions' => [
                'source' => Url::to(['/pacient/index']),
                'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'pacient_id') . "').val(ui.item.id);
            }")
            ],
        ])
    ?>
    <?= Html::activeHiddenInput($model, 'pacient_id') ?>

    <?php echo
        $form->field($model, 'doctorFullName', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(\yii\jui\AutoComplete::classname(), [
            'options' => [
                'class' => 'form-control',
                'value' => (isset($model->doctor->name)) ? $model->doctor->name : null
            ],
            'clientOptions' => [
                'source' => Url::to(['/user-viewer/index']),
                'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'doctor_id') . "').val(ui.item.id);
            }")
            ],
        ])
    ?>
    <?= Html::activeHiddenInput($model, 'doctor_id') ?>

    <?php // echo Html::error($model, 'doctor_id', ['class' => 'help-block']) ?>


    <?=
        $form->field($model, 'department_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Department)->treeList((new Department)->treeData()),
            ['prompt' => ''])
    ?>

    <?php
        if (isset($model->department_id))   //при зміні данних
        {
            echo $form->field($model, 'room_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList((new HandbookStacionary)->roomList($model->department_id, 0, $model->pacient_id),
                ['prompt' => '']);
        }
        else                            // для нового запису - пуста форма. Потім завантажуємо аяксом.
        {
            echo $form->field($model, 'room_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList(['prompt' => '']);
        }
    ?>

    <?php
    if (isset($model->room_id))             //при зміні данних
        {
            echo $form->field($model, 'place_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList((new HandbookStacionary)->roomList(null, $model->room_id,  $model->pacient_id),
                ['prompt' => '']);
        }
        else                                // для нового запису - пуста форма. Потім завантажуємо аяксом.
        {
            echo $form->field($model, 'place_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList(['prompt' => '']);
        }
    ?>


    <?= $form->field($model, 'type', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList($model::itemAlias('type'), ['disabled' => 'disabled', 'prompt' => ''])
    ?>

    <?=
        $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'parent', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList(['0'=>Yii::t('app', 'No'),'1'=>Yii::t('app', 'Yes')])
    ?>

    <?php
        echo $form->field($model, 'date_stacionary', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(DateTimePicker::classname(), [
            'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii'
            ],
            'options' => [
                'value' => $model->date_stacionary ? $model->date_stacionary : date("Y-m-d H:i:s"),
            ],
        ]);
    ?>

    <?php
        echo $form->field($model, 'date_stacionary_out', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(DateTimePicker::classname(), [
            'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii'
            ],
            'options' => [
                'value' => $model->date_stacionary_out ? $model->date_stacionary_out : 0,
            ],
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php //$this->registerJsFile("@web/themes/atlant/js/plugins/chained/jquery.chained.min.js",
        //    ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<?php $this->registerJs("
    $(document).on('change', '#" . Html::getInputId($model, 'pacientFullName') . "', function(event) {
            if (!$(this).val()) {
                $('#" . Html::getInputId($model, 'pacient_id') . "').val('');
            }
        });

    $(document).on('change', '#" . Html::getInputId($model, 'doctorFullName') . "', function(event) {
           if (!$(this).val()) {
               $('#" . Html::getInputId($model, 'doctor_id') . "').val('');
           }
       });

// Завантажуємо список палат для вибранного відділення
    $('select#stacionary-department_id').change(function() {
        $.get( '".Url::toRoute('handbook/handbook-stacionary/rooms')."', { department: $('select#stacionary-department_id option:selected').val() } )
            .done(function( data )
               { console.log(data);
                    $('select#stacionary-room_id').val('').empty();
                    $('select#stacionary-room_id').append($('<option></option>').attr('value', '').text(''));

                    $.each(data, function(index, opt) {
                        $('select#stacionary-room_id').append($('<option></option>').attr('value', opt.id).text(opt.value));
                    });

                $('select#stacionary-room_id').val({$model->room_id});
            });
    });

// Завантажуємо список ліжок для вибранної палати
    $('select#stacionary-room_id').change(function() {
        $.get( '".Url::toRoute('handbook/handbook-stacionary/rooms')."', { room: $('select#stacionary-room_id option:selected').val() } )
            .done(function( data )
               {
                    $('select#stacionary-place_id').val('').empty();
                    $('select#stacionary-place_id').append($('<option></option>').attr('value', '').text(''));

                    $.each(data, function(index, opt) {
                        $('select#stacionary-place_id').append($('<option></option>').attr('value', opt.id).text(opt.value));
                    });

                $('select#stacionary-place_id').val({$model->place_id});
            });
    });

");?>



<?php $this->registerCss("
.ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }
    .ui-autocomplete::after, .ui-autocomplete::before {
        border: none;
    }
"); ?>

