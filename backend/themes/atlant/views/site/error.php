<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use Yii;

$this->title = Yii::t('app', $name);
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="error-container">
    <div class="error-code"><?= $exception->statusCode ?></div>
    <div class="error-text"><?= nl2br(Html::encode($message)) ?></div>
    <div class="error-subtext">
        <p>
            <?= Yii::t('app', 'The above error occurred while the Web server was processing your request.') ?>
        </p>
        <p>
            <?= Yii::t('app', 'Please contact us if you think this is a server error. Thank you.') ?>
        </p>
    </div>
</div>