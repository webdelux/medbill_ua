<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use backend\modules\diagnosis\models\Diagnosis;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientService */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="pacient-service-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?php echo
    $form->field($model, 'pacientFullName', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(\yii\jui\AutoComplete::classname(), [
            'options' => [
                'class' => 'form-control',
                'value' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null
            ],
            'clientOptions' => [
                'source' => Url::to(['/pacient/index']),
                'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'pacient_id') . "').val(ui.item.id);
            }")
            ],
        ])
    ?>
    <?= Html::activeHiddenInput($model, 'pacient_id') ?>

    <?php echo $form->field($model, 'diagnosis_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-2'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                        ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                ->andWhere(['pacient_id' => $model->pacient_id])
                                ->andWhere(['parent_id' => NULL])
                                ->all(),
                                'id', 'icd.name'), ['prompt' => '']);
    ?>

    <?=
        $form->field($model, 'service_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList($model->serviceList())
    ?>

    <?= $form->field($model, 'total', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'discount', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'date_service', ['labelOptions' => ['class' => 'col-sm-2 control-label']])->textInput()
            ->widget(DatePicker::className(),
            [
            'value' => date('Y-m-d'),
            'pluginOptions' =>
                [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        )
    ?>

    <?= $form->field($model, 'docum', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("
    $(document).on('change', '#" . Html::getInputId($model, 'pacientFullName') . "', function(event) {
        if (!$(this).val()) {
            $('#" . Html::getInputId($model, 'pacient_id') . "').val('');
        }
    });
"); ?>


<?php $this->registerCss("
.ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }
    .ui-autocomplete::after, .ui-autocomplete::before {
        border: none;
    }
"); ?>
