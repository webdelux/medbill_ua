<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientService */

$this->title = $model->pacient->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pacient-service-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'pacient_id',
                'value' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null
            ],
            [
                'attribute' => 'service_id',
                'value' => (isset($model->service->name)) ? $model->service->name : null
            ],
            'total',
            'discount',
            'date_service',
            'docum',
            'description',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            //'updated_user_id',
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
