<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PacientService */

$this->title = Yii::t('app', 'Create Service');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pacient-service-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
