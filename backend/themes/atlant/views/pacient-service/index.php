<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;
use backend\modules\handbook\models\Service;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PacientServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Services');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pacient-service-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Service'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin([
        'id' => 'pacient-service',
    ]); ?>
    <?= GridView::widget([
        'options' => [
            'id' => 'pacient-service-grid'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
            ],
            [
                'label' => Yii::t('app', 'Pacient'),
                'attribute' => 'fullName',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data) {
                    return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'fullName',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/pacient/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#pacient-service-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/pacient-service/index']) . "?"
                                . Html::getInputName($searchModel, 'pacient_id') . "=' + ui.item.id + '&"
                                . Html::getInputName($searchModel, 'fullName') . "=' + ui.item.value
                            });
                            jQuery('#pacient-service-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            [
                'attribute' => 'diagnosis_id',
                'value' => function ($data)
                {
                    return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                },
            ],
            [
                'label' => Yii::t('app', 'Service'),
                'attribute' => 'service_id',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data) {
                    return (isset($data->service->name)) ? $data->service->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'service_id',
                    (new backend\models\PacientService)->serviceList(),
                    ['class' => 'form-control', 'prompt' => '']),
            ],
            'total',
            'discount',
            [
                'attribute' => 'date_service',
                'format' => ['date', 'dd MMM Y'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '150'],
                'filter' => \kartik\date\DatePicker::widget([
                    'model'      => $searchModel,
                    'attribute'  => 'date_service',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',

                    ]
                ]),
            ],
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'user',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    $res = (isset($data->userProfile->name)) ? $data->userProfile->name : '';
                    $res .= ' (';
                    $res .= (isset($data->user->username)) ? $data->user->username : '';
                    $res .= ')';

                    return $res;
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'user',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#pacient-service-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/pacient-service/index']) . "?"
                                                . Html::getInputName($searchModel, 'user_id') . "=' + ui.item.id + '&"
                                                . Html::getInputName($searchModel, 'user') . "=' + ui.item.value
                            });
                            jQuery('#pacient-service-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            [
                'label' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUser',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                    {
                        $res = (isset($data->updatedUserProfile->name)) ? $data->updatedUserProfile->name : '';
                        $res .= ' (';
                        $res .= (isset($data->updatedUser->username)) ? $data->updatedUser->username : '';
                        $res .= ')';

                        return $res;
                    },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'updatedUser',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#pacient-service-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/pacient-service/index']) . "?"
                                                . Html::getInputName($searchModel, 'updated_user_id') . "=' + ui.item.id + '&"
                                                . Html::getInputName($searchModel, 'updatedUser') . "=' + ui.item.value
                            });
                            jQuery('#pacient-service-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            'created_at',
            'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>


<?php $this->registerCss("

    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }

"); ?>


<?php $this->registerJs("

    $(document).on('change', '#pacientservicesearch-fullname, #pacientservicesearch-user, #pacientservicesearch-updateduser', function(event) {
        if (!$(this).val())
        {
            jQuery('#pacient-service-grid').yiiGridView({'filterUrl': '" . Url::to(['/pacient-service/index']) . "'});
            jQuery('#pacient-service-grid').yiiGridView('applyFilter');
        }
    });

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

"); ?>