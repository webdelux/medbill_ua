<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientService */

$this->title = Yii::t('app', 'Update') . ' ' .Yii::t('app', 'Service');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacient') . ' '. Yii::t('app', 'Services') , 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pacient->fullName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pacient-service-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
