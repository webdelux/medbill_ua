<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use backend\modules\handbook\models\Status;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pacient-search">

    <?php $form = ActiveForm::begin([
        'id' => 'form-search',
        'layout' => 'horizontal',
        'action' => ['index'],
        'method' => 'get',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?php echo $form->field($model, 'name') ?>

    <?php echo $form->field($model, 'surname') ?>

    <?php echo $form->field($model, 'lastname')?>

    <?= $form->field($model, 'birthday', ['inputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'day-month-year'),]])?>

    <?php  echo $form->field($model, 'ipn') ?>

    <?= $form->field($model, 'phone',
            ['inputOptions' => [
                'class' => 'mask_phone form-control',
                'placeholder' => '+380670000000',
            ]])
    ?>

    <?= $form->field($model, 'phone1',
            ['inputOptions' => [
                'class' => 'mask_phone form-control',
                'placeholder' => '+380670000000',
            ]])
    ?>

    <?= $form->field($model, 'docum_type')->dropDownList($model::itemAlias('docum'), ['prompt' => ''])?>

    <?php  echo $form->field($model, 'docum_number') ?>

    <?= $form->field($model, 'dispensary_group')->dropDownList($model::itemAlias('yn'), ['prompt' => ''])?>

    <?= $form->field($model, 'contingent')->dropDownList($model::itemAlias('contingent'), ['prompt' => ''])?>

    <?php  echo $form->field($model, 'memo') ?>

    <?php  echo $form->field($model, 'card_amb') ?>

    <?php  echo $form->field($model, 'card_stac') ?>

    <?= $form->field($model, 'status_id')->dropDownList(Status::childList(), ['prompt' => ''])?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Reset'), ['/pacient/index'],  ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>