<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\emc\models\PathologicalExamination */

//\yii\helpers\VarDumper::dump($model->additional,10,2);
?>

<table style="width: 100%;" class="_form066o_header">

    <tr>
        <td>
            Міністерство охорони здоров'я України
        </td>

        <td style="text-align: right">
            Код форми за ЗКУД: __ __ __ __ __ __ __ __
        </td>
    </tr>

    <tr>
        <td>
            &nbsp;
        </td>

        <td style="text-align: right">
            Код установи за ЗКПО:
                <?php echo isset($models['Diagnosis']->department->edrpou) ? $models['Diagnosis']->department->edrpou : ''; ?>
        </td>
    </tr>

    <tr>
        <td>
             &nbsp;
        </td>
    </tr>

    <tr>
        <td>
            Найменування закладу:
                <?php echo isset($models['Diagnosis']->department->name) ? $models['Diagnosis']->department->name : ''; ?>,
        </td>

        <td style="text-align: right">
            МЕДИЧНА ДОКУМЕНТАЦІЯ <br>
            ФОРМА 066/о <br>
            Затевердженно наказом МОЗ України <br>
            26.07.1999р. №184
        </td>
    </tr>

    <tr>
        <td>
             &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center">
            СТАТИСТИЧНА КАРТА ХВОРОГО, <br>
            ЯКИЙ ВИБУВ ІЗ СТАЦІОНАРУ №
            <?php echo $model->id; ?>
        </td>
    </tr>

    <tr>
        <td>
             &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="2">
            1. Дата госпіталізації:
                <?php echo isset($models['Hospitalization']->hospitalization_date) ? date('d-m-Y h:m', strtotime($models['Hospitalization']->hospitalization_date)) : ''; ?>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            2. Прізвище, ім'я, по-батькові:
                <?php echo $model->fullname ?>
        </td>
    </tr>

    <tr>
        <td>
            3. Стать:
                <?php
                    if (isset($model->gender))
                        echo ($model->gender == 1) ?  Yii::t('app', 'Man'): Yii::t('app', 'Woman');
                ?>
        </td>
        <td>
            4. Дата народження:
                <?php
                    if (isset($model->birthday))
                        echo date('d-m-Y', strtotime($model->birthday));
                ?>
        </td>
    </tr>

    <tr>
        <td>
            5. Житель:
                <?php
                    if (isset($model->city_type))
                        echo ($model->city_type == 1) ?  'міста' : 'села';
                ?>
        </td>
        <td>
            6. Проживає за адресою:
                <?php
                    if (isset($model->city)){
                        echo $model->city->region . ', ';
                        echo $model->city->title . ', ';
                    }
                    if (isset($model->address)){
                        echo $model->address;
                    }
                ?>

        </td>
    </tr>

    <tr>
        <td colspan="2">
            7. Ким направлений (медичний заклад):
                <?php
                    if (isset($models['Hospitalization']->refferal)){
                        echo $models['Hospitalization']->refferal->pib;
                    }
                ?>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            8. Діагноз при направленні:
                <?php
                    if (isset($models['Hospitalization']->diagnosis_in)){
                        echo $models['Hospitalization']->diagnosis_in;
                    }
                ?>
        </td>
    </tr>

    <tr>
        <td>
            9. Відділення госпіталізації:
                <?php
                    if (isset($models['Hospitalization']->department_id)){
                        echo $models['Hospitalization']->department->name;
                    }
                ?>
        </td>
        <td>
            10. Профіль ліжок:
                <?php
                    if (isset($models['Stacionary']->place->bedType)){
                        echo $models['Stacionary']->place->bedType->bed_type;
                    }
                ?>
        </td>
    </tr>

    <tr>
        <td>
            11. Госпіталізація:
                <?php
                    if (isset($models['Hospitalization']->hospitalization_type)){
                        echo ($models['Hospitalization']->hospitalization_type == 1) ? 'екстрена' : 'планова';
                    }
                ?>
        </td>
        <td>
            12. Строки госпіталізації (годин):
                <?php
                    if (isset($models['Hospitalization']->hospitalization_interval)){
                        echo $models['Hospitalization']->hospitalization_interval;
                    }
                ?>

        </td>
    </tr>

    <tr>
        <td>
            13. Госпіталізація з приводу даного захворювання в даному році:
                <?php
                    if (isset($models['Hospitalization']->first_hospitalization)){
                        echo ($models['Hospitalization']->first_hospitalization == 1) ? 'вперше' : 'повторно';
                    }
                ?>
        </td>
        <td>
            14. Результат лікування:
                <?php
                    if (isset($models['Hospitalization']->cure_result)){
                        echo $models['Hospitalization']::itemAlias('cure_result', $models['Hospitalization']->cure_result);
                    }
                ?>

        </td>
    </tr>

    <tr>
        <td>
            15. Дата виписки, смерті:
                <?php
                    echo isset($models['Hospitalization']->hospitalization_date_out) ? date('d-m-Y h:m', strtotime($models['Hospitalization']->hospitalization_date_out)) : '';
                ?>
        </td>
        <td>
            16. Проведено ліжко-днів:
                <?php
                    if (isset($models['Hospitalization']->hospitalization_date) && isset($models['Hospitalization']->hospitalization_date_out)){
                        echo intval((strtotime($models['Hospitalization']->hospitalization_date_out) - strtotime($models['Hospitalization']->hospitalization_date))/(60*60*24)); //Кількість днів
                    }
                ?>

        </td>
    </tr>

</table>

<table style="width: 100%;" cellpadding='5' class="_form066o_diagnosis">

    <tr>
        <td colspan="4">
            &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="4">
            17. Діагноз стаціонару:
        </td>
    </tr>

    <tr style="border: 1px solid black;">
        <td colspan="2" style="text-align: center; border: 1px solid black;">
            Основний <br>
            а)клінічний; б)патолого-анатомічний
        </td>

        <td style="text-align: center; border: 1px solid black;">
            Ускладнення
        </td>

        <td style="text-align: center; border: 1px solid black;">
            Супутні захворювання
        </td>

        <td style="text-align: center; border: 1px solid black;" >
            Код МКХ-10 основного діагнозу
        </td>

    </tr>

    <tr style="border: 1px solid black;">
        <td style="text-align: right; border: 1px solid black;">
            а)
        </td>
        <td style="border: 1px solid black;">
            <?php
                if ($models['Diagnosis']->alternative_print == 1)
                {
                    isset($models['Diagnosis']->alternative) ? $models['Diagnosis']->alternative : '';
                }
                else
                {
                    echo  isset($models['Diagnosis']->icd->name) ? $models['Diagnosis']->icd->name : '';
                }
            ?>
        </td>

        <td style="border: 1px solid black;">
            <?php
            foreach ($models['Diagnosis']->diagnosisAdditionals as $value)
            {
                if ($value->type == 1)          // 1 = ускладнення основного
                    echo ($value->icd->name . '; ');
            }
            ?>
        </td>

        <td style="border: 1px solid black;">
            <?php
            foreach ($models['Diagnosis']->diagnosisAdditionals as $value)
            {
                if ($value->type == 2)          // 1 = супутні
                    echo ($value->icd->name . '; ');
            }
            ?>
        </td>

        <td style="border: 1px solid black;">
            <?php if (isset($models['Diagnosis']->icd->code)){
                echo $models['Diagnosis']->icd->code;
            }
            ?>
        </td>

    </tr>


    <?php
        if ($models['Pathological']){
    ?>

    <tr style="border: 1px solid black;">
        <td style="text-align: right; border: 1px solid black;">
            б)
        </td>

        <td style="border: 1px solid black;">
            <?php if(isset($models['Pathological']->diagnosisPathological->icd->name)){
                echo $models['Pathological']->diagnosisPathological->icd->name;
            }
            ?>
        </td>

        <td style="border: 1px solid black;">
            <?php
            foreach ($models['Pathological']->additional as $value)
            {
                if ($value->type == 1)          // 1 = ускладнення основного
                    echo ($value->icd->name . '; ');
            }
            ?>
        </td>

        <td style="border: 1px solid black;">
            <?php
            foreach ($models['Pathological']->additional as $value)
            {
                if ($value->type == 2)          // 1 = супутні
                    echo ($value->icd->name . '; ');
            }
            ?>
        </td>

        <td style="border: 1px solid black;">
            <?php if (isset($models['Pathological']->diagnosisPathological->icd->code)){
                echo $models['Pathological']->diagnosisPathological->icd->code;
            }
            ?>
        </td>

    </tr>

    <tr>
        <td colspan="5">
            18. У випадку смерті (вказати причину):
        </td>

    </tr>

    <tr>
        <td colspan="2">
            І. Безпосередня причина смерті
        </td>

        <td colspan="3">
            a)
            <?php if (isset($models['Pathological']->cause)){
                echo $models['Pathological']->cause;
            }
            ?>
        </td>

    </tr>

    <tr>
        <td colspan="2">
            <?php echo Yii::t('app', 'Ills'); ?>
        </td>

        <td colspan="3">
            б)
            <?php if (isset($models['Pathological']->ills)){
                echo $models['Pathological']->ills;
            }
            ?>
        </td>

    </tr>

    <tr>
        <td colspan="5">

            II. <?php echo Yii::t('app', 'Other reasons'); ?>

        </td>
    </tr>

    <tr>

        <td colspan="5">
            <?php if (isset($models['Pathological']->other_reasons)){
                echo $models['Pathological']->other_reasons;
            }
            ?>
        </td>

    </tr>

    <?php
        }
    ?>

    <tr>
        <td colspan="5" style="text-align: center">
            &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="5" style="text-align: center">
            19. Хірургічні операції
        </td>
    </tr>

    <tr style="border: 1px solid black;">
        <td style="text-align: center; border: 1px solid black;">
            Дата, час
        </td>

        <td colspan="2" style="text-align: center; border: 1px solid black;">
            Найменування операції (а)
        </td>

        <td style="text-align: center; border: 1px solid black;">
            Ускладнення (б)
        </td>

        <td style="text-align: center; border: 1px solid black;">
            &nbsp;
        </td>

    </tr>


        <?php
            foreach ($models['Surgery'] as $value)
            {
        ?>
    <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">
            <?php
                if (isset($value->start_operation_date))
                    echo date('d-m-Y', strtotime($value->start_operation_date)) . ' ';

                if (isset($value->start_operation_time))
                    echo $value->start_operation_time;
            ?>
        </td>

        <td colspan="2" style="border: 1px solid black;">
            <?php
                if (isset($value->surgery->name))
                    echo $value->surgery->name . ' (';

                if (isset($value->surgery->code))
                    echo $value->surgery->code . ')';
            ?>

        </td>

        <td style="border: 1px solid black;">
            <?php
                if (isset($value->complication->name))
                    echo $value->complication->name . '; ';

                if (isset($value->handbook_emc_complication_note))
                echo $value->handbook_emc_complication_note;
            ?>
        </td>

        <td style="border: 1px solid black;">
            &nbsp;
        </td>

    </tr>

        <?php
            }
        ?>

    <tr>
        <td colspan="5" style="text-align: left">
            20. Обстежений на "RW":

            <?php
                foreach ($models['Protocols'] as $value)
                {
                    if ($value->protocol->code == 'AAE3-21'){
                        echo $value->protocol_date;
                        break;
                    }
                }

            ?>
        </td>
    </tr>

    <tr>
        <td colspan="5" style="text-align: left">
                      на ВІЛ:

            <?php
                foreach ($models['Protocols'] as $value)
                {
                    if ($value->protocol->code == 'BAE3-73'){
                        echo $value->protocol_date;
                        break;
                    }
                }

            ?>
        </td>
    </tr>

    <tr>
        <td colspan="5" style="text-align: left">
            21. <?php
                if (isset($model->contingent)){
                    echo $model::itemAlias('contingent', ($model->contingent) ? $model->contingent : '');
                }
            ?>
            (
            <?php
                if (isset($model->docum_type)){
                    echo $model::itemAlias('docum', ($model->docum_type) ? $model->docum_type : '');
                }
            ?>
            , №
            <?php
                if (isset($model->docum_number)){
                    echo $model->docum_number;
                }
            ?>
            )
        </td>
    </tr>

    <tr>
        <td>
             &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="5" style="text-align: right">
            Підпис лікаря: ________________________________
        </td>
    </tr>

</table>
