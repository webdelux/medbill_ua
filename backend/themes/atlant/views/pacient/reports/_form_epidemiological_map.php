<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\emc\models\PathologicalExamination */

//\yii\helpers\VarDumper::dump($model->additional,10,2);
?>

<table style="width: 100%;">

    <tr>
        <td>
             &nbsp;
        </td>

        <td style="width: 40%; text-align: left">
            Додаток 2 <br>
            до Інструкції щодо організації контролю <br>
            та профілактики післяопераційних <br>
            гнійно-запальних інфекцій, спричинених <br>
            мікроорганізмами, резистентними до дії <br>
            антимікромбних препаратів.
        </td>
    </tr>

    <tr>
        <td colspan="2">
             &nbsp;
        </td>
    </tr>


    <tr>
        <td colspan="2" style="text-align: center; padding-bottom: 5px;">
            КАРТА <br>
            епідеміологічного спостереження
        </td>
    </tr>

</table>


<table style="width: 100%; text-align: center; border: 1px solid black;">

    <tr>
        <td style="text-align: left; padding-top: 5px;">
            ПАЦІЄНТ:
                <?= $model->fullname ?>.
        </td>
    </tr>

    <tr>
        <td style="text-align: left;">
            Історія хвороби №
                <?= $model->id ?>
        </td>
    </tr>

    <tr>
        <td style="text-align: left;">
            Палата:
            <?php if (isset($models['Stacionary']->room->room)): ?>
                <?= $models['Stacionary']->room->room ?>
            <?php endif; ?>,

            Ліжко:
            <?php if (isset($models['Stacionary']->place->place)): ?>
                <?= $models['Stacionary']->place->place ?>
            <?php endif; ?>
        </td>
    </tr>

    <tr>
        <td style="text-align: left;">
            Відділення:
            <?php if (isset($models['Stacionary']->department->name)): ?>
                <?= '(' . $models['Stacionary']->department->parentsName($models['Stacionary']->department->id, 3) . ') ' . $models['Stacionary']->department->name?>
            <?php endif; ?>

        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding-bottom: 5px;">
            Дата госпіталізації:
            <?php echo isset($models['Hospitalization']->hospitalization_date) ? date('d-m-Y', strtotime($models['Hospitalization']->hospitalization_date)) : ''; ?>

            Дата виписки:
            <?php echo isset($models['Hospitalization']->hospitalization_date_out) ? date('d-m-Y', strtotime($models['Hospitalization']->hospitalization_date_out)) : ''; ?>
        </td>
    </tr>

</table>

<table style="width: 100%; text-align: center; border: 1px solid black;">

    <tr>
        <td style="text-align: left; padding-top: 5px;">
            ХІРУРГІЧНІ ВТРУЧАННЯ:
        </td>

        <td style="text-align: right;">
            ДАТА:
                <?php if (isset($models['Surgery']->start_operation_date)) : ?>
                    <?= date('d-m-Y', strtotime($models['Surgery']->start_operation_date)); ?>
                <?php endif; ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Вид операції:
                <?php if (isset($models['Surgery']->surgery->name)) : ?>
                    <?= $models['Surgery']->surgery->name; ?>
                <?php endif; ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Клас рани __________________________ "чисті" ______________________________ "умовно-чисті" _______________________
            "контаміновані" ___________________________ "брудні або інфіковані" ______________________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Стані пацієнта за шкалою оцінки ризику: ______1 ______2 ______3 ______4 ______5
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Тривалість операції: початок
            <?php if (isset($models['Surgery']->start_operation_time)) : ?>
                <?= ($models['Surgery']->start_operation_time); ?>
            <?php endif; ?>
            закінчення
            <?php if (isset($models['Surgery']->start_operation_time) && isset($models['Surgery']->duration_operation_hour) && isset($models['Surgery']->duration_operation_minute)) : ?>
                <?php
                    echo date('H:i:s', strtotime($models['Surgery']->start_operation_time
                            . ' + ' . $models['Surgery']->duration_operation_hour . ' hour'
                            . ' + ' . $models['Surgery']->duration_operation_minute . ' minute'));
                ?>



            <?php endif; ?>
        </td>
    </tr>

    <tr>
        <td style="text-align: left;">
            Операція:
            <?php if (isset($models['Surgery']->type_id)) : ?>
                <?= $models['Surgery']::itemAlias('type_id', $models['Surgery']->type_id); ?>
            <?php endif; ?>
        </td>

        <td style="text-align: left; padding-bottom: 5px;">
            Протез/імплант ______так ______ні
        </td>
    </tr>

</table>

<table style="width: 100%; text-align: center; border: 1px solid black;">

    <tr>
        <td colspan="2" style="text-align: left; padding-top: 5px;">
            АНТИБІОТИКО-ПРОФІЛАКТИКА: _____так _____ні ___________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="padding-left: 50%; text-align: left; line-height: 12px; font-size: 10px;">
            назва антибіотика
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Антибіотико-профілактика проведена _______________ до операції ___________ після операції ___________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left; padding-bottom: 5px;">
         Дата __________________ Тривалість ________________ днів
        </td>
    </tr>

</table>

<table style="width: 100%; text-align: center; border: 1px solid black;">

    <tr>
        <td style="text-align: left; padding-top: 5px;">
            ПІСЛЯОПЕРАЦІЙНІ ГНІЙНО-ЗАПАЛЬНІ ІНФЕКЦІЇ ________ так ________ ні
        </td>
    </tr>

    <tr>
        <td style="text-align: left;">
            Дата виявлення _________________ клінічна форма _____________________________________________________________
        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding-bottom: 5px;">
            Локалізація інфекції: ______________ поверхнева _______________ глибока __________________ органів/порожнини
        </td>
    </tr>
</table>

<table style="width: 100%; text-align: center; border: 1px solid black;">

    <tr style="border: 1px solid black;">
        <td style="width: 60%; text-align: left; padding-top: 5px; border: 1px solid black;">
            Збудник(и) інфекції:
        </td>

        <td style="text-align: left; padding-top: 5px; border: 1px solid black;">
            Чутливість до АМП:
        </td>
    </tr>

    <tr>
        <td rowspan="4" style="width: 60%; text-align: left; padding-top: 5px; border: 1px solid black;">
            1. _____________________________________________________________________
        </td>

        <td style="text-align: left; padding-top: 5px;">
            Чутливі
        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding-top: 5px;">
            до _________________________________________
        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding-top: 5px;">
            Стійкі
        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding: 5px;">
            до _________________________________________
        </td>
    </tr>

    <tr style="text-align: left; padding-top: 5px; border: 1px solid black;">
        <td rowspan="4" style="width: 60%; text-align: left; padding-top: 5px; border: 1px solid black;">
            2. _____________________________________________________________________
        </td>

        <td style="text-align: left; padding-top: 5px;">
            Чутливі
        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding-top: 5px;">
            до _________________________________________
        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding-top: 5px;">
            Стійкі
        </td>
    </tr>

    <tr>
        <td style="text-align: left; padding: 5px;">
            до _________________________________________
        </td>
    </tr>




</table>

