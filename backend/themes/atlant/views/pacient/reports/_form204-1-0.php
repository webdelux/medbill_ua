<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\emc\models\PathologicalExamination */

//\yii\helpers\VarDumper::dump($model->additional,10,2);
?>

<table style="width: 100%;" class="_form066o_header">

    <tr>
        <td>
            Міністерство охорони здоров'я України
        </td>

        <td style="text-align: right">
            Код форми за ЗКУД: __ __ __ __ __ __ __ __
        </td>
    </tr>

    <tr>
        <td>
            &nbsp;
        </td>

        <td style="text-align: right">
            Код установи за ЗКПО:
                <?php echo isset($models['Diagnosis']->department->edrpou) ? $models['Diagnosis']->department->edrpou : ''; ?>
        </td>
    </tr>

    <tr>
        <td>
             &nbsp;
        </td>
    </tr>

    <tr>
        <td>
            Найменування закладу:
                <?php echo isset($models['Diagnosis']->department->name) ? $models['Diagnosis']->department->name : ''; ?>,
        </td>

        <td style="text-align: right">
            МЕДИЧНА ДОКУМЕНТАЦІЯ <br>
            ФОРМА 204-1/0 <br>
            Затевердженно наказом МОЗ України <br>
            04.01.2001р. №1
        </td>
    </tr>

    <tr>
        <td>
             &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center">
            НАПРАВЛЕННЯ №_______. <br>
            на мікробіологічне дослідження
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center">
            "_____"____________________20____р.______годин_____хвилин
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center; line-height: 12px;font-size: 10px;">
            (дата і час взяття біоматеріалу)
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            В ___________________________________________________________________________ лабораторію.
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Прізвище, ім'я, по-батькові:
                <?php echo $model->fullname .'.' ?>
            Вік:
                <?php
                    if (isset($model->birthday))
                        echo $model->age;
                ?>
            років.
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Заклад______________________________________Відділення___________________________________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left;">
            Адреса постійного місця проживання / тимчасового ( з зазначенням ПІБ особи, у якої мешкає досліджуваний):
                <?php
                    if (isset($model->city)){
                        echo $model->city->region . ', ';
                        echo $model->city->title . ', ';
                    }
                    if (isset($model->address)){
                        echo $model->address;
                    }
                ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            _____________________________________________________________________________________________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            _____________________________________________________________________________________________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            Місце роботи, навчання (найменування дитячого закладу, школи):
                <?php
                    if (isset($model->work)){
                        echo $model->work;
                    }
                ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            Діагноз:
                <?php
                    if ($models['Diagnosis']->alternative_print == 1)
                    {
                        isset($models['Diagnosis']->alternative) ? $models['Diagnosis']->alternative : '';
                    }
                    else
                    {
                        echo  isset($models['Diagnosis']->icd->name) ? $models['Diagnosis']->icd->name : '';
                    }
                ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            Дата захворювання:
                <?php
                    if (isset($models['Diagnosis']->date_at)){
                        echo date('d-m-Y', strtotime($models['Diagnosis']->date_at));
                    }
                ?>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            Показання для обстеження: хворий, реконвалесцент, бактеріо-, вірусо-, паразитоносій, контактний, профілактичне обстеження
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            _____________________________________________________________________________________________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center; line-height: 12px; font-size: 10px;">
            (підкреслити, вписати інше)
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            Матеріал: кров, сеча, мокротиння, кал, дуоденальний вміст, пунктат, спинномозкова рідина, гній, виділення з рани, випіт, сенкційний матеріал, мазок із слизової оболонки, зіскоб, тощо _____________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            Мета та найменування дослідження: _____________________________________________________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center; line-height: 12px; font-size: 10px;">
            (на які інфекції досліджувати)
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            Посада, прізвище, підпис особи. яка направила матеріал ________________________________________________________
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left">
            "_____"_________________________20_____р.
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: left; line-height: 12px; font-size: 10px;">
            (дата доставки біоматеріалу, аналізу)
        </td>
    </tr>




</table>
