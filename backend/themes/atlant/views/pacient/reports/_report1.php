<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\emc\models\PathologicalExamination */

//\yii\helpers\VarDumper::dump($model->additional,10,2);
?>

<table style="width: 100%" class="pathological-report1">

    <tr>
        <td colspan="4" style="text-align: center">
            <strong>
                <?php echo Yii::t('app', 'Invoice') . ' №'; ?>
                <?php echo $models['Diagnosis']->id; ?>
            </strong>
        </td>
    </tr>

    <tr>
        <td  colspan="4" style="text-align: center">
            <strong>
                <?php echo Yii::t('app', 'Invoice from'); ?>
                <?php echo date('d-m-Y', strtotime($models['Diagnosis']->date_at)); ?>
            </strong>
        </td>
    </tr>

    <tr>
        <td  colspan="4" style="text-align: center">
            <strong>
                <?php echo Yii::t('app', 'Diagnosis'); ?>:
                <?php echo ($models['Diagnosis']->alternative) ? $models['Diagnosis']->alternative_print : ''; ?>
            </strong>
        </td>
    </tr>

    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Receiver'); ?>:
            </strong>
            <?php echo isset($models['Diagnosis']->department->name) ? $models['Diagnosis']->department->name : ''; ?>,
            <?php echo isset($models['Diagnosis']->department->address) ? $models['Diagnosis']->department->address : ''; ?>,
        </td>
    </tr>

    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Phone'); ?>:
            </strong>
            <?php echo isset($models['Diagnosis']->department->phone) ? $models['Diagnosis']->department->phone : ''; ?>,


        </td>
    </tr>

    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Edrpou'); ?>:
            </strong>
            <?php echo isset($models['Diagnosis']->department->edrpou) ? $models['Diagnosis']->department->edrpou : ''; ?>,

        </td>
    </tr>

    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Requisites'); ?>:
            </strong>
            <?php echo isset($models['Diagnosis']->department->requisites) ? $models['Diagnosis']->department->requisites : ''; ?>,
        </td>
    </tr>

    <tr>
        <td>
            &nbsp;
        </td>
    </tr>


    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Pacient'); ?>:
            </strong>
            <?php echo $model->fullname; ?>
        </td>
    </tr>

    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Birthday'); ?>:
            </strong>
            <?php echo date('d-m-Y', strtotime($model->birthday)); ?>
        </td>
    </tr>

    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Address'); ?>:
            </strong>
            <?php echo $model->city->region; ?>,
            <?php echo $model->city->title; ?>,
            <?php echo $model->address; ?>
        </td>
    </tr>

    <tr>
        <td>
            <strong>
                <?php echo Yii::t('app', 'Phone'); ?>:
            </strong>
            <?php echo $model->phone; ?>
        </td>
    </tr>

    <tr>
        <td>
            &nbsp;
        </td>
    </tr>

    <?php
    $tot = 0;
    $dis = 0;
    $total = 0;
    $discount = 0;
    ?>

    <?php if($models['Protocol']){?>
    <tr>
        <td  colspan="5" style="text-align: center">
            <strong>
                <?php echo Yii::t('app', 'Protocols'); ?>
            </strong>
        </td>
    </tr>

    <tr style="border: 1px solid black;">
        <td style=" text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Date'); ?>
            </strong>
        </td>

        <td  colspan="2" style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Name'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Total'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Discount'); ?>
            </strong>
        </td>
    </tr>


    <?php foreach ($models['Protocol'] as $value)
    {
        ?>
        <tr style="border: 1px solid black;">
            <td style="border: 1px solid black;">
                <?php echo date('h:m d-m-Y', strtotime($value->protocol_date)); ?>
            </td>

            <td colspan="2" style="border: 1px solid black;">
                <?php echo (isset($value->protocol->name) ? $value->protocol->name . ' (' . $value->category->code . ' ' . $value->category->name . ')':'' ); ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo $value->total ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo $value->discount ?>
            </td>
        </tr>

        <?php
        $total = $total + $value->total;
        $discount = $discount + $value->discount;
    }
    ?>

    <tr style="border: 1px solid black;">
        <td>
            &nbsp;
        </td>

        <td colspan="2">
            &nbsp;
        </td>

        <td style="border: 1px solid black;">
            <strong>
                <?php echo $total ?>
            </strong>

        </td>

        <td style="border: 1px solid black;">
            <strong>
                <?php echo $discount ?>
            </strong>

        </td>
    </tr>

    <tr>
        <td>
            &nbsp;
        </td>
    </tr>


    <?php }?>

    <?php if($models['Supplies']){?>

    <tr>
        <td  colspan="4" style="text-align: center">
            <strong>
                <?php echo Yii::t('app', 'Medical Supplies'); ?>
            </strong>
        </td>
    </tr>

    <tr style="border: 1px solid black;">
        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Date'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Name'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Quantity'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Total'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Discount'); ?>
            </strong>
        </td>
    </tr>

    <?php
    $tot = $tot + $total;
    $dis = $dis + $discount;
    $total = 0;
    $discount = 0;
    ?>

    <?php foreach ($models['Supplies'] as $value)
    {
        ?>
        <tr style="border: 1px solid black;">
            <td style="border: 1px solid black;">
                <?php echo date('h:m d-m-Y', strtotime($value->docum_date)); ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo ($value->medicalSupplies->name . ' (' . $value->medicalSupplies->measurement->name . ')' ); ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo $value->quantity ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo $value->total ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo $value->discount ?>
            </td>
        </tr>

        <?php
        $total = $total + $value->total;
        $discount = $discount + $value->discount;
    }
    ?>

    <tr style="border: 1px solid black;">
        <td>
            &nbsp;
        </td>

        <td>
            &nbsp;
        </td>

        <td>
            &nbsp;
        </td>

        <td style="border: 1px solid black;">
            <strong>
                <?php echo $total ?>
            </strong>

        </td>

        <td style="border: 1px solid black;">
            <strong>
                <?php echo $discount ?>
            </strong>

        </td>
    </tr>

    <?php }?>

    <tr>
        <td>
            &nbsp;
        </td>
    </tr>

    <?php if($models['Services']){?>

    <tr>
        <td  colspan="5" style="text-align: center">
            <strong>
<?php echo Yii::t('app', 'Services'); ?>
            </strong>
        </td>
    </tr>

    <tr style="text-align: center; border: 1px solid black;">
        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Date'); ?>
            </strong>
        </td>

        <td colspan="2" style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Name'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Total'); ?>
            </strong>
        </td>

        <td style="text-align: center; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Discount'); ?>
            </strong>
        </td>
    </tr>

    <?php
    $tot = $tot + $total;
    $dis = $dis + $discount;
    $total = 0;
    $discount = 0;
    ?>

<?php foreach ($models['Services'] as $value)
{
    ?>
        <tr style="border: 1px solid black;">
            <td>
                <?php echo date('h:m d-m-Y', strtotime($value->date_service)); ?>
            </td>

            <td colspan="2" style="border: 1px solid black;">
                <?php echo ($value->service->name); ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo $value->total ?>
            </td>

            <td style="border: 1px solid black;">
                <?php echo $value->discount ?>
            </td>
        </tr>

        <?php
        $total = $total + $value->total;
        $discount = $discount + $value->discount;
    }
    ?>

    <tr style="border: 1px solid black;">
        <td>
            &nbsp;
        </td>

        <td colspan="2">
            &nbsp;
        </td>

        <td style="border: 1px solid black;">
            <strong>
                <?php echo $total ?>
            </strong>
        </td>

        <td style="border: 1px solid black;">
            <strong>
                <?php echo $discount ?>
            </strong>
        </td>
    </tr>

    <?php }?>

    <!--    До cплати : -->
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>

    <tr style="border: 1px solid black;">
        <td  colspan="3" style="text-align: right; padding-right: 10px; border: 1px solid black;">
            <strong>
                <?php echo Yii::t('app', 'Summary'); ?>:
            </strong>
        </td>

        <td >
            <strong>
                <?php echo Yii::t('app', 'Total'); ?>
            </strong>
        </td>

        <td>
            <strong>
                <?php echo Yii::t('app', 'Discount'); ?>
            </strong>
        </td>
    </tr>

    <?php
    $tot = $tot + $total;
    $dis = $dis + $discount;
    ?>
    <tr>
        <td>
            &nbsp;
        </td>

        <td colspan="2">
            &nbsp;
        </td>

        <td>
            <strong>
<?php echo $tot ?>
            </strong>

        </td>

        <td>
            <strong>
<?php echo $dis ?>
            </strong>

        </td>
    </tr>

</table>