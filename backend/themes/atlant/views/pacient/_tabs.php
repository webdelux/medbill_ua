<?php

use yii\helpers\Url;
use yii\bootstrap\Nav;
?>

<?= Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
    ],
    'items' => [
        [
            'label' => Yii::t('app', 'Total information'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 1],
        ],
        [
            'label' => Yii::t('app', 'Hospitalization') . '/' . Yii::t('app', 'Discharge'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 8],
        ],
        [
            'label' => Yii::t('app', 'Dispensary'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 6],
        ],
        [
            'label' => Yii::t('app', 'Signal mark'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 2],
        ],
        [
            'label' => Yii::t('app', 'Planning'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 4],
        ],
        [
            'label' => Yii::t('app', 'Consultations of experts'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 3],
        ],
        [
            'label' => Yii::t('app', 'Disease history'),
            'items' => [
                [
                    'label' => Yii::t('app', 'Diagnoses'),
                    'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 19],
                ],
                [
                    'label' => Yii::t('app', 'Surgeries'),
                    'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 14],
                ],
                [
                    'label' => Yii::t('app', 'Protocols'),
                    'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 15],
                ],
            ],
        ],
        [
            'label' => Yii::t('app', 'Incapacities'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 7],
        ],
        [
            'label' => Yii::t('app', 'Medical Supplies'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 9],
        ],
        [
            'label' => Yii::t('app', 'Others'),
            'items' => [
                [
                    'label' => Yii::t('app', 'Services'),
                    'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 10],
                ],
                [
                    'label' => Yii::t('app', 'Payments'),
                    'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 11],
                ],
                [
                    'label' => Yii::t('app', 'Insurance'),
                    'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 12],
                ],
                [
                    'label' => Yii::t('app', 'Pathological examination'),
                    'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 16],
                ],
            ],
        ],
        [
            'label' => Yii::t('app', 'Diary'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 17],
        ],
        [
            'label' => Yii::t('app', 'Attachments'),
            'url' => ['/pacient/view', 'id' => $model->id, 'tab' => 18],
        ],
        [
            'label' => Yii::t('app', 'Documents'),
            'url' => ['/pacient/reports', 'id' => $model->id, 'tab' => 20],
        ],
    ],
]) ?>

<!--
<ul class="nav nav-tabs" role="tablist">
    <li class="<?php echo ($tab == 1) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 1]); ?>">
            <?php echo Yii::t('app', 'Total information'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 8) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 8]); ?>">
            <?php echo Yii::t('app', 'Hospitalization') . '/' . Yii::t('app', 'Discharge'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 6) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 6]); ?>">
            <?php echo Yii::t('app', 'Dispensary'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 2) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 2]); ?>">
            <?php echo Yii::t('app', 'Signal mark'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 4) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 4]); ?>">
            <?php echo Yii::t('app', 'Planning'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 3) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 3]); ?>">
            <?php echo Yii::t('app', 'Consultations of experts'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 5) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/history', 'id' => $model->id, 'tab' => 5]); ?>">
            <?php echo Yii::t('app', 'Disease history'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 7) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 7]); ?>">
            <?php echo Yii::t('app', 'Incapacities'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 9) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 9]); ?>">
            <?php echo Yii::t('app', 'Medical Supplies'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 10) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 10]); ?>">
            <?php echo Yii::t('app', 'Services'); ?>
        </a>
    </li>
    <li class="<?php echo ($tab == 11) ? 'active' : ''; ?>">
        <a href="<?php echo Url::to(['/pacient/view', 'id' => $model->id, 'tab' => 11]); ?>">
            <?php echo Yii::t('app', 'Payments'); ?>
        </a>
    </li>
</ul>
-->