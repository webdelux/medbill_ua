<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use backend\modules\handbook\models\Status;
use backend\models\Pacient;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\web\Request;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PacientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pacients');
$this->params['breadcrumbs'][] = $this->title;

$sval = Yii::$app->getRequest()->get('PacientSearch');
$res = '';
if ($sval)
    foreach ($sval as $key => $value)
    {
        if ($value)
        {
            $res = $res . 'Pacient[' . $key . ']' . '=' . $value . '&';
        }
    }
?>

<div class="pacient-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Pacient'), ['create?' . $res], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Extended search'), ['#'], ['class' => 'btn btn-success', 'id' => 'e-search']) ?>
    </p>

    <div id="extended-search">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <?php Pjax::begin(['id' => 'pacient']); ?>

    <?=
    GridView::widget([
        'options' => [
            'id' => 'pacient-grid'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '50'],
            ],
            [
                'label' => Yii::t('app', 'Pacient'),
                'attribute' => 'fullName',
                //'contentOptions' => ['width' => '250'],
                'format' => 'raw',
                'value' => function ($data)
                    {
                        return (isset($data->fullName)) ? Html::a($data->fullName, ['view', 'id' => $data->id]) : '';
                    },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'fullName',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/pacient/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#pacient-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/pacient/index']) . "?"
                                . Html::getInputName($searchModel, 'id') . "=' + ui.item.id + '&"
                                . Html::getInputName($searchModel, 'fullName') . "=' + ui.item.value
                            });
                            jQuery('#pacient-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            [
                'attribute' => 'birthday',
                //'format' => ['date', 'dd MMM Y'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'd-m-Y'),
                ],
            ],
            [
                'attribute' => 'phone',
                'contentOptions' => ['width' => '130'],
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', '+380670000000'),
                ],
            ],
            [
                'label' => Yii::t('app', 'City'),
                'attribute' => 'cityName',
                //'contentOptions' => ['width' => '130'],
                'value' => function ($data)
                    {
                        return (isset($data->city->address)) ? $data->city->address : '';
                    },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'cityName',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/handbook/city/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#pacient-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/pacient/index']) . "?"
                                . Html::getInputName($searchModel, 'city_id') . "=' + ui.item.id + '&"
                                . Html::getInputName($searchModel, 'cityName') . "=' + ui.item.value
                            });
                            jQuery('#pacient-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            //'citizenship_id'
//            'address',
//            [
//                'attribute' => 'gender',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '50'],
//                'filter' => Pacient::itemAlias('gender'),
//                'value' => function ($data) {
//                    $res = $data::itemAlias('gender', ($data->gender) ? $data->gender : '');
//                    $res = StringHelper::truncate($res, 3, '.');
//                    return $res;
//                }
//            ],
//            [
//                'attribute' => 'city_type',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '50'],
//                'filter' => Pacient::itemAlias('city_type'),
//                'value' => function ($data) {
//                    return $data::itemAlias('city_type', ($data->city_type) ? $data->city_type : '');
//                }
//            ],
//            [
//                'attribute' => 'dispensary_group',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '50'],
//                'filter' => Pacient::itemAlias('yn'),
//                'value' => function ($data) {
//                    return $data::itemAlias('yn', ($data->dispensary_group) ? $data->dispensary_group : '');
//                }
//            ],
//            [
//                'attribute' => 'contingent',
//                'filter' => Pacient::itemAlias('contingent'),
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '50'],
//                'value' => function ($data) {
//                    return $data::itemAlias('contingent', ($data->contingent) ? $data->contingent : '');
//                }
//            ],
//            [
//                'label' => Yii::t('app', 'User'),
//                'attribute' => 'user',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
//                'value' => function ($data) {
//                    $res = (isset($data->userProfile->name)) ? $data->userProfile->name : '';
//                    $res .= ' (';
//                    $res .= (isset($data->user->username)) ? $data->user->username : '';
//                    $res .= ')';
//                    return $res;
//                },
//                'filter' => AutoComplete::widget([
//                    'model' => $searchModel,
//                    'attribute' => 'user',
//                    'options' => [
//                        'class' => 'form-control',
//                    ],
//                    'clientOptions' => [
//                        'source' => Url::to(['/user-viewer/index']),
//                        'select' => new JsExpression("function(event, ui) {
//                            jQuery('#pacient-grid').yiiGridView({
//                                'filterUrl': '" . Url::to(['/pacient/index']) . "?"
//                                . Html::getInputName($searchModel, 'user_id') . "=' + ui.item.id + '&"
//                                . Html::getInputName($searchModel, 'user') . "=' + ui.item.value
//                            });
//                            jQuery('#pacient-grid').yiiGridView('applyFilter');
//                        }"),
//                    ],
//                ]),
//            ],
//            [
//                'label' => Yii::t('app', 'Updated User'),
//                'attribute' => 'updatedUser',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
//                'value' => function ($data) {
//                    $res = (isset($data->updatedUserProfile->name)) ? $data->updatedUserProfile->name : '';
//                    $res .= ' (';
//                    $res .= (isset($data->updatedUser->username)) ? $data->updatedUser->username : '';
//                    $res .= ')';
//                    return $res;
//                },
//                'filter' => AutoComplete::widget([
//                    'model' => $searchModel,
//                    'attribute' => 'updatedUser',
//                    'options' => [
//                        'class' => 'form-control',
//                    ],
//                    'clientOptions' => [
//                        'source' => Url::to(['/user-viewer/index']),
//                        'select' => new JsExpression("function(event, ui) {
//                            jQuery('#pacient-grid').yiiGridView({
//                                'filterUrl': '" . Url::to(['/pacient/index']) . "?"
//                                . Html::getInputName($searchModel, 'updated_user_id') . "=' + ui.item.id + '&"
//                                . Html::getInputName($searchModel, 'updatedUser') . "=' + ui.item.value
//                            });
//                            jQuery('#pacient-grid').yiiGridView('applyFilter');
//                        }"),
//                    ],
//                ]),
//            ],
//            [
//                'attribute' => 'card_amb',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '100'],
//            ],
//            [
//                'attribute' => 'card_stac',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '100'],
//            ],
            [
                'attribute' => 'status_id',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'filter' => Status::childList(),
                'value' => function ($data)
                    {
                        return (isset($data->status->name)) ? $data->status->name : null;
                    }
            ],
            [
                'attribute' => 'created_at',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
            ],
//            [
//                'attribute' => 'updated_at',
//                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
//            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '50'],
                'template' => '{view} &nbsp {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                            return Html::a('<span class="fa fa-inbox"></span>', ['view', 'id' => $model->id],
                            [
                                'title' => Yii::t('app', 'Medical card'),
                                //'class'=>'btn btn-primary btn-xs',
                            ]);
                        },
                ],
            ],
        ],
    ]);
    ?>

<?php Pjax::end(); ?>

</div>


<?php $this->registerCss("

    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }

"); ?>


<?php $this->registerJs("
    $(document).ready(function() {
        var res = '';
        $('#extended-search :input').each(function(){
           res =  res + $(this).val();
        });
        if (res){
                $('#extended-search').show();
            } else {
                $('#extended-search').hide();
            }
    });

    $('#e-search' ).on( 'click', function( event ) {
        $('#extended-search').toggle();
        return false;
    });

    $(document).on('change', '#pacientsearch-user, #pacientsearch-updateduser, #pacientsearch-cityname, #pacientsearch-fullname', function(event) {
        if (!$(this).val())
        {
            jQuery('#pacient-grid').yiiGridView({'filterUrl': '" . Url::to(['/pacient/index']) . "'});
            jQuery('#pacient-grid').yiiGridView('applyFilter');
        }
    });

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

"); ?>
