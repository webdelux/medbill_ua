<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = $model->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo Alert::widget(); ?>

<div class="pacient-history">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <div class="panel panel-default tabs">

        <?php echo $this->render('_tabs', ['model' => $model, 'tab' => $tab]); ?>

        <div class="panel-body tab-content tab-content-<?php echo $tab; ?>">
            <div class="tab-pane active">

                <?php echo $this->render('tabs/_tab-' . $tab, ['model' => $model, 'models' => $models, 'form' => $form]); ?>

            </div>
        </div>
        <div class="panel-footer">

            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php echo $this->render('//modules/handbookemc/views/icd/_selector'); ?>

<?php $this->registerCss("

    // ...

"); ?>

<?php $this->registerJsFile('@web/themes/atlant/js/plugins/chained/jquery.chained.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJs("

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

    $(document).on('click', 'a[data-delete]', function(event) {
        event.preventDefault();
        if (confirm($(this).attr('data-delete')))
        {
            var container = $(this).attr('data-container');

            $.ajax({
                method: 'POST',
                url: $(this).attr('href')
            })
            .done(function(data) {
                $.pjax.reload({
                    container: container
                });
            });
        }
    })

    function initWidgetDiagnosisAdditional(id)
    {
        jQuery.when(jQuery('#diagnosisadditional-handbook_emc_icd_id-complication-id' + id).select2(select2_d35d49a5)).done(initS2Loading('diagnosisadditional-handbook_emc_icd_id-complication-id' + id,'s2options_6cc131ae'));
        jQuery('#diagnosisadditional-date_at-complication-id' + id + '-kvdate').kvDatepicker(kvDatepicker_493f0995);

        jQuery.when(jQuery('#diagnosisadditional-handbook_emc_icd_id-concomitant-id' + id).select2(select2_d35d49a5)).done(initS2Loading('diagnosisadditional-handbook_emc_icd_id-concomitant-id' + id,'s2options_6cc131ae'));
        jQuery('#diagnosisadditional-date_at-concomitant-id' + id + '-kvdate').kvDatepicker(kvDatepicker_493f0995);
    }

    function initWidgetPacientOperation(id)
    {
        jQuery('#pacientoperation-start_operation_date-operation-id' + id + '-kvdate').kvDatepicker(kvDatepicker_493f0995);
        jQuery('#pacientoperation-date_at-operation-id' + id + '-kvdate').kvDatepicker(kvDatepicker_493f0995);

        jQuery('#pacientoperation-start_operation_time-operation-id' + id).timepicker(timepicker_4f34c80a);

        jQuery.when(jQuery('#pacientoperation-handbook_emc_icd_id-operation-id' + id).select2(select2_d35d49a5)).done(initS2Loading('pacientoperation-handbook_emc_icd_id-operation-id' + id,'s2options_6cc131ae'));
        jQuery.when(jQuery('#pacientoperation-handbook_emc_surgery_id-operation-id' + id).select2(select2_dfc66292)).done(initS2Loading('pacientoperation-handbook_emc_surgery_id-operation-id' + id,'s2options_6cc131ae'));
        jQuery.when(jQuery('#pacientoperation-surgeon_user_id-operation-id' + id).select2(select2_d1bcdbad)).done(initS2Loading('pacientoperation-surgeon_user_id-operation-id' + id,'s2options_6cc131ae'));
        jQuery.when(jQuery('#pacientoperation-anesthetist_user_id-operation-id' + id).select2(select2_3c7a5207)).done(initS2Loading('pacientoperation-anesthetist_user_id-operation-id' + id,'s2options_6cc131ae'));
        jQuery.when(jQuery('#pacientoperation-handbook_emc_complication_id-operation-id' + id).select2(select2_0b9c4884)).done(initS2Loading('pacientoperation-handbook_emc_complication_id-operation-id' + id,'s2options_6cc131ae'));
        jQuery.when(jQuery('#pacientoperation-department_id-operation-id' + id).select2(select2_4b5010b9)).done(initS2Loading('pacientoperation-department_id-operation-id' + id,'s2options_6cc131ae'));
    }

    function setValueFieldByProtocolDate(id, date)
    {
        $('#protocol-protocol_date-id' + id).val(date.format('YYYY-MM-DD HH:mm'));
        $('#modal-diagnosis-template-protocol-calendar-id' + id).modal('hide');
    }

    function initWidgetFullCalendar(id)
    {
        // Chained Selects Plugin
        $('#protocol-doctor_id-id' + id).chained('#protocol-department_id-id' + id);
        $('#protocol-doctor_id-fc' + id).chained('#protocol-department_id-fc' + id);

        $('#modal-diagnosis-template-protocol-calendar-id' + id).on('hidden.bs.modal', function (e) {

            $('#protocol-category_id-id' + id).val($('#protocol-category_id-fc' + id).val());
            $('#protocol-department_id-id' + id).val($('#protocol-department_id-fc' + id).val());

            // Chained Selects Plugin
            $('#protocol-department_id-id' + id).trigger('change');
            $('#protocol-doctor_id-id' + id).val($('#protocol-doctor_id-fc' + id).val());
        });

        $('#modal-diagnosis-template-protocol-calendar-id' + id).on('shown.bs.modal', function (e) {

            $('#protocol-category_id-fc' + id).val($('#protocol-category_id-id' + id).val());
            $('#protocol-department_id-fc' + id).val($('#protocol-department_id-id' + id).val());

            // Chained Selects Plugin
            $('#protocol-department_id-fc' + id).trigger('change');
            $('#protocol-doctor_id-fc' + id).val($('#protocol-doctor_id-id' + id).val());

            $('#diagnosis-template-protocol-calendar-id' + id).fullCalendar('render');
        });

        $(document).on('change', '#modal-filter-diagnosis-template-protocol-calendar-id' + id + ' :input', function(event) {
            //$('#diagnosis-template-protocol-calendar-id' + id).fullCalendar('refetchEvents');
        })

        $(document).on('click', '#protocol-id-btn-refresh-fc' + id, function(event) {
            $('#diagnosis-template-protocol-calendar-id' + id).fullCalendar('refetchEvents');
        })
    }

    function initWidgetProtocol(id)
    {
        jQuery('#protocol-protocol_date-id' + id + '-datetime').datetimepicker(datetimepicker_f3636cdf);

        jQuery.when(jQuery('#protocol-pacient_id-id' + id).select2(select2_12483548)).done(initS2Loading('protocol-pacient_id-id' + id,'s2options_6cc131ae'));
        jQuery.when(jQuery('#protocol-protocol_id-id' + id).select2(select2_4f605300)).done(initS2Loading('protocol-protocol_id-id' + id,'s2options_6cc131ae'));
        jQuery.when(jQuery('#protocol-refferal_id-id' + id).select2(select2_fde3f179)).done(initS2Loading('protocol-refferal_id-id' + id,'s2options_6cc131ae'));

        if ($('#diagnosis-template-protocol-calendar-id' + id).length)
        {
            jQuery('#diagnosis-template-protocol-calendar-id' + id).fullCalendar({
                'loading': function (isLoading, view) {
                    jQuery('#diagnosis-template-protocol-calendar-id' + id).find('.fc-loading').toggle(isLoading);
                },
                'allDaySlot': false,
                'firstDay': 1,
                'defaultView': 'agendaDay',
                'minTime': '08:00',
                'maxTime': '22:00',
                'slotDuration': '00:20:00',
                'defaultTimedEventDuration': '00:20:00',
                'slotLabelFormat': 'HH:mm',
                'timezone': '" . Yii::$app->timeZone . "',
                'height': 'auto',
                'contentHeight': 'auto',
                'fixedWeekCount': false,
                'displayEventTime': false,
                'eventRender': function (event, element, view) {
                    element.popover({
                        title: event.title,
                        placement: 'bottom',
                        trigger: 'hover',
                        content: event.description,
                        container: 'body'
                    });
                },
                'eventClick': function (event, jsEvent, view) {
                    if (event.url) {
                        window.open(event.url);
                        return false;
                    }
                },
                'dayClick': function (date, jsEvent, view, resourceObj) {
                    setValueFieldByProtocolDate(id, date);
                },
                'events': {
                    url: '" . Url::to(['/protocol/calendar']) . "',
                    data: function () {
                        return {
                            doctor_id: $('#protocol-doctor_id-fc' + id).val(),
                            department_id: $('#protocol-department_id-fc' + id).val(),
                            category_id: $('#protocol-category_id-fc' + id).val()
                        };
                    }
                },
                'header': {
                    'center': 'title',
                    'left': 'prev,next today',
                    'right': 'month,agendaWeek,agendaDay'
                }
            });

            initWidgetFullCalendar(id);
        }
    }

    $(document).on('click', 'button[data-save]', function(event) {

        var form = $(this).closest('form');
        var modal = $(this).attr('data-modal') + ' .modal-body';
        var container = $(this).attr('data-container');
        var id = $(this).attr('data-id');

        $.ajax({
            method: 'POST',
            url: '" . Url::to() . "',
            data: $(modal).find('input, textarea, select').serialize(),
            dataType: 'html'
        })
        .done(function(data) {
            $(modal).html($(data).find(modal).html());
            $(modal).find('input').prop('disabled', false);

            initWidgetDiagnosisAdditional(id);
            initWidgetPacientOperation(id);
            initWidgetProtocol(id);

            $.pjax.reload({
                container: container
            });
        });
    })

    // Показати/Заховати
    $(document).on('click', 'fieldset > legend > a', function(event) {
        $(this).parent().parent().parent().find('fieldset > div').toggle();
        event.preventDefault();
    })

"); ?>