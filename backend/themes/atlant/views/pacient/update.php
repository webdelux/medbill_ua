<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pacient */

$this->title = Yii::t('app', 'Update') . ' ' .Yii::t('app', 'pacient');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fullName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="pacient-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>