<?= \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'surname',
        'lastname',
        [
            'attribute' => 'birthday',
            'format' => ['date', 'dd MMM Y'],
        ],
        'ipn',
        'phone',
        'phone1',
        [
          'attribute' => 'citizenship_id',
            'value' => (isset($model->country->title)) ? $model->country->title : null
        ],
        [
          'attribute' => 'city_type',
            'value' => $model::itemAlias('city_type', ($model->city_type) ? $model->city_type : '')
        ],
        [
          'attribute' => 'city_id',
            'value' => (isset($model->city->address)) ? $model->city->address : null
        ],
        'address',
        [
          'attribute' => 'gender',
            'value' => $model::itemAlias('gender', ($model->gender) ? $model->gender : '')
        ],
        'work',
        'seat',
        [
          'attribute' => 'dispensary_group',
            'value' => $model::itemAlias('yn', ($model->dispensary_group) ? $model->dispensary_group : '')
        ],
        [
          'attribute' => 'contingent',
            'value' => $model::itemAlias('contingent', ($model->contingent) ? $model->contingent : '')
        ],
        [
          'attribute' => 'docum_type',
            'value' => $model::itemAlias('docum', ($model->docum_type) ? $model->docum_type : '')
        ],
        'docum_number',
        'memo:ntext',
        [
            'attribute' => 'user_id',
            'value' => (isset($model->user->username)) ? $model->user->username : null
        ],
        [
            'attribute' => 'updated_user_id',
            'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
        ],
        'created_at:datetime',
        'updated_at:datetime',
        [
          'attribute' => 'card_amb',
            'value' => $model::itemAlias('card_amb', ($model->card_amb) ? $model->card_amb : '')
        ],
        [
          'attribute' => 'card_stac',
            'value' => $model::itemAlias('card_stac', ($model->card_stac) ? $model->card_stac : '')
        ],
        [
            'attribute' => 'status_id',
            'value' => (isset($model->status->name)) ? $model->status->name : null
        ],
    ],
]) ?>