<?php

use yii\helpers\Html;
?>

<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($models['PacientSignalmark'], 'id') ?>
<?= Html::activeHiddenInput($models['PacientPlanning'], 'id') ?>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->render('forms/_form-dispensary', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

    </div>
</div>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

    // ...

"); ?>