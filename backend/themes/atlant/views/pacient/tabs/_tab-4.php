<?php

use yii\helpers\Html;
?>

<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($models['PacientSignalmark'], 'id') ?>
<?= Html::activeHiddenInput($models['PacientPlanning'], 'id') ?>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->render('forms/_form-planning-survey', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-planning-speciality', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-planning-treatment', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <!--
        <fieldset class="form-group">
            <legend class="form-heading">
                <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
                    <?php echo Yii::t('app', 'Plan treatment'); ?>
                </a>
            </legend>
            <?php $countRow = ($models['PacientPlanning']->treatment || $models['PacientPlanning']->treatment) ? true : false; ?>
            <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
                <div class="col-md-12">

                    <?= $form->field($models['PacientPlanning'], 'treatment', [])->textarea(['rows' => 2])->label(false) ?>

                </div>
            </div>
        </fieldset>
        -->

    </div>
</div>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

    // ...

"); ?>