<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\Pjax;

use yii\data\ActiveDataProvider;

use yii\bootstrap\Modal;
use yii\bootstrap\Alert;

use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\select2\Select2;

use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\diagnosis\models\DiagnosisAdditional;
use backend\modules\handbook\models\Department;

$form->layout = 'horizontal';
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Basic'); ?>
        </a>
    </legend>
    <div class="row form-body">
        <div class="col-md-12">

            <div class="form-horizontal">

                <br/>

                <?= $form->field($models['Diagnosis'], 'department_id')->dropDownList((new Department)->getTreeListByUserId()) ?>

                <?= $form->field($models['Diagnosis'], 'handbook_emc_icd_id', [
                    'template' => '{label}'
                    . '<div class="col-sm-6">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#selector-handbook-emc-icd-modal',
                                    'data-parent' => Html::getInputId($models['Diagnosis'], 'handbook_emc_icd_id'),
                                ])
                            . '</span>'
                        . '</div>'
                        . '{error}'
                        . '{hint}'
                    . '</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['Diagnosis']->icd) ? '' : $models['Diagnosis']->icd->name,
                    'options' => [
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?php if ($models['Diagnosis']->id): ?>
                    <?php $diagnosis = Diagnosis::find()->where(['pacient_id' => $models['Diagnosis']->pacient_id, 'parent_id' => $models['Diagnosis']->id])->orderBy('id DESC')->all(); ?>
                    <?php if (count($diagnosis)): ?>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <ul class="diagnosis-archive">
                                    <?php $check = [$models['Diagnosis']->type => $models['Diagnosis']->id]; ?>
                                    <?php foreach ($diagnosis as $value): ?>

                                        <?php
                                        // Перекреслюються діагнози тільки тоді, коли є такий самий тип але із пізнішою датою створення
                                        $through = false;
                                        if (isset($check[$value->type]))
                                            $through = true;
                                        else
                                            $check[$value->type] = $value->id;
                                        ?>

                                        <li class="<?php echo ($through) ? 'through' : ''; ?>">
                                            <?php echo date("d.m.Y H:i", strtotime($value->created_at)); ?>
                                            <?php echo (isset($value->icd->name)) ? $value->icd->name : null; ?>
                                            (<?php echo Diagnosis::itemAlias("type", $value->type); ?>)
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

                <?= $form->field($models['Diagnosis'], 'alternative')->textarea([
                    'rows' => 2,
                ]) ?>

                <?= $form->field($models['Diagnosis'], 'alternative_print')->radioList($models['Diagnosis']::itemAlias('alternative_print'), [
                    'prompt' => '',
                ]) ?>

                <?= $form->field($models['Diagnosis'], 'type')->radioList($models['Diagnosis']::itemAlias('type'), [
                    'prompt' => '',
                ]) ?>

                <?= $form->field($models['Diagnosis'], 'first_set')->radioList($models['Diagnosis']::itemAlias('first_set'), [
                    'prompt' => '',
                ]) ?>

                <?= $form->field($models['Diagnosis'], 'prophylactic_set')->radioList($models['Diagnosis']::itemAlias('prophylactic_set'), [
                    'prompt' => '',
                ]) ?>

                <?= $form->field($models['Diagnosis'], 'trauma')->radioList($models['Diagnosis']::itemAlias('trauma'), [
                    'prompt' => '',
                ]) ?>

                <?= $form->field($models['Diagnosis'], 'date_at')->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Diagnosis']->date_at != '0000-00-00') ? $models['Diagnosis']->date_at : date('Y-m-d')
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

            </div>

        </div>
    </div>
</fieldset>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Complications'); ?>
        </a>
    </legend>
    <div class="row form-body">
        <div class="col-md-12">

            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => "modal-diagnosis-complication",
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Complications') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['DiagnosisAdditional_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['DiagnosisAdditional_alert']['type']
                    ],
                    'body' => $models['DiagnosisAdditional_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => "form-diagnosis-complication",
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?php /* echo $form->field($models['DiagnosisAdditional'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList(ArrayHelper::map(Diagnosis::find()->where(['pacient_id' => $models['Diagnosis']->pacient_id])->all(), 'id', 'icd.name'), [
                    'prompt' => '',
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'diagnosis_id') . "-complication",
                ]) */ ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'diagnosis_id', [
                    'value' => $models['Diagnosis']->id,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'diagnosis_id') . "-complication",
                ]) ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'type', [
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'type') . "-complication",
                    'value' => DiagnosisAdditional::TYPE_COMPLICATION,
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'handbook_emc_icd_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['DiagnosisAdditional']->icd) ? '' : $models['DiagnosisAdditional']->icd->name,
                    'options' => [
                        'placeholder' => '',
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'handbook_emc_icd_id') . "-complication",
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?= $form->field($models['DiagnosisAdditional'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea([
                    'rows' => 2,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'description') . "-complication",
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'date_at') . "-complication",
                        'value' => ($models['DiagnosisAdditional']->date_at) ? $models['DiagnosisAdditional']->date_at : date('Y-m-d'),
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => "#modal-diagnosis-complication",
                            'data-container' => "#diagnosis-complication",
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => "diagnosis-complication",
                'linkSelector' => "#diagnosis-complication a[data-sort], #diagnosis-complication a[data-page]",
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => "grid-diagnosis-complication"
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => DiagnosisAdditional::find()->joinWith('diagnosis')->where([
                        'diagnosis_id' => $models['Diagnosis']->id,
                        '{{%diagnosis_additional}}.type' => DiagnosisAdditional::TYPE_COMPLICATION,
                    ])->orderBy([
                        'date_at' => SORT_ASC,
                    ]),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    /*
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data) {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
                    */
                    [
                        'attribute' => 'handbook_emc_icd_id',
                        'value' => function ($data) {
                            return (isset($data->icd->name)) ? $data->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/diagnosis/diagnosis-additional/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => "#diagnosis-complication",
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Concomitant'); ?>
        </a>
    </legend>
    <div class="row form-body">
        <div class="col-md-12">

            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => "modal-diagnosis-concomitant",
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Concomitant') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['DiagnosisAdditional_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['DiagnosisAdditional_alert']['type']
                    ],
                    'body' => $models['DiagnosisAdditional_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => "form-diagnosis-concomitant",
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?php /* echo $form->field($models['DiagnosisAdditional'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList(ArrayHelper::map(Diagnosis::find()->where(['pacient_id' => $models['Diagnosis']->pacient_id])->all(), 'id', 'icd.name'), [
                    'prompt' => '',
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'diagnosis_id') . "-concomitant",
                ]) */ ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'diagnosis_id', [
                    'value' => $models['Diagnosis']->id,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'diagnosis_id') . "-concomitant",
                ]) ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'type', [
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'type') . "-concomitant",
                    'value' => DiagnosisAdditional::TYPE_CONCOMITANT,
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'handbook_emc_icd_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['DiagnosisAdditional']->icd) ? '' : $models['DiagnosisAdditional']->icd->name,
                    'options' => [
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'handbook_emc_icd_id') . "-concomitant",
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?= $form->field($models['DiagnosisAdditional'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea([
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'description') . "-concomitant",
                    'rows' => 2,
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'date_at') . "-concomitant",
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => "#modal-diagnosis-concomitant",
                            'data-container' => "#diagnosis-concomitant",
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => "diagnosis-concomitant",
                'linkSelector' => "#diagnosis-concomitant a[data-sort], #diagnosis-concomitant a[data-page]",
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => "grid-diagnosis-concomitant"
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => DiagnosisAdditional::find()->joinWith('diagnosis')->where([
                        'diagnosis_id' => $models['Diagnosis']->id,
                        '{{%diagnosis_additional}}.type' => DiagnosisAdditional::TYPE_CONCOMITANT,
                    ])->orderBy([
                        'date_at' => SORT_ASC,
                    ]),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    /*
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data) {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
                    */
                    [
                        'attribute' => 'handbook_emc_icd_id',
                        'value' => function ($data) {
                            return (isset($data->icd->name)) ? $data->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/diagnosis/diagnosis-additional/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => "#diagnosis-concomitant",
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

    .form-group.field-diagnosis-handbook_emc_icd_id {
        margin-bottom: 10px;
    }
    .form-group ul.diagnosis-archive {
        margin: 0;
        padding: 0;
        list-style: none inside;
    }
    .form-group ul.diagnosis-archive li.through {
        text-decoration: line-through;
    }

"); ?>


<?php $this->registerJs("

    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            selectInit('diagnosisadditional-handbook_emc_icd_id-complication');
            selectInit('diagnosisadditional-handbook_emc_icd_id-concomitant');
        }

    });

    $('#modal-diagnosis-complication').on('hidden.bs.modal', function (e) {
        $(this).find('input, textarea, select').prop('disabled', true);
    });
    $('#modal-diagnosis-complication').on('shown.bs.modal', function (e) {
        $(this).find('input, textarea, select').prop('disabled', false);
        $('#modal-diagnosis-concomitant').find('input, textarea, select').prop('disabled', true);
    });

    $('#modal-diagnosis-concomitant').on('hidden.bs.modal', function (e) {
        $(this).find('input, textarea, select').prop('disabled', true);
    });
    $('#modal-diagnosis-concomitant').on('shown.bs.modal', function (e) {
        $(this).find('input, textarea, select').prop('disabled', false);
        $('#modal-diagnosis-complication').find('input, textarea, select').prop('disabled', true);
    });

"); ?>