<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\select2\Select2;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\emc\models\Hospitalization;
use backend\modules\handbook\models\Department;

$countRow = Hospitalization::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Hospitalization') . ' / ' . Yii::t('app', 'Discharge'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-diagnosis',
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Hospitalization') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['Diagnosis_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['Diagnosis_alert']['type']
                    ],
                    'body' => $models['Diagnosis_alert']['body']
                ]) : '' ?>

                <?= (isset($models['Hospitalization']->modelAlert)) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-danger'
                    ],
                    'body' => $models['Hospitalization']->modelAlert
                ]) : '' ?>



                <?php Pjax::begin([
                    'id' => 'form-diagnosis',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['Diagnosis'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= Html::activeHiddenInput($models['Diagnosis'], 'type', [
                    'value' => $models['Diagnosis']::TYPE_PREVIOUS
                ]) ?>

                <?= Html::activeHiddenInput($models['Hospitalization'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= Html::activeHiddenInput($models['Hospitalization'], 'diagnosis_id', [
                    'value' => 0
                ]) ?>

                <?= Html::activeHiddenInput($models['Diagnosis'], 'date_at', [
                    'value' => 0
                ]) ?>

                <?= $form->field($models['Hospitalization'], 'hospitalization_date', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9 datetime-picker">{input}{error}{hint}</div>'
                ])->widget(kartik\datetime\DateTimePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00 hh:ii',
                        'value' => ($models['Hospitalization']->hospitalization_date) ? $models['Hospitalization']->hospitalization_date : date("Y-m-d H:i")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii'
                    ]
                ]) ?>

                <?= $form->field($models['Diagnosis'], 'handbook_emc_icd_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['Diagnosis']->icd) ? '' : $models['Diagnosis']->icd->name,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ])->label(Yii::t('app', 'Diagnosis')); ?>

                <?= $form->field($models['Diagnosis'], 'first_set', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->radioList($models['Diagnosis']::itemAlias('first_set'), ['prompt' => '']) ?>

                <?= $form->field($models['Diagnosis'], 'prophylactic_set', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->radioList($models['Diagnosis']::itemAlias('prophylactic_set'), ['prompt' => '']) ?>

                <?= $form->field($models['Diagnosis'], 'trauma', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->radioList($models['Diagnosis']::itemAlias('trauma'), ['prompt' => '']) ?>

                <?= $form->field($models['Hospitalization'], 'department_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList((new Department)->treeList(),['prompt' => '']) ?>

                <?= $form->field($models['Hospitalization'], 'refferal_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['Hospitalization']->refferal->pib) ? '' : $models['Hospitalization']->refferal->pib,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/handbook/refferal/index?results=true']),
                            'dataType' => 'json',
                        ]
                    ],
                ])->label(Yii::t('app', 'Refferal')); ?>

                <?= $form->field($models['Hospitalization'], 'hospitalization_type', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList($models['Hospitalization']::itemAlias('hospitalization_type'), ['prompt' => '']) ?>

                <?= $form->field($models['Hospitalization'], 'hospitalization_interval', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textInput(['maxlength' => true]) ?>

                <?= $form->field($models['Hospitalization'], 'diagnosis_in', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textInput(['maxlength' => true]) ?>

                <?= $form->field($models['Hospitalization'], 'first_hospitalization', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList([1 => Yii::t('app', 'First time'), 2 => Yii::t('app', 'Repeatedly') ],['prompt' => '']) ?>



                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-diagnosis',
                            'data-container' => '#diagnosis',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'diagnosis',
                'linkSelector' => '#diagnosis a[data-sort], #diagnosis a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'diagnosis-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => Hospitalization::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'hospitalization_date' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'headerOptions' => ['width' => '50', 'style' => 'white-space:normal']
                    ],
                    [
                        'attribute' => 'hospitalization_date',
                        'headerOptions' => ['width' => '100', 'style' => 'white-space:normal']
                    ],
                    [
                        'attribute' => 'hospitalization_date_out',
                        'contentOptions' => function ($data)
                        {
                            return ['class' => ($data->hospitalization_date_out == 0) ? 'success' : 'danger'];
                        },
                        'headerOptions' => ['width' => '100', 'style' => 'white-space:normal']
                    ],
                    [
                        'attribute' => 'department_id',
                        'value' => function ($data)
                        {
                            return (isset($data->department->name)) ? $data->department->name : '';
                        },
                    ],
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data) {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
//                    [
//                        'attribute' => 'refferal_id',
//                        'value' => function ($data)
//                        {
//                            return (isset($data->refferal->pib)) ? $data->refferal->pib . ' ('.$data->refferal->category->name.')' : '';
//                        },
//
//                    ],
//                    [
//                        'attribute' => 'hospitalization_type',
//                        'value' => function ($data) {
//                            return $data::itemAlias('hospitalization_type', $data->hospitalization_type);
//                        },
//                    ],
//                    [
//                        'attribute' => 'hospitalization_interval',
//                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
//                    ],
//                                [
//                        'attribute' => 'diagnosis_in',
//                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
//                    ],
                    [
                        'attribute' => 'diagnosis_end_id',
                        'value' => function ($data) {
                            return (isset($data->diagnosisEnd->icd->name)) ? $data->diagnosisEnd->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'first_set',
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'label' => Yii::t('app', 'Newly diagnosed'),
                        'value' => function ($data) {
                            if (isset($data->diagnosis->first_set))
                                $first_set = $data->diagnosis->first_set;

                            if (isset($data->diagnosisEnd->first_set))
                                $first_set = $data->diagnosisEnd->first_set;

                            return (isset($first_set)) ? (($first_set == 2) ? Yii::t('app', 'No') : Yii::t('app', 'Yes')) : '';
                        },
                    ],
                    [
                        'attribute' => 'prophylactic_set',
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'label' => Yii::t('app', 'Established in routine inspection'),
                        'value' => function ($data) {
                            if (isset($data->diagnosis->prophylactic_set))
                                $prophylactic_set = $data->diagnosis->prophylactic_set;

                            if (isset($data->diagnosisEnd->prophylactic_set))
                                $prophylactic_set = $data->diagnosisEnd->prophylactic_set;

                            return (isset($prophylactic_set)) ? (($prophylactic_set == 2) ? Yii::t('app', 'No') : Yii::t('app', 'Yes')) : '';
                        },
                    ],
                    [
                        'attribute' => 'trauma',
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'label' => Yii::t('app', 'Trauma'),
                        'value' => function ($data) {
                            return (isset($data->diagnosis->trauma)) ?  $data::itemAlias('trauma', $data->diagnosis->trauma) : '';
                        },
                    ],
                    [
                        'attribute' => 'cure_result',
                        'value' => function ($data)
                        {
                            return isset($data->cure_result) ? $data::itemAlias('cure_result', $data->cure_result) : '';
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{release} {edit} {delete}',
                        'buttons' => [
                            'release' => function ($url, $data, $key) {
                                if ($data->hospitalization_date_out == 0){
                                    return Html::a('<span class="fa fa-share"></span>',
                                        Url::toRoute([
                                            '/emc/hospitalization/update',
                                            'type' => 2,
                                            'id' => $data->id,
                                            'tab' =>8
                                        ]),
                                        [
                                            'title' => Yii::t('app', 'Release'),
                                            'data-container' => '#stacionary',
                                        ]
                                    );
                                }
                            },
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/emc/hospitalization/update',
                                        'id' => $data->id,
                                        'tab' =>8
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Edit'),
                                        'data-container' => '#stacionary',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/emc/hospitalization/delete',
                                        'id' => $data->id,
                                        'diagnosis' => $data->diagnosis_id,
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#diagnosis',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

    .field-diagnosis-first_set .control-label,
    .field-diagnosis-prophylactic_set .control-label {
        line-height: normal;
    }

"); ?>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    function dateInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-datetimepicker');
            jQuery('#' + itemname).datetimepicker(window[settings]);

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2
            selectInit('diagnosis-handbook_emc_icd_id');
            selectInit('hospitalization-refferal_id');

            dateInit('hospitalization-hospitalization_date');
        }

    });

"); ?>