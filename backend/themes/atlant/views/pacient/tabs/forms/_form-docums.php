<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

use backend\modules\diagnosis\models\Diagnosis;
use backend\models\PacientOperation;
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Documents'); ?>
        </a>
    </legend>
    <div class="row form-body" >
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-docum',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Documents') . '</h4>',
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <div id="diagnosis" class="form-group required" style="display: none">
                    <label class="control-label" for="diagnosis"><?php echo Yii::t('app', 'Diagnosis'); ?></label>

                    <?=
                        Html::dropDownList('diagnosis', null, ArrayHelper::map(Diagnosis::find()
                                            ->andWhere(['pacient_id' => $model->id])
                                            ->andWhere(['parent_id' => NULL])
                                            ->all(), 'id', 'icd.name', 'date_at'), ['prompt' => '', 'required' => 'required',
                        'class' => 'form-control', 'id' => 'diagnosis'])
                    ?>

                    <div class="help-block help-block-error" style="display: none;"><?php echo Yii::t('app', 'Choose diagnosis'); ?></div>

                </div>

                <div id="surgery" class="form-group required" style="display: none">
                    <label class="control-label" for="surgery"><?php echo Yii::t('app', 'Operation'); ?></label>

                    <?php
                    $surgery = PacientOperation::find()
                            ->innerJoin('{{%diagnosis}} AS diagnosis', 'diagnosis.id = {{%pacient_operation}}.diagnosis_id')
                            ->andWhere(['diagnosis.pacient_id' => $model->id])
                            ->all();

                    $surgeryOptions = [];

                    foreach ($surgery as $value)
                        $surgeryOptions[$value->id] = ['class' => $value->diagnosis_id];

                        echo Html::dropDownList('surgery', null, ArrayHelper::map($surgery, 'id', 'surgery.name'),
                            ['prompt' => '', 'required' => 'required',
                            'class' => 'form-control', 'id' => 'surgery', 'options' => $surgeryOptions])
                    ?>

                    <div class="help-block help-block-error" style="display: none;"><?php echo Yii::t('app', 'Choose surgery'); ?></div>

                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::a(Yii::t('app', 'Print'),
                            ['reports', 'id' => $model->id],
                            ['id' =>'print_reports','class' => 'btn btn-primary print'])
                        ?>
                    </div>
                </div>

                <?php Modal::end(); ?>

            </div>

            <ul>
                <li>
                    <?= Html::a(Yii::t('app', 'Invoice'),
                        ['reports'],
                        [
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-docum',
                            'data-report' => '1',
                            'data-title' => Yii::t('app', 'Print') . ' ' . Yii::t('app', 'Invoice'),
                        ])
                    ?>
                </li>

                <li>
                    <?= Html::a(Yii::t('app', 'form066o'),
                        ['reports'],
                        [
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-docum',
                            'data-report' => '3',
                            'data-title' => Yii::t('app', 'Print') . ' ' . Yii::t('app', 'form066o'),
                        ])
                    ?>
                </li>

                <li>
                    <?= Html::a(Yii::t('app', 'Referral to microbiological research'),
                        ['reports'],
                        [
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-docum',
                            'data-report' => '4',
                            'data-title' => Yii::t('app', 'Print') . ' ' . Yii::t('app', 'Referral to microbiological research'),
                        ])
                    ?>
                </li>

                <li>
                    <?= Html::a(Yii::t('app', 'Epidemiological surveillance map'),
                        ['reports',],
                        [
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-docum',
                            'data-report' => '5',
                            'data-title' => Yii::t('app', 'Print') . ' ' . Yii::t('app', 'Epidemiological surveillance map'),
                        ])
                    ?>
                </li>

                <li>
                    <?= Html::a(Yii::t('app', 'Pathological examination _report1'),
                        ['reports', 'id' => $model->id, 'report' => 2],
                        ['target' => '_blank'])
                    ?>
                </li>
            </ul>

            <hr/>

            <ul>
                <li>
                    <?= Html::a(Yii::t('app', 'Add') . ' ' . \common\helpers\StringHelper::strToLower(Yii::t('app', 'Preoperative epicrisis')), [
                        '/emc/preoperative-epicrisis/create', 'pacient_id' => $model->id],
                    ['target' => '_blank']) ?>
                </li>
                <li>
                    <?= Html::a(Yii::t('app', 'Add') . ' ' . \common\helpers\StringHelper::strToLower(Yii::t('app', 'Epitome epicrisis')), [
                        '/emc/epitome-epicrisis/create', 'pacient_id' => $model->id],
                    ['target' => '_blank']) ?>
                </li>
            </ul>

            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th style="width: 25%;">
                            <?= Yii::t('app', 'Docum Type') ?>
                        </th>
                        <th>
                            <?= Yii::t('app', 'Diagnosis') ?>
                        </th>
                        <th style="width: 100px;">
                            <?= Yii::t('app', 'Date') ?>
                        </th>
                        <th style="width: 50px;">
                            <?= Yii::t('app', 'Actions') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <?php $pEpicrisis = backend\modules\emc\models\PreoperativeEpicrisis::find()->joinWith('diagnosis')->andWhere(['pacient_id' => $model->id])->all(); ?>
                    <?php if ($pEpicrisis): ?>
                        <?php foreach ($pEpicrisis as $key => $value): ?>
                            <tr>
                                <td><?= Yii::t('app', 'Preoperative epicrisis') ?></td>
                                <td><?= (isset($value->diagnosis->icd->name)) ? $value->diagnosis->icd->name : null ?></td>
                                <td><?= $value->start_operation_date ?></td>
                                <td>
                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', [
                                        '/emc/preoperative-epicrisis/update', 'id' => $value->id], [
                                        'title' => Yii::t('app', 'Update'),
                                    ]) ?>

                                    &nbsp;

                                    <?= Html::a('<span class="glyphicon glyphicon-print"></span>', [
                                        '/emc/preoperative-epicrisis/print', 'id' => $value->id], [
                                        'title' => Yii::t('app', 'Print'),
                                        'target' => '_blank',
                                    ]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="4">
                                <?= Yii::t('app', 'No records') ?>
                            </td>
                        </tr>
                    <?php endif; ?>

                    <?php $eEpicrisis = backend\modules\emc\models\EpitomeEpicrisis::find()->joinWith('diagnosis')->andWhere(['{{%diagnosis}}.pacient_id' => $model->id])->all(); ?>
                    <?php //yii\helpers\VarDumper::dump($eEpicrisis,10,2);?>
                    <?php if ($eEpicrisis): ?>
                        <?php foreach ($eEpicrisis as $key => $value): ?>
                            <tr>
                                <td><?= Yii::t('app', 'Epitome epicrisis') ?></td>
                                <td><?= (isset($value->diagnosis->icd->name)) ? $value->diagnosis->icd->name : null ?></td>
                                <td><?= $value->epicrisis_date ?></td>
                                <td>
                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', [
                                        '/emc/epitome-epicrisis/update', 'id' => $value->id, 'pacient_id' => $model->id], [
                                        'title' => Yii::t('app', 'Update'),
                                    ]) ?>

                                    &nbsp;

                                    <?= Html::a('<span class="glyphicon glyphicon-print"></span>', [
                                        '/emc/epitome-epicrisis/print', 'id' => $value->id], [
                                        'title' => Yii::t('app', 'Print'),
                                        'target' => '_blank',
                                    ]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="4">
                                <?= Yii::t('app', 'No records') ?>
                            </td>
                        </tr>
                    <?php endif; ?>

                </tbody>
            </table>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

var report = 0;

$('#modal-docum').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    report = button.attr('data-report');
    var title = button.attr('data-title');
    var url = $('#print_reports').attr('href') + '&report=' + report;

    var modal = $(this);
    modal.find('.modal-title').text(title);
    modal.find('#print_reports').attr('href', url);

    if (report == 1 || report == 3 || report == 4 || report == 5)
        modal.find('#diagnosis').show();

    if (report == 5)
        modal.find('#surgery').show();

    // Chained Selects Plugin
        $('#surgery').chained('#diagnosis');
})

$('#modal-docum').on('hide.bs.modal', function (event) {

    var modal = $(this);

    if (report == 1 || report == 3 || report == 4 || report == 5)
        modal.find('#diagnosis').hide();

    if (report == 5)
        modal.find('#surgery').hide();
})



$('.print').on('click', function(e) {
    e.preventDefault();

    var ready = 0;
    var url = $(this).attr('href');

    if (report == 1 || report == 3 || report == 4 || report == 5)
    {
        var diagnosis = $('select#diagnosis option:selected').val();
        url = url + '&diagnosis=' + diagnosis;

        if (diagnosis > 0)
        {
            $('#diagnosis').removeClass('has-error');
            $('#diagnosis .help-block-error').hide();
            ready = 1;

        }else{
            $('#diagnosis').addClass('has-error');
            $('#diagnosis .help-block-error').show();
            ready = 0;
        }
    }

    if (report == 5)
    {
        var surgery = $('select#surgery option:selected').val();
        url = url + '&surgery=' + surgery;

        if (surgery > 0)
        {
            $('#surgery').removeClass('has-error');
            $('#surgery .help-block-error').hide();
            ready = 1;

        }else{
            $('#surgery').addClass('has-error');
            $('#surgery .help-block-error').show();
            ready = 0;
        }
    }

    if (ready)
        window.open(url);
})

"); ?>