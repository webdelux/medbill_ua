<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;

$countRow = \backend\modules\emc\models\Insurance::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Insurance'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-insurance',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Insurance') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['Insurance_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['Insurance_alert']['type']
                    ],
                    'body' => $models['Insurance_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-insurance',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['Insurance'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?=
                    $form->field($models['Insurance'], 'insurance_id', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->dropDownList(ArrayHelper::map(backend\modules\handbook\models\InsuranceHandbook::find()->orderBy('name')->all(), 'id', 'name'), ['prompt' => ''])
                ?>

                <?=
                    $form->field($models['Insurance'], 'insurance_number', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>


                <?= $form->field($models['Insurance'], 'insurance_date', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(\kartik\date\DatePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Insurance']->insurance_date) ? $models['Insurance']->insurance_date : date("Y-m-d")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?= $form->field($models['Insurance'], 'insurance_date_end', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(\kartik\date\DatePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Insurance']->insurance_date_end) ? $models['Insurance']->insurance_date_end : date("Y-m-d")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?=
                    $form->field($models['Insurance'], 'description', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-insurance',
                            'data-container' => '#insurance',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'insurance',
                'linkSelector' => '#insurance a[data-sort], #insurance a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'insurance-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => \backend\modules\emc\models\Insurance::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'insurance_date' => SORT_DESC,
                        'id' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '80'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
                    ],
//                    [
//                        'label' => Yii::t('app', 'Pacient'),
//                        'attribute' => 'fullName',
//                        'contentOptions' => ['width' => '200'],
//                        'value' => function ($data)
//                        {
//                            return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
//                        },
//                    ],
                    [
                        'attribute' => 'insurance_id',
                        'value' => function ($data)
                        {
                            return (isset($data->insurance->name)) ? $data->insurance->name : '';
                        },
                    ],
                    [
                        'attribute' => 'insurance_number',
                    ],
                    [
                        'attribute' => 'insurance_date',
                        'format' => ['date', 'dd MMM Y'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '120'],
                        'value' => function ($data)
                        {
                            return ($data->insurance_date) ? $data->insurance_date : NULL;
                        }
                    ],
                    [
                        'attribute' => 'insurance_date_end',
                        'format' => ['date', 'dd MMM Y'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '120'],
                        'value' => function ($data)
                        {
                            return ($data->insurance_date_end) ? $data->insurance_date_end : NULL;
                        }
                    ],
//                    [
//                        'attribute' => 'description',
//                        'contentOptions' => ['width' => '80'],
//                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
//                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{edit} {delete}',
                        'buttons' => [
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/emc/insurance/update',
                                        'id' => $data->id,
                                        'tab' => 12,
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Edit'),
                                        'data-container' => '#insurance',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/emc/insurance/delete',
                                        'id' => $data->id,
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#insurance',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

"); ?>

<?php $this->registerJs("
    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {

        }

    });

");?>