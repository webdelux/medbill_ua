<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use backend\modules\handbookemc\models\EmcList;
use kartik\date\DatePicker;
use backend\modules\signalmark\models\PacientDiabetes;

$countRow = PacientDiabetes::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Pancreatic diabetes'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-diabetes',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Pancreatic diabetes') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                ]); ?>

                <?= (isset($models['PacientDiabetes_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientDiabetes_alert']['type']
                    ],
                    'body' => $models['PacientDiabetes_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-pacient-diabetes',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['PacientDiabetes'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['PacientDiabetes'], 'handbook_emc_list_degree_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label}'
                    . '<div class="col-sm-9">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#emc-list-' . EmcList::DEGREE . '-modal',
                                    'data-container' => '#form-pacient-diabetes'
                                ])
                            . '</span>'
                        . '</div>'
                    . '</div>'
                ])->dropDownList(EmcList::childList(EmcList::DEGREE), ['prompt' => '']) ?>

                <?= $form->field($models['PacientDiabetes'], 'handbook_emc_list_stage_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label}'
                    . '<div class="col-sm-9">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#emc-list-' . EmcList::STAGE . '-modal',
                                    'data-container' => '#form-pacient-diabetes'
                                ])
                            . '</span>'
                        . '</div>'
                    . '</div>'
                ])->dropDownList(EmcList::childList(EmcList::STAGE), ['prompt' => '']) ?>

                <?= $form->field($models['PacientDiabetes'], 'handbook_emc_list_treatment_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label}'
                    . '<div class="col-sm-9">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#emc-list-' . EmcList::TREATMENT . '-modal',
                                    'data-container' => '#form-pacient-diabetes'
                                ])
                            . '</span>'
                        . '</div>'
                    . '</div>'
                ])->dropDownList(EmcList::childList(EmcList::TREATMENT), ['prompt' => '']) ?>

                <?= $form->field($models['PacientDiabetes'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea(['rows' => 2]) ?>

                <?= $form->field($models['PacientDiabetes'], 'term', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textInput(['maxlength' => true]) ?>

                <?= $form->field($models['PacientDiabetes'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['PacientDiabetes']->date_at) ? $models['PacientDiabetes']->date_at : date('Y-m-d')
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-diabetes',
                            'data-container' => '#pacient-diabetes',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-diabetes',
                'linkSelector' => '#pacient-diabetes a[data-sort], #pacient-diabetes a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'options' => [
                    'id' => 'pacient-diabetes-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => PacientDiabetes::find()->where([
                        'pacient_id' => $model->id
                    ])->orderBy('date_at DESC'),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'handbook_emc_list_degree_id',
                        'value' => function ($data) {
                            return (isset($data->degree->name)) ? $data->degree->name : '';
                        }
                    ],
                    [
                        'attribute' => 'handbook_emc_list_stage_id',
                        'value' => function ($data) {
                            return (isset($data->stage->name)) ? $data->stage->name : '';
                        }
                    ],
                    [
                        'attribute' => 'handbook_emc_list_treatment_id',
                        'value' => function ($data) {
                            return (isset($data->treatment->name)) ? $data->treatment->name : '';
                        }
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'attribute' => 'term',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/signalmark/pacient-diabetes/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-diabetes',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

            <?php echo $this->render('//pacient/tabs/modals/_modal_emc-list', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
                'parentId' => $models['EmcList']::DEGREE,
                'title' => Yii::t('app', 'Degree')
            ]); ?>

            <?php echo $this->render('//pacient/tabs/modals/_modal_emc-list', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
                'parentId' => $models['EmcList']::STAGE,
                'title' => Yii::t('app', 'Stage')
            ]); ?>

            <?php echo $this->render('//pacient/tabs/modals/_modal_emc-list', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
                'parentId' => $models['EmcList']::TREATMENT,
                'title' => Yii::t('app', 'Treatment')
            ]); ?>

        </div>
    </div>
</fieldset>