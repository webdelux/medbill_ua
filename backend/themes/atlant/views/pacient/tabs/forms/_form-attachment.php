<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;


$countRow = backend\modules\emc\models\Attachment::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Attachments'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php
                echo Html::a(Yii::t('app', 'Add'), ['/emc/attachment/create', 'id' => $model->id, 'tab' => $tab], ['class' => 'btn btn-primary']);
                ?>



            </div>

            <?php
            Pjax::begin([
                'id' => 'attachment',
                'linkSelector' => '#attachment a[data-sort], #attachment a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]);
            ?>

            <?=
            GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'attachment-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => backend\modules\emc\models\Attachment::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'created_at' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                        ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '60'],
                    ],
                    [
                        'attribute' => 'filename',
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '60'],
                    ],
//                    [
//                        'label' => Yii::t('app', 'Pacient'),
//                        'attribute' => 'fullName',
//                        'contentOptions' => ['width' => '200'],
//                        'value' => function ($data)
//                        {
//                            return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
//                        },
//                    ],
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data)
                        {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'protocol_id',
                        'value' => function ($data)
                        {
                            return (isset($data->protocol->protocol->name)) ? $data->protocol->protocol->name : '';
                        },
                    ],
                    'description',
                    // 'user_id',
                    // 'updated_user_id',
                    [
                        'attribute' => 'created_at',
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '100'],
                    ],
                    [
                        'attribute' => 'updated_at',
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '100'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{view} {delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key)
                            {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute([
                                                    '/emc/attachment/delete',
                                                    'id' => $data->id,
                                                ]), [
                                            'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'title' => Yii::t('app', 'Delete'),
                                            'data-container' => '#attachment',
                                                ]
                                );
                            },
                            'view' => function ($url, $data, $key)
                            {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                        '/uploads/'. $data->path. $data->filename,
                                        [
                                            'title' => Yii::t('app', 'View'),
                                            'data-container' => '#attachment',
                                            'target' => '_blank',
                                        ]
                                );
                            },
                        ]
                    ],
                ],
            ]);
            ?>

        <?php Pjax::end(); ?>

                </div>
            </div>
        </fieldset>


        <?php $this->registerCss("

"); ?>


        <?php $this->registerJs("

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2
            if ($('#attachment-imagefile').length)
            {
                jQuery('#attachment-imagefile').fileinput(fileinput_bcca5f44);
            }



        }

    });
"); ?>