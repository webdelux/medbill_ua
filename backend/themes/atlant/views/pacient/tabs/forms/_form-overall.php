<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\date\DatePicker;
use backend\modules\signalmark\models\PacientOverall;

$countRow = PacientOverall::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'The overall objective status'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-overall',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'The overall objective status') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                ]); ?>

                <?= (isset($models['PacientOverall_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientOverall_alert']['type']
                    ],
                    'body' => $models['PacientOverall_alert']['body']
                ]) : '' ?>

                <?= Html::activeHiddenInput($models['PacientOverall'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['PacientOverall'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['PacientOverall']->date_at) ? $models['PacientOverall']->date_at : date('Y-m-d')
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?= $form->field($models['PacientOverall'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea(['rows' => 2]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-overall',
                            'data-container' => '#pacient-overall',
                        ]) ?>
                    </div>
                </div>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-overall',
                'linkSelector' => '#pacient-overall a[data-sort], #pacient-overall a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'options' => [
                    'id' => 'pacient-overall-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => PacientOverall::find()->where([
                        'pacient_id' => $model->id
                    ])->orderBy('date_at DESC'),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/signalmark/pacient-overall/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-overall',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>