<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\date\DatePicker;
use backend\modules\signalmark\models\Dispensary;

$countRow = Dispensary::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">

    <div class="row form-body">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-dispensary',
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Dispensary') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['Dispensary_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['Dispensary_alert']['type']
                    ],
                    'body' => $models['Dispensary_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-pacient-dispensary',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['Dispensary'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['Dispensary'], 'date_in', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Dispensary']->date_in) ? $models['Dispensary']->date_in : ''
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?= $form->field($models['Dispensary'], 'date_out', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Dispensary']->date_out) ? $models['Dispensary']->date_out : ''
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?= $form->field($models['Dispensary'], 'group', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList($models['Dispensary']::itemAlias('group'), ['prompt' => '']) ?>

                <?= $form->field($models['Dispensary'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea(['rows' => 2]) ?>



                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-dispensary',
                            'data-container' => '#dispensary',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'dispensary',
                'linkSelector' => '#dispensary a[data-sort], #dispensary a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'pacient-dispensary-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => Dispensary::find()->where([
                        'pacient_id' => $model->id
                    ])->orderBy('id ASC'),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_in',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'date_out',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'group',
                        'headerOptions' => ['width' => '80'],
                        'value' => function ($data)
                        {
                            return $data::itemAlias('group', ($data->group) ? $data->group : '');
                        },
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '50'],
                        'template' => '{edit}{delete}',
                        'buttons' => [
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/signalmark/dispensary/update',
                                        'id' => $data->id,
                                        'tab' => Yii::$app->request->get('tab', 1),
                                    ]),
                                    [
                                        'data-update' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Update'),
                                        'data-container' => '#dispensary',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/signalmark/dispensary/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#dispensary',
                                    ]
                                );
                            }

                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

    });

"); ?>