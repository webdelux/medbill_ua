<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use backend\modules\diagnosis\models\Diagnosis;
use yii\helpers\ArrayHelper;


$countRow = backend\models\PacientService::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Services'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-service',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Services') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['PacientService_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientService_alert']['type']
                    ],
                    'body' => $models['PacientService_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-service',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['PacientService'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= Html::activeHiddenInput($models['PacientService'], 'pacientFullName', [
                    'value' => 'init some text'
                ]) ?>

                <?php echo $form->field($models['PacientService'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                        ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                ->andWhere(['pacient_id' => $model->id])
                                ->andWhere(['parent_id' => NULL])
                                ->all(),
                                'id', 'icd.name'), ['prompt' => '']);
                ?>

                <?php
                    $services =  $models['PacientService']->serviceList();
                    $prices = $models['PacientService']->serviceList(1);
                ?>

                <?=
                    $form->field($models['PacientService'], 'service_id', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->dropDownList($services,
                        ['prompt' => ''])
                ?>

                <?=
                    $form->field($models['PacientService'], 'total', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?=
                    $form->field($models['PacientService'], 'discount', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?= $form->field($models['PacientService'], 'date_service', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(\kartik\date\DatePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['PacientService']->date_service) ? $models['PacientService']->date_service : date("Y-m-d")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?=
                    $form->field($models['PacientService'], 'docum', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?=
                    $form->field($models['PacientService'], 'description', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-service',
                            'data-container' => '#service',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'service',
                'linkSelector' => '#service a[data-sort], #service a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'service-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => backend\models\PacientService::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'date_service' => SORT_DESC,
                        'id' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '80'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
                    ],
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data)
                        {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
//                    [
//                        'label' => Yii::t('app', 'Pacient'),
//                        'attribute' => 'fullName',
//                        'contentOptions' => ['width' => '200'],
//                        'value' => function ($data)
//                        {
//                            return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
//                        },
//                    ],
                    [
                        'attribute' => 'date_service',
                        //'format' => ['date', 'dd MMM Y hh:mm'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return ($data->date_service) ? $data->date_service : NULL;
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Service'),
                        'attribute' => 'service_id',
                        'contentOptions' => ['width' => '200'],
                        'value' => function ($data) {
                            return (isset($data->service->name)) ? $data->service->name : '';
                        },
                    ],

                    [
                        'attribute' => 'total',
                        'contentOptions' => function ($data)
                        {
                            return [
                                'width' => '80',
                                'class' => ($data->total > 0) ? 'success' : 'danger'
                            ];
                        },
                    ],
                    [
                        'attribute' => 'discount',
                    ],
                    [
                        'attribute' => 'docum',
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{edit} {delete}',
                        'buttons' => [
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/pacient-service/update',
                                        'id' => $data->id,
                                        'tab' => 10,
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Edit'),
                                        'data-container' => '#service',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/pacient-service/delete',
                                        'id' => $data->id,
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#service',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

"); ?>


<?php $this->registerJs("

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // todo list
        }

    });

    $('#pacientservice-service_id').change(function(){
        var arrayFromPHP = '" . json_encode($prices) . "';
        arrayFromPHP = JSON.parse(arrayFromPHP);
        var id = $(this).val();

        $('#pacientservice-total').val(arrayFromPHP[id]);

    });

");?>