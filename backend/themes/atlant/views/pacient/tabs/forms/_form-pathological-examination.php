<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\select2\Select2;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\handbook\models\Department;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ButtonDropdown;

$countRow = backend\modules\emc\models\PathologicalExamination::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Pathological examination'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php
                Modal::begin([
                    'id' => 'modal-pathological',
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Pathological examination') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]);
                ?>

                <?=
                (isset($models['PathologicalExamination_alert'])) ? Alert::widget([
                            'options' => [
                                'class' => 'alert-' . $models['PathologicalExamination_alert']['type']
                            ],
                            'body' => $models['PathologicalExamination_alert']['body']
                        ]) : ''
                ?>

                <?php
                Pjax::begin([
                    'id' => 'form-pathological',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]);
                ?>

                <?=
                Html::activeHiddenInput($models['PathologicalExamination'], 'pacient_id', [
                    'value' => $model->id
                ])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'department_id', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->dropDownList((new Department)->treeList(), ['prompt' => ''])
                ?>

                <?=
                $form->field($models['PathologicalExamination'], 'doctor_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['PathologicalExamination']->doctor) ? '' : $models['PathologicalExamination']->doctor->name,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/user-viewer/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]);
                ?>


                <?php
                echo $form->field($models['PathologicalExamination'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                        ->andWhere(['pacient_id' => $model->id])
                                        ->andWhere(['parent_id' => NULL])
                                        ->all(), 'id', 'icd.name', 'date_at'), ['prompt' => '']);
                ?>

                <?php
                echo $form->field($models['PathologicalExamination'], 'pathological_diagnosis_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                        ->andWhere(['pacient_id' => $model->id])
                                        ->andWhere(['parent_id' => NULL])
                                        ->andWhere(['type' => Diagnosis::TYPE_PATHOLOGICAL])
                                        ->all(), 'id', 'icd.name', 'date_at'), ['prompt' => '']);
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'pathological_number', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->textInput(['maxlength' => true])
                ?>

                <?=
                $form->field($models['PathologicalExamination'], 'pathological_date', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9 datetime-picker">{input}{error}{hint}</div>'
                ])->widget(kartik\date\DatePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['PathologicalExamination']->pathological_date) ? $models['PathologicalExamination']->pathological_date : date("Y-m-d")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'accuracy', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                        ])
                        ->dropDownList($models['PathologicalExamination']::itemAlias('accuracy'), ['prompt' => ''])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'divergence', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                        ])
                        ->dropDownList($models['PathologicalExamination']::itemAlias('divergence'), ['prompt' => ''])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'ill_time', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                        ])
                        ->textInput(['maxlength' => true])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'cause', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->textInput(['maxlength' => true])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'ills', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->textInput(['maxlength' => true])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'other_reasons', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->textInput(['maxlength' => true])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'pregnancy_interval', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->textInput(['maxlength' => true])
                ?>

                <?=
                        $form->field($models['PathologicalExamination'], 'description', [
                            'labelOptions' => ['class' => 'col-sm-3 compacted-control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->textInput(['maxlength' => true])
                ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?=
                        Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pathological',
                            'data-container' => '#pathological',
                        ])
                        ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php
            Pjax::begin([
                'id' => 'pathological',
                'linkSelector' => '#pathological a[data-sort], #pathological a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]);
            ?>

            <?=
            GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'pathological-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => backend\modules\emc\models\PathologicalExamination::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'pathological_date' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                        ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '60'],
                    ],
//                    [
//                        'label' => Yii::t('app', 'Pacient'),
//                        'attribute' => 'fullName',
//                        'contentOptions' => ['width' => '200'],
//                        'value' => function ($data)
//                        {
//                            return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
//                        },
//                    ],
                    [
                        'attribute' => 'department_id',
                        'value' => function ($data)
                        {
                            return (isset($data->department->name)) ? $data->department->name : '';
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Doctor'),
                        'attribute' => 'doctor_id',
                        'contentOptions' => ['width' => '200'],
                        'value' => function ($data)
                {
                    return (isset($data->doctor->name)) ? $data->doctor->name : '';
                },
                    ],
                    [
                        'attribute' => 'pathological_date',
                        //'format' => ['date', 'dd MMM Y hh:mm'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'value' => function ($data)
                {
                    return ($data->pathological_date) ? $data->pathological_date : NULL;
                }
                    ],
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data)
                        {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'pathological_diagnosis_id',
                        'value' => function ($data)
                        {
                            return (isset($data->diagnosisPathological->icd->name)) ? $data->diagnosisPathological->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'pathological_number',
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'value' => function ($data)
                {
                    return (isset($data->pathological_number)) ? $data->pathological_number : '';
                },
                    ],
                    [
                        'attribute' => 'accuracy',
                        'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                        'value' => function ($data)
                {
                    return $data::itemAlias('accuracy', ($data->accuracy) ? $data->accuracy : '');
                }
                    ],
                    [
                        'attribute' => 'divergence',
                        'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                        'value' => function ($data)
                {
                    return $data::itemAlias('divergence', ($data->divergence) ? $data->divergence : '');
                }
                    ],
                    // 'ill_time',
                    // 'cause',
                    // 'ills',
                    // 'other_reasons',
                    // 'pregnancy_interval',
                    // 'description',
                    // 'user_id',
                    // 'updated_user_id',
                    // 'created_at',
                    // 'updated_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{edit} {delete}',
                        'buttons' => [
                            'edit' => function ($url, $data, $key)
                            {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute([
                                                    '/emc/pathological-examination/update',
                                                    'id' => $data->id,
                                                    'tab' => 16,
                                                ]), [
                                            'title' => Yii::t('app', 'Edit'),
                                            'data-container' => '#pathological',
                                                ]
                                );
                            },
                                    'delete' => function ($url, $data, $key)
                            {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute([
                                                    '/emc/pathological-examination/delete',
                                                    'id' => $data->id,
                                                ]), [
                                            'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'title' => Yii::t('app', 'Delete'),
                                            'data-container' => '#pathological',
                                                ]
                                );
                            }
                                ]
                            ],
                        ],
                    ]);
                    ?>

                    <?php Pjax::end(); ?>

                </div>
            </div>
        </fieldset>

        <div class="panel-footer">

            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>

            <?php
            if ($countRow > 0)
            {
                echo ButtonDropdown::widget([
                    'label' => Yii::t('app', 'Print'),
                    'options' => [
                        'class' => 'btn-default',
                        'style' => 'margin:5px'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => Yii::t('app', 'Pathological examination _report1'),
                                'url' => Yii::$app->urlManager->createUrl(['/emc/pathological-examination/report', 'id' => $model->id]),
                                'linkOptions' => ['target' => '_blank']
                            ],
                        ]
                    ]
                ]);
            }
            ?>


        </div>

<?php $this->registerCss("

"); ?>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2
            selectInit('pathologicalexamination-doctor_id');
            selectInit('pathologicalexamination-icd');
        }

    });
"); ?>