<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\select2\Select2;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\handbook\models\Department;
use yii\helpers\ArrayHelper;
use backend\modules\handbook\models\HandbookStacionary;
use yii\web\JsExpression;

$countRow = \backend\models\Stacionary::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Stacionary'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-stacionary',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Stacionary') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['Stacionary_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['Stacionary_alert']['type']
                    ],
                    'body' => $models['Stacionary_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-stacionary',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['Stacionary'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= Html::activeHiddenInput($models['Stacionary'], 'pacientFullName', [
                    'value' => 'init some text'
                ]) ?>

                <?= Html::activeHiddenInput($models['Stacionary'], 'doctorFullName', [
                    'value' => 'init some text'
                ]) ?>



                <?php echo $form->field($models['Stacionary'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                        ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                ->andWhere(['pacient_id' => $model->id])
                                ->andWhere(['parent_id' => NULL])
                                ->all(),
                                'id', 'icd.name'), ['prompt' => '']);
                ?>

                <?= $form->field($models['Stacionary'], 'doctor_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['Stacionary']->doctor) ? '' : $models['Stacionary']->doctor->name,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/user-viewer/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?=
                    $form->field($models['Stacionary'], 'department_id', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->dropDownList((new Department)->treeList(),
                        ['prompt' => ''])
                ?>

                <?php
                    if (isset($models['Stacionary']->department_id))   //при зміні данних
                    {
                        echo $form->field($models['Stacionary'], 'room_id', [
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->dropDownList((new HandbookStacionary)->roomList($models['Stacionary']->department_id, 0, $models['Stacionary']->pacient_id),
                            ['prompt' => '']);
                    }
                    else                            // для нового запису - пуста форма. Потім завантажуємо аяксом.
                    {
                        echo $form->field($models['Stacionary'], 'room_id', [
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->dropDownList(['prompt' => '']);
                    }
                ?>

                <?php
                    if (isset($models['Stacionary']->room_id))             //при зміні данних
                    {
                        echo $form->field($models['Stacionary'], 'place_id', [
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->dropDownList((new HandbookStacionary)->roomList(null, $models['Stacionary']->room_id,  $models['Stacionary']->pacient_id),
                            ['prompt' => '']);
                    }
                    else                                // для нового запису - пуста форма. Потім завантажуємо аяксом.
                    {
                        echo $form->field($models['Stacionary'], 'place_id', [
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->dropDownList(['prompt' => '']);
                    }
                ?>


                <?php $models['Stacionary']->type = 1; ?>            <!-- ініціалізуємо значення -->
                <?= $form->field($models['Stacionary'], 'type', [
                    'labelOptions' => ['class' => 'col-sm-3 control-label'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                    ])
                        ->dropDownList($models['Stacionary']::itemAlias('type'), ['readonly' => 'true', 'prompt' => ''])
                ?>

                <?=
                    $form->field($models['Stacionary'], 'description', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?= $form->field($models['Stacionary'], 'parent', [
                    'labelOptions' => ['class' => 'col-sm-3 control-label'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                        ->dropDownList(['0'=>Yii::t('app', 'No'),'1'=>Yii::t('app', 'Yes')])
                ?>


                <?= $form->field($models['Stacionary'], 'date_stacionary', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9 datetime-picker">{input}{error}{hint}</div>'
                ])->widget(kartik\datetime\DateTimePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00 hh:ii',
                        'value' => ($models['Stacionary']->date_stacionary) ? $models['Stacionary']->date_stacionary : date("Y-m-d H:i:s")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii'
                    ]
                ]) ?>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-stacionary',
                            'data-container' => '#stacionary',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'stacionary',
                'linkSelector' => '#stacionary a[data-sort], #stacionary a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'stacionary-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => \backend\models\Stacionary::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'date_stacionary' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '80'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
                    ],
//                    [
//                        'label' => Yii::t('app', 'Pacient'),
//                        'attribute' => 'fullName',
//                        'contentOptions' => ['width' => '200'],
//                        'value' => function ($data)
//                        {
//                            return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
//                        },
//                    ],
                    [
                        'label' => Yii::t('app', 'Doctor'),
                        'attribute' => 'doctor_id',
                        'contentOptions' => ['width' => '200'],
                        'value' => function ($data)
                        {
                            return (isset($data->doctor->name)) ? $data->doctor->name : '';
                        },
                    ],
                    [
                        'attribute' => 'date_stacionary',
                        //'format' => ['date', 'dd MMM Y hh:mm'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return ($data->date_stacionary) ? $data->date_stacionary : NULL;
                        }
                    ],
                    [
                        'attribute' => 'date_stacionary_out',
                        //'format' => ['date', 'dd MMM Y hh:mm'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return ($data->date_stacionary_out) ? $data->date_stacionary_out : NULL;
                        }
                    ],
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data)
                        {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'department_id',
                        'value' => function ($data)
                        {
                            return (isset($data->department->name)) ? $data->department->name : '';
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Room'),
                        'attribute' => 'room_id',
                        'contentOptions' => ['width' => '200'],
                        'value' => function ($data)
                        {
                            return (isset($data->room->room)) ? $data->room->room : '';
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Place'),
                        'attribute' => 'place_id',
                        'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return (isset($data->place->place)) ? $data->place->place : '';
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Type'),
                        'attribute' => 'type',
                        'contentOptions' => function ($data)
                        {
                            return ['class' => ($data->type == 1) ? 'success' : 'danger', 'width' => '200'];
                        },
                        'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return $data::itemAlias('type', ($data->type) ? $data->type : '');
                        }
                    ],
                    [
                        'attribute' => 'parent',
                        'contentOptions' => ['width' => '50'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '50'],
                        'value' => function ($data)
                        {
                            return ($data->parent ==0) ? Yii::t('app', 'No') : Yii::t('app', 'Yes');
                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{release} {edit} {delete}',
                        'buttons' => [
                            'release' => function ($url, $data, $key) {
                                if ($data->type == 1){
                                    return Html::a('<span class="fa fa-share"></span>',
                                        Url::toRoute([
                                            '/stacionary/update',
                                            'type' => 2,
                                            'id' => $data->id,
                                        ]),
                                        [
                                            'data-delete' => Yii::t('app', 'Are you sure you want to release this pacient?'),
                                            'title' => Yii::t('app', 'Release'),
                                            'data-container' => '#stacionary',
                                        ]
                                    );
                                }
                            },
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/stacionary/update',
                                        'id' => $data->id,
                                        'tab' => 8,
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Edit'),
                                        'data-container' => '#stacionary',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/stacionary/delete',
                                        'id' => $data->id,
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#stacionary',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

"); ?>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    function dateInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-datetimepicker');
            jQuery('#' + itemname).datetimepicker(window[settings]);

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2
            selectInit('diagnosis-handbook_emc_icd_id');
            selectInit('hospitalization-refferal_id');
            selectInit('stacionary-doctor_id');
            dateInit('hospitalization-hospitalization_date-datetime');

            load_rooms();
            load_places();
        }

    });

// виконуємо при завантаженні вікна
load_rooms();
load_places();


// Завантажуємо список палат для вибранного відділення
function load_rooms(){
    $('select#stacionary-department_id').change(function() {
        $.get( '".Url::toRoute('handbook/handbook-stacionary/rooms')."', { department: $('select#stacionary-department_id option:selected').val() } )
            .done(function( data )
               { console.log(data);
                    $('select#stacionary-room_id').val('').empty();
                    $('select#stacionary-room_id').append($('<option></option>').attr('value', '').text(''));

                    $.each(data, function(index, opt) {
                        $('select#stacionary-room_id').append($('<option></option>').attr('value', opt.id).text(opt.value));
                    });

                $('select#stacionary-room_id').val({$models['Stacionary']->room_id});
            });
    });
}

// Завантажуємо список ліжок для вибранної палати
function load_places(){
    $('select#stacionary-room_id').change(function() {
        $.get( '".Url::toRoute('handbook/handbook-stacionary/rooms')."', { room: $('select#stacionary-room_id option:selected').val() } )
            .done(function( data )
               {
                    $('select#stacionary-place_id').val('').empty();
                    $('select#stacionary-place_id').append($('<option></option>').attr('value', '').text(''));

                    $.each(data, function(index, opt) {
                        $('select#stacionary-place_id').append($('<option></option>').attr('value', opt.id).text(opt.value));
                    });

                $('select#stacionary-place_id').val({$models['Stacionary']->place_id});
            });
    });
}

");?>