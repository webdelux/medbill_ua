<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\Pjax;

use yii\data\ActiveDataProvider;

use yii\bootstrap\Modal;
use yii\bootstrap\Alert;

use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use kartik\select2\Select2;

use backend\models\PacientOperation;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\handbook\models\Department;
use backend\modules\handbook\models\Speciality;

$countRow = PacientOperation::find()->joinWith('diagnosis')->andWhere(['pacient_id' => $model->id])->count();
$speciality = new Speciality;
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Surgeries'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-operation',
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Surgeries') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['PacientOperation_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientOperation_alert']['type']
                    ],
                    'body' => $models['PacientOperation_alert']['body']
                ]) : '' ?>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'diagnosis_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                ->where(['pacient_id' => $model->id])->main()->all(), 'id', 'icd.name'), ['prompt' => '']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'type_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList($models['PacientOperation']::itemAlias('type_id'), [
                            'prompt' => '',
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'handbook_emc_surgery_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(Select2::classname(), [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'initValueText' => empty($models['PacientOperation']->surgery) ? '' : $models['PacientOperation']->surgery->name,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                    'url' => Url::to(['/handbookemc/surgery/index']),
                                    'dataType' => 'json',
                                ]
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'handbook_emc_surgery_note', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'handbook_emc_icd_id', [
                            'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(Select2::classname(), [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'initValueText' => empty($models['PacientOperation']->icd) ? '' : $models['PacientOperation']->icd->name,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                    'url' => Url::to(['/handbookemc/icd/index']),
                                    'dataType' => 'json',
                                ]
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'handbook_emc_icd_note', [
                            'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'start_operation_date', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(DatePicker::classname(), [
                            'options' => [
                                'style' => 'width: 90px;',
                                'placeholder' => '0000-00-00',
                                'value' => ($models['PacientOperation']->start_operation_date) ? $models['PacientOperation']->start_operation_date : date('Y-m-d'),
                            ],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'start_operation_time', [
                            'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(TimePicker::classname(), [
                            'options' => [
                                'placeholder' => '00:00',
                                'value' => ($models['PacientOperation']->start_operation_time) ? $models['PacientOperation']->start_operation_time : date('H:i'),
                            ],
                            'pluginOptions' => [
                                'showSeconds' => false,
                                'showMeridian' => false,
                                'template' => false
                            ]
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'duration_operation_hour', [
                            'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textInput([
                            'style' => 'width: 60px;',
                            'placeholder' => '0',
                            'value' => ($models['PacientOperation']->duration_operation_hour) ? $models['PacientOperation']->duration_operation_hour : 0,
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'duration_operation_minute', [
                            'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textInput([
                            'style' => 'width: 60px;',
                            'placeholder' => '0',
                            'value' => ($models['PacientOperation']->duration_operation_minute) ? $models['PacientOperation']->duration_operation_minute : 0,
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?php // echo $form->field($models['PacientOperation'], 'surgeon_user_id', [
//                            'labelOptions' => ['class' => 'control-label col-sm-4'],
//                            'inputOptions' => ['class' => 'form-control'],
//                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
//                        ])->widget(Select2::classname(), [
//                            'theme' => Select2::THEME_BOOTSTRAP,
//                            'initValueText' => empty($models['PacientOperation']->surgeonUser) ? '' : $models['PacientOperation']->surgeonUser->profile->name,
//                            'options' => [
//                                'placeholder' => '',
//                            ],
//                            'pluginOptions' => [
//                                'allowClear' => true,
//                                'minimumInputLength' => 3,
//                                'ajax' => [
//                                    'url' => Url::to(['/user-viewer/index', 'results' => true, 'speciality' => 'surgeons']),
//                                    'dataType' => 'json',
//                                ]
//                            ],
//                        ]); ?>

                        <?= $form->field($models['PacientOperation'], 'surgeon_user_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList(ArrayHelper::map($speciality->getDoctors('surgeons'), 'user_id', 'name'), [
                            'prompt' => '',
                        ]) ?>

                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'department_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(Select2::classname(), [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'initValueText' => empty($models['PacientOperation']->department) ? '' : $models['PacientOperation']->department->name,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'data' => (new Department)->treeList((new Department)->treeData()),
                            'pluginOptions' => [
                                'allowClear' => true,
                                /*
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/handbook/department/index']),
                                    'dataType' => 'json',
                                ]
                                */
                            ],
                        ]); ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?php //echo  $form->field($models['PacientOperation'], 'anesthetist_user_id', [
//                            'labelOptions' => ['class' => 'control-label col-sm-4'],
//                            'inputOptions' => ['class' => 'form-control'],
//                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
//                        ])->widget(Select2::classname(), [
//                            'theme' => Select2::THEME_BOOTSTRAP,
//                            'initValueText' => empty($models['PacientOperation']->anesthetistUser) ? '' : $models['PacientOperation']->anesthetistUser->profile->name,
//                            'options' => [
//                                'placeholder' => '',
//                            ],
//                            'pluginOptions' => [
//                                'allowClear' => true,
//                                'minimumInputLength' => 3,
//                                'ajax' => [
//                                    'url' => Url::to(['/user-viewer/index', 'results' => true, 'speciality' => 'anesthetists']),
//                                    'dataType' => 'json',
//                                ]
//                            ],
//                        ]); ?>

                        <?= $form->field($models['PacientOperation'], 'anesthetist_user_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList(ArrayHelper::map($speciality->getDoctors('anesthetists'), 'user_id', 'name'), [
                            'prompt' => '',
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'anesthesia_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList($models['PacientOperation']::itemAlias('anesthesia_id'), [
                            'prompt' => '',
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'handbook_emc_complication_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(Select2::classname(), [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'initValueText' => empty($models['PacientOperation']->complication) ? '' : $models['PacientOperation']->complication->name,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                    'url' => Url::to(['/handbookemc/complication/index']),
                                    'dataType' => 'json',
                                ]
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'handbook_emc_complication_note', [
                            'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'date_at', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(DatePicker::classname(), [
                            'options' => [
                                'style' => 'width: 90px;',
                                'placeholder' => '0000-00-00',
                                'value' => ($models['PacientOperation']->date_at) ? $models['PacientOperation']->date_at : date('Y-m-d')
                            ],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['PacientOperation'], 'result_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList($models['PacientOperation']::itemAlias('result_id'), [
                            'prompt' => '',
                        ]) ?>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                                <?= Html::button(Yii::t('app', 'Save'), [
                                    'class' => 'btn btn-primary',
                                    'data-save' => '',
                                    'data-modal' => '#modal-pacient-operation',
                                    'data-container' => '#pacient-operation',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-operation',
                'linkSelector' => '#pacient-operation a[data-sort], #pacient-operation a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'pacient-operation-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => PacientOperation::find()->joinWith('diagnosis')->andWhere([
                        'pacient_id' => $model->id
                    ])->orderBy([
                        'diagnosis_id' => SORT_ASC,
                        '{{%pacient_operation}}.date_at' => SORT_ASC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    'id',
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data) {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                        'group' => true,
                    ],
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'handbook_emc_surgery_id',
                        'value' => function ($data) {
                            return (isset($data->surgery->name)) ? $data->surgery->name : '';
                        },
                    ],
                    [
                        'attribute' => 'handbook_emc_icd_id',
                        'value' => function ($data) {
                            return (isset($data->icd->name)) ? $data->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'type_id',
                        'value' => function ($data) {
                            return ($data->type_id) ? PacientOperation::itemAlias('type_id', $data->type_id) : '';
                        },
                    ],
                    [
                        'attribute' => 'result_id',
                        'value' => function ($data) {
                            return ($data->result_id) ? PacientOperation::itemAlias('result_id', $data->result_id) : '';
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '65'],
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                    Url::toRoute([
                                        '/pacient-operation/view',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'View'),
                                        'data-target' => "#modal-pacient-operation-view",
                                        'data-toggle' => "modal",
                                    ]
                                );
                            },
                            'update' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/pacient-operation/update',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Update'),
                                        'data-target' => "#modal-pacient-operation-update",
                                        'data-toggle' => "modal",
                                        'data-container' => '#pacient-operation',
                                        'data-token' => $data->id . $data->diagnosis_id,
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/pacient-operation/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-operation',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php Modal::begin([
    'id' => "modal-pacient-operation-view",
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Operation') . '</h4>',
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
    'clientOptions' => [
        'remote' => false
    ],
]); ?>
<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => "modal-pacient-operation-update",
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Operation') . '</h4>',
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
    'clientOptions' => [
        'remote' => false
    ],
]); ?>

<div class="modal-form">
    <div class="modal-form-body"></div>
    <div class="modal-form-footer" style="display: none;">
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'id' => 'btn-pacient-operation-update'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::end(); ?>


<?php $this->registerJs("

    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    function timeInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-timepicker');
            jQuery('#' + itemname ).timepicker(window[settings]);

        }
    }

    function dateInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-kvDatepicker');
            jQuery('#' + itemname).kvDatepicker(window[settings]);

        }
    }

    function initWidgetPacientOperation()
    {
        selectInit('pacientoperation-handbook_emc_surgery_id');
        selectInit('pacientoperation-handbook_emc_icd_id');
        //selectInit('pacientoperation-surgeon_user_id');
        selectInit('pacientoperation-handbook_emc_complication_id');
        selectInit('pacientoperation-department_id');

        timeInit('pacientoperation-start_operation_time');
    }

    function initWidgetPacientOperationUpdate(id)
    {
        selectInit('pacientoperation-handbook_emc_surgery_id-operation-id' + id);
        selectInit('pacientoperation-handbook_emc_icd_id-operation-id' + id);
        selectInit('pacientoperation-handbook_emc_complication_id-operation-id' + id);
        selectInit('pacientoperation-department_id-operation-id' + id);

        timeInit('pacientoperation-start_operation_time-operation-id' + id);

        dateInit('pacientoperation-start_operation_date-operation-id' + id + '-kvdate');
        dateInit('pacientoperation-date_at-operation-id' + id + '-kvdate');
    }

    $('#modal-pacient-operation-view').on('show.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
    });
    $('#modal-pacient-operation-view').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-body').load(button.attr('href') + ' #pacient-operation-view', function(response, status, xhr) {
            if (status == 'success') {
                // ...
            }
        });
    });
    $('#modal-pacient-operation-view').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('');
    });

    $('#modal-pacient-operation-update').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-form-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
        modal.find('.modal-form-body').load(button.attr('href') + ' #pacient-operation-form-content', function(response, status, xhr) {
            if (status == 'success') {
                initWidgetPacientOperationUpdate(button.attr('data-token'));

                $('#btn-pacient-operation-update').attr('data-href', button.attr('href'));
                $('#btn-pacient-operation-update').attr('data-container', button.attr('data-container'));
                $('#btn-pacient-operation-update').attr('data-token', button.attr('data-token'));

                modal.find('.modal-form-footer').show();
            }
        });
    });
    $('#modal-pacient-operation-update').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-form-body').html('');
        modal.find('.modal-form-footer').hide();
    });
    $(document).on('click', '#btn-pacient-operation-update', function(event) {
        var modal = $('#modal-pacient-operation-update');
        var container = $(this).attr('data-container');
        var token = $(this).attr('data-token');

        $.ajax({
            method: 'POST',
            url: $(this).attr('data-href'),
            data: modal.find('input, textarea, select').serialize(),
            dataType: 'html'
        })
        .done(function(data) {
            modal.find('#pacient-operation-form-content').html($(data).find('#pacient-operation-form-content').html());
            modal.find('input').prop('disabled', false);

            initWidgetPacientOperationUpdate(token);

            $.pjax.reload({
                container: container
            });
        });
    })

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            initWidgetPacientOperation();
        }

    });

"); ?>