<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use backend\modules\handbookemc\models\EmcList;
use kartik\date\DatePicker;
use backend\modules\signalmark\models\PacientHypertension;

$countRow = PacientHypertension::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Hypertensive disease'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-hypertension',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Hypertensive disease') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                ]); ?>

                <?= (isset($models['PacientHypertension_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientHypertension_alert']['type']
                    ],
                    'body' => $models['PacientHypertension_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-pacient-hypertension',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['PacientHypertension'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['PacientHypertension'], 'handbook_emc_list_degree_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label}'
                    . '<div class="col-sm-9">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#emc-list-' . EmcList::HYPERTENSION_DEGREE . '-modal',
                                    'data-container' => '#form-pacient-hypertension'
                                ])
                            . '</span>'
                        . '</div>'
                    . '</div>'
                ])->dropDownList(EmcList::childList(EmcList::HYPERTENSION_DEGREE), ['prompt' => '']) ?>

                <?= $form->field($models['PacientHypertension'], 'handbook_emc_list_stage_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label}'
                    . '<div class="col-sm-9">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#emc-list-' . EmcList::HYPERTENSION_STAGE . '-modal',
                                    'data-container' => '#form-pacient-hypertension'
                                ])
                            . '</span>'
                        . '</div>'
                    . '</div>'
                ])->dropDownList(EmcList::childList(EmcList::HYPERTENSION_STAGE), ['prompt' => '']) ?>

                <?= $form->field($models['PacientHypertension'], 'handbook_emc_list_risk_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label}'
                    . '<div class="col-sm-9">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#emc-list-' . EmcList::HYPERTENSION_RISK . '-modal',
                                    'data-container' => '#form-pacient-hypertension'
                                ])
                            . '</span>'
                        . '</div>'
                    . '</div>'
                ])->dropDownList(EmcList::childList(EmcList::HYPERTENSION_RISK), ['prompt' => '']) ?>

                <?= $form->field($models['PacientHypertension'], 'handbook_emc_list_category_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label}'
                    . '<div class="col-sm-9">'
                        . '<div class="input-group">'
                            . '{input}'
                            . '<span class="input-group-btn">'
                                . Html::button('<span class="fa fa-list"></span>', [
                                    'class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#emc-list-' . EmcList::HYPERTENSION_CATEGORY . '-modal',
                                    'data-container' => '#form-pacient-hypertension'
                                ])
                            . '</span>'
                        . '</div>'
                    . '</div>'
                ])->dropDownList(EmcList::childList(EmcList::HYPERTENSION_CATEGORY), ['prompt' => '']) ?>

                <?= $form->field($models['PacientHypertension'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea(['rows' => 2]) ?>

                <?= $form->field($models['PacientHypertension'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['PacientHypertension']->date_at) ? $models['PacientHypertension']->date_at : date('Y-m-d')
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-hypertension',
                            'data-container' => '#pacient-hypertension',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-hypertension',
                'linkSelector' => '#pacient-hypertension a[data-sort], #pacient-hypertension a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'options' => [
                    'id' => 'pacient-hypertension-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => PacientHypertension::find()->where([
                        'pacient_id' => $model->id
                    ])->orderBy('date_at DESC'),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'handbook_emc_list_degree_id',
                        'value' => function ($data) {
                            return (isset($data->degree->name)) ? $data->degree->name : '';
                        }
                    ],
                    [
                        'attribute' => 'handbook_emc_list_stage_id',
                        'value' => function ($data) {
                            return (isset($data->stage->name)) ? $data->stage->name : '';
                        }
                    ],
                    [
                        'attribute' => 'handbook_emc_list_risk_id',
                        'value' => function ($data) {
                            return (isset($data->risk->name)) ? $data->risk->name : '';
                        }
                    ],
                    [
                        'attribute' => 'handbook_emc_list_category_id',
                        'value' => function ($data) {
                            return (isset($data->category->name)) ? $data->category->name : '';
                        }
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/signalmark/pacient-hypertension/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-hypertension',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

            <?php echo $this->render('//pacient/tabs/modals/_modal_emc-list', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
                'parentId' => $models['EmcList']::HYPERTENSION_DEGREE,
                'title' => Yii::t('app', 'Degree')
            ]); ?>

            <?php echo $this->render('//pacient/tabs/modals/_modal_emc-list', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
                'parentId' => $models['EmcList']::HYPERTENSION_STAGE,
                'title' => Yii::t('app', 'Stage')
            ]); ?>

            <?php echo $this->render('//pacient/tabs/modals/_modal_emc-list', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
                'parentId' => $models['EmcList']::HYPERTENSION_RISK,
                'title' => Yii::t('app', 'Risk')
            ]); ?>

            <?php echo $this->render('//pacient/tabs/modals/_modal_emc-list', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
                'parentId' => $models['EmcList']::HYPERTENSION_CATEGORY,
                'title' => Yii::t('app', 'Category')
            ]); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

    .field-pacienthypertension-description .control-label {
        line-height: normal;
    }

"); ?>