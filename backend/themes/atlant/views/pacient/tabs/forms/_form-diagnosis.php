<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
?>


<!-- START ACCORDION -->
<div class="panel-group accordion accordion-dc" id="blk-diagnoses">

    <?php foreach ($models['Data'] as $value): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="#blk-diagnosis-template-id<?php echo $value->id; ?>">
                    <?php echo Yii::t('app', 'Diagnosis'); ?>: <?php echo (isset($value->icd->name)) ? $value->icd->name : null; ?>
                </a>
            </h4>
        </div>
        <div class="panel-body" id="blk-diagnosis-template-id<?php echo $value->id; ?>">

            <?php $template = $this->render('//modules/diagnosis/views/diagnosis/_template', ['model' => $value, 'models' => $models]); ?>
            <?php $template = preg_replace("/<form[^>]+\>/i", "", $template); ?>
            <?php echo str_replace("</form>", "", $template); ?>

        </div>
    </div>
    <?php endforeach; ?>

    <div class="panel panel-default" style="display: none;" id="blk-diagnosis-template">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="#blk-diagnosis-template-id">
                    <?php echo Yii::t('app', 'Diagnosis'); ?>
                </a>
            </h4>
        </div>
        <div class="panel-body" id="blk-diagnosis-template-id"></div>
    </div>


    <?php Modal::begin([
        'id' => "modal-diagnosis-template-operation-view",
        'size' => Modal::SIZE_LARGE,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Operation') . '</h4>',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
        'clientOptions' => [
            'remote' => false
        ],
    ]); ?>
    <?php Modal::end(); ?>

    <?php Modal::begin([
        'id' => "modal-diagnosis-template-operation-update",
        'size' => Modal::SIZE_LARGE,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Operation') . '</h4>',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
        'clientOptions' => [
            'remote' => false
        ],
    ]); ?>

    <div class="modal-form">
        <div class="modal-form-body"></div>
        <div class="modal-form-footer" style="display: none;">
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            <?= Html::button(Yii::t('app', 'Save'), [
                                'class' => 'btn btn-primary',
                                'id' => 'btn-diagnosis-template-operation-update'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php Modal::end(); ?>


    <?php Modal::begin([
        'id' => "modal-diagnosis-template-protocol-view",
        'size' => Modal::SIZE_LARGE,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
        'clientOptions' => [
            'remote' => false
        ],
    ]); ?>
    <?php Modal::end(); ?>

    <?php Modal::begin([
        'id' => "modal-diagnosis-template-protocol-update",
        'size' => Modal::SIZE_LARGE,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
        'clientOptions' => [
            'remote' => false
        ],
    ]); ?>

    <div class="modal-form">
        <div class="modal-form-body"></div>
        <div class="modal-form-footer" style="display: none;">
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            <?= Html::button(Yii::t('app', 'Save'), [
                                'class' => 'btn btn-primary',
                                'id' => 'btn-diagnosis-template-protocol-update'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php Modal::end(); ?>


</div>
<!-- END ACCORDION -->

<div>
    <p>&nbsp;</p>
    <?= Html::button(Yii::t('app', 'Add'), [
        'id' => 'btn-add-diagnosis',
        'class' => 'btn btn-primary btn-sm',
    ]) ?>
</div>


<?php $this->registerCss("

    .panel .panel-heading {
        padding: 5px;
    }

    .panel .panel-heading .panel-title {
        font-size: 14px;
        font-weight: normal;
        line-height: normal;
    }

    .panel-group {
        margin-bottom: 0px;
    }

    .panel-group .panel {
        margin-top: 5px;
    }

    .panel p {
        margin: 0;
    }

    .modal .modal-content .modal-body table {
        margin-bottom: 0px;
    }

    .form-group.field-diagnosis-handbook_emc_icd_id {
        margin-bottom: 10px;
    }
    .form-group ul.diagnosis-archive {
        margin: 0;
        padding: 0;
        list-style: none inside;
    }
    .form-group ul.diagnosis-archive li.through {
        text-decoration: line-through;
    }

    .form-group.field-protocol-protocol_date input {
        border-radius: 0;
        font-size: 13px;
    }
    .form-group.field-protocol-protocol_date .input-group-btn > .btn {
        margin-left: 0;
    }

    .modal .modal-content .modal-body .modal-fullcalendar {
        border-bottom: 1px solid #ddd;
    }

"); ?>

<?php $this->registerJs("

    /**
     * Multiple modals overlay (Backdrop z-index fix).
     * This solution uses a setTimeout because the .modal-backdrop isn't created when the event show.bs.modal is triggered.
     */
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    /**
     * Scrollbar fix.
     * If you have a modal on your page that exceeds the browser height, then you can't scroll in it when closing an second modal.
     */
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

    $('#modal-diagnosis-template-operation-view').on('show.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
    });

    $('#modal-diagnosis-template-operation-view').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-body').load(button.attr('href') + ' #pacient-operation-view', function(response, status, xhr) {
            if (status == 'success') {
                // ...
            }
        });
    });

    $('#modal-diagnosis-template-operation-view').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('');
    });

    $('#modal-diagnosis-template-operation-update').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-form-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
        modal.find('.modal-form-body').load(button.attr('href') + ' #pacient-operation-form-content', function(response, status, xhr) {
            if (status == 'success') {
                initWidgetPacientOperation(button.attr('data-token'));

                $('#btn-diagnosis-template-operation-update').attr('data-href', button.attr('href'));
                $('#btn-diagnosis-template-operation-update').attr('data-id', button.attr('data-id'));
                $('#btn-diagnosis-template-operation-update').attr('data-token', button.attr('data-token'));
                $('#btn-diagnosis-template-operation-update').attr('data-container', button.attr('data-container'));

                modal.find('.modal-form-footer').show();
            }
        });
    });

    $('#modal-diagnosis-template-operation-update').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-form-body').html('');
        modal.find('.modal-form-footer').hide();
    });

    $(document).on('click', '#btn-diagnosis-template-operation-update', function(event) {
        var modal = $('#modal-diagnosis-template-operation-update');
        var container = $(this).attr('data-container');
        var token = $(this).attr('data-token');

        $.ajax({
            method: 'POST',
            url: $(this).attr('data-href'),
            data: modal.find('input, textarea, select').serialize(),
            dataType: 'html'
        })
        .done(function(data) {
            modal.find('#pacient-operation-form-content').html($(data).find('#pacient-operation-form-content').html());
            modal.find('input').prop('disabled', false);

            initWidgetPacientOperation(token);

            $.pjax.reload({
                container: container
            });
        });
    })

    $('#modal-diagnosis-template-protocol-view').on('show.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
    });

    $('#modal-diagnosis-template-protocol-view').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-body').load(button.attr('href') + ' #protocol-view', function(response, status, xhr) {
            if (status == 'success') {
                // ...
            }
        });
    });

    $('#modal-diagnosis-template-protocol-view').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('');
    });

    $('#modal-diagnosis-template-protocol-update').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-form-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
        modal.find('.modal-form-body').load(button.attr('href') + ' #protocol-form-content', function(response, status, xhr) {
            if (status == 'success') {
                initWidgetProtocol(button.attr('data-token'));

                // Chained Selects Plugin
                $('#protocol-doctor_id-id' + button.attr('data-token')).chained('#protocol-department_id-id' + button.attr('data-token'));

                $('#btn-diagnosis-template-protocol-update').attr('data-href', button.attr('href'));
                $('#btn-diagnosis-template-protocol-update').attr('data-id', button.attr('data-id'));
                $('#btn-diagnosis-template-protocol-update').attr('data-token', button.attr('data-token'));
                $('#btn-diagnosis-template-protocol-update').attr('data-container', button.attr('data-container'));

                modal.find('.modal-form-footer').show();
            }
        });
    });

    $('#modal-diagnosis-template-protocol-update').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-form-body').html('');
        modal.find('.modal-form-footer').hide();
    });

    $(document).on('click', '#btn-diagnosis-template-protocol-update', function(event) {
        var modal = $('#modal-diagnosis-template-protocol-update');
        var container = $(this).attr('data-container');
        var token = $(this).attr('data-token');

        $.ajax({
            method: 'POST',
            url: $(this).attr('data-href'),
            data: modal.find('input, textarea, select').serialize(),
            dataType: 'html'
        })
        .done(function(data) {
            modal.find('#protocol-form-content').html($(data).find('#protocol-form-content').html());
            modal.find('input').prop('disabled', false);

            initWidgetProtocol(token);

            // Chained Selects Plugin
            $('#protocol-doctor_id-id' + token).chained('#protocol-department_id-id' + token);

            $.pjax.reload({
                container: container
            });
        });
    })

    $(document).on('click', '#btn-add-diagnosis', function(event) {
        $.ajax({
            type: 'GET',
            url: '" . Url::to(['/diagnosis/diagnosis/template']) . "',
            data: {pacient_id: '" . $model->id . "'},
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                if (data)
                {
                    var template = $('#blk-diagnosis-template').clone().appendTo('#blk-diagnoses').removeAttr('id').show();

                    var link = template.find('.panel-title a');

                    link.attr('href', link.attr('href') + data.model.id);

                    var body = template.find('.panel-body');

                    body.attr('id', body.attr('id') + data.model.id);

                    body.html(data.template);

                    jQuery.when(jQuery('#diagnosis-handbook_emc_icd_id-id' + data.model.id).select2(select2_d35d49a5)).done(initS2Loading('diagnosis-handbook_emc_icd_id-id' + data.model.id, 's2options_6cc131ae'));
                    jQuery('#diagnosis-date_at-id' + data.model.id + '-kvdate').kvDatepicker(kvDatepicker_493f0995);

                    initWidgetDiagnosisAdditional(data.model.id);
                    initWidgetPacientOperation(data.model.id);
                    initWidgetProtocol(data.model.id);
                }
                else
                {
                    alert('" . Yii::t('app', 'The requested page does not exist.') . "');
                }
            }
        });
    })

"); ?>