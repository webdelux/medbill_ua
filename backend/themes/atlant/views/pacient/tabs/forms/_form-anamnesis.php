<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
//use kartik\date\DatePicker;
use kartik\select2\Select2;
use backend\modules\signalmark\models\PacientAnamnesis;

$countRow = PacientAnamnesis::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Anamnesis life'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-anamnesis',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Anamnesis life') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['PacientAnamnesis_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientAnamnesis_alert']['type']
                    ],
                    'body' => $models['PacientAnamnesis_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-pacient-anamnesis',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['PacientAnamnesis'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['PacientAnamnesis'], 'type', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList($models['PacientAnamnesis']::itemAlias('type'), ['prompt' => '']) ?>

                <?= $form->field($models['PacientAnamnesis'], 'handbook_emc_icd_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['PacientAnamnesis']->icd) ? '' : $models['PacientAnamnesis']->icd->name,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?= $form->field($models['PacientAnamnesis'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea(['rows' => 2]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-anamnesis',
                            'data-container' => '#pacient-anamnesis',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-anamnesis',
                'linkSelector' => '#pacient-anamnesis a[data-sort], #pacient-anamnesis a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'pacient-anamnesis-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => PacientAnamnesis::find()->where([
                        'pacient_id' => $model->id
                    ])->orderBy('type ASC'),
                    'pagination' => [
                        'pageSize' => 9,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'type',
                        'value' => function ($data) {
                            return $data::itemAlias('type', $data->type);
                        },
                        'group' => true,
                    ],
                    [
                        'attribute' => 'handbook_emc_icd_id',
                        'value' => function ($data) {
                            return (isset($data->icd->name)) ? $data->icd->name : '';
                        }
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/signalmark/pacient-anamnesis/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-anamnesis',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <?= $form->field($models['PacientSignalmark'], 'anamnesis_note')->textarea(['rows' => 2, 'maxlength' => true]) ?>
        </div>
    </div>
</fieldset>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2
//            if ($('#pacientanamnesis-handbook_emc_icd_id').length)
//            {
//                jQuery.when(jQuery('#pacientanamnesis-handbook_emc_icd_id').select2(select2_d35d49a5)).done(initS2Loading('pacientanamnesis-handbook_emc_icd_id','s2options_6cc131ae'));
//            }

        selectInit('pacientanamnesis-handbook_emc_icd_id');
        }

    });

"); ?>