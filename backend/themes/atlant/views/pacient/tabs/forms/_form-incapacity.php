<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\date\DatePicker;
use backend\modules\emc\models\Incapacity;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\handbook\models\Department;

$countRow = Incapacity::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">

    <div class="row form-body">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-incapacity',
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Incapacity') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['Incapacity_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['Incapacity_alert']['type']
                    ],
                    'body' => $models['Incapacity_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-pacient-incapacity',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['Incapacity'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['Incapacity'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                    ])->dropDownList(ArrayHelper::map(backend\modules\diagnosis\models\Diagnosis::find()
                            ->andWhere(['pacient_id' => $model->id])
                            ->andWhere(['parent_id' => NULL])
                            ->all(),
                            'id', 'icd.name'))
                ?>

                <?= $form->field($models['Incapacity'], 'doctor_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['Incapacity']->doctor) ? '' : $models['Incapacity']->doctor->name,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/user-viewer/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?= $form->field($models['Incapacity'], 'department_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                    ])->dropDownList((new Department)->treeList((new Department)->treeData()),  ['prompt' => ''])
                ?>

                <?= $form->field($models['Incapacity'], 'incapacity_type', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                    ])->dropDownList($models['Incapacity']::itemAlias('incapacity_type'),  ['prompt' => ''])
                ?>

                <?= $form->field($models['Incapacity'], 'incapacity_number', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea(['rows' => 1]) ?>


                <?= $form->field($models['Incapacity'], 'incapacity_date', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Incapacity']->incapacity_date) ? $models['Incapacity']->incapacity_date : date("Y-m-d")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?= $form->field($models['Incapacity'], 'incapacity_date_start', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Incapacity']->incapacity_date_start) ? $models['Incapacity']->incapacity_date_start : ''
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?= $form->field($models['Incapacity'], 'incapacity_date_end', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Incapacity']->incapacity_date_end) ? $models['Incapacity']->incapacity_date_end : ''
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>


                <?= $form->field($models['Incapacity'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea(['rows' => 1]) ?>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-incapacity',
                            'data-container' => '#incapacity',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'incapacity',
                'linkSelector' => '#incapacity a[data-sort], #incapacity a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'pacient-incapacity-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => Incapacity::find()->where([
                        'pacient_id' => $model->id
                    ])->orderBy('id ASC'),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [

                    'incapacity_number',
                    'incapacity_date',
                    'incapacity_date_start',
                    'incapacity_date_end',
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data)
                        {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'doctor_id',
                        'value' => function ($data)
                        {
                            return (isset($data->doctor->name)) ? $data->doctor->name : '';
                        },
                    ],
                    [
                        'attribute' => 'department_id',
                        'value' => function ($data)
                        {
                            return (isset($data->department->name)) ? $data->department->name : '';
                        },
                    ],
                    [
                        'attribute' => 'incapacity_type',
                        'value' => function ($data)
                        {
                            return $data::itemAlias('incapacity_type', ($data->incapacity_type) ? $data->incapacity_type : '');
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '50'],
                        'template' => '{edit}{delete}',
                        'buttons' => [
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/emc/incapacity/update',
                                        'id' => $data->id,
                                        'tab' => Yii::$app->request->get('tab', 1),
                                    ]),
                                    [
                                        'data-update' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Update'),
                                        'data-container' => '#incapacity',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/emc/incapacity/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#incapacity',
                                    ]
                                );
                            }

                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2, DepDrop
            if ($('#incapacity-doctor_id').length)
            {
                selectInit('incapacity-doctor_id');
            }
        }

    });

"); ?>