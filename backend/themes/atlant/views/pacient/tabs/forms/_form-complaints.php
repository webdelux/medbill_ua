<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\date\DatePicker;
use backend\modules\handbookemc\models\Organ;
use backend\modules\handbookemc\models\OrganComplaints;
use kartik\depdrop\DepDrop;
use backend\modules\signalmark\models\PacientComplaints;

$countRow = PacientComplaints::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Complaints and history of disease'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-complaints',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Complaints and history of disease') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['PacientComplaints_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientComplaints_alert']['type']
                    ],
                    'body' => $models['PacientComplaints_alert']['body']
                ]) : '' ?>

                <?= Html::activeHiddenInput($models['PacientComplaints'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['PacientComplaints'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['PacientComplaints']->date_at) ? $models['PacientComplaints']->date_at : date('Y-m-d')
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?= $form->field($models['PacientComplaints'], 'handbook_emc_organ_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList((new Organ)->treeList((new Organ)->treeData()), ['prompt' => '', 'id' => 'pacientcomplaints-organ_id']) ?>

                <?= $form->field($models['PacientComplaints'], 'handbook_emc_organ_complaints_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => ['id' => 'pacientcomplaints-organ_complaints_id'],
                    'pluginOptions' => [
                        'depends' => ['pacientcomplaints-organ_id'],
                        'placeholder' => '',
                        'url' => Url::to(['/handbookemc/organ-complaints/index'])
                    ]
                ]); ?>

                <?= $form->field($models['PacientComplaints'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea([
                    'rows' => 2,
                    'placeholder' => Yii::t('app', 'Day') . '/' . Yii::t('app', 'Week') . '/' . Yii::t('app', 'Month') . '/' . Yii::t('app', 'Year')
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-complaints',
                            'data-container' => '#pacient-complaints',
                        ]) ?>
                    </div>
                </div>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-complaints',
                'linkSelector' => '#pacient-complaints a[data-sort], #pacient-complaints a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'options' => [
                    'id' => 'pacient-complaints-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => PacientComplaints::find()->where([
                        'pacient_id' => $model->id
                    ])->orderBy('date_at DESC'),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'handbook_emc_organ_id',
                        'value' => function ($data) {

                            $prefix = '';
                            if ($data->organ->parent)
                                $prefix .= $data->organ->parent->name . ' &rarr; ';

                            return (isset($data->organ->name)) ? $prefix . $data->organ->name : '';
                        },
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'handbook_emc_organ_complaints_id',
                        'value' => function ($data) {
                            return (isset($data->organComplaints->name)) ? $data->organComplaints->name : '';
                        }
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/signalmark/pacient-complaints/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-complaints',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <?= $form->field($models['PacientSignalmark'], 'complaints_note')->textarea(['rows' => 2, 'maxlength' => true]) ?>
        </div>
    </div>
</fieldset>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            $('#pacientcomplaints-organ_id').val('');

            // Ініціалізація kartik-v/Select2, DepDrop
//            if ($('#pacientcomplaints-organ_complaints_id').length)
//            {
//                jQuery('#pacientcomplaints-organ_complaints_id').depdrop(depdrop_1b64ded5);
//                jQuery.when(jQuery('#pacientcomplaints-organ_complaints_id').select2(select2_7aac4114)).done(initS2Loading('pacientcomplaints-organ_complaints_id','s2options_d6851687'));
//                initDepdropS2('pacientcomplaints-organ_complaints_id','Loading ...');
//            }

            if ($('#pacientcomplaints-organ_complaints_id').length)
            {
                var depdr = el.attr('data-krajee-depdrop');

                jQuery('#pacientcomplaints-organ_complaints_id').depdrop(window[depdr]);
                selectInit('pacientcomplaints-organ_complaints_id');
                initDepdropS2('pacientcomplaints-organ_complaints_id','Loading ...');

            }
        }

    });

"); ?>