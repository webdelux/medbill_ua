<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\date\DatePicker;
use backend\modules\handbookemc\models\Category;
use kartik\depdrop\DepDrop;
use backend\modules\planning\models\PacientSurvey;
use backend\modules\diagnosis\models\Diagnosis;

$countRow = PacientSurvey::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Plan survey'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-survey',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Survey') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['PacientSurvey_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['PacientSurvey_alert']['type']
                    ],
                    'body' => $models['PacientSurvey_alert']['body']
                ]) : '' ?>

                <?= Html::activeHiddenInput($models['PacientSurvey'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= $form->field($models['PacientSurvey'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                        ->where(['pacient_id' => $model->id, 'type' => Diagnosis::TYPE_PREVIOUS])->all(), 'id', 'icd.name'), ['prompt' => '']) ?>

                <?= $form->field($models['PacientSurvey'], 'handbook_emc_category_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->dropDownList((new Category)->treeList((new Category)->treeData()), ['prompt' => '', 'id' => 'pacientsurvey-category_id']) ?>

                <?= $form->field($models['PacientSurvey'], 'handbook_emc_protocol_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => ['id' => 'pacientsurvey-protocol_id'],
                    'pluginOptions' => [
                        'depends' => ['pacientsurvey-category_id'],
                        'placeholder' => '',
                        'url' => Url::to(['/handbookemc/protocol/index'])
                    ]
                ]); ?>

                <?= $form->field($models['PacientSurvey'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea([
                    'rows' => 2,
                ]) ?>

                <?= $form->field($models['PacientSurvey'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['PacientSurvey']->date_at) ? $models['PacientSurvey']->date_at : date('Y-m-d')
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-pacient-survey',
                            'data-container' => '#pacient-survey',
                        ]) ?>
                    </div>
                </div>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-survey',
                'linkSelector' => '#pacient-survey a[data-sort], #pacient-survey a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'pacient-survey-grid'
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => PacientSurvey::find()->joinWith('protocol')->andWhere([
                        'pacient_id' => $model->id
                    ])->orderBy([
                        'diagnosis_id' => SORT_ASC,
                        'date_at' => SORT_ASC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data) {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                        'group' => true,
                        'headerOptions' => ['width' => '400'],
                    ],
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'handbook_emc_category_id',
                        'value' => function ($data) {
                            return (isset($data->protocol->category->name)) ? $data->protocol->category->name : '';
                        },
                    ],
                    [
                        'attribute' => 'handbook_emc_protocol_id',
                        'value' => function ($data) {
                            return (isset($data->protocol->name)) ? $data->protocol->name : '';
                        }
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/planning/pacient-survey/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-survey',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            $('#pacientsurvey-category_id').val('');

            // Ініціалізація kartik-v/Select2, DepDrop
            if ($('#pacientsurvey-protocol_id').length)
            {
                var depdr = el.attr('data-krajee-depdrop');

                jQuery('#pacientsurvey-protocol_id').depdrop(window[depdr]);
                selectInit('pacientsurvey-protocol_id');
                initDepdropS2('pacientsurvey-protocol_id','Loading ...');
            }
        }

    });

"); ?>