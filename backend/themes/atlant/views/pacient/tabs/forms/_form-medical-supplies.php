<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use kartik\select2\Select2;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\handbook\models\Department;
use yii\helpers\ArrayHelper;

$countRow = \backend\models\MedicalSuppliesOperation::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Medical Supplies'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-msupplies',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Medeical Supplies') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['MedicalSuppliesOperation_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['MedicalSuppliesOperation_alert']['type']
                    ],
                    'body' => $models['MedicalSuppliesOperation_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-msupplies',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['MedicalSuppliesOperation'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= Html::activeHiddenInput($models['MedicalSuppliesOperation'], 'fullName', [
                    'value' => 'init some text'
                ]) ?>

                <?= Html::activeHiddenInput($models['MedicalSuppliesOperation'], 'msupplies', [
                    'value' => 'init some text'
                ]) ?>

                <?php echo $form->field($models['MedicalSuppliesOperation'], 'diagnosis_id', [
                    'labelOptions' => ['class' => 'compacted-control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
                        ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                ->andWhere(['pacient_id' => $model->id])
                                ->andWhere(['parent_id' => NULL])
                                ->all(),
                                'id', 'icd.name'), ['prompt' => '']);
                ?>



                <?=
                    $form->field($models['MedicalSuppliesOperation'], 'department_id', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->dropDownList((new Department)->treeList(),
                        ['prompt' => ''])
                ?>

                <?= $form->field($models['MedicalSuppliesOperation'], 'medical_supplies_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['MedicalSuppliesOperation']->medicalSuppliesMeasurement) ? '' : $models['MedicalSuppliesOperation']->medicalSuppliesMeasurement,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/handbook/medical-supplies/index', 'results' => true]),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?=
                    $form->field($models['MedicalSuppliesOperation'], 'quantity', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?=
                    $form->field($models['MedicalSuppliesOperation'], 'discount', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?= $form->field($models['MedicalSuppliesOperation'], 'docum_date', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(\kartik\date\DatePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['MedicalSuppliesOperation']->docum_date) ? $models['MedicalSuppliesOperation']->docum_date : date("Y-m-d")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?=
                    $form->field($models['MedicalSuppliesOperation'], 'docum_num', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?=
                    $form->field($models['MedicalSuppliesOperation'], 'docum', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?=
                    $form->field($models['MedicalSuppliesOperation'], 'description', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-msupplies',
                            'data-container' => '#msupplies',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'msupplies',
                'linkSelector' => '#msupplies a[data-sort], #msupplies a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'msupplies-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => \backend\models\MedicalSuppliesOperation::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'docum_date' => SORT_DESC,
                        'id' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '80'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
                    ],
//                    [
//                        'label' => Yii::t('app', 'Pacient'),
//                        'attribute' => 'fullName',
//                        'contentOptions' => ['width' => '200'],
//                        'value' => function ($data)
//                        {
//                            return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
//                        },
//                    ],
                    [
                        'attribute' => 'docum_date',
                        //'format' => ['date', 'dd MMM Y hh:mm'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return ($data->docum_date) ? $data->docum_date : NULL;
                        }
                    ],
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data)
                        {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : '';
                        },
                    ],
                    [
                        'attribute' => 'department_id',
                        'value' => function ($data)
                        {
                            return (isset($data->department->name)) ? $data->department->name : '';
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Medical Supplies'),
                        'attribute' => 'supplies',
                        'value' => function ($data)
                        {
                            return (isset($data->medicalSuppliesMeasurement)) ? $data->medicalSuppliesMeasurement . ' - ' .$data->medicalSupplies->category->name  : '';
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Quantity'),    //(+ надходження,  - витрата)
                        'attribute' => 'quantity',
                        'contentOptions' => function ($data)
                        {
                            return [
                                'width' => '80',
                                'class' => ($data->quantity > 0) ? 'success' : 'danger'
                            ];
                        },
                    ],
                    [
                        'attribute' => 'discount',
                        'value' => function ($data)
                        {
                            return (isset($data->discount)) ? $data->discount : '';
                        },
                    ],
                    [
                        'attribute' => 'total',
                        'value' => function ($data)
                        {
                            return (isset($data->total)) ? $data->total : '';
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{edit} {delete}',
                        'buttons' => [
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/medical-supplies-operation/update',
                                        'id' => $data->id,
                                        'tab' => 9,
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Edit'),
                                        'data-container' => '#msupplies',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/medical-supplies-operation/delete',
                                        'id' => $data->id,
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#msupplies',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

"); ?>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {
    
        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2
            selectInit('medicalsuppliesoperation-medical_supplies_id');

        }
    });

");?>