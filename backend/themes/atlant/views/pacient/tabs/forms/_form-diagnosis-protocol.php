<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\Pjax;

use yii\bootstrap\Modal;
use yii\bootstrap\Alert;

use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

use backend\models\Protocol;
use backend\models\ProtocolSearch;
use backend\models\Pacient;
use backend\models\User;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\handbook\models\Department;
use backend\modules\handbookemc\models\Category;

$countRow = Protocol::find()->andWhere(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Protocols'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-pacient-protocol',
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['Protocol_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['Protocol_alert']['type']
                    ],
                    'body' => $models['Protocol_alert']['body']
                ]) : '' ?>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'diagnosis_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList(ArrayHelper::map(Diagnosis::find()
                                ->where(['pacient_id' => $model->id])->main()->all(), 'id', 'icd.name'), ['prompt' => '']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'department_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList((new Department)->getTreeListByUserId(), [
                            'prompt' => '',
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= Html::activeHiddenInput($models['Protocol'], 'pacient_id', [
                            'value' => $model->id,
                            'id' => null,
                        ]) ?>

                        <?php if (!isset($models['Protocol']->pacient)): ?>
                            <?php $models['Protocol']->pacient_id = $model->id; ?>
                        <?php endif; ?>

                        <?= $form->field($models['Protocol'], 'pacient_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(Select2::classname(), [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'initValueText' => Pacient::findOne($model->id)->fullName,
                            'disabled' => true,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/pacient/index', 'results' => true]),
                                    'dataType' => 'json',
                                ]
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-6">

                        <?php
                        // Список всіх користувачів з відділень доступ до яких має поточний користувач
                        $doctors = User::getListByDepartments(array_keys((new Department)->getListByUserId()));

                        // Chained Selects Plugin
                        $doctorsOptions = [];
                        foreach ($doctors as $key => $value)
                            $doctorsOptions[$key] = ['class' => implode(' ', array_keys((new Department)->getListParentsByUserId($key)))];
                        ?>

                        <?= $form->field($models['Protocol'], 'doctor_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList($doctors, [
                            'prompt' => '',
                            // Chained Selects Plugin
                            'options' => $doctorsOptions
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">

                        <?php
                        if (empty($models['Protocol']->protocol_id))
                            $models['Protocol']->protocol_id = 103; // Стандарт лаборат. обст. №1
                        ?>

                        <?= $form->field($models['Protocol'], 'protocol_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} '
                            . '<div class="col-sm-8">'
                                . '<div class="input-group">'
                                    . '{input}'
                                        . '<span class="input-group-btn">'
                                        . Html::button('<span class="glyphicon glyphicon-plus"></span>',[
                                                'class' => 'btn btn-primary',
                                                'id' => 'add_protocol_btn',
                                            ])
                                    . '</span>'
                                . '</div>'
                                . '{error}{hint}'
                            . '</div>'
                        ])->widget(Select2::classname(), [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'initValueText' => empty($models['Protocol']->protocol) ? '' : $models['Protocol']->protocol->name,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/handbookemc/protocol/index', 'results' => true]),
                                    'dataType' => 'json',
                                ]
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'refferal_id', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->widget(Select2::classname(), [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'initValueText' => empty($models['Protocol']->refferal) ? '' : $models['Protocol']->refferal->pib,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/handbook/refferal/index', 'results' => true]),
                                    'dataType' => 'json',
                                ]
                            ],
                        ]); ?>
                    </div>
                </div>

                <div class="row modal-row multiprotocol">

                    <label class="checkbox control-label col-sm-12 chk" style="display: none;">
                        <input class="chk" type="checkbox" name="multiprotocol[]" />
                    </label>

                    <div class="col-md-6 odd">
                        <?php if (isset($_POST['multiprotocol']) && is_array($_POST['multiprotocol'])): ?>
                            <?php foreach ($_POST['multiprotocol'] as $protocol): ?>

                                <label class="control-label col-sm-12 chk">
                                    <input class="chk" type="checkbox" name="multiprotocol[]" value="<?php echo $protocol; ?>" checked="checked" />
                                    <?= backend\modules\handbookemc\models\Protocol::findOne($protocol)->name; ?>
                                    (
                                    <?= backend\modules\handbookemc\models\Protocol::findOne($protocol)->category->name; ?>
                                    )
                                </label>

                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-6 even">
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'total', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textInput([
                            'maxlength' => true,
                            'style' => 'width: 100px;'
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'type', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList($models['Protocol']::itemAlias('type'), [
                            'prompt' => ''
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'discount', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textInput([
                            'maxlength' => true,
                            'style' => 'width: 100px;'
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'place', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList($models['Protocol']::itemAlias('place'), [
                            'prompt' => ''
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'protocol_date', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label}'
                            . '<div class="col-sm-8 datetime-picker">'
                                . '<div class="input-group">'
                                    . '{input}'
                                    . '<span class="input-group-btn">'
                                        . Html::button('<span class="fa fa-list"></span>', [
                                            'class' => 'btn btn-primary btn-sm',
                                            'data-toggle' => 'modal',
                                            'data-target' => "#modal-protocol-calendar",
                                        ])
                                    . '</span>'
                                . '</div>'
                                . '{error}{hint}'
                            . '</div>'
                        ])->widget(DateTimePicker::classname(), [
                            'options' => [
                                'value' => ($models['Protocol']->protocol_date) ? $models['Protocol']->protocol_date : date('Y-m-d H:i'),
                            ],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd hh:ii'
                            ]
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'status', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->dropDownList($models['Protocol']::itemAlias('status'), [
                            'prompt' => ''
                        ]) ?>
                    </div>
                </div>

                <div class="row modal-row">
                    <div class="col-md-6">
                        <?= $form->field($models['Protocol'], 'description', [
                            'labelOptions' => ['class' => 'control-label col-sm-4'],
                            'inputOptions' => ['class' => 'form-control'],
                            'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
                        ])->textarea([
                            'rows' => 2
                        ]) ?>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                                <?= Html::button(Yii::t('app', 'Save'), [
                                    'class' => 'btn btn-primary',
                                    'data-save' => '',
                                    'data-modal' => '#modal-pacient-protocol',
                                    'data-container' => '#pacient-protocol',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'pacient-protocol',
                'linkSelector' => '#pacient-protocol a[data-sort], #pacient-protocol a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?php
                $searchModel = new ProtocolSearch();
                $searchModel->pacient_id = $model->id;
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'pacient-protocol-grid'
                ],
                /*
                'dataProvider' => new ActiveDataProvider([
                    'query' => Protocol::find()->andWhere([
                        'pacient_id' => $model->id
                    ])->orderBy([
                        'diagnosis_id' => SORT_ASC,
                        'protocol_date' => SORT_ASC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                */
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'diagnosis_id',
                        'value' => function ($data) {
                            return (isset($data->diagnosis->icd->name)) ? $data->diagnosis->icd->name : null;
                        },
                        'group' => true,
                        'filter' => Html::activeDropDownList($searchModel, 'diagnosis_id',
                            ArrayHelper::map(Diagnosis::find()->where(['pacient_id' => $model->id])->main()->all(), 'id', 'icd.name'),
                            ['class' => 'form-control', 'prompt' => '']
                        )
                    ],
                    [
                        'headerOptions' => ['width' => '150'],
                        'attribute' => 'protocol_date',
                        'format' => ['date', 'yyyy-MM-dd'],
                        'filterType' => GridView::FILTER_DATE,
                        'filterWidgetOptions' => [
                            'removeButton' => false,
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true,
                                'todayHighlight' => true,
                            ]
                        ],
                        /*
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'protocol_date',
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]),
                        */
                        /*
                        'filter' => \yii\jui\DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'protocol_date',
                            'dateFormat' => 'php:Y-m-d',
                        ]),
                        */
                    ],
                    [
                        'attribute' => 'department_id',
                        'value' => function ($data) {
                            return (isset($data->department->name))
                                ? (null !== $data->department->parentsName($data->department->id, 3))
                                    ? '(' . $data->department->parentsName($data->department->id, 3) . ') ' . $data->department->name
                                    : $data->department->name
                                : '';
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'department_id', (new Department)->getTreeListByUserId(),
                            ['class' => 'form-control', 'prompt' => '']),
                    ],
                    [
                        'attribute' => 'category_id',
                        'value' => function ($data) {
                            return (isset($data->category->name))
                                ? (null !== $data->category->parentsName($data->category->id, 3))
                                    ? '(' . $data->category->parentsName($data->category->id, 3) . ') ' . $data->category->name
                                    : $data->category->name
                                : '';
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'category_id', (new Category)->treeList((new Category)->treeData()),
                            ['class' => 'form-control', 'prompt' => '']
                        ),
                    ],
                    [
                        'attribute' => 'protocol',
                        'value' => function ($data) {
                            return (isset($data->protocol->name)) ? $data->protocol->name : '';
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'protocol_id',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/handbookemc/protocol/index', 'results' => true]),
                                    'dataType' => 'json',
                                ]
                            ],
                        ])
                    ],
                    [
                        'attribute' => 'total',
                    ],
                    [
                        'attribute' => 'discount',
                    ],
                    [
                        'label' => Yii::t('app', 'Doctor'),
                        'attribute' => 'doctor',
                        'value' => function ($data) {
                            return (isset($data->doctor->name)) ? $data->doctor->name : '';
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'doctor_id', $doctors, [
                            'class' => 'form-control',
                            'prompt' => '',
                            // Chained Selects Plugin
                            'options' => $doctorsOptions
                        ])
                        /*
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'doctor_id',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/user-viewer/index', 'results' => true]),
                                    'dataType' => 'json',
                                ]
                            ],
                        ])
                        */
                    ],
                    [
                        'attribute' => 'refferal',
                        'value' => function ($data) {
                            return (isset($data->refferal->pib))
                                ? (null !== $data->refferal->category->parentsName($data->refferal->category_id, 3))
                                    ? '(' . $data->refferal->category->parentsName($data->refferal->category_id, 3) . ') ' . $data->refferal->pib
                                    : $data->refferal->pib
                                : '';
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'refferal_id',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/handbook/refferal/index', 'results' => true]),
                                    'dataType' => 'json',
                                ]
                            ],
                        ])
                    ],
                    [
                        'attribute' => 'type',
                        'value' => function ($data) {
                            return $data::itemAlias('type', ($data->type) ? $data->type : '');
                        },
                        'filter' => Protocol::itemAlias('type')
                    ],
                    [
                        'attribute' => 'place',
                        'value' => function ($data) {
                            return $data::itemAlias('place', ($data->place) ? $data->place : '');
                        },
                        'filter' => Protocol::itemAlias('place')
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function ($data) {
                            return $data::itemAlias('status', ($data->status) ? $data->status : '');
                        },
                        'filter' => Protocol::itemAlias('status'),
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '65'],
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                    Url::toRoute([
                                        '/protocol/view',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'View'),
                                        'data-target' => "#modal-pacient-protocol-view",
                                        'data-toggle' => "modal",
                                    ]
                                );
                            },
                            'update' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/protocol/update',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Update'),
                                        'data-target' => "#modal-pacient-protocol-update",
                                        'data-toggle' => "modal",
                                        'data-container' => '#pacient-protocol',
                                        'data-token' => $data->id . $data->diagnosis_id,
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/protocol/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#pacient-protocol',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php Modal::begin([
    'id' => "modal-pacient-protocol-view",
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
    'clientOptions' => [
        'remote' => false
    ],
]); ?>
<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => "modal-pacient-protocol-update",
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
    'clientOptions' => [
        'remote' => false
    ],
]); ?>

<div class="modal-form">
    <div class="modal-form-body"></div>
    <div class="modal-form-footer" style="display: none;">
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'id' => 'btn-pacient-protocol-update'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => "modal-protocol-calendar",
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
]); ?>

<div class="modal-fullcalendar">

    <div class="well well-sm" id="modal-filter-protocol-calendar">
        <div class="row">
            <div class="col-md-3">
                <?php echo Html::activeLabel($models['Protocol'], 'doctor_id'); ?>
                <?php echo Html::activeDropDownList($models['Protocol'], 'doctor_id', $doctors, [
                    'id' => Html::getInputId($models['Protocol'], 'doctor_id') . "-fc",
                    'name' => false,
                    'class' => 'form-control',
                    'prompt' => '',
                    // Chained Selects Plugin
                    'options' => $doctorsOptions
                ]); ?>
            </div>
            <div class="col-md-3">
                <?php echo Html::activeLabel($models['Protocol'], 'department_id'); ?>
                <?php echo Html::activeDropDownList($models['Protocol'], 'department_id', (new Department)->getTreeListByUserId(), [
                    'id' => Html::getInputId($models['Protocol'], 'department_id') . "-fc",
                    'name' => false,
                    'class' => 'form-control',
                    'prompt' => '',
                ]); ?>
            </div>
            <div class="col-md-3">
                <?php echo Html::activeLabel($models['Protocol'], 'category_id'); ?>
                <?php echo Html::activeDropDownList($models['Protocol'], 'category_id', (new Category)->treeList((new Category)->treeData()), [
                    'id' => Html::getInputId($models['Protocol'], 'category_id') . "-fc",
                    'name' => false,
                    'class' => 'form-control',
                    'prompt' => '',
                ]); ?>
            </div>
            <div class="col-md-3">
                <label>&nbsp;</label>
                <?= Html::button(Yii::t('app', 'Show') . '/' . Yii::t('app', 'Refresh'), [
                    'class' => 'btn btn-default btn-block',
                    'id' => Html::getInputId($models['Protocol'], 'id') . "-btn-refresh-fc",
                ]) ?>
            </div>
        </div>
    </div>

    <?php $fcTime = (new Department)->getBetweenWorking(); ?>

    <?php echo \yii2fullcalendar\yii2fullcalendar::widget(array(
        'id' => "protocol-calendar",
        'options' => [
            'lang' => substr(Yii::$app->language, 0, 2),
        ],
        'clientOptions' => [
            'allDaySlot' => false,
            'firstDay' => 1,
            'defaultView' => 'agendaDay',
            'minTime' => $fcTime->from,
            'maxTime' => $fcTime->to,
            'slotDuration' => '00:20:00',
            'defaultTimedEventDuration' => '00:20:00',
            'slotLabelFormat' => 'HH:mm',
            'timezone' => Yii::$app->timeZone,
            'height' => 'auto',
            'contentHeight' => 'auto',
            'fixedWeekCount' => false,
            'displayEventTime' => false,
            'eventRender' => new \yii\web\JsExpression('function(event, element, view) {
                element.popover({
                    title: event.title,
                    placement: "bottom",
                    trigger: "hover",
                    content: event.description,
                    container: "body"
                });
            }'),
            'eventClick' => new \yii\web\JsExpression('function(event, jsEvent, view) {
                if (event.url) {
                    window.open(event.url);
                    return false;
                }
            }'),
            'dayClick' => new \yii\web\JsExpression('function(date, jsEvent, view, resourceObj) {
                setValueFieldByProtocolDate(date);
            }'),
        ],
        'header' => [
            'center' => 'title',
            'left' => 'prev,next today',
            'right' => 'month,agendaWeek,agendaDay',
        ],
        //'ajaxEvents' => Url::to(['/protocol/calendar'])
        'ajaxEvents' => new \yii\web\JsExpression('{
            url: "' . Url::to(['/protocol/calendar']) . '",
            data: function() {
                return {
                    doctor_id: $("#protocol-doctor_id-fc").val(),
                    department_id: $("#protocol-department_id-fc").val(),
                    category_id: $("#protocol-category_id-fc").val()
                };
            }
        }'),
    )); ?>

</div>

<?php Modal::end(); ?>


<?php echo $this->render('//modules/pattern/views/protocol/_modal'); ?>


<?php $this->registerCss("

    .form-group.field-protocol-protocol_date input {
        border-radius: 0;
        font-size: 13px;
    }
    .form-group.field-protocol-protocol_date .input-group-btn > .btn {
        margin-left: 0;
        line-height: 24px;
    }

    .modal .modal-content .modal-body .modal-fullcalendar {
        border-bottom: 1px solid #ddd;
    }

"); ?>


<?php $this->registerJs("

        function selectInit(itemname){
            if ($('#' + itemname).length)
            {
                var el = $('#' + itemname);
                var settings = el.attr('data-krajee-select2');

                var settings1 = el.attr('data-s2-options');

                jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

            }
        }

        function dateInit(itemname){
            if ($('#' + itemname).length)
            {
                var el = $('#' + itemname);
                var settings = el.attr('data-krajee-datetimepicker');
                jQuery('#' + itemname).datetimepicker(window[settings]);

            }
        }

    /**
     * jQuery UI Tooltip Widget
     */
    $('body').tooltip({
        disabled: true
    });

    // Chained Selects Plugin
    $('#protocolsearch-doctor_id').chained('#protocolsearch-department_id');

    $('body').on('beforeSubmit', 'form#pacient-form-tab-15', function (event) {
        $.pjax.reload({
            url: '" . Url::current() . "&' + $('#pacient-protocol-grid-filters input, #pacient-protocol-grid-filters select').serialize(),
            container: '#pacient-protocol',
            method: 'GET',
            push: false,
            replace: false,
            timeout: 1000,
            scrollTo: false
        });
        return false;
    });

    /**
     * Multiple modals overlay (Backdrop z-index fix).
     * This solution uses a setTimeout because the .modal-backdrop isn't created when the event show.bs.modal is triggered.
     */
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    /**
     * Scrollbar fix.
     * If you have a modal on your page that exceeds the browser height, then you can't scroll in it when closing an second modal.
     */
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

    $('#protocol-protocol_id').on('select2:select', function (e) {
        $('#protocol-total').val(e.params.data.amount);
    });

    function initWidgetProtocol()
    {
        selectInit('protocol-pacient_id');
        selectInit('protocol-protocol_id');
        selectInit('protocol-refferal_id');

        dateInit('protocol-protocol_date-datetime');

        // Chained Selects Plugin
        $('#protocol-doctor_id').chained('#protocol-department_id');
    }

    function initWidgetProtocolUpdate(id)
    {
        selectInit('protocol-pacient_id-id' + id);
        selectInit('protocol-protocol_id-id' + id);
        selectInit('protocol-refferal_id-id' + id);

        dateInit('protocol-protocol_date-id' + id);

        jQuery('#protocol-pacient_id-id' + id).prop('disabled', true);

        // Chained Selects Plugin
        $('#protocol-doctor_id-id' + id).chained('#protocol-department_id-id' + id);

        $('#protocol-protocol_id-id' + id).on('select2:select', function (e) {
            $('#btn-protocol-pattern-id' + id).attr('data-protocol', e.params.data.id);
            $('#btn-protocol-pattern-id' + id).attr('data-protocol-name', e.params.data.label);
        });
    }

    function setValueFieldByProtocolDate(date)
    {
        $('#protocol-protocol_date').val(date.format('YYYY-MM-DD HH:mm'));
        $('#modal-protocol-calendar').modal('hide');
    }

    function initWidgetFullCalendar()
    {
        // Chained Selects Plugin
        $('#protocol-doctor_id').chained('#protocol-department_id');
        $('#protocol-doctor_id-fc').chained('#protocol-department_id-fc');

        $('#modal-protocol-calendar').on('hidden.bs.modal', function (e) {

            $('#protocol-category_id').val($('#protocol-category_id-fc').val());
            $('#protocol-department_id').val($('#protocol-department_id-fc').val());

            // Chained Selects Plugin
            $('#protocol-department_id').trigger('change');
            $('#protocol-doctor_id').val($('#protocol-doctor_id-fc').val());
        });

        $('#modal-protocol-calendar').on('shown.bs.modal', function (e) {

            $('#protocol-category_id-fc').val($('#protocol-category_id').val());
            $('#protocol-department_id-fc').val($('#protocol-department_id').val());

            // Chained Selects Plugin
            $('#protocol-department_id-fc').trigger('change');
            $('#protocol-doctor_id-fc').val($('#protocol-doctor_id').val());

            $('#protocol-calendar').fullCalendar('render');
        });

        $(document).on('click', '#protocol-id-btn-refresh-fc', function(event) {
            $('#protocol-calendar').fullCalendar('refetchEvents');
        })
    }
    initWidgetFullCalendar();

    $('#modal-pacient-protocol-view').on('show.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
    });
    $('#modal-pacient-protocol-view').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-body').load(button.attr('href') + ' #protocol-view', function(response, status, xhr) {
            if (status == 'success') {
                // ...
            }
        });
    });
    $('#modal-pacient-protocol-view').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body').html('');
    });

    $('#modal-pacient-protocol-update').on('shown.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);
        modal.find('.modal-form-body').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
        modal.find('.modal-form-body').load(button.attr('href') + ' #protocol-form-content', function(response, status, xhr) {
            if (status == 'success') {
                initWidgetProtocolUpdate(button.attr('data-token'));

                $('#btn-pacient-protocol-update').attr('data-href', button.attr('href'));
                $('#btn-pacient-protocol-update').attr('data-container', button.attr('data-container'));
                $('#btn-pacient-protocol-update').attr('data-token', button.attr('data-token'));

                modal.find('.modal-form-footer').show();
            }
        });
    });
    $('#modal-pacient-protocol-update').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-form-body').html('');
        modal.find('.modal-form-footer').hide();
    });
    $(document).on('click', '#btn-pacient-protocol-update', function(event) {
        var modal = $('#modal-pacient-protocol-update');
        var container = $(this).attr('data-container');
        var token = $(this).attr('data-token');

        $.ajax({
            method: 'POST',
            url: $(this).attr('data-href'),
            data: modal.find('input, textarea, select').serialize(),
            dataType: 'html'
        })
        .done(function(data) {
            modal.find('#protocol-form-content').html($(data).find('#protocol-form-content').html());
            modal.find('input').prop('disabled', false);

            initWidgetProtocolUpdate(token);

            $.pjax.reload({
                container: container
            });
        });
    })

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            initWidgetProtocol();

            $('#protocol-protocol_id').on('select2:select', function (e) {
                $('#protocol-total').val(e.params.data.amount);
            });

            initMyBtn();
            updCheckBox();
        }

    });

    // Створюємо (копіюємо) dom -  елементи з поточного значення select'a
    var arr = [];

    function in_array(needle, arr) {
        return (arr.indexOf(needle) !== -1);
    }

    function protocolSum(){
        $.ajax({
            type: 'POST',
            url: '" . Url::toRoute(['protocol-sum']) . "',
            data: $('#pacient-form-tab-15').serialize(),
            dataType: 'json',
            success: function(data)
            {
                $('#protocol-total').val(data.sum.toFixed(2))
            }
        });
    }


    function initMyBtn(){
        $('#add_protocol_btn').on('click', function(event) {
            var protocol = $('#protocol-protocol_id option:selected');

            if (protocol.val())
            {
                if (!in_array(protocol.val(), arr))
                {
                    arr.push(protocol.val());

                    var el = $('.multiprotocol .checkbox').clone().show().removeClass('checkbox');
                    el.find('input').val(protocol.val());
                    el.find('input').prop('checked', true);
                    el.append(protocol.text());

                    el.prependTo('.multiprotocol .odd');

                    protocolSum();
                }
                else alert('" . Yii::t('app', 'Value is duplicated!') . "');
            }
        })
    }

    function updCheckBox(){
        $('.multiprotocol').change(function() {
            protocolSum();
        });

        $('#protocol-protocol_id').change(function() {
            protocolSum();
        });
    }

    initMyBtn();
    updCheckBox();

"); ?>