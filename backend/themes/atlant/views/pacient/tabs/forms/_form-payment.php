<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use backend\modules\handbook\models\PaymentMethods;

$countRow = \backend\models\MedicalSuppliesOperation::find()->where(['pacient_id' => $model->id])->count();
?>

<fieldset class="form-group">
    <legend class="form-heading">
        <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
            <?php echo Yii::t('app', 'Payments'); ?>
        </a>
    </legend>
    <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
        <div class="col-md-12">
            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => 'modal-payment',
                    'size' => Modal::SIZE_DEFAULT,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Payments') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['Payment_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['Payment_alert']['type']
                    ],
                    'body' => $models['Payment_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => 'form-payment',
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['Payment'], 'pacient_id', [
                    'value' => $model->id
                ]) ?>

                <?= Html::activeHiddenInput($models['Payment'], 'pacientFullName', [
                    'value' => 'init some text'
                ]) ?>

                <?=
                    $form->field($models['Payment'], 'total', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?=
                    $form->field($models['Payment'], 'discount', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?= $form->field($models['Payment'], 'date_payment', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(\kartik\date\DatePicker::classname(), [
                    'options' => [
                        //'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['Payment']->date_payment) ? $models['Payment']->date_payment : date("Y-m-d")
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <?=
                    $form->field($models['Payment'], 'type', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->dropDownList($models['Payment']::itemAlias('type'),
                        ['prompt' => ''])
                ?>

                <?=
                    $form->field($models['Payment'], 'description', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->textInput(['maxlength' => true])
                ?>

                <?=
                    $form->field($models['Payment'], 'payment_method_id', [
                        'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',])
                    ->dropDownList(ArrayHelper::map(PaymentMethods::find()->all(), 'id', 'name'),
                        ['prompt' => ''])
                ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => '#modal-payment',
                            'data-container' => '#payment',
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => 'payment',
                'linkSelector' => '#payment a[data-sort], #payment a[data-page]',
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => 'payment-grid'
                ],
                'striped' => false,
                'dataProvider' => new ActiveDataProvider([
                    'query' => \backend\models\Payment::find()->where([
                        'pacient_id' => $model->id,
                    ])->orderBy([
                        'date_payment' => SORT_DESC,
                        'id' => SORT_DESC,
                    ]),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '80'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
                    ],
                    [
                        'label' => Yii::t('app', 'Pacient'),
                        'attribute' => 'fullName',
                        'contentOptions' => ['width' => '200'],
                        'value' => function ($data)
                        {
                            return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
                        },
                    ],
                    [
                        'attribute' => 'date_payment',
                        //'format' => ['date', 'dd MMM Y hh:mm'],
                        'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return ($data->date_payment) ? $data->date_payment : NULL;
                        }
                    ],
                    [
                        'attribute' => 'total',
                        'contentOptions' => ['width' => '80'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
                        'contentOptions' => function ($data)
                        {
                            return [
                                'width' => '80',
                                'class' => ($data->total > 0) ? 'success' : 'danger'
                            ];
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Discount'),
                        'attribute' => 'discount',
                        'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '100'],
                        'value' => function ($data)
                        {
                            return (isset($data->discount)) ? $data->discount : '';
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Type'),
                        'attribute' => 'type',
                        'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '100'],
                        'value' => function ($data)
                        {
                            return backend\models\Payment::itemAlias('type', ($data->type) ? $data->type : '');
                        },
                    ],
                    [
                        'attribute' => 'description',
                        'contentOptions' => ['width' => '80'],
                        'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
                    ],
                    [
                        'label' => Yii::t('app', 'Payment method'),
                        'attribute' => 'payment_method_id',
                        'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                        'value' => function ($data)
                        {
                            return (isset($data->paymentMethod->name)) ? $data->paymentMethod->name : null;
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '80'],
                        'template' => '{edit} {delete}',
                        'buttons' => [
                            'edit' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute([
                                        '/payment/update',
                                        'id' => $data->id,
                                        'tab' => 11,
                                    ]),
                                    [
                                        'title' => Yii::t('app', 'Edit'),
                                        'data-container' => '#payment',
                                    ]
                                );
                            },
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/payment/delete',
                                        'id' => $data->id,
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => '#payment',
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
    </div>
</fieldset>


<?php $this->registerCss("

"); ?>


<?php $this->registerJs("
    function selectInit(itemname){
        if ($('#' + itemname).length)
        {
            var el = $('#' + itemname);
            var settings = el.attr('data-krajee-select2');

            var settings1 = el.attr('data-s2-options');

            jQuery.when(jQuery('#' + itemname).select2(window[settings])).done(initS2Loading(itemname, settings1));

        }
    }

    // Оброблювач завершення всіх поточних ajax-запитів
    $(document).ajaxStop(function(e) {

        var a = $(e.currentTarget.activeElement);

        // Якщо перезавантажено ajax-ом модальне вікно
        if (a.attr('class') == 'modal-open')
        {
            // Ініціалізація kartik-v/Select2
            selectInit('medicalsuppliesoperation-medical_supplies_id');
        }

    });

");?>