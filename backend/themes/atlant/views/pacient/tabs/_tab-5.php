<?php

use yii\helpers\Html;
?>

<?= Html::activeHiddenInput($model, 'id') ?>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->render('forms/_form-diagnosis', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php //echo $this->render('//modules/handbookemc/views/icd/_selector'); ?>

    </div>
</div>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

    // ...

"); ?>