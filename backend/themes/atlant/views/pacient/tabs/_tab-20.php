<?php

use yii\helpers\Html;
?>

<?= Html::activeHiddenInput($model, 'id') ?>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->render('forms/_form-docums', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
            'tab' => $tab,
        ]); ?>

    </div>
</div>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

    // ...

"); ?>