<?php

use yii\helpers\Html;
?>

<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($models['PacientSignalmark'], 'id') ?>
<?= Html::activeHiddenInput($models['PacientPlanning'], 'id') ?>

<div class="row">
    <div class="col-md-6">

        <fieldset class="form-group">
            <legend class="form-heading">
                <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
                    <?php echo Yii::t('app', 'Classification of blood'); ?>
                </a>
            </legend>
            <?php $countRow = ($models['PacientSignalmark']->blood_group || $models['PacientSignalmark']->blood_rhesus) ? true : false; ?>
            <div class="row form-body" style="display: <?php echo ($countRow) ? 'block' : 'none'; ?>;">
                <div class="col-md-6">
                    <?= $form->field($models['PacientSignalmark'], 'blood_group')
                        ->dropDownList($models['PacientSignalmark']::itemAlias('blood_group'), ['prompt' => '']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($models['PacientSignalmark'], 'blood_rhesus')
                        ->dropDownList($models['PacientSignalmark']::itemAlias('blood_rhesus'), ['prompt' => '']) ?>
                </div>
            </div>
        </fieldset>

        <?php echo $this->render('forms/_form-transfusion', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-infectious', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-allergic', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-anamnesis', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-overall', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

    </div>
    <div class="col-md-6">

        <?php echo $this->render('forms/_form-diabetes', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-surgery', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-intolerance', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-complaints', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

        <?php echo $this->render('forms/_form-hypertension', [
            'model' => $model,
            'models' => $models,
            'form' => $form,
        ]); ?>

    </div>
</div>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

    // ...

"); ?>