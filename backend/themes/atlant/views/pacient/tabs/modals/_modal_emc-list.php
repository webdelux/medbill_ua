<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use yii\web\JsExpression;
?>

<?php Modal::begin([
    'id' => "emc-list-{$parentId}-modal",
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4 class="modal-title">' . $title . '</h4>',
    //'toggleButton' => ['label' => $title, 'class' => 'btn btn-primary btn-sm btn-block'],
    'clientEvents' => [
        'show.bs.modal' => new JsExpression("function (event) {
            // Закриваємо всі відкриті модальні вікна
            $('.modal').modal('hide');
            // Об'єкт модального вікна
            var modal = $(this);
            // Запамятовуємо ідентифікатор батьківського контейнера
            var container = $(event.relatedTarget).attr('data-container');
            modal.attr('data-container', container);
            // Робимо доступними всі поля в відкритому модальному вікні
            modal.find('.modal-body input').prop('disabled', false);
        }"),
        'hide.bs.modal' => new JsExpression("function (event) {
            // При закриванні вікна - блокуємо всі поля в модальному вікні
            var modal = $(this);
            modal.find('.modal-body input').prop('disabled', true);
        }"),
        'hidden.bs.modal' => new JsExpression("function (event) {
            // Оновлюємо контент батьківського контейнера
            var container = $(event.currentTarget).attr('data-container');
            $.pjax.reload({container: container});
        }"),
    ]
]); ?>

<div class="well well-sm">

    <?= (isset($models['EmcList_alert'])) ? Alert::widget([
        'options' => [
            'class' => 'alert-' . $models['EmcList_alert']['type'],
        ],
        'body' => $models['EmcList_alert']['body']
    ]) : '' ?>

    <div style="clear: both;"></div>
    <div class="flex-display">

        <?= Html::activeHiddenInput($models['EmcList'], 'parent_id', [
            'value' => $parentId,
            'disabled' => 'disabled',
        ]) ?>

        <?= Html::activeInput('text', $models['EmcList'], 'name', [
            'class' => 'form-control',
            'disabled' => 'disabled',
            'placeholder' => Yii::t('app', 'Name')
        ]) ?>

        <?= Html::button(Yii::t('app', 'Save'), [
            'style' => 'margin-left: 5px;',
            'class' => 'btn btn-primary',
            'data-save' => '',
            'data-modal' => "#emc-list-{$parentId}-modal",
            'data-container' => "#emc-list-{$parentId}",
        ]) ?>

    </div>
</div>

<?php Pjax::begin([
    'id' => "emc-list-{$parentId}",
    'linkSelector' => "#emc-list-{$parentId} a[data-sort], #emc-list-{$parentId} a[data-page]",
    'enablePushState' => false,
    'clientOptions' => [
        'method' => 'GET'
    ]
]); ?>

<?= GridView::widget([
    'options' => [
        'id' => "emc-list-{$parentId}-grid"
    ],
    'dataProvider' => new ActiveDataProvider([
        'query' => \backend\modules\handbookemc\models\EmcList::find()->where([
            'parent_id' => $parentId
        ])->orderBy('name'),
        'pagination' => [
            'pageSize' => 5,
        ],
    ]),
    'columns' => [
        [
            'attribute' => 'name',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('app', 'Actions'),
            'headerOptions' => ['width' => '30'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $data, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        Url::toRoute([
                            '/handbookemc/emc-list/delete',
                            'id' => $data->id
                        ]),
                        [
                            'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'title' => Yii::t('app', 'Delete'),
                            'data-container' => "#emc-list-{$data->parent_id}",
                        ]
                    );
                }
            ]
        ],
    ],
]); ?>

<?php Pjax::end(); ?>

<?php Modal::end(); ?>