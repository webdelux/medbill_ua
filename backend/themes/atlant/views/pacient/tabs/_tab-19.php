<?php

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\Pjax;

use yii\data\ActiveDataProvider;

use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\select2\Select2;

use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\diagnosis\models\DiagnosisSearch;
?>

<div class="row">
    <div class="col-md-12">

        <fieldset class="form-group">
            <legend class="form-heading">
                <a href="#" title="<?php echo Yii::t('app', 'Show/Hide'); ?>">
                    <?php echo Yii::t('app', 'Diagnoses'); ?>
                </a>
            </legend>
            <div class="row form-body">
                <div class="col-md-12">

                    <div class="form-horizontal">
                        <?= Html::a(Yii::t('app', 'Add'), ['/pacient/view', 'id' => $model->id, 'tab' => 13, 'diagnosis_create' => true], [
                            'class' => 'btn btn-primary btn-sm btn-block',
                        ]) ?>
                    </div>

                    <?php Pjax::begin([
                        'id' => 'pacient-diagnosis',
                        'linkSelector' => '#pacient-diagnosis a[data-sort], #pacient-diagnosis a[data-page]',
                        'enablePushState' => false,
                        'clientOptions' => [
                            'method' => 'GET'
                        ]
                    ]); ?>

                    <?php
                        $searchModel = new DiagnosisSearch();
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        $dataProvider->query->andWhere("pacient_id = {$model->id}")->main();
                    ?>

                    <?= GridView::widget([
                        'export' => false,
                        'options' => [
                            'id' => 'pacient-diagnosis-grid'
                        ],
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'headerOptions' => ['width' => '80'],
                            ],
                            [
                                'attribute' => 'date_at',
                                'headerOptions' => ['width' => '120'],
                                'format' => ['date', 'dd MMMM Y'],
                                'value' => function ($data) {
                                    return ($data->date_at != '0000-00-00') ? $data->date_at : null;
                                },
                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'date_at',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]),
                            ],
                            [
                                'attribute' => 'handbook_emc_icd_id',
                                'value' => function ($data) {
                                    return (isset($data->icd->name)) ? $data->icd->code . ' ' . $data->icd->name
                                            .' ('. $data->icd->parentName . ')' : null;
                                },
                                'filter' => Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'handbook_emc_icd_id',
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'placeholder' => '',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 3,
                                        'ajax' => [
                                            'url' => Url::to(['/handbookemc/icd/index']),
                                            'dataType' => 'json',
                                        ]
                                    ],
                                ])
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'header' => Yii::t('app', 'Actions'),
                                'headerOptions' => ['width' => '50'],
                                'template' => '{view} {delete}',
                                'buttons' => [
                                    'view' => function ($url, $data, $key) {
                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                            Url::toRoute([
                                                '/pacient/view',
                                                'id' => $data->pacient_id,
                                                'tab' => 13,
                                                'diagnosis_id' => $data->id,
                                            ]),
                                            [
                                                'title' => Yii::t('app', 'View'),
                                            ]
                                        );
                                    },
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    if ($action === 'delete')
                                        return Url::toRoute(['/diagnosis/diagnosis/delete', 'id' => $model->id]);
                                }
                            ],
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>

                </div>
            </div>
        </fieldset>

    </div>
</div>


<?php $this->registerCss("

    // ...

"); ?>


<?php $this->registerJs("

    $('body').on('beforeSubmit', 'form#pacient-form-tab-19', function (event) {
        $.pjax.reload({
            url: '" . Url::current() . "&' + $('#pacient-diagnosis-grid-filters input, #pacient-diagnosis-grid-filters select').serialize(),
            container: '#pacient-diagnosis',
            method: 'GET',
            push: false,
            replace: false,
            timeout: 1000,
            scrollTo: false
        });
        return false;
    });

"); ?>