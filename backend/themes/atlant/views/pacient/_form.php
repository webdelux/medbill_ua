<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use backend\modules\handbook\models\Status;
use yii\web\JsExpression;
use common\widgets\Alert;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Pacient */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="pacient-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['name']) ? Yii::$app->getRequest()->get('Pacient')['name'] : $model->name])
    ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['surname']) ? Yii::$app->getRequest()->get('Pacient')['surname'] : $model->surname])
    ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['lastname']) ? Yii::$app->getRequest()->get('Pacient')['lastname'] : $model->lastname])
    ?>

    <?= $form->field($model, 'birthday', ['inputOptions' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'day-month-year'),]])->textInput([
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['birthday']) ? Yii::$app->getRequest()->get('Pacient')['birthday'] : $model->birthday])
//        ->hint(Yii::t('app', 'Year-month-day'), ['class' => 'help-block col-sm-6 col-sm-offset-3'])
    ?>

    <?= $form->field($model, 'ipn')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['ipn']) ? Yii::$app->getRequest()->get('Pacient')['ipn'] : $model->ipn])
    ?>

    <?= $form->field($model, 'phone',
            ['inputOptions' => [
                'class' => 'mask_phone form-control',
                'placeholder' => '+380670000000',
            ]])->textInput(['value' => isset(Yii::$app->getRequest()->get('Pacient')['phone']) ? Yii::$app->getRequest()->get('Pacient')['phone'] : $model->phone])
    ?>

    <?= $form->field($model, 'phone1',
            ['inputOptions' => [
                'class' => 'mask_phone form-control',
                'placeholder' => '+380670000000',
            ]])->textInput(['value' => isset(Yii::$app->getRequest()->get('Pacient')['phone1']) ? Yii::$app->getRequest()->get('Pacient')['phone1'] : $model->phone1])
    ?>

    <?= $form->field($model, 'citizenship')->widget(\yii\jui\AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control',
            'value' => (isset($model->country->title)) ? $model->country->title : null
        ],
        'clientOptions' => [
            'source' => Url::to(['/handbook/country/index']),
            'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'citizenship_id') . "').val(ui.item.id);
            }")
        ],
    ]) ?>

    <?= Html::activeHiddenInput($model, 'citizenship_id') ?>

    <?= $form->field($model, 'city_type')->dropDownList($model::itemAlias('city_type'), ['prompt' => '',
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['city_type']) ? Yii::$app->getRequest()->get('Pacient')['city_type'] : $model->city_type])
    ?>


    <?php
        $model->cityName = isset(Yii::$app->getRequest()->get('Pacient')['cityName']) ? Yii::$app->getRequest()->get('Pacient')['cityName'] : $model->cityName;
        $model->city_id = isset(Yii::$app->getRequest()->get('Pacient')['city_id']) ? Yii::$app->getRequest()->get('Pacient')['city_id'] : $model->city_id;
    ?>
    <?= $form->field($model, 'cityName')->widget(\yii\jui\AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control',
            'value' => (isset($model->city->address)) ? $model->city->address : null
        ],
        'clientOptions' => [
            'source' => Url::to(['/handbook/city/index']),
            'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'city_id') . "').val(ui.item.id);
            }")
        ],
    ]) ?>

    <?= Html::activeHiddenInput($model, 'city_id') ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['address']) ? Yii::$app->getRequest()->get('Pacient')['address'] : $model->address])
    ?>

    <?= $form->field($model, 'gender')->dropDownList($model::itemAlias('gender'), ['prompt' => '',
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['address']) ? Yii::$app->getRequest()->get('Pacient')['address'] : $model->gender])
    ?>

    <?= $form->field($model, 'work')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['work']) ? Yii::$app->getRequest()->get('Pacient')['work'] : $model->work])
    ?>

    <?= $form->field($model, 'seat')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['seat']) ? Yii::$app->getRequest()->get('Pacient')['seat'] : $model->seat])
    ?>


    <?php
        $model->dispensary_group = isset(Yii::$app->getRequest()->get('Pacient')['dispensary_group']) ? Yii::$app->getRequest()->get('Pacient')['dispensary_group'] : $model->dispensary_group;
    ?>
    <?= $form->field($model, 'dispensary_group')->dropDownList($model::itemAlias('yn'), ['prompt' => '',
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['dispensary_group']) ? Yii::$app->getRequest()->get('Pacient')['dispensary_group'] : $model->dispensary_group])
    ?>


    <?php
        $model->contingent = isset(Yii::$app->getRequest()->get('Pacient')['contingent']) ? Yii::$app->getRequest()->get('Pacient')['contingent'] : $model->contingent;
    ?>
    <?= $form->field($model, 'contingent')->dropDownList($model::itemAlias('contingent'), ['prompt' => '',
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['contingent']) ? Yii::$app->getRequest()->get('Pacient')['contingent'] : $model->contingent])
    ?>


    <?php
        $model->docum_type = isset(Yii::$app->getRequest()->get('Pacient')['docum_type']) ? Yii::$app->getRequest()->get('Pacient')['docum_type'] : $model->docum_type;
    ?>
    <?= $form->field($model, 'docum_type')->dropDownList($model::itemAlias('docum'), ['prompt' => ''])?>


    <?= $form->field($model, 'docum_number')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['docum_number']) ? Yii::$app->getRequest()->get('Pacient')['docum_number'] : $model->docum_number])
    ?>

    <?= $form->field($model, 'memo')->textarea(['rows' => 6,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['memo']) ? Yii::$app->getRequest()->get('Pacient')['memo'] : $model->memo])
    ?>

    <?= $form->field($model, 'card_amb')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['card_amb']) ? Yii::$app->getRequest()->get('Pacient')['card_amb'] : $model->card_amb])
    ?>

    <?= $form->field($model, 'card_stac')->textInput(['maxlength' => true,
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['card_stac']) ? Yii::$app->getRequest()->get('Pacient')['card_stac'] : $model->card_stac])
    ?>

    <?php
        $model->status_id = isset(Yii::$app->getRequest()->get('Pacient')['status_id']) ? Yii::$app->getRequest()->get('Pacient')['status_id'] : $model->status_id;
    ?>
    <?= $form->field($model, 'status_id')->dropDownList(Status::childList(), ['prompt' => '',
        'value' => isset(Yii::$app->getRequest()->get('Pacient')['status_id']) ? Yii::$app->getRequest()->get('Pacient')['status_id'] : $model->status_id])
    ?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php $this->registerJs("

    $(document).on('change', '#" . Html::getInputId($model, 'cityName') . "', function(event) {
        if (!$(this).val()) {
            $('#" . Html::getInputId($model, 'city_id') . "').val('');
        }
    });

"); ?>


<?php $this->registerCss("

    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }
    .ui-autocomplete::after, .ui-autocomplete::before {
        border: none;
    }

"); ?>