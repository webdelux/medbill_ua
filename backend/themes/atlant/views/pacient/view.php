<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model backend\models\Pacient */

$this->title = $model->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo Alert::widget(); ?>

<div class="pacient-view">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'pacient-form-tab-' . $tab,
            'enctype' => 'multipart/form-data'
        ],
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ]); ?>

    <div class="panel panel-default tabs">

        <?php echo $this->render('_tabs', ['model' => $model, 'tab' => $tab]); ?>

        <div class="panel-body tab-content tab-content-<?php echo $tab; ?>">
            <div class="tab-pane active">

                <?php echo $this->render('tabs/_tab-' . $tab, ['model' => $model, 'models' => $models, 'form' => $form, 'tab' => $tab]); ?>

            </div>
        </div>
        <div class="panel-footer">

            <?php if ($tab == 1): ?>

                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>

            <?php else: ?>

                <?php if (!in_array($tab, [6, 8, 14, 15, 16, 18, 19, 20])): ?>
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['id' => 'pacient-btn-submit-' . $tab, 'class' => 'btn btn-primary']) ?>
                <?php endif; ?>

                <?php if (in_array($tab, [13])): ?>
                    <?= Html::a(Yii::t('app', 'Close'), ['view', 'id' => $model->id, 'tab' => 19], ['class' => 'btn btn-default']) ?>
                <?php endif; ?>

            <?php endif; ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if (in_array($tab, [13])): ?>
        <?php echo $this->render('//modules/handbookemc/views/icd/_selector'); ?>
    <?php endif; ?>

</div>


<?php $this->registerCss("

    legend {
        font-size: 15px;
        font-style: italic;
        margin-bottom: 10px;
    }
    fieldset {
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .tab-content .table {
        margin-bottom: 0;
    }
    .tab-content .well {
        margin-bottom: 5px;
    }
    .tab-content .pagination {
        margin: 5px 0;
    }
    .tab-content .form-horizontal .btn-block {
        margin-bottom: 5px;
    }

    .modal .modal-content .modal-body .modal-row {
        margin-bottom: 10px;
    }

    .compacted.control-label {
        line-height: normal;
    }

"); ?>


<?php $this->registerJsFile('@web/themes/atlant/js/plugins/chained/jquery.chained.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJs("

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

    $(document).on('click', 'a[data-delete]', function(event) {
        event.preventDefault();
        if (confirm($(this).attr('data-delete')))
        {
            var container = $(this).attr('data-container');

            $.ajax({
                method: 'POST',
                url: $(this).attr('href')
            })
            .done(function(data) {
                $.pjax.reload({
                    container: container
                });
            });
        }
    })

    $(document).on('click', 'button[data-save]', function(event) {

        var form = $(this).closest('form');
        var modal = $(this).attr('data-modal') + ' .modal-body';
        var container = $(this).attr('data-container');

        $.ajax({
            method: 'POST',
            url: '" . Url::to() . "',
            data: form.serialize(),
            dataType: 'html'
        })
        .done(function(data) {
            $(modal).html($(data).find(modal).html());
            $(modal).find('input').prop('disabled', false);

            if (!$('.datetime-picker').length){
                if ($('.input-group.date').length)
                {
                    $('.input-group.date').kvDatepicker({
                        'autoclose': true,
                        'format': 'yyyy-mm-dd',
                        'language': '" . substr(Yii::$app->language, 0, 2) . "'
                    });
                }
            }

            $.pjax.reload({
                container: container
            });
        });
    })

    // Показати/Заховати
    $(document).on('click', '.form-group > .form-heading a', function(event) {
        $(this).parent().parent().find('.form-body').toggle();
        event.preventDefault();
    })

"); ?>