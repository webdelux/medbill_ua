<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Pacient */

$this->title = Yii::t('app', 'Create Pacient');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pacient-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>