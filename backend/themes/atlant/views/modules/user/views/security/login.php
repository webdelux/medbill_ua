<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\captcha\Captcha;
use yii\web\Session;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="login-container" style="min-height: 99vh">

            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong><?=Yii::t('user', 'Welcome, ')?></strong><?=Yii::t('user', 'Please login')?></div>

                     <?php $form = ActiveForm::begin([
                    'id'                     => 'login-form',
                    'enableAjaxValidation'   => false,
                    'enableClientValidation' => false,
                    'validateOnBlur'         => false,
                    'validateOnType'         => false,
                    'validateOnChange'       => false,
                ]) ?>

                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput();?>
                        </div>
                    </div>
                    <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>
                    <div class="form-group">
                        <div class="col-md-6">
                            <?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn-info btn-block', 'tabindex' => '3']) ?>
                        </div>
                    </div>

                    <?php //Yii::$app->session->get('mycount') ?>
                    <?php if (Yii::$app->session->get('mycount') > Yii::$app->session->get('mycount_try'))
                        echo ($form->field($model, 'captcha')->widget(Captcha::className(), ['captchaAction' => ['/site/captcha']]));
                    ?>

                    <?php ActiveForm::end(); ?>

                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; <?= Yii::$app->name ?> <?= date('Y') ?>
                    </div>
                </div>
            </div>


        <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?php // Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
            </p>
        <?php endif ?>
        <?php if ($module->enableRegistration): ?>
            <p class="text-center">
                <?php // Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
            </p>
        <?php endif ?>
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>
        </div>

