<?php

use yii\helpers\Url;

$this->title = Yii::t('app', 'Templates');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-2">
        <a class="tile tile-default" href="<?= Url::to(['/pattern/default/index']) ?>">
            <?= Yii::t('app', 'Templates') ?><br/>
            <span class="fa fa-list"></span>
        </a>
    </div>
</div>