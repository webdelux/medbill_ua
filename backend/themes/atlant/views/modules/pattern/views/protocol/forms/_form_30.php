<?php

use kartik\date\DatePicker;
?>

<div class="form-horizontal">

    <div class="row modal-row" style="margin-bottom: 20px;">
        <div class="col-md-6">

            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-6">
                    <span class="badge">OD</span>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-6">
                    <span class="badge">OS</span>
                </div>
            </div>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_2')->textInput(['maxlength' => true])->hint('15 - 32', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_6')->textInput(['maxlength' => true])->hint('15 - 32', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_3')->textInput(['maxlength' => true])->hint('0 - 5', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_7')->textInput(['maxlength' => true])->hint('0 - 5', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_4')->textInput(['maxlength' => true])->hint('1 - 7', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_8')->textInput(['maxlength' => true])->hint('1 - 7', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_5')->textInput(['maxlength' => true])->hint('10 - 30', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_9')->textInput(['maxlength' => true])->hint('10 - 30', ['class' => 'help-block col-sm-6 col-sm-offset-6']) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_1')->widget(DatePicker::classname(), [
                'options' => [
                    'placeholder' => '0000-00-00',
                    'value' => ($model->field_1 != '0000-00-00') ? $model->field_1 : date('Y-m-d')
                ],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>

        </div>
    </div>

    <script>
        function initWidgetForm()
        {
            var kvDatepicker_493f0995 = {
                "autoclose":true,
                "format":"yyyy-mm-dd",
                "language":"<?= substr(Yii::$app->language, 0, 2) ?>"
            };

            jQuery('#protocolpattern_30-field_1-kvdate').kvDatepicker(kvDatepicker_493f0995);
        }
    </script>

</div>
