<?php

use yii\helpers\Url;
use yii\bootstrap\Modal;
use dosamigos\tinymce\TinyMceLangAsset;
use kartik\date\DatePickerAsset;

$dateTimePicker = DatePickerAsset::register($this);
$dateTimePicker->js[] = "{$dateTimePicker->baseUrl}/js/locales/bootstrap-datepicker." . substr(Yii::$app->language, 0, 2) . ".min.js";

$tinyMceAsset = TinyMceLangAsset::register($this);
?>

<?php Modal::begin([
    'id' => "modal-protocol-pattern",
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
    'toggleButton' => false,
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
    'clientOptions' => [
        'remote' => false
    ],
]); ?>

<div class="modal-body-content"></div>

<?php Modal::end(); ?>


<?php $this->registerJs("

    function initWidgetTinymce()
    {
        tinymce.init({
            selector: 'textarea.js-tinymce',
            language: '" . substr(Yii::$app->language, 0, 2) . "',
            language_url: '{$tinyMceAsset->baseUrl}/langs/" . substr(Yii::$app->language, 0, 2) . ".js',
            height: 300,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar_items_size: 'small',
            menubar: false,
            statusbar: true
        });
    }

    $('#modal-protocol-pattern').on('shown.bs.modal', function (e) {

        var button = $(e.relatedTarget);
        var modal = $(this);
        var url = '" . Url::to(['/pattern/protocol/update']) . "?id=' + button.attr('data-id') + '&protocol_id=' + button.attr('data-protocol');

        modal.find('.modal-header .modal-title').text(button.attr('data-protocol-name'));

        modal.find('.modal-body-content').html('<div style=\"text-align: center;\"><i class=\"fa fa-circle-o-notch fa-2x fa-spin\"></i></div>');
        modal.find('.modal-body-content').load(url + ' .modal-form', function(response, status, xhr) {
            if (status == 'success') {
                // В випадку якщо форма недоступна то показуємо всю відповідь
                if (!$(response).find('.modal-form').html())
                    modal.find('.modal-body-content').html(response);
                else
                {
                    initWidgetTinymce();

                    $(response).find('.modal-form').find('script').each(function(index, value) {

                        // Виконання JavaScript коду
                        eval($(this).text());

                        if (typeof initWidgetForm == 'function')
                            initWidgetForm();
                    });
                }
            }
        });

    })

    $('#modal-protocol-pattern').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.modal-body-content').html('');
    });

    $('#modal-protocol-pattern').on('hide.bs.modal', function (e) {
        tinymce.remove('textarea.js-tinymce');
    });

    $(document).on('click', '#btn-modal-protocol-pattern-save', function(event) {
        var modal = $('#modal-protocol-pattern');
        var body = $('#modal-protocol-pattern .modal-form-body');

        body.hide();

        tinymce.remove('textarea.js-tinymce');

        $.ajax({
            method: 'POST',
            url: $(this).attr('data-url'),
            data: modal.find('input, textarea, select').serialize(),
            dataType: 'html'
        })
        .done(function(data) {
            modal.find('.modal-body-content').html($(data).find('.modal-form').html());

            initWidgetTinymce();

            if (typeof initWidgetForm == 'function')
                initWidgetForm();

            body.show();
        });
    })

"); ?>


<?php $this->registerCss("

    .form-group.field-protocol-protocol_id .input-group-btn .btn {
        padding: 6px 12px;
        margin-left: 0px;
    }
    .form-group.field-protocol-protocol_id .select2-container--bootstrap .select2-selection {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    .compacted.control-label {
        line-height: normal;
    }
    .modal-dialog .alert {
        margin-bottom: 15px;
    }

"); ?>