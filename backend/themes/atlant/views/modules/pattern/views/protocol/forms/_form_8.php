<div class="form-horizontal">

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_1')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_2')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_3')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

</div>