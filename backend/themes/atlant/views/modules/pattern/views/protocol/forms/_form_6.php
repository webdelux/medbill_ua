<div class="form-horizontal">

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_1')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_2')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_3')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_4')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_5')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_6')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_7')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_8')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_9')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_10')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_11')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_12', [
                'labelOptions' => ['class' => 'compacted control-label col-sm-6'],
            ])->textInput(['maxlength' => true]) ?>

        </div>
    </div>

</div>