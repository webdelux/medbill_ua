<div class="form-horizontal">

    <div class="row modal-row">
        <div class="col-md-12">

            <?= $form->field($model, 'field_1', [
                'labelOptions' => ['class' => 'control-label col-sm-3'],
            ])->textarea(['rows' => 3, 'maxlength' => true]) ?>

        </div>
    </div>

</div>