<?php

use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\date\DatePicker;
?>

<div class="form-horizontal">

    <div class="row modal-row">
        <div class="col-md-12">

            <?= $form->field($model, 'field_1', [
                'labelOptions' => ['class' => 'control-label col-sm-3'],
                'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($model->icd) ? '' : $model->icd->name,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbookemc/icd/index']),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'field_3')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'field_4')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'field_5')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'field_6')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'field_7')->dropDownList($model::itemAlias('field_7')) ?>

                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'field_8')->dropDownList($model::itemAlias('field_8')) ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-12">

            <?= $form->field($model, 'field_9', [
                'labelOptions' => ['class' => 'control-label col-sm-3'],
                'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
            ])->textarea(['rows' => 3, 'maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'field_2')->widget(DatePicker::classname(), [
                        'options' => [
                            'placeholder' => '0000-00-00',
                            'value' => ($model->field_2 != '0000-00-00') ? $model->field_2 : date('Y-m-d')
                        ],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]) ?>

                </div>
            </div>
        </div>
    </div>

    <script>
        function initWidgetForm()
        {
            var s2options_6cc131ae = {
                "themeCss":".select2-container--bootstrap",
                "sizeCss":"",
                "doReset":true,
                "doToggle":false,
                "doOrder":false
            };

            var select2_d35d49a5 = {
                "allowClear":true,
                "minimumInputLength":3,
                "ajax":{
                    "url":"<?= Url::to(['/handbookemc/icd/index']) ?>",
                    "dataType":"json"
                },
                "theme":"bootstrap",
                "width":"100%",
                "placeholder":"",
                "language":"<?= Yii::$app->language ?>"
            };

            var kvDatepicker_493f0995 = {
                "autoclose":true,
                "format":"yyyy-mm-dd",
                "language":"<?= substr(Yii::$app->language, 0, 2) ?>"
            };

            jQuery.when(jQuery('#protocolpattern_11-field_1').select2(select2_d35d49a5)).done(initS2Loading('protocolpattern_11-field_1','s2options_6cc131ae'));

            jQuery('#protocolpattern_11-field_2-kvdate').kvDatepicker(kvDatepicker_493f0995);
        }
    </script>

</div>