<div class="form-horizontal">

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_1')->dropDownList($model::itemAlias('field_1'), ['prompt' => '']) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_2')->textarea(['rows' => 2, 'maxlength' => true]) ?>

        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= $form->field($model, 'field_3')->dropDownList($model::itemAlias('field_3'), ['prompt' => '']) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'field_4')->textarea(['rows' => 2, 'maxlength' => true]) ?>

        </div>
    </div>

</div>
