<div class="form-horizontal">

    <div class="row modal-row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'field_1')->dropDownList($model::itemAlias('field_1')) ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-12">

            <?= $form->field($model, 'field_2', [
                'labelOptions' => ['class' => 'control-label col-sm-3'],
                'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>',
            ])->textarea(['rows' => 3, 'maxlength' => true]) ?>

        </div>
    </div>

</div>