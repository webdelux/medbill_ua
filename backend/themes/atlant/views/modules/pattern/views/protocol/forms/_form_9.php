<?php

use backend\modules\pattern\models\ProtocolPattern;
?>

<div class="form-horizontal">

    <div class="row modal-row">
        <div class="col-md-12">

            <?php if (empty($model->field_1))
                $model->field_1 = (isset($substitutional)) ? $substitutional : ProtocolPattern::substitution($model->protocol_id); ?>

            <?= $form->field($model, 'field_1', [
                'labelOptions' => ['class' => 'control-label col-sm-12', 'style' => 'text-align: left;'],
                'inputOptions' => ['class' => 'form-control js-tinymce'],
                'template' => '{label} <div class="col-sm-12">{input}{error}{hint}</div>',
            ])->textarea(['rows' => 5]) ?>

        </div>
    </div>

</div>