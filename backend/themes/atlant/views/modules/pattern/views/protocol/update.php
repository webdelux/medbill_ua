<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-6',
            'wrapper' => 'col-sm-6',
        ],
    ],
]); ?>

<div class="modal-form">
    <div class="modal-form-body">

        <?php echo Alert::widget(); ?>

        <?= $this->render('forms/_form_' . $pattern, [
            'id' => $id,
            'pattern' => $pattern,
            'form' => $form,
            'model' => $model
        ]) ?>

    </div>
    <div class="modal-form-footer">
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-6">

                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'id' => 'btn-modal-protocol-pattern-save',
                            'data-url' => Url::current()
                        ]) ?>

                        <?= (isset($model->protocol->protocol->template->substitutional))
                            ? Html::a(Yii::t('app', 'Print'), Url::current(['print' => true]), [
                                'class' => 'btn btn-default',
                                'target' => '_blank'
                            ])
                        : null ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>