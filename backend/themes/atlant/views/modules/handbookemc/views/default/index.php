<?php

use yii\helpers\Url;

$this->title = Yii::t('app', 'Handbooks "Electronic medical card"');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-2">
        <a class="tile tile-default" href="<?= Url::to(['/handbookemc/icd/index']) ?>">
            <?= Yii::t('app', 'ICD-10') ?><br/>
            <span class="fa fa-list"></span>
        </a>
    </div>
</div>