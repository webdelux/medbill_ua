<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\widgets\Alert;
use backend\modules\handbookemc\models\Template;
use backend\modules\handbookemc\models\Category;
use backend\modules\handbook\models\Status;
use dosamigos\tinymce\TinyMce;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="template-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'handbook_emc_category_id')->dropDownList((new Category)->treeList((new Category)->treeData()), ['prompt' => '']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'substitutional')->widget(TinyMce::className(), [
        'options' => ['rows' => 25],
        'language' => substr(Yii::$app->language, 0, 2),
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste code"
            ],
            'toolbar' => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            'statusbar' => false
        ]
    ]); ?>

    <div class="form-group template-markers">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="well well-sm">

                <?= Yii::t('app', 'Markers') ?>:

                <?php
                $dataProvider = new ArrayDataProvider([
                    'allModels' => [
                        [
                            'name' => Yii::t('app', 'Number survey'),
                            'marker' => '{id}',
                            'description' => Yii::t('app', 'Serial number of the survey')
                        ],
                        [
                            'name' => Yii::t('app', 'Pacient'),
                            'marker' => '{patient}',
                            'description' => Yii::t('app', 'Full Name') . ' ' . Yii::t('app', 'pacient')
                        ],
                        [
                            'name' => Yii::t('app', 'Birthday'),
                            'marker' => '{date-of-birth-of-the-patient}',
                            'description' => Yii::t('app', 'Birthday') . ' ' . Yii::t('app', 'pacient')
                        ],
                        [
                            'name' => Yii::t('app', 'Date of survey'),
                            'marker' => '{date-of-examination-of-patient}',
                            'description' => Yii::t('app', 'Date of survey') . ' ' . Yii::t('app', 'pacient')
                        ],
                        [
                            'name' => Yii::t('app', 'Doctor'),
                            'marker' => '{doctor}',
                            'description' => Yii::t('app', 'Full Name') . ' ' . Yii::t('app', 'doctor')
                        ],
                    ],
                    'sort' => false,
                    'pagination' => false
                ]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}",
                    'columns' => [
                        [
                            'label' => Yii::t('app', 'Name'),
                            'attribute' => 'name',
                            'headerOptions' => ['width' => '200'],
                        ],
                        [
                            'label' => Yii::t('app', 'Marker'),
                            'attribute' => 'marker',
                            'headerOptions' => ['width' => '200'],
                        ],
                        [
                            'label' => Yii::t('app', 'Description'),
                            'attribute' => 'description',
                        ],
                    ],
                ]) ?>

            </div>
        </div>
    </div>

    <?php //echo $form->field($model, 'pattern')->dropDownList(Template::itemAlias('pattern'), ['prompt' => '']); ?>

    <?= $form->field($model, 'status_id')->dropDownList(Status::childList(), ['prompt' => '']) ?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php $this->registerCss("

    .form-horizontal > .field-template-substitutional {
        margin-bottom: 0;
    }

    .template-markers .well,
    .template-markers .table,
    .template-markers .pagination{
        margin-bottom: 0;
    }

"); ?>