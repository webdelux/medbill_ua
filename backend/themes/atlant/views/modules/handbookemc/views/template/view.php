<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMceLangAsset;
use kartik\date\DatePickerAsset;
use backend\modules\pattern\models\ProtocolPattern;

$dateTimePicker = DatePickerAsset::register($this);
$dateTimePicker->js[] = "{$dateTimePicker->baseUrl}/js/locales/bootstrap-datepicker." . substr(Yii::$app->language, 0, 2) . ".min.js";

$tinyMceAsset = TinyMceLangAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\Template */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="template-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'handbook_emc_category_id',
                'value' => (isset($model->category->name)) ? $model->category->name : null
            ],
            'name',
            'description:ntext',
            //'substitutional:ntext',
            [
                'attribute' => 'status_id',
                'value' => (isset($model->status->name)) ? $model->status->name : null
            ],
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <hr/>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <?= Yii::t('app', 'Form pattern') ?>
            </h3>
        </div>
        <div class="panel-body">

            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableAjaxValidation' => false,
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-6',
                        'wrapper' => 'col-sm-6',
                    ],
                ],
            ]); ?>

            <?php $pattern = ProtocolPattern::choiceTemplateId($model->id, $model->handbook_emc_category_id); ?>

            <?php $protocolPattern = '\backend\modules\pattern\models\ProtocolPattern_' . $pattern; ?>

            <?= $this->render('//modules/pattern/views/protocol/forms/_form_' . $pattern, [
                'pattern' => $pattern,
                'form' => $form,
                'model' => new $protocolPattern,
                'substitutional' => $model->substitutional
            ]) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>


<?php $this->registerJs("

    tinymce.init({
        selector: 'textarea.js-tinymce',
        language: '" . substr(Yii::$app->language, 0, 2) . "',
        language_url: '{$tinyMceAsset->baseUrl}/langs/" . substr(Yii::$app->language, 0, 2) . ".js',
        height: 300,
        toolbar: false,
        menubar: false,
        statusbar: false
    });

"); ?>
