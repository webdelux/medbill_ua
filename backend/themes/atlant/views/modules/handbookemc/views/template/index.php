<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\handbookemc\models\Template;
use backend\modules\handbookemc\models\Category;
use backend\modules\handbook\models\Status;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbookemc\models\TemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Templates');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="template-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],

            [
                'attribute' => 'handbook_emc_category_id',
                'value' => function ($data) {
                    return (isset($data->category->name)) ? $data->category->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => (new Category)->treeList((new Category)->treeData()),
            ],
            'name',
            /*
            [
                'attribute' => 'pattern',
                'value' => function ($data) {
                    return (is_numeric($data->pattern)) ? Template::itemAlias('pattern', $data->pattern) : null;
                },
                'filter' => Template::itemAlias('pattern'),
            ],
            */
            [
                'attribute' => 'status_id',
                'value' => function ($data) {
                    return (isset($data->status->name)) ? $data->status->name : null;
                },
                'headerOptions' => ['width' => '150'],
                'filter' => Status::childList(),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>

</div>