<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use backend\modules\handbookemc\models\Icd;
use backend\modules\handbookemc\models\IcdSearch;

$searchModel = new IcdSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $parent = false);
$dataProvider->pagination->pageSize = 5;
?>

<?php Modal::begin([
    'id' => "selector-handbook-emc-icd-modal",
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Icds') . '</h4>',
]); ?>

<?php Pjax::begin([
    'id' => "selector-handbook-emc-icd-pjax",
    'linkSelector' => "#selector-handbook-emc-icd-pjax a[data-sort], #selector-handbook-emc-icd-pjax a[data-page]",
    'enablePushState' => false,
    'clientOptions' => [
        'method' => 'GET'
    ]
]); ?>

<?= GridView::widget([
    'options' => [
        'id' => "selector-handbook-emc-icd-grid"
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'code',
            'headerOptions' => ['width' => '100'],
        ],
        [
            'attribute' => 'parent_id',
            'value' => function ($data) {
                return (isset($data->parent->name)) ? $data->parent->name : null;
            },
            'headerOptions' => ['width' => '250'],
            'filter' => ArrayHelper::map(Icd::find()->where(['parent_id' => NULL])->orderBy('id')->all(), 'id', 'fullName'),
        ],
        [
            'attribute' => 'name',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('app', 'Actions'),
            'headerOptions' => ['width' => '70'],
            'template' => '{select}',
            'buttons' => [
                'select' => function ($url, $data, $key) {
                    return Html::a('<i class="fa fa-check"></i>', '#',
                        [
                            'class' => 'btn btn-default btn-select',
                            'title' => Yii::t('app', 'Select'),
                            'data-id' => $data->id,
                            'data-name' => $data->name,
                        ]
                    );
                }
            ]
        ],
    ],
]); ?>

<?php Pjax::end(); ?>

<?php Modal::end(); ?>


<?php $this->registerCss("

    .select2-container--bootstrap .select2-dropdown {
        border-color: #aaa;
        box-shadow: none;
    }
    .select2-container--bootstrap.select2-container--focus .select2-selection,
    .select2-container--bootstrap.select2-container--open .select2-selection {
        border-color: #aaa;
        box-shadow: none;
    }
    .select2-container--bootstrap .select2-selection {
        border-radius: 4px 0 0 4px;
    }
    .form-group .input-group-btn .btn {
        line-height: 24px;
    }

"); ?>

<?php $this->registerJs("

    $(document).on('click', '#selector-handbook-emc-icd-grid a.btn-select', function(event) {
        event.preventDefault();

        var modal = $('#selector-handbook-emc-icd-modal');

        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');

        var selector = $('#' + modal.attr('data-parent'));

        selector.empty().append($('<option></option>').attr('value', id).text(name));
        selector.val(id).trigger('change');

        modal.modal('hide');
    })

    $('#selector-handbook-emc-icd-modal').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var modal = $(this);

        modal.attr('data-parent', button.attr('data-parent'));
    });

"); ?>