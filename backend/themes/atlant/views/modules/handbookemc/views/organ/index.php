<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\handbookemc\models\Organ;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbookemc\models\OrganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Organs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="organ-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Complaints'), ['/handbookemc/organ-complaints/index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            [
                'attribute' => 'parent_id',
                'value' => function ($data) {
                    return (isset($data->parent->name)) ? $data->parent->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => ArrayHelper::map(Organ::find()->where(['parent_id' => NULL])->orderBy('id')->all(), 'id', 'name'),
            ],
            'name',
            'description',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>

</div>