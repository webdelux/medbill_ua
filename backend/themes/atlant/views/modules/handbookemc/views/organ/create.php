<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\Organ */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Organs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="organ-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>