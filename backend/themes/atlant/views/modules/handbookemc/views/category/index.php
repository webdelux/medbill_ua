<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\handbookemc\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbookemc\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['#']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            [
                'attribute' => 'code',
            ],
            [
                'attribute' => 'parent_id',
                'value' => function ($data) {
                    return (isset($data->parent->name)) ? $data->parent->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => (new Category)->listData(false),
            ],
            [
                'attribute' => 'name',
                'headerOptions' => ['width' => '300'],
            ],
            [
                'attribute' => 'description',
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    return Category::itemAlias('status', $data->status);
                },
                'headerOptions' => ['width' => '150'],
                'filter' => Category::itemAlias('status'),
            ],
            //'user_id',
            //'updated_user_id',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>

</div>