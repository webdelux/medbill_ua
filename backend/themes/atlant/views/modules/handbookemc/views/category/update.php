<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\Category */

$this->title = Yii::t('app', 'ID') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>