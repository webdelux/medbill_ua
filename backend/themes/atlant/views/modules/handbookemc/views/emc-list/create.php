<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\EmcList */

$this->title = Yii::t('app', 'Create Emc List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Emc Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emc-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
