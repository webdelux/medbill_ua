<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\handbookemc\models\Category;
use backend\modules\handbookemc\models\Template;
use backend\modules\handbook\models\Status;
use backend\modules\handbookemc\models\Protocol;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbookemc\models\ProtocolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Protocols');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="protocol-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            'code',
            [
                'attribute' => 'handbook_emc_category_id',
                'value' => function ($data) {
                    return (isset($data->category->name)) ? $data->category->name .' ('. $data->category->parentsName($data->category->id, 3) . ')': null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => (new Category)->listData(false),
            ],
            [
                'attribute' => 'handbook_emc_template_id',
                'value' => function ($data) {
                    return (isset($data->template->name)) ? $data->template->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => ArrayHelper::map(Template::find()->orderBy('id')->all(), 'id', 'name'),
            ],
            'name',
            'abbreviation',
            'amount',
            'execution',
            [
                'attribute' => 'required',
                'value' => function ($data) {
                    return Protocol::itemAlias('required', $data->required);
                },
                'headerOptions' => ['width' => '150'],
                'filter' => Protocol::itemAlias('required'),
            ],
            [
                'attribute' => 'status_id',
                'value' => function ($data) {
                    return (isset($data->status->name)) ? $data->status->name : null;
                },
                'headerOptions' => ['width' => '150'],
                'filter' => Status::childList(),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>

</div>