<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\Protocol */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Protocols'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="protocol-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'handbook_emc_category_id',
                'value' => (isset($model->category->name)) ? $model->category->name : null
            ],
            [
                'attribute' => 'handbook_emc_template_id',
                'value' => (isset($model->template->name)) ? $model->template->name : null
            ],
            'name',
            'code',
            'abbreviation',
            'description:ntext',
            'amount',
            'execution',
            [
                'attribute' => 'required',
                'value' => $model::itemAlias('required', $model->required)
            ],
            [
                'attribute' => 'status_id',
                'value' => (isset($model->status->name)) ? $model->status->name : null
            ],
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>