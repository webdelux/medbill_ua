<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\widgets\Alert;
use backend\modules\handbookemc\models\Protocol;
use backend\modules\handbookemc\models\Category;
use backend\modules\handbookemc\models\Template;
use backend\modules\handbook\models\Status;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\Protocol */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="protocol-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'handbook_emc_category_id')->dropDownList((new Category)->listData(false), ['prompt' => '']) ?>

    <?= $form->field($model, 'handbook_emc_template_id')->dropDownList(ArrayHelper::map(
            Template::find()->orderBy('id')->all(), 'id', 'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'abbreviation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'execution')->textInput() ?>

    <?= $form->field($model, 'required')->dropDownList(Protocol::itemAlias('required')) ?>

    <?= $form->field($model, 'status_id')->dropDownList(Status::childList(), ['prompt' => '']) ?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>