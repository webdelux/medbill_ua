<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\SurgeryCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Surgeries'), 'url' => ['/handbookemc/surgery/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="surgery-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>