<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\handbookemc\models\SurgeryCategory;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbookemc\models\SurgeryCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Surgeries'), 'url' => ['/handbookemc/surgery/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="surgery-category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            [
                'attribute' => 'parent_id',
                'value' => function ($data) {
                    return (isset($data->parent->name)) ? $data->parent->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => (new SurgeryCategory)->treeList((new SurgeryCategory)->treeData()),
            ],
            [
                'attribute' => 'code',
                'headerOptions' => ['width' => '75'],
            ],
            'name',
            'description',
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    return SurgeryCategory::itemAlias('status', $data->status);
                },
                'headerOptions' => ['width' => '150'],
                'filter' => SurgeryCategory::itemAlias('status'),
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>