<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\widgets\Alert;
use backend\modules\handbookemc\models\SurgeryCategory;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\SurgeryCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="surgery-category-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'parent_id')->dropDownList((new SurgeryCategory)->treeList((new SurgeryCategory)->treeData()), ['prompt' => '']) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(SurgeryCategory::itemAlias('status')) ?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>