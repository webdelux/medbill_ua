<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\handbookemc\models\Surgery;
use backend\modules\handbookemc\models\SurgeryCategory;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbookemc\models\SurgerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Surgeries');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="surgery-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Categories'), ['/handbookemc/surgery-category/index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            /*
            [
                'attribute' => 'parent_id',
                'value' => function ($data) {
                    return (isset($data->parent->name)) ? $data->parent->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => ArrayHelper::map(Surgery::find()->where(['parent_id' => NULL])->orderBy('id')->all(), 'id', 'fullName'),
            ],
            */
            [
                'attribute' => 'handbook_emc_surgery_category_id',
                'value' => function ($data) {
                    return (isset($data->category->name)) ? $data->category->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => (new SurgeryCategory)->treeList((new SurgeryCategory)->treeData()),
            ],
            [
                'attribute' => 'code',
                'headerOptions' => ['width' => '100'],
            ],
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>

</div>