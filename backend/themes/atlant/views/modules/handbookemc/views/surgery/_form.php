<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\widgets\Alert;
use backend\modules\handbookemc\models\Surgery;
use backend\modules\handbookemc\models\SurgeryCategory;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\Surgery */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="surgery-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(
            Surgery::find()->where(['parent_id' => NULL])->orderBy('id')->all(), 'id', 'fullName'), ['prompt' => '']) */ ?>

    <?= $form->field($model, 'handbook_emc_surgery_category_id')
            ->dropDownList((new SurgeryCategory)->treeList((new SurgeryCategory)->treeData()), ['prompt' => '']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>