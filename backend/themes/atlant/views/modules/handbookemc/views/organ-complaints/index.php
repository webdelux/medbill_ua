<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\handbookemc\models\Organ;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbookemc\models\OrganComplaintsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Complaints');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Organs'), 'url' => ['/handbookemc/organ/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="organ-complaints-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            [
                'attribute' => 'handbook_emc_organ_id',
                'value' => function ($data) {
                    return (isset($data->organ->name)) ? $data->organ->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => ArrayHelper::map(Organ::find()->where(['parent_id' => NULL])->orderBy('id')->all(), 'id', 'name'),
            ],
            'name',
            //'user_id',
            //'updated_user_id',
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>

</div>