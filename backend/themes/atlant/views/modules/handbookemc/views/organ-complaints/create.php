<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\OrganComplaints */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Organs'), 'url' => ['/handbookemc/organ/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Complaints'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="organ-complaints-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>