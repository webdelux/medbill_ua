<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbookemc\models\OrganComplaints */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handbooks'), 'url' => ['/handbookemc/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Organs'), 'url' => ['/handbookemc/organ/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Complaints'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="organ-complaints-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'handbook_emc_organ_id',
                'value' => (isset($model->organ->name)) ? $model->organ->name : null
            ],
            'name',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>