<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Create');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="status-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>