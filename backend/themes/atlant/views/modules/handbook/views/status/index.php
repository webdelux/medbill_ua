<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use execut\widget\TreeView;

$this->title = Yii::t('app', 'Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="status-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php /* echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'parent_id',
            'sort',
            'name',
            'description',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */ ?>

    <?php echo TreeView::widget([
        'data' => $tree,
        'size' => TreeView::SIZE_NORMAL,
        'clientOptions' => [
            'enableLinks' => true,
        ],
    ]); ?>

</div>


<?php $this->registerCss("

    .treeview .list-group .list-group-item:last-child {
        border-bottom: 1px solid #e5e5e5;
    }

"); ?>