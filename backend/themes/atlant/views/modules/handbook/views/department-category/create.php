<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\DepartmentCategory */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments category'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-category-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
