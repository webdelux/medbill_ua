<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\handbook\models\DepartmentCategory;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\DepartmentCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Departments category');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
   <?=
    GridView::widget([
        'options' => [
            'id' => 'stacionary-grid'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
            ],
            [
                'attribute' => 'parent_id',
                'value' => function ($data) {
                    return (isset($data->parent->name)) ? $data->parent->name : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => (new DepartmentCategory)->treeList((new DepartmentCategory)->treeData()),
            ],
            //'sort',
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'description',
            ],
            'user_id',
            'updated_user_id',
            'created_at',
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
