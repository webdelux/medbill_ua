<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Workplace */

$this->title = Yii::t('app', 'Add') . ' ' . Yii::t('app', 'Workplace');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refferal'), 'url' => ['refferal/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workplace'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="workplace-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
