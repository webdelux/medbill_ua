<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\handbook\models\Status;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\WorkplaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Workplace');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refferal'), 'url' => ['refferal/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="workplace-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'). ' ' . Yii::t('app', 'Workplace'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            'name',
            'description',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'userName',
                'value' => function ($data)
                {
                    return (isset ($data->username)) ? $data->username : '';
                }
            ],
            [
                'label' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUser',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    return (isset ($data->updatedUserName)) ? $data->updatedUserName : '';
                }
            ],
            // 'created_at',
            // 'updated_at',
            [
                'attribute' => 'status_id',
                'filter' => Status::childList(),
                'value' => function ($data)
                {
                    return (isset ($data->status->name)) ? $data->status->name : '';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view}{update}{delete}{link}',
            ],
        ],
    ]); ?>

</div>
