<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Workplace */

$this->title = Yii::t('app', 'Update') . ' ' .Yii::t('app', 'Workplace');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refferal'), 'url' => ['refferal/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workplace'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="workplace-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
