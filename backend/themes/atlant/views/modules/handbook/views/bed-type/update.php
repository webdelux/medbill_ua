<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\BedType */

$this->title = Yii::t('app', 'Update') . ' ' .Yii::t('app', 'Bed type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bed type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bed_type, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bed-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
