<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\BedType */

$this->title = Yii::t('app', 'Add') . ' ' . Yii::t('app', 'Bed type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bed Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bed-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
