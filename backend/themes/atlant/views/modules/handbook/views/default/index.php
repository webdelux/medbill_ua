<?php

use yii\helpers\Url;

$this->title = Yii::t('app', 'Handbooks');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-2">
        <a class="tile tile-default" href="<?= Url::to(['/handbook/department/index']) ?>">
            <?= Yii::t('app', 'Departments') ?>
            <span class="fa fa-hospital-o"></span>
        </a>
    </div>
</div>