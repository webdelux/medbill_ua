<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Region */

$this->title = Yii::t('app', 'Create');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="region-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>