<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\ArrayHelper;
use backend\modules\handbook\models\Country;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Region */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="region-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'country_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList(ArrayHelper::map(Country::find()->all(), 'id', 'title'), ['prompt' => '']) ?>

    <?= $form->field($model, 'title', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>