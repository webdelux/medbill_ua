<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\InsuranceHandbookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Handbook') . ' ' . Yii::t('app', 'Insurance companies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurance-handbook-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add') . ' ' . Yii::t('app', 'Insurance company'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'options' => [
            'id' => 'insurance-handbook-grid'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            'name',
            'address',
            'contacts',
            //'www',
            'edrpou',
            // 'description',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'user',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    $res = (isset($data->userProfile->name)) ? $data->userProfile->name : '';
                    $res .= ' (';
                    $res .= (isset($data->user->username)) ? $data->user->username : '';
                    $res .= ')';

                    return $res;
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'user',
                    'options' => [
                        'class' => 'form-control ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#insurance-handbook-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/handbook/insurance-handbook/index']) . "?"
                                                . Html::getInputName($searchModel, 'user_id') . "=' + ui.item.id + '&"
                                                . Html::getInputName($searchModel, 'user') . "=' + ui.item.value
                            });
                            jQuery('#insurance-handbook-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            // 'updated_user_id',
            'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<?php $this->registerCss("

    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }

"); ?>


<?php $this->registerJs("
    $(document).on('change', '.ms-operation',
        function(event) {
            if (!$(this).val())
            {
                jQuery('#insurance-handbook-grid').yiiGridView({'filterUrl': '" . Url::to(['/handbook/insurance-handbook/index']) . "'});
                jQuery('#insurance-handbook-grid').yiiGridView('applyFilter');
            }
        }
    );

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

"); ?>
