<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\InsuranceHandbook */

$this->title = Yii::t('app', 'Add') . ' ' . Yii::t('app', 'Insurance company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Insurance companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurance-handbook-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
