<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\InsuranceHandbook */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Insurance company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Insurance companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="insurance-handbook-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
