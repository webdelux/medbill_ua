<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use execut\widget\TreeView;
use leandrogehlen\treegrid\TreeGrid;
use backend\modules\handbook\models\Status;

$this->title = Yii::t('app', 'Measurements');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="measurement-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Measurement'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    echo TreeGrid::widget([
        'dataProvider' => $dataProvider,
        'keyColumnName' => 'id',
        'parentColumnName' => 'parent_id',
        'columns' => [
            [
                'header' => Yii::t('app', 'Name'),
                'attribute' => 'name',
            ],
            [
                'header' => Yii::t('app', 'Abbreviation'),
                'attribute' => 'abbreviation',
            ],
            [
                'header' => Yii::t('app', 'Description'),
                //'label' => 'rrrr',
                'attribute' => 'description',
                'value' => function ($data)
                {
                    return $data['description'] ? $data['description'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'User'),
                'attribute' => 'user_id',
                'value' => function ($data)
                {
                    $user = backend\modules\user\models\Users::findOne(['id' => $data['user_id']]);
                    return $user['username'] ? $user['username'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'Updated User'),
                'attribute' => 'updated_user_id',
                'value' => function ($data)
                {
                    $user = backend\modules\user\models\Users::findOne(['id' => $data['updated_user_id']]);
                    return $user['username'] ? $user['username'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'Created At'),
                'attribute' => 'created_at',
                'value' => function ($data)
                {
                    return $data['created_at'] ? $data['created_at'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'Updated At'),
                'attribute' => 'updated_at',
                'value' => function ($data)
                {
                    return $data['updated_at'] ? $data['updated_at'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'Status'),
                'attribute' => 'status_id',
                'value' => function ($data)
                {
                    $status = Status::findOne(['id' => $data['status_id']]);
                    return $status['name'] ? $status['name'] : '';
                }
            ],
            ['class' => 'yii\grid\ActionColumn']
        ]
    ]);
    ?>

</div>

