<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use execut\widget\TreeView;
use leandrogehlen\treegrid\TreeGrid;
use backend\modules\handbook\models\Status;

$this->title = Yii::t('app', 'Measurements');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="measurement-index">

    <?php
    echo  GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'abbreviation',
            ],
        ]
    ]);
    ?>

</div>

