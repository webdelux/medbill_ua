<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\MeasurementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<!--<div class="measurement-search">-->

    <?php /* $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); */?>
    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'parent_id') ?>

    <?php // echo $form->field($model, 'sort') ?>

    <?php //echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'abbreviation') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'updated_user_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'status_id') ?>

        <!--<div class="form-group">-->
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'hidden btn btn-primary']) ?>
        <!--</div>-->
    <?php // ActiveForm::end(); ?>

<?php //echo Html::input('search', 'name', "", ['class' => "form-control", "id" => "name"]) ?>
<!--</div>-->

<?php $this->registerJsFile("@web/themes/atlant/js/plugins/jquery-filtertable/jquery.filtertable.js",
            ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<?php $this->registerJs('
    $("table").filterTable({
        label: "",
        placeholder: "' . Yii::t('app', 'Search text') . '",
        containerTag: "div",
        containerClass: "filter-table"
    });
    //if this code appears after your tables; otherwise, include it in your document.ready() code.
    $(".filter-table input").addClass("form-control");
'); ?>
<?php $this->registerCss("

    .filter-table .quick { margin-left: 0.5em; font-size: 0.8em; text-decoration: none; }

    .fitler-table .quick:hover { text-decoration: underline; }

    td.alt { background-color: #ffc; background-color: rgba(255, 255, 0, 0.2); }

    .filter-table {
        margin-bottom: 10px;
    }

"); ?>

