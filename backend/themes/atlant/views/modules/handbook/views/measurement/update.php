<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Update measurements');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Measurements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="measurement-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
