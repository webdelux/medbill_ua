<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Create Measurement');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Measurements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="measurement-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
