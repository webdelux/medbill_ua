<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\handbook\models\Department;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\HandbookStacionary */

$this->title =  Yii::t('app', 'Room') . ' ' . $model->place;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List of places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="handbook-stacionary-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'department',
                'value' => (isset($model->room->department->name)) ? $model->room->department->name : null
            ],
            [
                'attribute' => 'room_id',
                'value' => (isset($model->room->room)) ? $model->room->room : null
            ],
            'place',
            'description',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
            [
                'attribute' => 'bed_type',
                'value' => (isset($model->bedType->bed_type)) ? $model->bedType->bed_type : null
            ],
            [
                'attribute' => 'pacient_id',
                 'value' => ($model->pacient_id == 0) ? Yii::t('app', 'Yes') : Yii::t('app', 'No')
            ],
            [
                'attribute' => 'type',
                 'value' => $model::itemAlias('gender_type', ($model->type) ? $model->type : '')
            ],
            [
                'attribute' => 'status',
                 'value' => $model::itemAlias('status', ($model->status) ? $model->status : '')
            ],
        ],
    ]) ?>

</div>
