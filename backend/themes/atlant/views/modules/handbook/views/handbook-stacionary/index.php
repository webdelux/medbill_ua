<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use backend\modules\handbook\models\Department;
use backend\modules\handbook\models\HandbookStacionary;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\HandbookStacionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List of places');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="handbook-stacionary-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'). ' ' . Yii::t('app', 'room'), ['handbook-room/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Add'). ' ' . mb_strtolower(Yii::t('app', 'Place')), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            [
                'attribute' => 'department',
                'group'=>true,  // enable grouping
                'value' => function ($data)
                {
                    return (isset ($data->room->department->name)) ? $data->room->department->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'department',
                        (new Department)->treeList((new Department)->treeData()),
                        ['class'=>'form-control', 'prompt' => '']),
                'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                    return [
                        'mergeColumns' => [[0,1]], // columns to merge in summary
                        'content' => [             // content to show in each summary cell
                            1 => Yii::t('app', 'Summary') . '(' . $model->room->department->name . '):',
                            2 => GridView::F_COUNT,
                            3 => GridView::F_COUNT,

                        ],
                        'contentOptions'=>[      // content html attributes for each summary cell
                            0=>['style'=>'font-variant:small-caps; text-align:left'],
                            2=>['style'=>'text-align:right'],
                            3=>['style'=>'text-align:right'],
                        ],
                        // html attributes for group summary row
                        'options'=>['class'=>'info','style'=>'font-weight:bold;']
                    ];
                },
            ],
            [
                'attribute' => 'room_id',
                'group'=>true,  // enable grouping
                'subGroupOf'=>1, // supplier column index is the parent group
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
                'value' => function ($data)
                {
                    return (isset ($data->room->room)) ? $data->room->room : '';
                },
//                'pageSummary'=>Yii::t('app', 'Summary' ).':',
//                'pageSummaryOptions'=>['class'=>'text-right text-warning'],
            ],
            [
                'attribute' => 'place',
                'contentOptions' => function ($data)
                {
                    return ['class' => ($data->pacient_id == 0) ? 'success' : 'danger'];
                },
//                'pageSummary'=>true,
//                'pageSummaryOptions'=>['class'=>'text-right text-warning'],
//                'pageSummaryFunc'=>GridView::F_COUNT,
            ],
             [
                'attribute' => 'description',
                'contentOptions' => function ($data)
                {
                    return ['class' => ($data->pacient_id == 0) ? 'success' : 'danger'];
                },
            ],
            // 'user_id',
            // 'updated_user_id',
            [
                'label' => Yii::t('app', 'User'),
                'contentOptions' => function ($data)
                {
                    return ['class' => ($data->pacient_id == 0) ? 'success' : 'danger'];
                },
                'attribute' => 'userName',
                'value' => function ($data)
                {
                    return (isset ($data->username)) ? $data->username : '';
                }
            ],
//            [
//                'label' => Yii::t('app', 'Updated User'),
//                'contentOptions' => function ($data)
//                {
//                    return ['class' => ($data->pacient_id == 0) ? 'success' : 'danger'];
//                },
//                'attribute' => 'updatedUser',
//                'value' => function ($data)
//                {
//                    return (isset ($data->updatedUserName)) ? $data->updatedUserName : '';
//                }
//            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => function ($data)
                {
                    return ['class' => ($data->pacient_id == 0) ? 'success' : 'danger'];
                },
            ],
            [
                'attribute' => 'updated_at',
                'contentOptions' => function ($data)
                {
                    return ['class' => ($data->pacient_id == 0) ? 'success' : 'danger'];
                },
            ],
            [
                'attribute' => 'bed_type',
                'filter' => Html::activeDropDownList($searchModel, 'bed_type',
                        ArrayHelper::map(backend\modules\handbook\models\BedType::find()->all(), 'id', 'bed_type'),
                        ['class'=>'form-control', 'prompt' => '']),
                'value' => function ($data)
                {
                    return (isset ($data->bedType->bed_type)) ? $data->bedType->bed_type : '';
                },
            ],
            [
                'attribute' => 'type',
                'filter' => HandbookStacionary::itemAlias('gender_type'),
                'value' => function ($data)
                {
                    $res = $data::itemAlias('gender_type', ($data->type) ? $data->type : '');
                    return $res;
                }
            ],
            [
                'attribute' => 'pacient_id',
                'contentOptions' => function ($data)
                {
                    return ['class' => ($data->pacient_id == 0) ? 'success' : 'danger'];
                },
                'filter' => Html::activeDropDownList($searchModel, 'pacient_id',
                    ['0'=>Yii::t('app', 'Empty'),'1'=>Yii::t('app', 'Occupied by')], ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($data)
                {

                    return ($data->pacient_id == 0) ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                }
            ],
                    [
                'attribute' => 'status',
                'filter' => HandbookStacionary::itemAlias('status'),
                'value' => function ($data)
                {
                    $res = $data::itemAlias('status', ($data->status) ? $data->status : '');
                    return $res;
                }
            ],

            [
                //'class' => 'yii\grid\ActionColumn',
                'class' => 'kartik\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view} {update} {delete}{link}',
            ],
        ],
    ]); ?>

</div>
