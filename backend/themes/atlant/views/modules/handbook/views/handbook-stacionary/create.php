<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\HandbookStacionary */

$this->title = Yii::t('app', 'Add') . ' ' . mb_strtolower(Yii::t('app', 'Place'));
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List of places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="handbook-stacionary-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
