<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use backend\modules\handbook\models\Department;
use yii\helpers\Url;
//use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\HandbookStacionary */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="handbook-stacionary-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?php
        $model->department = (isset($model->room->department_id)) ? $model->room->department_id : '';
    ?>

    <?=
        $form->field($model, 'department', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Department)->treeList((new Department)->treeData()), ['prompt' => '',
            'onchange'=>'
            $.get( "'.Url::toRoute('handbook-room/index').'", { department: $(this).val() } )
                .done(function( data )
                   {
                        $("select#handbookstacionary-room_id").val("").empty();

                        $("select#handbookstacionary-room_id").append($("<option></option>").attr("value", "").text(""));

                        $.each(data, function(index, option) {
                            $("select#handbookstacionary-room_id").append($("<option></option>").attr("value", option.id).text(option.value));
                        });
                });
            '])
    ?>

    <?php
         echo $form->field($model, 'room_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList(['prompt' => ''])
    ?>

    <?=
        $form->field($model, 'place', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'bed_type', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList(ArrayHelper::map(backend\modules\handbook\models\BedType::find()->all(), 'id', 'bed_type'), ['prompt' => ''])
    ?>

    <?=
        $form->field($model, 'pacient_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList(['0'=>Yii::t('app', 'Empty')], ['prompt' => ''])
    ?>

    <?=
        $form->field($model, 'type', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList($model::itemAlias('gender_type'))
    ?>

    <?=
        $form->field($model, 'status', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList($model::itemAlias('status'))
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs("
//При update показуємо збережене значення палати
$(window).load(function() {
$.get( '".Url::toRoute('handbook-room/index')."', { department: '{$model->department}'} )
                .done(function( data )
                   {
                        $('select#handbookstacionary-room_id').val('').empty();

                        $('select#handbookstacionary-room_id').append($('<option></option>').attr('value', '').text(''));

                        $.each(data, function(index, option) {
                            $('select#handbookstacionary-room_id').append($('<option></option>').attr('value', option.id).text(option.value));
                        });


                        $('select#handbookstacionary-room_id').val({$model->room_id});
                });
                });
"); ?>

