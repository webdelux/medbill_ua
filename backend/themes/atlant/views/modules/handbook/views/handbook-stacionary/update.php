<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\HandbookStacionary */

$this->title = Yii::t('app', 'Update') . ' '. Yii::t('app', 'room');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hospital'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Place') . ' ' . $model->place, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="handbook-stacionary-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
