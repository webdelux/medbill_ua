<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use backend\modules\handbook\models\Department;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\HandbookRoom */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="handbook-room-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?=
        $form->field($model, 'department_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Department)->treeList((new Department)->treeData()))
    ?>

    <?=
        $form->field($model, 'room', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
