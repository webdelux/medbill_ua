<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\HandbookRoom */

$this->title = Yii::t('app', 'Create') . ' ' . Yii::t('app', 'Room');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Places'), 'url' => ['handbook-stacionary/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rooms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="handbook-room-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
