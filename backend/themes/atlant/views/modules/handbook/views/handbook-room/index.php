<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use backend\modules\handbook\models\Department;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\HandbookRoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rooms');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List of places'), 'url' => ['handbook-stacionary/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="handbook-room-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'List of places'), ['handbook-stacionary/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Add') . ' ' . Yii::t('app', 'room'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            [
                'attribute' => 'department_id',
                'group'=>true,  // enable grouping
                'pageSummary'=>Yii::t('app', 'Total rooms:' ),
                'pageSummaryOptions'=>['class'=>'text-right text-warning'],
                'value' => function ($data)
                {
                    return (isset ($data->department->name)) ? $data->department->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'department_id',
                        (new Department)->treeList((new Department)->treeData()),
                        ['class'=>'form-control', 'prompt' => '']),
            ],
            [   'attribute' => 'room',
                'pageSummary'=>true,
                'pageSummaryOptions'=>['class'=>'text-left text-warning'],
                'pageSummaryFunc'=>GridView::F_COUNT,
            ],
            [   'attribute' => 'description',
            ],
            //'user_id',
            // 'updated_user_id',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'userName',
                'value' => function ($data)
                {
                    return (isset ($data->username)) ? $data->username : '';
                }
            ],
            [
                'label' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUser',
                'value' => function ($data)
                {
                    return (isset ($data->updatedUserName)) ? $data->updatedUserName : '';
                }
            ],
            'created_at',
            'updated_at',

            [
                //'class' => 'yii\grid\ActionColumn',
                'class' => 'kartik\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view} {update} {delete}{link}',
            ],
        ],
    ]); ?>

</div>
