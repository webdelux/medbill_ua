<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\HandbookRoom */

$this->title = Yii::t('app', 'Edit') . ' ' . Yii::t('app', 'room');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Places'), 'url' => ['handbook-stacionary/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rooms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->room, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="handbook-room-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
