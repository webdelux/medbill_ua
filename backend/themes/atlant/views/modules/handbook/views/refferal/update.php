<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Refferal */

$this->title = Yii::t('app', 'Update') . ' ' .Yii::t('app', 'Refferal');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refferal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pib, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="refferal-update">

    <?= $this->render('_form', [
        'model' => $model,
        'categoryId' => $categoryId
    ]) ?>

</div>
