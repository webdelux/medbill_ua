<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Refferal */

$this->title = Yii::t('app', 'Add') . ' ' . Yii::t('app', 'Refferal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refferal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refferal-create">

    <?= $this->render('_form', [
        'model' => $model,
        'categoryId' => $categoryId
    ]) ?>

</div>
