<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Category;
use backend\modules\handbook\models\Status;
use backend\modules\handbook\models\Speciality;
use backend\modules\handbook\models\Workplace;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Refferal */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="refferal-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?=
        $form->field($model, 'category_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Category)->treeList((new Category)->treeData($categoryId)))
    ?>

    <?=
        $form->field($model, 'speciality_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Speciality)->treeList((new Speciality)->treeData()))
    ?>

    <?= $form->field($model, 'pib', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'gender', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList($model::itemAlias('gender'))
    ?>

    <?= $form->field($model, 'birthday', ['labelOptions' => ['class' => 'col-sm-2 control-label'],
        'inputOptions' => ['class' => 'mask_date form-control']])->textInput(['maxlength' => true])
            ->hint(Yii::t('app', 'Year-month-day'),['class'=>'help-block'])
    ?>

    <?= $form->field($model, 'phone', ['labelOptions' => ['class' => 'col-sm-2 control-label'],
        'inputOptions' => ['class' => 'mask_phone form-control ']])->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'phone1', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'email', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'requisites', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

     <?=
        $form->field($model, 'work_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList(ArrayHelper::map(Workplace::find()->all(), 'id', 'name'))
    ?>

    <?= $form->field($model, 'cityName',['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->widget(\yii\jui\AutoComplete::classname(),
        [
            'options' => [
                'class' => 'form-control',
                'value' => (isset($model->city->address)) ? $model->city->address : null
            ],
            'clientOptions' => [
                'source' => Url::to(['/handbook/city/index']),
                'select' => new JsExpression("function(event, ui) {
                    $('#" . Html::getInputId($model, 'city_id') . "').val(ui.item.id);
                }")
            ],
        ])
    ?>

    <?= Html::activeHiddenInput($model, 'city_id') ?>


    <?= $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'status_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList(Status::childList(), ['prompt' => ''])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("
    $(document).on('change', '#" . Html::getInputId($model, 'cityName') . "', function(event) {
        if (!$(this).val()) {
            $('#" . Html::getInputId($model, 'city_id') . "').val('');
        }
    });
"); ?>

<?php $this->registerCss("
    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }
    .ui-autocomplete::after, .ui-autocomplete::before {
        border: none;
    }
"); ?>