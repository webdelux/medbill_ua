<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\handbook\models\Status;
use backend\models\Category;
use backend\modules\handbook\models\Speciality;
use backend\modules\handbook\models\Workplace;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\RefferalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Refferal');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refferal-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Category'), ['index', 'category'=>TRUE], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Workplace'), ['workplace/index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Add'). ' ' . Yii::t('app', 'Refferal'), ['create'], ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($data)
                {
                    return (isset($data->category->name)) ? $data->category->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'category_id',
                        (new Category)->treeList((new Category)->treeData($categoryId)),
                        ['class'=>'form-control', 'prompt' => '']),
            ],
            [
                'attribute' => 'speciality_id',
                'value' => function ($data)
                {
                    return (isset($data->speciality->name)) ? $data->speciality->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'speciality_id',
                        (new Speciality)->treeList((new Speciality)->treeData()),
                        ['class'=>'form-control', 'prompt' => '']),
            ],
            'pib',
            [
                'attribute' => 'gender',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '50'],
                'filter' => backend\modules\handbook\models\Refferal::itemAlias('gender'),
                'value' => function ($data)
                {
                    $res = $data::itemAlias('gender', ($data->gender) ? $data->gender : '');
                    $res = StringHelper::truncate($res, 3, '.');
                    return $res;
                }
            ],
            [
                'attribute' => 'birthday',
                'format' => ['date', 'dd MMM Y'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            'phone',
            ['attribute' =>'phone1',
             'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            'email:email',
            'requisites',
            [
                'attribute' => 'work_id',
                'value' => function ($data)
                {
                    return (isset($data->workplace->name)) ? $data->workplace->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'work_id',
                        ArrayHelper::map(Workplace::find()->all(), 'id', 'name'),
                        ['class'=>'form-control', 'prompt' => '']),
            ],
            [
                'label' => Yii::t('app', 'City'),
                'attribute' => 'cityName',
                'contentOptions' => ['width' => '130'],
                'value' => function ($data)
                    {
                        return (isset($data->city->address)) ? $data->city->address : '';
                    }
            ],
            'description',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'userName',
                'value' => function ($data)
                {
                    return (isset ($data->username)) ? $data->username : '';
                }
            ],
            [
                'label' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUser',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    return (isset ($data->updatedUserName)) ? $data->updatedUserName : '';
                }
            ],
            'created_at',
            'updated_at',
            [
                'attribute' => 'status_id',
                'filter' => Status::childList(),
                'value' => function ($data)
                {
                    return (isset ($data->status->name)) ? $data->status->name : '';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view}{update}{delete}{link}',
            ],
        ],
    ]); ?>

</div>
