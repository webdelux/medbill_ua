<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Refferal */

$this->title = $model->pib;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refferal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refferal-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => (isset($model->category->name)) ? $model->category->name : null
            ],
            [
                'attribute' => 'speciality_id',
                'value' => (isset($model->speciality->name)) ? $model->speciality->name : null
            ],
            'pib',
            [
              'attribute' => 'gender',
                'value' => $model::itemAlias('gender', ($model->gender) ? $model->gender : '')
            ],
            'birthday',
            'phone',
            'phone1',
            'email:email',
            'requisites',
            [
              'attribute' => 'work_id',
                'value' => (isset($model->workplace->name)) ? $model->workplace->name : null
            ],
            [
              'attribute' => 'city_id',
                'value' => (isset($model->city->address)) ? $model->city->address : null
            ],
            'description',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
            [
                'attribute' => 'status_id',
                'value' => (isset($model->status->name)) ? $model->status->name : null
            ],
        ],
    ]) ?>

</div>
