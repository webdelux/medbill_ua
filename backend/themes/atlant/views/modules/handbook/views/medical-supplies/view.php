<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\MedicalSupplies */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Medical Supplies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-supplies-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => (isset($model->category->name)) ? $model->category->name : null
            ],
            'name',
            [
                'attribute' => 'measurement_id',
                'value' => (isset($model->measurement->name)) ? $model->measurement->name : null
            ],
            'description',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
            [
                'attribute' => 'status_id',
                'value' => (isset($model->status->name)) ? $model->status->name : null
            ],
        ],
    ]) ?>

</div>
