<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Category;
use backend\modules\handbook\models\Status;
use backend\modules\handbook\models\Measurement;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\MedicalSupplies */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="medical-supplies-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?=
        $form->field($model, 'category_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Category)->treeList((new Category)->treeData($categoryId)))
    ?>

    <?php // echo $form->field($model, 'sort')->textInput()  ?>

    <?=
        $form->field($model, 'name', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?php echo
    $form->field($model, 'measurementName', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(\yii\jui\AutoComplete::classname(), [
            'options' => [
                'class' => 'form-control',
                'value' => (isset($model->measurement->name)) ? $model->measurement->name : null
            ],
            'clientOptions' => [
                'source' => Url::to(['/handbook/measurement/index']),
                'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'measurement_id') . "').val(ui.item.id);
            }")
            ],
        ])
    ?>
    <?= Html::activeHiddenInput($model, 'measurement_id') ?>

    <?=
        $form->field($model, 'price', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>


    <?php // echo $form->field($model, 'measurement_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])->textInput() ?>

    <?=
        $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'status_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList(Status::childList(), ['prompt' => ''])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("
    $(document).on('change', '#" . Html::getInputId($model, 'measurementName') . "', function(event) {
        if (!$(this).val()) {
            $('#" . Html::getInputId($model, 'measurement_id') . "').val('');
        }
    });
"); ?>


<?php $this->registerCss("
.ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }
    .ui-autocomplete::after, .ui-autocomplete::before {
        border: none;
    }
"); ?>