<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\handbook\models\Status;
use backend\models\Category;
use yii\widgets\Pjax;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\MedicalSuppliesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Medical Supplies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-supplies-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create category'), ['index', 'category'=>TRUE], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Create Medical Supplies'), ['create', 'category'=>FALSE], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'hb-medical-supplies',]); ?>

    <?= GridView::widget([
        'options' => [
            'id' => 'medical-supplies-grid'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($data)
                {
                    return (isset($data->category->name)) ? $data->category->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'category_id',
                        (new Category)->treeList((new Category)->treeData($categoryId)),
                        ['class'=>'form-control', 'prompt' => '']),
            ],
            //'sort',
            'name',
            [
                'attribute' => 'measurement',
                'value' => function ($data)
                {
                    return (isset ($data->measurement->name)) ? $data->measurement->name : '';
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'measurement',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/handbook/measurement/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#medical-supplies-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/handbook/medical-supplies/index']) . "?"
                                . Html::getInputName($searchModel, 'measurement_id') . "=' + ui.item.id + '&"
                                . Html::getInputName($searchModel, 'measurement') . "=' + ui.item.value
                            });
                            jQuery('#medical-supplies-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            'price',
            'description',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'user',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    $res = (isset($data->userProfile->name)) ? $data->userProfile->name : '';
                    $res .= ' (';
                    $res .= (isset($data->user->username)) ? $data->user->username : '';
                    $res .= ')';

                    return $res;
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'user',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#medical-supplies-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/handbook/medical-supplies/index']) . "?"
                                                . Html::getInputName($searchModel, 'user_id') . "=' + ui.item.id + '&"
                                                . Html::getInputName($searchModel, 'user') . "=' + ui.item.value
                            });
                            jQuery('#medical-supplies-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            [
                'label' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUser',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                    {
                        $res = (isset($data->updatedUserProfile->name)) ? $data->updatedUserProfile->name : '';
                        $res .= ' (';
                        $res .= (isset($data->updatedUser->username)) ? $data->updatedUser->username : '';
                        $res .= ')';

                        return $res;
                    },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'updatedUser',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#medical-supplies-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/handbook/medical-supplies/index']) . "?"
                                                . Html::getInputName($searchModel, 'updated_user_id') . "=' + ui.item.id + '&"
                                                . Html::getInputName($searchModel, 'updatedUser') . "=' + ui.item.value
                            });
                            jQuery('#medical-supplies-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            'created_at',
            'updated_at',
            [
                'attribute' => 'status_id',
                'filter' => Status::childList(),
                'value' => function ($data)
                {
                    return (isset ($data->status->name)) ? $data->status->name : '';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view} {update} {delete}{link}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php $this->registerCss("

    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }

"); ?>


<?php $this->registerJs("

    $(document).on('change', '#medicalsuppliessearch-measurement, #medicalsuppliessearch-user, #medicalsuppliessearch-updateduser', function(event) {
        if (!$(this).val())
        {
            jQuery('#medical-supplies-grid').yiiGridView({'filterUrl': '" . Url::to(['/handbook/medical-supplies/index']) . "'});
            jQuery('#medical-supplies-grid').yiiGridView('applyFilter');
        }
    });

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

"); ?>