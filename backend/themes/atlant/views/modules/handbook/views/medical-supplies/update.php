<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\MedicalSupplies */

$this->title = Yii::t('app', 'Update') . ' ' .Yii::t('app', 'Medical Supplies');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Medical Supplies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="medical-supplies-update">


    <?= $this->render('_form', [
        'model' => $model,
        'categoryId' => $categoryId
    ]) ?>

</div>
