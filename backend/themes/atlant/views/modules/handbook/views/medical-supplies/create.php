<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\MedicalSupplies */

$this->title = Yii::t('app', 'Create Medical Supplies');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Medical Supplies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-supplies-create">

    <?= $this->render('_form', [
        'model' => $model,
        'categoryId' => $categoryId
    ]) ?>

</div>
