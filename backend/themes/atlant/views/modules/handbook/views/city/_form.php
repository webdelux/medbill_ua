<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\ArrayHelper;
use backend\modules\handbook\models\Country;
use backend\modules\handbook\models\Region;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="city-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'country_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList(ArrayHelper::map(Country::find()->all(), 'id', 'title'), ['prompt' => '']) ?>

    <?= $form->field($model, 'important', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList($model::itemAlias('important')) ?>

    <?= $form->field($model, 'region_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList(ArrayHelper::map(Region::find()->all(), 'id', 'title'), ['prompt' => '']) ?>

    <?= $form->field($model, 'title', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>