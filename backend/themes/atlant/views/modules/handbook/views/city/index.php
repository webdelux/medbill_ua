<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\handbook\models\Country;
use backend\modules\handbook\models\City;
use backend\modules\handbook\models\Region;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="city-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            [
                'attribute' => 'country_id',
                'value' => function ($data) {
                    return $data->country->title;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => ArrayHelper::map(Country::find()->all(), 'id', 'title'),
            ],
            [
                'attribute' => 'region_id',
                'value' => function ($data) {
                    return (isset($data->regionLink->title)) ? $data->regionLink->title : null;
                },
                'headerOptions' => ['width' => '200'],
                'filter' => ArrayHelper::map(Region::find()->all(), 'id', 'title'),
            ],
            [
                'attribute' => 'important',
                'value' => function ($data) {
                    return City::itemAlias('important', $data->important);
                },
                'headerOptions' => ['width' => '200'],
                'filter' => City::itemAlias('important'),
            ],
            'title',
            'area',
            'zip',
            //'region',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '75'],
            ],
        ],
    ]); ?>

</div>