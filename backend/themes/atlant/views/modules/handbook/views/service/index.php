<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\handbook\models\Status;
use backend\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Services');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create category'), ['index', 'category'=>TRUE], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Create Service'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($data)
                {
                    return (isset($data->category->name)) ? $data->category->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'category_id',
                        (new Category)->treeList((new Category)->treeData($categoryId)),
                        ['class'=>'form-control', 'prompt' => '']),
            ],
            'name',
            'price',
            'description',
            [
                'header' => Yii::t('app', 'User'),
                'attribute' => 'userName',
                'value' => function ($data)
                {
                    return (isset ($data->username)) ? $data->username : '';
                }
            ],
            // 'updated_user_id',
            [
                'header' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUserName',
                'value' => function ($data)
                {
                    return (isset ($data->updatedUserName)) ? $data->updatedUserName : '';
                }
            ],
            'created_at',
            'updated_at',
            // 'status_id',
            [
                'attribute' => 'status_id',
                'filter' => Status::childList(),
                'value' => function ($data)
                {
                    return (isset ($data->status->name)) ? $data->status->name : '';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view} {update} {delete}{link}',
            ],

        ],
    ]); ?>

</div>
