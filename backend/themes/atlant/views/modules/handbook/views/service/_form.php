<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Category;
use backend\modules\handbook\models\Status;
use common\widgets\Alert;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="service-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?=
        $form->field($model, 'category_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Category)->treeList((new Category)->treeData($categoryId)))
    ?>

    <?=
        $form->field($model, 'name', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'price', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?=
        $form->field($model, 'status_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList(Status::childList(), ['prompt' => ''])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
