<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="department-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'category_id',
                'value' => (isset($model->category->name)) ? $model->category->name : null
            ],
            'address',
            'phone',
            'email',
            'edrpou',
            'requisites',
            [
                'attribute' => 'status_id',
                'value' => (isset($model->status->name)) ? $model->status->name : null
            ],
            [
                'attribute' => 'type_id',
                'value' => $model::itemAlias('type_id', ($model->type_id) ? $model->type_id : '')
            ],
            [
                'attribute' => 'city_type',
                'value' => $model::itemAlias('city_type', ($model->city_type) ? $model->city_type : '')
            ],
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>