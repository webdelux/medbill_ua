<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'ID') . $model->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="department-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>