<?php

use yii\helpers\Html;
//use yii\grid\GridView;
//use yii\web\JsExpression;
use execut\widget\TreeView;

$this->title = Yii::t('app', 'Departments');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="department-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php /* echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */ ?>

    <?php echo TreeView::widget([
        'data' => $tree,
        'size' => TreeView::SIZE_NORMAL,
        'template' => TreeView::TEMPLATE_ADVANCED,
        'header' => Yii::t('app', 'Departments'),
        'searchOptions' => [
            'inputOptions' => [
                'placeholder' => Yii::t('app', 'Search')
            ],
        ],
        'clientOptions' => [
            'enableLinks' => true,
        ],
    ]); ?>

</div>


<?php $this->registerCss("

    .treeview .list-group .list-group-item:last-child {
        border-bottom: 1px solid #e5e5e5;
    }

"); ?>