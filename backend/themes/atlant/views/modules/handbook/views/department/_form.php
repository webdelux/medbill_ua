<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use backend\modules\handbook\models\Status;
use backend\modules\handbook\models\DepartmentCategory;
use kartik\time\TimePicker;
?>

<?php echo Alert::widget(); ?>

<div class="department-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'parent_id')->dropDownList($model::treeList()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList((new DepartmentCategory)->treeList((new DepartmentCategory)->treeData()), ['prompt' => '']) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone', ['inputOptions' => ['class' => 'mask_phone form-control']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edrpou')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'requisites')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'working_days')->checkboxList($model::itemAlias('working_days')) ?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <table class="field-department-working_time">
                <tr>
                    <?php foreach ($model::itemAlias('working_days') as $key => $value): ?>
                        <td>

                            <?= $form->field($model, "working_time_{$key}_from", [
                                'template' => '{label} {input} {error}',
                                'inputOptions' => ['class' => 'form-control'],
                                'labelOptions' => ['class' => 'control-label col-sm-2']
                            ])->widget(TimePicker::classname(), [
                                'options' => [
                                    'placeholder' => '00:00:00',
                                ],
                                'pluginOptions' => [
                                    'showSeconds' => true,
                                    'showMeridian' => false,
                                    'template' => false
                                ]
                            ]) ?>

                            <?= $form->field($model, "working_time_{$key}_to", [
                                'template' => '{label} {input} {error}',
                                'inputOptions' => ['class' => 'form-control'],
                                'labelOptions' => ['class' => 'control-label col-sm-2']
                            ])->widget(TimePicker::classname(), [
                                'options' => [
                                    'placeholder' => '00:00:00',
                                ],
                                'pluginOptions' => [
                                    'showSeconds' => true,
                                    'showMeridian' => false,
                                    'template' => false
                                ]
                            ]) ?>

                        </td>
                    <?php endforeach; ?>
                </tr>
            </table>
        </div>
    </div>

    <?= $form->field($model, 'status_id')->dropDownList(Status::childList(), ['prompt' => '']) ?>

    <?= $form->field($model, 'type_id')->dropDownList($model::itemAlias('type_id'), ['prompt' => '']) ?>

    <?= $form->field($model, 'city_type')->dropDownList($model::itemAlias('city_type'), ['prompt' => '']) ?>

    <hr/>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php $this->registerCss("

    .field-department-working_days {
        margin: 0;
    }
    #department-working_days {
        display: table;
    }
    #department-working_days > .checkbox {
        margin: 0;
        padding: 0;
        display: table-cell;
        width: 100px;
        text-align: left;
    }

    .field-department-working_time tr > td {
        width: 100px;
        padding-right: 5px;
    }
    .field-department-working_time tr > td > .form-group {
        margin: 0;
    }
    .field-department-working_time tr > td > .form-group label {
        margin: 0;
        padding: 0;
        display: block;
        width: 90px;
        text-align: center;
    }

"); ?>