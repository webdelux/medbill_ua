<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Create');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="department-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>