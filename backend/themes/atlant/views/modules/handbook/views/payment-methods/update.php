<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\PaymentMethods */

$this->title = Yii::t('app', 'Update') . ' ' .Yii::t('app', 'Payment methods');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-methods-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
