<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\PaymentMethods */

$this->title = Yii::t('app', 'Add') . ' ' . Yii::t('app', 'Payment methods');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-methods-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
