<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\handbook\models\Status;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\PaymentMethodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Payment methods');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-methods-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'). ' ' . Yii::t('app', 'Payment method'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            'name',
            'description',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'userName',
                'value' => function ($data)
                {
                    return (isset ($data->username)) ? $data->username : '';
                }
            ],
            [
                'label' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUser',
                'value' => function ($data)
                {
                    return (isset ($data->updatedUserName)) ? $data->updatedUserName : '';
                }
            ],
            [
                'attribute' => 'status_id',
                'filter' => Status::childList(),
                'value' => function ($data)
                {
                    return (isset ($data->status->name)) ? $data->status->name : '';
                }
            ],
            'created_at',
            'updated_at',
            // 'status_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view} {update} {delete}{link}',
            ],
        ],
    ]); ?>

</div>
