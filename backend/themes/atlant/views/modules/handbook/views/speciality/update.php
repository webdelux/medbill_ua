<?php

use yii\helpers\Html;

$this->title = Yii::t('speciality', 'Update Speciality');

$this->params['breadcrumbs'][] = ['label' => Yii::t('speciality', 'Specialities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="speciality-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
