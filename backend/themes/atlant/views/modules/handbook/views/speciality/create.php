<?php

use yii\helpers\Html;

$this->title = Yii::t('speciality', 'Create Speciality');

$this->params['breadcrumbs'][] = ['label' => Yii::t('speciality', 'Specialities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="speciality-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
