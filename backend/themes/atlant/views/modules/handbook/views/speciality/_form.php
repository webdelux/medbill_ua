<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use backend\modules\handbook\models\Status;
?>

<?php echo Alert::widget(); ?>

<div class="speciality-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'parent_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList($model->treeList($model->treeData()), ['prompt' => '']) ?>

    <?= $form->field($model, 'name', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList(Status::childList(), ['prompt' => '']) ?>


    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
