<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use execut\widget\TreeView;

$this->title = Yii::t('speciality', 'Specialities');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="speciality-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('speciality', 'Create Speciality'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php /* GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'parent_id',
            'name',
            'description',
            'user_id',
            // 'updated_user_id',
            // 'created_at',
            // 'updated_at',
            // 'status_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */ ?>

    <?php echo TreeView::widget([
        'data' => $tree,
        'size' => TreeView::SIZE_NORMAL,
        'clientOptions' => [
            'enableLinks' => true,
        ],
    ]); ?>

</div>

<?php $this->registerCss("

    .treeview .list-group .list-group-item:last-child {
        border-bottom: 1px solid #e5e5e5;
    }

"); ?>