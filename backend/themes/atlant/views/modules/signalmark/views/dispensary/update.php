<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\signalmark\models\Dispensary */

$this->title = Yii::t('app', 'Update') . ' ' . $model->pacient->fullName . '-' .Yii::t('app', 'Dispensary');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['/pacient/index']];
$this->params['breadcrumbs'][] = ['label' =>  $model->pacient->fullName, 'url' => ['/pacient/view', 'id' =>$model->pacient_id, 'tab' =>$tab]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dispensary-update">

    <?= $this->render('_form', [
        'model' => $model,
        'tab' => $tab,

    ]) ?>

</div>
