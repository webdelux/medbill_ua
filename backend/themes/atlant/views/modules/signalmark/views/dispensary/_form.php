<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\signalmark\models\Dispensary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dispensary-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ],
        ],
    ]); ?>

    <?=
        $form->field($model, 'group' )->dropDownList($model::itemAlias('group'))
    ?>

    <?= $form->field($model, 'date_in', [
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($model->date_in) ? $model->date_in : ''
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
    ?>

    <?= $form->field($model, 'date_out', [
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($model->date_out) ? $model->date_out : ''
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
    ?>

    <?=
        $form->field($model, 'description')->textInput(['maxlength' => true])
    ?>


     <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
