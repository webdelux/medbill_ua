<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\signalmark\models\Dispensary */

$this->title = Yii::t('app', 'Create Dispensary');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispensaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dispensary-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
