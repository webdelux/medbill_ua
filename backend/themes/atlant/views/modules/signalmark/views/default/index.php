<?php

use yii\helpers\Url;

$this->title = Yii::t('app', 'Signal mark');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-2">
        <a class="tile tile-default" href="<?= Url::to(['/signalmark/default/index']) ?>">
            <?= Yii::t('app', 'Signal mark') ?><br/>
            <span class="fa fa-list"></span>
        </a>
    </div>
</div>