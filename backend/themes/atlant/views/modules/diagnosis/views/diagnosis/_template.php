<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use kartik\select2\Select2;
use backend\modules\diagnosis\models\Diagnosis;
use backend\modules\diagnosis\models\DiagnosisAdditional;
use backend\models\PacientOperation;
use backend\modules\handbook\models\Department;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
?>

<div class="row">
    <div class="col-sm-12">
        <fieldset class="blk-diagnoses-basic">
            <legend><?php echo Html::a(Yii::t('app', 'Basic'), '#'); ?></legend>

            <?php Yii::$app->request->enableCsrfValidation = false; ?>

            <?php $form = ActiveForm::begin([
                'id' => 'form-diagnosis-template',
                'layout' => 'horizontal',
                'enableAjaxValidation' => false,
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-sm-6',
                    ],
                ],
            ]); ?>
            <?php ActiveForm::end(); ?>

            <?= Html::activeHiddenInput($model, 'pacient_id', [
                'value' => $model->pacient_id,
                'id' => Html::getInputId($model, 'pacient_id') . "-id{$model->id}",
                'name' => $model->formName() . "[{$model->id}][pacient_id]"
            ]) ?>

            <?= $form->field($model, 'handbook_emc_icd_id', [
                'template' => '{label}'
                . '<div class="col-sm-6">'
                    . '<div class="input-group">'
                        . '{input}'
                        . '<span class="input-group-btn">'
                            . Html::button('<span class="fa fa-list"></span>', [
                                'class' => 'btn btn-primary',
                                'data-toggle' => 'modal',
                                'data-target' => '#selector-handbook-emc-icd-modal',
                                'data-parent' => Html::getInputId($model, 'handbook_emc_icd_id') . "-id{$model->id}",
                            ])
                        . '</span>'
                    . '</div>'
                    . '{error}'
                    . '{hint}'
                . '</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($model->icd) ? '' : $model->icd->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($model, 'handbook_emc_icd_id') . "-id{$model->id}",
                    'name' => $model->formName() . "[{$model->id}][handbook_emc_icd_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbookemc/icd/index']),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>

            <?php if ($model->id): ?>
                <?php $diagnosis = Diagnosis::find()->where(['pacient_id' => $model->pacient_id, 'parent_id' => $model->id])->orderBy('id DESC')->all(); ?>
                <?php if (count($diagnosis)): ?>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <ul class="diagnosis-archive">
                                <?php $check = [$model->type => $model->id]; ?>
                                <?php foreach ($diagnosis as $value): ?>

                                    <?php
                                    // Перекреслюються діагнози тільки тоді, коли є такий самий тип але із пізнішою датою створення
                                    $through = false;
                                    if (isset($check[$value->type]))
                                        $through = true;
                                    else
                                        $check[$value->type] = $value->id;
                                    ?>

                                    <li class="<?php echo ($through) ? 'through' : ''; ?>">
                                        <?php echo date("d.m.Y H:i", strtotime($value->created_at)); ?>
                                        <?php echo (isset($value->icd->name)) ? $value->icd->name : null; ?>
                                        (<?php echo Diagnosis::itemAlias("type", $value->type); ?>)
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?= $form->field($model, 'alternative')->textarea([
                'rows' => 2,
                'id' => Html::getInputId($model, 'alternative') . "-id{$model->id}",
                'name' => $model->formName() . "[{$model->id}][alternative]"
            ]) ?>

            <?= $form->field($model, 'alternative_print')->radioList($model::itemAlias('alternative_print'), [
                'prompt' => '',
                'id' => Html::getInputId($model, 'alternative_print') . "-id{$model->id}",
                'name' => $model->formName() . "[{$model->id}][alternative_print]"
            ]) ?>

            <?= $form->field($model, 'type')->radioList($model::itemAlias('type'), [
                'prompt' => '',
                'id' => Html::getInputId($model, 'type') . "-id{$model->id}",
                'name' => $model->formName() . "[{$model->id}][type]"
            ]) ?>

            <?= $form->field($model, 'first_set')->radioList($model::itemAlias('first_set'), [
                'prompt' => '',
                'id' => Html::getInputId($model, 'first_set') . "-id{$model->id}",
                'name' => $model->formName() . "[{$model->id}][first_set]"
            ]) ?>

            <?= $form->field($model, 'prophylactic_set')->radioList($model::itemAlias('prophylactic_set'), [
                'prompt' => '',
                'id' => Html::getInputId($model, 'prophylactic_set') . "-id{$model->id}",
                'name' => $model->formName() . "[{$model->id}][prophylactic_set]"
            ]) ?>

            <?= $form->field($model, 'trauma')->radioList($model::itemAlias('trauma'), [
                'prompt' => '',
                'id' => Html::getInputId($model, 'trauma') . "-id{$model->id}",
                'name' => $model->formName() . "[{$model->id}][trauma]"
            ]) ?>

            <?= $form->field($model, 'date_at')->widget(DatePicker::classname(), [
                'options' => [
                    'id' => Html::getInputId($model, 'date_at') . "-id{$model->id}",
                    'name' => $model->formName() . "[{$model->id}][date_at]",
                    'style' => 'width: 90px;',
                    'placeholder' => '0000-00-00',
                    'value' => ($model->date_at != '0000-00-00') ? $model->date_at : date('Y-m-d')
                ],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>

        </fieldset>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <fieldset class="blk-diagnoses-complication sub-block" style="margin-top: 20px;">
            <legend><?php echo Yii::t('app', 'Complications'); ?></legend>

            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => "modal-diagnosis-template-complication-id{$model->id}",
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Complications') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['DiagnosisAdditional_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['DiagnosisAdditional_alert']['type']
                    ],
                    'body' => $models['DiagnosisAdditional_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => "form-diagnosis-template-complication-id{$model->id}",
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'diagnosis_id', [
                    'value' => $model->id,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'diagnosis_id') . "-complication-id{$model->id}",
                    'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_COMPLICATION . "]" . "[{$model->id}][diagnosis_id]"
                ]) ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'type', [
                    'value' => DiagnosisAdditional::TYPE_COMPLICATION,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'type') . "-complication-id{$model->id}",
                    'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_COMPLICATION . "]" . "[{$model->id}][type]"
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'handbook_emc_icd_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['DiagnosisAdditional']->icd) ? '' : $models['DiagnosisAdditional']->icd->name,
                    'options' => [
                        'placeholder' => '',
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'handbook_emc_icd_id') . "-complication-id{$model->id}",
                        'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_COMPLICATION . "]" . "[{$model->id}][handbook_emc_icd_id]"
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?= $form->field($models['DiagnosisAdditional'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea([
                    'rows' => 2,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'description') . "-complication-id{$model->id}",
                    'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_COMPLICATION . "]" . "[{$model->id}][description]"
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['DiagnosisAdditional']->date_at) ? $models['DiagnosisAdditional']->date_at : date('Y-m-d'),
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'date_at') . "-complication-id{$model->id}",
                        'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_COMPLICATION . "]" . "[{$model->id}][date_at]"
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => "#modal-diagnosis-template-complication-id{$model->id}",
                            'data-container' => "#diagnosis-template-complication-id{$model->id}",
                            'data-id' => $model->id,
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => "diagnosis-template-complication-id{$model->id}",
                'linkSelector' => "#diagnosis-template-complication-id{$model->id} a[data-sort], #diagnosis-template-complication-id{$model->id} a[data-page]",
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => "grid-diagnosis-template-complication-id{$model->id}"
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => DiagnosisAdditional::find()->where([
                        'diagnosis_id' => $model->id,
                        'type' => DiagnosisAdditional::TYPE_COMPLICATION,
                    ])->orderBy([
                        'date_at' => SORT_ASC,
                    ]),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'handbook_emc_icd_id',
                        'value' => function ($data) {
                            return (isset($data->icd->name)) ? $data->icd->name : '';
                        },
                        //'headerOptions' => ['width' => '500'],
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/diagnosis/diagnosis-additional/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => "#diagnosis-template-complication-id{$data->diagnosis_id}",
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </fieldset>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <fieldset class="blk-diagnoses-concomitant sub-block">
            <legend><?php echo Yii::t('app', 'Concomitant'); ?></legend>

            <div class="form-horizontal">

                <?php Modal::begin([
                    'id' => "modal-diagnosis-template-concomitant-id{$model->id}",
                    'size' => Modal::SIZE_LARGE,
                    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Concomitant') . '</h4>',
                    'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
                    'options' => [
                        'tabindex' => false // important for Select2 to work properly
                    ],
                ]); ?>

                <?= (isset($models['DiagnosisAdditional_alert'])) ? Alert::widget([
                    'options' => [
                        'class' => 'alert-' . $models['DiagnosisAdditional_alert']['type']
                    ],
                    'body' => $models['DiagnosisAdditional_alert']['body']
                ]) : '' ?>

                <?php Pjax::begin([
                    'id' => "form-diagnosis-template-concomitant-id{$model->id}",
                    'enablePushState' => false,
                    'clientOptions' => [
                        'method' => 'GET'
                    ]
                ]); ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'diagnosis_id', [
                    'value' => $model->id,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'diagnosis_id') . "-concomitant-id{$model->id}",
                    'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_CONCOMITANT . "]" . "[{$model->id}][diagnosis_id]"
                ]) ?>

                <?= Html::activeHiddenInput($models['DiagnosisAdditional'], 'type', [
                    'value' => DiagnosisAdditional::TYPE_CONCOMITANT,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'type') . "-concomitant-id{$model->id}",
                    'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_CONCOMITANT . "]" . "[{$model->id}][type]"
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'handbook_emc_icd_id', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($models['DiagnosisAdditional']->icd) ? '' : $models['DiagnosisAdditional']->icd->name,
                    'options' => [
                        'placeholder' => '',
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'handbook_emc_icd_id') . "-concomitant-id{$model->id}",
                        'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_CONCOMITANT . "]" . "[{$model->id}][handbook_emc_icd_id]"
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>

                <?= $form->field($models['DiagnosisAdditional'], 'description', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->textarea([
                    'rows' => 2,
                    'id' => Html::getInputId($models['DiagnosisAdditional'], 'description') . "-concomitant-id{$model->id}",
                    'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_CONCOMITANT . "]" . "[{$model->id}][description]"
                ]) ?>

                <?= $form->field($models['DiagnosisAdditional'], 'date_at', [
                    'labelOptions' => ['class' => 'control-label col-sm-3'],
                    'inputOptions' => ['class' => 'form-control'],
                    'template' => '{label} <div class="col-sm-9">{input}{error}{hint}</div>'
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($models['DiagnosisAdditional']->date_at) ? $models['DiagnosisAdditional']->date_at : date('Y-m-d'),
                        'id' => Html::getInputId($models['DiagnosisAdditional'], 'date_at') . "-concomitant-id{$model->id}",
                        'name' => $models['DiagnosisAdditional']->formName() . "[" . DiagnosisAdditional::TYPE_CONCOMITANT . "]" . "[{$model->id}][date_at]"
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?= Html::button(Yii::t('app', 'Save'), [
                            'class' => 'btn btn-primary',
                            'data-save' => '',
                            'data-modal' => "#modal-diagnosis-template-concomitant-id{$model->id}",
                            'data-container' => "#diagnosis-template-concomitant-id{$model->id}",
                            'data-id' => $model->id,
                        ]) ?>
                    </div>
                </div>

                <?php Pjax::end(); ?>

                <?php Modal::end(); ?>

            </div>

            <?php Pjax::begin([
                'id' => "diagnosis-template-concomitant-id{$model->id}",
                'linkSelector' => "#diagnosis-template-concomitant-id{$model->id} a[data-sort], #diagnosis-template-concomitant-id{$model->id} a[data-page]",
                'enablePushState' => false,
                'clientOptions' => [
                    'method' => 'GET'
                ]
            ]); ?>

            <?= GridView::widget([
                'export' => false,
                'options' => [
                    'id' => "grid-diagnosis-template-concomitant-id{$model->id}"
                ],
                'dataProvider' => new ActiveDataProvider([
                    'query' => DiagnosisAdditional::find()->where([
                        'diagnosis_id' => $model->id,
                        'type' => DiagnosisAdditional::TYPE_CONCOMITANT,
                    ])->orderBy([
                        'date_at' => SORT_ASC,
                    ]),
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                ]),
                'columns' => [
                    [
                        'attribute' => 'date_at',
                        'headerOptions' => ['width' => '80'],
                    ],
                    [
                        'attribute' => 'handbook_emc_icd_id',
                        'value' => function ($data) {
                            return (isset($data->icd->name)) ? $data->icd->name : '';
                        },
                        //'headerOptions' => ['width' => '500'],
                    ],
                    [
                        'attribute' => 'description',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['width' => '30'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $data, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    Url::toRoute([
                                        '/diagnosis/diagnosis-additional/delete',
                                        'id' => $data->id
                                    ]),
                                    [
                                        'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-container' => "#diagnosis-template-concomitant-id{$data->diagnosis_id}",
                                    ]
                                );
                            }
                        ]
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </fieldset>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <fieldset class="blk-diagnoses-operation">
            <legend><?php echo Html::a(Yii::t('app', 'Operation'), '#'); ?></legend>

            <?= $this->render('_template_operation', [
                'model' => $model,
                'models' => $models,
                'form' => $form,
            ]) ?>

        </fieldset>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <fieldset class="blk-diagnoses-protocol">
            <legend><?php echo Html::a(Yii::t('app', 'Protocols'), '#'); ?></legend>
            <div class="blk-diagnoses-protocol-body">

                <?= $this->render('_template_protocol', [
                    'model' => $model,
                    'models' => $models,
                    'form' => $form,
                ]) ?>

            </div>
        </fieldset>
    </div>
</div>


<?php $this->registerCss("

    fieldset legend {
        font-style: italic;
        font-size: 15px;
    }
    fieldset.sub-block legend {
        margin-bottom: 10px;
    }

    .compacted.control-label {
        line-height: normal;
    }

    .form-horizontal .modal .modal-content .modal-body .modal-row {
        margin-bottom: 10px;
    }

    .form-horizontal .modal .modal-content .modal-body .modal-row .table {
        margin-bottom: 0px;
    }

    .form-horizontal .modal .modal-content .modal-body .modal-row .table tr td {
        padding: 0px;
        border: none;
    }

"); ?>

<?php $this->registerJs("

    // Показати/Заховати
    $(document).on('click', 'fieldset.blk-diagnoses-basic > legend > a', function(event) {
        $(this).closest('.panel-body').find('.blk-diagnoses-complication').parent().parent().toggle();
        $(this).closest('.panel-body').find('.blk-diagnoses-concomitant').parent().parent().toggle();
        event.preventDefault();
    })

"); ?>