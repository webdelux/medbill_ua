<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use backend\modules\handbook\models\Department;
use backend\models\PacientOperation;
?>

<div class="form-horizontal">

    <?php Modal::begin([
        'id' => "modal-diagnosis-template-operation-id{$model->id}",
        'size' => Modal::SIZE_LARGE,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Operation') . '</h4>',
        'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
    ]); ?>

    <?= (isset($models['PacientOperation_alert'])) ? Alert::widget([
        'options' => [
            'class' => 'alert-' . $models['PacientOperation_alert']['type']
        ],
        'body' => $models['PacientOperation_alert']['body']
    ]) : '' ?>

    <?php Pjax::begin([
        'id' => "form-diagnosis-template-operation-id{$model->id}",
        'enablePushState' => false,
        'clientOptions' => [
            'method' => 'GET'
        ]
    ]); ?>

    <?= Html::activeHiddenInput($models['PacientOperation'], 'diagnosis_id', [
        'value' => $model->id,
        'id' => Html::getInputId($models['PacientOperation'], 'diagnosis_id') . "-operation-id{$model->id}",
        'name' => $models['PacientOperation']->formName() . "[{$model->id}][diagnosis_id]"
    ]) ?>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'handbook_emc_surgery_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['PacientOperation']->surgery) ? '' : $models['PacientOperation']->surgery->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['PacientOperation'], 'handbook_emc_surgery_id') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][handbook_emc_surgery_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbookemc/surgery/index']),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'handbook_emc_surgery_note', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textInput([
                'id' => Html::getInputId($models['PacientOperation'], 'handbook_emc_surgery_note') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][handbook_emc_surgery_note]"
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'handbook_emc_icd_id', [
                'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['PacientOperation']->icd) ? '' : $models['PacientOperation']->icd->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['PacientOperation'], 'handbook_emc_icd_id') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][handbook_emc_icd_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbookemc/icd/index']),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'handbook_emc_icd_note', [
                'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textInput([
                'id' => Html::getInputId($models['PacientOperation'], 'handbook_emc_icd_note') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][handbook_emc_icd_note]"
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'start_operation_date', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(DatePicker::classname(), [
                'options' => [
                    'style' => 'width: 90px;',
                    'placeholder' => '0000-00-00',
                    'value' => ($models['PacientOperation']->start_operation_date) ? $models['PacientOperation']->start_operation_date : date('Y-m-d'),
                    'id' => Html::getInputId($models['PacientOperation'], 'start_operation_date') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][start_operation_date]"
                ],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'start_operation_time', [
                'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(TimePicker::classname(), [
                'options' => [
                    'placeholder' => '00:00',
                    'value' => ($models['PacientOperation']->start_operation_time) ? $models['PacientOperation']->start_operation_time : date('H:i'),
                    'id' => Html::getInputId($models['PacientOperation'], 'start_operation_time') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][start_operation_time]"
                ],
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'template' => false
                ]
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'duration_operation_hour', [
                'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textInput([
                'style' => 'width: 60px;',
                'placeholder' => '0',
                'value' => ($models['PacientOperation']->duration_operation_hour) ? $models['PacientOperation']->duration_operation_hour : 0,
                'id' => Html::getInputId($models['PacientOperation'], 'duration_operation_hour') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][duration_operation_hour]"
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'duration_operation_minute', [
                'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textInput([
                'style' => 'width: 60px;',
                'placeholder' => '0',
                'value' => ($models['PacientOperation']->duration_operation_minute) ? $models['PacientOperation']->duration_operation_minute : 0,
                'id' => Html::getInputId($models['PacientOperation'], 'duration_operation_minute') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][duration_operation_minute]"
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'surgeon_user_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['PacientOperation']->surgeonUser) ? '' : $models['PacientOperation']->surgeonUser->profile->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['PacientOperation'], 'surgeon_user_id') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][surgeon_user_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/user-viewer/index', 'results' => true, 'speciality' => 'surgeons']),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'department_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['PacientOperation']->department) ? '' : $models['PacientOperation']->department->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['PacientOperation'], 'department_id') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][department_id]"
                ],
                'data' => (new Department)->treeList((new Department)->treeData()),
                'pluginOptions' => [
                    'allowClear' => true,
                    /*
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbook/department/index']),
                        'dataType' => 'json',
                    ]
                    */
                ],
            ]); ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'anesthetist_user_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['PacientOperation']->anesthetistUser) ? '' : $models['PacientOperation']->anesthetistUser->profile->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['PacientOperation'], 'anesthetist_user_id') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][anesthetist_user_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/user-viewer/index', 'results' => true, 'speciality' => 'anesthetists']),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'anesthesia_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList($models['PacientOperation']::itemAlias('anesthesia_id'), [
                'prompt' => '',
                'id' => Html::getInputId($models['PacientOperation'], 'anesthesia_id') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][anesthesia_id]"
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'handbook_emc_complication_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['PacientOperation']->complication) ? '' : $models['PacientOperation']->complication->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['PacientOperation'], 'handbook_emc_complication_id') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][handbook_emc_complication_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbookemc/complication/index']),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'handbook_emc_complication_note', [
                'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textInput([
                'id' => Html::getInputId($models['PacientOperation'], 'handbook_emc_complication_note') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][handbook_emc_complication_note]"
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'type_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList($models['PacientOperation']::itemAlias('type_id'), [
                'prompt' => '',
                'id' => Html::getInputId($models['PacientOperation'], 'type_id') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][type_id]"
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'result_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList($models['PacientOperation']::itemAlias('result_id'), [
                'prompt' => '',
                'id' => Html::getInputId($models['PacientOperation'], 'result_id') . "-operation-id{$model->id}",
                'name' => $models['PacientOperation']->formName() . "[{$model->id}][result_id]"
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['PacientOperation'], 'date_at', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(DatePicker::classname(), [
                'options' => [
                    'style' => 'width: 90px;',
                    'placeholder' => '0000-00-00',
                    'value' => ($models['PacientOperation']->date_at) ? $models['PacientOperation']->date_at : date('Y-m-d'),
                    'id' => Html::getInputId($models['PacientOperation'], 'date_at') . "-operation-id{$model->id}",
                    'name' => $models['PacientOperation']->formName() . "[{$model->id}][date_at]"
                ],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div>

    <hr/>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4">
                    <?= Html::button(Yii::t('app', 'Save'), [
                        'class' => 'btn btn-primary',
                        'data-save' => '',
                        'data-modal' => "#modal-diagnosis-template-operation-id{$model->id}",
                        'data-container' => "#diagnosis-template-operation-id{$model->id}",
                        'data-id' => $model->id,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <?php Pjax::end(); ?>

    <?php Modal::end(); ?>

</div>

<?php Pjax::begin([
    'id' => "diagnosis-template-operation-id{$model->id}",
    'linkSelector' => "#diagnosis-template-operation-id{$model->id} a[data-sort], #diagnosis-template-operation-id{$model->id} a[data-page]",
    'enablePushState' => false,
    'clientOptions' => [
        'method' => 'GET'
    ]
]); ?>

<?= GridView::widget([
    'export' => false,
    'options' => [
        'id' => "grid-diagnosis-template-operation-id{$model->id}",
        'class' => 'grid-diagnosis-template-operation'
    ],
    'dataProvider' => new ActiveDataProvider([
        'query' => PacientOperation::find()->where([
            'diagnosis_id' => $model->id,
        ])->orderBy([
            'date_at' => SORT_DESC,
        ]),
        'pagination' => [
            'pageSize' => 5,
        ],
    ]),
    'columns' => [
        [
            'attribute' => 'date_at',
            'headerOptions' => ['width' => '80'],
        ],
        [
            'attribute' => 'handbook_emc_surgery_id',
            'value' => function ($data) {
                return (isset($data->surgery->name)) ? $data->surgery->name : '';
            },
        ],
        [
            'attribute' => 'handbook_emc_icd_id',
            'value' => function ($data) {
                return (isset($data->icd->name)) ? $data->icd->name : '';
            },
        ],
        [
            'attribute' => 'type_id',
            'value' => function ($data) {
                return ($data->type_id) ? PacientOperation::itemAlias('type_id', $data->type_id) : '';
            },
        ],
        [
            'attribute' => 'result_id',
            'value' => function ($data) {
                return ($data->result_id) ? PacientOperation::itemAlias('result_id', $data->result_id) : '';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('app', 'Actions'),
            'headerOptions' => ['width' => '65'],
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $data, $key) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                        Url::toRoute([
                            '/pacient-operation/view',
                            'id' => $data->id
                        ]),
                        [
                            'title' => Yii::t('app', 'View'),
                            'data-target' => "#modal-diagnosis-template-operation-view",
                            'data-toggle' => "modal",
                        ]
                    );
                },
                'update' => function ($url, $data, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                        Url::toRoute([
                            '/pacient-operation/update',
                            'id' => $data->id
                        ]),
                        [
                            'title' => Yii::t('app', 'Update'),
                            'data-target' => "#modal-diagnosis-template-operation-update",
                            'data-toggle' => "modal",
                            'data-token' => $data->id . $data->diagnosis_id,
                            'data-id' => $data->diagnosis_id,
                            'data-container' => '#diagnosis-template-operation-id' . $data->diagnosis_id
                        ]
                    );
                },
                'delete' => function ($url, $data, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        Url::toRoute([
                            '/pacient-operation/delete',
                            'id' => $data->id
                        ]),
                        [
                            'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'title' => Yii::t('app', 'Delete'),
                            'data-container' => "#diagnosis-template-operation-id{$data->diagnosis_id}",
                        ]
                    );
                }
            ]
        ],
    ],
]); ?>

<?php Pjax::end(); ?>