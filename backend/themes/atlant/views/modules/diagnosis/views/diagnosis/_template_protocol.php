<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use backend\models\Protocol;
use backend\modules\handbook\models\Department;
use backend\modules\handbookemc\models\Category;
use backend\models\User;
?>

<div class="form-horizontal">

    <?php Modal::begin([
        'id' => "modal-diagnosis-template-protocol-id{$model->id}",
        'size' => Modal::SIZE_LARGE,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
        'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
    ]); ?>

    <?= (isset($models['Protocol_alert'])) ? Alert::widget([
        'options' => [
            'class' => 'alert-' . $models['Protocol_alert']['type']
        ],
        'body' => $models['Protocol_alert']['body']
    ]) : '' ?>

    <?php Pjax::begin([
        'id' => "form-diagnosis-template-protocol-id{$model->id}",
        'enablePushState' => false,
        'clientOptions' => [
            'method' => 'GET'
        ]
    ]); ?>

    <?= Html::activeHiddenInput($models['Protocol'], 'diagnosis_id', [
        'value' => $model->id,
        'id' => Html::getInputId($models['Protocol'], 'diagnosis_id') . "-id{$model->id}",
        'name' => $models['Protocol']->formName() . "[{$model->id}][diagnosis_id]"
    ]) ?>

    <div class="row modal-row">
        <div class="col-md-6">

            <?= Html::activeHiddenInput($models['Protocol'], 'pacient_id', [
                'value' => $model->pacient_id,
                'id' => null,
                'name' => $models['Protocol']->formName() . "[{$model->id}][pacient_id]"
            ]) ?>

            <?php if (empty($models['Protocol']->pacient)): ?>

                <?php $models['Protocol']->pacient_id = $model->pacient_id; ?>

            <?php endif; ?>

            <?= $form->field($models['Protocol'], 'pacient_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['Protocol']->pacient) ? $model->pacient->fullName : $models['Protocol']->pacient->fullName,
                'disabled' => true,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['Protocol'], 'pacient_id') . "-id{$model->id}",
                    'name' => $models['Protocol']->formName() . "[{$model->id}][pacient_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/pacient/index', 'results' => true]),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'department_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList((new Department)->getTreeListByUserId(), [
                'prompt' => '',
                'id' => Html::getInputId($models['Protocol'], 'department_id') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][department_id]"
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'protocol_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['Protocol']->protocol) ? '' : $models['Protocol']->protocol->name,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['Protocol'], 'protocol_id') . "-id{$model->id}",
                    'name' => $models['Protocol']->formName() . "[{$model->id}][protocol_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbookemc/protocol/index', 'results' => true]),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-6">

            <?php
            // Список всіх користувачів з відділень доступ до яких має поточний користувач
            $doctors = User::getListByDepartments(array_keys((new Department)->getListByUserId()));

            // Chained Selects Plugin
            $doctorsOptions = [];
            foreach ($doctors as $key => $value)
                $doctorsOptions[$key] = ['class' => implode(' ', array_keys((new Department)->getListParentsByUserId($key)))];
            ?>

            <?= $form->field($models['Protocol'], 'doctor_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList($doctors, [
                'prompt' => '',
                'id' => Html::getInputId($models['Protocol'], 'doctor_id') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][doctor_id]",
                // Chained Selects Plugin
                'options' => $doctorsOptions
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'total', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textInput([
                'id' => Html::getInputId($models['Protocol'], 'total') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][total]",
                'maxlength' => true,
                'style' => 'width: 100px;'
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'refferal_id', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'initValueText' => empty($models['Protocol']->refferal) ? '' : $models['Protocol']->refferal->pib,
                'options' => [
                    'placeholder' => '',
                    'id' => Html::getInputId($models['Protocol'], 'refferal_id') . "-id{$model->id}",
                    'name' => $models['Protocol']->formName() . "[{$model->id}][refferal_id]"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbook/refferal/index', 'results' => true]),
                        'dataType' => 'json',
                    ]
                ],
            ]); ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'discount', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textInput([
                'id' => Html::getInputId($models['Protocol'], 'discount') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][discount]",
                'maxlength' => true,
                'style' => 'width: 100px;'
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'type', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList($models['Protocol']::itemAlias('type'), [
                'id' => Html::getInputId($models['Protocol'], 'type') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][type]",
                'prompt' => ''
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'protocol_date', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label}'
                . '<div class="col-sm-8">'
                    . '<div class="input-group">'
                        . '{input}'
                        . '<span class="input-group-btn">'
                            . Html::button('<span class="fa fa-list"></span>', [
                                'class' => 'btn btn-primary btn-sm',
                                'data-toggle' => 'modal',
                                'data-target' => "#modal-diagnosis-template-protocol-calendar-id{$model->id}",
                            ])
                        . '</span>'
                    . '</div>'
                    . '{error}{hint}'
                . '</div>'
            ])->widget(DateTimePicker::classname(), [
                'options' => [
                    'value' => ($models['Protocol']->protocol_date) ? $models['Protocol']->protocol_date : date('Y-m-d H:i'),
                    'id' => Html::getInputId($models['Protocol'], 'protocol_date') . "-id{$model->id}",
                    'name' => $models['Protocol']->formName() . "[{$model->id}][protocol_date]"
                ],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'place', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList($models['Protocol']::itemAlias('place'), [
                'id' => Html::getInputId($models['Protocol'], 'place') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][place]",
                'prompt' => ''
            ]) ?>
        </div>
    </div>

    <div class="row modal-row">
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'description', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->textarea([
                'id' => Html::getInputId($models['Protocol'], 'description') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][description]",
                'rows' => 2
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($models['Protocol'], 'status', [
                'labelOptions' => ['class' => 'control-label col-sm-4'],
                'inputOptions' => ['class' => 'form-control'],
                'template' => '{label} <div class="col-sm-8">{input}{error}{hint}</div>'
            ])->dropDownList($models['Protocol']::itemAlias('status'), [
                'id' => Html::getInputId($models['Protocol'], 'status') . "-id{$model->id}",
                'name' => $models['Protocol']->formName() . "[{$model->id}][status]",
                'prompt' => ''
            ]) ?>
        </div>
    </div>

    <hr/>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4">
                    <?= Html::button(Yii::t('app', 'Save'), [
                        'class' => 'btn btn-primary',
                        'data-save' => '',
                        'data-modal' => "#modal-diagnosis-template-protocol-id{$model->id}",
                        'data-container' => "#diagnosis-template-protocol-id{$model->id}",
                        'data-id' => $model->id,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <?php Pjax::end(); ?>

    <?php Modal::end(); ?>


    <?php Modal::begin([
        'id' => "modal-diagnosis-template-protocol-calendar-id{$model->id}",
        'size' => Modal::SIZE_LARGE,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Protocol') . '</h4>',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
    ]); ?>

    <div class="modal-fullcalendar">

        <div class="well well-sm" id="modal-filter-diagnosis-template-protocol-calendar-id<?= $model->id ?>">
            <div class="row">
                <div class="col-md-4">
                    <?php echo Html::activeLabel($models['Protocol'], 'doctor_id'); ?>
                    <?php echo Html::activeDropDownList($models['Protocol'], 'doctor_id', $doctors, [
                        'id' => Html::getInputId($models['Protocol'], 'doctor_id') . "-fc{$model->id}",
                        'class' => 'form-control',
                        'prompt' => '',
                        // Chained Selects Plugin
                        'options' => $doctorsOptions
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::activeLabel($models['Protocol'], 'department_id'); ?>
                    <?php echo Html::activeDropDownList($models['Protocol'], 'department_id', (new Department)->getTreeListByUserId(), [
                        'id' => Html::getInputId($models['Protocol'], 'department_id') . "-fc{$model->id}",
                        'class' => 'form-control',
                        'prompt' => '',
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::activeLabel($models['Protocol'], 'category_id'); ?>
                    <?php echo Html::activeDropDownList($models['Protocol'], 'category_id', (new Category)->treeList((new Category)->treeData()), [
                        'id' => Html::getInputId($models['Protocol'], 'category_id') . "-fc{$model->id}",
                        'class' => 'form-control',
                        'prompt' => '',
                    ]); ?>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <?= Html::button(Yii::t('app', 'Refresh'), [
                        'class' => 'btn btn-default btn-block',
                        'id' => Html::getInputId($models['Protocol'], 'id') . "-btn-refresh-fc{$model->id}",
                    ]) ?>
                </div>
            </div>
        </div>

        <?php
        // Увага! При зміні/доповненні будь якого з параметрів в віджеті
        // потрібно і змінити ці параметри в функції переініцілізації календара "initWidgetProtocol()"
        echo \yii2fullcalendar\yii2fullcalendar::widget(array(
            'id' => "diagnosis-template-protocol-calendar-id{$model->id}",
            'options' => [
                'lang' => substr(Yii::$app->language, 0, 2),
            ],
            'clientOptions' => [
                'allDaySlot' => false,
                'firstDay' => 1,
                'defaultView' => 'agendaDay',
                'minTime' => '08:00',
                'maxTime' => '22:00',
                'slotDuration' => '00:20:00',
                'defaultTimedEventDuration' => '00:20:00',
                'slotLabelFormat' => 'HH:mm',
                'timezone' => Yii::$app->timeZone,
                'height' => 'auto',
                'contentHeight' => 'auto',
                'fixedWeekCount' => false,
                'displayEventTime' => false,
                'eventRender' => new \yii\web\JsExpression('function(event, element, view) {
                    element.popover({
                        title: event.title,
                        placement: "bottom",
                        trigger: "hover",
                        content: event.description,
                        container: "body"
                    });
                }'),
                'eventClick' => new \yii\web\JsExpression('function(event, jsEvent, view) {
                    if (event.url) {
                        window.open(event.url);
                        return false;
                    }
                }'),
                'dayClick' => new \yii\web\JsExpression('function(date, jsEvent, view, resourceObj) {
                    setValueFieldByProtocolDate(' . $model->id . ', date);
                }'),
            ],
            'header' => [
                'center' => 'title',
                'left' => 'prev,next today',
                'right' => 'month,agendaWeek,agendaDay',
            ],
            //'ajaxEvents' => Url::to(['/protocol/calendar'])
            'ajaxEvents' => new \yii\web\JsExpression('{
                url: "' . Url::to(['/protocol/calendar']) . '",
                data: function() {
                    return {
                        doctor_id: $("#protocol-doctor_id-fc' . $model->id . '").val(),
                        department_id: $("#protocol-department_id-fc' . $model->id . '").val(),
                        category_id: $("#protocol-category_id-fc' . $model->id . '").val()
                    };
                }
            }'),
        )); ?>

    </div>

    <?php Modal::end(); ?>

</div>

<?php Pjax::begin([
    'id' => "diagnosis-template-protocol-id{$model->id}",
    'linkSelector' => "#diagnosis-template-protocol-id{$model->id} a[data-sort], #diagnosis-template-protocol-id{$model->id} a[data-page]",
    'enablePushState' => false,
    'clientOptions' => [
        'method' => 'GET'
    ]
]); ?>

<?= GridView::widget([
    'export' => false,
    'options' => [
        'id' => "grid-diagnosis-template-protocol-id{$model->id}",
        'class' => 'grid-diagnosis-template-protocol'
    ],
    'dataProvider' => new ActiveDataProvider([
        'query' => Protocol::find()->where([
            'diagnosis_id' => $model->id,
            'pacient_id' => $model->pacient_id,
        ])->orderBy([
            'protocol_date' => SORT_DESC,
        ]),
        'pagination' => [
            'pageSize' => 5,
        ],
    ]),
    'columns' => [
        [
            'attribute' => 'id',
            'headerOptions' => ['width' => '80'],
        ],
        [
            'attribute' => 'protocol_id',
            'value' => function ($data) {
                return (isset($data->protocol->name)) ? $data->protocol->name : '';
            },
        ],
        [
            'attribute' => 'department_id',
            'value' => function ($data) {
                return (isset($data->department->name))
                    ? (null !== $data->department->parentsName($data->department->id, 3))
                        ? '(' . $data->department->parentsName($data->department->id, 3) . ') ' . $data->department->name
                        : $data->department->name
                    : '';
            },
        ],
        [
            'attribute' => 'doctor_id',
            'value' => function ($data) {
                return (isset($data->doctor->name)) ? $data->doctor->name : '';
            },
        ],
        [
            'attribute' => 'category_id',
            'value' => function ($data) {
                return (isset($data->category->name))
                    ? (null !== $data->category->parentsName($data->category->id, 3))
                        ? '(' . $data->category->parentsName($data->category->id, 3) . ') ' . $data->category->name
                        : $data->category->name
                    : '';
            },
        ],
        [
            'attribute' => 'refferal_id',
            'value' => function ($data) {
                return (isset($data->refferal->pib))
                    ? (null !== $data->refferal->category->parentsName($data->refferal->category_id, 3))
                        ? '(' . $data->refferal->category->parentsName($data->refferal->category_id, 3) . ') ' . $data->refferal->pib
                        : $data->refferal->pib
                    : '';
            },
        ],
        [
            'attribute' => 'protocol_date',
            'format' => ['date', 'dd MMMM Y'],
        ],
        [
            'attribute' => 'status',
            'value' => function ($data) {
                return $data::itemAlias('status', ($data->status) ? $data->status : '');
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('app', 'Actions'),
            'headerOptions' => ['width' => '65'],
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $data, $key) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                        Url::toRoute([
                            '/protocol/view',
                            'id' => $data->id
                        ]),
                        [
                            'title' => Yii::t('app', 'View'),
                            'data-target' => "#modal-diagnosis-template-protocol-view",
                            'data-toggle' => "modal",
                        ]
                    );
                },
                'update' => function ($url, $data, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                        Url::toRoute([
                            '/protocol/update',
                            'id' => $data->id
                        ]),
                        [
                            'title' => Yii::t('app', 'Update'),
                            'data-target' => "#modal-diagnosis-template-protocol-update",
                            'data-toggle' => "modal",
                            'data-token' => $data->id . $data->diagnosis_id,
                            'data-id' => $data->diagnosis_id,
                            'data-container' => '#diagnosis-template-protocol-id' . $data->diagnosis_id
                        ]
                    );
                },
                'delete' => function ($url, $data, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        Url::toRoute([
                            '/protocol/delete',
                            'id' => $data->id
                        ]),
                        [
                            'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'title' => Yii::t('app', 'Delete'),
                            'data-container' => "#diagnosis-template-protocol-id{$data->diagnosis_id}",
                        ]
                    );
                }
            ]
        ],
    ],
]); ?>

<?php Pjax::end(); ?>


<?php $this->registerJs("

    initWidgetFullCalendar({$model->id});

"); ?>