<?php

use yii\helpers\Url;

$this->title = Yii::t('app', 'Diagnoses');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-2">
        <a class="tile tile-default" href="<?= Url::to(['/diagnosis/default/index']) ?>">
            <?= Yii::t('app', 'Diagnoses') ?><br/>
            <span class="fa fa-list"></span>
        </a>
    </div>
</div>