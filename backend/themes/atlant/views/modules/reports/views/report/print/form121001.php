<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\emc\models\PathologicalExamination */

?>

<?php
    $cache = Yii::$app->cache;
    $val = $cache->get($name);

    if ($val === false)
    {
        return;
    }
    $val = $val[0];
?>

<table style="width: 100%;" class="_form066o_header">

    <tr>
        <td>
            Міністерство охорони здоров'я України
        </td>

        <td style="text-align: right">
            Код форми за ЗКУД: __ __ __ __ __ __ __ __
        </td>
    </tr>

    <tr>
        <td>
            &nbsp;
        </td>

        <td style="text-align: right">
            Код установи за ЗКПО:
            <?php echo isset($val['common']['edrpou']) ? $val['common']['edrpou'] : ''; ?>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>

    <tr>
        <td>
            Найменування закладу:
            <?php echo isset($val['common']['name']) ? $val['common']['name'] : ''; ?>,
        </td>

        <td style="text-align: right">
            МЕДИЧНА ДОКУМЕНТАЦІЯ <br>
            ФОРМА №12 <br>
            Затевердженно наказом МОЗ України <br>
            10.07.2007р. №378
        </td>
    </tr>

    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center">
            ЗВІТ <br>
            про захворювання, зареєстровані у хворих, які проживають у районі <br>
            обслуговування лікувально-профілактичного закладу, за період <br>
            <?php echo $val['from'] . ' - ' . $val['to']; ?>
        </td>
    </tr>



    <tr>
        <td colspan="2" style="padding-left: 2em">
            <br>
            1. Діти віком 0-14 років включно
        </td>
    </tr>

    <tr>
        <td colspan="2">
            Таблиця 1001
        </td>
    </tr>
</table>


<table class="t1000" style="width: 100%;">
    <tr>
        <th  style="text-align: center">
            Кількість хворих, які перебували під диспансерним наглядом на кінець звітного року із захворюваннями на:
        </th>


        <th  style="text-align: center">
            Шифр (МКХ-10)
        </th>

        <th  style="text-align: center">
            Усього
        </th>

        <th  style="text-align: center">
            з них дівчатка
        </th>

    </tr>


    <!--    Далі йде таблична частина з даними-->
    <tr>
        <td>
            Злоякісні новоутворення
        </td>

        <td style="text-align: center">
            C00-C97
        </td>

        <td style="text-align: center">
            <?= $val['val10'][0] > 0 ? $val['val10'][0] : ''; ?>
        </td>

        <td style="text-align: center">
            <?= $val['val10'][0] > 0 ? $val['val10'][0] - $val['val10'][1] : ''; ?>
        </td>

    </tr>

    <tr>
        <td>
            Активний туберкульоз
        </td>

        <td style="text-align: center">
            А15-A19
        </td>

        <td style="text-align: center">
            <?= $val['val20'][0] > 0 ? $val['val20'][0] : ''; ?>
        </td>

        <td style="text-align: center">
            <?= $val['val20'][0] > 0 ? $val['val20'][0] - $val['val20'][1] : ''; ?>
        </td>
    </tr>

    <tr>
        <td>
            з них: активний туберкульоз легенів
        </td>

        <td style="text-align: center">
            A15.0-3, А16.0-2, A19
        </td>

        <td style="text-align: center">
            <?= $val['val30'][0] > 0 ? $val['val30'][0] : ''; ?>
        </td>

        <td style="text-align: center">
            <?= $val['val30'][0] > 0 ? $val['val30'][0] - $val['val30'][1] : ''; ?>
        </td>
    </tr>

    <tr>
        <td>
            Сифіліс - усі форми
        </td>

        <td style="text-align: center">
            А50-А53
        </td>

        <td style="text-align: center">
            <?= $val['val40'][0] > 0 ? $val['val40'][0] : ''; ?>
        </td>

        <td style="text-align: center">
            <?= $val['val40'][0] > 0 ? $val['val40'][0] - $val['val40'][1] : ''; ?>
        </td>
    </tr>

</table>

<br>


<table style="width: 100%;">
    <tr>
        <td colspan="2" style="text-align: right">
            Підпис лікаря: ________________________________
        </td>
    </tr>

</table>