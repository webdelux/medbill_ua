<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Diagnosis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incapacity-index">

<?php //  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'options' => [
            'id' => 'diagnosis-grid'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id',
            //'parent_id',
            [
                'label' => Yii::t('app', 'Pacient'),
                'attribute' => 'pacient_id',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data)
                {
                    return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
                },
            ],
            [
                'label' => Yii::t('app', 'Diagnosis'),
                'attribute' => 'handbook_emc_icd_id',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data)
                {
                    return (isset($data->icd->name)) ? $data->icd->name : '';
                },
            ],
            'date_at',
            'type',
            'first_set',
            'prophylactic_set',
            // 'description',
            // 'user_id',
            // 'updated_user_id',
            // 'created_at',
            // 'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '100'],
                'template' => '{view} {update} {delete} {link}',
            ],
        ],
    ]);
    ?>


</div>