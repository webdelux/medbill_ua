<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use backend\modules\handbook\models\Department;

/* @var $this yii\web\View */
/* @var $model backend\modules\diagnosis\models\DiagnosisSearch */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Form12');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="protocol-search">

    <?php $report = Yii::$app->request->get('report'); ?>

    <?php
    $form = ActiveForm::begin([
                'id' => 'form-search',
                'layout' => 'horizontal',
                'method' => 'get',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'action' => ['index', 'report' => $report, 'done' => true],
                'options' => ['target' => '_blank']
    ]);
    ?>
        <?= Html::hiddenInput('report_data', '',['id' => 'report-data']); ?>

        <div class="form-group" >
            <?=
                    $form->field($modelForm, 'from')
                    ->widget(DatePicker::className(), [
                        'language' => 'uk',
                        'value' => date('Y-m-d'),
                        'pluginOptions' =>
                        [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                            'minViewMode' => 1,
                        ],
                        'pluginEvents' =>
                        [
                            'changeDate' => 'function (e){
                                var date = new Date(e.date);

                                $("#diagnosisreport-to-kvdate").kvDatepicker(
                                     "setStartDate", date
                                    );

                             }',
                        ]
                    ])
            ?>
        </div>

        <div class="form-group" >
            <?=
                $form->field($modelForm, 'to')
                ->widget(DatePicker::className(), [
                    'language' => 'uk',
                    'value' => date('Y-m-d'),
                    'pluginOptions' =>
                    [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                        'minViewMode' => 1,
                    ],
                ])
            ?>

        </div>

        <div class="form-group">
            <?=
            $form->field($modelForm, 'department')->dropDownList((new Department)->getTreeListByUserId(), ['prompt' => ''])
            ?>
        </div>


        <div class="row">
            <div class="col-md-3">
                <?= Html::a(Yii::t('app', '12t1000'), ['index'], ['class' => 'form12', 'id' => 121000]) ?>
            </div>

            <div class="col-md-3">
                <?= Html::a(Yii::t('app', '12t1001'), ['index'], ['class' => 'form12', 'id' => 121001]) ?>
            </div>

            <div class="col-md-3">

            </div>

            <div class="col-md-3">

            </div>
        </div>



    <?php ActiveForm::end(); ?>

</div>


<?php $this->registerJs("
    var flag = false;

    $('#diagnosisreport-to-kvdate').change(function (e){
        var date = new Date(e.target.value);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        if (flag)
        {
            return;
        }

        flag = true;

        $('#diagnosisreport-to-kvdate').kvDatepicker('setDate', lastDay);

        flag = false;

        });

    $('.form12').on('click', function (e){
        e.preventDefault();
        $('#report-data').val(this.id);
        $('#form-search').submit();
    })
");?>



