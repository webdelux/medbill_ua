<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\emc\models\PathologicalExamination */

?>

<?php
    $cache = Yii::$app->cache;
    $val = $cache->get($name);

    if ($val === false)
    {
        return;
    }

    $val = $val[0];
?>

<table style="width: 100%;" class="_form066o_header">

    <tr>
        <td>
            Міністерство охорони здоров'я України
        </td>

        <td style="text-align: right">
            Код форми за ЗКУД: __ __ __ __ __ __ __ __
        </td>
    </tr>

    <tr>
        <td>
            &nbsp;
        </td>

        <td style="text-align: right">
            Код установи за ЗКПО:
            <?php echo isset($val['common']['edrpou']) ? $val['common']['edrpou'] : ''; ?>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>

    <tr>
        <td>
            Найменування закладу:
            <?php echo isset($val['common']['name']) ? $val['common']['name'] : ''; ?>,
        </td>

        <td style="text-align: right">
            МЕДИЧНА ДОКУМЕНТАЦІЯ <br>
            ФОРМА №12 <br>
            Затевердженно наказом МОЗ України <br>
            10.07.2007р. №378
        </td>
    </tr>

    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>

    <tr>
        <td colspan="2" style="text-align: center">
            ЗВІТ <br>
            про захворювання, зареєстровані у хворих, які проживають у районі <br>
            обслуговування лікувально-профілактичного закладу, за період <br>
            <?php echo $val['from'] . ' - ' . $val['to']; ?>
        </td>
    </tr>



    <tr>
        <td colspan="2" style="padding-left: 2em">
            <br>
            1. Діти віком 0-14 років включно
        </td>
    </tr>

    <tr>
        <td colspan="2">
            Таблиця 1000
        </td>
    </tr>
</table>


<table class="t1000" style="width: 100%;">
    <tr>
        <th rowspan="2" style="text-align: center">
            Найменування класів і окремих хвороб
        </th>

        <th rowspan="2" style="text-align: center">
            Номер рядка
        </th>

        <th rowspan="2" style="text-align: center">
            Шифр (МКХ-10)
        </th>

        <th colspan="4" style="text-align: center">
            Зареєстровано захворюваноь - усього у дітей віком (включно)
        </th>

        <th colspan="4" style="text-align: center">
            у тому числі вперше у житті дітей віком (включно)
        </th>

        <th rowspan="2" style="text-align: center">
            Перебувають під диспансерним наглядом на кінець звітного року
        </th>
    </tr>

    <tr>
        <th style="text-align: center">
            0-14
            років
        </th>

        <th style="text-align: center">
            з них у
            хлопчиків
        </th>

        <th style="text-align: center">
            0-6
            років
        </th>

        <th style="text-align: center">
            7-14
            років
        </th>

        <th style="text-align: center">
            0-14
            років
        </th>

        <th style="text-align: center">
            з них у
            хлопчиків
        </th>

        <th style="text-align: center">
            0-6
            років
        </th>

        <th style="text-align: center">
            7-14
            років
        </th>
    </tr>

    <tr>
        <td style="text-align: center">
            A
        </td>

        <td style="text-align: center">
            Б
        </td>

        <td style="text-align: center">
            В
        </td>

        <td style="text-align: center">
            1
        </td>

        <td style="text-align: center">
            2
        </td>

        <td style="text-align: center">
            3
        </td>

        <td style="text-align: center">
            4
        </td>

        <td style="text-align: center">
            5
        </td>

        <td style="text-align: center">
            6
        </td>

        <td style="text-align: center">
            7
        </td>

        <td style="text-align: center">
            8
        </td>

        <td style="text-align: center">
            9
        </td>
    </tr>

    <!--    Далі йде таблична частина з даними-->
    <tr>
        <td>
            Усі хвороби
        </td>

        <td style="text-align: center">
            1,0
        </td>

        <td style="text-align: center">
            А00-T98
        </td>

        <?php
            foreach ($val['val10'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі деякі інфекційні та
            паразитарні хвороби
        </td>

        <td style="text-align: center">
            2,0
        </td>

        <td style="text-align: center">
            А00-В99
        </td>

        <?php
            foreach ($val['val20'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            з них: хронічний вірусний гепатит B
        </td>

        <td style="text-align: center">
            2,1
        </td>

        <td style="text-align: center">
            B18.0.1
        </td>

        <?php
            foreach ($val['val21'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічний вірусний гепатит C
        </td>

        <td style="text-align: center">
            2,2
        </td>

        <td style="text-align: center">
            B18.2
        </td>

        <?php
            foreach ($val['val22'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Новоутворення
        </td>

        <td style="text-align: center">
            3,0
        </td>

        <td style="text-align: center">
            С00-D48
        </td>

        <?php
            foreach ($val['val30'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі : доброякісні новоутворення шкіри
        </td>

        <td style="text-align: center">
            3,2
        </td>

        <td style="text-align: center">
            D22, D23, D28.0, D29.0,2,4
        </td>

        <?php
            foreach ($val['val32'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            яєчника
        </td>

        <td style="text-align: center">
            3,4
        </td>

        <td style="text-align: center">
            D27
        </td>

        <?php
            foreach ($val['val34'] as $key => $value)
            {
                echo '<td style="text-align: center">';
                if ($key == 1 || $key == 5)
                {
                    echo 'x';
                }
                else
                {
                    echo $value > 0 ? $value : '';
                    echo '</td>';
                }
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби крові, кровотворних органів та окремі порушення із залученням імунного механізму
        </td>

        <td style="text-align: center">
            4,0
        </td>

        <td style="text-align: center">
            D50-D89
        </td>

        <?php
            foreach ($val['val40'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: анемії
        </td>

        <td style="text-align: center">
            4,1
        </td>

        <td style="text-align: center">
            D50-D64
        </td>

        <?php
            foreach ($val['val41'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            з них залізодефіцитні
        </td>

        <td style="text-align: center">
            4,2
        </td>

        <td style="text-align: center">
            D50
        </td>

        <?php
            foreach ($val['val42'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            апластичні
        </td>

        <td style="text-align: center">
            4,3
        </td>

        <td style="text-align: center">
            D60, D61
        </td>

        <?php
            foreach ($val['val43'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            гемофілія
        </td>

        <td style="text-align: center">
            4,4
        </td>

        <td style="text-align: center">
            D66, D67, D68.0.1.4
        </td>

        <?php
            foreach ($val['val44'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            імунодефіцити (всі форми)
        </td>

        <td style="text-align: center">
            4,6
        </td>

        <td style="text-align: center">
            D80 - D84
        </td>

        <?php
            foreach ($val['val46'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            саркоїдоз
        </td>

        <td style="text-align: center">
            4,7
        </td>

        <td style="text-align: center">
            D86
        </td>

        <?php
            foreach ($val['val47'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            інші порушення із залученням імунного механізму, не класифіковані в інших рубриках
        </td>

        <td style="text-align: center">
            4,8
        </td>

        <td style="text-align: center">
            D89
        </td>

        <?php
            foreach ($val['val48'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби ендокринної системи, розладу харчування, порушення обміну речовин
        </td>

        <td style="text-align: center">
            5,0
        </td>

        <td style="text-align: center">
            E00 - E90
        </td>

        <?php
            foreach ($val['val50'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: дифузний зоб I ступеня
        </td>

        <td style="text-align: center">
            5,1
        </td>

        <td style="text-align: center">
            Е01.0, Е04.0,
        </td>

        <?php
            foreach ($val['val51'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            дифузний зоб II-III ступенів
        </td>

        <td style="text-align: center">
            5,2
        </td>

        <td style="text-align: center">
            Е01.0, Е04.0,
        </td>

        <?php
            foreach ($val['val52'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            набутий гіпотиреоз та інші форми гіпотиреозу
        </td>

        <td style="text-align: center">
            5,3
        </td>

        <td style="text-align: center">
            Е01.8, Е03
        </td>

        <?php
            foreach ($val['val53'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            вузловий зоб (ендемічний і нетоксичний)
        </td>

        <td style="text-align: center">
            5,4
        </td>

        <td style="text-align: center">
            Е01.1, Е04.2
        </td>

        <?php
            foreach ($val['val54'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            тиреотоксикоз (гіпертиреоз)
        </td>

        <td style="text-align: center">
            5,5
        </td>

        <td style="text-align: center">
            Е05
        </td>

        <?php
            foreach ($val['val55'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            тиреоїдити
        </td>

        <td style="text-align: center">
            5,6
        </td>

        <td style="text-align: center">
            Е06
        </td>

        <?php
            foreach ($val['val56'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            цукровий діабет
        </td>

        <td style="text-align: center">
            5,8
        </td>

        <td style="text-align: center">
            Е10 - E14
        </td>

        <?php
            foreach ($val['val58'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            нецукровий діабет
        </td>

        <td style="text-align: center">
            5,17
        </td>

        <td style="text-align: center">
            E23.2
        </td>

        <?php
            foreach ($val['val517'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            ожиріння
        </td>

        <td style="text-align: center">
            5,18
        </td>

        <td style="text-align: center">
            E66
        </td>

        <?php
            foreach ($val['val518'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            післяопераційний гіпотиреоз
        </td>

        <td style="text-align: center">
            5,19
        </td>

        <td style="text-align: center">
            Е89.0
        </td>

        <?php
            foreach ($val['val519'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Розлади психіки і поведінки
        </td>

        <td style="text-align: center">
            6,0
        </td>

        <td style="text-align: center">
            F00 - F99
        </td>

        <?php
            foreach ($val['val60'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби нервової системи
        </td>

        <td style="text-align: center">
            7,0
        </td>

        <td style="text-align: center">
            G00 - G99
        </td>

        <?php
            foreach ($val['val70'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: запальні хвороби ЦНС
        </td>

        <td style="text-align: center">
            7,1
        </td>

        <td style="text-align: center">
            G00, G03, G04, G06, G08, G09
        </td>

        <?php
            foreach ($val['val71'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            епілепсія
        </td>

        <td style="text-align: center">
            7,5
        </td>

        <td style="text-align: center">
            G40-G41
        </td>

        <?php
            foreach ($val['val75'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хвороби периферичної нервової системи
        </td>

        <td style="text-align: center">
            7,8
        </td>

        <td style="text-align: center">
            G50-G52, G54, G56-G58, G60-G62, G64, G70-G72
        </td>

        <?php
            foreach ($val['val78'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            дитячий церебральний параліч
        </td>

        <td style="text-align: center">
            7,11
        </td>

        <td style="text-align: center">
            G80
        </td>

        <?php
            foreach ($val['val711'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби ока та придаткового апарату
        </td>

        <td style="text-align: center">
            8,0
        </td>

        <td style="text-align: center">
            Н00-Н59
        </td>

        <?php
            foreach ($val['val80'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: кон'юнктивіт та інші захворювання кон'юнктиви
        </td>

        <td style="text-align: center">
            8,2
        </td>

        <td style="text-align: center">
            Н10-Н11
        </td>

        <?php
            foreach ($val['val82'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            катаракта
        </td>

        <td style="text-align: center">
            8,4
        </td>

        <td style="text-align: center">
            Н25-Н26
        </td>

        <?php
            foreach ($val['val84'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            косоокість
        </td>

        <td style="text-align: center">
            8,8
        </td>

        <td style="text-align: center">
            Н49-Н50
        </td>

        <?php
            foreach ($val['val88'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            міопія (короткозорість)
        </td>

        <td style="text-align: center">
            8,11
        </td>

        <td style="text-align: center">
            Н52.1
        </td>

        <?php
            foreach ($val['val811'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби вуха та соскоподібного відростка
        </td>

        <td style="text-align: center">
            9,0
        </td>

        <td style="text-align: center">
            Н60-Н95
        </td>

        <?php
            foreach ($val['val90'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: хвороби середнього вуха та соскоподібного відростка
        </td>

        <td style="text-align: center">
            9,1
        </td>

        <td style="text-align: center">
            Н65-Н66, Н68-Н74
        </td>

        <?php
            foreach ($val['val91'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            з них гострий отит середнього вуха
        </td>

        <td style="text-align: center">
            9,2
        </td>

        <td style="text-align: center">
            H65.0.1, H66.0
        </td>

        <?php
            foreach ($val['val92'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічний отит середнього вуха
        </td>

        <td style="text-align: center">
            9,3
        </td>

        <td style="text-align: center">
            H65.2.3.4, H66.1.2.3
        </td>

        <?php
            foreach ($val['val93'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби системи кровообігу
        </td>

        <td style="text-align: center">
            10,0
        </td>

        <td style="text-align: center">
            I00-I99
        </td>

        <?php
            foreach ($val['val100'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: гостра ревматична гарячка
        </td>

        <td style="text-align: center">
            10,1
        </td>

        <td style="text-align: center">
            I00-I02
        </td>

        <?php
            foreach ($val['val101'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            з них без ураження серця
        </td>

        <td style="text-align: center">
            10,2
        </td>

        <td style="text-align: center">
            I00
        </td>

        <?php
            foreach ($val['val102'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічні ревматичні хвороби серця
        </td>

        <td style="text-align: center">
            10,3
        </td>

        <td style="text-align: center">
            I05-I09
        </td>

        <?php
            foreach ($val['val103'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            з них ревматичні ураження клапанів серця
        </td>

        <td style="text-align: center">
            10,4
        </td>

        <td style="text-align: center">
            I05-I08
        </td>

        <?php
            foreach ($val['val104'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            есенціальна гіпертензія
        </td>

        <td style="text-align: center">
            10,6
        </td>

        <td style="text-align: center">
            I10
        </td>

        <?php
            foreach ($val['val106'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            неревматичні ураження клапанів серця
        </td>

        <td style="text-align: center">
            10,15
        </td>

        <td style="text-align: center">
            I34-I38
        </td>

        <?php
            foreach ($val['val1015'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби органів дихання
        </td>

        <td style="text-align: center">
            11,0
        </td>

        <td style="text-align: center">
            J00-J99
        </td>

        <?php
            foreach ($val['val110'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: гострий фарингіт та гострий тонзиліт
        </td>

        <td style="text-align: center">
            11,1
        </td>

        <td style="text-align: center">
            J02-J03
        </td>

        <?php
            foreach ($val['val111'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            гострий ларингіт та трахеїт
        </td>

        <td style="text-align: center">
            11,2
        </td>

        <td style="text-align: center">
            J04
        </td>

        <?php
            foreach ($val['val112'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            пневмонії
        </td>

        <td style="text-align: center">
            11,5
        </td>

        <td style="text-align: center">
            J12-J16, J18
        </td>

        <?php
            foreach ($val['val115'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            алергічний риніт
        </td>

        <td style="text-align: center">
            11,8
        </td>

        <td style="text-align: center">
            J30.1-4
        </td>

        <?php
            foreach ($val['val118'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічний риніт, назофарингіт, фарингіт
        </td>

        <td style="text-align: center">
            11,9
        </td>

        <td style="text-align: center">
            J31
        </td>

        <?php
            foreach ($val['val119'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічні хвороби мигдалин та аденоїдів
        </td>

        <td style="text-align: center">
            11,10
        </td>

        <td style="text-align: center">
            J35
        </td>

        <?php
            foreach ($val['val1110'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічний ларингіт, ларинготрахеїт
        </td>

        <td style="text-align: center">
            11,12
        </td>

        <td style="text-align: center">
            J37
        </td>

        <?php
            foreach ($val['val1112'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            бронхіт хронічний
        </td>

        <td style="text-align: center">
            11,14
        </td>

        <td style="text-align: center">
            J41-J42
        </td>

        <?php
            foreach ($val['val1114'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            бронхіальна астма
        </td>

        <td style="text-align: center">
            11,16
        </td>

        <td style="text-align: center">
            J45-J46
        </td>

        <?php
            foreach ($val['val1116'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби органів травлення
        </td>

        <td style="text-align: center">
            12,0
        </td>

        <td style="text-align: center">
            К00-К93
        </td>

        <?php
            foreach ($val['val120'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: гастроезофагеальний рефлюкс
        </td>

        <td style="text-align: center">
            12,1
        </td>

        <td style="text-align: center">
            К21
        </td>

        <?php
            foreach ($val['val121'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: виразка шлунка та 12-палої кишки
        </td>

        <td style="text-align: center">
            12,3
        </td>

        <td style="text-align: center">
            К25-К27
        </td>

        <?php
            foreach ($val['val123'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            гастрит та дуоденіт
        </td>

        <td style="text-align: center">
            12,4
        </td>

        <td style="text-align: center">
            К29
        </td>

        <?php
            foreach ($val['val124'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            диспепсії
        </td>

        <td style="text-align: center">
            12,5
        </td>

        <td style="text-align: center">
            К30
        </td>

        <?php
            foreach ($val['val125'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            функціональні розлади шлунка
        </td>

        <td style="text-align: center">
            12,6
        </td>

        <td style="text-align: center">
            К31
        </td>

        <?php
            foreach ($val['val126'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хвороба Крона
        </td>

        <td style="text-align: center">
            12,7
        </td>

        <td style="text-align: center">
            К50
        </td>

        <?php
            foreach ($val['val127'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            неспецифічний виразковий коліт
        </td>

        <td style="text-align: center">
            12,8
        </td>

        <td style="text-align: center">
            К51
        </td>

        <?php
            foreach ($val['val128'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            синдром подразненого кишечника
        </td>

        <td style="text-align: center">
            12,10
        </td>

        <td style="text-align: center">
            К58
        </td>

        <?php
            foreach ($val['val1210'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічний гепатит
        </td>

        <td style="text-align: center">
            12,12
        </td>

        <td style="text-align: center">
            К73, К75.2,3
        </td>

        <?php
            foreach ($val['val1212'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            жовчнокам'яна хвороба
        </td>

        <td style="text-align: center">
            12,13
        </td>

        <td style="text-align: center">
            К80
        </td>

        <?php
            foreach ($val['val1213'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            холецистит, холангіт
        </td>

        <td style="text-align: center">
            12,14
        </td>

        <td style="text-align: center">
            К81, К83.0
        </td>

        <?php
            foreach ($val['val1214'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хвороби підшлункової залози
        </td>

        <td style="text-align: center">
            12,15
        </td>

        <td style="text-align: center">
            К85, К86
        </td>

        <?php
            foreach ($val['val1215'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            целіакія
        </td>

        <td style="text-align: center">
            12,16
        </td>

        <td style="text-align: center">
            К90.0
        </td>

        <?php
            foreach ($val['val1216'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби шкіри та підшкірної клітковини
        </td>

        <td style="text-align: center">
            13,0
        </td>

        <td style="text-align: center">
            L00 - L99
        </td>

        <?php
            foreach ($val['val130'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: інфекції шкіри та підшкірної клітковини
        </td>

        <td style="text-align: center">
            13,1
        </td>

        <td style="text-align: center">
            L00 - L08
        </td>

        <?php
            foreach ($val['val131'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            атопічний дерматит
        </td>

        <td style="text-align: center">
            13,2
        </td>

        <td style="text-align: center">
            L20
        </td>

        <?php
            foreach ($val['val132'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            контактні дерматити
        </td>

        <td style="text-align: center">
            13,3
        </td>

        <td style="text-align: center">
            L23-L25
        </td>

        <?php
            foreach ($val['val133'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби кістково-м'язової системи та сполучної тканини
        </td>

        <td style="text-align: center">
            14,0
        </td>

        <td style="text-align: center">
            М00-М99
        </td>

        <?php
            foreach ($val['val140'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: ревматоїдний артрит
        </td>

        <td style="text-align: center">
            14,2
        </td>

        <td style="text-align: center">
            М05, М06, M08.0
        </td>

        <?php
            foreach ($val['val142'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            системний червоний вовчак
        </td>

        <td style="text-align: center">
            14,9
        </td>

        <td style="text-align: center">
            М32
        </td>

        <?php
            foreach ($val['val149'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Хвороби сечостатевої системи
        </td>

        <td style="text-align: center">
            15,0
        </td>

        <td style="text-align: center">
            N00-N99
        </td>

        <?php
            foreach ($val['val150'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: гострий гломерулонефрит
        </td>

        <td style="text-align: center">
            15,2
        </td>

        <td style="text-align: center">
            N00
        </td>

        <?php
            foreach ($val['val152'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічний гломерулонефрит
        </td>

        <td style="text-align: center">
            15,4
        </td>

        <td style="text-align: center">
            N03
        </td>

        <?php
            foreach ($val['val154'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            інфекції нирок
        </td>

        <td style="text-align: center">
            15,6
        </td>

        <td style="text-align: center">
            N10-N12
        </td>

        <?php
            foreach ($val['val156'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            з них хронічний пієлонефрит
        </td>

        <td style="text-align: center">
            15,7
        </td>

        <td style="text-align: center">
            N11
        </td>

        <?php
            foreach ($val['val157'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            цистит
        </td>

        <td style="text-align: center">
            15,10
        </td>

        <td style="text-align: center">
            N30
        </td>

        <?php
            foreach ($val['val1510'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            з них гострий цистит
        </td>

        <td style="text-align: center">
            15,11
        </td>

        <td style="text-align: center">
            N30.0
        </td>

        <?php
            foreach ($val['val1511'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            хронічний цистит
        </td>

        <td style="text-align: center">
            15,12
        </td>

        <td style="text-align: center">
            N30.1.2
        </td>

        <?php
            foreach ($val['val1512'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            гідроцеле та сперматоцеле
        </td>

        <td style="text-align: center">
            15,15
        </td>

        <td style="text-align: center">
            N43
        </td>

        <?php
            foreach ($val['val1515'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            розлади менструацій
        </td>

        <td style="text-align: center">
            15,28
        </td>

        <td style="text-align: center">
            N91-N92, N94
        </td>

        <?php
            foreach ($val['val1528'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Вагітність, пологи та післяпологовий період
        </td>

        <td style="text-align: center">
            16,0
        </td>

        <td style="text-align: center">
            О00-О99 (крім O80)
        </td>

        <?php
            foreach ($val['val160'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Окремі стани, що виникають у перинатальному періоді
        </td>

        <td style="text-align: center">
            17,0
        </td>

        <td style="text-align: center">
            Р05-Р96
        </td>

        <?php
            foreach ($val['val170'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі: пологова травма
        </td>

        <td style="text-align: center">
            17,1
        </td>

        <td style="text-align: center">
            Р10-Р15
        </td>

        <?php
            foreach ($val['val171'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            внутрішньоматкова гіпоксія та асфіксія в пологах
        </td>

        <td style="text-align: center">
            17,2
        </td>

        <td style="text-align: center">
            Р20, Р21
        </td>

        <?php
            foreach ($val['val172'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            гемолітична хвороба плоду або новонародженого, обумовлена ізоімунізацією
        </td>

        <td style="text-align: center">
            17,3
        </td>

        <td style="text-align: center">
            Р55, Р56, P57.0
        </td>

        <?php
            foreach ($val['val173'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Уроджені аномалії (вади розвитку), деформації і хромосомні порушення
        </td>

        <td style="text-align: center">
            18,0
        </td>

        <td style="text-align: center">
            Q00-Q99
        </td>

        <?php
            foreach ($val['val180'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            у тому числі вроджені аномалії системи кровообігу
        </td>

        <td style="text-align: center">
            18,1
        </td>

        <td style="text-align: center">
            Q20-Q26
        </td>

        <?php
            foreach ($val['val181'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Симптоми, ознаки та відхилення від норми, що виявлені під час лабораторних
            та клінічних досліджень, не класифіковані в інших рубриках
        </td>

        <td style="text-align: center">
            19,0
        </td>

        <td style="text-align: center">
            R00-R99
        </td>

        <?php
            foreach ($val['val190'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>

    <tr>
        <td>
            Травми, отруєння та деякі інші наслідки дії зовнішніх причин
        </td>

        <td style="text-align: center">
            20,0
        </td>

        <td style="text-align: center">
            S00-T98
        </td>

        <?php
            foreach ($val['val200'] as $value)
            {
                echo '<td style="text-align: center">';
                echo $value > 0 ? $value : '';
                echo '</td>';
            }
        ?>
    </tr>


</table>

<br>


<table style="width: 100%;">
    <tr>
        <td colspan="2" style="text-align: right">
            Підпис лікаря: ________________________________
        </td>
    </tr>

</table>