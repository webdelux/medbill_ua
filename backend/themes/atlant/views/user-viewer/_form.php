<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
?>

<?php echo Alert::widget(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'profile-form',
    'layout' => 'horizontal',
    'options' => ['enctype'=>'multipart/form-data'],
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-6',
        ],
    ],
]); ?>

    <div class="panel panel-default tabs">

        <ul id="profile-tabs" class="nav nav-tabs" role="tablist">

            <li class="<?= (Yii::$app->request->get('tab') == 0) ? 'active' : '' ?>">

                <a href="<?= Url::current(['tab' => 0]) ?>" role="tab" data-toggle="_tab">
                    <?php echo Yii::t('user', 'Account details'); ?>
                </a>

            </li>

            <li class="<?= (Yii::$app->request->get('tab') == 1) ? 'active' : '' ?>">

                <a href="<?= Url::current(['tab' => 1]) ?>" role="tab" data-toggle="_tab">
                    <?php echo Yii::t('user', 'Profile details'); ?>
                </a>

            </li>

            <li class="<?= (Yii::$app->request->get('tab') == 2) ? 'active' : '' ?>">

                <a href="<?= Url::current(['tab' => 2]) ?>" role="tab" data-toggle="_tab">
                    <?php echo Yii::t('app', 'Work schedule'); ?>
                </a>

            </li>

        </ul>

        <div class="panel-body tab-content">

            <div class="tab-pane <?= (Yii::$app->request->get('tab') == 0) ? 'active' : '' ?>" id="tab-0">

                <?php echo $this->render('tabs/_tab-0', [
                    'form' => $form,
                    'user' => $user,
                    'profile' => $profile,
                    'profileSpeciality' => $profileSpeciality,
                    'profileDepartment' => $profileDepartment,
                    'dataProfileSchedule' => $dataProfileSchedule,
                    'assignment' => $assignment
                ]); ?>

            </div>

            <div class="tab-pane <?= (Yii::$app->request->get('tab') == 1) ? 'active' : '' ?>" id="tab-1">

                <?php echo $this->render('tabs/_tab-1', [
                    'form' => $form,
                    'user' => $user,
                    'profile' => $profile,
                    'profileSpeciality' => $profileSpeciality,
                    'profileDepartment' => $profileDepartment,
                    'dataProfileSchedule' => $dataProfileSchedule,
                    'assignment' => $assignment
                ]); ?>

            </div>

            <div class="tab-pane <?= (Yii::$app->request->get('tab') == 2) ? 'active' : '' ?>" id="tab-2">

                <?php echo $this->render('tabs/_tab-2', [
                    'form' => $form,
                    'user' => $user,
                    'profile' => $profile,
                    'profileSpeciality' => $profileSpeciality,
                    'profileDepartment' => $profileDepartment,
                    'dataProfileSchedule' => $dataProfileSchedule,
                    'assignment' => $assignment
                ]); ?>

            </div>

        </div>

        <div class="panel-footer">
            <?= Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $user->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    </div>

<?php ActiveForm::end(); ?>


<?php $this->registerCss("

    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }
    .ui-autocomplete::after, .ui-autocomplete::before {
        border: none;
    }

    .field-profile-working_days {
        margin: 0;
    }

    #profile-working_days,
    .profiledepartment-working_day {
        display: table;
        width: 665px;
    }
    #profile-working_days > .checkbox,
    .profiledepartment-working_day > .checkbox{
        margin: 0;
        padding: 0;
        display: table-cell;
        width: 95px;
        text-align: left;
    }

    #profile-schedule-form-create {
        width: 648px;
    }

    .field-profile-working_time {
        width: 665px;
    }
    .field-profile-working_time tr > td {
        padding-right: 5px;
    }
    .field-profile-working_time tr > td > .form-group {
        margin: 0;
    }
    .field-profile-working_time tr > td > .form-group label {
        margin: 0;
        padding: 0;
        display: block;
        width: 90px;
        text-align: center;
    }

    .field-profile-schedule .panel {
        width: 668px;
    }
    .field-profile-schedule .panel,
    .field-profile-schedule .table {
        margin: 0;
    }
    .field-profile-schedule .panel .panel-title {
        font-size: 12px;
    }
    .field-profile-schedule .panel .panel-heading {
        padding: 5px;
    }
    .field-profile-schedule .panel .panel-body {
        padding: 10px;
    }
    .field-profile-schedule .panel .panel-body .pagination {
        margin: 10px 0 0 0;
    }
    .field-profile-schedule input.error {
        color: #b64645;
    }

    .form-inline {
        border-bottom: 1px solid #ddd;
        width: auto;
        padding-bottom: 10px;
        margin-bottom: 10px;
    }
    .form-inline .inline-label {
        display: inline-block;
        margin: 0;
    }
    .form-inline .inline-control {
        display: inline-block;
        width: 120px;
    }
    .form-inline .btn {
        display: inline-block;
    }
    .form-inline .inline-ajax-loading {
        display: inline-block;
        width: 30px;
        font-size: 16px;
        text-align: center;
        vertical-align: middle;
    }

    #profile-department-schedule .panel .panel-heading {
        padding: 5px;
    }
    #profile-department-schedule .panel .panel-title {
        font-size: 12px;
        line-height: normal;
    }
    #profile-department-schedule .panel .panel-body {
        padding: 10px;
    }

"); ?>

<?php $this->registerJs("

    $(document).on('change', '#" . Html::getInputId($profile, 'cityAddress') . "', function(event) {
        if (!$(this).val()) {
            $('#" . Html::getInputId($profile, 'city_id') . "').val('');
        }
    })

    $(document).on('click', 'a[data-delete]', function(event) {
        event.preventDefault();
        if (confirm($(this).attr('data-delete')))
        {
            var container = $(this).attr('data-container');

            $.ajax({
                method: 'POST',
                url: $(this).attr('href')
            })
            .done(function(data) {
                $.pjax.reload({
                    container: container
                });
            });
        }
    })

    $(document).on('click', '.js-btn-profile-schedule-create', function(event) {

        var id = $(this).attr('data-id');
        var departmentId = $(this).attr('data-department-id');
        var userId = $(this).attr('data-user-id');

        var typeId = $('#profile-schedule-type_id-id' + departmentId);
        var dateFrom = $('#profile-schedule-date_from-id' + departmentId);
        var dateTo = $('#profile-schedule-date_to-id' + departmentId);

        if (!typeId.val())
            typeId.addClass('error');
        else
            typeId.removeClass('error');

        if (!dateFrom.val())
            dateFrom.addClass('error');
        else
            dateFrom.removeClass('error');

        if (!dateTo.val())
            dateTo.addClass('error');
        else
            dateTo.removeClass('error');

        $.ajax({
            method: 'POST',
            url: '" . Url::toRoute(['create-schedule']) . "?user_id=' + userId + '&profile_department_id=' + id,
            data: $('#profile-schedule-form-create-id' + departmentId).find('input, textarea, select').serialize(),
            beforeSend: function(xhr) {
                $('#profile-schedule-form-ajax-loading-id' + departmentId).show();
            }
        })
        .always(function() {
            $('#profile-schedule-form-ajax-loading-id' + departmentId).hide();
        })
        .done(function(data) {

            typeId.val('');
            dateFrom.val('');
            dateTo.val('');

            $.pjax.reload({
                container: '#profile-schedule-id' + departmentId
            });
        });

    })

    /*
    $(document).on('click', '#profile-schedule-btn_create', function(event) {

        var typeId = $('#profile-schedule-type_id');
        var dateFrom = $('#profile-schedule-date_from');
        var dateTo = $('#profile-schedule-date_to');

        if (!typeId.val())
            typeId.addClass('error');
        else
            typeId.removeClass('error');

        if (!dateFrom.val())
            dateFrom.addClass('error');
        else
            dateFrom.removeClass('error');

        if (!dateTo.val())
            dateTo.addClass('error');
        else
            dateTo.removeClass('error');

        $.ajax({
            method: 'POST',
            url: '" . Url::toRoute(['create-schedule', 'user_id' => $user->id]) . "',
            data: $('#profile-form').serialize(),
            beforeSend: function(xhr) {
                $('#profile-schedule-form-ajax-loading').show();
            }
        })
        .always(function() {
            $('#profile-schedule-form-ajax-loading').hide();
        })
        .done(function(data) {

            typeId.val('');
            dateFrom.val('');
            dateTo.val('');

            $.pjax.reload({
                container: '#profile-schedule'
            });
        });
    })
    */

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

    // встановлюємо максимальний розмір вікна
    $('#modal-avatar').on('show.bs.modal', function () {
        $('.modal .modal-body').css('overflow-y', 'auto');
        $('.modal .modal-body').css('max-height', $(window).height() * 0.7);
    });

"); ?>