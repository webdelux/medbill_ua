<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Create');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-create">

    <?= $this->render('_form', [
        'user' => $user,
        'profile' => $profile,
        'profileSpeciality' => $profileSpeciality,
        'profileDepartment' => $profileDepartment,
        'dataProfileSchedule' => $dataProfileSchedule,
    ]) ?>

</div>