<?php

use yii\helpers\Html;

$this->title = $user->profile->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

//$this->params['layoutPanelTitle'] = $this->title;
?>

<div class="user-update">

    <?= $this->render('_form', [
        'user' => $user,
        'profile' => $profile,
        'profileSpeciality' => $profileSpeciality,
        'profileDepartment' => $profileDepartment,
        'dataProfileSchedule' => $dataProfileSchedule,
        'assignment' => $assignment
    ]) ?>

</div>