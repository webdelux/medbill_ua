<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use backend\modules\handbook\models\Status;
use backend\modules\handbook\models\Speciality;
use backend\modules\handbook\models\Department;
?>

<?= $form->field($profile, 'name') ?>
<?= $form->field($profile, 'public_email') ?>
<?= $form->field($profile, 'website') ?>
<?= $form->field($profile, 'gravatar_email') ?>
<?= $form->field($profile, 'bio')->textarea() ?>
<?= $form->field($profile, 'payment') ?>
<?= $form->field($profile, 'note') ?>

<?= $form->field($profile, 'cityAddress')->widget(\yii\jui\AutoComplete::classname(), [
    'options' => [
        'class' => 'form-control',
        'value' => (isset($profile->city->address)) ? $profile->city->address : null
    ],
    'clientOptions' => [
        'source' => Url::to(['/handbook/city/index']),
        'select' => new JsExpression("function(event, ui) {
            $('#" . Html::getInputId($profile, 'city_id') . "').val(ui.item.id);
        }")
    ],
]) ?>

<?= Html::activeHiddenInput($profile, 'city_id') ?>

<?= $form->field($profile, 'location') ?>

<?= $form->field($profileSpeciality, 'speciality_id')->dropDownList((new Speciality)->treeList((new Speciality)->treeData()),
            ['class' => 'form-control select', 'multiple' => 'multiple', 'data-live-search' => 'true', 'title' => '']) ?>

<?= $form->field($profileDepartment, 'department_id')->dropDownList((new Department)->treeList(),
            ['class' => 'form-control select', 'multiple' => 'multiple', 'data-live-search' => 'true', 'title' => '']) ?>

<?= $form->field($profile, 'status_id')->dropDownList(Status::childList(), ['prompt' => '']) ?>

<?= $form->field($profile, 'mark_id')->dropDownList($profile::itemAlias('mark_id'), ['prompt' => '']) ?>