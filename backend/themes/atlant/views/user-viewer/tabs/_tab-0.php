<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\AuthItem;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
//use bupy7\cropbox\Cropbox;
use common\widgets\Cropbox;
?>

<div class="form-horizontal">
    <?php Modal::begin([
        'id' => 'modal-avatar',
        'size' => Modal::SIZE_DEFAULT,
        'header' => '<h4 class="modal-title">' . Yii::t('app', 'Change photo') . '</h4>',
        //'toggleButton' => ['label' => Yii::t('app', 'Add'), 'class' => 'btn btn-primary btn-sm btn-block'],
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
    ]); ?>

    <?=
        $form->field($profile, 'image',[
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-12 col-sm-offset-0',
            ]])->widget(Cropbox::className(), [
                'attributeCropInfo' => 'crop_info',
                //]);->widget(FileInput::classname(), [
                //'options' => ['multiple' => false, 'accept' => 'image/*'],
                //'pluginOptions' => ['previewFileType' => 'image']
            ])->label(false);
    ?>

    <?=
        Html::submitButton( Yii::t('app', 'Save'), ['class' =>  'btn btn-primary'])
    ?>

    <?php Modal::end(); ?>

</div>

<div class="col-md-3 col-sm-4 col-xs-5">
    <form action="#" class="form-horizontal">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="text-center" id="user_image">
                    <?php
                        if (isset($profile->avatar_url)){
                            echo Html::img('@web/images/' . $profile->avatar_url, ['class' => 'img-thumbnail', 'style' => 'max-Width:250px;']);
                        } else {
                            echo Html::img('@web/images/default-user.png', ['class' => 'img-thumbnail']);
                        }
                    ?>

                </div>
            </div>
            <div class="panel-body form-group-separated">

                <div class="form-group">
                    <div class="col-md-12 col-xs-12">
                        <a href="#" class="btn btn-primary btn-block btn-rounded" data-toggle="modal" data-target="#modal-avatar"><?= Yii::t('app', 'Change photo'); ?></a>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

<div class="col-md-9 col-sm-8 col-xs-7">

    <?php /* if (!$user->isNewRecord): ?>
        <?= $form->field($assignment, 'items')->widget(Select2::className(), [
            //'data' => $assignment->getAvailableItems(),
            'data' => ArrayHelper::map(AuthItem::find()
                ->where(['type' => 1])
                ->andWhere(['!=', 'name', Yii::$app->params['roleAdmin']])
                ->orderBy('name')
                ->all(), 'name', 'fullName'
            ),
            'options' => [
                'id' => 'items',
                'multiple' => true
            ],
        ])->label(Yii::t('user', 'Role')); ?>
    <?php endif; */ ?>

    <?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($user, 'phone')->textInput(['maxlength' => 13, 'placeholder' => '+380671234567']) ?>
    <?= $form->field($user, 'password')->passwordInput() ?>

</div>