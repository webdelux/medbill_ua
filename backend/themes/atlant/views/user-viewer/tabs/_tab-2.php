<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\ProfileSchedule;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use backend\modules\user\models\ProfileDepartment;

$departments = ProfileDepartment::findAll(['user_id' => $user->id]);
?>


<?php if ($departments): ?>

    <div id="profile-department-schedule" class="panel-group accordion accordion-dc">

        <?php foreach ($departments as $value): ?>

            <?php $department = ProfileDepartment::findOne(['user_id' => $user->id, 'department_id' => $value->department_id]); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#panel-department-<?= $value->department_id ?>">
                            <?= $value->department->name ?>
                        </a>
                    </h4>
                </div>
                <div class="panel-body" id="panel-department-<?= $value->department_id ?>">

                    <?php $department->working_days = explode(',', $department->working_days); ?>

                    <?= $form->field($department, 'working_days')->checkboxList(ProfileDepartment::itemAlias('working_days'), [
                        'id' => Html::getInputId($department, 'working_days') . "-id{$department->department_id}",
                        'name' => $department->formName() . "[{$department->department_id}][working_days]",
                        'class' => 'profiledepartment-working_day'
                    ]) ?>

                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <table class="field-profile-working_time">
                                <tr>
                                    <?php foreach (ProfileDepartment::itemAlias('working_days') as $key => $val): ?>
                                        <td>

                                            <?= $form->field($department, "working_time_{$key}_from", [
                                                'template' => '{label} {input} {error}',
                                                'inputOptions' => ['class' => 'form-control'],
                                                'labelOptions' => ['class' => 'control-label col-sm-2']
                                            ])->widget(TimePicker::classname(), [
                                                'options' => [
                                                    'placeholder' => '00:00:00',
                                                    'id' => Html::getInputId($department, "working_time_{$key}_from") . "-id{$department->department_id}",
                                                    'name' => $department->formName() . "[{$department->department_id}][working_time_{$key}_from]",
                                                ],
                                                'pluginOptions' => [
                                                    'showSeconds' => true,
                                                    'showMeridian' => false,
                                                    'template' => false
                                                ]
                                            ]) ?>

                                            <?= $form->field($department, "working_time_{$key}_to", [
                                                'template' => '{label} {input} {error}',
                                                'inputOptions' => ['class' => 'form-control'],
                                                'labelOptions' => ['class' => 'control-label col-sm-2']
                                            ])->widget(TimePicker::classname(), [
                                                'options' => [
                                                    'placeholder' => '00:00:00',
                                                    'id' => Html::getInputId($department, "working_time_{$key}_to") . "-id{$department->department_id}",
                                                    'name' => $department->formName() . "[{$department->department_id}][working_time_{$key}_to]",
                                                ],
                                                'pluginOptions' => [
                                                    'showSeconds' => true,
                                                    'showMeridian' => false,
                                                    'template' => false
                                                ]
                                            ]) ?>

                                        </td>
                                    <?php endforeach; ?>
                                </tr>
                            </table>
                        </div>
                    </div>





                    <div class="form-group field-profile-schedule">
                        <label class="control-label col-sm-3">
                            &nbsp;
                        </label>
                        <div class="col-sm-5">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <?php echo Yii::t('app', 'Additions working schedule'); ?>
                                    </h3>
                                </div>
                                <div class="panel-body panel-body-open">

                                    <div id="profile-schedule-form-create-id<?= $department->department_id ?>" class="form-inline">

                                        <?= Html::label(Yii::t('app', 'Type'), 'profile-schedule-type_id' . "-id{$department->department_id}", ['class' => 'control-label inline-label']) ?>

                                        <?= Html::dropDownList("ProfileSchedule[type_id]", '', ProfileSchedule::itemAlias('type_id'), [
                                            'id' => 'profile-schedule-type_id' . "-id{$department->department_id}",
                                            'class' => 'form-control inline-control',
                                            'prompt' => ''
                                        ]) ?>


                                        <?= Html::label(Yii::t('app', 'From time'), 'profile-schedule-date_from' . "-id{$department->department_id}", ['class' => 'control-label inline-label']) ?>
                                        <?= DatePicker::widget([
                                            'id' => 'profile-schedule-date_from' . "-id{$department->department_id}",
                                            'name' => "ProfileSchedule[date_from]",
                                            'options' => [
                                                'class' => 'inline-control',
                                                'placeholder' => '0000-00-00',
                                            ],
                                            'type' => DatePicker::TYPE_INPUT,
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'autoclose' => true,
                                                'todayHighlight' => true,
                                                'todayBtn' => true,
                                                'format' => 'yyyy-mm-dd'
                                            ]
                                        ]);?>


                                        <?= Html::label(Yii::t('app', 'To time'), 'profile-schedule-date_to' . "-id{$department->department_id}", ['class' => 'control-label inline-label']) ?>
                                        <?= DatePicker::widget([
                                            'id' => 'profile-schedule-date_to' . "-id{$department->department_id}",
                                            'name' => "ProfileSchedule[date_to]",
                                            'options' => [
                                                'class' => 'inline-control',
                                                'placeholder' => '0000-00-00',
                                            ],
                                            'type' => DatePicker::TYPE_INPUT,
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'autoclose' => true,
                                                'todayHighlight' => true,
                                                'todayBtn' => true,
                                                'format' => 'yyyy-mm-dd'
                                            ]
                                        ]);?>

                                        <?= Html::hiddenInput("ProfileSchedule[user_id]", $user->id, [
                                            'id' => 'profile-schedule-user_id' . "-id{$department->department_id}",
                                        ]) ?>

                                        <?= Html::button(Yii::t('app', 'Create'), [
                                            //'id' => 'profile-schedule-btn_create',
                                            'class' => 'btn btn-primary js-btn-profile-schedule-create',
                                            'data-id' => $department->id,
                                            'data-department-id' => $department->department_id,
                                            'data-user-id' => $user->id,
                                        ]) ?>

                                        <div id="profile-schedule-form-ajax-loading-id<?= $department->department_id ?>" class="inline-ajax-loading" style="display: none;">
                                            <i class="fa fa-refresh fa-spin"></i>
                                        </div>
                                    </div>

                                    <?php Pjax::begin([
                                        'id' => "profile-schedule-id{$department->department_id}",
                                        'linkSelector' => "#profile-schedule-id{$department->department_id} a[data-sort], #profile-schedule-id{$department->department_id} a[data-page]",
                                        'enablePushState' => false,
                                        'clientOptions' => [
                                            'method' => 'GET'
                                        ]
                                    ]); ?>

                                    <?= GridView::widget([
                                        'dataProvider' => new ActiveDataProvider([
                                            'query' => ProfileSchedule::find()->where([
                                                'profile_department_id' => $department->id,
                                                'user_id' => $user->id,
                                            ]),
                                            'pagination' => [
                                                'pageSize' => 5,
                                            ],
                                        ]),
                                        'options' => [
                                            'id' => 'profile-schedule-grid' . "-id{$department->department_id}"
                                        ],
                                        'columns' => [
                                            [
                                                'attribute' => 'type_id',
                                                'value' => function ($data) {
                                                    return ProfileSchedule::itemAlias('type_id', $data->type_id);
                                                }
                                            ],
                                            [
                                                'attribute' => 'date_from',
                                                'headerOptions' => ['width' => '100'],
                                            ],
                                            [
                                                'attribute' => 'date_to',
                                                'headerOptions' => ['width' => '100'],
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'headerOptions' => ['width' => '70'],
                                                'template' => '{delete}',
                                                'buttons' => [
                                                    'delete' => function ($url, $model, $key) {
                                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                                            Url::toRoute([
                                                                'delete-schedule',
                                                                'id' => $model->id,
                                                                'user_id' => $model->user_id,
                                                            ]),
                                                            [
                                                                'data-delete' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                                                'data-container' => "#profile-schedule-id{$model->profileDepartment->department_id}",
                                                                'title' => Yii::t('app', 'Delete'),
                                                            ]
                                                        );
                                                    }
                                                ]
                                            ],
                                        ],
                                    ]); ?>

                                    <?php Pjax::end(); ?>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        <?php endforeach; ?>

    </div>

<?php else: ?>

    <?= yii\bootstrap\Alert::widget([
        'options' => [
            'class' => 'alert-warning'
        ],
        'body' => Yii::t('app', 'To make the schedule please add at least one branch!')
    ]) ?>

<?php endif; ?>