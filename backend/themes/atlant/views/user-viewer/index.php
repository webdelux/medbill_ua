<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'id' => 'user-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            [
                'attribute' => 'profileName',
                'label' => Yii::t('user', 'Full name'),
                'value' => function ($model) {
                    return (isset($model->profile->name)) ? $model->profile->name : null;
                },
            ],
            'username',
            [
                'label' => Yii::t('user', 'Role'),
                'value' => function ($model) {
                    return Html::ul($model->authAssignment, ['item' => function($item) {
                        if ($item->itemName->type == 1) {

                            $assignment = $item->itemName->name;

                            if ($item->itemName->description)
                                $assignment .= " ({$item->itemName->description})";

                            return Html::tag('li', $assignment);
                        }
                    }]);
                },
                'format' => 'html',
            ],
            'phone',
            'email:email',
            [
                'attribute' => 'registration_ip',
                'value' => function ($model) {
                    return $model->registration_ip == null
                        ? '<span class="not-set">' . Yii::t('user', '(not set)') . '</span>'
                        : $model->registration_ip;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    if (extension_loaded('intl')) {
                        return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                    } else {
                        return date('Y-m-d G:i:s', $model->created_at);
                    }
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>

</div>


<?php $this->registerCss("

    #user-grid ul {
        padding: 0;
        margin-bottom: 0;
        list-style-type: none;
        list-style-position: inside;
    }

"); ?>