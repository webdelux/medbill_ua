<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii2fullcalendar\yii2fullcalendar;
use kartik\select2\Select2;
use backend\modules\handbook\models\Department;
use backend\modules\handbookemc\models\Category;

$this->title = Yii::t('app', 'Registry');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Html::a(Yii::t('app', 'Add') . ' ' . Yii::t('app', 'protocol'), ['/protocol/create', 'target' => true, 'date' => date('Y-m-d H:i')], [
    'target' => '_blank',
    'class' => 'btn btn-success',
]) ?>

<hr/>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-3">
            <?= Html::activeLabel($model, 'category_id') ?>
            <?= Html::activeDropDownList($model, 'category_id', (new Category)->treeList((new Category)->treeData()), [
                'class' => 'form-control',
                'prompt' => '',
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::activeLabel($model, 'protocol_id') ?>
            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'protocol_id',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => ''
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Url::to(['/handbookemc/protocol/index', 'results' => true]),
                        'dataType' => 'json',
                    ]
                ],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= Html::activeLabel($model, 'department_id') ?>
            <?= Html::activeDropDownList($model, 'department_id', (new Department)->getTreeListByUserId(), [
                'class' => 'form-control',
                'prompt' => '',
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::activeLabel($model, 'doctor_id') ?>
            <?= Html::activeDropDownList($model, 'doctor_id', $doctors, [
                'class' => 'form-control',
                'prompt' => '',
                // Chained Selects Plugin
                'options' => $doctorsOptions
            ]) ?>
        </div>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <?= Html::button(Yii::t('app', 'Show') . '/' . Yii::t('app', 'Refresh'), [
                'id' => 'btn-registry-calendar-refresh',
                'class' => 'btn btn-primary btn-block',
            ]) ?>
        </div>
    </div>
</div>

<?php $fcTime = (new Department)->getBetweenWorking(); ?>

<?= yii2fullcalendar::widget([
    'id' => "registry-calendar",
    'options' => [
        'lang' => substr(Yii::$app->language, 0, 2),
    ],
    'clientOptions' => [
        'allDaySlot' => false,
        'firstDay' => 1,
        'defaultView' => 'agendaDay',
        'minTime' => $fcTime->from,
        'maxTime' => $fcTime->to,
        'slotDuration' => '00:20:00',
        'defaultTimedEventDuration' => '00:20:00',
        'slotLabelFormat' => 'HH:mm',
        'timezone' => Yii::$app->timeZone,
        'height' => 'auto',
        'contentHeight' => 'auto',
        'fixedWeekCount' => false,
        'displayEventTime' => false,
        'eventRender' => new JsExpression('function(event, element, view) {
            element.popover({
                title: event.title,
                placement: "bottom",
                trigger: "hover",
                content: event.description,
                container: "body"
            });
        }'),
        'eventClick' => new JsExpression('function(event, jsEvent, view) {
            if (event.url) {
                window.open(event.url);
                return false;
            }
        }'),
        'dayClick' => new JsExpression('function(date, jsEvent, view, resourceObj) {
            if (confirm("' . Yii::t('app', 'Add') . ' ' . Yii::t('app', 'protocol') . '?")) {
                window.open("' . Url::to(['/protocol/create', 'target' => true]) . '&date=" + date.format(\'YYYY-MM-DD HH:mm\'));
            }
        }'),
    ],
    'header' => [
        'center' => 'title',
        'left' => 'prev,next today',
        'right' => 'month,agendaWeek,agendaDay',
    ],
    'ajaxEvents' => new JsExpression('{
        url: "' . Url::to(['/protocol/calendar']) . '",
        data: function() {
            return {
                category_id: $("#protocol-category_id").val(),
                protocol_id: $("#protocol-protocol_id").val(),
                department_id: $("#protocol-department_id").val(),
                doctor_id: $("#protocol-doctor_id").val()
            };
        }
    }'),
]) ?>


<?php $this->registerCss("

    hr {
        margin-bottom: 10px;
        margin-top: 10px;
    }

"); ?>

<?php $this->registerJsFile('@web/themes/atlant/js/plugins/chained/jquery.chained.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJs("

    // Chained Selects Plugin
    $('#protocol-doctor_id').chained('#protocol-department_id');

    $(document).on('click', '#btn-registry-calendar-refresh', function(event) {
        $('#registry-calendar').fullCalendar('refetchEvents');
    })

"); ?>