<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\handbook\models\Status;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\handbook\models\MedicalSuppliesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', ucfirst(Yii::$app->controller->id)), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo Alert::widget(); ?>

<div class="medical-supplies-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add category'), ['create', 'category'=>TRUE], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'header' => Yii::t('app', 'ID'),
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '80'],
            ],
            [
                'header' => Yii::t('app', 'Parent category'),
                'attribute' => 'parent_id',
                'value' => function ($data)
                {
                    if (isset($data['parent_id']))
                        $category = backend\models\Category::findOne(['id' => $data['parent_id']]);
                    return (isset($category['name'])) ? $category['name'] : '';
                }
            ],
            //'sort',
            [
                'header' => Yii::t('app', 'Name'),
                'attribute' => 'name',
            ],
            [
                'header' => Yii::t('app', 'Description'),
                //'label' => 'rrrr',
                'attribute' => 'description',
                'value' => function ($data)
                {
                    return $data['description'] ? $data['description'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'User'),
                'attribute' => 'user_id',
                'value' => function ($data)
                {
                    $user = backend\modules\user\models\Users::findOne(['id' => $data['user_id']]);
                    return $user['username'] ? $user['username'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'Updated User'),
                'attribute' => 'updated_user_id',
                'value' => function ($data)
                {
                    $user = backend\modules\user\models\Users::findOne(['id' => $data['updated_user_id']]);
                    return $user['username'] ? $user['username'] : '';
                }
            ],
             [
                'header' => Yii::t('app', 'Created At'),
                'attribute' => 'created_at',
                'value' => function ($data)
                {
                    return $data['created_at'] ? $data['created_at'] : '';
                }
            ],
            [
                'header' => Yii::t('app', 'Updated At'),
                'attribute' => 'updated_at',
                'value' => function ($data)
                {
                    return $data['updated_at'] ? $data['updated_at'] : '';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
                'template' => '{view} {update} {delete} {link}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model['id'], 'category' => true]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            ['update', 'id' => $model['id'], 'category' => true]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model['id'], 'category' => true],
                            ['data' => [
                                'confirm' => Yii::t('app', "Are you sure you want to delete this category?"),
                                'method' => 'post',
                                ]
                            ]);
                    },


                ]
            ],
        ],
    ]); ?>

</div>
