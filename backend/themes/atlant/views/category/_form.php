<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Category;
use common\widgets\Alert;


/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\MedicalSupplies */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="medical-supplies-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'parent_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->dropDownList($model->treeList($model->treeData($categoryId))) ?>

    <?php // echo $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'name', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
            ->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
