<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\handbook\models\MedicalSupplies */

$this->title = Yii::t('app', 'Update category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', Yii::$app->controller->id), 'url' => ['index', 'category'=>FALSE]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index', 'category'=>TRUE]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-supplies-update">


    <?= $this->render('//category/_form', [
        'model' => $model,
        'categoryId' => $categoryId
    ]) ?>

</div>
