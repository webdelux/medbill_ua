<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientOperation */

$this->title = Yii::t('app', 'ID') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['/pacient/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Surgeries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="pacient-operation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>