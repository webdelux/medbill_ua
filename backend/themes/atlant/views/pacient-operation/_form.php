<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use kartik\select2\Select2;
use common\widgets\Alert;
use backend\modules\handbook\models\Department;
use backend\modules\handbook\models\Speciality;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientOperation */
/* @var $form yii\widgets\ActiveForm */

$speciality = new Speciality;
?>

<?php //echo Alert::widget(); ?>

<div class="pacient-operation-form">

    <?php $form = ActiveForm::begin([
        'id' => 'pacient-operation-form',
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'offset' => 'col-sm-offset-4',
                'label' => 'col-sm-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => 'col-sm-4',
            ],
        ],
    ]); ?>

    <div id="pacient-operation-form-content">

        <?php echo Alert::widget(); ?>

        <?= Html::activeHiddenInput($model, 'id') ?>

        <?php $token = $model->id . $model->diagnosis_id; ?>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'handbook_emc_surgery_id', [
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($model->surgery) ? '' : $model->surgery->name,
                    'options' => [
                        'placeholder' => '',
                        'id' => Html::getInputId($model, 'handbook_emc_surgery_id') . "-operation-id{$token}",
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/surgery/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'handbook_emc_surgery_note', [
                ])->textInput([
                    'id' => Html::getInputId($model, 'handbook_emc_surgery_note') . "-operation-id{$token}",
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'handbook_emc_icd_id', [
                    'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($model->icd) ? '' : $model->icd->name,
                    'options' => [
                        'placeholder' => '',
                        'id' => Html::getInputId($model, 'handbook_emc_icd_id') . "-operation-id{$token}",
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/icd/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'handbook_emc_icd_note', [
                    'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                ])->textInput([
                    'id' => Html::getInputId($model, 'handbook_emc_icd_note') . "-operation-id{$token}",
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'start_operation_date', [
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($model->start_operation_date) ? $model->start_operation_date : date('Y-m-d'),
                        'id' => Html::getInputId($model, 'start_operation_date') . "-operation-id{$token}",
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'start_operation_time', [
                    'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                ])->widget(TimePicker::classname(), [
                    'options' => [
                        'placeholder' => '00:00',
                        'value' => ($model->start_operation_time) ? $model->start_operation_time : date('H:i'),
                        'id' => Html::getInputId($model, 'start_operation_time') . "-operation-id{$token}",
                    ],
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'template' => false
                    ]
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'duration_operation_hour', [
                    'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                ])->textInput([
                    'style' => 'width: 60px;',
                    'placeholder' => '0',
                    'value' => ($model->duration_operation_hour) ? $model->duration_operation_hour : 0,
                    'id' => Html::getInputId($model, 'duration_operation_hour') . "-operation-id{$token}",
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'duration_operation_minute', [
                    'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                ])->textInput([
                    'style' => 'width: 60px;',
                    'placeholder' => '0',
                    'value' => ($model->duration_operation_minute) ? $model->duration_operation_minute : 0,
                    'id' => Html::getInputId($model, 'duration_operation_minute') . "-operation-id{$token}",
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?php //echo $form->field($model, 'surgeon_user_id', [
//                ])->widget(Select2::classname(), [
//                    'theme' => Select2::THEME_BOOTSTRAP,
//                    'initValueText' => empty($model->surgeonUser) ? '' : $model->surgeonUser->profile->name,
//                    'options' => [
//                        'placeholder' => '',
//                        'id' => Html::getInputId($model, 'surgeon_user_id') . "-operation-id{$token}",
//                    ],
//                    'pluginOptions' => [
//                        'allowClear' => true,
//                        'minimumInputLength' => 3,
//                        'ajax' => [
//                            'url' => Url::to(['/user-viewer/index', 'results' => true, 'speciality' => 'surgeons']),
//                            'dataType' => 'json',
//                        ]
//                    ],
//                ]); ?>

                <?= $form->field($model, 'surgeon_user_id', [
                ])->dropDownList(ArrayHelper::map($speciality->getDoctors('surgeons'), 'user_id', 'name'), [
                    'prompt' => '',
                    'id' => Html::getInputId($model, 'surgeon_user_id') . "-operation-id{$token}",
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'department_id', [
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($model->department) ? '' : $model->department->name,
                    'options' => [
                        'placeholder' => '',
                        'id' => Html::getInputId($model, 'department_id') . "-operation-id{$token}",
                    ],
                    'data' => (new Department)->treeList((new Department)->treeData()),
                    'pluginOptions' => [
                        'allowClear' => true,
                        /*
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbook/department/index']),
                            'dataType' => 'json',
                        ]
                        */
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?php //echo $form->field($model, 'anesthetist_user_id', [
//                ])->widget(Select2::classname(), [
//                    'theme' => Select2::THEME_BOOTSTRAP,
//                    'initValueText' => empty($model->anesthetistUser) ? '' : $model->anesthetistUser->profile->name,
//                    'options' => [
//                        'placeholder' => '',
//                        'id' => Html::getInputId($model, 'anesthetist_user_id') . "-operation-id{$token}",
//                    ],
//                    'pluginOptions' => [
//                        'allowClear' => true,
//                        'minimumInputLength' => 3,
//                        'ajax' => [
//                            'url' => Url::to(['/user-viewer/index', 'results' => true, 'speciality' => 'anesthetists']),
//                            'dataType' => 'json',
//                        ]
//                    ],
//                ]); ?>

                <?= $form->field($model, 'anesthetist_user_id', [
                ])->dropDownList(ArrayHelper::map($speciality->getDoctors('anesthetists'), 'user_id', 'name'), [
                    'prompt' => '',
                    'id' => Html::getInputId($model, 'anesthetist_user_id') . "-operation-id{$token}",
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'anesthesia_id', [
                ])->dropDownList($model::itemAlias('anesthesia_id'), [
                    'prompt' => '',
                    'id' => Html::getInputId($model, 'anesthesia_id') . "-operation-id{$token}",
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'handbook_emc_complication_id', [
                ])->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'initValueText' => empty($model->complication) ? '' : $model->complication->name,
                    'options' => [
                        'placeholder' => '',
                        'id' => Html::getInputId($model, 'handbook_emc_complication_id') . "-operation-id{$token}",
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/handbookemc/complication/index']),
                            'dataType' => 'json',
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'handbook_emc_complication_note', [
                    'labelOptions' => ['class' => 'compacted control-label col-sm-4'],
                ])->textInput([
                    'id' => Html::getInputId($model, 'handbook_emc_complication_note') . "-operation-id{$token}",
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'type_id', [
                ])->dropDownList($model::itemAlias('type_id'), [
                    'prompt' => '',
                    'id' => Html::getInputId($model, 'type_id') . "-operation-id{$token}",
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'result_id', [
                ])->dropDownList($model::itemAlias('result_id'), [
                    'prompt' => '',
                    'id' => Html::getInputId($model, 'result_id') . "-operation-id{$token}",
                ]) ?>
            </div>
        </div>

        <div class="row modal-row">
            <div class="col-md-6">
                <?= $form->field($model, 'date_at', [
                ])->widget(DatePicker::classname(), [
                    'options' => [
                        'style' => 'width: 90px;',
                        'placeholder' => '0000-00-00',
                        'value' => ($model->date_at) ? $model->date_at : date('Y-m-d'),
                        'id' => Html::getInputId($model, 'date_at') . "-operation-id{$token}",
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerCss("

    .form-horizontal .modal-row {
        margin-bottom: 15px;
    }

"); ?>