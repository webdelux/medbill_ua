<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\PacientOperation;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientOperation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['/pacient/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Surgeries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pacient-operation-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'id' => 'pacient-operation-view',
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'diagnosis_id',
                'value' => (isset($model->diagnosis->icd->name)) ? $model->diagnosis->icd->name : '',
            ],
            [
                'attribute' => 'handbook_emc_surgery_id',
                'value' => (isset($model->surgery->name)) ? $model->surgery->name : '',
            ],
            'handbook_emc_surgery_note',
            [
                'attribute' => 'handbook_emc_icd_id',
                'value' => (isset($model->icd->name)) ? $model->icd->name : '',
            ],
            'handbook_emc_icd_note',
            'start_operation_date:date',
            'start_operation_time',
            'duration_operation_hour',
            'duration_operation_minute',
            [
                'attribute' => 'surgeon_user_id',
                'value' => (isset($model->surgeonUser->profile->name)) ? $model->surgeonUser->profile->name : '',
            ],
            [
                'attribute' => 'department_id',
                'value' => (isset($model->department->name)) ? $model->department->name : '',
            ],
            [
                'attribute' => 'anesthetist_user_id',
                'value' => (isset($model->anesthetistUser->profile->name)) ? $model->anesthetistUser->profile->name : '',
            ],
            [
                'attribute' => 'anesthesia_id',
                'value' => ($model->anesthesia_id) ? PacientOperation::itemAlias('anesthesia_id', $model->anesthesia_id) : '',
            ],
            [
                'attribute' => 'handbook_emc_complication_id',
                'value' => (isset($model->complication->name)) ? $model->complication->name : '',
            ],
            'handbook_emc_complication_note',
            [
                'attribute' => 'type_id',
                'value' => ($model->type_id) ? PacientOperation::itemAlias('type_id', $model->type_id) : '',
            ],
            [
                'attribute' => 'result_id',
                'value' => ($model->type_id) ? PacientOperation::itemAlias('result_id', $model->result_id) : '',
            ],
            'date_at:date',
        ],
    ]) ?>

</div>