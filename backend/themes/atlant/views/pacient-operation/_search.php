<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PacientOperationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pacient-operation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'diagnosis_id') ?>

    <?= $form->field($model, 'handbook_emc_surgery_id') ?>

    <?= $form->field($model, 'handbook_emc_surgery_note') ?>

    <?= $form->field($model, 'handbook_emc_icd_id') ?>

    <?php // echo $form->field($model, 'handbook_emc_icd_note') ?>

    <?php // echo $form->field($model, 'start_operation_date') ?>

    <?php // echo $form->field($model, 'start_operation_time') ?>

    <?php // echo $form->field($model, 'duration_operation_hour') ?>

    <?php // echo $form->field($model, 'duration_operation_minute') ?>

    <?php // echo $form->field($model, 'surgeon_user_id') ?>

    <?php // echo $form->field($model, 'department_id') ?>

    <?php // echo $form->field($model, 'result_id') ?>

    <?php // echo $form->field($model, 'anesthetist_user_id') ?>

    <?php // echo $form->field($model, 'anesthesia_id') ?>

    <?php // echo $form->field($model, 'handbook_emc_complication_id') ?>

    <?php // echo $form->field($model, 'handbook_emc_complication_note') ?>

    <?php // echo $form->field($model, 'type_id') ?>

    <?php // echo $form->field($model, 'date_at') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'updated_user_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
