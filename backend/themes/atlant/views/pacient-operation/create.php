<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PacientOperation */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['/pacient/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Surgeries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pacient-operation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>