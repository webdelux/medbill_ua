<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PacientOperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Surgeries');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacients'), 'url' => ['/pacient/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pacient-operation-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'diagnosis_id',
            'handbook_emc_surgery_id',
            'handbook_emc_surgery_note',
            'handbook_emc_icd_id',
            // 'handbook_emc_icd_note',
            // 'start_operation_date',
            // 'start_operation_time',
            // 'duration_operation_hour',
            // 'duration_operation_minute',
            // 'surgeon_user_id',
            // 'department_id',
            // 'result_id',
            // 'anesthetist_user_id',
            // 'anesthesia_id',
            // 'handbook_emc_complication_id',
            // 'handbook_emc_complication_note',
            // 'type_id',
            // 'date_at',
            // 'user_id',
            // 'updated_user_id',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>