<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\grid\GridView;
use backend\modules\handbook\models\Department;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MedicalSuppliesOperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Movement') . ' ' . Yii::t('app', 'medical supplies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-supplies-operation-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Receive') . ' / ' . Yii::t('app', 'Send') . ' ' . Yii::t('app', 'medical supplies'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'medical-supplies-operation',]); ?>

    <?= GridView::widget([
        'options' => [
            'id' => 'medical-supplies-operation-grid'
        ],
        'export' => false,
        //'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '80'],
                'headerOptions' => ['style' => 'white-space: normal;', 'width' => '80'],
            ],
            [
                'label' => Yii::t('app', 'Medical Supplies'),
                'group'=>true,  // enable grouping
                'attribute' => 'supplies',
                'value' => function ($data)
                {
                    return (isset($data->medicalSuppliesMeasurement)) ? $data->medicalSuppliesMeasurement : '';
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'supplies',
                    'options' => [
                        'class' => 'form-control ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/handbook/medical-supplies/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#medical-supplies-operation-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/medical-supplies-operation/index']) . "?"
                                . Html::getInputName($searchModel, 'medical_supplies_id') . "=' + ui.item.id + '&"
                                . Html::getInputName($searchModel, 'supplies') . "=' + ui.item.value
                            });
                            jQuery('#medical-supplies-operation-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
                'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                    return [
                        'mergeColumns' => [[0,1]], // columns to merge in summary
                        'content' => [             // content to show in each summary cell
                            1 => Yii::t('app', 'Summary') . ':',
                            2 => GridView::F_COUNT,
                            3 => GridView::F_SUM,

                        ],
                        'contentOptions'=>[      // content html attributes for each summary cell
                            0=>['style'=>'font-variant:small-caps; text-align:left'],
                            2=>['style'=>'text-align:right'],
                            3=>['style'=>'text-align:right'],
                        ],
                        // html attributes for group summary row
                        'options'=>['class'=>'info','style'=>'font-weight:bold;']
                    ];
                },
            ],
            [
                'attribute' => 'docum_date',
                'format' => ['date', 'dd MMM Y'],
                'headerOptions' => ['style' => 'white-space:normal;', 'width' => '150'],
                'filter' => \kartik\date\DatePicker::widget([
                    'model'      => $searchModel,
                    'attribute'  => 'docum_date',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',

                    ]
                ]),
            ],
            [
                'label' => Yii::t('app', 'Quantity'),    //(+ надходження,  - витрата)
                'attribute' => 'quantity',
                'contentOptions' => function ($data)
                {
                    return [
                        'width' => '80',
                        'class' => ($data->quantity > 0) ? 'success' : 'danger'
                    ];
                },
                'pageSummary'=>true,
                'pageSummaryOptions'=>['class'=>'text-right text-warning'],
                'pageSummaryFunc'=>GridView::F_SUM,
            ],
            [
                'attribute' => 'total',
                'value' => function ($data)
                {
                    return (isset($data->total)) ? $data->total : '';
                },
            ],
            [
                'attribute' => 'discount',
                'value' => function ($data)
                {
                    return (isset($data->discount)) ? $data->discount : '';
                },
            ],
            [
                'attribute' => 'diagnosis_id',
                'value' => function ($data)
                {
                    return (isset($data->diagnosis_id)) ? $data->diagnosis->icd->name : '';
                },
            ],
            [
                'label' => Yii::t('app', 'Pacient'),
                'attribute' => 'fullName',
                'contentOptions' => ['width' => '200'],
                'value' => function ($data) {
                    return (isset($data->pacient->fullName)) ? $data->pacient->fullName : '';
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'fullName',
                    'options' => [
                        'class' => 'form-control ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/pacient/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#medical-supplies-operation-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/medical-supplies-operation/index']) . "?"
                                . Html::getInputName($searchModel, 'pacient_id') . "=' + ui.item.id + '&"
                                . Html::getInputName($searchModel, 'fullName') . "=' + ui.item.value
                            });
                            jQuery('#medical-supplies-operation-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            [
                'attribute' => 'department_id',
                'value' => function ($data)
                {
                    return (isset($data->department->name)) ? $data->department->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'department_id',
                        (new Department)->treeList(),
                        ['class' => 'form-control', 'prompt' => '']),
            ],
//            [
//                'label' => Yii::t('app', 'Type'),    //(+ надходження,  - витрата)
//                'attribute' => 'type',
//                'contentOptions' => function ($data)
//                {
//                    return [
//                        'width' => '80',
//                        'class' => ($data->quantity > 0) ? 'success' : 'danger'
//                    ];
//                },
//                'value' => function ($data) {
//                    return ($data->quantity > 0) ? Yii::t('app', 'Receive') : Yii::t('app', 'Send');
//                },
//                //'filter' => \yii\helpers\ArrayHelper::map(\backend\models\MedicalSuppliesOperation::findAll(['id' => $this->id]), 'id', 'type'),
//                    //['prompt' => '', '1' => Yii::t('app', 'Receive'), '0' => Yii::t('app', 'Send')]),
//            ],

            // 'docum_num',
            // 'docum',
            // 'description',
            [
                'label' => Yii::t('app', 'User'),
                'attribute' => 'user',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                {
                    $res = (isset($data->userProfile->name)) ? $data->userProfile->name : '';
                    $res .= ' (';
                    $res .= (isset($data->user->username)) ? $data->user->username : '';
                    $res .= ')';

                    return $res;
                },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'user',
                    'options' => [
                        'class' => 'form-control ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#medical-supplies-operation-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/medical-supplies-operation/index']) . "?"
                                                . Html::getInputName($searchModel, 'user_id') . "=' + ui.item.id + '&"
                                                . Html::getInputName($searchModel, 'user') . "=' + ui.item.value
                            });
                            jQuery('#medical-supplies-operation-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            [
                'label' => Yii::t('app', 'Updated User'),
                'attribute' => 'updatedUser',
                'headerOptions' => ['style' => 'white-space:pre-wrap;', 'width' => '80'],
                'value' => function ($data)
                    {
                        $res = (isset($data->updatedUserProfile->name)) ? $data->updatedUserProfile->name : '';
                        $res .= ' (';
                        $res .= (isset($data->updatedUser->username)) ? $data->updatedUser->username : '';
                        $res .= ')';

                        return $res;
                    },
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'updatedUser',
                    'options' => [
                        'class' => 'form-control ms-operation',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/user-viewer/index']),
                        'select' => new JsExpression("function(event, ui) {
                            jQuery('#medical-supplies-operation-grid').yiiGridView({
                                'filterUrl': '" . Url::to(['/medical-supplies-operation/index']) . "?"
                                                . Html::getInputName($searchModel, 'updated_user_id') . "=' + ui.item.id + '&"
                                                . Html::getInputName($searchModel, 'updatedUser') . "=' + ui.item.value
                            });
                            jQuery('#medical-supplies-operation-grid').yiiGridView('applyFilter');
                        }"),
                    ],
                ]),
            ],
            'created_at',
            'updated_at',

            [
                //'class' => 'yii\grid\ActionColumn',
                'class' => 'kartik\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '80'],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php $this->registerCss("

    .ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }

"); ?>


<?php $this->registerJs("
    $(document).on('change', '.ms-operation',
        function(event) {
            if (!$(this).val())
            {
                jQuery('#medical-supplies-operation-grid').yiiGridView({'filterUrl': '" . Url::to(['/medical-supplies-operation/index']) . "'});
                jQuery('#medical-supplies-operation-grid').yiiGridView('applyFilter');
            }
        }
    );

    $(document).on('pjax:timeout', function(event) {
        // Prevent default timeout redirection behavior
        event.preventDefault();
    })

"); ?>
