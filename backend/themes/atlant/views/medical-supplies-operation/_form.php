<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\web\JsExpression;
use backend\modules\handbook\models\Department;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\MedicalSuppliesOperation */
/* @var $form yii\widgets\ActiveForm */
?>

<?php echo Alert::widget(); ?>

<div class="medical-supplies-operation-form">

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-sm-10\">\n{input}\n{hint}\n{error}\n</div>",
                ],
    ]);
    ?>

    <?php echo
    $form->field($model, 'fullName', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(\yii\jui\AutoComplete::classname(), [
            'options' => [
                'class' => 'form-control',
                'value' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null
            ],
            'clientOptions' => [
                'source' => Url::to(['/pacient/index']),
                'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'pacient_id') . "').val(ui.item.id);
            }")
            ],
        ])
    ?>
    <?= Html::activeHiddenInput($model, 'pacient_id') ?>

    <?=
        $form->field($model, 'diagnosis_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput()
    ?>

    <?=
        $form->field($model, 'department_id', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->dropDownList((new Department)->treeList(),
            ['prompt' => ''])
    ?>

    <?php echo
    $form->field($model, 'msupplies', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->widget(\yii\jui\AutoComplete::classname(), [
            'options' => [
                'class' => 'form-control',
                'value' => (isset($model->medicalSuppliesMeasurement)) ? $model->medicalSuppliesMeasurement : null
            ],
            'clientOptions' => [
                'source' => Url::to(['/handbook/medical-supplies/index']),
                'select' => new JsExpression("function(event, ui) {
                $('#" . Html::getInputId($model, 'medical_supplies_id') . "').val(ui.item.id);
            }")
            ],
        ])
    ?>
    <?= Html::activeHiddenInput($model, 'medical_supplies_id') ?>

    <?= $form->field($model, 'quantity', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'discount', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'docum_date', ['labelOptions' => ['class' => 'col-sm-2 control-label']])->textInput()
            ->widget(DatePicker::className(),
            [
            'value' => date('Y-m-d'),
            'pluginOptions' =>
                [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        )
    ?>

    <?= $form->field($model, 'docum_num', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'docum', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'description', ['labelOptions' => ['class' => 'col-sm-2 control-label']])
        ->textInput(['maxlength' => true])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("
    $(document).on('change', '#" . Html::getInputId($model, 'fullName') . "', function(event) {
        if (!$(this).val()) {
            $('#" . Html::getInputId($model, 'pacient_id') . "').val('');
        }
    });
"); ?>


<?php $this->registerCss("
.ui-widget-content .ui-state-focus {
        border: none;
        border-bottom: 1px solid #d5d5d5;
    }
    .ui-autocomplete::after, .ui-autocomplete::before {
        border: none;
    }
"); ?>
