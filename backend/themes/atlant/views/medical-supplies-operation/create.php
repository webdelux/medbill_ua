<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MedicalSuppliesOperation */

$this->title = Yii::t('app', 'Movement') . ' ' . Yii::t('app', 'medical supplies');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Medical Supplies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-supplies-operation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
