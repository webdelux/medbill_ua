<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MedicalSuppliesOperation */

$this->title = $model->medicalSuppliesMeasurement;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Medical Supplies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-supplies-operation-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'pacient_id',
                'value' => (isset($model->pacient->fullName)) ? $model->pacient->fullName : null
            ],
            [
                'attribute' => 'diagnosis_id',
                'value' => (isset($model->diagnosis_id)) ? $model->diagnosis->icd->name : null
            ],
            [
                'attribute' => 'department_id',
                'value' => (isset($model->department->name)) ? $model->department->name : null
            ],
            [
                'attribute' => 'medical_supplies_id',
                'value' => (isset($model->medicalSuppliesMeasurement)) ? $model->medicalSuppliesMeasurement : null
            ],
            'quantity',
            'docum_date',
            'docum_num',
            'docum',
            'description',
            [
                'attribute' => 'user_id',
                'value' => (isset($model->user->username)) ? $model->user->username : null
            ],
            [
                'attribute' => 'updated_user_id',
                'value' => (isset($model->updatedUser->username)) ? $model->updatedUser->username : null
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
