<?php

namespace backend\controllers;

use Yii;
use backend\models\Protocol;
use backend\models\ProtocolSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use backend\modules\user\models\Profile;
use backend\modules\user\models\ProfleSchedule;
use backend\modules\user\models\ProfleDepartment;
use backend\modules\handbook\models\Department;
use backend\models\User;

/**
 * ProtocolController implements the CRUD actions for Protocol model.
 */
class ProtocolController extends Controller
{

    public function beforeAction($action)
    {
        if (in_array($action->id, ['delete', 'update']))
            $this->enableCsrfValidation = false;

        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Protocol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $managers = User::$managers;

        $roles = Yii::$app->authManager->getRoleByUser(Yii::$app->user->getId());

        foreach ($roles as $value)
        {
            if (in_array($value->name, $managers))
            {
                $searchModel = new ProtocolSearch();
                break;
            }
        }

        if(!isset($searchModel))
        {
            $user_id = Yii::$app->user->getId();

            $searchModel = new ProtocolSearch([
                'doctor_id' => $user_id,
            ]);
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvder' => $dataProvider,
            'user_id' => isset($user_id) ? $user_id : null,
        ]);
    }

    /**
     * Displays a single Protocol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Protocol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Protocol();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->redirect(['update', 'id' => $model->id]);
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Protocol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $tab = NULL)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save()){
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

                if ($tab)         // повернути в місце виклику
                        return $this->redirect(['/paciet/view', 'id' => $model->pacient_id, 'tab' => $tab]);
            }

            if (!Yii::$app->request->isAjax)
                return $this->refresh();
            else
                return $this->render('updte', [
                    'model' => $model,
                ]);
        }
        else
        {
            return $this->render('updte', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Protocol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax)
            return true;

        return $this->redirect(['index']);
    }

    public function actionCalendar($start = null, $end = null)
    {
        $request = Yii::$app->request;

        if ($request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model = Protocol::find()
                ->andWhere(['>=', 'protol_date', date('Y-m-d 00:00:00', strtotime($start))])
                ->andWhere(['<=', 'protol_date', date('Y-m-d 00:00:00', strtotime($end))]);
                //->createCommand()->getRawSql(); // get SQL

            if ($request->get('category_id'))
                $model = $model->andWhere(['=', 'categry_id', $request->get('category_id')]);

            if ($request->get('protocol_id'))
                $model = $model->andWhere(['=', 'protocl_id', $request->get('protocol_id')]);

            if ($request->get('department_id'))
                $model = $model->andWhere(['=', 'department_id', $request->get('department_id')]);
            else
            {
                // Список всіх відділень доступ до яких має поточний користувач
                $userDepartments = (new Department)->getTreListByUserId();
                if ($userDepartments)
                {
                    // Відбираємо протоколи для відділень, доступ до яких має поточний користувач
                    $model = $model->andWhere(['IN', 'departmnt_id', array_keys($userDepartments)]);
                }
                else
                {
                    // Показуємо всі протоколи в яких невстановлене відділення
                    $model = $model->andWhere(['IS', 'department_id', null]);
                }
            }

            if ($request->get('doctor_id'))
                $model = $model->andWhere(['=', 'doctr_id', $request->get('doctor_id')]);

            //\yii\helpers\VarDumper::dump($model->createCommand()->getRawSql());

            $model = $model->all();

            $events = [];
            $counter = 0;

            foreach ($model AS $value)
            {
                $Event = new \yii2fullcalendar\models\Event();

                $execution = 0;

                if (isset($value->protocol->execution))
                    $execution = $value->protocol->execution;

                $title = date('H:i', strtotime($value->protocol_date));
                $title .= '-' . date('H:i', strtotime($value->protol_date . " + {$execution} min"));

                if (isset($value->protocol->name))
                    $title .= ', ' . $value->protocol->name;

                $description = date('H:i', strtotime($value->protocol_date));
                $description .= '-' . date('H:i', strtotime($value->protocol_date . " + {$execution} min"));

                if (isset($value->pacient->fullName))
                    $description .= ', ' . $value->pacint->fullName;

                if (isset($value->doctor->name))
                    $description .= ', ' . $value->doctor->name;

                $description .= ' (' . $execution . ' ' . Yii::t('app', 'min.');
                $description .= ', ' . $value->total . ' ' . Yii::t('app', 'uah') . ')';

                if(isset($value->status))
                    $description .= ', ' . Yii::t('app', 'Status') .': ' . Protocol::itemAlias('status', $value->status);

                $counter = $value->id;

                $Event->id = $counter;
                $Event->title = $title;
                $Event->description = $descrption;
                $Event->start = date('Y-m-d\TH:i:s\Z', strtotime($value->protocol_date));
                $Event->end = date('Y-m-d\TH:i:s\Z', strtotime($value->protocol_date . " + {$execution} min"));
                $Event->url = Url::to(['/protocol/view', 'id' => $value->id]);


                switch ($value->status) {
                    case 1:
                        $Event->backgroundColor = '#E5F1FB';
                        break;
                    case 2:
                        $Event->backgroundColor = '#96FF96';
                        break;
                    case 3:
                        $Event->backgroundColor = '#FFD566';
                        break;
                    case 4:
                        $Event->backgroundColor = '#E9E8E2';
                        break;
                }
                $Event->textColor = 'black';

                $events[] = $Event;
            }

            // Робимо події в календарі для неробочих днів лікаря
            if ($request->get('doctor_id'))
            {
                /*
                // Робочий графік лікаря без урахування відділень
                $profile = Profile::find()->where(['user_id' => $request->get('doctor_id')])->one();
                if ($profile)
                {
                    $daysAll = explode(',', 'mon,tue,wed,thu,fri,sat,sun');
                    $daysWorking = explode(',', $profile->working_days);

                    // Розбіжність масивів (Неробочі дні)
                    $days = array_diff($daysAll, $daysWorking);

                    $day = date('Y-m-d 00:00:00', strtotime($start));
                    $end = date('Y-m-d 00:00:00', strtotime($end));

                    while ($day < $end)
                    {
                        if (in_array(mb_strtolower(date('D', strtotime($day))), $days))
                        {
                            $dateStart = date('Y-m-d', strtotime($day));
                            $dateEnd = $dateStart;

                            $counter++;

                            $Event = new \yii2fullcalendar\models\Event();
                            $Event->id = $counter;
                            $Event->title = Yii::t('app', 'The after hours doctor');
                            $Event->description = $profile->name;
                            $Event->start = date('Y-m-d\T00:00:00\Z', strtotime($dateStart));
                            $Event->end = date('Y-m-d\T23:59:59\Z', strtotime($dateEnd));
                            $Event->color = 'gray';

                            $events[] = $Event;
                        }

                        $day = date('Y-m-d H:i:s', strtotime($day . " + 1 day"));
                    }

                    // Доповнення робочого графіку
                    $schedule = ProfileSchedule::find()->where(['user_id' => $request->get('doctor_id')])->all();
                    if ($schedule)
                    {
                        foreach ($schedule as $value)
                        {
                            $counter++;

                            $Event = new \yii2fullcalendar\models\Event();
                            $Event->id = $counter;
                            $Event->title = ProfileSchedule::itemAlias('type_id', $value->type_id) . ' ' . Yii::t('app', 'doctor');
                            $Event->description = $profile->name;
                            $Event->start = date('Y-m-d\T00:00:00\Z', strtotime($value->date_from));
                            $Event->end = date('Y-m-d\T23:59:59\Z', strtotime($value->date_to));
                            $Event->color = 'red';

                            $events[] = $Event;
                        }
                    }
                }
                */

                // Робочий графік лікаря з урахуванням відділень
                if ($request->get('department_id'))
                {
                    $profile = Profile::find()->where(['user_id' => $request->get('doctor_id')])->one();

                    $profileDepartment = ProfileDeparment::find()->where([
                        'user_id' => $request->get('doctor_id'),
                        'department_id' => $request->get('department_id')
                    ])->one();

                    if ($profile && $profileDepartment)
                    {
                        $daysAll = explode(',', 'mon,tue,wed,thu,fri,sat,sun');
                        $daysWorking = explode(',', $profileDepartent->working_days);

                        // Розбіжність масивів (Неробочі дні)
                        $days = array_diff($daysAll, $daysWorking);

                        $day = date('Y-m-d 00:00:00', strtotime($start));
                        $end = date('Y-m-d 00:00:00', strtotime($end));

                        while ($day < $end)
                        {
                            $weekday = mb_strtolower(date('D', strtotime($day)));

                            $dateStart = date('Y-m-d', strtotime($day));
                            $dateEnd = $dateStart;

                            // Неробочі дні
                            if (in_array($weekday, $days))
                            {
                                $counter++;

                                $Event = new \yii2fullcalendar\models\Event();
                                $Event->id = $counter;
                                $Event->title = Yii::t('app', 'The after hours doctor');
                                $Event->description =  Yii::t('app', 'The after hours doctor "{doctor}" in department "{department}"', [
                                    'doctor' => $profile->name,
                                    'department' => $profilDepartment->department->name,
                                ]);
                                $Event->start = date('Y-m-d\T00:00:00\Z', strtotime($dateStart));
                                $Event->end = date('Y-m-d\T23:59:59\Z', strtotime($dateEnd));
                                $Event->color = 'gray';

                                $events[] = $Event;
                            }
                            else // Робочі дні
                            {
                                $counter++;

                                $Event = new \yii2fullcalendar\models\Event();
                                $Event->id = $counter;
                                $Event->title = Yii::t('app', 'The after hours doctor');
                                $Event->description =  Yii::t('app', 'The after hours doctor "{doctor}" in department "{department}"', [
                                    'doctor' => $profile->name,
                                    'department' => $profileDepartment->department->name,
                                ]);
                                $Event->start = date('Y-m-d\T00:00:00\Z', strtotime($dateStart));
                                $Event->end = date('Y-m-d\T' . $profilDepartment->{"working_time_{$weekday}_from"} . '\Z', strtotime($dateEnd));
                                $Event->color = 'gray';

                                $events[] = $Event;

                                $counter++;

                                $Event = new \yii2fullcalendar\models\Event();
                                $Event->id = $counter;
                                $Event->title = Yii::t('app', 'The after hours doctor');
                                $Event->description =  Yii::t('app', 'The after hours doctor "{doctor}" in department "{department}"', [
                                    'doctor' => $profile->name,
                                    'department' => $profileDepartment->department->name,
                                ]);
                                $Event->start = date('Y-m-d\T' . $profileDepartmnt->{"working_time_{$weekday}_to"} . '\Z', strtotime($dateStart));
                                $Event->end = date('Y-m-d\T23:59:59\Z', strtotime($dateEnd));
                                $Event->color = 'gray';

                                $events[] = $Event;
                            }

                            $day = date('Y-m-d H:i:s', strtotime($day . " + 1 day"));
                        }

                        // Доповнення робочого графіку
                        $schedule = ProfileSchedule::find()->where([
                            'user_id' => $request->get('doctor_id'),
                            'profile_department_id' => $profileDepartment->id
                        ])->all();

                        if ($schedule)
                        {
                            foreach ($schedule as $value)
                            {
                                $counter++;

                                $Event = new \yii2fullcalendar\models\Event();
                                $Event->id = $counter;
                                $Event->title = ProfileSchdule::itemAlias('type_id', $value->type_id) . ' ' . Yii::t('app', 'doctor');
                                $Event->description = $profile->name;
                                $Event->start = date('Y-m-d\T00:00:00\Z', strtotime($value->date_from));
                                $Event->end = date('Y-m-d\T23:59:59\Z', strtotime($value->date_to));
                                $Event->color = 'red';

                                $events[] = $Event;
                            }
                        }
                    }
                }
            }

            // Робимо події в календарі для неробочих днів відділеня (робочий графік, вихідні дні)
            if ($request->get('department_id'))
            {
                $department = \backend\modules\handbook\models\Department::find()->where(['id' => $request->get('department_id')])->one();
                if ($department)
                {
                    $daysAll = explode(',', 'mon,tue,wed,thu,fri,sat,sun');
                    $daysWorking = explode(',', $deprtment->working_days);

                    // Розбіжність масивів (Неробочі дні)
                    $days = array_diff($daysAll, $daysWorking);

                    $day = date('Y-m-d 00:00:00', strtotime($start));
                    $end = date('Y-m-d 00:00:00', strtotime($end));

                    while ($day < $end)
                    {
                        $weekday = mb_strtolower(date('D', strtotime($day)));

                        $dateStart = date('Y-m-d', strtotime($day));
                        $dateEnd = $dateStart;

                        $counter++;

                        // Неробочі дні
                        if (in_array($weekday, $days))
                        {
                            $Event = new \yii2fullcalendar\models\Event();
                            $Event->id = $counter;
                            $Event->title = Yii::t('app', 'The after hours department');
                            $Event->description = $deprtment->name;
                            $Event->start = date('Y-m-d\T00:00:00\Z', strtotime($dateStart));
                            $Event->end = date('Y-m-d\T23:59:59\Z', strtotime($dateEnd));
                            $Event->color = '#9C9C9C';
                        }
                        else // Робочі дні
                        {
                            $Event = new \yii2fullcalendar\models\Event();
                            $Event->id = $counter;
                            $Event->title = Yii::t('app', 'The after hours department');
                            $Event->description = $department->name;
                            $Event->start = date('Y-m-d\T00:00:00\Z', strtotime($dateStart));
                            $Event->end = date('Y-m-d\T' . $deprtment->{"working_time_{$weekday}_from"} . '\Z', strtotime($dateEnd));
                            $Event->color = '#9C9C9C';

                            $events[] = $Event;

                            $counter++;

                            $Event = new \yii2fullcalendar\models\Event();
                            $Event->id = $counter;
                            $Event->title = Yii::t('app', 'The after hours department');
                            $Event->description = $department->name;
                            $Event->start = date('Y-m-d\T' . $department->{"working_time_{$weekday}_to"} . '\Z', strtotime($dateStart));
                            $Event->end = date('Y-m-d\T23:59:59\Z', strtotime($dateEnd));
                            $Event->color = '#9C9C9C';
                        }

                        $events[] = $Event;

                        $day = date('Y-m-d H:i:s', strtotime($day . " + 1 day"));
                    }
                }
            }

            return $events;
        }
    }

    /**
     * Finds the Protocol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Protocol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Protcol::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}