<?php

namespace backend\controllers;

use Yii;
use backend\models\Stacionary;
use backend\models\StacionarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use \backend\modules\handbook\models\HandbookStacionary;

/**
 * StacionaryController implements the CRUD actions for Stacionary model.
 */
class StacionaryController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stacionary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StacionarySearch();
        //$searchModel->type = 1;                 // фильтр по замовчанню
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stacionary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stacionary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = NULL, $id = NULL)
    {
        $model = new Stacionary();

        $model->type = 1;       //прибув. Вибув тільчи через update потрібного запису

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Stacionary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($type = NULL, $id, $tab = NULL)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($tab)         // повернути в місце виклику
                    return $this->redirect(['/pacient/view', 'id' => $model->pacient_id, 'tab' => $tab]);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if ($type){    // type = 2 виписати пацієнта
                $model->type = 2;
                $model->date_stacionary_out = date("Y-m-d H:i:s");
                $model->save(false);

                if (Yii::$app->request->isAjax)
                    return true;

                return $this->redirect(['index']);
            }
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Stacionary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);         //шукаємо запис, що видаляємо
        $place = $model->place_id;
        $type = $model->type;

        $model->delete();

        if ($type == 1)
            $model->setStatus($place, 2);

        if (Yii::$app->request->isAjax)
            return true;

        return $this->redirect(['index']);
    }

    /**
     * Finds the Stacionary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stacionary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stacionary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
