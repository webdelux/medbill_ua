<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use backend\models\User;
use backend\modules\handbook\models\Department;

class RegistryController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function actionIndex()
    {
        $model = new \backend\models\Protocol();

        // Список всіх користувачів з відділень доступ до яких має поточний користувач
        $doctors = User::getListByDepartments(array_keys((new Department)->getListByUserId()));

        // Chained Selects Plugin
        $doctorsOptions = [];
        foreach ($doctors as $key => $value)
            $doctorsOptions[$key] = ['class' => implode(' ', array_keys((new Department)->getListParentsByUserId($key)))];

        return $this->render('index', [
            'model' => $model,
            'doctors' => $doctors,
            'doctorsOptions' => $doctorsOptions,
        ]);
    }

}