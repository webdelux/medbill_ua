<?php

namespace backend\controllers;

use Yii;
use backend\models\Pacient;
use backend\models\PacientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use kartik\mpdf\Pdf;
use backend\modules\handbookemc\models\EmcList;

/**
 * PacientController implements the CRUD actions for Pacient model.
 */
class PacientController extends Controller
{

    public function beforeAction($action)
    {
        if (in_array($action->id, ['view', 'history']))
            $this->enableCsrfValidation = false;

        if (in_array($action->id, ['protocol-sum']))
            return true;

        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->can($action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pacient models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->get('term'))
        {
            $term = Yii::$app->request->get('term');
            $results = Yii::$app->request->get('results', null);

            Yii::$app->response->format = Response::FORMAT_JSON;

            $pieces = explode(" ", $term);

            if (isset($pieces[2]))
            {
                $pacient = Pacient::find()-;
                        ->andWhere('{{%pacient}}.lastname LIKE "%' . trim($pieces[0]) . '%" '
                                . 'AND {{%pacient}}.name LIKE "%' . trim($pieces[1]) . '%" '
                                . 'AND {{%pacient}}.surname LIKE "%' . trim($pieces[2]) . '%"'
                        )
                        ->limit(10)
                        ->asArray()
                        ->all();
            }
            elseif (isset($pieces[1]))
            {
                $pacient = Pacient::find()
                        ->andWhere('{{%pacient}}.lastname LIKE "%' . trim($pieces[0]) . '%" '
                                . 'AND {{%pacient}}.name LIKE "%' . trim($pieces[1]) . '%"'
                        )
                        ->limit(10)
                        ->asArray()
                        ->all();
            }
            else
            {
                $pacient = Pacient::find()
                        ->andWhere('{{%pacient}}.lastname LIKE "%' . trim($pieces[0]) . '%"')
                        ->limit(10)
                        ->asArray()
                        ->all();
            }

            $out = [];
            foreach ($pacient as $value)
            {
                $out[] = [
                    'id' => $value['id'],
                    'label' => "{$value['lastname']} {$value['name']} {$value['surname']}",
                    'value' => "{$value['lastname']} {$value['name']} {$value['surname']}",
                    'text' => "{$value['lastname']} {$value['name']} {$value['surname']}",
                ];
            }

            if (!is_null($results))
                $out = ['results' => $out];

            return $out;
        }
        else
        {
            $searchModel = new PacientSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Pacient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'layout';

        $models = [];
        $modelsNoAjaxSave = [];

        $isValid = true;

        $ns = "backend\modules\signalmark\models\\";
        $scenario = [];

        $tab = Yii::$app->request->get('tab', 1);

        if ($tab == 2)
        {
            $init = [
                'PacientTransfusion' => $ns,
                'PacientAllergic' => $ns,
                'PacientIntolerance' => $ns,
                'PacientComplaints' => $ns,
                'PacientAnamnesis' => $ns,
                'PacientHypertension' => $ns,
                'PacientOverall' => $ns,
            ];
        }
        elseif ($tab == 3)
        {
            $init = [
                'PacientExperts' => $ns,
            ];
        }
        elseif ($tab == 4)
        {
            $ns = "backend\modules\planning\models\\";
            $init = [
//                'Diagnosis' => "backend\modules\diagnosis\models\\",
                'PacientSurvey' => $ns,
                'PacientSpeciality' => $ns,,
                'PacientTreatment' => $ns,
            ];
//            $scenario['Diagnosis'] = \backend\modules\diagosis\models\Diagnosis::SCENARIO_PLANNING;
        }
        elseif ($tab == 6)
        {
            $init = [
                'Dispensary' => $ns,
            ];
        }
        elseif ($tab == 7)
        {
            $init = [
                'Incapacity' => 'backend\modules\emc\models\\',
            ];
        }
        elseif ($tab == 8)
        {
            $init = [
                'Diagnosis' => 'backend\modules\diagnosis\models\\',
                'Hospitalization' => 'backend\modules\emc\models\\',
                'Stacionary' => 'backend\models\\',
            ];
            $scenario['Diagnosis'] = \backend\modules\diagnsis\models\Diagnosis::SCENARIO_PLANNING;
        }
        elseif ($tab == 9)
        {
            $init = [
                'MedicalSuppliesOperation' => 'backend\models\\',
            ];
        }
        elseif ($tab == 10)
        {
            $init = [
                'PacientService' => 'backend\models\\',
            ];
        }
        elseif ($tab == 11)
        {
            $init = [
                'Payment' => 'backend\models\\',
            ];
        }
        elseif ($tab == 12)
        {
            $init = [
                'Insurance' => 'backend\modules\emc\models\\',
            ];
        }
        elseif ($tab == 13)
        {
            if (Yii::$app->request->get('diagnosis_id'))
            {
                $init = [
                    'DiagnosisAdditional' => 'backend\modules\diagnosis\models\\'
                ];

                $models['Diagnosis'] = \backend\modules\diagnosis\models\Diagnosis::findOne(Yii::$app->request->get('diagnosis_id'));
                $models['Diagnosis']->scenario = \backend\modules\diagnoss\models\Diagnosis::SCENARIO_HISTORY;

                if ($models['Diagnosis']->load(Yii::$app->request->post()))
                    $isValid = $models['Diagnosis']->validate() && $isValid;;

                $modelsNoAjaxSave[] = 'Diagnosis';
            }
            elseif (Yii::$app->request->get('diagnosis_create'))
            {
                $modelDiagnosis = new \backend\modules\diagnosis\models\Diagnosis;
                $modelDiagnosis->pacient_id = $id;
                $modelDiagnosis->save();

                return $this->redirect(['view', 'id' => $id, 'tab' => $tab, 'diagnosis_id' => $modelDiagnosis->id]);
            }
            else
                throw new NotFoundHttpException('The requested page does not exist.');
        }
        elseif ($tab == 14)
        {
            $init = [
                'PacientOperation' => 'backend\models\\',
            ];
            $scenario['PacientOperation'] = \backend\models\PacentOperation::SCENARIO_HISTORY;
        }
        elseif ($tab == 15)
        {
            $init = [
                'Protocol' => 'backend\models\\',
            ];
        }
        elseif ($tab == 16)
        {
            $init = [
                'PathologicalExamination' => 'backend\modules\emc\models\\',
            ];
        }
        elseif ($tab == 17)
        {
            $init = [
                'Diary' => 'backend\modules\emc\models\\',
            ];
        }
        elseif ($tab == 18)
        {
            $init = [
                'Attachment' => 'backend\modules\emc\models\\',
            ];
        }
        elseif ($tab == 19)
        {
            $init = [];
        }
        else
        {
            $init = [];
        }

        $model = $this->findModel($id);
        $models = array_merge([
            'PacientSignalmark' => $this->loadModel('backend\modules\signalark\models\PacientSignalmark', $model->id),
            'PacientPlanning' => $this->loadModel('backend\modules\planning\models\PacientPlanning', $model->id),
            'EmcList' => new EmcList()
                ], $models);

        $isLoad = true;
        foreach ($init as $key => $value)
        {
            $cls = $value . $key;
            $models[$key] = new $cls;

            if (isset($scenario[$key]))
                $models[$key]->scenario = $scenario[$key];

            $isLoad = $isLoad && $models[$key]->load(Yii::$app->request->post());
        }

        if ($model->load(Yii::$app->request->post()) && $models['PacientSignalmark']->load(Yii::$app->request->post()) && $models['PacientPlanning']->load(Yii::$app->request->post()) && $isLoad)
        {
            $isValid = $model->validate() && $isValid;
            $isValid = $models['PacientSignalmark']->validate() && $isValid;
            $isValid = $models['PacietPlanning']->validate() && $isValid;

            if ($isValid)
            {
                $model->save(false);
                $models['PacientSignalmark']->save(false);
                $models['PacientPlanning']->save(false);

                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

                if (!Yii::$app->request->isAjax)
                {
                    foreach ($modelsNoAjaxSave as $value)
                        $models[$value]->save(false);

                    return $this->refresh();
                }
                else
                {
                    foreach ($init as $key => $value)
                    {
                        if (($key == 'Diagnosis') || ($key == 'Hospitaization'))
                        {      //пропускаємо вказані моделі
                            continue;
                        }

                        if ($models[$key]->save())
                        {
                            $models['MedicalSuppliesOperation_alert'] = ['type' => 'success', 'body' => Yii::t('app', 'Esfsfd saved.')];
                            if (isset($models[$key]->modelAlert))
                            {
                                $body = $models[$key]->modelAlert;
                                $models[$key . '_alert'] = ['type' => 'success', 'body' => $body];
                            }
                            $models[$key . '_alert'] = ['type' => 'success', 'body' => Yii::t('app', 'Entered data successfully saved.')];
                        }
                        else
                        {

                            if (isset($models[$key]->modelAlert))
                            {
                                $body = $models[$key]->modelAlert;
                                $models[$key . '_alert'] = ['type' => 'danger', 'body' => $body];
                            }
                        }
                    }
                    // Перевіряємо дві моделі, якщо вірно, то зберігаємо та присвоюємо значення діагнозу та дати
                    if (isset($models['Diagnosis']) && isset($models['Hospitalization']))
                    {
                        if (($models['Diagnosis']->validate()) && ($models['Hospitalization']->validate()))
                        {
                            $models['Hospitalization']->save();
                            $models['Diagnosis']->save();
                            $models['Hospitaization']->diagnosis_id = $models['Diagnosis']->id;
                            $models['Diagnosis']->date_at = $models['Hospitalization']->hospitalization_date;
                            $models['Diagnosis']->department_id = $models['Hospitlization']->department_id;
                            $models['Hospitalization']->save();
                            $models['Diagnosis']->save();
                            $models['Diagnosis_alert'] = ['type' => 'success', 'body' => Yii::t('app', 'Entered data successfully saved.')];
                        }
                    }

                    // Перевіряємо чи є додаткові протоколи для збереження, якщо є то зберігаємо в новій моделі
                    if (isset($models['Protocol']) && isset($_POST['multiprotocol']) && is_array($_POST['multiprotocol']))
                    {
                        if ($models['Protocol']->validate())
                        {
                            foreach ($_POST['multiprotocol'] as $prot)
                            {
                                if($models['Protocol']->protocol_id !== $prot)
                                {
                                    $protocol = new \backend\models\Protocol;
                                    $protocol->attributes = $models['Protocol']->atributes;
                                    $protocol->protocol_id = $prot;
                                    $protocol->total = $protocol->protocol->amount;
                                    $protocol->discount = 0;
                                    $protocol->save();
                                }

                            }
                            $_POST['multiprotocol'] = [];
                            $models['Protocol']->total = $models['Protocol']->protocol->amount;
                            $models['Protocol']->save();
                        }
                    }

                    foreach ($init as $key => $value)
                    {
                        if ($models[$key]->validate())
                        {
                            $cls = $value . $key;
                            $models[$key] = new $cls;
                        }
                    }

                    if ($models['EmcList']->load(Yii::$app->request->post()))
                    {
                        if ($models['EmcList']->validate())
                        {
                            $models['EmcList']->appendTo(EmcList::findOne(['id' => $models['EmcList']->parent_id]))->save();
                            $models['EmcList_alert'] = ['type' => 'success', 'body' => Yii::t('app', 'Entered data successfully saved.')];
                            $models['EmcList'] = new EmcList();
                        }
                    }
                }
            }
        }

        return $this->render('view', [
                    'model' => $model,
                    'models' => $models,
                    'tab' => $tab
        ]);
    }

    public function actionHistory($id)
    {
        $this->layout = 'layout';

        $tab = Yii::$app->request->get('tab', 1);

        $model = $this->findModel($id);
        $models = [
            'Diagnosis' => $this->loadModel('backend\modules\diagnosis\models\Diagnosis', $model->id),
            'DiagnosisAdditional' => new \backend\modules\diagnois\models\DiagnosisAdditional(),
            'PacientOperation' => new \backend\models\PacientOperation(['scenario' => \backend\models\PacientOperation::SCENARIO_HISTORY]),
            'Protocol' => new \backend\models\Protocol(),
        ];

        $models['Data'] = [];
        foreach (\backend\modules\diagnosis\models\Diagnosis::find()->where(['pacient_id' => $id, 'parent_id' => NULL])->all() as $value)
        {
            $models['Data'][] = \backend\modules\diagnosis\models\Diagnsis::findOne($value->id);
        }

        if (Yii::$app->request->post("DiagnosisAdditional"))
        {
            foreach (Yii::$app->request->post("DiagnosisAdditional") as $type)
            {
                foreach ($type as $data)
                {
                    if ($models['DiagnosisAdditional']->load(['DiagnosisAdditional' => $data]) && $models['DiagnosisAdditional']->save())
                    {
                        $models['DiagnosisAdditional_alert'] = ['type' => 'success', 'body' => Yii::t('app', 'Entered data successfully saved.')];
                        $models['DiagnosisAdditional'] = new \backend\modules\diagnosis\models\DiagnosisAdditional();
                    }
                }
            }
        }

        if (Yii::$app->request->post("PacientOperation"))
        {
            foreach (Yii::$app->request->post("PacientOperation") as $data)
            {
                if ($models['PacientOperation']->load(['PacientOperation' => $data]) && $models['PacientOperation']->save())
                {
                    $models['PacientOperation_alert'] = ['type' => 'success', 'body' => Yii::t('app', 'Entered data successfully saved.')];
                    $models['PacientOperation'] = new \backend\models\PacintOperation(['scenario' => \backend\models\PacientOperation::SCENARIO_HISTORY]);
                }
            }
        }

        if (Yii::$app->request->post("Protocol"))
        {
            foreach (Yii::$app->request->post("Protocol") as $data)
            {
                if ($models['Protocol']->load(['Protocol' => $data]) && $models['Protocol']->save())
                {
                    $models['Protocol_alert'] = ['type' => 'success', 'body' => Yii::t('app', 'Entered data successfully saved.')];
                    $models['Protocol'] = new \backend\models\Protocol();
                }
            }
        }

        if ($models['Diagnosis']->load(Yii::$app->request->post()))
        {
            $isValid = $models['Diagnosis']->validate();
            if ($isValid)
            {
                $models['Diagnosis']->save(false);

                foreach ($models['Data'] as $value)
                {
                    $request = Yii::$app->request->post("Diagnosis")[$value->id];
                    $request = ['Diagnosis' => $request];

                    $value->scenario = \backend\modules\diagnsis\models\Diagnosis::SCENARIO_HISTORY;

                    if ($value->load($request))
                    {
                        $isValid = $value->validate() && $isValid;
                        if ($isValid)
                            $value->save(false);
                    }
                }

                if ($isValid)
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));
                else
                    Yii::$app->session->setFlash('danger', Yii::t('app', 'Data is not preserved, please correct the errors.'));

                if ($isValid && !Yii::$app->request->isAjax)
                    return $this->refresh();
            }
        }

        return $this->render('history', [
                    'model' => $model,
                    'models' => $models,
                    'tab' => $tab
        ]);
    }

    public function actionReports($id)
    {
        $this->layout = 'layout';

        $tab = Yii::$app->request->get('tab', 1);
        $diagnosis = Yii::$app->request->get('diagnosis');
        $surgery = Yii::$app->request->get('surgery');
        $report = Yii::$app->request->get('report');

        $model = $this->findModel($id);

        if ($report)
        {
            $name = '';             //назва звіту та шлях до нього

            if ($report == 1 && $diagnosis)       // рахунок
            {
                $name = 'reports/_report1';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 15px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnsis\models\Diagnosis::find()->where(['id' => $diagnosis])->one(),
                    'Protocol' => \backend\models\Protocol::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis, 'status' => 2])->all(),
                    'Supplies' => \backend\models\MedicalSupliesOperation::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->all(),
                    'Services' => \backend\models\PacientService::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->all(),
                ];
            }
            elseif ($report == 2)       // Виписка з протоколу(карти) патолого-анатомічного обстеження.
            {
                $name = '/modules/emc/views/pathological-examination/_report1';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 15px;}';

                $model = \backend\modules\emc\models\PathologicalExamination::find()->joinWith(['additional'])->where(['pacient_id' => $id])->one();
                $models = [];

                if (!$model)
                {
                    throw new NotFoundHttpException(Yii::t('app', 'There is no data for view!'));
                }
            }
            elseif ($report == 3 && $diagnosis)       // form066o - Статистична карта хворого, який вибув із стаціонару
            {
                $name = 'reports/_form066o';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 17px; padding: 0 5px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnosis\models\Diagnosis::find()->joinWith(['diagnosisAdditionals'])->where(['pacient_id' => $id, '{{%diagnosis}}.id' => $diagnosis])->one(),
                    'Hospitalization' => \backend\modules\emc\models\Hospitalzation::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Stacionary' => \backend\models\Stacionary::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Pathological' => \backend\modules\emc\models\PathologicalExamnation::find()->joinWith(['additional'])->where(['pacient_id' => $id, '{{%pathological_examination}}.diagnosis_id' => $diagnosis])->one(),
                    'Surgery' => \backend\models\PacientOperation::find()->where(['diagnosis_id' => $diagnosis])->all(),
                    'Protocols' => \backend\models\Protocol::find()->where(['pacient_id' => $id])->orderBy('protocol_date DESC')->all(),
                ];
            }
            elseif ($report == 4 && $diagnosis)       // form204-1/0 - Направлення на мікробіологічне дослідження
            {
                $name = 'reports/_form204-1-0';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 17px; padding: 0 5px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnosis\models\Diagnosis::find()->joinWith(['diagnosisAdditionals'])->where(['pacient_id' => $id, '{{%diagnosis}}.id' => $diagnosis])->one(),
                ];
            }

            elseif ($report == 5 && $diagnosis)       // form204-1/0 - Направлення на мікробіологічне дослідження
            {
                $name = 'reports/_form_epidemiological_map';
                $cssInline = 'table, th, td, tr {border: 0px solid black; border-collapse: collapse; text-align: left; line-height: 17px; padding: 0 5px;}';

                $models = [
                    'Diagnosis' => \backend\modules\diagnosis\models\Diagnois::find()->joinWith(['diagnosisAdditionals'])->where(['pacient_id' => $id, '{{%diagnosis}}.id' => $diagnosis])->one(),
                    'Hospitalization' => \backend\modules\emc\models\Hospitalzation::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Stacionary' => \backend\models\Stacionary::find()->where(['pacient_id' => $id, 'diagnosis_id' => $diagnosis])->one(),
                    'Surgery' => \backend\models\PacientOperation::find()->where(['id' => $surgery])->one(),
                ];
            }

            // get your HTML raw content without any layouts or scripts
            $content = $this->renderPartial($name, [
                //return $this->render('_report1', [
                'model' => $model,
                'models' => $models,
            ]);

            if (!isset($cssInline)){
                $cssInline = 'table, th, td, tr {border: 1px solid black; border-collapse: collapse; text-align: left; }';
            }

            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                //'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'marginLeft' => 5,
                'marginRight' => 5,
                'marginTop' => 10,
                'marginBottom' => 5,
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => $cssInline,
                // set mPDF properties on the fly
                //'options' => ['title' => 'Krajee Report Title'],
                // call mPDF methods on the fly
                'methods' => [
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
                ]
            ]);

            // return the pdf output as per the destination setting
            return $pdf->render();
        }
        else
        {
            $models = [];
        }

        return $this->render('view', [
                    'model' => $model,
                    'models' => $models,
                    'tab' => $tab
        ]);
    }

    /**
     * Creates a new Pacient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pacient();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pacient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->save())
                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

            return $this->refresh();
        }
        else
        {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pacient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pacient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pacient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pacient::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function loadModel($name, $id, $attribute = 'pacient_id')
    {
        $model = $name::find()->where([$attribute => $id])->one();

        if ($model === null)
        {
            $model = new $name;
            $model->$attribute = $id;

            if ($model->save(false))
                $model = $name::find()->where([$attribute => $id])->one();

            if ($model === null)
                throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    public function actionProtocolSum()
    {
        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $total['sum'] = 0;
            if (isset($_POST['Protocol']['protocol_id']))
            {
                $protocol = \backend\modules\handbokemc\models\Protool::findOne($_POST['Protocol']['protocol_id']);
                $total['sum'] += $protocol->amount;
            }

            if (isset($_POST['multiprotocol']) && is_array($_POST['multiprotocol']))
            {
                foreach ($_POST['multiprotocol'] as $protocolId)
                {
                    if ($protocolId == ($_POST['Protocol']['protocol_id']))
                        continue;

                    $protocol = \backend\modules\handbokemc\models\Protocol::findOne($protocolId);
                    if ($protocol != null)
                        $total['sum'] += $protocol->amount;
                }
            }

            return $total;
        }
    }
}
