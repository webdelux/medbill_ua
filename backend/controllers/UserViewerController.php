<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;

use backend\models\User;
use backend\models\UserSearch;

use backend\modules\user\models\Profile;
use backend\modules\user\models\ProfileSchedule;
use backend\modules\user\models\ProfileScheuleSearch;
use backend\modules\user\models\ProfileSpeciality;
use backend\modules\user\models\ProfileDepartment;

use backend\modules\handbook\models\Speciality;

use dektrium\rbac\models\Assignment;

use yii\web\UploadedFile;

/**
 * UserViewerController implements the CRUD actions for User model.
 */
class UserViewerController extends Controller
{

    public $layout = 'layout1';

    public function beforeAction($action)
    {
        if (parent::beforeAction($action))
        {
            // Дозволяємо поточному користувачу редагувати свій профіль обминаючи права на доступ
            if ($action->id == 'update' && Yii::$app->request->get('id', null) == Yii::$app->user->id)
                return true;

            if ($action->id == 'create-schedule' && Yii::$app->request->get('user_id', null) == Yii::$app->user->id)
                return true;

            if ($action->id == 'delete-schedule' && Yii::$app->request->get('user_id', null) == Yii::$app->user->id)
                return true;

            if (!\Yii::$app->user->can($action->controller->id . '_' . $action->id))
            {
                throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $term = Yii::$app->request->get('term', null);
            $results = Yii::$app->request->get('results', null);
            $speciality = Yii::$app->request->get('speciality', null);

            $out = [];

            if (!is_null($term))
            {
                $user = Profile::find()
                        ->with('user')
                        ->andWhere('{{%profile}}.name LIKE "%' . trim($term) . '%" ');

                if (!is_null($speciality))
                {
                    $specialityModel = new Speciality;

                    if ($speciality == 'surgeons')
                    {
                        $user = $user->innerJoin('{{%profile_speiality}} AS profile_speciality', '{{%profile}}.user_id = profile_speciality.user_id')
                                ->andWhere(['IN', 'profile_speciality.speciaity_id', $specialityModel->getSurgeons()]);
                    }

                    if ($speciality == 'anesthetists')
                    {
                        $user = $user->innerJoin('{{%profile_speciality}} AS profile_speciality', '{{%profile}}.user_id = profile_specality.user_id')
                                ->andWhere(['IN', 'profile_specility.speciality_id', $specialityModel->getAnesthetists()]);
                    }
                }

                $user = $user->limit(10)
                        ->asArray()
                        ->all();

                foreach ($user as $value)
                {
                    $out[] = [
                        'id' => $value['user_id'],
                        'label' => $value['name'] . ' (' . $value['user']['userame'] . ')',
                        'text' => $value['name'],
                        'value' => $value['name'],
                    ];
                }
            }

            if (!is_null($results))
                $out = ['results' => $out];

            return $out;
        }
        else
        {
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searhModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'layout';

        $user = new User();
        $user->scenario = 'create';

        $profile = new Profile();
        $profileSpeciality = new ProfileSpecility();
        $profileDepartment = new ProfileDeprtment();

        $profileSpeciality->load(Yii::$app->request->post());
        $profileDepartment->load(Yii::$app->request->post());

        $searchModel = new ProfileScheduleSearch();
        $dataProfileSchedule = $searchModel->search([$searchModel->formName() => [
            'user_id' => null
        ]]);

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()))
        {
            $isValid = $user->validate();
            $isValid = $profile->validate() && $isValid;

            if ($isValid)
            {
                $profile->imageFile = UploadedFile::getInstance($profile, 'imagFile');

                if (isset($profile->imageFile)){

                    if ($profile->upload()) {

                        $profile->avatar_url .= $profile->imageFile->name;
                    }
                    else
                    {

                        Yii::$app->session->setFlash('danger', $profile->errors['imageFile'][0]);
                        return $this->refresh();
                    }
                }

                if ($user->save(false))
                {
                    $profile->user_id = $user->id;
                    $profile->save(false);

                    if ($profileSpeciality->speciality_id)
                    {
                        ProfileSpeciality::deleteAll(['user_id' => $user->id]);

                        if (is_array($profileSpeciality->speciality_id))
                        {
                            foreach ($profileSpeciality->speciality_id as $value)
                            {
                                $model = new ProfilSpeciality();
                                $model->user_id = $user->id;
                                $model->speciality_id = $value;
                                $model->save();
                            }
                        }
                    }

                    if ($profileDepartment->department_id)
                    {
                        ProfileDepartment::deleteAll(['user_id' => $user->id]);

                        if (is_array($profileDepartment->department_id))
                        {
                            foreach ($profileDepartment->department_id as $value)
                            {
                                $model = new ProfileDepartment();
                                $model->user_id = $user->id;
                                $model->department_id = $value;
                                $model->save();
                            }
                        }
                    }

                    Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

                    return $this->redirect(['update', 'id' => $user->id]);
                }
            }
        }
        else
        {
            if (is_string($profile->working_days))
                $profile->working_days = explode(',', $profile->working_days);
        }

        return $this->render('create', [
            'user' => $user,
            'profile' => $profile,
            'profileSpecality' => $profileSpeciality,
            'profileDepartment' => $profileDepartment,
            'dataProleSchedule' => $dataProfileSchedule,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'layout';

        $user = $this->findModel($id);

        // Перевіряємо чи у редагованого профілі є права адміністратора
        $isAdmin = false;
        foreach ($user->authAssignment as $value)
            if ($value->item_name == Yii::$app->params['roleAdmin'])
                $isAdmin = true;

        // Перевіряємо чи у редагованого профілі є права адміністратора,
        // якщо є, а у користувача який редагує немає - забороняємо доступ.
        if ($isAdmin && !Yii::$app->user->identity->isAdmin)
            throw new ForbiddenHttpException(Yii::t('app', 'You are not authorized to perform this action.'));

        $profile = $user->profile;
        if ($profile == null)
        {
            $profile = Yii::createObject(Profile::className());
            $profile->link('user', $user);
        }

        $assignment = Yii::createObject(['class' => Assignment::className(), 'user_id' => $id]);
        if ($assignment->load(\Yii::$app->request->post()))
            $assignment->updateAssignments();

        $searchModel = new ProfileScheduleSearch();
        $dataProfileSchedule = $searchModel->search([$searchModel->formName() => [
            'user_id' => $id
        ]]);

        $profileSpeciality = new ProfileSpeciality();
        if ($profileSpeciality->load(Yii::$app->request->post()))
        {
            ProfileSpeciality::deleteAll(['user_id' => $id]);

            if (is_array($profileSpecality->speciality_id))
            {
                foreach ($profileSpecality->speciality_id as $value)
                {
                    $model = new ProfileSpeciality();
                    $model->user_id = $id;
                    $model->speciality_id = $value;
                    $model->save();
                }
            }
        }
        else
        {
            $profileSpeciality->speciality_id = ArrayHelper::map(ProfileSpecality::findAll(['user_id' => $id]), 'id', 'speciality_id');
        }

        $profileDepartment = new ProfileDepartment();
        if ($profileDepartment->load(Yii::$app->request->post()))
        {
            //ProfileDepartment::deleteAll(['user_id' => $id]);
            $pds = ProfileDepartment::findAll(['user_id' => $id]);
            foreach ($pds as $pd)
                $pd->delete();

            if (is_array($profileDepartment->department_id))
            {
                foreach ($profileDepartment->department_id as $value)
                {
                    $model = new ProfileDepartment();
                    $model->user_id = $id;
                    $model->department_id = $value;

                    if (isset(Yii::$app->request->post('ProfileDepartment')[$value]))
                    {
                        $model->working_days = implode(',', Yii::$app->request->post('ProfileDepartment')[$value]['working_days']);

                        foreach (ProfilDepartment::itemAlias('working_days') as $day => $n)
                        {
                            $model->{"working_time_{$day}_from"} = Yii::$app->request->post('ProfileDepartment')[$value]["working_time_{$day}_from"];
                            $model->{"working_time_{$day}_to"} = Yii::$app->request->post('ProfilDepartment')[$value]["working_time_{$day}_to"];
                        }
                    }

                    $model->save();
                }
            }
        }
        else
        {
            $profileDepartment->department_id = ArrayHelper::map(ProfileDepartmnt::findAll(['user_id' => $id]), 'id', 'department_id');
        }

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()))
        {
            $isValid = $user->validate();
            $isValid = $profile->validate() && $isValid;

            if ($isValid)
            {
                $profile->image = UploadedFile::getInstance($profile, 'image');

                if (isset($profile->image)){

                    if ($profile->upload()) {

                        $profile->avatar_url .= 'thumb_' . $profile->user_id . '.' . $profile->image->getExtension();
                    }
                    else
                    {
                        if (isset($profile->errors['image'][0]))
                            Yii::$app->session->setFlash('danger', $profile->errors['image'][0]);

                        return $this->refresh();
                    }
                }
                $user->save(false);
                $profile->save(false);

                Yii::$app->session->setFlash('success', Yii::t('app', 'Entered data successfully saved.'));

                return $this->refresh();
            }
        }
        else
        {
            if (is_string($profile->working_days))
                $profile->working_days = explode(',', $profile->working_days);
        }

        return $this->render('update', [
            'user' => $user,
            'profile' => $profile,
            'profilSpeciality' => $profileSpeciality,
            'profileDepartment' => $profileDepatment,
            'dataProfileSchedule' => $dataProfileSchedule,
            'assignment' => $assignment
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteSchedule($id, $user_id)
    {
        if (($model = ProfileSchedule::find()->where(['id' => $id, 'user_id' => $user_id])->one()) !== null)
        {
            return $model->delete();
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreateSchedule($user_id, $profile_department_id)
    {
        $model = new ProfileSchedule();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $model->user_id = $user_id;
            $model->profile_departent_id = $profile_department_id;

            return $model->save();
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}