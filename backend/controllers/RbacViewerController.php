<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;

use yii\filters\AccessControl;
use common\filters\AccessRule;

use backend\models\AuthItem;
use backend\models\AuthItemChild;

use yii\web\ForbiddenHttpException;

class RbacViewerController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $items = [];
        $i = 1;

        $authItem = AuthItem::find()->where('type = 2')->all();
        foreach ($authItem as $value)
        {
            $items[] = [
                'id' => $i,
                'text' => $value->name,
                'parent' => 0,
            ];
            $i++;
        }

        foreach ($items as $key => $value)
        {
            $authItemChild = AuthItemChild::find()->joinWith('parent0')->where(['child' => $value['text']])->one();
            if ($authItemChild !== null && $authItemChild->parent0->type == 2)
            {
                $items[$key] = array_merge($items[$key], [
                    'parent' => $this->getParent($items, $authItemChild->parent)
                ]);
            }
        }

        return $this->render('index', [
            'tree' => $this->getTree($items),
        ]);
    }

    protected function getParent($items, $text)
    {
        foreach ($items as $value)
        {
            if ($value['text'] == $text)
                return $value['id'];
        }
    }

    protected function getTree($res)
    {
        $levels = array();
        $tree = array();
        $cur = array();

        foreach ($res as $rows)
        {
            $cur = &$levels[$rows['id']];
            $cur['parent'] = $rows['parent'];
            $cur['text'] = $rows['text'];

            if ($rows['parent'] == 0)
            {
                $tree[$rows['id']] = &$cur;
            }
            else
            {
                $levels[$rows['parent']]['nodes'][$rows['id']] = &$cur;
            }
        }

        return $tree;
    }

}