<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public $layout = 'base';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'transparent' => false,
                //'foreColor'=>0xE16020, //цвет символов
                'backColor' => 0x4A5560, //цвет фона капчи
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'layout1';

        $items = [User::ROLE_DOCTOR];

        $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

        foreach ($roles as $value)
        {
            if (in_array($value->name, $items))
                $this->redirect('/pacient/index');
        }

        return $this->render('index');
    }

    public function actionLogin()
    {
        return $this->redirect(['/user/security/login']);

        $this->layout = "main";

        if (!\Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            return $this->goBack();
        }
        else
        {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        return $this->redirect(['/user/security/logout']);

        Yii::$app->user->logout();

        return $this->goHome();
    }

}