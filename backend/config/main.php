<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'backend\controllers',
    'modules' => [
        'user' => [
            'controllerMap' => [
                'admin' => 'backend\modules\user\controllers\AdminController',
                'security' => 'backend\modules\user\controllers\SecurityController',
            ],
            'modelMap' => [
                'User' => 'backend\modules\user\models\Users',
                'UserSearch' => 'backend\modules\user\models\UsersSearch',
                'LoginForm' => 'backend\modules\user\models\LoginForm',
                'Profile' => 'backend\modules\user\models\Profile',
            ],
            // following line will restrict access to admin page
            'as backend' => 'dektrium\user\filters\BackendFilter',
        ],
        'handbook' => [
            'class' => 'backend\modules\handbook\Module',
        ],
        'handbookemc' => [
            'class' => 'backend\modules\handbookemc\Module',
        ],
        'signalmark' => [
            'class' => 'backend\modules\signalmark\Module',
        ],
        'planning' => [
            'class' => 'backend\modules\planning\Module',
        ],
        'emc' => [
            'class' => 'backend\modules\emc\Module',
        ],
        'diagnosis' => [
            'class' => 'backend\modules\diagnosis\Module',
        ],
        'pattern' => [
            'class' => 'backend\modules\pattern\Module',
        ],
        'reports' => [
            'class' => 'backend\modules\reports\Module',
        ],
    ],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@backend/views' => '@backend/themes/atlant/views',
                    '@backend/modules' => '@backend/themes/atlant/views/modules',
                    '@dektrium/user/views' => '@backend/themes/atlant/views/modules/user/views',
                    '@dektrium/rbac/views' => '@backend/themes/atlant/views/modules/rbac/views',
                ],
                'baseUrl' => '@backend/themes/atlant/views',
                'basePath' => '@backend/themes/atlant/views',
            ],
        ],
        /*
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        */
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];