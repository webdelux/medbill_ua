<?php

use yii\db\Schema;
use yii\db\Migration;

class m151215_140747_add_permission_signalmark extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151215_140747_add_permission_signalmark cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'signalmark_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'signalmark_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'signalmark_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'signalmark_operation_delete'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'signalmark_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'signalmark_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'signalmark_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'signalmark_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_operation_delete',
            'type' => '2'
        ]);
    }

}