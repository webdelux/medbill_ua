<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151028_124852_create_speciality_table extends Migration
{
//    public function up()
//    {
//
//    }
//
//    public function down()
//    {
//        echo "m151028_124852_create_speciality_table cannot be reverted.\n";
//
//        return false;
//    }

     public function safeUp()
    {

        if ($this->db->driverName === 'mysql') {
            $this->tableOptions .= ' AUTO_INCREMENT = 1000';
        }

        $this->createTable('{{%speciality}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer()->notNull(),

            'name' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%speciality}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%speciality}}', 'name');
        $this->createIndex('idx_description', '{{%speciality}}', 'description');

        $this->createIndex('idx_user_id', '{{%speciality}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%speciality}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%speciality}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%speciality}}', 'updated_at');

        $this->createIndex('idx_status_id', '{{%speciality}}', 'status_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%speciality}}');
    }
}
