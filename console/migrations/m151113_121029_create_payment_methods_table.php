<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151113_121029_create_payment_methods_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151113_121029_create_payment_methods_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%payment_methods}}', [
            'id' => $this->primaryKey(),

            'name' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_name', '{{%payment_methods}}', 'name');
        $this->createIndex('idx_description', '{{%payment_methods}}', 'description');

        $this->createIndex('idx_user_id', '{{%payment_methods}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%payment_methods}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%payment_methods}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%payment_methods}}', 'updated_at');
        $this->createIndex('idx_status_id', '{{%payment_methods}}', 'status_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_methods}}');
    }
}
