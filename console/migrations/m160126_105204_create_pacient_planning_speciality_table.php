<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160126_105204_create_pacient_planning_speciality_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160126_105204_create_pacient_planning_speciality_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_planning_speciality}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'speciality_id' => $this->integer()->notNull(),
            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),
            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_planning_speciality}}', 'pacient_id');

        $this->createIndex('idx_speciality_id', '{{%pacient_planning_speciality}}', 'speciality_id');

        $this->createIndex('idx_description', '{{%pacient_planning_speciality}}', 'description');
        $this->createIndex('idx_date_at', '{{%pacient_planning_speciality}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_planning_speciality}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_planning_speciality}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_planning_speciality}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_planning_speciality}}', 'updated_at');

        $this->addForeignKey('fk_pacient_planning_speciality_ib_1', '{{%pacient_planning_speciality}}', 'pacient_id', '{{%pacient}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_planning_speciality_ib_2', '{{%pacient_planning_speciality}}', 'speciality_id', '{{%speciality}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_planning_speciality_ib_1', '{{%pacient_planning_speciality}}');
        $this->dropForeignKey('fk_pacient_planning_speciality_ib_2', '{{%pacient_planning_speciality}}');

        $this->dropTable('{{%pacient_planning_speciality}}');
    }

}