<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160112_084555_create_handbook_emc_organ_complaints_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160112_084555_create_handbook_emc_organ_complaints_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%handbook_emc_organ_complaints}}', [
            'id' => $this->primaryKey(),

            'handbook_emc_organ_id' => $this->integer()->notNull(),
            'name' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_handbook_emc_organ_id', '{{%handbook_emc_organ_complaints}}', 'handbook_emc_organ_id');
        $this->createIndex('idx_name', '{{%handbook_emc_organ_complaints}}', 'name');

        $this->createIndex('idx_user_id', '{{%handbook_emc_organ_complaints}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_emc_organ_complaints}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_emc_organ_complaints}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_emc_organ_complaints}}', 'updated_at');

        $this->addForeignKey('fk_handbook_emc_organ_complaints_ib_1', '{{%handbook_emc_organ_complaints}}', 'handbook_emc_organ_id', '{{%handbook_emc_organ}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_handbook_emc_organ_complaints_ib_1', '{{%handbook_emc_organ_complaints}}');

        $this->dropTable('{{%handbook_emc_organ_complaints}}');
    }

}