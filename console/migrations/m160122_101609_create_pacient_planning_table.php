<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160122_101609_create_pacient_planning_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160122_101609_create_pacient_planning_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_planning}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),

            'treatment' => $this->text(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_planning}}', 'pacient_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%pacient_planning}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_planning}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_planning}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_planning}}', 'updated_at');

        $this->addForeignKey('fk_pacient_planning_ib_1', '{{%pacient_planning}}', 'pacient_id', '{{%pacient}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_planning_ib_1', '{{%pacient_planning}}');

        $this->dropTable('{{%pacient_planning}}');
    }

}