<?php

use yii\db\Schema;
use yii\db\Migration;

class m151111_074008_add_permission_service extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151111_074008_add_permission_service cannot be reverted.\n";

        return false;
    }
    */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_service_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_service_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_service_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_service_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_service_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_create',
            'child' => 'handbook_service_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_update',
            'child' => 'handbook_service_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_view',
            'child' => 'handbook_service_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_delete',
            'child' => 'handbook_service_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_view',
            'child' => 'handbook_service_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_create',
            'child' => 'handbook_service_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_update',
            'child' => 'handbook_service_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_view',
            'child' => 'handbook_service_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_delete',
            'child' => 'handbook_service_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_service_operation_view',
            'child' => 'handbook_service_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_service_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_service_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_service_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_service_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_service_operation_delete',
            'type' => '2'
        ]);
    }
}
