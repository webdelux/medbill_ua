<?php

/* Таблиця "Пацієнти" містить поля:
 * -	ПІБ пацієнта, дата народження, ІПН;
 * -	Телефон, адреса, місто, стать;
 * -	Робоче місце, посада;
 * - 	Контингент (інвалід, учасник ВВ, пенсіонер, тощо);
 * -	Диспансерна група;
 * -	Нотатки.
 */

//use yii\db\Migration;
use console\migrations\Migration;

class m151022_122425_create_pacient extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%pacient}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(30)->notNull(),
            'surname' => $this->string(30)->notNull(),
            'lastname' => $this->string(30)->notNull(),
            'birthday' => $this->date(),
            'ipn' => $this->string(20),
            'phone' => $this->string(20),
            'phone1' => $this->string(20),
            'city_id' => $this->integer(11)->notNull(),
            'address' => $this->string(50),
            'gender' => $this->smallInteger()->defaultValue(NULL),
            'work' => $this->string(50),
            'seat' => $this->string(50),
            'dispensary_group' => $this->boolean(),
            'contingent' => $this->string(),
            'docum_type' => $this->string(100),
            'docum_number' => $this->string(20),
            'memo' => $this->text(),
            'user_id' => $this->integer(11)->notNull(),
            'updated_user_id' => $this->integer(11),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_name', '{{%pacient}}', 'name');
        $this->createIndex('idx_pacient_surname', '{{%pacient}}', 'surname');
        $this->createIndex('idx_pacient_lastname', '{{%pacient}}', 'lastname');
        $this->createIndex('idx_pacient_birthday', '{{%pacient}}', 'birthday');
        $this->createIndex('idx_pacient_ipn', '{{%pacient}}', 'ipn');
        $this->createIndex('idx_pacient_phone', '{{%pacient}}', 'phone');
        $this->createIndex('idx_pacient_phone1', '{{%pacient}}', 'phone1');
        $this->createIndex('idx_pacient_city_id', '{{%pacient}}', 'city_id');
        $this->createIndex('idx_pacient_address', '{{%pacient}}', 'address');
        $this->createIndex('idx_pacient_work', '{{%pacient}}', 'work');
        $this->createIndex('idx_pacient_seat', '{{%pacient}}', 'seat');
        $this->createIndex('idx_pacient_contingent', '{{%pacient}}', 'contingent');
        $this->createIndex('idx_pacient_docum_number', '{{%pacient}}', 'docum_number');
        $this->createIndex('idx_pacient_user_id', '{{%pacient}}', 'user_id');
        $this->createIndex('idx_pacient_updated_user_id', '{{%pacient}}', 'updated_user_id');
        $this->createIndex('idx_pacient_created_at', '{{%pacient}}', 'created_at');
        $this->createIndex('idx_pacient_updated_at', '{{%pacient}}', 'updated_at');
        $this->createIndex('idx_pacient_status_id', '{{%pacient}}', 'status_id');

        $this->addForeignKey(
                'fk_pacient_user', '{{%pacient}}', 'user_id', '{{%user}}', 'id', 'RESTRICT'
        );

    }


    public function safeDown()
    {
        $this->dropTable('{{%pacient}}');
    }

}
