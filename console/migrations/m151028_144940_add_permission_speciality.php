<?php

use yii\db\Schema;
use yii\db\Migration;

class m151028_144940_add_permission_speciality extends Migration
{
//    public function up()
//    {
//
//    }
//
//    public function down()
//    {
//        echo "m151028_144940_add_permission_speciality cannot be reverted.\n";
//
//        return false;
//    }

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_speciality_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_speciality_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_speciality_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_speciality_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_speciality_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_create',
            'child' => 'handbook_speciality_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_update',
            'child' => 'handbook_speciality_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_view',
            'child' => 'handbook_speciality_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_delete',
            'child' => 'handbook_speciality_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_view',
            'child' => 'handbook_speciality_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_create',
            'child' => 'handbook_speciality_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_update',
            'child' => 'handbook_speciality_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_view',
            'child' => 'handbook_speciality_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_delete',
            'child' => 'handbook_speciality_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_speciality_operation_view',
            'child' => 'handbook_speciality_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_speciality_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_speciality_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_speciality_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_speciality_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_speciality_operation_delete',
            'type' => '2'
        ]);
    }
}
