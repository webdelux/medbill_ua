<?php

use yii\db\Schema;
use yii\db\Migration;

class m160105_100914_add_permission_protocol extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160105_100914_add_permission_protocol cannot be reverted.\n";

        return false;
    }
    *
    */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'protocol_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'protocol_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'protocol_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'protocol_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_create',
            'child' => 'protocol_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_update',
            'child' => 'protocol_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_view',
            'child' => 'protocol_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_delete',
            'child' => 'protocol_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_view',
            'child' => 'protocol_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_create',
            'child' => 'protocol_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_update',
            'child' => 'protocol_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_view',
            'child' => 'protocol_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_delete',
            'child' => 'protocol_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_view',
            'child' => 'protocol_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'protocol_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'protocol_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'protocol_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'protocol_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_operation_delete',
            'type' => '2'
        ]);
    }
}
