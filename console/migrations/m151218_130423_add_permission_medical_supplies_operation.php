<?php

use yii\db\Schema;
use yii\db\Migration;

class m151218_130423_add_permission_medical_supplies_operation extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151218_130423_add_permission_medical_supplies_operation cannot be reverted.\n";

        return false;
    }
     *
     */

   public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'medical-supplies-operation_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'medical-supplies-operation_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'medical-supplies-operation_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'medical-supplies-operation_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_create',
            'child' => 'medical-supplies-operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_update',
            'child' => 'medical-supplies-operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_view',
            'child' => 'medical-supplies-operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_delete',
            'child' => 'medical-supplies-operation_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_view',
            'child' => 'medical-supplies-operation_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_create',
            'child' => 'medical-supplies-operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_update',
            'child' => 'medical-supplies-operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_view',
            'child' => 'medical-supplies-operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_delete',
            'child' => 'medical-supplies-operation_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'medical-supplies-operation_operation_view',
            'child' => 'medical-supplies-operation_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'medical-supplies-operation_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'medical-supplies-operation_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'medical-supplies-operation_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'medical-supplies-operation_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'medical-supplies-operation_operation_delete',
            'type' => '2'
        ]);
    }
}
