<?php

//use yii\db\Migration;
use console\migrations\Migration;

/**
 * Handles the creation for table `protocol_pattern_2_table`.
 */
class m160616_093415_create_protocol_pattern_2_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160616_093415_create_protocol_pattern_2_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%protocol_pattern_2}}', [
            'id' => $this->primaryKey(),

            'protocol_id' => $this->integer()->notNull(),

            'field_1' => $this->string()->comment(Yii::t('app', 'Hemoglobin')),
            'field_2' => $this->string()->comment(Yii::t('app', 'Eritrotsity')),
            'field_3' => $this->string()->comment(Yii::t('app', 'Color index')),
            'field_4' => $this->string()->comment(Yii::t('app', 'Erythrocyte sedimentation rate')),
            'field_5' => $this->string()->comment(Yii::t('app', 'Leukocytes')),
            'field_6' => $this->string()->comment(Yii::t('app', 'Leukocyte formula')),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_2}}', Yii::t('app', 'protocol_pattern_2'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_2}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_2}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_2}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_2}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_2}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_2_ib_1', '{{%protocol_pattern_2}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_pattern_2_ib_1', '{{%protocol_pattern_2}}');

        $this->dropCommentFromTable('{{%protocol_pattern_2}}');

        $this->dropTable('{{%protocol_pattern_2}}');
    }

}