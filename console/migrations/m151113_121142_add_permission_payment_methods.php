<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_121142_add_permission_payment_methods extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151113_121142_add_permission_payment_methods cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_payment-methods_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_payment-methods_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_payment-methods_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_payment-methods_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_create',
            'child' => 'handbook_payment-methods_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_update',
            'child' => 'handbook_payment-methods_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_view',
            'child' => 'handbook_payment-methods_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_delete',
            'child' => 'handbook_payment-methods_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_view',
            'child' => 'handbook_payment-methods_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_create',
            'child' => 'handbook_payment-methods_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_update',
            'child' => 'handbook_payment-methods_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_view',
            'child' => 'handbook_payment-methods_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_delete',
            'child' => 'handbook_payment-methods_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_payment-methods_operation_view',
            'child' => 'handbook_payment-methods_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_payment-methods_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_payment-methods_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_payment-methods_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_payment-methods_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_payment-methods_operation_delete',
            'type' => '2'
        ]);
    }
}
