<?php

use yii\db\Migration;

/**
 * Handles adding column_trauma to table `diagnosis`.
 */
class m160524_083144_add_column_trauma_to_diagnosis extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%diagnosis}}', 'trauma', $this->integer());
        $this->createIndex('idx_trauma', '{{%diagnosis}}', 'trauma');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_trauma', '{{%diagnosis}}');
        $this->dropColumn('{{%diagnosis}}', 'trauma');

    }
}
