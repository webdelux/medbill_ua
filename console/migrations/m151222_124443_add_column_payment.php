<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_124443_add_column_payment extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151222_124443_add_column_payment cannot be reverted.\n";

        return false;
    }
     *
     */

     public function safeUp()
    {
        $this->addColumn('{{%payment}}', 'date_payment', $this->timestamp()->notNull()->defaultValue(0));
        $this->createIndex('idx_date_payment', '{{%payment}}', 'date_payment');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_date_payment', '{{%payment}}');
        $this->dropColumn('{{%payment}}', 'date_payment');
    }
}
