<?php

use yii\db\Migration;

class m160815_072804_add_permission_reports extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'reports_operation_view',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'reports_report_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'reports_report_print',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'reports_operation_view',
            'child' => 'reports_report_index'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'reports_operation_view',
            'child' => 'reports_report_print'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'reports_operation_view'
        ]);

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'reports_operation_view'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'reports_operation_view',
            'child' => 'reports_report_print'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'reports_operation_view',
            'child' => 'reports_report_index'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'reports_report_print',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'reports_report_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'reports_operation_view',
            'type' => '2'
        ]);
    }
}
