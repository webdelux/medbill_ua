<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_124953_add_permission_handbook_emc_list extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151217_124953_add_permission_handbook_emc_list cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_emc-list_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_emc-list_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_emc-list_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_emc-list_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_create',
            'child' => 'handbookemc_emc-list_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_update',
            'child' => 'handbookemc_emc-list_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_view',
            'child' => 'handbookemc_emc-list_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_delete',
            'child' => 'handbookemc_emc-list_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_view',
            'child' => 'handbookemc_emc-list_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_create',
            'child' => 'handbookemc_emc-list_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_update',
            'child' => 'handbookemc_emc-list_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_view',
            'child' => 'handbookemc_emc-list_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_delete',
            'child' => 'handbookemc_emc-list_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_emc-list_operation_view',
            'child' => 'handbookemc_emc-list_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_emc-list_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_emc-list_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_emc-list_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_emc-list_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_emc-list_operation_delete',
            'type' => '2'
        ]);
    }

}