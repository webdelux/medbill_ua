<?php

use yii\db\Migration;

class m160510_143139_add_additional_permission_pathological_examination extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160510_143139_add_additional_permission_pathological_examination cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_report',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_view',
            'child' => 'emc_pathological-examination_report'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_view',
            'child' => 'emc_pathological-examination_report'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_report',
            'type' => '2'
        ]);
    }
}
