<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151116_103439_create_refferal_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151116_103439_create_refferral_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%refferal}}', [
            'id' => $this->primaryKey(),

            'category_id' => $this->integer()->notNull(),

            'speciality_id' => $this->integer(),

            'pib' => $this->string(),
            'gender' => $this->smallInteger(),
            'birthday' => $this->date(),
            'phone' => $this->string(20),
            'phone1' => $this->string(20),
            'email' => $this->string(50),
            'requisites' => $this->string(),

            'work_id' => $this->integer(11),

            'city_id' => $this->integer(11),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_category_id', '{{%refferal}}', 'category_id');
        $this->createIndex('idx_speciality_id', '{{%refferal}}', 'speciality_id');
        $this->createIndex('idx_pib', '{{%refferal}}', 'pib');
        $this->createIndex('idx_gender', '{{%refferal}}', 'gender');
        $this->createIndex('idx_birthday', '{{%refferal}}', 'birthday');
        $this->createIndex('idx_phone', '{{%refferal}}', 'phone');
        $this->createIndex('idx_phone1', '{{%refferal}}', 'phone1');
        $this->createIndex('idx_email', '{{%refferal}}', 'email');
        $this->createIndex('idx_requisites', '{{%refferal}}', 'requisites');
        $this->createIndex('idx_work_id', '{{%refferal}}', 'work_id');
        $this->createIndex('idx_city_id', '{{%refferal}}', 'city_id');
        $this->createIndex('idx_description', '{{%refferal}}', 'description');

        $this->createIndex('idx_user_id', '{{%refferal}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%refferal}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%refferal}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%refferal}}', 'updated_at');
        $this->createIndex('idx_status_id', '{{%refferal}}', 'status_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%refferal}}');
    }
}
