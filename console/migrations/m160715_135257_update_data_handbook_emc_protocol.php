<?php

use yii\db\Migration;

class m160715_135257_update_data_handbook_emc_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160715_135257_update_data_handbook_emc_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 7], 'id = :id', [':id' => 2830]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2830]);
    }

}