<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160114_121936_create_pacient_signalmark_anamnesis_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160114_121936_create_pacient_signalmark_anamnesis_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_signalmark_anamnesis}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer()->notNull(),

            'handbook_emc_icd_id' => $this->integer()->notNull(),

            'description' => $this->string(),
            'type' => $this->integer()->notNull(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_signalmark_anamnesis}}', 'pacient_id');

        $this->createIndex('idx_handbook_emc_icd_id', '{{%pacient_signalmark_anamnesis}}', 'handbook_emc_icd_id');

        $this->createIndex('idx_description', '{{%pacient_signalmark_anamnesis}}', 'description');
        $this->createIndex('idx_type', '{{%pacient_signalmark_anamnesis}}', 'type');

        $this->createIndex('idx_user_id', '{{%pacient_signalmark_anamnesis}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_signalmark_anamnesis}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_signalmark_anamnesis}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_signalmark_anamnesis}}', 'updated_at');

        $this->addForeignKey('fk_pacient_signalmark_anamnesis_ib_1', '{{%pacient_signalmark_anamnesis}}', 'pacient_id',
                '{{%pacient}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_anamnesis_ib_2', '{{%pacient_signalmark_anamnesis}}', 'handbook_emc_icd_id',
                '{{%handbook_emc_icd}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_anamnesis_ib_1', '{{%pacient_signalmark_anamnesis}}');
        $this->dropForeignKey('fk_pacient_signalmark_anamnesis_ib_2', '{{%pacient_signalmark_anamnesis}}');

        $this->dropTable('{{%pacient_signalmark_anamnesis}}');
    }

}