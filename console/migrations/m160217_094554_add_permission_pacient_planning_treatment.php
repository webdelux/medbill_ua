<?php

use yii\db\Migration;

class m160217_094554_add_permission_pacient_planning_treatment extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160217_094554_add_permission_pacient_planning_treatment cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_operation_create',
            'child' => 'planning_pacient-treatment_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_operation_update',
            'child' => 'planning_pacient-treatment_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_operation_view',
            'child' => 'planning_pacient-treatment_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_operation_delete',
            'child' => 'planning_pacient-treatment_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_create',
            'child' => 'planning_pacient-treatment_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_update',
            'child' => 'planning_pacient-treatment_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_view',
            'child' => 'planning_pacient-treatment_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_delete',
            'child' => 'planning_pacient-treatment_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_view',
            'child' => 'planning_pacient-treatment_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_create',
            'child' => 'planning_pacient-treatment_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_update',
            'child' => 'planning_pacient-treatment_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_view',
            'child' => 'planning_pacient-treatment_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_delete',
            'child' => 'planning_pacient-treatment_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_pacient-treatment_operation_view',
            'child' => 'planning_pacient-treatment_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_operation_create',
            'child' => 'planning_pacient-treatment_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_operation_update',
            'child' => 'planning_pacient-treatment_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_operation_view',
            'child' => 'planning_pacient-treatment_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'planning_operation_delete',
            'child' => 'planning_pacient-treatment_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'planning_pacient-treatment_operation_delete',
            'type' => '2'
        ]);
    }

}