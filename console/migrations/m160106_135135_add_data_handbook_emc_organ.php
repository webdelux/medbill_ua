<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_135135_add_data_handbook_emc_organ extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160106_135135_add_data_handbook_emc_organ cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $data = [
            [1, NULL, 'Око'],

            [2, 1, 'OD'],
            [3, 1, 'OS'],
            [4, 1, 'OU'],

            [5, NULL, 'Вухо'],

            [6, 5, 'Зовнішнє вухо'],
            [7, 5, 'Середнє вухо'],
            [8, 5, 'Внутрішнє вухо'],

            [9, NULL, 'Загально'],
        ];

        foreach ($data as $value)
        {
            $this->insert('{{%handbook_emc_organ}}', [
                'id' => $value[0],
                'parent_id' => $value[1],
                'name' => $value[2],
                'sort' => 0,
                'user_id' => 0
            ]);
        }
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->truncateTable('{{%handbook_emc_organ}}');
    }

}