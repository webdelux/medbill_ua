<?php

use yii\db\Migration;

class m161006_090512_add_data_handbook_emc_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m161006_090512_add_data_handbook_emc_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $data = [
            [201, 83, "CF2-101", Yii::t('app', 'protocol_pattern_30'), 30],
            [202, 83, "CF2-102", Yii::t('app', 'protocol_pattern_31'), 31],
        ];

        foreach ($data as $index => $value) {
            $this->insert('{{%handbook_emc_protocol}}', [
                'id' => $value[0],
                'handbook_emc_category_id' => $value[1],
                'code' => $value[2],
                'name' => $value[3],
                'handbook_emc_template_id' => $value[4],
                'user_id' => 0
            ]);
        }
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%handbook_emc_protocol}}', [
            'id' => 201,
        ]);

        $this->delete('{{%handbook_emc_protocol}}', [
            'id' => 202,
        ]);
    }

}
