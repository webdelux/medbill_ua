<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_104233_add_permission_medical_supplies extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151029_104233_add_permission_medical_supplies cannot be reverted.\n";

        return false;
    }
    */
     public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_medical-supplies_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_medical-supplies_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_medical-supplies_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_medical-supplies_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_create',
            'child' => 'handbook_medical-supplies_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_update',
            'child' => 'handbook_medical-supplies_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_view',
            'child' => 'handbook_medical-supplies_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_delete',
            'child' => 'handbook_medical-supplies_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_view',
            'child' => 'handbook_medical-supplies_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_create',
            'child' => 'handbook_medical-supplies_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_update',
            'child' => 'handbook_medical-supplies_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_view',
            'child' => 'handbook_medical-supplies_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_delete',
            'child' => 'handbook_medical-supplies_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_medical-supplies_operation_view',
            'child' => 'handbook_medical-supplies_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_medical-supplies_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_medical-supplies_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_medical-supplies_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_medical-supplies_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_medical-supplies_operation_delete',
            'type' => '2'
        ]);
    }
}