<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151127_091101_create_handbook_emc_template_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151127_091101_create_handbook_emc_template_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%handbook_emc_template}}', [
            'id' => $this->primaryKey(),

            'handbook_emc_category_id' => $this->integer()->notNull(),

            'name' => $this->string(),
            'description' => $this->text(),
            'substitutional' => $this->text(),

            'status_id' => $this->integer(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_handbook_emc_category_id', '{{%handbook_emc_template}}', 'handbook_emc_category_id');

        $this->createIndex('idx_name', '{{%handbook_emc_template}}', 'name');

        $this->createIndex('idx_status_id', '{{%handbook_emc_template}}', 'status_id');

        $this->createIndex('idx_user_id', '{{%handbook_emc_template}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_emc_template}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_emc_template}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_emc_template}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%handbook_emc_template}}');
    }

}