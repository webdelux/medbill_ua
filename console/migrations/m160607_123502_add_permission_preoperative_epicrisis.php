<?php

use yii\db\Migration;

class m160607_123502_add_permission_preoperative_epicrisis extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_preoperative-epicrisis_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_preoperative-epicrisis_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_preoperative-epicrisis_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_preoperative-epicrisis_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_create',
            'child' => 'emc_preoperative-epicrisis_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_update',
            'child' => 'emc_preoperative-epicrisis_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_view',
            'child' => 'emc_preoperative-epicrisis_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_delete',
            'child' => 'emc_preoperative-epicrisis_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_view',
            'child' => 'emc_preoperative-epicrisis_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_create',
            'child' => 'emc_preoperative-epicrisis_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_update',
            'child' => 'emc_preoperative-epicrisis_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_view',
            'child' => 'emc_preoperative-epicrisis_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_delete',
            'child' => 'emc_preoperative-epicrisis_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_view',
            'child' => 'emc_preoperative-epicrisis_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_preoperative-epicrisis_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_preoperative-epicrisis_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_preoperative-epicrisis_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_preoperative-epicrisis_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_operation_delete',
            'type' => '2'
        ]);
    }
}
