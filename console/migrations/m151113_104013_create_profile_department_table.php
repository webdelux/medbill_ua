<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151113_104013_create_profile_department_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151113_104013_create_profile_department_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_department}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),

            'department_id' => $this->integer()->notNull(),

            'created_user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_user_id', '{{%profile_department}}', 'user_id');
        $this->addForeignKey('fk_profile_department_user', '{{%profile_department}}', 'user_id', '{{%user}}', 'id', 'RESTRICT');

        $this->createIndex('idx_department_id', '{{%profile_department}}', 'department_id');
        $this->addForeignKey('fk_profile_department', '{{%profile_department}}', 'department_id', '{{%department}}', 'id', 'RESTRICT');

        $this->createIndex('idx_user_department_unique', '{{%profile_department}}', ['user_id', 'department_id'], $unique = true);

        $this->createIndex('idx_created_user_id', '{{%profile_department}}', 'created_user_id');
        $this->createIndex('idx_updated_user_id', '{{%profile_department}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%profile_department}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%profile_department}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_department}}');
    }

}