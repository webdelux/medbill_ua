<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160125_103027_create_pacient_planning_diagnosis_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160125_103027_create_pacient_planning_diagnosis_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_planning_diagnosis}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),

            'handbook_emc_icd_id' => $this->integer()->notNull(),

            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_planning_diagnosis}}', 'pacient_id');

        $this->createIndex('idx_handbook_emc_icd_id', '{{%pacient_planning_diagnosis}}', 'handbook_emc_icd_id');

        $this->createIndex('idx_description', '{{%pacient_planning_diagnosis}}', 'description');
        $this->createIndex('idx_date_at', '{{%pacient_planning_diagnosis}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_planning_diagnosis}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_planning_diagnosis}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_planning_diagnosis}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_planning_diagnosis}}', 'updated_at');

        $this->addForeignKey('fk_pacient_planning_diagnosis_ib_1', '{{%pacient_planning_diagnosis}}', 'pacient_id', '{{%pacient}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_planning_diagnosis_ib_2', '{{%pacient_planning_diagnosis}}', 'handbook_emc_icd_id', '{{%handbook_emc_icd}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_planning_diagnosis_ib_1', '{{%pacient_planning_diagnosis}}');
        $this->dropForeignKey('fk_pacient_planning_diagnosis_ib_2', '{{%pacient_planning_diagnosis}}');

        $this->dropTable('{{%pacient_planning_diagnosis}}');
    }

}