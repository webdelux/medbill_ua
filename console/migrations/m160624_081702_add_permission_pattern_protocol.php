<?php

use yii\db\Migration;

class m160624_081702_add_permission_pattern_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160624_081702_add_permission_pattern_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pattern_protocol_operation_view',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'pattern_protocol_view',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pattern_operation_view',
            'child' => 'pattern_protocol_operation_view'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pattern_protocol_operation_view',
            'child' => 'pattern_protocol_view'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pattern_protocol_operation_view',
            'child' => 'pattern_protocol_view'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pattern_operation_view',
            'child' => 'pattern_protocol_operation_view'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pattern_protocol_view',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pattern_protocol_operation_view',
            'type' => '2'
        ]);
    }

}