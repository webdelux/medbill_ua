<?php

use yii\db\Migration;

/**
 * Handles adding permission_reports to table `pacient`.
 */
class m160519_100335_add_permission_reports_to_pacient extends Migration
{
     public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_reports',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_reports'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_reports'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_reports',
            'type' => '2'
        ]);
    }
}
