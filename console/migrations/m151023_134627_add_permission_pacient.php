<?php

use yii\db\Schema;
use yii\db\Migration;

class m151023_134627_add_permission_pacient extends Migration
{
//    public function up()
//    {
//
//    }
//
//    public function down()
//    {
//        echo "m151023_134627_add_permission_pacient cannot be reverted.\n";
//
//        return false;
//    }

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'pacient_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'pacient_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'pacient_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'pacient_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_create',
            'child' => 'pacient_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_update',
            'child' => 'pacient_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_delete',
            'child' => 'pacient_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_create',
            'child' => 'pacient_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_update',
            'child' => 'pacient_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_delete',
            'child' => 'pacient_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'pacient_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'pacient_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'pacient_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'pacient_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_operation_delete',
            'type' => '2'
        ]);
    }

}
