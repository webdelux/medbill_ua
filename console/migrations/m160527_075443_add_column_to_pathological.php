<?php

use yii\db\Migration;

/**
 * Handles adding column to table `pathological`.
 */
class m160527_075443_add_column_to_pathological extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('idx_icd', '{{%pathological_examination}}');
        $this->dropColumn('{{%pathological_examination}}', 'icd');

        $this->addColumn('{{%pathological_examination}}', 'pathological_diagnosis_id', $this->integer()->notNull()->after('diagnosis_id'));
        $this->createIndex('idx_pathological_diagnosis_id', '{{%pathological_examination}}', 'pathological_diagnosis_id');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_pathological_diagnosis_id', '{{%pathological_examination}}');
        $this->dropColumn('{{%pathological_examination}}', 'pathological_diagnosis_id');

        $this->addColumn('{{%pathological_examination}}', 'icd', $this->integer()->notNull()->after('doctor_id'));
        $this->createIndex('idx_icd', '{{%pathological_examination}}', 'icd');

    }
}
