<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_102218_add_column_pacient extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160209_102218_add_column_pacient cannot be reverted.\n";

        return false;
    }
     *
     */

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%pacient}}', 'card_amb', $this->string());
        $this->addColumn('{{%pacient}}', 'card_stac', $this->string());
        $this->addColumn('{{%pacient}}', 'city_type', $this->integer());

        $this->createIndex('idx_card_amb', '{{%pacient}}', 'card_amb');
        $this->createIndex('idx_card_stac', '{{%pacient}}', 'card_stac');
        $this->createIndex('idx_city_type', '{{%pacient}}', 'city_type');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_card_amb', '{{%pacient}}');
        $this->dropIndex('idx_card_stac', '{{%pacient}}');
        $this->dropIndex('idx_city_type', '{{%pacient}}');

        $this->dropColumn('{{%pacient}}', 'card_amb');
        $this->dropColumn('{{%pacient}}', 'card_stac');
        $this->dropColumn('{{%pacient}}', 'city_type');
    }
}
