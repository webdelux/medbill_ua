<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160105_100840_create_protocol_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160105_100840_create_ptotocol_table cannot be reverted.\n";

        return false;
    }
    *
    */

    public function safeUp()
    {
        $this->createTable('{{%protocol}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer(),
            'department_id' => $this->integer(),
            'doctor_id' => $this->integer(),
            'protocol_id' => $this->integer(),
            'refferal_id' => $this->integer(),
            'category_id' => $this->integer(),

            'protocol_date' => $this->timestamp()->notNull()->defaultValue(0),
            'total' => $this->decimal(10,2),
            'discount' => $this->decimal(10,2),

            'status' => $this->integer(),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%protocol}}', 'pacient_id');
        $this->createIndex('idx_department_id', '{{%protocol}}', 'department_id');
        $this->createIndex('idx_doctor_id', '{{%protocol}}', 'doctor_id');
        $this->createIndex('idx_protocol_id', '{{%protocol}}', 'protocol_id');
        $this->createIndex('idx_refferal_id', '{{%protocol}}', 'refferal_id');
        $this->createIndex('idx_category_id', '{{%protocol}}', 'category_id');

        $this->createIndex('idx_protocol_date', '{{%protocol}}', 'protocol_date');
        $this->createIndex('idx_total', '{{%protocol}}', 'total');
        $this->createIndex('idx_discount', '{{%protocol}}', 'discount');

        $this->createIndex('idx_status', '{{%protocol}}', 'status');

        $this->createIndex('idx_description', '{{%protocol}}', 'description');

        $this->createIndex('idx_user_id', '{{%protocol}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%protocol}}');
    }
}
