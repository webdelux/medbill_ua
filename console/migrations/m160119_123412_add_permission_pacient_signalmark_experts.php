<?php

use yii\db\Schema;
use yii\db\Migration;

class m160119_123412_add_permission_pacient_signalmark_experts extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160119_123412_add_permission_pacient_signalmark_experts cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_create',
            'child' => 'signalmark_pacient-experts_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_update',
            'child' => 'signalmark_pacient-experts_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_view',
            'child' => 'signalmark_pacient-experts_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_delete',
            'child' => 'signalmark_pacient-experts_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_create',
            'child' => 'signalmark_pacient-experts_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_update',
            'child' => 'signalmark_pacient-experts_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_view',
            'child' => 'signalmark_pacient-experts_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_delete',
            'child' => 'signalmark_pacient-experts_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_view',
            'child' => 'signalmark_pacient-experts_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_create',
            'child' => 'signalmark_pacient-experts_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_update',
            'child' => 'signalmark_pacient-experts_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_view',
            'child' => 'signalmark_pacient-experts_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_delete',
            'child' => 'signalmark_pacient-experts_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-experts_operation_view',
            'child' => 'signalmark_pacient-experts_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_create',
            'child' => 'signalmark_pacient-experts_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_update',
            'child' => 'signalmark_pacient-experts_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_view',
            'child' => 'signalmark_pacient-experts_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_delete',
            'child' => 'signalmark_pacient-experts_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-experts_operation_delete',
            'type' => '2'
        ]);
    }

}