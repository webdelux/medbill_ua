<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_131357_add_column_handbook_stacionary extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160208_131357_add_column_bed_type cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->addColumn('{{%handbook_stacionary}}', 'bed_type', $this->integer());
        $this->addColumn('{{%handbook_stacionary}}', 'type', $this->integer());

        $this->createIndex('idx_bed_type', '{{%handbook_stacionary}}', 'bed_type');
        $this->createIndex('idx_type', '{{%handbook_stacionary}}', 'type');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_bed_type', '{{%handbook_stacionary}}');
        $this->dropIndex('idx_type', '{{%handbook_stacionary}}');
        $this->dropColumn('{{%handbook_stacionary}}', 'bed_type');
        $this->dropColumn('{{%handbook_stacionary}}', 'type');
    }
}
