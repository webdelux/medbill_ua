<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_084315_add_permission_geo extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151029_084315_add_permission_geo cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        /* --- City --- */

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_city_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_city_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_city_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_city_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_city_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_create',
            'child' => 'handbook_city_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_update',
            'child' => 'handbook_city_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_view',
            'child' => 'handbook_city_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_delete',
            'child' => 'handbook_city_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_view',
            'child' => 'handbook_city_index'
        ]);

        /* --- End City --- */

        /* --- Region --- */

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_region_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_region_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_region_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_region_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_region_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_create',
            'child' => 'handbook_region_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_update',
            'child' => 'handbook_region_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_view',
            'child' => 'handbook_region_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_delete',
            'child' => 'handbook_region_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_view',
            'child' => 'handbook_region_index'
        ]);

        /* --- End Region --- */

        /* --- Country --- */

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_country_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_country_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_country_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_country_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_country_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_create',
            'child' => 'handbook_country_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_update',
            'child' => 'handbook_country_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_view',
            'child' => 'handbook_country_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_delete',
            'child' => 'handbook_country_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_view',
            'child' => 'handbook_country_index'
        ]);

        /* --- End Country --- */
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        /* --- City --- */

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_create',
            'child' => 'handbook_city_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_update',
            'child' => 'handbook_city_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_view',
            'child' => 'handbook_city_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_delete',
            'child' => 'handbook_city_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_city_operation_view',
            'child' => 'handbook_city_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_city_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_city_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_city_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_city_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_city_operation_delete',
            'type' => '2'
        ]);

        /* --- End City --- */

        /* --- Region --- */

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_create',
            'child' => 'handbook_region_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_update',
            'child' => 'handbook_region_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_view',
            'child' => 'handbook_region_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_delete',
            'child' => 'handbook_region_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_region_operation_view',
            'child' => 'handbook_region_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_region_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_region_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_region_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_region_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_region_operation_delete',
            'type' => '2'
        ]);

        /* --- End Region --- */

        /* --- Country --- */

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_create',
            'child' => 'handbook_country_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_update',
            'child' => 'handbook_country_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_view',
            'child' => 'handbook_country_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_delete',
            'child' => 'handbook_country_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_country_operation_view',
            'child' => 'handbook_country_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_country_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_country_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_country_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_country_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_country_operation_delete',
            'type' => '2'
        ]);

        /* --- End Country --- */
    }

}