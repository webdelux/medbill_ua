<?php

use yii\db\Schema;
use yii\db\Migration;

class m151120_142620_add_permission_pacient_service extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151120_142620_add_permission_pacient_service cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-service_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'pacient-service_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'pacient-service_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'pacient-service_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'pacient-service_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_create',
            'child' => 'pacient-service_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_update',
            'child' => 'pacient-service_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_view',
            'child' => 'pacient-service_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_delete',
            'child' => 'pacient-service_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_view',
            'child' => 'pacient-service_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_create',
            'child' => 'pacient-service_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_update',
            'child' => 'pacient-service_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_view',
            'child' => 'pacient-service_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_delete',
            'child' => 'pacient-service_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-service_operation_view',
            'child' => 'pacient-service_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'pacient-service_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'pacient-service_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'pacient-service_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'pacient-service_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-service_operation_delete',
            'type' => '2'
        ]);
    }
}
