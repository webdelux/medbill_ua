<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_125149_add_permission_pacient_signalmark_anamnesis extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160114_125149_add_permission_pacient_signalmark_anamnesis cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_create',
            'child' => 'signalmark_pacient-anamnesis_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_update',
            'child' => 'signalmark_pacient-anamnesis_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_view',
            'child' => 'signalmark_pacient-anamnesis_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_delete',
            'child' => 'signalmark_pacient-anamnesis_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_create',
            'child' => 'signalmark_pacient-anamnesis_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_update',
            'child' => 'signalmark_pacient-anamnesis_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_view',
            'child' => 'signalmark_pacient-anamnesis_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_delete',
            'child' => 'signalmark_pacient-anamnesis_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_view',
            'child' => 'signalmark_pacient-anamnesis_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_create',
            'child' => 'signalmark_pacient-anamnesis_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_update',
            'child' => 'signalmark_pacient-anamnesis_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_view',
            'child' => 'signalmark_pacient-anamnesis_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_delete',
            'child' => 'signalmark_pacient-anamnesis_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_pacient-anamnesis_operation_view',
            'child' => 'signalmark_pacient-anamnesis_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_create',
            'child' => 'signalmark_pacient-anamnesis_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_update',
            'child' => 'signalmark_pacient-anamnesis_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_view',
            'child' => 'signalmark_pacient-anamnesis_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_delete',
            'child' => 'signalmark_pacient-anamnesis_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_pacient-anamnesis_operation_delete',
            'type' => '2'
        ]);
    }

}