<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151120_142513_create_pacient_service_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151120_142513_create_pacient_service_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%pacient_service}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer(),
            'service_id' => $this->integer(),

            'total' => $this->decimal(10,2),
            'discount' => $this->decimal(10,2),

            'docum' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_service}}', 'pacient_id');
        $this->createIndex('idx_service_id', '{{%pacient_service}}', 'service_id');

        $this->createIndex('idx_total', '{{%pacient_service}}', 'total');
        $this->createIndex('idx_discount', '{{%pacient_service}}', 'discount');

        $this->createIndex('idx_docum', '{{%pacient_service}}', 'docum');
        $this->createIndex('idx_description', '{{%pacient_service}}', 'description');

        $this->createIndex('idx_user_id', '{{%pacient_service}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_service}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_service}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_service}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%pacient_service}}');
    }
}
