<?php

use yii\db\Migration;

class m160811_093259_update_data_handbook_emc_protocol extends Migration
{
    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 10], 'id = :id', [':id' => 2840]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2840]);
    }
}
