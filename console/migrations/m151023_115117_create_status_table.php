<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151023_115117_create_status_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151023_115117_create_status_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%status}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer()->notNull(),

            'name' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%status}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%status}}', 'name');
        $this->createIndex('idx_description', '{{%status}}', 'description');

        $this->createIndex('idx_user_id', '{{%status}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%status}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%status}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%status}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%status}}');
    }

}