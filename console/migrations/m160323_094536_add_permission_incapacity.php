<?php

use yii\db\Migration;

class m160323_094536_add_permission_incapacity extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160323_094536_add_permission_incapacity cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_incapacity_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_incapacity_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_incapacity_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_incapacity_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_incapacity_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_create',
            'child' => 'emc_incapacity_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_update',
            'child' => 'emc_incapacity_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_view',
            'child' => 'emc_incapacity_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_delete',
            'child' => 'emc_incapacity_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_view',
            'child' => 'emc_incapacity_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_create',
            'child' => 'emc_incapacity_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_update',
            'child' => 'emc_incapacity_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_view',
            'child' => 'emc_incapacity_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_delete',
            'child' => 'emc_incapacity_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_incapacity_operation_view',
            'child' => 'emc_incapacity_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_incapacity_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_incapacity_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_incapacity_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_incapacity_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_incapacity_operation_delete',
            'type' => '2'
        ]);
    }
}
