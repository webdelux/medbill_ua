<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160127_125024_create_diagnosis_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160127_125024_create_diagnosis_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%diagnosis}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),

            'pacient_id' => $this->integer()->notNull(),

            'handbook_emc_icd_id' => $this->integer(),

            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),

            'alternative' => $this->text(),
            'alternative_print' => $this->smallInteger()->notNull()->defaultValue(0),

            'type' => $this->smallInteger(),
            'first_set' => $this->smallInteger(),
            'prophylactic_set' => $this->smallInteger(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_id', '{{%diagnosis}}', 'parent_id');

        $this->createIndex('idx_pacient_id', '{{%diagnosis}}', 'pacient_id');

        $this->createIndex('idx_handbook_emc_icd_id', '{{%diagnosis}}', 'handbook_emc_icd_id');

        $this->createIndex('idx_description', '{{%diagnosis}}', 'description');
        $this->createIndex('idx_date_at', '{{%diagnosis}}', 'date_at');

        $this->createIndex('idx_alternative_print', '{{%diagnosis}}', 'alternative_print');

        $this->createIndex('idx_type', '{{%diagnosis}}', 'type');
        $this->createIndex('idx_first_set', '{{%diagnosis}}', 'first_set');
        $this->createIndex('idx_prophylactic_set', '{{%diagnosis}}', 'prophylactic_set');

        $this->createIndex('idx_user_id', '{{%diagnosis}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%diagnosis}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%diagnosis}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%diagnosis}}', 'updated_at');

        $this->addForeignKey('fk_diagnosis_ib_1', '{{%diagnosis}}', 'pacient_id', '{{%pacient}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_diagnosis_ib_2', '{{%diagnosis}}', 'handbook_emc_icd_id', '{{%handbook_emc_icd}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_diagnosis_ib_1', '{{%diagnosis}}');
        $this->dropForeignKey('fk_diagnosis_ib_2', '{{%diagnosis}}');

        $this->dropTable('{{%diagnosis}}');
    }

}