<?php

use yii\db\Schema;
use yii\db\Migration;

class m151117_114749_add_permission_payment extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151117_114749_add_permission_payment cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'payment_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'payment_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'payment_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'payment_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'payment_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'payment_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'payment_operation_create',
            'child' => 'payment_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'payment_operation_update',
            'child' => 'payment_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'payment_operation_view',
            'child' => 'payment_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'payment_operation_delete',
            'child' => 'payment_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'payment_operation_view',
            'child' => 'payment_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'payment_operation_create',
            'child' => 'payment_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'payment_operation_update',
            'child' => 'payment_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'payment_operation_view',
            'child' => 'payment_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'payment_operation_delete',
            'child' => 'payment_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'payment_operation_view',
            'child' => 'payment_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'payment_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'payment_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'payment_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'payment_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'payment_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'payment_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'payment_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'payment_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'payment_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'payment_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'payment_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'payment_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'payment_operation_delete',
            'type' => '2'
        ]);
    }
}
