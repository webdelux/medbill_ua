<?php

use yii\db\Migration;

class m160721_123749_add_column_profile_schedule extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160721_123749_add_column_profile_schedule cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_schedule}}', 'profile_department_id', $this->integer()->after('user_id'));
        $this->createIndex('idx_profile_department_id', '{{%profile_schedule}}', 'profile_department_id', $unique = false);
        $this->addForeignKey('fk_profile_schedule_ib_1', '{{%profile_schedule}}', 'profile_department_id', '{{%profile_department}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_profile_schedule_ib_1', '{{%profile_schedule}}');
        $this->dropIndex('idx_profile_department_id', '{{%profile_schedule}}');
        $this->dropColumn('{{%profile_schedule}}', 'profile_department_id');
    }

}