<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160211_081232_create_dispensary_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160211_081232_create_dispensary_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%dispensary}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),

            'group' => $this->string(),
            'date_in' => $this->date(),
            'date_out' => $this->date(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%dispensary}}', 'pacient_id');
        $this->createIndex('idx_group', '{{%dispensary}}', 'group');
        $this->createIndex('idx_date_in', '{{%dispensary}}', 'date_in');
        $this->createIndex('idx_date_out', '{{%dispensary}}', 'date_out');
        $this->createIndex('idx_description', '{{%dispensary}}', 'description');

        $this->createIndex('idx_user_id', '{{%dispensary}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%dispensary}}', 'updated_user_id');
        $this->createIndex('idx_created_at', '{{%dispensary}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%dispensary}}', 'updated_at');


    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%dispensary}}');
    }
}
