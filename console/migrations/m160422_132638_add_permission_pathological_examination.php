<?php

use yii\db\Migration;

class m160422_132638_add_permission_pathological_examination extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160422_132638_add_permission_pathological_examination cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_pathological-examination_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_pathological-examination_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_pathological-examination_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_pathological-examination_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_create',
            'child' => 'emc_pathological-examination_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_update',
            'child' => 'emc_pathological-examination_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_view',
            'child' => 'emc_pathological-examination_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_delete',
            'child' => 'emc_pathological-examination_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_view',
            'child' => 'emc_pathological-examination_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_create',
            'child' => 'emc_pathological-examination_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_update',
            'child' => 'emc_pathological-examination_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_view',
            'child' => 'emc_pathological-examination_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_delete',
            'child' => 'emc_pathological-examination_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_pathological-examination_operation_view',
            'child' => 'emc_pathological-examination_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_pathological-examination_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_pathological-examination_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_pathological-examination_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_pathological-examination_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_pathological-examination_operation_delete',
            'type' => '2'
        ]);
    }
}
