<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160426_111737_create_attachment_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%attachment}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'diagnosis_id' => $this->integer()->defaultValue(0),
            'protocol_id' => $this->integer()->defaultValue(0),

            'filename' => $this->string(),
            'path' => $this->string(),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%attachment}}', 'pacient_id');
        $this->createIndex('idx_diagnosis_id', '{{%attachment}}', 'diagnosis_id');
        $this->createIndex('idx_protocol_id', '{{%attachment}}', 'protocol_id');

        $this->createIndex('idx_filename', '{{%attachment}}', 'filename');
        $this->createIndex('idx_path', '{{%attachment}}', 'path');

        $this->createIndex('idx_description', '{{%attachment}}', 'description');
        $this->createIndex('idx_user_id', '{{%attachment}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%attachment}}', 'updated_user_id');
        $this->createIndex('idx_created_at', '{{%attachment}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%attachment}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%attachment}}');
    }
}
