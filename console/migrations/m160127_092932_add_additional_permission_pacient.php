<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_092932_add_additional_permission_pacient extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160127_092932_add_additional_permission_pacient cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient_history',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_history'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient_operation_view',
            'child' => 'pacient_history'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient_history',
            'type' => '2'
        ]);
    }

}