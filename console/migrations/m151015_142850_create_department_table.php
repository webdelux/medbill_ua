<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151015_142850_create_department_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151015_142850_create_department_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        /*
        $this->createTable('{{%department}}', [
            'id' => Schema::TYPE_PK,
            // 'tree' => Schema::TYPE_INTEGER,
            'lft' => Schema::TYPE_INTEGER . ' NOT NULL',
            'rgt' => Schema::TYPE_INTEGER . ' NOT NULL',
            'depth' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        */

        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            //'tree' => $this->integer(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),

            'address' => $this->string(),
            'phone' => $this->string(32),
            'email' => $this->string(64),

            'edrpou' => $this->string(32),
            'requisites' => $this->string(),

            'working_days' => $this->string()->notNull()->defaultValue("mon,tue,wed,thu,fri,sat,sun"),

            // Monday
            'working_time_mon_from' => $this->time()->notNull()->defaultValue('08:00:00'),
            'working_time_mon_to' => $this->time()->notNull()->defaultValue('22:00:00'),
            // Tuesday
            'working_time_tue_from' => $this->time()->notNull()->defaultValue('08:00:00'),
            'working_time_tue_to' => $this->time()->notNull()->defaultValue('22:00:00'),
            // Wednesday
            'working_time_wed_from' => $this->time()->notNull()->defaultValue('08:00:00'),
            'working_time_wed_to' => $this->time()->notNull()->defaultValue('22:00:00'),
            // Thursday
            'working_time_thu_from' => $this->time()->notNull()->defaultValue('08:00:00'),
            'working_time_thu_to' => $this->time()->notNull()->defaultValue('22:00:00'),
            // Friday
            'working_time_fri_from' => $this->time()->notNull()->defaultValue('08:00:00'),
            'working_time_fri_to' => $this->time()->notNull()->defaultValue('22:00:00'),
            // Saturday
            'working_time_sat_from' => $this->time()->notNull()->defaultValue('08:00:00'),
            'working_time_sat_to' => $this->time()->notNull()->defaultValue('22:00:00'),
            // Sunday
            'working_time_sun_from' => $this->time()->notNull()->defaultValue('08:00:00'),
            'working_time_sun_to' => $this->time()->notNull()->defaultValue('22:00:00'),

            'status_id' => $this->integer(),
            'type_id' => $this->integer(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_lft', '{{%department}}', 'lft');
        $this->createIndex('idx_rgt', '{{%department}}', 'rgt');
        $this->createIndex('idx_depth', '{{%department}}', 'depth');
        $this->createIndex('idx_name', '{{%department}}', 'name');

        $this->createIndex('idx_address', '{{%department}}', 'address');
        $this->createIndex('idx_phone', '{{%department}}', 'phone');
        $this->createIndex('idx_email', '{{%department}}', 'email');

        $this->createIndex('idx_edrpou', '{{%department}}', 'edrpou');
        $this->createIndex('idx_requisites', '{{%department}}', 'requisites');

        $this->createIndex('idx_working_days', '{{%department}}', 'working_days');

        $this->createIndex('idx_status_id', '{{%department}}', 'status_id');
        $this->createIndex('idx_type_id', '{{%department}}', 'type_id');

        $this->createIndex('idx_user_id', '{{%department}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%department}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%department}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%department}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%department}}');
    }

}