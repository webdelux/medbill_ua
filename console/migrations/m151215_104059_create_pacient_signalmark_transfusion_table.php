<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151215_104059_create_pacient_signalmark_transfusion_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151215_104059_create_pacient_signalmark_transfusion_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_signalmark_transfusion}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer()->notNull(),

            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_signalmark_transfusion}}', 'pacient_id');

        $this->createIndex('idx_description', '{{%pacient_signalmark_transfusion}}', 'description');
        $this->createIndex('idx_date_at', '{{%pacient_signalmark_transfusion}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_signalmark_transfusion}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_signalmark_transfusion}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_signalmark_transfusion}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_signalmark_transfusion}}', 'updated_at');

        $this->addForeignKey('fk_pacient_signalmark_transfusion_ib_1', '{{%pacient_signalmark_transfusion}}', 'pacient_id',
                '{{%pacient}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_transfusion_ib_1', '{{%pacient_signalmark_transfusion}}');

        $this->dropTable('{{%pacient_signalmark_transfusion}}');
    }

}