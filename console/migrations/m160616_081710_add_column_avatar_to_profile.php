<?php

use yii\db\Migration;

/**
 * Handles adding column_avatar to table `profile`.
 */
class m160616_081710_add_column_avatar_to_profile extends Migration
{
    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'avatar_url', $this->string());
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'avatar_url');
    }
}
