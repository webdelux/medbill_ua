<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_085200_add_column_pacient_signalmark extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160115_085200_add_column_pacient_signalmark cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%pacient_signalmark}}', 'anamnesis_note', $this->text());
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pacient_signalmark}}', 'anamnesis_note');
    }

}