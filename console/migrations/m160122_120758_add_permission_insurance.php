<?php

use yii\db\Schema;
use yii\db\Migration;

class m160122_120758_add_permission_insurance extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160122_120758_add_permission_insurance cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_insurance_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_insurance_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_insurance_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_insurance_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_insurance_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_create',
            'child' => 'emc_insurance_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_update',
            'child' => 'emc_insurance_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_view',
            'child' => 'emc_insurance_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_delete',
            'child' => 'emc_insurance_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_view',
            'child' => 'emc_insurance_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_create',
            'child' => 'emc_insurance_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_update',
            'child' => 'emc_insurance_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_view',
            'child' => 'emc_insurance_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_delete',
            'child' => 'emc_insurance_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_insurance_operation_view',
            'child' => 'emc_insurance_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_insurance_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_insurance_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_insurance_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_insurance_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_insurance_operation_delete',
            'type' => '2'
        ]);
    }
}
