<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_141031_add_data_handbook_emc_organ_complaints extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160112_141031_add_data_handbook_emc_organ_complaints cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $data = [
            [1, 'Анізокорія'],
            [1, 'Астигматизм ока'],
            [1, 'Більмо'],
            [1, 'Бленорея'],
            [1, 'Блефарит'],
            [1, 'Відшарування сітківки'],
            [1, 'Гетерохромія'],
            [1, 'Глаукома'],
            [1, 'Дальтонізм'],
            [1, 'Дейтеранопія'],
            [1, 'Демодекоз'],
            [1, 'Деструкція склоподібного тіла'],
            [1, 'Дистрофії рогівки'],
            [1, 'Діабетична ретинопатія'],
            [1, 'Екзофтальм'],
            [1, 'Електрофтальмія'],
            [1, 'Зіниці Аргайлла Робертсона'],
            [1, 'Ірит'],
            [1, 'Катаракта'],
            [1, 'Кератит'],
            [1, 'Кератоконус'],
            [1, 'Колобома'],
            [1, 'Кон\'юнктивіт'],
            [1, 'Косоокість'],
            [1, 'Ксерофтальмія'],
            [1, 'Куряча сліпота (хвороба)'],
            [1, 'Лагофтальм'],
            [1, 'Офтальмоплегія'],
            [1, 'Панофтальміт'],
            [1, 'Сліпота'],
            [1, 'Тромбоз центральної вени сітківки'],
            [1, 'Фотофобія'],
            [1, 'Хоріосклероз'],
            [1, 'Ячмінь (хвороба)'],

            [5, 'Аудіометрія'],
            [5, 'Глухота'],
            [5, 'Гострий середній отит'],
            [5, 'Мастоїдит'],
            [5, 'Мізофонія'],
            [5, 'Отит'],
            [5, 'Хронічний середній отит'],

            [9, 'Опис'],
        ];

        $i = 1;

        foreach ($data as $value)
        {
            $this->insert('{{%handbook_emc_organ_complaints}}', [
                'id' => $i,
                'handbook_emc_organ_id' => $value[0],
                'name' => $value[1],
                'user_id' => 0
            ]);

            $i++;
        }
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        //$this->truncateTable('{{%handbook_emc_organ_complaints}}');

        for ($i = 1; $i <= 42; $i++)
            $this->delete('{{%handbook_emc_organ_complaints}}', ['id' => $i]);
    }

}