<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_134931_add_data_handbook_emc_list extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151217_134931_add_data_handbook_emc_list cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $data = [
            // Цукровий діабет
            [1, NULL, 'Лікування'],
            [2, NULL, 'Ступінь'],
            [3, NULL, 'Стадія'],

            [4, 1, 'Інсулінотерапія'],
            [5, 1, 'Цукрознижувальні препарати'],
            [6, 1, 'Дієта'],

            [7, 2, 'Легка'],
            [8, 2, 'Середня'],
            [9, 2, 'Важка'],

            [10, 3, 'Компенсація'],
            [11, 3, 'Декомпенсація'],

            // Гіпертонічна хвороба
            [12, NULL, 'Стадія'],
            [13, NULL, 'Ступінь'],
            [14, NULL, 'Ризик'],
            [15, NULL, 'Категорія'],

            [16, 12, 'I'],
            [17, 12, 'II'],
            [18, 12, 'III'],

            [19, 13, '1'],
            [20, 13, '2'],
            [21, 13, '3'],

            [22, 14, 'Невисокий'],
            [23, 14, 'Високий'],
            [24, 14, 'Дуже високий'],

            [25, 15, 'СН 0 - СН І'],
            [26, 15, 'СН ІІ А'],
            [27, 15, 'СН ІІ Б'],
            [28, 15, 'СН ІІІ А'],
            [29, 15, 'СН ІІІ Б'],
        ];

        foreach ($data as $value)
        {
            $this->insert('{{%handbook_emc_list}}', [
                'id' => $value[0],
                'parent_id' => $value[1],
                'name' => $value[2],
                'sort' => 0,
                'user_id' => 0
            ]);
        }
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->truncateTable('{{%handbook_emc_list}}');
    }

}