<?php

use yii\db\Migration;
use backend\modules\handbook\models\Status;

class m160715_112449_add_data_handbook_emc_template extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160715_112449_add_data_handbook_emc_template cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%handbook_emc_template}}', [
            'id' => 2,
            'handbook_emc_category_id' => 209,
            'name' => Yii::t('app', 'protocol_pattern_2'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 3,
            'handbook_emc_category_id' => 209,
            'name' => Yii::t('app', 'protocol_pattern_3'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 4,
            'handbook_emc_category_id' => 209,
            'name' => Yii::t('app', 'protocol_pattern_4'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 5,
            'handbook_emc_category_id' => 209,
            'name' => Yii::t('app', 'protocol_pattern_5'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 6,
            'handbook_emc_category_id' => 210,
            'name' => Yii::t('app', 'protocol_pattern_6'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $this->delete('{{%handbook_emc_template}}', ['id' => 2]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 3]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 4]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 5]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 6]);

        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }

}