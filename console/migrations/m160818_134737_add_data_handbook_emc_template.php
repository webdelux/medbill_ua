<?php

use yii\db\Migration;
use backend\modules\handbook\models\Status;

class m160818_134737_add_data_handbook_emc_template extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160818_134737_add_data_handbook_emc_template cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%handbook_emc_template}}', [
            'id' => 13,
            'handbook_emc_category_id' => 67,
            'name' => Yii::t('app', 'protocol_pattern_13'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 14,
            'handbook_emc_category_id' => 67,
            'name' => Yii::t('app', 'protocol_pattern_14'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 15,
            'handbook_emc_category_id' => 67,
            'name' => Yii::t('app', 'protocol_pattern_15'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $this->delete('{{%handbook_emc_template}}', ['id' => 13]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 14]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 15]);

        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }

}