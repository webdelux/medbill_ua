<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151230_103004_create_pacient_signalmark_complaints_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151230_103004_create_pacient_signalmark_complaints_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_signalmark_complaints}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),
            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_signalmark_complaints}}', 'pacient_id');

        $this->createIndex('idx_description', '{{%pacient_signalmark_complaints}}', 'description');
        $this->createIndex('idx_date_at', '{{%pacient_signalmark_complaints}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_signalmark_complaints}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_signalmark_complaints}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_signalmark_complaints}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_signalmark_complaints}}', 'updated_at');

        $this->addForeignKey('fk_pacient_signalmark_complaints_ib_1', '{{%pacient_signalmark_complaints}}', 'pacient_id', '{{%pacient}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_complaints_ib_1', '{{%pacient_signalmark_complaints}}');

        $this->dropTable('{{%pacient_signalmark_complaints}}');
    }

}