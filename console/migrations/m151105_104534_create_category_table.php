<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151105_104534_create_category_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151105_104534_create_category_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions .= ' AUTO_INCREMENT = 1000';
        }

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer(),

            'name' => $this->string()->notNull(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%category}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%category}}', 'name');
        $this->createIndex('idx_description', '{{%category}}', 'description');

        $this->createIndex('idx_user_id', '{{%category}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%category}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%category}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%category}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%category}}');
    }
}
