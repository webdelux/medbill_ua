<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_144643_add_measurement_data extends Migration
{
    /*
      public function up()
      {

      }

      public function down()
      {
      echo "m151029_144643_add_measurement_data cannot be reverted.\n";

      return false;
      }
     */

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $data = [
            [1, NULL, 'Фізичні одиниці вимірювання', 'Фізичні од.вим.'],
            [2, 1, 'Одиниці вимірювання довжини', 'Довжина'],
            [3, 2, 'Метр', 'м.'],
            [4, 2, 'Кілометр', 'км.'],
            [5, 2, 'Дециметр', 'дм.'],
            [6, 2, 'Сантиметр', 'см.'],
            [7, 2, 'Міліметр', 'мм.'],
            [8, 1, 'Одиниці вимірювання маси', 'Маса'],
            [9, 8, 'Тона', 'т.'],
            [10, 8, 'Центнер', 'ц.'],
            [11, 8, 'Кілограм', 'кг.'],
            [12, 8, 'Грам', 'г.'],
            [13, 1, 'Одиниці вимірювання площі', 'Площа'],
            [14, 13, 'Метр квадратний', 'м.кв.'],
            [15, 13, 'Сантиметр квадратний', 'см.кв.'],
            [16, 13, 'Міліметр квадратний', 'мм.кв.'],
            [17, 1, "Одиниці вимірювання об'єму", "Об'єм"],
            [18, 17, 'Літр', 'л.'],
            [19, 17, 'Мілілітр', 'мл.'],
            [20, 17, 'Дециметр кубічний', 'дм.куб.'],
            [21, 17, 'Сантиметр кубічний', 'см.куб.'],
            [22, 17, 'Сантиметр кубічний', 'см.куб.'],
            [23, 1, "Одиниці вимірювання часу", "Час"],
            [24, 23, 'Година', 'год.'],
            [25, 23, 'Хвилина', 'хв.'],
            [26, 23, 'Секунда', 'сек.'],
            [27, 23, 'Мілісекунда', 'мс.'],
            [28, 1, "Одиниці вимірювання обсягу", "Обсяг"],
            [29, 28, 'Штука', 'шт.'],
            [30, NULL, 'Медичні одиниці вимірювання', 'Медичні од.вим.'],
            [31, 30, 'Ампула', 'амп.'],
            [32, 30, 'Пігулка', 'піг.'],


        ];

        foreach ($data as $key => $value)
        {
            $this->insert('{{%measurement}}', [
                'id' => $value[0],
                'parent_id' => $value[1],
                'name' => $value[2],
                'abbreviation' => $value[3],
            ]);
        }
    }

    public function safeDown()
    {
        $this->truncateTable('{{%measurement}}');
    }

}
