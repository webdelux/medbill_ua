<?php

use yii\db\Migration;
use backend\modules\handbook\models\Status;

class m160811_093224_add_data_handbook_emc_template extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%handbook_emc_template}}', [
            'id' => 10,
            'handbook_emc_category_id' => 210,
            'name' => Yii::t('app', 'protocol_pattern_10'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $this->delete('{{%handbook_emc_template}}', ['id' => 10]);

        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }
}
