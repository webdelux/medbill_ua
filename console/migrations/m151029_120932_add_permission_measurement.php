<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_120932_add_permission_measurement extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151029_104233_add_permission_measurement cannot be reverted.\n";

        return false;
    }
    */
     public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_measurement_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_measurement_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_measurement_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_measurement_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_measurement_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_create',
            'child' => 'handbook_measurement_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_update',
            'child' => 'handbook_measurement_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_view',
            'child' => 'handbook_measurement_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_delete',
            'child' => 'handbook_measurement_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_view',
            'child' => 'handbook_measurement_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_create',
            'child' => 'handbook_measurement_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_update',
            'child' => 'handbook_measurement_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_view',
            'child' => 'handbook_measurement_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_delete',
            'child' => 'handbook_measurement_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_measurement_operation_view',
            'child' => 'handbook_measurement_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_measurement_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_measurement_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_measurement_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_measurement_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_measurement_operation_delete',
            'type' => '2'
        ]);
    }
}
