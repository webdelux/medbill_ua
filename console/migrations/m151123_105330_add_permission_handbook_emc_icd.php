<?php

use yii\db\Schema;
use yii\db\Migration;

class m151123_105330_add_permission_handbook_emc_icd extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151123_105330_add_permission_handbook_emc_icd cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_icd_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'handbookemc_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'handbookemc_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'handbookemc_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'handbookemc_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_icd_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_icd_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_icd_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_icd_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_create',
            'child' => 'handbookemc_icd_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_update',
            'child' => 'handbookemc_icd_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_view',
            'child' => 'handbookemc_icd_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_delete',
            'child' => 'handbookemc_icd_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_view',
            'child' => 'handbookemc_icd_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_create',
            'child' => 'handbookemc_icd_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_update',
            'child' => 'handbookemc_icd_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_view',
            'child' => 'handbookemc_icd_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_delete',
            'child' => 'handbookemc_icd_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_icd_operation_view',
            'child' => 'handbookemc_icd_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_icd_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_icd_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_icd_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_icd_operation_delete'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'handbookemc_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'handbookemc_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'handbookemc_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'handbookemc_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_icd_operation_delete',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_operation_delete',
            'type' => '2'
        ]);
    }

}