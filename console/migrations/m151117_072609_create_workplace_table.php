<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151117_072609_create_workplace_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151117_072609_create_workplace_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%workplace}}', [
            'id' => $this->primaryKey(),

            'name' => $this->string()->notNull(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_name', '{{%workplace}}', 'name');
        $this->createIndex('idx_description', '{{%workplace}}', 'description');

        $this->createIndex('idx_user_id', '{{%workplace}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%workplace}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%workplace}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%workplace}}', 'updated_at');

        $this->createIndex('idx_status_id', '{{%workplace}}', 'status_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%workplace}}');
    }
}
