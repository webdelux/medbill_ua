<?php

use yii\db\Migration;

class m160818_135509_update_data_handbook_emc_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160818_135509_update_data_handbook_emc_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 13], 'id = :id', [':id' => 1589]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 14], 'id = :id', [':id' => 1590]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 15], 'id = :id', [':id' => 1591]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 1589]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 1590]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 1591]);
    }

}