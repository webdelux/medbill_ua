<?php

use yii\db\Migration;

class m160328_080918_add_permission_hospitalization extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160328_080918_add_permission_hospitalization cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_hospitalization_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_hospitalization_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_hospitalization_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_hospitalization_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_hospitalization_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_create',
            'child' => 'emc_hospitalization_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_update',
            'child' => 'emc_hospitalization_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_view',
            'child' => 'emc_hospitalization_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_delete',
            'child' => 'emc_hospitalization_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_view',
            'child' => 'emc_hospitalization_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_create',
            'child' => 'emc_hospitalization_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_update',
            'child' => 'emc_hospitalization_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_view',
            'child' => 'emc_hospitalization_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_delete',
            'child' => 'emc_hospitalization_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_hospitalization_operation_view',
            'child' => 'emc_hospitalization_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_hospitalization_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_hospitalization_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_hospitalization_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_hospitalization_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_hospitalization_operation_delete',
            'type' => '2'
        ]);
    }
}
