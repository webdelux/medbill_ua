<?php

use yii\db\Migration;

class m160624_083723_add_permission_pattern_protocol_to_update extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160624_083723_add_permission_pattern_protocol_to_update cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pattern_protocol_operation_update',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'pattern_protocol_update',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pattern_operation_update',
            'child' => 'pattern_protocol_operation_update'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pattern_protocol_operation_update',
            'child' => 'pattern_protocol_update'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pattern_protocol_operation_update',
            'child' => 'pattern_protocol_update'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pattern_operation_update',
            'child' => 'pattern_protocol_operation_update'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pattern_protocol_update',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pattern_protocol_operation_update',
            'type' => '2'
        ]);
    }

}