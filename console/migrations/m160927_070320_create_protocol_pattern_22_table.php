<?php

//use yii\db\Migration;
use yii\helpers\Html;
use console\migrations\Migration;
use backend\modules\pattern\models\ProtocolPattern_22 as ProtocolPattern;

/**
 * Handles the creation for table `protocol_pattern_22`.
 */
class m160927_070320_create_protocol_pattern_22_table extends Migration
{

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $model = new ProtocolPattern;

        $this->createTable('{{%protocol_pattern_22}}', [
            'id' => $this->primaryKey(),

            'protocol_id' => $this->integer()->notNull(),

            'field_1' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_1'))),
            'field_2' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_2'))),
            'field_3' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_3'))),
            'field_4' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_4'))),
            'field_5' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_5'))),
            'field_6' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_6'))),
            'field_7' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_7'))),
            'field_8' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_8'))),
            'field_9' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_9'))),
            'field_10' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_10'))),
            'field_11' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_11'))),
            'field_12' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_12'))),
            'field_13' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_13'))),
            'field_14' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_14'))),
            'field_15' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_15'))),
            'field_16' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_16'))),
            'field_17' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_17'))),
            'field_18' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_18'))),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_22}}', Yii::t('app', 'protocol_pattern_22'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_22}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_22}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_22}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_22}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_22}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_22_ib_1', '{{%protocol_pattern_22}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_pattern_22_ib_1', '{{%protocol_pattern_22}}');

        $this->dropCommentFromTable('{{%protocol_pattern_22}}');

        $this->dropTable('{{%protocol_pattern_22}}');
    }

}
