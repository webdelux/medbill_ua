<?php

use yii\db\Migration;

/**
 * Handles adding column_department to table `diagnosis`.
 */
class m160520_112000_add_column_department_to_diagnosis extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%diagnosis}}', 'department_id', $this->integer()->after('parent_id'));
        $this->createIndex('idx_department_id', '{{%diagnosis}}', 'department_id');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_department_id', '{{%diagnosis}}');
        $this->dropColumn('{{%diagnosis}}', 'department_id');
    }
}
