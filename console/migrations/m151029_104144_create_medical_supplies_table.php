<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151029_104144_create_medical_supplies_table extends Migration
{
    /*
    public function up()
    {
    }
    public function down()
    {
        echo "m151029_104144_create_medical_supplies_table cannot be reverted.\n";
        return false;
    }
    */

    public function safeUp()
    {
        $this->createTable('{{%medical_supplies}}', [
            'id' => $this->primaryKey(),

            'category_id' => $this->integer(),

            'name' => $this->string()->notNull(),
            'measurement_id' => $this->integer(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_category_id', '{{%medical_supplies}}', 'category_id');

        $this->createIndex('idx_name', '{{%medical_supplies}}', 'name');
        $this->createIndex('idx_measurement_id', '{{%medical_supplies}}', 'measurement_id');
        $this->createIndex('idx_description', '{{%medical_supplies}}', 'description');

        $this->createIndex('idx_user_id', '{{%medical_supplies}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%medical_supplies}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%medical_supplies}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%medical_supplies}}', 'updated_at');

        $this->createIndex('idx_status_id', '{{%medical_supplies}}', 'status_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%medical_supplies}}');
    }
}
