<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160217_093244_create_pacient_planning_treatment_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160217_093244_create_pacient_planning_treatment_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_planning_treatment}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'diagnosis_id' => $this->integer(),
            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),
            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_planning_treatment}}', 'pacient_id');
        $this->createIndex('idx_diagnosis_id', '{{%pacient_planning_treatment}}', 'diagnosis_id');

        $this->createIndex('idx_description', '{{%pacient_planning_treatment}}', 'description');
        $this->createIndex('idx_date_at', '{{%pacient_planning_treatment}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_planning_treatment}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_planning_treatment}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_planning_treatment}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_planning_treatment}}', 'updated_at');

        $this->addForeignKey('fk_pacient_planning_treatment_ib_1', '{{%pacient_planning_treatment}}', 'pacient_id', '{{%pacient}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_planning_treatment_ib_2', '{{%pacient_planning_treatment}}', 'diagnosis_id', '{{%diagnosis}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_planning_treatment_ib_1', '{{%pacient_planning_treatment}}');
        $this->dropForeignKey('fk_pacient_planning_treatment_ib_2', '{{%pacient_planning_treatment}}');

        $this->dropTable('{{%pacient_planning_treatment}}');
    }

}