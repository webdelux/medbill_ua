<?php

//use yii\db\Migration;
use console\migrations\Migration;
use backend\modules\pattern\models\ProtocolPattern_20 as ProtocolPattern;

/**
 * Handles the creation for table `protocol_pattern_20`.
 */
class m160819_114813_create_protocol_pattern_20_table extends Migration
{

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $model = new ProtocolPattern;

        $this->createTable('{{%protocol_pattern_20}}', [
            'id' => $this->primaryKey(),

            'protocol_id' => $this->integer()->notNull(),

            'field_1' => $this->integer()->comment($model->getAttributeLabel('field_1')),
            'field_2' => $this->text()->comment($model->getAttributeLabel('field_2')),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_20}}', Yii::t('app', 'protocol_pattern_20') . '. ' . Yii::t('app', 'Testing for RW'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_20}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_20}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_20}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_20}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_20}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_20_ib_1', '{{%protocol_pattern_20}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_pattern_20_ib_1', '{{%protocol_pattern_20}}');

        $this->dropCommentFromTable('{{%protocol_pattern_20}}');

        $this->dropTable('{{%protocol_pattern_20}}');
    }

}