<?php

use yii\db\Migration;

/**
 * Handles adding column_price to table `medical_supplies_operation`.
 */
class m160520_141033_add_column_price_to_medical_supplies_operation extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%medical_supplies_operation}}', 'total', $this->decimal(10,2)->after('quantity'));
        $this->createIndex('idx_total', '{{%medical_supplies_operation}}', 'total');

        $this->addColumn('{{%medical_supplies_operation}}', 'discount', $this->decimal(10,2)->after('total'));
        $this->createIndex('idx_discount', '{{%medical_supplies_operation}}', 'discount');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_total', '{{%medical_supplies_operation}}');
        $this->dropColumn('{{%medical_supplies_operation}}', 'total');

        $this->dropIndex('idx_discount', '{{%medical_supplies_operation}}');
        $this->dropColumn('{{%medical_supplies_operation}}', 'discount');
    }
}
