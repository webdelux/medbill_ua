<?php

//use yii\db\Migration;
use console\migrations\Migration;

/**
 * Handles the creation for table `protocol_pattern_1_table`.
 */
class m160615_121432_create_protocol_pattern_1_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160615_121432_create_protocol_pattern_1_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%protocol_pattern_1}}', [
            'id' => $this->primaryKey(),

            'protocol_id' => $this->integer()->notNull(),

            'field_1' => $this->string()->comment(Yii::t('app', 'Hemoglobin')),
            'field_2' => $this->string()->comment(Yii::t('app', 'Eritrotsity')),
            'field_3' => $this->string()->comment(Yii::t('app', 'Color index')),
            'field_4' => $this->string()->comment(Yii::t('app', 'The average content of hemoglobin in one erythrocyte')),
            'field_5' => $this->string()->comment(Yii::t('app', 'Leukocytes')),
            'field_6' => $this->string()->comment(Yii::t('app', 'Leukocyte formula')),
            'field_7' => $this->string()->comment(Yii::t('app', 'Erythrocyte sedimentation rate')),
            'field_8' => $this->string()->comment(Yii::t('app', 'Platelets')),
            'field_9' => $this->string()->comment(Yii::t('app', 'Reticulocytes')),
            'field_10' => $this->string()->comment(Yii::t('app', 'The morphology erythrocytes')),
            'field_11' => $this->string()->comment(Yii::t('app', 'The morphology leukocytes')),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_1}}', Yii::t('app', 'protocol_pattern_1'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_1}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_1}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_1}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_1}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_1}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_1_ib_1', '{{%protocol_pattern_1}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_pattern_1_ib_1', '{{%protocol_pattern_1}}');

        $this->dropCommentFromTable('{{%protocol_pattern_1}}');

        $this->dropTable('{{%protocol_pattern_1}}');
    }

}