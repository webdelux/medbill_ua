<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160422_123834_create_pathological_examination_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%pathological_examination}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'department_id' => $this->integer(),
            'doctor_id' => $this->integer()->notNull(),
            'diagnosis_id' => $this->integer()->notNull(),
            'icd' => $this->integer(),

            'pathological_number' => $this->string(),
            'pathological_date' => $this->date()->notNull()->defaultValue(0),

        // Збіг клінічного та патолого-анатомічного діагнозів: Повний збіг (0);Розбіжність основного діагнозу (1);Розбіжність супутнього діагнозу (2);Розбіжність ускладнень діагнозу (3);Повна розбіжність (4).
            'accuracy' => $this->integer()->notNull()->defaultValue(0),

        // Причини розбіжності: Об’єктивні труднощі діагностики (1); Короткочасне перебування в лікарні (2); Недообстеження хворого (3); Переоцінка даних обстеження (4); Рідкісне захворювання (5); Неправильне оформлення діагнозу (6);
            'divergence' => $this->integer()->notNull()->defaultValue(0),

            'ill_time' => $this->string(),
            'cause' => $this->string(),
            'ills' => $this->string(),
            'other_reasons' => $this->string(),
            'pregnancy_interval' => $this->string(),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pathological_examination}}', 'pacient_id');
        $this->createIndex('idx_department_id', '{{%pathological_examination}}', 'department_id');
        $this->createIndex('idx_doctor_id', '{{%pathological_examination}}', 'doctor_id');
        $this->createIndex('idx_diagnosis_id', '{{%pathological_examination}}', 'diagnosis_id');
        $this->createIndex('idx_icd', '{{%pathological_examination}}', 'icd');

        $this->createIndex('idx_pathological_number', '{{%pathological_examination}}', 'pathological_number');
        $this->createIndex('idx_pathological_date', '{{%pathological_examination}}', 'pathological_date');

        $this->createIndex('idx_accuracy', '{{%pathological_examination}}', 'accuracy');
        $this->createIndex('idx_divergence', '{{%pathological_examination}}', 'divergence');

        $this->createIndex('idx_ill_time', '{{%pathological_examination}}', 'ill_time');
        $this->createIndex('idx_cause', '{{%pathological_examination}}', 'cause');
        $this->createIndex('idx_ills', '{{%pathological_examination}}', 'ills');
        $this->createIndex('idx_other_reasons', '{{%pathological_examination}}', 'other_reasons');
        $this->createIndex('idx_pregnancy_interval', '{{%pathological_examination}}', 'pregnancy_interval');


        $this->createIndex('idx_description', '{{%pathological_examination}}', 'description');
        $this->createIndex('idx_user_id', '{{%pathological_examination}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pathological_examination}}', 'updated_user_id');
        $this->createIndex('idx_created_at', '{{%pathological_examination}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pathological_examination}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%pathological_examination}}');
    }

}
