<?php

use yii\db\Migration;

/**
 * Handles adding column_price to table `medical_supplies`.
 */
class m160520_140734_add_column_price_to_medical_supplies extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%medical_supplies}}', 'price', $this->decimal(10,2)->after('measurement_id'));
        $this->createIndex('idx_price', '{{%medical_supplies}}', 'price');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_price', '{{%medical_supplies}}');
        $this->dropColumn('{{%medical_supplies}}', 'price');
    }
}
