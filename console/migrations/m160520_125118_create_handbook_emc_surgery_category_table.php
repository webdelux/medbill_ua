<?php

//use yii\db\Migration;
use console\migrations\Migration;

/**
 * Handles the creation for table `handbook_emc_surgery_category_table`.
 */
class m160520_125118_create_handbook_emc_surgery_category_table extends Migration
{

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%handbook_emc_surgery_category}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer()->notNull(),

            'name' => $this->string(),
            'description' => $this->string(),

            'code' => $this->string(),
            'status' => $this->integer(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%handbook_emc_surgery_category}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%handbook_emc_surgery_category}}', 'name');
        $this->createIndex('idx_description', '{{%handbook_emc_surgery_category}}', 'description');

        $this->createIndex('idx_code', '{{%handbook_emc_surgery_category}}', 'code');
        $this->createIndex('idx_status', '{{%handbook_emc_surgery_category}}', 'status');

        $this->createIndex('idx_user_id', '{{%handbook_emc_surgery_category}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_emc_surgery_category}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_emc_surgery_category}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_emc_surgery_category}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%handbook_emc_surgery_category}}');
    }

}