<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151124_082952_create_handbook_emc_surgery_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151124_082952_create_handbook_emc_surgery_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%handbook_emc_surgery}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer()->notNull(),

            'name' => $this->string(),
            'code' => $this->string(16),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%handbook_emc_surgery}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%handbook_emc_surgery}}', 'name');
        $this->createIndex('idx_code', '{{%handbook_emc_surgery}}', 'code');

        $this->createIndex('idx_user_id', '{{%handbook_emc_surgery}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_emc_surgery}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_emc_surgery}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_emc_surgery}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%handbook_emc_surgery}}');
    }

}