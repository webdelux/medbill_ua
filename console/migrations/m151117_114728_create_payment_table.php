<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151117_114728_create_payment_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151117_114728_create_payment_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%payment}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer(),

            'total' => $this->decimal(10,2),
            'discount' => $this->decimal(10,2),
            'type' => $this->integer(),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

            'payment_method_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%payment}}', 'pacient_id');

        $this->createIndex('idx_total', '{{%payment}}', 'total');
        $this->createIndex('idx_discount', '{{%payment}}', 'discount');
        $this->createIndex('idx_type', '{{%payment}}', 'type');

        $this->createIndex('idx_description', '{{%payment}}', 'description');

        $this->createIndex('idx_user_id', '{{%payment}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%payment}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%payment}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%payment}}', 'updated_at');

        $this->createIndex('idx_payment_method', '{{%payment}}', 'payment_method_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment}}');
    }
}
