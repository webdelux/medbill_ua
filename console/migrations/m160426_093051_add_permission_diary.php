<?php

use yii\db\Migration;

class m160426_093051_add_permission_diary extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160426_093051_add_permission_diary cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_diary_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_diary_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_diary_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_diary_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_diary_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_create',
            'child' => 'emc_diary_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_update',
            'child' => 'emc_diary_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_view',
            'child' => 'emc_diary_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_delete',
            'child' => 'emc_diary_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_view',
            'child' => 'emc_diary_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_create',
            'child' => 'emc_diary_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_update',
            'child' => 'emc_diary_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_view',
            'child' => 'emc_diary_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_delete',
            'child' => 'emc_diary_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_diary_operation_view',
            'child' => 'emc_diary_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_diary_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_diary_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_diary_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_diary_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_diary_operation_delete',
            'type' => '2'
        ]);
    }
}
