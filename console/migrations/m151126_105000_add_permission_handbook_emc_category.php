<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_105000_add_permission_handbook_emc_category extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151126_105000_add_permission_handbook_emc_category cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_category_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_category_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_category_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_category_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_category_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_create',
            'child' => 'handbookemc_category_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_update',
            'child' => 'handbookemc_category_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_view',
            'child' => 'handbookemc_category_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_delete',
            'child' => 'handbookemc_category_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_view',
            'child' => 'handbookemc_category_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_create',
            'child' => 'handbookemc_category_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_update',
            'child' => 'handbookemc_category_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_view',
            'child' => 'handbookemc_category_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_delete',
            'child' => 'handbookemc_category_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_category_operation_view',
            'child' => 'handbookemc_category_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_category_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_category_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_category_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_category_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_category_operation_delete',
            'type' => '2'
        ]);
    }

}