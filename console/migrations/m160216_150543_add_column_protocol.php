<?php

use yii\db\Migration;

class m160216_150543_add_column_protocol extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160216_150543_add_column_protocol cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->addColumn('{{%protocol}}', 'type', $this->integer());    // Тип протоколу (профілактичний огляд, інше);
        $this->addColumn('{{%protocol}}', 'place', $this->integer());   // Місце лікування (поліклініка, вдома, денний стаціонар, стаціонар вдома, стаціонар).

        $this->createIndex('idx_type', '{{%protocol}}', 'type');
        $this->createIndex('idx_place', '{{%protocol}}', 'place');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_type', '{{%protocol}}');
        $this->dropIndex('idx_place', '{{%protocol}}');

        $this->dropColumn('{{%protocol}}', 'place');
        $this->dropColumn('{{%protocol}}', 'type');
    }
}
