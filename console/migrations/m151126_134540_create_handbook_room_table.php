<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151126_134540_create_handbook_room_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151126_134540_create_handbook_room_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%handbook_room}}', [
            'id' => $this->primaryKey(),

            'department_id' => $this->integer(),

            'room' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_department_id', '{{%handbook_room}}', 'department_id');
        $this->createIndex('idx_room', '{{%handbook_room}}', 'room');

        $this->createIndex('idx_description', '{{%handbook_room}}', 'description');

        $this->createIndex('idx_user_id', '{{%handbook_room}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_room}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_room}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_room}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%handbook_room}}');
    }
}
