<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_122524_add_payment_methods_data extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151113_122524_add_payment_methods_data cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $data = [
            [1,  'Готівка', 'Готівковий розрахунок'],
            [2,  'Платіжна картка', 'Розрахунок карткою'],
            [3,  'Переказ', 'Безготівковий переказ'],
        ];

        foreach ($data as $key => $value)
        {
            $this->insert('{{%payment_methods}}', [
                'id' => $value[0],
                'name' => $value[1],
                'description' => $value[2],
            ]);
        }
    }

    public function safeDown()
    {
         $this->truncateTable('{{%payment_methods}}');
    }
}
