<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160208_121441_create_bed_type_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160208_121441_create_beds_type_table cannot be reverted.\n";

        return false;
    }
     *
     */

     public function safeUp()
    {
        $this->createTable('{{%bed_type}}', [
            'id' => $this->primaryKey(),

            'bed_type' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_bed_type', '{{%bed_type}}', 'bed_type');
        $this->createIndex('idx_description', '{{%bed_type}}', 'description');
        $this->createIndex('idx_user_id', '{{%bed_type}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%bed_type}}', 'updated_user_id');
        $this->createIndex('idx_created_at', '{{%bed_type}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%bed_type}}', 'updated_at');


    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%bed_type}}');
    }
}
