<?php

//use yii\db\Migration;
use console\migrations\Migration;

/**
 * Handles the creation for table `epitome_epicrisis`.
 */
class m160712_142026_create_epitome_epicrisis_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%epitome_epicrisis}}', [
            'id' => $this->primaryKey(),
            'diagnosis_id' => $this->integer(),
            'epicrisis_date' => $this->date()->notNull()->defaultValue(0),
            'epicrisis_text' => $this->string(),

            'doctor_id' => $this->integer(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_diagnosis_id', '{{%epitome_epicrisis}}', 'diagnosis_id');
        $this->createIndex('idx_epicrisis_date', '{{%epitome_epicrisis}}', 'epicrisis_date');
        $this->createIndex('idx_doctor_id', '{{%epitome_epicrisis}}', 'doctor_id');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%epitome_epicrisis}}');
    }

}
