<?php

use yii\db\Migration;

class m160715_115412_update_data_handbook_emc_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160715_115412_update_data_handbook_emc_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 2], 'id = :id', [':id' => 2794]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 3], 'id = :id', [':id' => 2806]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 4], 'id = :id', [':id' => 2813]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 5], 'id = :id', [':id' => 2817]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 6], 'id = :id', [':id' => 2820]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2794]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2806]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2813]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2817]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2820]);
    }

}