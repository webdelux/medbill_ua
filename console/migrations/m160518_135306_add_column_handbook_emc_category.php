<?php

use yii\db\Migration;

class m160518_135306_add_column_handbook_emc_category extends Migration
{
   public function safeUp()
    {
        $this->addColumn('{{%handbook_emc_category}}', 'status', $this->integer()->defaultValue(1));
        $this->createIndex('idx_status', '{{%handbook_emc_category}}', 'status');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_status', '{{%handbook_emc_category}}');
        $this->dropColumn('{{%handbook_emc_category}}', 'status');
    }
}
