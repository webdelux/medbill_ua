<?php

//use yii\db\Migration;
use console\migrations\Migration;

/**
 * Handles the creation for table `preoperative_epicrisis`.
 */
class m160607_095614_create_preoperative_epicrisis extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%preoperative_epicrisis}}', [
            'id' => $this->primaryKey(),
            'diagnosis_id' => $this->integer(),

            'concomitant_diseases' => $this->string(),
            'surgery_id' => $this->integer(),
            'contraindication' => $this->string(),
            'anesthesia' => $this->string(),
            'preoperative_preparation_time' => $this->time()->notNull()->defaultValue(0),
            'preoperative_preparation' => $this->string(),

            'doctor_id' => $this->integer(),
            'start_operation_date' => $this->date()->notNull()->defaultValue(0),
            'surgeon_id' => $this->integer(),
            'anesthetist_id' => $this->integer(),
            'operation_early_period' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_diagnosis_id', '{{%preoperative_epicrisis}}', 'diagnosis_id');
        $this->createIndex('idx_surgery_id', '{{%preoperative_epicrisis}}', 'surgery_id');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%preoperative_epicrisis}}');
    }
}
