<?php

use yii\db\Schema;
use yii\db\Migration;
use backend\modules\handbook\models\Status;

class m151027_140058_add_data_status extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151027_140058_add_data_status cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%status}}', [
            'id' => Status::_COMMON,
            'sort' => 0,
            'name' => 'Загальні',
            'user_id' => 0
        ]);
        $this->insert('{{%status}}', [
            'id' => Status::_INACTIVE,
            'parent_id' => 1,
            'sort' => 100,
            'name' => 'Неактивний',
            'user_id' => 0
        ]);
        $this->insert('{{%status}}', [
            'id' => Status::_ACTIVE,
            'parent_id' => 1,
            'sort' => 200,
            'name' => 'Активний',
            'user_id' => 0
        ]);
        $this->insert('{{%status}}', [
            'id' => Status::_BLOCKED,
            'parent_id' => 1,
            'sort' => 300,
            'name' => 'Заблокований',
            'user_id' => 0
        ]);
        $this->insert('{{%status}}', [
            'id' => Status::_ARCHIVE,
            'parent_id' => 1,
            'sort' => 400,
            'name' => 'Архів',
            'user_id' => 0
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%status}}', [
            'id' => Status::_COMMON,
        ]);
        $this->delete('{{%status}}', [
            'id' => Status::_INACTIVE,
        ]);
        $this->delete('{{%status}}', [
            'id' => Status::_ACTIVE,
        ]);
        $this->delete('{{%status}}', [
            'id' => Status::_BLOCKED,
        ]);
        $this->delete('{{%status}}', [
            'id' => Status::_ARCHIVE,
        ]);
    }

}