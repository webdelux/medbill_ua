<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160328_080832_create_hospitalization_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%hospitalization}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'diagnosis_id' => $this->integer()->notNull()->defaultValue(0),
            'department_id' => $this->integer(),
            'refferal_id' => $this->integer(),
            'hospitalization_type' => $this->integer()->notNull()->defaultValue(0),

            'hospitalization_date' => $this->timestamp()->notNull()->defaultValue(0),
            'hospitalization_date_out' => $this->timestamp()->notNull()->defaultValue(0),
            'diagnosis_in' => $this->string(),
            'hospitalization_interval' => $this->integer(),
            'moved_to' => $this->string(),          //куди переведено при виписці
            'cure_type' => $this->integer(),            // проведене лікування - Спеціальне, паліативне, симптомічне
            'cure_working' => $this->integer(),            // відновлення працездатності (відновлена повністю, знижена, тимчасово втрачена, стійко втрачена у зв’язку із захворюванням, стійко втрачена з інших причин);
            'cure_result' => $this->integer(),          // результат лікування (виписаний(а) з одужанням – 1, поліпшенням – 2, погіршенням – 3, без змін – 4; помер (ла) – 5; переведений(а) в інший лікувальний заклад – 6; переведений(а) в інше відділення – 7; здоровий(а) – 8);
            'ill_days' => $this->string(),              // проведено ліжко-днів

            'diagnosis_end_id' => $this->integer()->notNull()->defaultValue(0),     // діагноз, при виписці
            'resume' => $this->string(),              // висновок для експертизи


            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%hospitalization}}', 'pacient_id');
        $this->createIndex('idx_diagnosis_id', '{{%hospitalization}}', 'diagnosis_id');
        $this->createIndex('idx_department_id', '{{%hospitalization}}', 'department_id');
        $this->createIndex('idx_refferal_id', '{{%hospitalization}}', 'refferal_id');
        $this->createIndex('idx_hospitalization_type', '{{%hospitalization}}', 'hospitalization_type');

        $this->createIndex('idx_hospitalization_date', '{{%hospitalization}}', 'hospitalization_date');
        $this->createIndex('idx_hospitalization_date_out', '{{%hospitalization}}', 'hospitalization_date_out');
        $this->createIndex('idx_diagnosis_in', '{{%hospitalization}}', 'diagnosis_in');
        $this->createIndex('idx_hospitalization_interval', '{{%hospitalization}}', 'hospitalization_interval');
        $this->createIndex('idx_moved_to', '{{%hospitalization}}', 'moved_to');
        $this->createIndex('idx_cure_type', '{{%hospitalization}}', 'cure_type');
        $this->createIndex('idx_cure_working', '{{%hospitalization}}', 'cure_working');
        $this->createIndex('idx_cure_result', '{{%hospitalization}}', 'cure_result');
        $this->createIndex('idx_ill_days', '{{%hospitalization}}', 'ill_days');
        $this->createIndex('idx_diagnosis_end_id', '{{%hospitalization}}', 'diagnosis_end_id');
        $this->createIndex('idx_resume', '{{%hospitalization}}', 'resume');

        $this->createIndex('idx_description', '{{%hospitalization}}', 'description');
        $this->createIndex('idx_user_id', '{{%hospitalization}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%hospitalization}}', 'updated_user_id');
        $this->createIndex('idx_created_at', '{{%hospitalization}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%hospitalization}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%hospitalization}}');
    }
}
