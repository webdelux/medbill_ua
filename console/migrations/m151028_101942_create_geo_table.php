<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151028_101942_create_geo_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151028_101942_create_geo_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%geo_country}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(64),
        ], $this->tableOptions);

        $this->createIndex('idx_title', '{{%geo_country}}', 'title');


        $this->createTable('{{%geo_region}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'title' => $this->string(160),
        ], $this->tableOptions);

        $this->createIndex('idx_country_id', '{{%geo_region}}', 'country_id');
        $this->createIndex('idx_title', '{{%geo_region}}', 'title');


        $this->createTable('{{%geo_city}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'important' => $this->smallInteger()->notNull(),
            'region_id' => $this->integer(),
            'title' => $this->string(160),
            'area' => $this->string(160),
            'region' => $this->string(160),
        ], $this->tableOptions);

        $this->createIndex('idx_country_id', '{{%geo_city}}', 'country_id');
        $this->createIndex('idx_important', '{{%geo_city}}', 'important');
        $this->createIndex('idx_region_id', '{{%geo_city}}', 'region_id');
        $this->createIndex('idx_title', '{{%geo_city}}', 'title');
        $this->createIndex('idx_area', '{{%geo_city}}', 'area');
        $this->createIndex('idx_region', '{{%geo_city}}', 'region');


        $this->addForeignKey(
            'fk_region_country', '{{%geo_region}}', 'country_id', '{{%geo_country}}', 'id', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk_city_country', '{{%geo_city}}', 'country_id', '{{%geo_country}}', 'id', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk_city_region', '{{%geo_city}}', 'region_id', '{{%geo_region}}', 'id', 'RESTRICT'
        );
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%geo_city}}');
        $this->dropTable('{{%geo_region}}');
        $this->dropTable('{{%geo_country}}');
    }

}