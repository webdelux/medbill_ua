<?php

use yii\db\Migration;

class m160518_115042_add_column_handbook_emc_protocol extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%handbook_emc_protocol}}', 'code', $this->string()->after('id'));
        $this->createIndex('idx_code', '{{%handbook_emc_protocol}}', 'code');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_code', '{{%handbook_emc_protocol}}');
        $this->dropColumn('{{%handbook_emc_protocol}}', 'code');
    }



}
