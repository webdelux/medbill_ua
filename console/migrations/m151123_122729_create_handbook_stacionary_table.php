<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151123_122729_create_handbook_stacionary_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151123_122729_create_handbook_stacionary_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%handbook_stacionary}}', [
            'id' => $this->primaryKey(),

            'room_id' => $this->integer(),

            'place' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

            'pacient_id' => $this->integer()->notNull()->defaultValue(0),  //0 - empty place;
        ], $this->tableOptions);

        $this->createIndex('idx_room_id', '{{%handbook_stacionary}}', 'room_id');
        $this->createIndex('idx_place', '{{%handbook_stacionary}}', 'place');
        $this->createIndex('idx_description', '{{%handbook_stacionary}}', 'description');

        $this->createIndex('idx_user_id', '{{%handbook_stacionary}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_stacionary}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_stacionary}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_stacionary}}', 'updated_at');

        $this->createIndex('idx_pacient_id', '{{%handbook_stacionary}}', 'pacient_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%handbook_stacionary}}');
    }
}
