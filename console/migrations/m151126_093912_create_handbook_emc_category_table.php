<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151126_093912_create_handbook_emc_category_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151126_093912_create_handbook_emc_category_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions .= ' AUTO_INCREMENT = 1000';
        }

        $this->createTable('{{%handbook_emc_category}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer()->notNull(),

            'name' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%handbook_emc_category}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%handbook_emc_category}}', 'name');
        $this->createIndex('idx_description', '{{%handbook_emc_category}}', 'description');

        $this->createIndex('idx_user_id', '{{%handbook_emc_category}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_emc_category}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_emc_category}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_emc_category}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%handbook_emc_category}}');
    }

}