<?php

use yii\db\Schema;
use yii\db\Migration;

class m151015_143931_add_permission_department extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151015_143931_add_permission_department cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'handbook_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'handbook_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'handbook_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'handbook_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_department_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_department_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_department_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_department_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_create',
            'child' => 'handbook_department_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_update',
            'child' => 'handbook_department_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_view',
            'child' => 'handbook_department_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_delete',
            'child' => 'handbook_department_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_view',
            'child' => 'handbook_department_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_create',
            'child' => 'handbook_department_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_update',
            'child' => 'handbook_department_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_view',
            'child' => 'handbook_department_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_delete',
            'child' => 'handbook_department_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department_operation_view',
            'child' => 'handbook_department_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_department_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_department_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_department_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_department_operation_delete'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'handbook_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'handbook_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'handbook_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'handbook_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department_operation_delete',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_operation_delete',
            'type' => '2'
        ]);
    }

}