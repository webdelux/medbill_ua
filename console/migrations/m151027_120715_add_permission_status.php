<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_120715_add_permission_status extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151027_120715_add_permission_status cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_status_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_status_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_status_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_status_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_status_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_create',
            'child' => 'handbook_status_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_update',
            'child' => 'handbook_status_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_view',
            'child' => 'handbook_status_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_delete',
            'child' => 'handbook_status_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_view',
            'child' => 'handbook_status_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_create',
            'child' => 'handbook_status_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_update',
            'child' => 'handbook_status_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_view',
            'child' => 'handbook_status_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_delete',
            'child' => 'handbook_status_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_status_operation_view',
            'child' => 'handbook_status_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_status_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_status_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_status_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_status_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_status_operation_delete',
            'type' => '2'
        ]);
    }

}