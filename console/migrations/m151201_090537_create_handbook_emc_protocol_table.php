<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151201_090537_create_handbook_emc_protocol_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151201_090537_create_handbook_emc_protocol_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%handbook_emc_protocol}}', [
            'id' => $this->primaryKey(),

            'handbook_emc_category_id' => $this->integer()->notNull(),
            'handbook_emc_template_id' => $this->integer()->notNull(),

            'name' => $this->string(),
            'abbreviation' => $this->string(128),
            'description' => $this->text(),

            'amount' => $this->decimal(10, 2)->notNull()->defaultValue(0),

            'execution' => $this->smallInteger()->notNull()->defaultValue(0),
            'required' => $this->smallInteger()->notNull()->defaultValue(0),

            'status_id' => $this->integer(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_handbook_emc_category_id', '{{%handbook_emc_protocol}}', 'handbook_emc_category_id');
        $this->createIndex('idx_handbook_emc_template_id', '{{%handbook_emc_protocol}}', 'handbook_emc_template_id');

        $this->createIndex('idx_name', '{{%handbook_emc_protocol}}', 'name');
        $this->createIndex('idx_abbreviation', '{{%handbook_emc_protocol}}', 'abbreviation');

        $this->createIndex('idx_amount', '{{%handbook_emc_protocol}}', 'amount');

        $this->createIndex('idx_execution', '{{%handbook_emc_protocol}}', 'execution');
        $this->createIndex('idx_required', '{{%handbook_emc_protocol}}', 'required');

        $this->createIndex('idx_status_id', '{{%handbook_emc_protocol}}', 'status_id');

        $this->createIndex('idx_user_id', '{{%handbook_emc_protocol}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_emc_protocol}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_emc_protocol}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_emc_protocol}}', 'updated_at');

        $this->addForeignKey('fk_handbook_emc_protocol_category', '{{%handbook_emc_protocol}}', 'handbook_emc_category_id',
                '{{%handbook_emc_category}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_handbook_emc_protocol_template', '{{%handbook_emc_protocol}}', 'handbook_emc_template_id',
                '{{%handbook_emc_template}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_handbook_emc_template_category', '{{%handbook_emc_template}}', 'handbook_emc_category_id',
                '{{%handbook_emc_category}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_handbook_emc_protocol_category', '{{%handbook_emc_protocol}}');
        $this->dropForeignKey('fk_handbook_emc_protocol_template', '{{%handbook_emc_protocol}}');

        $this->dropForeignKey('fk_handbook_emc_template_category', '{{%handbook_emc_template}}');

        $this->dropTable('{{%handbook_emc_protocol}}');
    }

}