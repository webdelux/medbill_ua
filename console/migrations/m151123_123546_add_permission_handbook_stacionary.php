<?php

use yii\db\Schema;
use yii\db\Migration;

class m151123_123546_add_permission_handbook_stacionary extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151123_123546_add_permission_handbook_stacionary cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_index',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_rooms',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_handbook-stacionary_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_handbook-stacionary_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_handbook-stacionary_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_handbook-stacionary_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_create',
            'child' => 'handbook_handbook-stacionary_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_update',
            'child' => 'handbook_handbook-stacionary_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_view',
            'child' => 'handbook_handbook-stacionary_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_delete',
            'child' => 'handbook_handbook-stacionary_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_view',
            'child' => 'handbook_handbook-stacionary_index'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_view',
            'child' => 'handbook_handbook-stacionary_rooms'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_create',
            'child' => 'handbook_handbook-stacionary_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_update',
            'child' => 'handbook_handbook-stacionary_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_view',
            'child' => 'handbook_handbook-stacionary_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_delete',
            'child' => 'handbook_handbook-stacionary_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_view',
            'child' => 'handbook_handbook-stacionary_index'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_handbook-stacionary_operation_view',
            'child' => 'handbook_handbook-stacionary_rooms'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_handbook-stacionary_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_handbook-stacionary_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_handbook-stacionary_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_handbook-stacionary_operation_delete'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_index',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_rooms',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_handbook-stacionary_operation_delete',
            'type' => '2'
        ]);
    }
}
