<?php

use yii\db\Migration;

class m160607_112353_add_column_pacient_signalmark_experts extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160607_112353_add_column_pacient_signalmark_experts cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%pacient_signalmark_experts}}', 'diagnosis_id', $this->integer()->after('id'));
        $this->createIndex('idx_diagnosis_id', '{{%pacient_signalmark_experts}}', 'diagnosis_id');
        $this->addForeignKey('fk_pacient_signalmark_experts_ib_3', '{{%pacient_signalmark_experts}}', 'diagnosis_id', '{{%diagnosis}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_experts_ib_3', '{{%pacient_signalmark_experts}}');
        $this->dropIndex('idx_diagnosis_id', '{{%pacient_signalmark_experts}}');
        $this->dropColumn('{{%pacient_signalmark_experts}}', 'diagnosis_id');
    }

}