<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `countries`.
 */
class m160523_140144_add_columns_to_countries extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%geo_country}}', 'code1', $this->string());
        $this->createIndex('idx_code1', '{{%geo_country}}', 'code1');

        $this->addColumn('{{%geo_country}}', 'code2', $this->string());
        $this->createIndex('idx_code2', '{{%geo_country}}', 'code2');

        $this->addColumn('{{%geo_country}}', 'code3', $this->string());
        $this->createIndex('idx_code3', '{{%geo_country}}', 'code3');

        $this->addColumn('{{%geo_country}}', 'note', $this->string());

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_code1', '{{%geo_country}}');
        $this->dropColumn('{{%geo_country}}', 'code1');

        $this->dropIndex('idx_code2', '{{%geo_country}}');
        $this->dropColumn('{{%geo_country}}', 'code2');

        $this->dropIndex('idx_code3', '{{%geo_country}}');
        $this->dropColumn('{{%geo_country}}', 'code3');

        $this->dropColumn('{{%geo_country}}', 'note');

    }
}
