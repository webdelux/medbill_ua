<?php

use yii\helpers\Html;
//use yii\db\Migration;
use console\migrations\Migration;
use backend\modules\pattern\models\ProtocolPattern_31 as ProtocolPattern;

/**
 * Handles the creation for table `protocol_pattern_31`.
 */
class m161006_092621_create_protocol_pattern_31_table extends Migration
{

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $model = new ProtocolPattern;

        $this->createTable('{{%protocol_pattern_31}}', [
            'id' => $this->primaryKey(),

            'protocol_id' => $this->integer()->notNull(),

            'field_1' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_1'))),
            'field_2' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_2'))),
            'field_3' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_3'))),
            'field_4' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_4'))),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_31}}', Yii::t('app', 'protocol_pattern_31'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_31}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_31}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_31}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_31}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_31}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_31_ib_1', '{{%protocol_pattern_31}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_pattern_31_ib_1', '{{%protocol_pattern_31}}');

        $this->dropCommentFromTable('{{%protocol_pattern_31}}');

        $this->dropTable('{{%protocol_pattern_31}}');
    }

}
