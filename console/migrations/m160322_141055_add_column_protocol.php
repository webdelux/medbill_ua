<?php

use yii\db\Migration;

class m160322_141055_add_column_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160322_141055_add_column_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%protocol}}', 'diagnosis_id', $this->integer());
        $this->createIndex('idx_diagnosis_id', '{{%protocol}}', 'diagnosis_id');
        $this->addForeignKey('fk_protocol_ib_1', '{{%protocol}}', 'diagnosis_id', '{{%diagnosis}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_ib_1', '{{%protocol}}');
        $this->dropIndex('idx_diagnosis_id', '{{%protocol}}');
        $this->dropColumn('{{%protocol}}', 'diagnosis_id');
    }

}