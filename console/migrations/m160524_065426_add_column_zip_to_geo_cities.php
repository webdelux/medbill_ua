<?php

use yii\db\Migration;

/**
 * Handles adding column_zip to table `geo_cities`.
 */
class m160524_065426_add_column_zip_to_geo_cities extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%geo_city}}', 'zip', $this->string());
        $this->createIndex('idx_zip', '{{%geo_city}}', 'zip');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_zip', '{{%geo_city}}');
        $this->dropColumn('{{%geo_city}}', 'zip');

    }
}
