<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160122_085431_create_insurance_handbook_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160122_085431_create_insurance_handbook_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%insurance_handbook}}', [
            'id' => $this->primaryKey(),

            'name' => $this->string()->notNull(),
            'address' => $this->string(),
            'contacts' => $this->string(),
            'www' => $this->string(),

            'edrpou' => $this->string(32),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_name', '{{%insurance_handbook}}', 'name');
        $this->createIndex('idx_address', '{{%insurance_handbook}}', 'address');
        $this->createIndex('idx_contacts', '{{%insurance_handbook}}', 'contacts');
        $this->createIndex('idx_www', '{{%insurance_handbook}}', 'www');
        $this->createIndex('idx_edrpou', '{{%insurance_handbook}}', 'edrpou');

        $this->createIndex('idx_description', '{{%insurance_handbook}}', 'description');

        $this->createIndex('idx_user_id', '{{%insurance_handbook}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%insurance_handbook}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%insurance_handbook}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%insurance_handbook}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%insurance_handbook}}');
    }
}
