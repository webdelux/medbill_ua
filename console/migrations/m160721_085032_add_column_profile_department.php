<?php

use yii\db\Migration;

class m160721_085032_add_column_profile_department extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160721_085032_add_column_profile_department cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_department}}', 'working_days', $this->string()->after('department_id')->notNull()->defaultValue("mon,tue,wed,thu,fri,sat,sun"));
        $this->createIndex('idx_working_days', '{{%profile_department}}', 'working_days');

        // Monday
        $this->addColumn('{{%profile_department}}', 'working_time_mon_from', $this->time()->after('working_days')->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile_department}}', 'working_time_mon_to', $this->time()->after('working_time_mon_from')->notNull()->defaultValue('22:00:00'));
        // Tuesday
        $this->addColumn('{{%profile_department}}', 'working_time_tue_from', $this->time()->after('working_time_mon_to')->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile_department}}', 'working_time_tue_to', $this->time()->after('working_time_tue_from')->notNull()->defaultValue('22:00:00'));
        // Wednesday
        $this->addColumn('{{%profile_department}}', 'working_time_wed_from', $this->time()->after('working_time_tue_to')->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile_department}}', 'working_time_wed_to', $this->time()->after('working_time_wed_from')->notNull()->defaultValue('22:00:00'));
        // Thursday
        $this->addColumn('{{%profile_department}}', 'working_time_thu_from', $this->time()->after('working_time_wed_to')->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile_department}}', 'working_time_thu_to', $this->time()->after('working_time_thu_from')->notNull()->defaultValue('22:00:00'));
        // Friday
        $this->addColumn('{{%profile_department}}', 'working_time_fri_from', $this->time()->after('working_time_thu_to')->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile_department}}', 'working_time_fri_to', $this->time()->after('working_time_fri_from')->notNull()->defaultValue('22:00:00'));
        // Saturday
        $this->addColumn('{{%profile_department}}', 'working_time_sat_from', $this->time()->after('working_time_fri_to')->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile_department}}', 'working_time_sat_to', $this->time()->after('working_time_sat_from')->notNull()->defaultValue('22:00:00'));
        // Sunday
        $this->addColumn('{{%profile_department}}', 'working_time_sun_from', $this->time()->after('working_time_sat_to')->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile_department}}', 'working_time_sun_to', $this->time()->after('working_time_sun_from')->notNull()->defaultValue('22:00:00'));
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        // Monday
        $this->dropColumn('{{%profile_department}}', 'working_time_mon_from');
        $this->dropColumn('{{%profile_department}}', 'working_time_mon_to');
        // Tuesday
        $this->dropColumn('{{%profile_department}}', 'working_time_tue_from');
        $this->dropColumn('{{%profile_department}}', 'working_time_tue_to');
        // Wednesday
        $this->dropColumn('{{%profile_department}}', 'working_time_wed_from');
        $this->dropColumn('{{%profile_department}}', 'working_time_wed_to');
        // Thursday
        $this->dropColumn('{{%profile_department}}', 'working_time_thu_from');
        $this->dropColumn('{{%profile_department}}', 'working_time_thu_to');
        // Friday
        $this->dropColumn('{{%profile_department}}', 'working_time_fri_from');
        $this->dropColumn('{{%profile_department}}', 'working_time_fri_to');
        // Saturday
        $this->dropColumn('{{%profile_department}}', 'working_time_sat_from');
        $this->dropColumn('{{%profile_department}}', 'working_time_sat_to');
        // Sunday
        $this->dropColumn('{{%profile_department}}', 'working_time_sun_from');
        $this->dropColumn('{{%profile_department}}', 'working_time_sun_to');

        $this->dropIndex('idx_working_days', '{{%profile_department}}');
        $this->dropColumn('{{%profile_department}}', 'working_days');
    }

}