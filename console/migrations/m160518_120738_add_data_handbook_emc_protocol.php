<?php

use yii\db\Migration;

class m160518_120738_add_data_handbook_emc_protocol extends Migration
{

    public function safeUp()
    {
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $this->truncateTable('{{%handbook_emc_protocol}}');

        $this->execute('SET FOREIGN_KEY_CHECKS = 1');

    }

}
