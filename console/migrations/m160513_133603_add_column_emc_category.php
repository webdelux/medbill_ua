<?php

use yii\db\Migration;

class m160513_133603_add_column_emc_category extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%handbook_emc_category}}', 'code', $this->string());
        $this->createIndex('idx_code', '{{%handbook_emc_category}}', 'code');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_code', '{{%handbook_emc_category}}');
        $this->dropColumn('{{%handbook_emc_category}}', 'code');
    }
}
