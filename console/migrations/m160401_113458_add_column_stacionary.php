<?php

use yii\db\Migration;

class m160401_113458_add_column_stacionary extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160401_113458_add_column_stacionary cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->addColumn('{{%stacionary}}', 'date_stacionary_out', $this->timestamp()->notNull()->defaultValue(0));     // дата вибуття з палати/ліжка
        $this->addColumn('{{%stacionary}}', 'diagnosis_id', $this->integer()->notNull()->defaultValue(0));     // прикріпляємо до таблиці діагнозів




        $this->createIndex('idx_date_stacionary_out', '{{%stacionary}}', 'date_stacionary_out');
        $this->createIndex('idx_diagnosis_id', '{{%stacionary}}', 'diagnosis_id');


        $this->dropIndex('idx_type_note', '{{%stacionary}}');
        $this->dropIndex('idx_archive', '{{%stacionary}}');


        $this->dropColumn('{{%stacionary}}', 'type_note');
        $this->dropColumn('{{%stacionary}}', 'archive');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_date_stacionary_out', '{{%stacionary}}');
        $this->dropIndex('idx_diagnosis_id', '{{%stacionary}}');


        $this->dropColumn('{{%stacionary}}', 'date_stacionary_out');
        $this->dropColumn('{{%stacionary}}', 'diagnosis_id');


        $this->addColumn('{{%stacionary}}', 'type_note', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%stacionary}}', 'archive', $this->integer()->notNull()->defaultValue(0));

        $this->createIndex('idx_type_note', '{{%stacionary}}', 'type_note');
        $this->createIndex('idx_archive', '{{%stacionary}}', 'archive');

    }
}
