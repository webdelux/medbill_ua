<?php

use yii\db\Migration;

class m160818_125510_change_auto_increment_for_handbook_emc_template_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160818_125510_change_auto_increment_for_handbook_emc_template_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql')
            $this->execute('ALTER TABLE {{%handbook_emc_template}} AUTO_INCREMENT = 10000');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        if ($this->db->driverName === 'mysql')
            $this->execute('ALTER TABLE {{%handbook_emc_template}} AUTO_INCREMENT = 13');
    }

}