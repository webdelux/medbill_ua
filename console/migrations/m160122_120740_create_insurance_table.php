<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160122_120740_create_insurance_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160122_120740_create_insurance_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%insurance}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer(),

            'insurance_id' => $this->integer(),

            'insurance_number' => $this->string(),
            'insurance_date' => $this->timestamp()->notNull()->defaultValue(0),
            'insurance_date_end' => $this->timestamp()->notNull()->defaultValue(0),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%insurance}}', 'pacient_id');
        $this->createIndex('idx_insurance_id', '{{%insurance}}', 'insurance_id');

        $this->createIndex('idx_insurance_number', '{{%insurance}}', 'insurance_number');
        $this->createIndex('idx_insurance_date', '{{%insurance}}', 'insurance_date');
        $this->createIndex('idx_insurance_date_end', '{{%insurance}}', 'insurance_date_end');

        $this->createIndex('idx_description', '{{%insurance}}', 'description');

        $this->createIndex('idx_user_id', '{{%insurance}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%insurance}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%insurance}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%insurance}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%insurance}}');
    }
}
