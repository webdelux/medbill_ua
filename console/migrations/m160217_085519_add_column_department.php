<?php

use yii\db\Migration;

class m160217_085519_add_column_department extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160216_150543_add_column_protocol cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->addColumn('{{%department}}', 'city_type', $this->integer());    // Приналежність до типу (область, район, місто, село);
        $this->addColumn('{{%department}}', 'category_id', $this->integer());   // Тип Профілю із довідника.

        $this->createIndex('idx_city_type', '{{%department}}', 'city_type');
        $this->createIndex('idx_category_id', '{{%department}}', 'category_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_city_type', '{{%department}}');
        $this->dropIndex('idx_category_id', '{{%department}}');

        $this->dropColumn('{{%department}}', 'city_type');
        $this->dropColumn('{{%department}}', 'category_id');
    }
}
