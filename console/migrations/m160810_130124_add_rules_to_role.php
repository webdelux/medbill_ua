<?php

use yii\db\Migration;

class m160810_130124_add_rules_to_role extends Migration
{
    public function safeUp()
    {
        $data = [
            ['user-viewer_index'],
        ];

        foreach ($data as $value)
        {
            $this->insert('{{%auth_item_child}}', [
                'parent' => backend\models\User::ROLE_DOCTOR,
                'child' => $value[0],
            ]);
        }
    }

    public function safeDown()
    {
         $data = [
            ['user-viewer_index'],
        ];

        foreach ($data as $value)
        {
            $this->delete('{{%auth_item_child}}', [
                'parent' => backend\models\User::ROLE_DOCTOR,
                'child' => $value[0],
            ]);
        }
    }
}
