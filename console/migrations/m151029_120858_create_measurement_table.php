<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151029_120858_create_measurement_table extends Migration
{
    /*
    public function up()
    {
    }
    public function down()
    {
        echo "m151029_104144_create_measurement_table cannot be reverted.\n";
        return false;
    }
    */

    public function safeUp()
    {
        $this->createTable('{{%measurement}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer()->notNull(),

            'name' => $this->string(),
            'abbreviation' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%measurement}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%measurement}}', 'name');
        $this->createIndex('idx_abbreviation', '{{%measurement}}', 'abbreviation');
        $this->createIndex('idx_description', '{{%measurement}}', 'description');

        $this->createIndex('idx_user_id', '{{%measurement}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%measurement}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%measurement}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%measurement}}', 'updated_at');

        $this->createIndex('idx_status_id', '{{%measurement}}', 'status_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%measurement}}');
    }
}
