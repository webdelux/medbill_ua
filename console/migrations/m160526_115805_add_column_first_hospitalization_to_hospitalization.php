<?php

use yii\db\Migration;

/**
 * Handles adding column_first_hospitalization to table `hospitalization`.
 */
class m160526_115805_add_column_first_hospitalization_to_hospitalization extends Migration
{
   public function safeUp()
    {
        $this->addColumn('{{%hospitalization}}', 'first_hospitalization', $this->integer());
        $this->createIndex('idx_first_hospitalization', '{{%hospitalization}}', 'first_hospitalization');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_first_hospitalization', '{{%hospitalization}}');
        $this->dropColumn('{{%hospitalization}}', 'first_hospitalization');

    }
}
