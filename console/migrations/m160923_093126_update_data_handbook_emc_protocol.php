<?php

use yii\db\Migration;

class m160923_093126_update_data_handbook_emc_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160923_093126_update_data_handbook_emc_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 21], 'id = :id', [':id' => 2844]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 2844]);
    }

}
