<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160322_141848_create_incapacity_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%incapacity}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'diagnosis_id' => $this->integer()->notNull(),
            'icd' => $this->integer(),
            'doctor_id' => $this->integer()->notNull(),
            'department_id' => $this->integer(),
            'incapacity_type' => $this->integer()->notNull()->defaultValue(0),

            'incapacity_number' => $this->string(),
            'incapacity_date' => $this->date()->notNull()->defaultValue(0),
            'incapacity_date_start' => $this->date()->notNull()->defaultValue(0),
            'incapacity_date_end' => $this->date()->defaultValue(0),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%incapacity}}', 'pacient_id');
        $this->createIndex('idx_diagnosis_id', '{{%incapacity}}', 'diagnosis_id');
        $this->createIndex('idx_icd', '{{%incapacity}}', 'icd');
        $this->createIndex('idx_doctor_id', '{{%incapacity}}', 'doctor_id');
        $this->createIndex('idx_department_id', '{{%incapacity}}', 'department_id');
        $this->createIndex('idx_incapacity_type', '{{%incapacity}}', 'incapacity_type');
        $this->createIndex('idx_incapacity_number', '{{%incapacity}}', 'incapacity_number');
        $this->createIndex('idx_incapacity_date', '{{%incapacity}}', 'incapacity_date');
        $this->createIndex('idx_incapacity_date_start', '{{%incapacity}}', 'incapacity_date_start');
        $this->createIndex('idx_incapacity_date_end', '{{%incapacity}}', 'incapacity_date_end');
        $this->createIndex('idx_description', '{{%incapacity}}', 'description');
        $this->createIndex('idx_user_id', '{{%incapacity}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%incapacity}}', 'updated_user_id');
        $this->createIndex('idx_created_at', '{{%incapacity}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%incapacity}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%incapacity}}');
    }

}
