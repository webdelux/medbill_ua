<?php

//use yii\db\Migration;
use console\migrations\Migration;

/**
 * Handles the creation for table `departments_category_table`.
 */
class m160513_084305_create_departments_category_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%department_category}}', [
            'id' => $this->primaryKey(),

            'parent_id' => $this->integer(),
            'sort' => $this->integer()->notNull(),

            'name' => $this->string(),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_parent_sort', '{{%department_category}}', ['parent_id', 'sort']);

        $this->createIndex('idx_name', '{{%department_category}}', 'name');
        $this->createIndex('idx_description', '{{%department_category}}', 'description');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%department_category}}');
    }
}
