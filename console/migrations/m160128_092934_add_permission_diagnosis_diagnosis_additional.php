<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_092934_add_permission_diagnosis_diagnosis_additional extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160128_092934_add_permission_diagnosis_diagnosis_additional cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_create',
            'child' => 'diagnosis_diagnosis-additional_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_update',
            'child' => 'diagnosis_diagnosis-additional_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_view',
            'child' => 'diagnosis_diagnosis-additional_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_delete',
            'child' => 'diagnosis_diagnosis-additional_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_create',
            'child' => 'diagnosis_diagnosis-additional_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_update',
            'child' => 'diagnosis_diagnosis-additional_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_view',
            'child' => 'diagnosis_diagnosis-additional_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_delete',
            'child' => 'diagnosis_diagnosis-additional_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_view',
            'child' => 'diagnosis_diagnosis-additional_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_create',
            'child' => 'diagnosis_diagnosis-additional_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_update',
            'child' => 'diagnosis_diagnosis-additional_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_view',
            'child' => 'diagnosis_diagnosis-additional_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_delete',
            'child' => 'diagnosis_diagnosis-additional_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis-additional_operation_view',
            'child' => 'diagnosis_diagnosis-additional_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_create',
            'child' => 'diagnosis_diagnosis-additional_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_update',
            'child' => 'diagnosis_diagnosis-additional_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_view',
            'child' => 'diagnosis_diagnosis-additional_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_operation_delete',
            'child' => 'diagnosis_diagnosis-additional_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis-additional_operation_delete',
            'type' => '2'
        ]);
    }

}