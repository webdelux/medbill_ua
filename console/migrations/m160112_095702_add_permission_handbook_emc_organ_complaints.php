<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_095702_add_permission_handbook_emc_organ_complaints extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160112_095702_add_permission_handbook_emc_organ_complaints cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_organ-complaints_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_organ-complaints_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_organ-complaints_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_organ-complaints_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_create',
            'child' => 'handbookemc_organ-complaints_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_update',
            'child' => 'handbookemc_organ-complaints_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_view',
            'child' => 'handbookemc_organ-complaints_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_delete',
            'child' => 'handbookemc_organ-complaints_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_view',
            'child' => 'handbookemc_organ-complaints_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_create',
            'child' => 'handbookemc_organ-complaints_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_update',
            'child' => 'handbookemc_organ-complaints_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_view',
            'child' => 'handbookemc_organ-complaints_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_delete',
            'child' => 'handbookemc_organ-complaints_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_organ-complaints_operation_view',
            'child' => 'handbookemc_organ-complaints_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_organ-complaints_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_organ-complaints_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_organ-complaints_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_organ-complaints_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_organ-complaints_operation_delete',
            'type' => '2'
        ]);
    }

}