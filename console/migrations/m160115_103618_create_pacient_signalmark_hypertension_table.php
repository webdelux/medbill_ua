<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160115_103618_create_pacient_signalmark_hypertension_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160115_103618_create_pacient_signalmark_hypertension_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_signalmark_hypertension}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer()->notNull(),

            'handbook_emc_list_degree_id' => $this->integer(),
            'handbook_emc_list_stage_id' => $this->integer(),
            'handbook_emc_list_risk_id' => $this->integer(),
            'handbook_emc_list_category_id' => $this->integer(),

            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_signalmark_hypertension}}', 'pacient_id');

        $this->createIndex('idx_handbook_emc_list_degree_id', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_degree_id');
        $this->createIndex('idx_handbook_emc_list_stage_id', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_stage_id');
        $this->createIndex('idx_handbook_emc_list_risk_id', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_risk_id');
        $this->createIndex('idx_handbook_emc_list_category_id', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_category_id');

        $this->createIndex('idx_description', '{{%pacient_signalmark_hypertension}}', 'description');
        $this->createIndex('idx_date_at', '{{%pacient_signalmark_hypertension}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_signalmark_hypertension}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_signalmark_hypertension}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_signalmark_hypertension}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_signalmark_hypertension}}', 'updated_at');

        $this->addForeignKey('fk_pacient_signalmark_hypertension_ib_1', '{{%pacient_signalmark_hypertension}}', 'pacient_id',
                '{{%pacient}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_hypertension_ib_2', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_degree_id',
                '{{%handbook_emc_list}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_hypertension_ib_3', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_stage_id',
                '{{%handbook_emc_list}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_hypertension_ib_4', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_risk_id',
                '{{%handbook_emc_list}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_hypertension_ib_5', '{{%pacient_signalmark_hypertension}}', 'handbook_emc_list_category_id',
                '{{%handbook_emc_list}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_hypertension_ib_1', '{{%pacient_signalmark_hypertension}}');
        $this->dropForeignKey('fk_pacient_signalmark_hypertension_ib_2', '{{%pacient_signalmark_hypertension}}');
        $this->dropForeignKey('fk_pacient_signalmark_hypertension_ib_3', '{{%pacient_signalmark_hypertension}}');
        $this->dropForeignKey('fk_pacient_signalmark_hypertension_ib_4', '{{%pacient_signalmark_hypertension}}');
        $this->dropForeignKey('fk_pacient_signalmark_hypertension_ib_5', '{{%pacient_signalmark_hypertension}}');

        $this->dropTable('{{%pacient_signalmark_hypertension}}');
    }

}