<?php

use yii\helpers\Html;
//use yii\db\Migration;
use console\migrations\Migration;
use backend\modules\pattern\models\ProtocolPattern_30 as ProtocolPattern;

/**
 * Handles the creation for table `protocol_pattern_30`.
 */
class m161006_092613_create_protocol_pattern_30_table extends Migration
{

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $model = new ProtocolPattern;

        $this->createTable('{{%protocol_pattern_30}}', [
            'id' => $this->primaryKey(),

            'protocol_id' => $this->integer()->notNull(),

            'field_1' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_1'))),
            'field_2' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_2'))),
            'field_3' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_3'))),
            'field_4' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_4'))),
            'field_5' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_5'))),
            'field_6' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_6'))),
            'field_7' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_7'))),
            'field_8' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_8'))),
            'field_9' => $this->string()->comment(Html::encode($model->getAttributeLabel('field_9'))),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_30}}', Yii::t('app', 'protocol_pattern_30'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_30}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_30}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_30}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_30}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_30}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_30_ib_1', '{{%protocol_pattern_30}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_pattern_30_ib_1', '{{%protocol_pattern_30}}');

        $this->dropCommentFromTable('{{%protocol_pattern_30}}');

        $this->dropTable('{{%protocol_pattern_30}}');
    }

}
