<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151214_085721_create_pacient_signalmark_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151214_085721_create_pacient_signalmark_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_signalmark}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer()->notNull(),

            'blood_group' => $this->smallInteger(),
            'blood_rhesus' => $this->smallInteger(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_signalmark}}', 'pacient_id', $unique = true);

        $this->createIndex('idx_blood_group', '{{%pacient_signalmark}}', 'blood_group');
        $this->createIndex('idx_blood_rhesus', '{{%pacient_signalmark}}', 'blood_rhesus');

        $this->createIndex('idx_user_id', '{{%pacient_signalmark}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_signalmark}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_signalmark}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_signalmark}}', 'updated_at');

        $this->addForeignKey('fk_pacient_signalmark_ib_1', '{{%pacient_signalmark}}', 'pacient_id',
                '{{%pacient}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_ib_1', '{{%pacient_signalmark}}');

        $this->dropTable('{{%pacient_signalmark}}');
    }

}