<?php

use yii\db\Migration;

class m161011_131221_add_roles_to_rbac extends Migration
{
    private $data = [
            ['operation_view_all'],
            ['diagnosis_operation_create'],
            ['diagnosis_operation_delete'],
            ['diagnosis_operation_update'],
            ['diagnosis_operation_view'],
            ['emc_operation_create'],
            ['emc_operation_delete'],
            ['emc_operation_update'],
            ['emc_operation_view'],
            ['handbook_operation_view'],
            ['handbookemc_operation_view'],
            ['medical-supplies-operation_operation_create'],
            ['medical-supplies-operation_operation_delete'],
            ['medical-supplies-operation_operation_update'],
            ['medical-supplies-operation_operation_view'],
            ['medical-supplies-operation_view'],
            ['pacient_create'],
            ['pacient_index'],
            ['pacient_operation_create'],
            ['pacient_operation_update'],
            ['pacient_operation_view'],
            ['pacient_view'],
            ['pacient-operation_operation_create'],
            ['pacient-operation_operation_delete'],
            ['pacient-operation_operation_update'],
            ['pacient-operation_operation_view'],
            ['pacient-service_operation_create'],
            ['pacient-service_operation_delete'],
            ['pacient-service_operation_update'],
            ['pacient-service_operation_view'],
            ['pattern_operation_view'],
            ['payment_operation_create'],
            ['payment_operation_delete'],
            ['payment_operation_update'],
            ['payment_operation_view'],
            ['planning_operation_create'],
            ['planning_operation_delete'],
            ['planning_operation_update'],
            ['planning_operation_view'],
            ['protocol_operation_create'],
            ['protocol_operation_delete'],
            ['protocol_operation_update'],
            ['protocol_operation_view'],
            ['registry_operation_create'],
            ['registry_operation_delete'],
            ['registry_operation_update'],
            ['registry_operation_view'],
            ['signalmark_operation_create'],
            ['signalmark_operation_delete'],
            ['signalmark_operation_update'],
            ['signalmark_operation_view'],
            ['stacionary_operation_create'],
            ['stacionary_operation_delete'],
            ['stacionary_operation_update'],
            ['stacionary_operation_view'],
        ];

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => backend\models\User::ROLE_MANAGER,
            'type' => '1'
        ]);


        foreach ($this->data as $value)
        {
            $this->insert('{{%auth_item_child}}', [
                'parent' => backend\models\User::ROLE_MANAGER,
                'child' => $value[0],
            ]);
        }
    }

    public function safeDown()
    {
        foreach ($this->data as $value)
        {
            $this->delete('{{%auth_item_child}}', [
                'parent' => backend\models\User::ROLE_MANAGER,
                'child' => $value[0],
            ]);
        }

        $this->delete('{{%auth_item}}', [
            'name' => backend\models\User::ROLE_MANAGER,
            'type' => '1'
        ]);
    }
}
