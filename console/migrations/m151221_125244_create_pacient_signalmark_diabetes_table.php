<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151221_125244_create_pacient_signalmark_diabetes_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151221_125244_create_pacient_signalmark_diabetes_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_signalmark_diabetes}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer()->notNull(),

            'handbook_emc_list_degree_id' => $this->integer(),
            'handbook_emc_list_stage_id' => $this->integer(),
            'handbook_emc_list_treatment_id' => $this->integer(),

            'term' => $this->string(),
            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%pacient_signalmark_diabetes}}', 'pacient_id');

        $this->createIndex('idx_handbook_emc_list_degree_id', '{{%pacient_signalmark_diabetes}}', 'handbook_emc_list_degree_id');
        $this->createIndex('idx_handbook_emc_list_stage_id', '{{%pacient_signalmark_diabetes}}', 'handbook_emc_list_stage_id');
        $this->createIndex('idx_handbook_emc_list_treatment_id', '{{%pacient_signalmark_diabetes}}', 'handbook_emc_list_treatment_id');

        $this->createIndex('idx_term', '{{%pacient_signalmark_diabetes}}', 'term');
        $this->createIndex('idx_description', '{{%pacient_signalmark_diabetes}}', 'description');
        $this->createIndex('idx_date_at', '{{%pacient_signalmark_diabetes}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_signalmark_diabetes}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_signalmark_diabetes}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_signalmark_diabetes}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_signalmark_diabetes}}', 'updated_at');

        $this->addForeignKey('fk_pacient_signalmark_diabetes_ib_1', '{{%pacient_signalmark_diabetes}}', 'pacient_id',
                '{{%pacient}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_diabetes_ib_2', '{{%pacient_signalmark_diabetes}}', 'handbook_emc_list_degree_id',
                '{{%handbook_emc_list}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_diabetes_ib_3', '{{%pacient_signalmark_diabetes}}', 'handbook_emc_list_stage_id',
                '{{%handbook_emc_list}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_pacient_signalmark_diabetes_ib_4', '{{%pacient_signalmark_diabetes}}', 'handbook_emc_list_treatment_id',
                '{{%handbook_emc_list}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_diabetes_ib_1', '{{%pacient_signalmark_diabetes}}');
        $this->dropForeignKey('fk_pacient_signalmark_diabetes_ib_2', '{{%pacient_signalmark_diabetes}}');
        $this->dropForeignKey('fk_pacient_signalmark_diabetes_ib_3', '{{%pacient_signalmark_diabetes}}');
        $this->dropForeignKey('fk_pacient_signalmark_diabetes_ib_4', '{{%pacient_signalmark_diabetes}}');

        $this->dropTable('{{%pacient_signalmark_diabetes}}');
    }

}