<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151112_093533_create_profile_speciality_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151112_093533_create_profile_speciality_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_speciality}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),

            'speciality_id' => $this->integer()->notNull(),

            'created_user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_user_id', '{{%profile_speciality}}', 'user_id');
        $this->addForeignKey('fk_profile_speciality_user', '{{%profile_speciality}}', 'user_id', '{{%user}}', 'id', 'RESTRICT');

        $this->createIndex('idx_speciality_id', '{{%profile_speciality}}', 'speciality_id');
        $this->addForeignKey('fk_profile_speciality', '{{%profile_speciality}}', 'speciality_id', '{{%speciality}}', 'id', 'RESTRICT');

        $this->createIndex('idx_user_speciality_unique', '{{%profile_speciality}}', ['user_id', 'speciality_id'], $unique = true);

        $this->createIndex('idx_created_user_id', '{{%profile_speciality}}', 'created_user_id');
        $this->createIndex('idx_updated_user_id', '{{%profile_speciality}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%profile_speciality}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%profile_speciality}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_speciality}}');
    }

}