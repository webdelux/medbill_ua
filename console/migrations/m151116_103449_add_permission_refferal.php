<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_103449_add_permission_refferal extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151116_103449_add_permission_refferal cannot be reverted.\n";

        return false;
    }
     *
     */

   public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_refferal_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_refferal_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_refferal_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_refferal_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_refferal_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_create',
            'child' => 'handbook_refferal_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_update',
            'child' => 'handbook_refferal_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_view',
            'child' => 'handbook_refferal_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_delete',
            'child' => 'handbook_refferal_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_view',
            'child' => 'handbook_refferal_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_create',
            'child' => 'handbook_refferal_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_update',
            'child' => 'handbook_refferal_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_view',
            'child' => 'handbook_refferal_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_delete',
            'child' => 'handbook_refferal_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_refferal_operation_view',
            'child' => 'handbook_refferal_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_refferal_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_refferal_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_refferal_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_refferal_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_refferal_operation_delete',
            'type' => '2'
        ]);
    }
}
