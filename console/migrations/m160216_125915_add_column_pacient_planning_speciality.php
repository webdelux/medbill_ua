<?php

use yii\db\Migration;

class m160216_125915_add_column_pacient_planning_speciality extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160216_125915_add_column_pacient_planning_speciality cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%pacient_planning_speciality}}', 'diagnosis_id', $this->integer());
        $this->createIndex('idx_diagnosis_id', '{{%pacient_planning_speciality}}', 'diagnosis_id');
        $this->addForeignKey('fk_pacient_planning_speciality_ib_3', '{{%pacient_planning_speciality}}', 'diagnosis_id', '{{%diagnosis}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_planning_speciality_ib_3', '{{%pacient_planning_speciality}}');
        $this->dropIndex('idx_diagnosis_id', '{{%pacient_planning_speciality}}');
        $this->dropColumn('{{%pacient_planning_speciality}}', 'diagnosis_id');
    }

}