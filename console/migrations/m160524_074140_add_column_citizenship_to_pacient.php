<?php

use yii\db\Migration;

/**
 * Handles adding column_citizenship to table `pacient`.
 */
class m160524_074140_add_column_citizenship_to_pacient extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%pacient}}', 'citizenship_id', $this->integer());
        $this->createIndex('idx_citizenship_id', '{{%pacient}}', 'citizenship_id');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {

        $this->dropIndex('idx_citizenship_id', '{{%pacient}}');
        $this->dropColumn('{{%pacient}}', 'citizenship_id');

    }
}
