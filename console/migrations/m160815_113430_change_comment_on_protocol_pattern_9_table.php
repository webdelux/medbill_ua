<?php

use yii\db\Migration;

class m160815_113430_change_comment_on_protocol_pattern_9_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160815_113430_change_comment_on_protocol_pattern_9_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->dropCommentFromTable('{{%protocol_pattern_9}}');
        $this->addCommentOnTable('{{%protocol_pattern_9}}', Yii::t('app', 'Diagnostic conclusion'));
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropCommentFromTable('{{%protocol_pattern_9}}');
        $this->addCommentOnTable('{{%protocol_pattern_9}}', Yii::t('app', 'protocol_pattern_9'));
    }

}