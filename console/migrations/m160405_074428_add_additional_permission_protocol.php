<?php

use yii\db\Migration;

class m160405_074428_add_additional_permission_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160405_074428_add_additional_permission_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'protocol_calendar',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_view',
            'child' => 'protocol_calendar'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'protocol_operation_view',
            'child' => 'protocol_calendar'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'protocol_calendar',
            'type' => '2'
        ]);
    }

}