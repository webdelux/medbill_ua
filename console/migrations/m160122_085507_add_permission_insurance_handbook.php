<?php

use yii\db\Schema;
use yii\db\Migration;

class m160122_085507_add_permission_insurance_handbook extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160122_085507_add_permission_insurance_handbook cannot be reverted.\n";

        return false;
    }
     *
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_insurance-handbook_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_insurance-handbook_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_insurance-handbook_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_insurance-handbook_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_create',
            'child' => 'handbook_insurance-handbook_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_update',
            'child' => 'handbook_insurance-handbook_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_view',
            'child' => 'handbook_insurance-handbook_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_delete',
            'child' => 'handbook_insurance-handbook_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_view',
            'child' => 'handbook_insurance-handbook_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_create',
            'child' => 'handbook_insurance-handbook_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_update',
            'child' => 'handbook_insurance-handbook_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_view',
            'child' => 'handbook_insurance-handbook_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_delete',
            'child' => 'handbook_insurance-handbook_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_insurance-handbook_operation_view',
            'child' => 'handbook_insurance-handbook_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_insurance-handbook_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_insurance-handbook_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_insurance-handbook_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_insurance-handbook_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_insurance-handbook_operation_delete',
            'type' => '2'
        ]);
    }

}
