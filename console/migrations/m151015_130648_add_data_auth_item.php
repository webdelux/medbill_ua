<?php

use yii\db\Schema;
use yii\db\Migration;

class m151015_130648_add_data_auth_item extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151015_130648_add_data_auth_item cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'administrator',
            'type' => '1'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'operation_create_all',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'operation_update_all',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'operation_view_all',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'operation_delete_all',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_create_all'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_update_all'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_view_all'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_delete_all'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_create_all'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_update_all'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_view_all'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'administrator',
            'child' => 'operation_delete_all'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'operation_create_all',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'operation_update_all',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'operation_view_all',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'operation_delete_all',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'administrator',
            'type' => '1'
        ]);
    }

}