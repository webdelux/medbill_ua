<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151105_132522_create_profile_schedule_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151105_132522_create_profile_schedule_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_schedule}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'date_from' => $this->date()->notNull()->defaultValue(0),
            'date_to' => $this->date()->notNull()->defaultValue(0),
            'created_user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_user_id', '{{%profile_schedule}}', 'user_id');
        $this->addForeignKey('fk_profile_schedule_user', '{{%profile_schedule}}', 'user_id', '{{%user}}', 'id', 'RESTRICT');

        $this->createIndex('idx_type_id', '{{%profile_schedule}}', 'type_id');

        $this->createIndex('idx_date_from', '{{%profile_schedule}}', 'date_from');
        $this->createIndex('idx_date_to', '{{%profile_schedule}}', 'date_to');

        $this->createIndex('idx_created_user_id', '{{%profile_schedule}}', 'created_user_id');
        $this->createIndex('idx_updated_user_id', '{{%profile_schedule}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%profile_schedule}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%profile_schedule}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_schedule}}');
    }

}