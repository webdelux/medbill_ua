<?php

use yii\db\Migration;

class m160715_121948_rename_protocol_pattern_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160715_121948_rename_protocol_pattern_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->renameTable('{{%protocol_pattern_5}}', '{{%protocol_pattern_6}}');
        $this->renameTable('{{%protocol_pattern_4}}', '{{%protocol_pattern_5}}');
        $this->renameTable('{{%protocol_pattern_3}}', '{{%protocol_pattern_4}}');
        $this->renameTable('{{%protocol_pattern_2}}', '{{%protocol_pattern_3}}');
        $this->renameTable('{{%protocol_pattern_1}}', '{{%protocol_pattern_2}}');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->renameTable('{{%protocol_pattern_2}}', '{{%protocol_pattern_1}}');
        $this->renameTable('{{%protocol_pattern_3}}', '{{%protocol_pattern_2}}');
        $this->renameTable('{{%protocol_pattern_4}}', '{{%protocol_pattern_3}}');
        $this->renameTable('{{%protocol_pattern_5}}', '{{%protocol_pattern_4}}');
        $this->renameTable('{{%protocol_pattern_6}}', '{{%protocol_pattern_5}}');
    }

}