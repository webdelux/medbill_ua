<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_090107_add_permission_handbook_emc_surgery extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151124_090107_add_permission_handbook_emc_surgery cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_surgery_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_surgery_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_surgery_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_surgery_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_create',
            'child' => 'handbookemc_surgery_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_update',
            'child' => 'handbookemc_surgery_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_view',
            'child' => 'handbookemc_surgery_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_delete',
            'child' => 'handbookemc_surgery_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_view',
            'child' => 'handbookemc_surgery_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_create',
            'child' => 'handbookemc_surgery_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_update',
            'child' => 'handbookemc_surgery_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_view',
            'child' => 'handbookemc_surgery_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_delete',
            'child' => 'handbookemc_surgery_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_surgery_operation_view',
            'child' => 'handbookemc_surgery_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_create',
            'child' => 'handbookemc_surgery_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_update',
            'child' => 'handbookemc_surgery_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_view',
            'child' => 'handbookemc_surgery_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbookemc_operation_delete',
            'child' => 'handbookemc_surgery_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbookemc_surgery_operation_delete',
            'type' => '2'
        ]);
    }

}