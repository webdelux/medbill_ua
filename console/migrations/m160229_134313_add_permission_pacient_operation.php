<?php

use yii\db\Migration;

class m160229_134313_add_permission_pacient_operation extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160229_134313_add_permission_pacient_operation cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'pacient-operation_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'pacient-operation_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'pacient-operation_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'pacient-operation_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'pacient-operation_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_create',
            'child' => 'pacient-operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_update',
            'child' => 'pacient-operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_view',
            'child' => 'pacient-operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_delete',
            'child' => 'pacient-operation_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_view',
            'child' => 'pacient-operation_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_create',
            'child' => 'pacient-operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_update',
            'child' => 'pacient-operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_view',
            'child' => 'pacient-operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_delete',
            'child' => 'pacient-operation_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pacient-operation_operation_view',
            'child' => 'pacient-operation_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'pacient-operation_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'pacient-operation_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'pacient-operation_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'pacient-operation_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'pacient-operation_operation_delete',
            'type' => '2'
        ]);
    }

}