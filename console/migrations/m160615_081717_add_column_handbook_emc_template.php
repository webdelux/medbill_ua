<?php

use yii\db\Migration;

class m160615_081717_add_column_handbook_emc_template extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160615_081717_add_column_handbook_emc_template cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%handbook_emc_template}}', 'pattern', $this->integer()->after('substitutional'));
        $this->createIndex('idx_pattern', '{{%handbook_emc_template}}', 'pattern', $unique = false);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_pattern', '{{%handbook_emc_template}}');
        $this->dropColumn('{{%handbook_emc_template}}', 'pattern');
    }

}