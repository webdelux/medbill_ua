<?php

use yii\db\Migration;

class m160218_114842_add_archive_to_stacionary extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%stacionary}}', 'archive', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%stacionary}}', 'parent', $this->integer()->notNull()->defaultValue(0));

        $this->createIndex('idx_archive', '{{%stacionary}}', 'archive');
        $this->createIndex('idx_parent', '{{%stacionary}}', 'parent');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_archive', '{{%stacionary}}');
        $this->dropIndex('idx_parent', '{{%stacionary}}');

        $this->dropColumn('{{%stacionary}}', 'archive');
        $this->dropColumn('{{%stacionary}}', 'parent');
    }
}
