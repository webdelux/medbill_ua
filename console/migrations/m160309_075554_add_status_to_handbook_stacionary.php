<?php

use yii\db\Migration;

class m160309_075554_add_status_to_handbook_stacionary extends Migration
{
   public function safeUp()
    {
        $this->addColumn('{{%handbook_stacionary}}', 'status', $this->integer()->notNull()->defaultValue(1));


        $this->createIndex('idx_status', '{{%handbook_stacionary}}', 'status');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_status', '{{%handbook_stacionary}}');


        $this->dropColumn('{{%handbook_stacionary}}', 'status');

    }
}
