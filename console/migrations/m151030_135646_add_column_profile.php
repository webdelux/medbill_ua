<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_135646_add_column_profile extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151030_135646_add_column_profile cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'payment', $this->string());
        $this->createIndex('idx_payment', '{{%profile}}', 'payment');

        $this->addColumn('{{%profile}}', 'note', $this->string());
        $this->createIndex('idx_note', '{{%profile}}', 'note');

        $this->addColumn('{{%profile}}', 'working_days', $this->string()->notNull()->defaultValue("mon,tue,wed,thu,fri,sat,sun"));
        $this->createIndex('idx_working_days', '{{%profile}}', 'working_days');

        // Monday
        $this->addColumn('{{%profile}}', 'working_time_mon_from', $this->time()->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile}}', 'working_time_mon_to', $this->time()->notNull()->defaultValue('22:00:00'));
        // Tuesday
        $this->addColumn('{{%profile}}', 'working_time_tue_from', $this->time()->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile}}', 'working_time_tue_to', $this->time()->notNull()->defaultValue('22:00:00'));
        // Wednesday
        $this->addColumn('{{%profile}}', 'working_time_wed_from', $this->time()->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile}}', 'working_time_wed_to', $this->time()->notNull()->defaultValue('22:00:00'));
        // Thursday
        $this->addColumn('{{%profile}}', 'working_time_thu_from', $this->time()->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile}}', 'working_time_thu_to', $this->time()->notNull()->defaultValue('22:00:00'));
        // Friday
        $this->addColumn('{{%profile}}', 'working_time_fri_from', $this->time()->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile}}', 'working_time_fri_to', $this->time()->notNull()->defaultValue('22:00:00'));
        // Saturday
        $this->addColumn('{{%profile}}', 'working_time_sat_from', $this->time()->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile}}', 'working_time_sat_to', $this->time()->notNull()->defaultValue('22:00:00'));
        // Sunday
        $this->addColumn('{{%profile}}', 'working_time_sun_from', $this->time()->notNull()->defaultValue('08:00:00'));
        $this->addColumn('{{%profile}}', 'working_time_sun_to', $this->time()->notNull()->defaultValue('22:00:00'));

        $this->addColumn('{{%profile}}', 'city_id', $this->integer());
        $this->createIndex('idx_city_id', '{{%profile}}', 'city_id');
        $this->addForeignKey('fk_profile_city', '{{%profile}}', 'city_id', '{{%geo_city}}', 'id', 'RESTRICT');

        $this->addColumn('{{%profile}}', 'status_id', $this->integer());
        $this->createIndex('idx_status_id', '{{%profile}}', 'status_id');
        $this->addForeignKey('fk_profile_status', '{{%profile}}', 'status_id', '{{%status}}', 'id', 'RESTRICT');

        $this->addColumn('{{%profile}}', 'mark_id', $this->integer()->defaultValue(1));
        $this->createIndex('idx_mark_id', '{{%profile}}', 'mark_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_mark_id', '{{%profile}}');
        $this->dropColumn('{{%profile}}', 'mark_id');

        $this->dropForeignKey('fk_profile_status', '{{%profile}}');
        $this->dropIndex('idx_status_id', '{{%profile}}');
        $this->dropColumn('{{%profile}}', 'status_id');

        $this->dropForeignKey('fk_profile_city', '{{%profile}}');
        $this->dropIndex('idx_city_id', '{{%profile}}');
        $this->dropColumn('{{%profile}}', 'city_id');

        // Monday
        $this->dropColumn('{{%profile}}', 'working_time_mon_from');
        $this->dropColumn('{{%profile}}', 'working_time_mon_to');
        // Tuesday
        $this->dropColumn('{{%profile}}', 'working_time_tue_from');
        $this->dropColumn('{{%profile}}', 'working_time_tue_to');
        // Wednesday
        $this->dropColumn('{{%profile}}', 'working_time_wed_from');
        $this->dropColumn('{{%profile}}', 'working_time_wed_to');
        // Thursday
        $this->dropColumn('{{%profile}}', 'working_time_thu_from');
        $this->dropColumn('{{%profile}}', 'working_time_thu_to');
        // Friday
        $this->dropColumn('{{%profile}}', 'working_time_fri_from');
        $this->dropColumn('{{%profile}}', 'working_time_fri_to');
        // Saturday
        $this->dropColumn('{{%profile}}', 'working_time_sat_from');
        $this->dropColumn('{{%profile}}', 'working_time_sat_to');
        // Sunday
        $this->dropColumn('{{%profile}}', 'working_time_sun_from');
        $this->dropColumn('{{%profile}}', 'working_time_sun_to');

        $this->dropIndex('idx_working_days', '{{%profile}}');
        $this->dropColumn('{{%profile}}', 'working_days');

        $this->dropIndex('idx_note', '{{%profile}}');
        $this->dropColumn('{{%profile}}', 'note');

        $this->dropIndex('idx_payment', '{{%profile}}');
        $this->dropColumn('{{%profile}}', 'payment');
    }

}