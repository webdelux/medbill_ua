<?php

use yii\db\Migration;

class m160610_111258_add_permission_preoperative_epicrisis extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160610_111258_add_permission_preoperative_epicrisis cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_print',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_view',
            'child' => 'emc_preoperative-epicrisis_print'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_preoperative-epicrisis_operation_view',
            'child' => 'emc_preoperative-epicrisis_print'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_preoperative-epicrisis_print',
            'type' => '2'
        ]);
    }

}