<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160426_093026_create_diary_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%diary}}', [
            'id' => $this->primaryKey(),
            'pacient_id' => $this->integer()->notNull(),
            'diagnosis_id' => $this->integer()->notNull()->defaultValue(0),

            'diary_number' => $this->integer(),
            'diary_date' => $this->date()->notNull()->defaultValue(0),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%diary}}', 'pacient_id');
        $this->createIndex('idx_diagnosis_id', '{{%diary}}', 'diagnosis_id');

        $this->createIndex('idx_diary_number', '{{%diary}}', 'diary_number');
        $this->createIndex('idx_diary_date', '{{%diary}}', 'diary_date');

        $this->createIndex('idx_description', '{{%diary}}', 'description');
        $this->createIndex('idx_user_id', '{{%diary}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%diary}}', 'updated_user_id');
        $this->createIndex('idx_created_at', '{{%diary}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%diary}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%diary}}');
    }
}
