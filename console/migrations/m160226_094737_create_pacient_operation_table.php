<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160226_094737_create_pacient_operation_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160226_094737_create_pacient_operation_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%pacient_operation}}', [
            'id' => $this->primaryKey(),
            'diagnosis_id' => $this->integer()->notNull(),

            'handbook_emc_surgery_id' => $this->integer(),
            'handbook_emc_surgery_note' => $this->string(),

            'handbook_emc_icd_id' => $this->integer(),
            'handbook_emc_icd_note' => $this->string(),

            'start_operation_date' => $this->date()->notNull()->defaultValue(0),
            'start_operation_time' => $this->time()->notNull()->defaultValue(0),

            'duration_operation_hour' => $this->integer()->notNull()->defaultValue(0),
            'duration_operation_minute' => $this->integer()->notNull()->defaultValue(0),

            'surgeon_user_id' => $this->integer(),

            'department_id' => $this->integer(),
            'result_id' => $this->integer(),

            'anesthetist_user_id' => $this->integer(),
            'anesthesia_id' => $this->integer(),

            'handbook_emc_complication_id' => $this->integer(),
            'handbook_emc_complication_note' => $this->string(),

            'type_id' => $this->integer(),

            'date_at' => $this->date()->notNull()->defaultValue(0),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_diagnosis_id', '{{%pacient_operation}}', 'diagnosis_id');

        $this->createIndex('idx_handbook_emc_surgery_id', '{{%pacient_operation}}', 'handbook_emc_surgery_id');
        $this->createIndex('idx_handbook_emc_surgery_note', '{{%pacient_operation}}', 'handbook_emc_surgery_note');

        $this->createIndex('idx_handbook_emc_icd_id', '{{%pacient_operation}}', 'handbook_emc_icd_id');
        $this->createIndex('idx_handbook_emc_icd_note', '{{%pacient_operation}}', 'handbook_emc_icd_note');

        $this->createIndex('idx_start_operation_date', '{{%pacient_operation}}', 'start_operation_date');
        $this->createIndex('idx_start_operation_time', '{{%pacient_operation}}', 'start_operation_time');

        $this->createIndex('idx_duration_operation_hour', '{{%pacient_operation}}', 'duration_operation_hour');
        $this->createIndex('idx_duration_operation_minute', '{{%pacient_operation}}', 'duration_operation_minute');

        $this->createIndex('idx_surgeon_user_id', '{{%pacient_operation}}', 'surgeon_user_id');

        $this->createIndex('idx_department_id', '{{%pacient_operation}}', 'department_id');
        $this->createIndex('idx_result_id', '{{%pacient_operation}}', 'result_id');

        $this->createIndex('idx_anesthetist_user_id', '{{%pacient_operation}}', 'anesthetist_user_id');
        $this->createIndex('idx_anesthesia_id', '{{%pacient_operation}}', 'anesthesia_id');

        $this->createIndex('idx_handbook_emc_complication_id', '{{%pacient_operation}}', 'handbook_emc_complication_id');
        $this->createIndex('idx_handbook_emc_complication_note', '{{%pacient_operation}}', 'handbook_emc_complication_note');

        $this->createIndex('idx_type_id', '{{%pacient_operation}}', 'type_id');

        $this->createIndex('idx_date_at', '{{%pacient_operation}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%pacient_operation}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%pacient_operation}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%pacient_operation}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%pacient_operation}}', 'updated_at');

        $this->addForeignKey('fk_pacient_operation_ib_1', '{{%pacient_operation}}', 'diagnosis_id', '{{%diagnosis}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_2', '{{%pacient_operation}}', 'handbook_emc_surgery_id', '{{%handbook_emc_surgery}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_3', '{{%pacient_operation}}', 'handbook_emc_icd_id', '{{%handbook_emc_icd}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_4', '{{%pacient_operation}}', 'surgeon_user_id', '{{%user}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_5', '{{%pacient_operation}}', 'department_id', '{{%department}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_6', '{{%pacient_operation}}', 'anesthetist_user_id', '{{%user}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_7', '{{%pacient_operation}}', 'handbook_emc_complication_id', '{{%handbook_emc_complication}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_8', '{{%pacient_operation}}', 'user_id', '{{%user}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_operation_ib_9', '{{%pacient_operation}}', 'updated_user_id', '{{%user}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_operation_ib_1', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_2', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_3', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_4', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_5', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_6', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_7', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_8', '{{%pacient_operation}}');
        $this->dropForeignKey('fk_pacient_operation_ib_9', '{{%pacient_operation}}');

        $this->dropTable('{{%pacient_operation}}');
    }

}