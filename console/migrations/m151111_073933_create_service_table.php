<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151111_073933_create_service_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151111_073933_service_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),

            'category_id' => $this->integer(),

            'name' => $this->string()->notNull(),
            'price' => $this->decimal(10,2),
            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

            'status_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('idx_category_id', '{{%service}}', 'category_id');

        $this->createIndex('idx_name', '{{%service}}', 'name');
        $this->createIndex('idx_price', '{{%service}}', 'price');
        $this->createIndex('idx_description', '{{%service}}', 'description');

        $this->createIndex('idx_user_id', '{{%service}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%service}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%service}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%service}}', 'updated_at');

        $this->createIndex('idx_status_id', '{{%service}}', 'status_id');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%service}}');
    }
}
