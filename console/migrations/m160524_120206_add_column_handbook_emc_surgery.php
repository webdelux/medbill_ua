<?php

use yii\db\Migration;

class m160524_120206_add_column_handbook_emc_surgery extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160524_120206_add_column_handbook_emc_surgery cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%handbook_emc_surgery}}', 'handbook_emc_surgery_category_id', $this->integer()->after('id'));
        $this->createIndex('idx_handbook_emc_surgery_category_id', '{{%handbook_emc_surgery}}', 'handbook_emc_surgery_category_id');
        $this->addForeignKey('fk_handbook_emc_surgery_ib_1', '{{%handbook_emc_surgery}}', 'handbook_emc_surgery_category_id', '{{%handbook_emc_surgery_category}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_handbook_emc_surgery_ib_1', '{{%handbook_emc_surgery}}');
        $this->dropIndex('idx_handbook_emc_surgery_category_id', '{{%handbook_emc_surgery}}');
        $this->dropColumn('{{%handbook_emc_surgery}}', 'handbook_emc_surgery_category_id');
    }

}