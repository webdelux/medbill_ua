<?php

use yii\db\Schema;
use yii\db\Migration;

class m151215_072226_add_column_stacionary extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151215_072226_add_column_stacionary cannot be reverted.\n";

        return false;
    }
     *
     */


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%stacionary}}', 'date_stacionary', $this->timestamp()->notNull()->defaultValue(0));
        $this->createIndex('idx_date_stacionary', '{{%stacionary}}', 'date_stacionary');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_date_stacionary', '{{%stacionary}}');
        $this->dropColumn('{{%stacionary}}', 'date_stacionary');
    }

}



