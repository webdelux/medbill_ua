<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151208_081500_create_handbook_emc_category_department_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151208_081500_create_handbook_emc_category_department_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%handbook_emc_category_department}}', [
            'id' => $this->primaryKey(),

            'handbook_emc_category_id' => $this->integer()->notNull(),

            'department_id' => $this->integer()->notNull(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_handbook_emc_category_id', '{{%handbook_emc_category_department}}', 'handbook_emc_category_id');

        $this->createIndex('idx_department_id', '{{%handbook_emc_category_department}}', 'department_id');

        $this->createIndex('idx_handbook_emc_category_department_unique', '{{%handbook_emc_category_department}}', ['handbook_emc_category_id', 'department_id'], $unique = true);

        $this->createIndex('idx_user_id', '{{%handbook_emc_category_department}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%handbook_emc_category_department}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%handbook_emc_category_department}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%handbook_emc_category_department}}', 'updated_at');

        $this->addForeignKey('fk_handbook_emc_category_department_ib_1', '{{%handbook_emc_category_department}}', 'handbook_emc_category_id',
                '{{%handbook_emc_category}}', 'id', 'RESTRICT');

        $this->addForeignKey('fk_handbook_emc_category_department_ib_2', '{{%handbook_emc_category_department}}', 'department_id',
                '{{%department}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_handbook_emc_category_department_ib_1', '{{%handbook_emc_category_department}}');
        $this->dropForeignKey('fk_handbook_emc_category_department_ib_2', '{{%handbook_emc_category_department}}');

        $this->dropTable('{{%handbook_emc_category_department}}');
    }

}