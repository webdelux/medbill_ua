<?php

use yii\db\Schema;
use yii\db\Migration;

class m151006_140925_phone_row extends Migration
{
   public function safeUp()
    {
        $this->addColumn('{{%user}}','phone','VARCHAR(20) NOT NULL AFTER `email`');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}','phone');
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
