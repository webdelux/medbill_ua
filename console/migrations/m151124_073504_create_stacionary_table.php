<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151124_073504_create_stacionary_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151124_073504_create_stacionary_table cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->createTable('{{%stacionary}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer(),
            'department_id' => $this->integer(),
            'room_id' => $this->integer(),
            'place_id' => $this->integer(),

            'doctor_id' => $this->integer(),

            'type' => $this->integer()->notNull(),
            'type_note' => $this->integer()->notNull(),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%stacionary}}', 'pacient_id');
        $this->createIndex('idx_department_id', '{{%stacionary}}', 'department_id');
        $this->createIndex('idx_room_id', '{{%stacionary}}', 'room_id');
        $this->createIndex('idx_place_id', '{{%stacionary}}', 'place_id');

        $this->createIndex('idx_doctor_id', '{{%stacionary}}', 'doctor_id');

        $this->createIndex('idx_type', '{{%stacionary}}', 'type');
        $this->createIndex('idx_type_note', '{{%stacionary}}', 'type_note');
        $this->createIndex('idx_description', '{{%stacionary}}', 'description');

        $this->createIndex('idx_user_id', '{{%stacionary}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%stacionary}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%stacionary}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%stacionary}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%stacionary}}');
    }
}
