<?php

//use yii\db\Migration;
use console\migrations\Migration;

/**
 * Handles the creation for table `protocol_pattern_10`.
 */
class m160811_092220_create_protocol_pattern_10_table extends Migration
{

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%protocol_pattern_10}}', [
            'id' => $this->primaryKey(),
            'protocol_id' => $this->integer()->notNull(),
            'field_1' => $this->string()->comment(Yii::t('app', 'Quantity')),
            'field_2' => $this->string()->comment(Yii::t('app', 'Color')),
            'field_3' => $this->string()->comment(Yii::t('app', 'Transparency')),
            'field_4' => $this->string()->comment(Yii::t('app', 'Specific weight')),
            'field_5' => $this->string()->comment(Yii::t('app', 'Glucose')),
            'field_6' => $this->string()->comment(Yii::t('app', 'Ketone bodies')),
            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_10}}', Yii::t('app', 'protocol_pattern_10'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_10}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_10}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_10}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_10}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_10}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_10_ib_1', '{{%protocol_pattern_10}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_protocol_pattern_10_ib_1', '{{%protocol_pattern_10}}');

        $this->dropCommentFromTable('{{%protocol_pattern_10}}');

        $this->dropTable('{{%protocol_pattern_10}}');
    }
}
