<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_133909_add_column_pacient_signalmark extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160113_133909_add_column_pacient_signalmark cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%pacient_signalmark}}', 'complaints_note', $this->text());
        //$this->createIndex('idx_complaints_note', '{{%pacient_signalmark}}', 'complaints_note');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pacient_signalmark}}', 'complaints_note');
    }

}