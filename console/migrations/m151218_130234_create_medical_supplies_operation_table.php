<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m151218_130234_create_medical_supplies_operation_table extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151218_130234_create_medical_supplies_operation_table cannot be reverted.\n";

        return false;
    }
     *
     */

     public function safeUp()
    {
        $this->createTable('{{%medical_supplies_operation}}', [
            'id' => $this->primaryKey(),

            'pacient_id' => $this->integer(),

            'department_id' => $this->integer(),
            'medical_supplies_id' => $this->integer(),

            'quantity' => $this->integer(),      // кількість (+ надходження,  - витрата)

            'docum_date' => $this->timestamp()->notNull()->defaultValue(0),
            'docum_num' => $this->string(),
            'docum' => $this->string(),

            'description' => $this->string(),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_pacient_id', '{{%medical_supplies_operation}}', 'pacient_id');
        $this->createIndex('idx_department_id', '{{%medical_supplies_operation}}', 'department_id');

        $this->createIndex('idx_medical_supplies_id', '{{%medical_supplies_operation}}', 'medical_supplies_id');
        $this->createIndex('idx_quantity', '{{%medical_supplies_operation}}', 'quantity');

        $this->createIndex('idx_docum_date', '{{%medical_supplies_operation}}', 'docum_date');
        $this->createIndex('idx_docum_num', '{{%medical_supplies_operation}}', 'docum_num');
        $this->createIndex('idx_docum', '{{%medical_supplies_operation}}', 'docum');

        $this->createIndex('idx_description', '{{%medical_supplies_operation}}', 'description');

        $this->createIndex('idx_user_id', '{{%medical_supplies_operation}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%medical_supplies_operation}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%medical_supplies_operation}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%medical_supplies_operation}}', 'updated_at');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropTable('{{%medical_supplies_operation}}');
    }
}
