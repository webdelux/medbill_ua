<?php

use yii\db\Migration;

class m160513_084332_add_permission_departments_category extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_department-category_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_department-category_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_department-category_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_department-category_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_department-category_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_create',
            'child' => 'handbook_department-category_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_update',
            'child' => 'handbook_department-category_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_view',
            'child' => 'handbook_department-category_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_delete',
            'child' => 'handbook_department-category_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_view',
            'child' => 'handbook_department-category_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_create',
            'child' => 'handbook_department-category_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_update',
            'child' => 'handbook_department-category_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_view',
            'child' => 'handbook_department-category_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_delete',
            'child' => 'handbook_department-category_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_department-category_operation_view',
            'child' => 'handbook_department-category_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_department-category_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_department-category_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_department-category_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_department-category_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_department-category_operation_delete',
            'type' => '2'
        ]);
    }
}
