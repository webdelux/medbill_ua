<?php

//use yii\db\Migration;
use console\migrations\Migration;

class m160815_110932_delete_protocol_pattern_12_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160815_110932_delete_protocol_pattern_12_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_protocol_pattern_12_ib_1', '{{%protocol_pattern_12}}');

        $this->dropCommentFromTable('{{%protocol_pattern_12}}');

        $this->dropTable('{{%protocol_pattern_12}}');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->createTable('{{%protocol_pattern_12}}', [
            'id' => $this->primaryKey(),

            'protocol_id' => $this->integer()->notNull(),

            'field_1' => $this->text()->comment(Yii::t('app', 'Diagnostic conclusion')),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),

        ], $this->tableOptions);

        $this->addCommentOnTable('{{%protocol_pattern_12}}', Yii::t('app', 'protocol_pattern_12'));

        $this->createIndex('idx_protocol_id', '{{%protocol_pattern_12}}', 'protocol_id', $unique = true);

        $this->createIndex('idx_user_id', '{{%protocol_pattern_12}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%protocol_pattern_12}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%protocol_pattern_12}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%protocol_pattern_12}}', 'updated_at');

        $this->addForeignKey('fk_protocol_pattern_12_ib_1', '{{%protocol_pattern_12}}', 'protocol_id', '{{%protocol}}', 'id', 'RESTRICT');
    }

}