<?php

use yii\db\Migration;

class m160426_111755_add_permission_attachment extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160426_111755_add_permission_attachment cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'emc_attachment_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_attachment_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_attachment_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_attachment_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_attachment_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_create',
            'child' => 'emc_attachment_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_update',
            'child' => 'emc_attachment_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_view',
            'child' => 'emc_attachment_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_delete',
            'child' => 'emc_attachment_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_view',
            'child' => 'emc_attachment_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_create',
            'child' => 'emc_attachment_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_update',
            'child' => 'emc_attachment_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_view',
            'child' => 'emc_attachment_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_delete',
            'child' => 'emc_attachment_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_attachment_operation_view',
            'child' => 'emc_attachment_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_create',
            'child' => 'emc_attachment_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_update',
            'child' => 'emc_attachment_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_view',
            'child' => 'emc_attachment_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'emc_operation_delete',
            'child' => 'emc_attachment_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'emc_attachment_operation_delete',
            'type' => '2'
        ]);
    }
}
