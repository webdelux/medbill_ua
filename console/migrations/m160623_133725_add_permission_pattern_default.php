<?php

use yii\db\Migration;

class m160623_133725_add_permission_pattern_default extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160623_133725_add_permission_pattern_default cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'pattern_default_operation_view',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'pattern_default_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pattern_operation_view',
            'child' => 'pattern_default_operation_view'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'pattern_default_operation_view',
            'child' => 'pattern_default_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pattern_default_operation_view',
            'child' => 'pattern_default_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'pattern_operation_view',
            'child' => 'pattern_default_operation_view'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pattern_default_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'pattern_default_operation_view',
            'type' => '2'
        ]);
    }

}