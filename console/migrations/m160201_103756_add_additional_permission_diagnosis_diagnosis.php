<?php

use yii\db\Schema;
use yii\db\Migration;

class m160201_103756_add_additional_permission_diagnosis_diagnosis extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160201_103756_add_additional_permission_diagnosis_diagnosis cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis_template',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis_operation_view',
            'child' => 'diagnosis_diagnosis_template'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'diagnosis_diagnosis_operation_view',
            'child' => 'diagnosis_diagnosis_template'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'diagnosis_diagnosis_template',
            'type' => '2'
        ]);
    }

}