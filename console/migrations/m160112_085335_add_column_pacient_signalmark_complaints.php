<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_085335_add_column_pacient_signalmark_complaints extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160112_085335_add_column_pacient_signalmark_complaints cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->addColumn('{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_id', $this->integer());
        $this->addColumn('{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_complaints_id', $this->integer());

        $this->createIndex('idx_handbook_emc_organ_id', '{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_id');
        $this->createIndex('idx_handbook_emc_organ_complaints_id', '{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_complaints_id');

        $this->addForeignKey('fk_pacient_signalmark_complaints_ib_2', '{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_id', '{{%handbook_emc_organ}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_pacient_signalmark_complaints_ib_3', '{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_complaints_id', '{{%handbook_emc_organ_complaints}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pacient_signalmark_complaints_ib_2', '{{%pacient_signalmark_complaints}}');
        $this->dropForeignKey('fk_pacient_signalmark_complaints_ib_3', '{{%pacient_signalmark_complaints}}');

        $this->dropColumn('{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_id');
        $this->dropColumn('{{%pacient_signalmark_complaints}}', 'handbook_emc_organ_complaints_id');
    }

}