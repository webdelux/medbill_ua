<?php

use yii\db\Schema;
//use yii\db\Migration;
use console\migrations\Migration;

class m160127_144536_create_diagnosis_additional_table extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160127_144536_create_diagnosis_additional_table cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->createTable('{{%diagnosis_additional}}', [
            'id' => $this->primaryKey(),

            'diagnosis_id' => $this->integer()->notNull(),
            'handbook_emc_icd_id' => $this->integer()->notNull(),

            'type' => $this->integer()->notNull(),

            'description' => $this->string(),
            'date_at' => $this->date()->notNull()->defaultValue(0),

            'user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer(),

            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createIndex('idx_diagnosis_id', '{{%diagnosis_additional}}', 'diagnosis_id');

        $this->createIndex('idx_handbook_emc_icd_id', '{{%diagnosis_additional}}', 'handbook_emc_icd_id');

        $this->createIndex('idx_type', '{{%diagnosis_additional}}', 'type');

        $this->createIndex('idx_description', '{{%diagnosis_additional}}', 'description');
        $this->createIndex('idx_date_at', '{{%diagnosis_additional}}', 'date_at');

        $this->createIndex('idx_user_id', '{{%diagnosis_additional}}', 'user_id');
        $this->createIndex('idx_updated_user_id', '{{%diagnosis_additional}}', 'updated_user_id');

        $this->createIndex('idx_created_at', '{{%diagnosis_additional}}', 'created_at');
        $this->createIndex('idx_updated_at', '{{%diagnosis_additional}}', 'updated_at');

        $this->addForeignKey('fk_diagnosis_additional_ib_1', '{{%diagnosis_additional}}', 'diagnosis_id', '{{%diagnosis}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk_diagnosis_additional_ib_2', '{{%diagnosis_additional}}', 'handbook_emc_icd_id', '{{%handbook_emc_icd}}', 'id', 'RESTRICT');
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_diagnosis_additional_ib_1', '{{%diagnosis_additional}}');
        $this->dropForeignKey('fk_diagnosis_additional_ib_2', '{{%diagnosis_additional}}');

        $this->dropTable('{{%diagnosis_additional}}');
    }

}