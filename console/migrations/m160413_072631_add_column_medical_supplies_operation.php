<?php

use yii\db\Migration;

class m160413_072631_add_column_medical_supplies_operation extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160413_072631_add_column_medical_supplies_operation cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->addColumn('{{%medical_supplies_operation}}', 'diagnosis_id', $this->integer());     // дата вибуття з палати/ліжка
        $this->createIndex('idx_diagnosis_id', '{{%medical_supplies_operation}}', 'diagnosis_id');

        $this->alterColumn('{{%medical_supplies_operation}}', 'docum_date', $this->date()->notNull()->defaultValue(0));

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_diagnosis_id', '{{%medical_supplies_operation}}');
        $this->dropColumn('{{%medical_supplies_operation}}', 'diagnosis_id');


    }
}
