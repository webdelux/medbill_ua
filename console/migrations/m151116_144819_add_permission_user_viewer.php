<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_144819_add_permission_user_viewer extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151116_144819_add_permission_user_viewer cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_create-schedule',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'user-viewer_delete-schedule',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'user-viewer_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'user-viewer_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'user-viewer_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'user-viewer_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_create',
            'child' => 'user-viewer_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_update',
            'child' => 'user-viewer_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_view',
            'child' => 'user-viewer_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_delete',
            'child' => 'user-viewer_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_view',
            'child' => 'user-viewer_index'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_create',
            'child' => 'user-viewer_create-schedule'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_delete',
            'child' => 'user-viewer_delete-schedule'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_create',
            'child' => 'user-viewer_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_update',
            'child' => 'user-viewer_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_view',
            'child' => 'user-viewer_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_delete',
            'child' => 'user-viewer_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_view',
            'child' => 'user-viewer_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_create',
            'child' => 'user-viewer_create-schedule'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'user-viewer_operation_delete',
            'child' => 'user-viewer_delete-schedule'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'user-viewer_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'user-viewer_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'user-viewer_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'user-viewer_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_create-schedule',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_delete-schedule',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'user-viewer_operation_delete',
            'type' => '2'
        ]);
    }

}