<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_111911_add_category_data extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151105_111911_add_category_data cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        /*  Увага! Корневі категорії мають свій фіксований ID, інакше буде відображатися інша категорія! */
        $data = [
            [1, NULL, 'Медикаменти', 'Корнева категорія для розділу "Медикаменти"'],

        ];

        foreach ($data as $key => $value)
        {
            $this->insert('{{%category}}', [
                'id' => $value[0],
                'parent_id' => $value[1],
                'name' => $value[2],
                'description' => $value[3],
            ]);
        }
    }

    public function safeDown()
    {
        $this->truncateTable('{{%category}}');
    }
}
