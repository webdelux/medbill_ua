<?php

use yii\db\Migration;

class m160818_141211_update_data_handbook_emc_protocol extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160818_141211_update_data_handbook_emc_protocol cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4250]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4251]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4252]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4253]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4254]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4255]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4256]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4257]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4258]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4259]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4260]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4261]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4262]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4263]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 16], 'id = :id', [':id' => 4264]);

        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4265]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4266]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4267]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4268]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4269]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4270]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4271]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4272]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4273]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 17], 'id = :id', [':id' => 4274]);

        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 18], 'id = :id', [':id' => 4275]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 18], 'id = :id', [':id' => 4276]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 18], 'id = :id', [':id' => 4277]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 18], 'id = :id', [':id' => 4278]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 18], 'id = :id', [':id' => 4279]);

        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 19], 'id = :id', [':id' => 4280]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 19], 'id = :id', [':id' => 4281]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 19], 'id = :id', [':id' => 4282]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4250]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4251]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4252]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4253]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4254]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4255]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4256]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4257]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4258]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4259]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4260]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4261]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4262]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4263]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4264]);

        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4265]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4266]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4267]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4268]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4269]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4270]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4271]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4272]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4273]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4274]);

        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4275]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4276]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4277]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4278]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4279]);

        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4280]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4281]);
        $this->update('{{%handbook_emc_protocol}}', ['handbook_emc_template_id' => 1], 'id = :id', [':id' => 4282]);
    }

}