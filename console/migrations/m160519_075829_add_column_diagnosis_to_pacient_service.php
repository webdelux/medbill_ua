<?php

use yii\db\Migration;

/**
 * Handles adding column_diagnosis to table `pacient_service`.
 */
class m160519_075829_add_column_diagnosis_to_pacient_service extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%pacient_service}}', 'diagnosis_id', $this->integer());
        $this->createIndex('idx_diagnosis_id', '{{%pacient_service}}', 'diagnosis_id');

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropIndex('idx_diagnosis_id', '{{%pacient_service}}');
        $this->dropColumn('{{%pacient_service}}', 'diagnosis_id');


    }
}
