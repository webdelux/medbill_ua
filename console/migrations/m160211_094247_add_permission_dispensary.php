<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_094247_add_permission_dispensary extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160211_094247_add_permission_dispensary cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_create',
            'child' => 'signalmark_dispensary_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_update',
            'child' => 'signalmark_dispensary_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_view',
            'child' => 'signalmark_dispensary_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_delete',
            'child' => 'signalmark_dispensary_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_create',
            'child' => 'signalmark_dispensary_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_update',
            'child' => 'signalmark_dispensary_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_view',
            'child' => 'signalmark_dispensary_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_delete',
            'child' => 'signalmark_dispensary_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_view',
            'child' => 'signalmark_dispensary_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_create',
            'child' => 'signalmark_dispensary_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_update',
            'child' => 'signalmark_dispensary_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_view',
            'child' => 'signalmark_dispensary_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_delete',
            'child' => 'signalmark_dispensary_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_dispensary_operation_view',
            'child' => 'signalmark_dispensary_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_create',
            'child' => 'signalmark_dispensary_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_update',
            'child' => 'signalmark_dispensary_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_view',
            'child' => 'signalmark_dispensary_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'signalmark_operation_delete',
            'child' => 'signalmark_dispensary_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'signalmark_dispensary_operation_delete',
            'type' => '2'
        ]);
    }
}
