<?php

use yii\db\Migration;

class m160513_080852_add_permission_bed_type extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160513_080852_add_permission_bed_type cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'handbook_bed-type_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_bed-type_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_bed-type_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_bed-type_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_bed-type_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_create',
            'child' => 'handbook_bed-type_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_update',
            'child' => 'handbook_bed-type_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_view',
            'child' => 'handbook_bed-type_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_delete',
            'child' => 'handbook_bed-type_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_view',
            'child' => 'handbook_bed-type_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_create',
            'child' => 'handbook_bed-type_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_update',
            'child' => 'handbook_bed-type_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_view',
            'child' => 'handbook_bed-type_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_delete',
            'child' => 'handbook_bed-type_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_bed-type_operation_view',
            'child' => 'handbook_bed-type_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_create',
            'child' => 'handbook_bed-type_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_update',
            'child' => 'handbook_bed-type_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_view',
            'child' => 'handbook_bed-type_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'handbook_operation_delete',
            'child' => 'handbook_bed-type_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'handbook_bed-type_operation_delete',
            'type' => '2'
        ]);
    }
}
