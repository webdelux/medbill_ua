<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_130054_add_column_pacient_service extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151222_130054_add_column_pacient_service cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->addColumn('{{%pacient_service}}', 'date_service', $this->timestamp()->notNull()->defaultValue(0));
        $this->createIndex('idx_date_service', '{{%pacient_service}}', 'date_service');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_date_service', '{{%pacient_service}}');
        $this->dropColumn('{{%pacient_service}}', 'date_service');
    }
}
