<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_121617_add_data_handbook_emc_category extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151126_121617_add_data_handbook_emc_category cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $data = [
            [1, NULL,   '1', 'Загальноклiнiчнi'],
        ];

        foreach ($data as $value)
        {
            $this->insert('{{%handbook_emc_category}}', [
                'id' => $value[0],
                'parent_id' => $value[1],
                //'code' => $value[2],
                'name' => $value[3],
                'sort' => 0,
                'user_id' => 0,
            ]);
        }

    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->truncateTable('{{%handbook_emc_category}}');
    }

}