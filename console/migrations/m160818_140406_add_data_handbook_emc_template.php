<?php

use yii\db\Migration;
use backend\modules\handbook\models\Status;

class m160818_140406_add_data_handbook_emc_template extends Migration
{

    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m160818_140406_add_data_handbook_emc_template cannot be reverted.\n";

        return false;
    }
    */

    /**
     * Use safeUp to run migration code within a transaction
     */
    public function safeUp()
    {
        $this->insert('{{%handbook_emc_template}}', [
            'id' => 16,
            'handbook_emc_category_id' => 592,
            'name' => Yii::t('app', 'protocol_pattern_16'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 17,
            'handbook_emc_category_id' => 593,
            'name' => Yii::t('app', 'protocol_pattern_17'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 18,
            'handbook_emc_category_id' => 594,
            'name' => Yii::t('app', 'protocol_pattern_18'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);

        $this->insert('{{%handbook_emc_template}}', [
            'id' => 19,
            'handbook_emc_category_id' => 595,
            'name' => Yii::t('app', 'protocol_pattern_19'),
            'status_id' => Status::_ACTIVE,
            'user_id' => 0
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $this->delete('{{%handbook_emc_template}}', ['id' => 16]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 17]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 18]);
        $this->delete('{{%handbook_emc_template}}', ['id' => 19]);

        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }

}