<?php

use yii\db\Migration;

class m160606_141330_add_data_handbook_emc_protocol extends Migration
{
     public function safeUp()
    {
        /* Номера ID НЕ можна змінювати, так як вони можуть використовуватися у звітах та документах. Для нових протоколів зарезервовано 1001 значень.
         * Користувацькі протоколи додаються в кінець таблиці.
         *
         */

        $data = [
            [101,1,"1-01","Рекомендовано"],
            [102,1,"1-02","Лікування"],
             ];

        foreach ($data as $index => $value)
        {
            $this->insert('{{%handbook_emc_protocol}}', [
                'id' => $value[0],
                'handbook_emc_category_id' => $value[1],
                'code' => $value[2],
                'name' => $value[3],
                'handbook_emc_template_id' => 1,
                'user_id' => 0
            ]);

        }
    }

    public function safeDown()
    {
        $this->delete('{{%handbook_emc_protocol}}', [
            'id' => 101,
        ]);

        $this->delete('{{%handbook_emc_protocol}}', [
            'id' => 102,
        ]);



    }
}
