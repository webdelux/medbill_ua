<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_073527_add_permission_stacionary extends Migration
{
    /*
    public function up()
    {

    }

    public function down()
    {
        echo "m151124_073527_add_permission_stacionary cannot be reverted.\n";

        return false;
    }
     *
     */

    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_operation_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_operation_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_operation_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_operation_delete',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_create',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_update',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_view',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_delete',
            'type' => '2'
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'stacionary_index',
            'type' => '2'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'stacionary_operation_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'stacionary_operation_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'stacionary_operation_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'stacionary_operation_delete'
        ]);

        $this->insert('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_create',
            'child' => 'stacionary_create'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_update',
            'child' => 'stacionary_update'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_view',
            'child' => 'stacionary_view'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_delete',
            'child' => 'stacionary_delete'
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_view',
            'child' => 'stacionary_index'
        ]);
    }

    /**
     * Use safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_create',
            'child' => 'stacionary_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_update',
            'child' => 'stacionary_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_view',
            'child' => 'stacionary_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_delete',
            'child' => 'stacionary_delete'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'stacionary_operation_view',
            'child' => 'stacionary_index'
        ]);

        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_create_all',
            'child' => 'stacionary_operation_create'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_update_all',
            'child' => 'stacionary_operation_update'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_view_all',
            'child' => 'stacionary_operation_view'
        ]);
        $this->delete('{{%auth_item_child}}', [
            'parent' => 'operation_delete_all',
            'child' => 'stacionary_operation_delete'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_delete',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_index',
            'type' => '2'
        ]);

        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_operation_create',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_operation_update',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_operation_view',
            'type' => '2'
        ]);
        $this->delete('{{%auth_item}}', [
            'name' => 'stacionary_operation_delete',
            'type' => '2'
        ]);
    }
}
