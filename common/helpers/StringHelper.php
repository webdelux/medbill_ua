<?php

namespace common\helpers;

use Yii;
use yii\helpers\StringHelper as StrHelper;

class StringHelper extends StrHelper
{

    /**
     * Преобразует строку в нижний регистр
     *
     * @param type $string
     * @return type
     */
    public static function strToLower($string)
    {
        return mb_strtolower($string, Yii::$app->charset);
    }

    /**
     * Преобразует строку в верхний регистр
     *
     * @param type $string
     * @return type
     */
    public static function strToUpper($string)
    {
        return mb_strtoupper($string, Yii::$app->charset);
    }

}