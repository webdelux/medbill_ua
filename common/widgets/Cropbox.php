<?php

namespace common\widgets;

use Yii;

class Cropbox extends \bupy7\cropbox\Cropbox
{

    public $pathToView = '@bupy7/cropbox/views/field';

    protected function registerTranslations()
    {
        // В оригінальному класі віджета не вистачає в кінці символа слеша і зірочки 'bupy7/cropbox/*' - без нього не додаються нові переклади
        Yii::$app->i18n->translations['bupy7/cropbox/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@bupy7/cropbox/messages',
            'fileMap' => [
                'bupy7/cropbox' => 'core.php',
            ],
        ];
    }

}