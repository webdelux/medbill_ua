<?php

/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'ID' => '№',
    'Contact' => 'Зворотній зв\'язок',
    'Status' => 'Статус',
    'Created At' => 'Створено',
    'Updated At' => 'Оновлено',
    'Update {modelClass}: ' => 'Оновити {modelClass}: ',
    'Update' => 'Оновити',
    'Delete' => 'Видалити',
    'Create' => 'Створити',
    'Search' => 'Пошук',
    'Reset' => 'Скинути',
    'View' => 'Перегляд',
    'Save' => 'Зберегти',
    'Copyright' => 'Авторське право',
    'All Rights Reserved' => 'Всі права захищені',
    'Administration' => 'Адміністрування',
    'User' => 'Користувач',
    'Users' => 'Користувачі',
    'Login' => 'Увійти',
    'Login/Register' => 'Увійти/Зареєструватися',
    'Register' => 'Реєстрація',
    'Logout' => 'Вийти',
    'You are not authorized to perform this action.' => 'У вас недостатньо прав для виконання зазначеної дії.',
    'The hierarchy of permissions' => 'Ієрархія дозволів',
    'Add Contact' => 'Додати контакт',
    'Automatic payments' => 'Автоматичні платежі',
    'Balance replenishment' => 'Поповнення рахунку',
    'Blacklist' => 'Чорний список',
    'By group' => 'По групі',
    'Clients' => 'Клієнти',
    'Contact group' => 'Контактна група',
    'Contact us' => 'Зв\'язатися з нами',
    'Contacts' => 'Контакти',
    'Control panel' => 'Панель керування',
    'FAQ' => 'Часті питання',
    'Import contacts' => 'Імпортувати контакти',
    'List of contacts' => 'Список контактів',
    'Mailings' => 'Листи',
    'Manage Your Account' => 'Керування Вашим обліковим записом',
    'Management contacts' => 'Керування контактами',
    'Message templates' => 'Керування шаблонами',
    'My Information' => 'Моя інформація',
    'My balance' => 'Мій баланс',
    'My contacts' => 'Мої контакти',
    'Navigation' => 'Навігація',
    'News' => 'Новини',
    'Notifications' => 'Повідомлення',
    'Phone' => 'Телефон',
    'Read more' => 'Читати далі',
    'Reports' => 'Звіти',
    'Statistics' => 'Статистика',
    'About' => 'Про нас',
    'Home' => 'Головна',
    'Sign in' => 'Увійти',
    'Sign up' => 'Зареєструватися',
    'Lock Screen' => 'Блокування екрана',
    'Yes' => 'Так',
    'No' => 'Ні',
    'Are you sure you want to log out?' => 'Ви впевнені, що хочете вийти?',
    'Log Out' => 'Вийти',
    'Department' => 'Відділ',
    'Departments' => 'Відділення',
    'department' => 'відділення',
    'Handbook' => 'Довідник',
    'Handbooks' => 'Довідники',
    'Example' => 'Приклад',
    'Pacients' => 'Пацієнти',
    'Last name' => "Прізвище",
    'Surname' => "По-батькові",
    'Birthday' => "Дата народження",
    'Phone number' => "Номер телефону",
    'City' => "Місто",
    'Gender' => "Стать",
    'Contingent' => "Контингент",
    'Actions' => "Дії",
    'Create Pacient' => "Створити пацієнта",
    'Full Name' => "ПІБ",
    'Man' => "Чоловік",
    'Woman' => "Жінка",
    'Dispensary group' => "Диспансерна група",
    'Username' => "Користувач",
    'Name' => 'Назва',
    'First name' => 'Ім\'я',
    'Parent' => 'Батько',
    'Root' => 'Корінь',
    'Are you sure you want to delete this item?' => 'Ви впевнені, що хочете видалити цей елемент?',
    'Address' => 'Адреса',
    'Email' => 'E-mail',
    'Edrpou' => 'Код ЄДРПОУ',
    'Requisites' => 'Платіжні реквізити',
    'Working Days' => 'Робочі дні',
    'Work schedule' => 'Графік роботи',
    'Additions working schedule' => 'Доповнення робочого графіку',
    'Monday' => 'Понеділок',
    'Tuesday' => 'Вівторок',
    'Wednesday' => 'Середа',
    'Thursday' => 'Четвер',
    'Friday' => 'П\'ятниця',
    'Saturday' => 'Субота',
    'Sunday' => 'Неділя',
    'From time' => 'Початок',
    'To time' => 'Кінець',
    'Entered data successfully saved.' => 'Внесені дані успішно збережені!',
    'Type' => 'Тип',
    'mon.' => 'пн.',
    'tue.' => 'вт.',
    'wed.' => 'ср.',
    'thu.' => 'чт.',
    'fri.' => 'пт.',
    'sat.' => 'сб.',
    'sun.' => 'нд.',
    'Active' => 'Активний',
    'Inactive' => 'Неактивний',
    'Blocked' => 'Заблокований',
    'Archive' => 'Архів',
    'Common' => 'Загальний',
    'Work place' => 'Місце роботи',
    'Work Seat' => 'Посада',
    'Memo' => 'Нотатки',
    'Extended search' => 'Розширений пошук',
    'Additional phone number' => 'Додатковий телефоний номер',
    'Docum Number' => 'Номер документу',
    'Docum Type' => 'Документ',
    'Parental department' => 'Батьківське відділення',
    'Stationary' => 'Стаціонарне',
    'Ambulatory' => 'Амбулаторне',
    'Statuses' => 'Статуси',
    'Status' => 'Статус',
    'Parent status' => 'Батьківський статус',
    'Sort' => 'Сортування',
    'Description' => 'Опис',
    'Updated User' => 'Змінено користувачем',
    'Passport' => 'Паспорт',
    'Drive license' => 'Посвідчення водія',
    'Pension certificate' => 'Пенсійне посвідчення',
    'Other' => 'Інше',
    'Year-month-day' => 'Рік-місяць-число',
    'GEO' => 'Геолокація',
    'Cities' => 'Міста',
    'Regions' => 'Області',
    'Region' => 'Область',
    'Countries' => 'Країни',
    'Country' => 'Країна',
    'The above error occurred while the Web server was processing your request.' => 'Сталася помилка при обробки вашого запиту.',
    'Please contact us if you think this is a server error. Thank you.' => 'Будь ласка, зв\'яжіться з нами, якщо ви думаєте, що це помилка сервера. Дякуємо.',
    'Great town' => 'Великий населений пункт',
    'Area' => 'Район',
    'Service handbooks' => 'Службові довідники',
    'Measurements' => 'Одиниці вимірювання',
    'Measurement' => 'Одиниця вимірювання',
    'Create Measurement' => 'Додати одиницю вимірювання',
    'Update measurements' => 'Оновити одиницю вимірювання',
    'Parent category' => 'Батьківська категорія',
    'Abbreviation' => 'Аббревіатура',
    'Medical billing system' => 'Система електронного обліку для медичного закладу',
    'Search text' => 'Введіть текст для пошуку',
    'Medical Supplies' => 'Медикаменти',
    'Create Medical Supplies' => 'Додати медикамент',
    'Create category' => 'Створити категорію',
    'Category' => 'Категорія',
    'Categories' => 'Категорії',
    'Failed to save entered data!' => 'Не вдалося зберегти введенні дані',
    "You can't change parent category!" => 'Ви не можете змінити батьківську категорію!',
    "Are you sure you want to delete this category?" => 'Ви впевнені, що хочете видалити цю категорію?',
    'Close' => 'Закрити',
    'Add category' => 'Додати категорію',
    'Update category' => 'Оновити категорію',
    'Services' => 'Послуги',
    'Service' => 'Послуга',
    'service' => 'послугу',
    'price' => 'ціна',
    'Price' => 'Ціна',
    'Medical-supplies' => 'Медикаменти',
    "You can't delete parent category!" => 'Ви не можете видалити батьківську категорію!',
    'Create Service' => 'Додати послугу',
    'pacient' => 'пацієнта',
    'None' => 'Немає',
    'Payment methods' => 'Методи оплати',
    'Payment method' => 'Метод оплати',
    'Add' => 'Додати',
    'Discount' => 'Знижка',
    'Discounts' => 'Знижки',
    'discount' => 'знижку',
    'Value' => 'Значення',
    'Refferal' => 'Направлення',
    'Workplace' => 'Місце роботи',
    'Payments' => 'Платежі',
    'Pacient' => 'Пацієнт',
    'Total' => 'Сума',
    'Summary' => 'Разом',
    'Charges' => 'Нарахування',
    'uah' => 'грн.',
    'Handbooks "Electronic medical card"' => 'Довідники «Електронної медичної картки»',
    'E.M.C.' => 'Е.М.К.',
    'ICD-10' => 'МКХ-10',
    'Icds' => 'МКХ-10',
    'Code' => 'Код',
    'Stacionary' => 'Стаціонар',
    'Room' => 'Палата',
    'room' => 'палату',
    'Place' => 'Ліжко',
    'Rooms' => 'Палати',
    'Surgeries' => 'Операції',
    'Operation' => 'Операція',
    'Complications' => 'Ускладнення',
    'Template' => 'Шаблон',
    'Templates' => 'Шаблони',
    'Substitutional' => 'Підстановлювальний текст',
    'Marker' => 'Маркер',
    'Markers' => 'Маркери',
    'Number survey' => 'Номер обстеження',
    'Serial number of the survey' => 'Порядковий номер обстеження',
    'Date of survey' => 'Дата обстеження',
    'Doctor' => 'Лікар',
    'doctor' => 'лікаря',
    'Doctors' => 'Лікарі',
    'Entered' => 'Прибув',
    'Released' => 'Вибув',
    'New' => 'Новий',
    'Moved' => 'Переведено',
    'Healthy' => 'Одужав',
    'Died' => 'Помер',
    'Empty' => 'Вільно',
    'Occupied by' => 'Зайнято',
    'This place is not empty!' => 'Це ліжко зайняте!',
    'Note' => "Уточнення",
    'The Doctors name is incorrect!' => "Вказано невірне прізвище Лікаря!",
    'The Pacients name is incorrect!' => "Вказано невірне прізвище Пацієнта!",
    'You have added this room already!' => "Ви вже додали цю кімнату!",
    'You have added this place already!' => "Ви вже додали це ліжко!",
    'List of places' => 'Список ліжок',
    'Places' => 'Ліжка',
    'Edit' => 'Редагувати',
    'Total rooms:' => 'Всьго палат:',
    'Empty rooms:' => 'Вільних палат:',
    'Protocol' => 'Протокол',
    'Protocols' => 'Протоколи',
    'protocol' => 'протокол',
    'Amount' => 'Сума',
    'Execution' => 'Час виконання',
    'Required' => 'Обов\'язкове',
    'min.' => 'хв.',
    'Place a' => 'Розмістити',
    'This place is exempted!' => 'Це ліжко успішно звільнено!',
    'This place is occupied!' => 'Це ліжко успішно зайнято!',
    'This place was empty!' => 'Вже було вільне!',
    'Empty places' => 'Вільних ліжок',
    'from' => 'з',
    'Got payment' => 'Отримано кошти',
    'Refund a payment' => 'Повернуто кошти',
    'record' => 'запис',
    'IPN' => 'ІПН',
    'Total information' => 'Загальна інформація',
    'Signal mark' => 'Сигнальні позначки',
    'Objective status' => 'Об\'єктивний статус',
    'Blood group' => 'Група крові',
    'Rhesus factor' => 'Резус фактор',
    'Classification of blood' => 'Класифікація крові',
    'Blood transfusion' => 'Переливання крові',
    'Date' => 'Дата',
    'Date At' => 'Дата',
    'Pancreatic diabetes' => 'Цукровий діабет',
    'Degree' => 'Ступінь',
    'Stage' => 'Стадія',
    'Treatment' => 'Лікування',
    'Term illness' => 'Термін хвороби',
    'Quantity' => 'Кількість',
    'Receive' => 'Надходження',
    'Send' => 'Витрата',
    'Docum date' => 'Дата документу',
    'Movement' => 'Рух',
    'medical supplies' => 'медикаментів',
    'Wrong date format. Must be: year-month-date' => 'Невірний формат дати. Потрібно: рік-місяць-число!',
    'Orally' => 'Перорально',
    'Subcutaneously' => 'Підшкірно',
    'Vaccination' => 'Вакцинація',
    'vaccination' => 'вакцинацію',
    'Age' => 'Вік',
    'Dosage' => 'Доза',
    'Series' => 'Серія',
    'Medicament' => 'Назва препарату',
    'Usage' => 'Спосіб введення',
    'Local Reaction' => 'Місцева реакція',
    'Common Reaction' => 'Загальна реакція',
    'Contraindication' => 'Протипоказання',
    'Icd' => 'Найменування щеплення',
    'vaccinations' => 'вакцинації',
    'Date start' => 'Дата початку',
    'Date end' => 'Дата кінця',
    'Infectious disease' => 'Інфекційне захворювання',
    'Surgical interventions' => 'Хірургічні втручання',
    'Surgical intervention' => 'Хірургічне втручання',
    'Allergic anamnesis' => 'Алергологічний анамнез',
    'Intolerance to drugs' => 'Непереносимість до лікарських препаратів',
    'Preventive inspection' => 'Профілактичний огляд',
    'Complaints and history of disease' => 'Скарги та анамнез захворювання',
    'Organ' => 'Орган',
    'Organs' => 'Органи',
    'Day' => 'День',
    'Week' => 'Тиждень',
    'Month' => 'Місяць',
    'Year' => 'Рік',
    'Planned' => 'Планується',
    'Compleated' => 'Виконано',
    'Not defined' => 'Не визначено',
    'Protocol date' => 'Дата протоколу',
    'Complaints' => 'Скарги',
    'Anamnesis life' => 'Анамнез життя',
    'Disease' => 'Захворювання',
    'Diseases childhood' => 'Хвороби дитинства',
    'TVS, venous diseases, hereditary diseases' => 'ТВС, вен захворювання, спадкові хвороби',
    'Other' => 'Інше',
    'Hypertensive disease' => 'Гіпертонічна хвороба',
    'Risk' => 'Ризик',
    'Medical products' => 'Лікарські препарати',
    'The overall objective status' => 'Загальний об\'єктивний статус',
    'Show/Hide' => 'Показати/Заховати',
    'Canceled' => 'Скасовано',
    'Consultations of experts' => 'Консультації спеціалістів',
    'An additional objective status' => 'Додатковий об\'єктивний статус',
    'Consultant' => 'Консультант',
    'Insurance' => 'Страхування',
    'Insurance companies' => 'Страхові компанії',
    'Insurance company' => 'Страхова компанія',
    'Insurance polise' => 'Страховий поліс',
    'Insurance polises' => 'Страхові поліси',
    'Insurance number' => 'Номер страхового полісу',
    'Planning' => 'Планування',
    'The preliminary diagnosis' => 'Попередній діагноз',
    'Plan survey' => 'План обстеження',
    'Survey' => 'Обстеження',
    'Surveys' => 'Обстеження',
    'Plan treatment' => 'План лікування',
    'Consultation' => 'Консультація',
    'Consultations' => 'Консультації',
    'Speciality' => 'Спеціальність',
    'Specialities' => 'Спеціальності',
    'Note' => 'Примітка',
    'Current' => 'Поточний',
    'Disease history' => 'Історія хвороби',
    'Diagnosis' => 'Діагноз',
    'Diagnoses' => 'Діагнози',
    'An alternative diagnosis' => 'Альтернативний діагноз',
    'Print instead of the main alternative diagnosis' => 'Друк альтернативного діагноза замість основного',
    'Final diagnosis or clarified' => 'Заключний чи уточнений діагноз',
    'Newly diagnosed' => 'Вперше встановлений діагноз',
    'Established in preventive inspection' => 'Встановлено при профілактичному огляді',
    'Intended' => 'Передбачуваний',
    'Ending' => 'Заключний',
    'Corrected' => 'Уточнений',
    'Previous' => 'Попередній',
    'Clinical' => 'Клінічний',
    'Data is not preserved, please correct the errors.' => 'Дані не збереглися, виправте будь ласка помилки.',
    'Select' => 'Вибрати',
    'Bed type' => 'Профіль ліжка',
    'Repairing' => 'На ремонті',
    'Not active' => 'Недоступне',
    'Male' => 'Чоловічий',
    'Female' => 'Жіночий',
    'Stationary card #' => '№ медичної картки стаціонарного хворого',
    'Ambulatory card #' => '№ медичної картки амбулаторного хворого',
    'Village' => 'Село',
    'yyyy-mm-dd' => 'рік-місяць-число',
    'Dispensary' => 'Диспансерний нагляд',
    'Date in' => 'Взято на облік',
    'Date out' => 'Знято з обліку',
    'Dispensary note' => 'Причина',
    'Spot treatment' => 'Місце лікування',
    'Polyclinic' => 'Поліклініка',
    'At home' => 'Вдома',
    'Day hospital' => 'Денний стаціонар',
    'At home as hospital' => 'Стаціонар вдома',
    'Hospital' => 'Стаціонар',
    'Specialization' => 'Спеціалізація',
    'City type' => 'Тип приналежності',
    'Basic' => 'Основний',
    'Concomitant' => 'Супутні',
    'to' => 'до',
    'Release' => 'Виписати',
    "The mother's stay near the of a sick child" => "Перебування матері біля хворої дитини",
    'This pacient is placed already!' => 'Цього пацієнта вже розмістили!',
    'Additionally' => 'Додатково',
    'Diagnosis on operation' => 'Діагноз при операції',
    'Additional diagnosis' => 'Додатковий діагноз',
    'Beginning operation' => 'Початок операції',
    'time' => 'час',
    'hours.' => 'годин',
    'minutes.' => 'хвилин',
    'Duration' => 'Тривалість',
    'Surgeon' => 'Хірург',
    'Result' => 'Результат',
    'Anesthetist' => 'Анестезіолог',
    'Anesthesia' => 'Анестезія',
    'Additional complications' => 'Додаткові ускладнення',
    'Start time operation' => 'Час початку операції',
    'Written(a) recovery' => 'Виписаний(а) з одужанням',
    'Written(a) improving' => 'Виписаний(а) з поліпшенням',
    'Written(a) deterioration' => 'Виписаний(а) з погіршенням',
    'Written(a) unchanged' => 'Виписаний(а) без змін',
    'Died' => 'Помер',
    'Endotracheal' => 'Ендотрахеальна',
    'Intravenous' => 'Внутрішньовенна',
    'Intramuscularly' => 'Внутрішньом\'язева',
    'Peridural' => 'Перидуральна',
    'Laryngeal mask' => 'Ларингеальна маска',
    'Cerebrospinal' => 'Спинномозкова',
    'Inhalation' => 'Інгаляційна',
    'Nazoedotrahealna' => 'Назоедотрахеальна',
    'The local' => 'Місцева',
    'The type of operation' => 'Тип операції',
    'Urgent' => 'Екстрена',
    'Planned' => 'Планова',
    'More details' => 'Детальніше',
    'Established in routine inspection' => 'Встановлено при профілактичному огляді',
    'Incapacity' => 'Листок непрацездатності',
    'Incapacities' => 'Листки непрацездатності',
    'Sick' => 'Хворіє',
    'Still sick' => 'Продовжує хворіти',
    'Incapacity Type' => 'Тип листка',
    'Incapacity Number' => '№ листка непрацездатності',
    'Incapacity Date' => 'Дата листка непрацездатності',
    'Discharge' => 'Виписка',
    'Hospitalization' => 'Госпіталізація',
    'hospitalization' => 'госпіталізації',
    'Immediately' => 'Ургентна',    //'За терміновими показаннями',
    'Illness' => 'Планова', //'Через захворювання, одержання травми або в плановому порядку',
    'Hospitalization Interval' => 'Термін госпіталізації, годин',
    'Diagnosis In' => 'Діагноз закладу, що направив хворого',
    'discharge' => 'виписки',
    'Are you sure you want to release this pacient?' => 'Ви впевнені, що хочете виписати цього пацієнта?',
    'Cure type' => 'Проведене лікування',
    'Recovery' => 'Відновлення працездатності',
    'Cure result' => 'Результат лікування',
    'Ill days' => 'Проведено ліжко-днів',
    'Diagnosis out' => 'Діагноз при виписці',
    'Resume examination' => 'Висновок для тих, хто поступає на експертизу',
    'Special' => 'Спеціальне',
    'Palliative' => 'Паліативне',
    'Symptomic' => 'Симптомічне',
    'Fully restored' => 'Відновлена повністю',
    'Reduced' => 'Знижена',
    'Temporarily lost' => 'Тимчасово втрачена',
    'Steadily lost due to illness' => 'Стійко втрачена у зв’язку із захворюванням',
    'Steadily lost for other reasons' => 'Стійко втрачена з інших причин',
    'Transferred to another medical facility' => 'Переведений в інший лікувальний заклад',
    'Transferred to another department' => 'Переведений в інше відділення',
    'A healthy' => 'Здоровий',
    'Refresh' => 'Оновити',
    'The after hours' => 'Неробочі години',
    'The after hours doctor' => 'Неробочі години лікаря',
    'The after hours doctor "{doctor}" in department "{department}"' => 'Неробочі години лікаря "{doctor}" в відділенні "{department}"',
    'The after hours department' => 'Неробочі години відділення',
    'Others' => 'Інші',
    'Pathological examination' => 'Патолого-анатомічне обстеження',
    'Pathological number' => '№ патолого-анатомічного обстеження',
    'Pathological Date' => 'Дата розтину',
    'Accuracy' => 'Збіг клінічного та патолого-анатомічного діагнозів',
    'Divergence' => 'Причини розбіжності',
    'Ill time' => 'Приблизний час між початком захворювання та смертю',
    'Cause' => 'Безпосередня причина смерті',
    'Ills' => 'Захворювання та патологічні стани, що зумовили безпосередню причину смерті',
    'Other reasons' => 'Інші суттєві стани, які сприяли смерті, але не пов’язані із захворюванням чи його ускладненням, яке безпосередньо є причиною смерті',
    'Pregnancy interval' => 'Тиждень вагітності, день післяпологового періоду, тиждень після пологів',
    'Full coincidence' => 'Повний збіг',
    'Divergences primary diagnosis' => 'Розбіжність основного діагнозу',
    'Divergences concomitant diagnosis' => 'Розбіжність супутнього діагнозу',
    'Divergences сomplication diagnosis' => 'Розбіжність ускладнень діагнозу',
    'Full divergence' => 'Повна розбіжність',
    'The objective difficulties of diagnosis' => 'Об’єктивні труднощі діагностики',
    'Short hospital stay' => 'Короткочасне перебування в лікарні',
    'Less examination of the patient' => 'Недообстеження хворого',
    'Revaluation survey data' => 'Переоцінка даних обстеження',
    'Rare diseases' => 'Рідкісне захворювання',
    'Wrong disign of diagnosis' => 'Неправильне оформлення діагнозу',
    'Diary' => 'Щоденник',
    'diary' => 'щоденника',
    'Attachments' => 'Вкладені файли',
    'Attachment' => 'Вкладений файл',
    'Filename' => 'Назва файлу',
    'Print' => 'Друкувати',
    'Pathological examination _report1' => 'Виписка з протоколу патолого-анатомічного обстеження',
    'Departments category' => 'Категорії відділень',
    'Documents' => 'Документи',
    'Choose diagnosis' => 'Виберіть діагноз',
    'Invoice' => 'Рахунок',
    'Invoice from' => 'від',
    'Receiver' => 'Отримувач',
    'Code1' => 'Код цифровий',
    'Code2' => 'Код АЛЬФА2',
    'Code3' => 'Код АЛЬФА3',
    'Post index' => 'Поштовий індекс',
    'Citizenship' => 'Громадянство',
    'Trauma' => 'Травма',
    'Work injury' => 'Виробнича травма',
    'Non-productive injury' => 'Невиробнича травма',
    'There is no data for view!' => 'Немає даних для відображення',
    'form066o' => 'Статистична карта хворого, який вибув із стаціонару',
    'First time hospitalization' => 'В поточному році, з приводу даної хвороби, госпіталізований',
    'First time' => 'Вперше',
    'Repeatedly' => 'Повторно',
    'Pathological' => 'Патолого-анатомічний',
    'Code' => 'Код',
    'Referral to microbiological research' => 'Направлення на мікробіологічне дослідження',
    'Registry' => 'Реєстратура',
    'Show' => 'Показати',
    'Epidemiological surveillance map' => 'Карта епідеміологічного спостереження',
    'Choose surgery' => 'Виберіть операцію',
    'Preoperative epicrisis' => 'Передопераційний епікриз',
    'Preoperative preparation' => 'Передопераційна підготовка',
    'Operation early period' => 'Ранній післяопераційний період',
    'Preoperative preparation time' => 'Час передопераційної підготовки',
    'No records' => 'Нема записів',
    'Change photo' => 'Змінити фото',
    'Avatar' => 'Аватар',
    'Medical card' => 'Медична картка',
    'Return' => 'Повернутися',
    'Form pattern' => 'Шаблон форми',
    'Not available' => 'Недоступний',
    /* Шаблони протоколів */
    'protocol_pattern_1' => 'Основний',
    'protocol_pattern_2' => 'Клінічний аналіз крові N 1 (повний)',
    'protocol_pattern_3' => 'Клінічний аналіз крові N 2 (скорочений)',
    'protocol_pattern_4' => 'Клінічний аналіз крові N 3 (скорочений)',
    'protocol_pattern_5' => 'Клінічний аналіз крові N 4 (скорочений)',
    'protocol_pattern_6' => 'Клінічний аналіз сечі N 1 (повний)',
    'protocol_pattern_7' => 'Клінічний аналіз сечі N 2 (скорочений)',
    'protocol_pattern_8' => 'Аналіз сечі на глюкозу і кетонові тіла',
    'protocol_pattern_9' => 'МР-томографія головного мозку',
    'protocol_pattern_10' => 'Глюкозурічний профіль',
    'protocol_pattern_11' => 'МР-томографія орбіти',
    'protocol_pattern_12' => 'МР-томографія придаткових пазух носа',
    'protocol_pattern_13' => 'МР-томографія шинного відділу хребта',
    'protocol_pattern_14' => 'МР-томографія грудного відділу хребта',
    'protocol_pattern_15' => 'МР-томографія поясничного відділу хребта',
    'protocol_pattern_16' => 'Імунізація проти бактеріального захворювання',
    'protocol_pattern_17' => 'Імунізація проти вірусного захворювання',
    'protocol_pattern_18' => 'Медикаментозна профілактика протозойної та гельмінтозної інфекції',
    'protocol_pattern_19' => 'Медикаментозний захист проти інфекційних хвороб',
    'protocol_pattern_20' => 'Реакції неспецифічного імунітету',
    'protocol_pattern_21' => 'Аналіз калу N 1',
    'protocol_pattern_22' => 'Мікроскопія калу (копрограма)',
    'protocol_pattern_23' => 'Аналіз калу N 2',
    'protocol_pattern_24' => 'Біохімічне дослідження крові N 1 (повний аналіз)',
    'protocol_pattern_25' => 'Біохімічне дослідження крові N 2 (скорочений аналіз)',
    'protocol_pattern_26' => 'Біохімічне дослідження крові N 3 (скорочений аналіз)',
    'protocol_pattern_27' => 'Біохімічне дослідження крові N 4 (скорочений аналіз)',
    'protocol_pattern_28' => 'Аналіз крові (ревмокомплекс)',
    'protocol_pattern_29' => 'Аналіз сечі - зміст гормонів і медіаторів',
    'protocol_pattern_30' => 'A-сканування',
    'protocol_pattern_31' => 'B-сканування',
    /* */
    'Hemoglobin' => 'Гемоглобін',
    'Eritrotsity' => 'Еритроцити',
    'Color index' => 'Колірний показник',
    'The average content of hemoglobin in one erythrocyte' => 'Середній вміст гемоглобіну в 1 еритроциті',
    'Leukocytes' => 'Лейкоцити',
    'Leukocyte formula' => 'Лейкоцитарна формула',
    'Erythrocyte sedimentation rate' => 'Швидкість осідання еритроцитів',
    'Platelets' => 'Тромбоцити',
    'Reticulocytes' => 'Ретикулоцити',
    'The morphology erythrocytes' => 'Морфологія еритроцитів',
    'The morphology leukocytes' => 'Морфологія лейкоцитів',
    'Color' => 'Колір',
    'Transparency' => 'Прозорість',
    'Specific weight' => 'Питома вага',
    'Albumen' => 'Білок',
    'Glucose' => 'Глюкоза',
    'Ketone bodies' => 'Кетонові тіла',
    'Indican' => 'Індикан',
    'Bile pigments' => 'Жовчні пігменти',
    'Bilirubin' => 'Білірубін',
    'Urobilin' => 'Уробілін',
    'Microscopic examination of urine sediment' => 'Мікроскопічне дослідження осаду сечі',
    'The reaction of urine' => 'Реакція сечі',
    /* */
    'Epitome epicrisis' => 'Виписний епікриз',
    'Standard of laboratory inspection of patients №1 (before urgent surgery)' => 'Стандарт лабораторного обстеження хворих №1 (перед ургентною операцією)',
    'To make the schedule please add at least one branch!' => 'Для того, щоб заповнити графік роботи, будь ласка, додайте хоча б одне відділення!',
    'Plans' => 'Планується',
    'Diagnostic conclusion' => 'Діагностичний висновок',
    'Preventive vaccinations and drug prevention' => 'Профілактичні щеплення та медикаментозна профілактика',
    'Name vaccination' => 'Найменування щеплення',
    'The response to vaccination' => 'Реакція на щеплення',
    'Medical contraindications' => 'Медичні протипоказання',
    'Intravenous.' => 'В/в',
    'Intramuscular.' => 'В/м',
    'The total' => 'Загальна',
    'Testing for RW' => 'Обстеження на RW',
    'till' => 'по',
    'Form12' => 'Форма 12 (Звіт про захворювання, зареєстровані у хворих, які проживають у районі обслуговування лікувально-профілактичного закладу, за вибраний період)',
    '12t1000' => 'Форма 12 Таблиця 1000',
    'd-m-Y' => 'д/м/р',
    'Value is duplicated!' => 'Ці данні вже внесено!',
    'Consistency and form' => 'Консистенція та форма',
    'Scent' => 'Запах',
    '(PH) of stool' => '(рН) калу',
    'Occult blood' => 'Прихована кров',
    'Stercobilin' => 'Стеркобілін',
    'Muscle fibers from pokreslenistyu' => "М'язові волокна з покресленістю",
    'Muscle fibers without pokreslenosti' => "М'язові волокна без покресленості",
    'Connective tissue' => 'Сполучна тканина',
    'Fat neutral' => 'Жир нейтральний',
    'Fatty acids' => 'Жирні кислоти',
    'Mila' => 'Мила',
    'Digestible fiber' => 'Рослинна клітковина перетравна',
    'Fiber is not digestible' => 'Рослинна клітковина не перетравна',
    'Starch' => 'Крохмаль',
    'Yodofilni bacteria' => 'Йодофільні бактерії',
    'Crystals' => 'Кристали',
    'Mucus' => 'Слиз',
    'Epithelium' => 'Епітелій',
    'Elementary' => 'Найпростіші',
    'Eggs worms' => 'Яйця глистів',
    'Yeasts mushrooms' => 'Дріжджові гриби',
    'Eggs of helminths' => 'Яйця гельмінтів',
    'Total protein' => 'Загальний білок',
    'Protein fractions' => 'Білкові фракції',
    'Residual nitrogen' => 'Залишковий азот',
    'Urea' => 'Сечовина',
    'Creatinine' => 'Креатинін',
    'Uric acid' => 'Сечова кислота',
    'Lipids' => 'Ліпіди',
    'Cholesterol' => 'Холестерин',
    'Triglycerides' => 'Тригліцериди',
    'General fosfolipydy' => 'Загальні фосфоліпиди',
    'Beta lipoproteins' => 'Бета-ліпопротеїди',
    'Bilirubin and its fractions' => 'Білірубін і його фракції',
    'Sodium' => 'Натрій',
    'Calcium' => 'Кальцій',
    'Magnesium' => 'Магній',
    'Iron' => 'Залізо',
    'Chlorine' => 'Хлор',
    'Inorganic phosphorus' => 'Неорганічний фосфор',
    'AcAT' => 'АсАТ',
    'AlAT' => 'АлАТ',
    'Alpha-amylase' => 'Альфа-амілаза',
    'Fructose-1,6-diphosphate aldolaze' => 'Фруктозо-1,6- дифосфат-альдолази',
    'Creatine phosphokinase' => 'Креатинфосфокіназа',
    'Isoenzymes creatine phosphokinase' => 'Ізоферменти креатинфосфокінази',
    'Lactate dehydrogenase' => 'Лактатдегідрогеназа',
    'Isoenzymes of lactate dehydrogenase' => 'Ізоферменти лактатдегідрогенази',
    'Acid phosphatase' => 'Кисла фосфотаза',
    'Alkaline phosphatase' => 'Лужна фосфотаза',
    'Pseudocholinesterase' => 'Псевдохолінестераза',
    'Sialic acid' => 'Сіалові кислоти',
    'Seromucoid' => 'Серомукоїд',
    'Hexoses' => 'Гексоза',
    'Thymol test' => 'Тимолова проба',
    'Anti-streptolysin' => 'Анті-о- стептолізин',
    'Antyhialuronidazy' => 'Антигіалуронідази',
    'Antystreptokinaza' => 'Антистрептокіназа',
    'C-reactive protein' => 'С-реактивний білок',
    'Rheumatoid factor' => 'Ревматоїдний фактор',
    '12t1001' => 'Форма 12 Таблиця 1001',
    '17-oksikortikosteroidov' => '17-оксикортикостероїди',
    '17-ketosteroids' => '17-кетостероїди',
    'Prehnandiol' => 'Прегнандіол',
    'Estrogen' => 'Естрогени',
    'Adrenalin' => 'Адреналін',
    'Norepinephrine' => 'Норадреналін',
    'Dopa and dopamine' => 'ДОФА і дофаміни',
    '5-oksyyndoluksusnaya acid' => '5-оксиіндолуксусної кислоти',
    'Vanililmyhdaleva acid' => 'Ванілілмигдалева кислота',
    'PZO' => 'ПЗО',
    'PK' => 'ПК',
    'TK' => 'ТК',
    'Fit the retina' => 'Прилягання сітківки',
];
