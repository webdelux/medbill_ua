<?php
return [
    'Crop' => 'Обрізати',
    'Browse' => 'Огляд',
    'Show original' => 'Показати оригінал',
    'Height' => 'Висота',
    'Width' => 'Ширина',
    'Reset' => 'Скинути',
];
