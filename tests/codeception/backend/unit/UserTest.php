<?php

namespace tests\codeception\backend\unit;

use backend\models\User;
use Codeception\Specify;
use tests\codeception\backend\unit\DbTestCase;
//use yii\codeception\DbTestCase;

/**
 * @property array $users
 */
class UserTest extends DbTestCase
{
    use Specify;

    public function testValidateEmptyNewPassword()
    {
        $model = new User([
            'username' => 'TestName',
            'email' => 'other@example.com',
            'phone' => '+380673858978',
            'password' => '',
        ]);

        $model->scenario = 'create';

        expect('model is not valid', $model->validate())->false();
        expect('password is required', $model->errors)->hasKey('password');

    }

    public function testValidateCorrectPhoneFormat()
    {
        $model = new User([
            'username' => 'TestName',
            'email' => 'other@example.com',
            'phone' => '+380673858978',
            'password' => '11111111',
        ]);

        $model->scenario = 'create';
        expect('model is not valid', $model->validate())->true();
    }


    /**
     * @param string $phone
     * @dataProvider getPhones
     */
    public function testValidateWrongPhoneFormat($phone)
    {
        $model = new User([
            'username' => 'TestName',
            'email' => 'other@example.com',
            'phone' => $phone,
            'password' => '11111111',
        ]);

        $model->scenario = 'create';

        expect('model is not valid', $model->validate())->false();
        expect('password is required', $model->errors)->hasKey('phone');

    }

    public function getPhones()
    {
        return [
            'phone1' => ['abc'],
            'phone2' => ['+380-673858978'],
            'phone3' => ['+38-673858978'],
            'phone4' => ['+38 673858978'],
            'phone5' => ['+38a673858978'],
            'phone6' => ['+3806738589781'],
            'phone7' => ['+38067385897'],

        ];
    }



}
