<?php
// Run: #php user.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');

for ($index = 1; $index <= 100000; $index++)            // 26006 sec = 7.22hr
{
    $stacionary = new \backend\models\Stacionary;

    $stacionary->doctorFullName = 1;
    $stacionary->pacientFullName = 1;


    $pacient = \backend\models\Pacient::find()->select(['id'])->orderBy('rand()')->one();
    $stacionary->pacient_id = $pacient->id;

    $department = \backend\modules\handbook\models\Department::find()->select(['id'])->orderBy('rand()')->one();
    $stacionary->department_id = $department->id;

    $room = \backend\modules\handbook\models\HandbookRoom::find()->select(['id'])->orderBy('rand()')->one();
    $stacionary->room_id = $room->id;

    $place = \backend\modules\handbook\models\HandbookStacionary::find()->select(['id'])->orderBy('rand()')->one();
    $stacionary->place_id = $place->id;

    $doctor = \backend\modules\user\models\Profile::find()->select(['user_id'])->orderBy('rand()')->one();
    $stacionary->doctor_id = $doctor->user_id;

    $stacionary->type = $faker->numberBetween($min = 1, $max = 2);
    $stacionary->type_note = $faker->numberBetween($min = 1, $max = 2);


    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $stacionary->user_id = $user->id;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $stacionary->updated_user_id = $user->id;
    //$pacient->created_at = $faker->unixTime;
    //$pacient->updated_at = $faker->unixTime;

    $stacionary->date_stacionary = $faker->dateTime->format('Y-m-d H:i:s');


    if ($stacionary->validate())
    {
        $stacionary->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;