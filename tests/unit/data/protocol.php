<?php
// Run: #php protocol.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');

for ($index = 1; $index <= 100000; $index++)
{

    $protocol = new \backend\models\Protocol;

    $pacient = \backend\models\Pacient::find()->select(['id'])->orderBy('rand()')->one();
    $protocol->pacient_id = $pacient->id;

    $department = \backend\modules\handbook\models\Department::find()->select(['id'])->orderBy('rand()')->one();
    $protocol->department_id = $department->id;

    $doctor = \backend\modules\user\models\Profile::find()->select(['user_id'])->orderBy('rand()')->one();
    $protocol->doctor_id = $doctor->user_id;

    $prot = \backend\modules\handbookemc\models\Protocol::find()->select(['id'])->orderBy('rand()')->one();
    $protocol->protocol_id = $prot->id;

    $refferal = \backend\modules\handbook\models\Refferal::find()->select(['id'])->orderBy('rand()')->one();
    $protocol->refferal_id = $refferal->id;

    $category = \backend\modules\handbookemc\models\Category::find()->select(['id'])->orderBy('rand()')->one();
    $protocol->category_id = $category->id;

    $protocol->protocol_date = $faker->dateTimeBetween($startDate = '-180 days', $endDate = '180 days')->format('Y-m-d H:i');

    $protocol->total = $faker->numberBetween($min = 100, $max = 100000);
    $protocol->discount = $faker->numberBetween($min = 100, $max = 100000);
    $protocol->status = $faker->numberBetween($min = 1, $max = 3);
    $protocol->description = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $protocol->user_id = $user->id;

    $protocol->updated_user_id = $user->id;
    //$pacient->created_at = $faker->unixTime;
    //$pacient->updated_at = $faker->unixTime;

    if ($protocol->validate())
    {
        $protocol->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;