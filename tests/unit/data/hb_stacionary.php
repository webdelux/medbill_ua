<?php
// Run: #php user.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');


for ($index = 1; $index <= 20; $index++)          //генеруємо палати
{
    $hbRoom = new \backend\modules\handbook\models\HandbookRoom;

    $department = \backend\modules\handbook\models\Department::find()->select(['id'])->orderBy('rand()')->one();
    $hbRoom->department_id = $department->id;

    $hbRoom->room = $department->id . $faker->bothify($string = '##?');
    $hbRoom->description = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $hbRoom->user_id = $user->id;

    //$hbRoom->updated_user_id =$user->id;

    //$hbRoom->created_at =$faker->unixTime;
    //$hbRoom->updated_at =$faker->unixTime;


     if ($hbRoom->validate())
    {
        $hbRoom->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;



for ($index = 1; $index <= 100; $index++)              // Генеруємо ліжка
{
    $place = new \backend\modules\handbook\models\HandbookStacionary;

    $room = \backend\modules\handbook\models\HandbookRoom::find()->select(['id'])->orderBy('rand()')->one();
    $place->room_id = $room->id;

    $place->place = $faker->bothify($string = '##?');

    $place->description = $faker->realText;

    $place->status = $faker->numberBetween($min = 1, $max = 2);

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $place->user_id = $user->id;

    if ($place->validate())
    {
        $place->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;