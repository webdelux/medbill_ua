<?php
// Run: #php user.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');

for ($index = 1; $index <= 1000; $index++)
{
    $city = \backend\modules\handbook\models\City::find()->select(['id'])->orderBy('rand()')->one();

    $user = new \backend\models\User;
    $user->scenario = 'create';

    $user->username = $faker->userName;
    $user->email = $faker->email;
    $user->phone = substr('+38' . preg_replace('/[^0-9]/', '', $faker->phoneNumber), 0, 13);
    $user->password = $faker->password;
    $user->auth_key = Yii::$app->security->generateRandomString();
    $user->confirmed_at = $faker->unixTime;
    $user->created_at = $faker->unixTime;
    $user->updated_at = $faker->unixTime;
    $user->registration_ip = $faker->ipv4;

    if ($user->save())
    {
        print_r($index . '=');

        $profile = $user->profile;
        if ($profile == null)
        {
            $profile = Yii::createObject(\backend\modules\user\models\Profile::className());
            $profile->link('user', $user);

            $profile->name = $faker->name;
            $profile->public_email = $faker->freeEmail;
            $profile->gravatar_email = $faker->safeEmail;
            $profile->gravatar_id = md5(strtolower($profile->gravatar_email));

            $profile->location = $faker->streetAddress;
            $profile->website = 'http://' . $faker->domainName;

            $profile->payment = $faker->creditCardNumber;
            $profile->bio = $faker->realText;
            $profile->note = $faker->realText;

            $profile->city_id = $city->id;

            if ($profile->save())
                print_r('OK;');
        }
    }
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;