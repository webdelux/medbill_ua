<?php
// Run: #php user.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');


for ($index = 1; $index <= 200; $index++)
{
    $serviceHb = new \backend\modules\handbook\models\Service;

    $serviceHb->category_id =2;
    $serviceHb->name = $faker->sentence($nbWords = 2);
    $serviceHb->price = $faker->numberBetween($min = 100, $max = 100000);
    $serviceHb->description = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $serviceHb->user_id = $user->id;

    $serviceHb->updated_user_id =$user->id;

    $serviceHb->created_at =$faker->unixTime;
    $serviceHb->updated_at =$faker->unixTime;
    $serviceHb->status_id = $faker->numberBetween($min = 1, $max = 4);


     if ($serviceHb->validate())
    {
        $serviceHb->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;



for ($index = 1; $index <= 100000; $index++)
{
    $service = new \backend\models\PacientService;


    $service->pacientFullName = 1;


    $pacient = \backend\models\Pacient::find()->select(['id'])->orderBy('rand()')->one();
    $service->pacient_id = $pacient->id;

    $serviceHb = \backend\modules\handbook\models\Service::find()->select(['id'])->orderBy('rand()')->one();
    $service->service_id = $serviceHb->id;

    $service->total = $faker->numberBetween($min = 100, $max = 100000);
    $service->discount = $faker->numberBetween($min = 100, $max = 100000);

    $service->date_service = $faker->dateTime->format('Y-m-d');

    $service->docum = $faker->sentence($nbWords = 4);

    $service->description = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $service->user_id = $user->id;

//    $service->updated_user_id = $user->id;

    //$service->created_at = $faker->unixTime;
    //$service->updated_at = $faker->unixTime;



    if ($service->validate())
    {
        $service->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;