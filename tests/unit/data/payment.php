<?php
// Run: #php payment.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');


for ($index = 1; $index <= 100000; $index++)
{
    $payment = new \backend\models\Payment;

    $pacient = \backend\models\Pacient::find()->select(['id'])->orderBy('rand()')->one();
    $payment->pacient_id = $pacient->id;

    $payment->total = $faker->numberBetween($min = 100, $max = 100000);
    $payment->discount = $faker->numberBetween($min = 100, $max = 100000);

    $payment->date_payment = $faker->dateTime->format('Y-m-d');

    $payment->type = $faker->numberBetween($min = 1, $max = 2);

    $payment->description = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $payment->user_id = $user->id;

    $payment->payment_method_id = $faker->numberBetween($min = 0, $max = 3);

//    $service->updated_user_id = $user->id;

    //$service->created_at = $faker->unixTime;
    //$service->updated_at = $faker->unixTime;



    if ($payment->validate())
    {
        $payment->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;