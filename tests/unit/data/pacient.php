<?php
// Run: #php user.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');

for ($index = 1; $index <= 100000; $index++)  //3421 seconds = 57minutes
{
    $pacient = new \backend\models\Pacient;

    $name = explode(" ", $faker->name);
    $pacient->name = $name[0];

    $surname = explode(" ", $faker->name);
    $pacient->surname = $surname[1];
    $pacient->lastname = $faker->lastName;
    $pacient->birthday = $faker->date($format = 'Y-m-d', $max = 'now');
    $pacient->ipn = $faker->numerify($string = '##########');
    $pacient->phone = '+38'. $faker->numerify($string = '##########');
    $pacient->phone1 = '+38'. $faker->numerify($string = '##########');
    $pacient->city_id = $faker->randomElement([6, 276, 280, 292, 314, 444,663, 1057, 1158, 1351, 1509, 2334, 2538, 2574]); //$faker->numberBetween($min = 276, $max = 5468846);
    $pacient->address = $faker->streetAddress;
    $pacient->gender = $faker->numberBetween($min = 1, $max = 2);
    $pacient->work = $faker->company;
    $pacient->seat = $faker->company;
    $pacient->dispensary_group = 1;   //$faker->numberBetween($min = 0, $max = 1);
    $pacient->contingent = $faker->numberBetween($min = 1, $max = 9);
    $pacient->docum_type = $faker->sentence($nbWords = 6);
    $pacient->docum_number = $faker->regexify('[0-9]');
    $pacient->memo = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $pacient->user_id = $user->id;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $pacient->updated_user_id = $user->id;
    //$pacient->created_at = $faker->unixTime;
    //$pacient->updated_at = $faker->unixTime;
    $pacient->status_id = $faker->numberBetween($min = 1, $max = 4);


    if ($pacient->validate());
        $pacient->save();

    echo ' '.$index;


}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;