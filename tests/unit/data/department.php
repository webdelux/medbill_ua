<?php
// Run: #php department.php

namespace tests\unit;

use Yii;
use backend\modules\handbook\models\Department;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;

$time_start = microtime(true);

$faker = \Faker\Factory::create('ru_RU');


//Створюємо корневе відділення
$model = new Department;

$model->name = Yii::t('app', 'Departments') . ' 0';
$user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
$model->user_id = $user->id;

// Перевіряємо чи є кореневий вузол
$root = Department::find()->select(['id'])->orderBy('rand()')->one();
if (!$root)
{
    // Створення кореневий вузол
    $department = new Department($model->attributes);
    $department->makeRoot();

    if ($model->validate());
        $model->save();

    echo 'Main DEPARTMENT created!';
}
else
{
    echo 'Main DEPARTMENT already created!';
}




//Створюємо відділення
for ($index = 1; $index <= 20; $index++)
{
    $model = new Department;

    $dep = Department::find()->select(['id'])->where('`depth` = 0')->orderBy('rand()')->one();
    $model->parent_id = $dep->id;

    $model->name = Yii::t('app', 'Departments') . ' ' . $index;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $model->user_id = $user->id;

    $model->working_days = 'mon,tue,wed,thu,fri,sat,sun';
    $model->working_time_mon_from = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_mon_to = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_tue_from = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_tue_to = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_wed_from = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_wed_to = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_thu_from = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_thu_to = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_fri_from = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_fri_to = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_sat_from = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_sat_to = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_sun_from = $faker->time($format = 'H:i:s', $max = 'now');
    $model->working_time_sun_to = $faker->time($format = 'H:i:s', $max = 'now');


    // Перевіряємо чи є кореневий вузол
    $root = Department::findOne(['id' => $model->parent_id]);
    if ($root)
    {
        $department = new Department($model->attributes);
        $department->appendTo($root);
    }

//    if ($model->validate());
//        $model->save();

    echo ' '.$index;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;