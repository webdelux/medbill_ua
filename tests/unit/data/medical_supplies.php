<?php
// Run: #php medical_supplies.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');

$fullName = 'a';    // обходимо перевірку моделі
$msupplies ='a';    // обходимо перевірку моделі

for ($index = 1; $index <= 10000; $index++)
{
    $medicalSuppl = new \backend\models\MedicalSuppliesOperation;

    $pacient = \backend\models\Pacient::find()->select(['id'])->orderBy('rand()')->one();
    $medicalSuppl->pacient_id = $pacient->id;

    $department = \backend\modules\handbook\models\Department::find()->select(['id'])->orderBy('rand()')->one();
    $medicalSuppl->department_id = $department->id;

    $hb_ms = \backend\modules\handbook\models\MedicalSupplies::find()->select(['id'])->orderBy('rand()')->one();
    $medicalSuppl->medical_supplies_id = $hb_ms->id;

    $medicalSuppl->quantity = $faker->numberBetween($min = -1000, $max = 1000);


    $medicalSuppl->docum_date = $faker->dateTime->format('Y-m-d');
    $medicalSuppl->docum_num = $faker->bothify($string = '##???');
    $medicalSuppl->docum = $faker->sentence($nbWords = 2);
    $medicalSuppl->description = $faker->realText;;


    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $medicalSuppl->user_id = $user->id;

    $medicalSuppl->updated_user_id = $user->id;

    $medicalSuppl->created_at = $faker->unixTime;
    $medicalSuppl->updated_at = $faker->unixTime;

    $medicalSuppl->fullName = $fullName;
    $medicalSuppl->msupplies = $msupplies;


    if ($medicalSuppl->validate())
    {
        $medicalSuppl->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;