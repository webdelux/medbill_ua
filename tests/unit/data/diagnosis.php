<?php
// Run: #php diagnosis.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;


$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');

for ($index = 1; $index <= 100000; $index++)
{

    $diagnosis = new \backend\modules\diagnosis\models\Diagnosis;

    $pacient = \backend\models\Pacient::find()->select(['id'])->orderBy('rand()')->one();
    $diagnosis->pacient_id = $pacient->id;

    $department = \backend\modules\handbook\models\Department::find()->select(['id'])->orderBy('rand()')->one();
    $diagnosis->department_id = $department->id;


    $icd = \backend\modules\handbookemc\models\Icd::find()->select(['id'])->orderBy('rand()')->one();
    $diagnosis->handbook_emc_icd_id = $icd->id;

    $diagnosis->date_at = $faker->dateTimeBetween($startDate = '-380 days', $endDate = '0 days')->format('Y-m-d H:i');


    $diagnosis->description = $faker->realText;
    $diagnosis->type = 2;
    $diagnosis->prophylactic_set = 1;
    $diagnosis->first_set = $faker->numberBetween($min = 1, $max = 2);

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $diagnosis->user_id = $user->id;
    $diagnosis->updated_user_id = $user->id;

    if ($diagnosis->validate())
    {
        $diagnosis->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;