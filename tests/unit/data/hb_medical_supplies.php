<?php
// Run: #php user.php

namespace tests\unit;

use Yii;

require(__DIR__ . '/../_bootstrap.php');

echo Yii::$app->name . PHP_EOL;

$categoryId = 1; // ІД батьківської категорії  "Медикаменти".

$time_start = microtime(true);


$faker = \Faker\Factory::create('ru_RU');


for ($index = 1; $index <= 50; $index++)
{
    $msHb = new \backend\models\Category;

    //$msHb->parent_id =$categoryId;
    $root = \backend\models\Category::findOne(['id' => $categoryId]);
    $msHb->appendTo($root);


    $msHb->name = $faker->sentence($nbWords = 2);
    $msHb->description = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $msHb->user_id = $user->id;

    $msHb->updated_user_id =$user->id;

    $msHb->created_at =$faker->unixTime;
    $msHb->updated_at =$faker->unixTime;



     if ($msHb->validate())
    {
        $msHb->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;



for ($index = 1; $index <= 1000; $index++)
{
    $msHb = new \backend\modules\handbook\models\MedicalSupplies;

    $categoryHb = \backend\models\Category::find()->select(['id'])->where('parent_id='.$categoryId)->orderBy('rand()')->one();
    $msHb->category_id = $categoryHb->id;

    $msHb->name = $faker->sentence($nbWords = 2);

    $measurementHb = \backend\modules\handbook\models\Measurement::find()->select(['id'])->orderBy('rand()')->one();
    $msHb->measurement_id = $measurementHb->id;

    $msHb->description = $faker->realText;

    $user = \backend\models\User::find()->select(['id'])->orderBy('rand()')->one();
    $msHb->user_id = $user->id;

//    $service->updated_user_id = $user->id;

    //$service->created_at = $faker->unixTime;
    //$service->updated_at = $faker->unixTime;



    if ($msHb->validate())
    {
        $msHb->save();
    }

    echo 'index: '.$index.PHP_EOL;
}


$time_end = microtime(true);
$time = $time_end - $time_start;


echo PHP_EOL . "Processed in {$time} seconds." . PHP_EOL;